import sys
import os
from fabric.api import local, run, cd, env, hosts
from fabric.contrib.project import rsync_project
from fabric.context_managers import shell_env

RSYNC_EXCLUDE = (
    'fabfile.py',
    'fabfile.pyc',
    'var/*',
    'web/uploads/*',
    'web/public/*',
    '.git',
    '.gitignore',
    '.idea/',
    'nbproject/',
    'README.md',
    'vendor/',
)

env.use_ssh_config = True
env.home = "/home/prodigious/portail.honda-occasions.kora.pro/portail"
env.exclude = RSYNC_EXCLUDE
env.php = 'php7.2'

def kora():
    env.host_string = 'hondacorapreprod'
    env.environment = 'kora'

def prod_setting():
    env.host_string = 'hondacoraprod'
    env.environment = 'prod'

def rsync():
    rsync_project(
        env.home, os.getcwd() + "/", exclude=env.exclude, delete=False)

def ls():
  with cd(env.home):
        run("ls")

def cc():
  with cd(env.home):
        run(env.php + " bin/console cache:clear --env=prod ")

def composer():
  with cd(env.home):
        run(env.php + " ../../composer.phar install")

def parametersYml():
    with cd(env.home):
        if env.environment == 'prod':
            run("cd app/config && cp parameters_prod.yml parameters.yml")
        else:
            run("cd app/config && cp parameters_kora_preprod.yml parameters.yml")

def assets():
    with cd(env.home):
        #run(env.php + " bin/console ckeditor:install")
        run(env.php + " bin/console assets:install --env=prod")
        run(env.php + " bin/console assetic:dump --env=prod --no-debug")

def optimizeAutoload():
    with cd(env.home):
        run("composer.phar dumpautoload -o")

def buildJs():
    local("yarn encore production")

def symLink():
    with cd(env.home):
        run("ln -s "+ env.home +"/web " + env.home)

def permission():
    with cd(env.home + "/var"):
        run("chmod -R 777 cache/")
        run("chmod -R 777 logs/")
        run("chmod -R 777 sessions/")

def removeCache():
    with cd(env.home + "/var"):
        run("rm -rf cache/")

def deployKora():
    kora()
    buildJs()
    rsync()
    composer()
    parametersYml()
    assets()
    cc()
    permission()

def prod():
    prod_setting()
    buildJs()
    rsync()
    parametersYml()
    #composer()
    assets()
    removeCache()
    cc()
    permission()
