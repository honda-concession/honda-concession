(function($) {

    $(function() {

        var listes = $('.services-picto li span.control-label__text');

        $.each(listes, function() {
            var tmp = $(this).html().trim();
            var img = $('<img/>');
            var label = $(this).closest('label').removeClass('required');

            img.attr('src', tmp);
            img.attr('width', 50);
            $(this).html(img);

        });

    });

})(jQuery);
