<?php

namespace Honda\Bundle\VehiclesBundle\Repository;

use Honda\Bundle\VehiclesBundle\Entity\Vehicles;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryRepository extends \Doctrine\ORM\EntityRepository
{

    public function findModelsByCategory($id)
    {

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select('v, s','m', 'c')
                ->from(Vehicles::class, 'v')
                ->innerJoin('v.subModel','s')
                ->innerJoin('s.model','m')
                ->innerJoin('m.category','c');

        $query = $queryBuilder->getQuery()->getResult();

        $categories = [];
        $models = [];
        $submodels = [];
        $vehicles = [];

        foreach ($query as $vehicles){
            $vehicles_id = $vehicles->getId();

               var_dump($vehicles_id);

        }

        try {
            return  $query;
        } catch (\Doctrine\ORM\NoResultException $exception) {

            return null;
        }
    }

    /**
     * @param null $slugCategory
     * @return array
     */
    public function getCategories($slugCategory = null)
    {
        $qb = $this->createQueryBuilder('this');

        $qb
            ->addSelect('image')
            ->addSelect('fileTarifs')
            ->addSelect('file')
            ->leftJoin('this.image', 'image')
            ->leftJoin('this.fileTarifs', 'fileTarifs')
            ->leftJoin('this.file', 'file')
        ;

        if ($slugCategory) {
            $qb
                ->where('this.slug LIKE :slug')
                ->setParameter('slug', $slugCategory)
            ;
        }

        $qb
            ->orderBy('this.position', $order = 'ASC');

        return $qb->getQuery()->getResult();
    }

}
