<?php

namespace Honda\Bundle\VehiclesBundle\Controller\Api;

use Honda\Bundle\CommonBundle\Service\MediaUrl;
use Honda\Bundle\VehiclesBundle\Entity\Models;
use Honda\Bundle\VehiclesBundle\Utils\StringPriceFormatter;
use Honda\Bundle\VehiclesOccasionBundle\Entity\Color;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Honda\Bundle\VehiclesBundle\Entity\Category;
use Honda\Bundle\VehiclesBundle\Entity\Layer;
use Honda\Bundle\VehiclesBundle\Entity\Vehicles;
use Honda\Bundle\DistributorBundle\Entity\Page;
use Honda\Bundle\VehiclesBundle\Entity\SubModels;

class VehiclesController extends Controller
{

    /**
     * Gestion du stock vehicules neufs
     * @Route("/api/vn/inventory", name="api_get_inventory", methods={"GET"})
     */
    public function inventoryAction()
    {
        $baseUrl = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();

        $vehicles = $this->getDoctrine()->getRepository(Vehicles::class)->getInventories();

        $data = [];

        foreach ($vehicles as $key => $vehicle) {

            $data[$key]['id'] = $vehicle->getId();
            $data[$key]['title'] = $vehicle->getName();
            $data[$key]['price'] = $vehicle->getPrice();

            $data[$key]['color'] = '';
            $data[$key]['color_icon'] = '';
            $data[$key]['color_code'] = '';
            $data[$key]['color_kora_id'] = '';
            $data[$key]['color_kora_code'] = '';
            $data[$key]['color_kora_name'] = '';

            if ($vehicle->getColor()) {

                $data[$key]['color'] = $vehicle->getColor() ? $vehicle->getColor()->getColor() : '';
                $data[$key]['color_code'] = $vehicle->getColor() ? $vehicle->getColor()->getCode() : '';

                $data[$key]['color_kora_id'] = $vehicle->getColor() ? $vehicle->getColor()->getKoraColorId() : '';
                $data[$key]['color_kora_code'] = $vehicle->getColor() ? $vehicle->getColor()->getKoraColorCode() : '';
                $data[$key]['color_kora_name'] = $vehicle->getColor() ? $vehicle->getColor()->getKoraColorName() : '';

                if ($icon = $vehicle->getColor()->getLogo()) {
                    $data[$key]['color_icon'] = $baseUrl . $this->get(MediaUrl::class)->getUrl($icon, 'big');
                }
            }

            $data[$key]['model'] = '';
            $data[$key]['kora_id'] = '';
            $data[$key]['cylinder'] = '';
            $data[$key]['driving_license_a2'] = '';
            $data[$key]['reference'] = '';
            $data[$key]['category'] = '';

            if ($subModel = $vehicle->getSubModel()) {
                $data[$key]['cylinder'] = $subModel->getCylinder();
            }

            if ($vehicle->getSubModel() && $model = $vehicle->getSubModel()->getModel()) {
                $data[$key]['kora_id'] = $vehicle->getSubModel()->getModel()->getKoraId();
                $data[$key]['model'] = $model->getName();
                $data[$key]['driving_license_a2'] = $vehicle->getSubModel()->getLicense();
                $data[$key]['reference'] = $vehicle->getSubModel()->getRef();

                if ($model->getCategory()) {
                    $data[$key]['category'] = $model->getCategory()->getName();
                }

            }

            $data[$key]['submodel'] = $vehicle->getSubModel() ? $vehicle->getSubModel()->getName() : '';
            $data[$key]['submodel_id'] = $vehicle->getSubModel() ? $vehicle->getSubModel()->getId() : '';

            $data[$key]['image'] = '';

            if ($vehicle->getImage()) {
                $data[$key]['image'] = $baseUrl . $this->get(MediaUrl::class)->getUrl($vehicle->getImage(), 'big');
            }
        }

        return $this->json($data);
    }

    /**
     * @Route("/api/vn/category", name="api_get_nv_list_category", methods={"GET"})
     */
    public function listVnCategoryAction()
    {
        $categories = $this->getDoctrine()->getRepository('HondaVehiclesBundle:Category')->findBy([], ['position' => 'ASC']);

        $data = [];

        foreach ($categories as $key => $category) {

            $data[$key]['id'] = $category->getId();
            $data[$key]['category'] = $category->getName();
            $data[$key]['category_slug'] = $category->getSlug();
            $data[$key] = $category->setRedirectionValuesToArray($data[$key]);
        }

        return $this->json($data);
    }

    /**
     * Fiche vehicle new
     * @Route("/api/vn/model/fiche/{modelSlug}", name="api_get_nv_model", requirements={"modelSlug": "[a-zA-Z0-9\-_]+"}, methods={"GET"})
     * @Route("/api/vn/model/fiche/{modelSlug}/{vehicleSlug}", name="api_get_nv_model_submodel_vehicle", requirements={"modelSlug": "[a-zA-Z0-9\-_]+", "vehicleSlug": "[a-zA-Z0-9\-]+|null"}, methods={"GET"})
     */
    public function listVnModelAction(Request $request, $modelSlug, $vehicleSlug = null)
    {
        $data = [];
        $vehicles = [];

        $baseUrl = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();
        $formatter =  $this->get(StringPriceFormatter::class);

        $model = $this->getDoctrine()->getRepository('HondaVehiclesBundle:Models')->findOneBy([
            'slug' => $modelSlug,
            'published' => true
        ]);
        $em = $this->getDoctrine()->getManager();

        $layer = $this->getDoctrine()->getRepository(Layer::class)->findOneBy(['slug' => Layer::SLUG_ID]);

        $data['model_name'] = null;
        $data['category_slug'] = null;
        $data['model_slug'] = null;
        $data['slides'] = [];
        $data['vehicle'] = null;
        $data['submodels'] = [];
        $data['additional_text'] = $model->getAdditionalText();

        if (!empty($model)) {

            $data['model_name'] = $model->getName();
            $data['category_slug'] = $model->getCategory()->getSlug();
            $data['model_slug'] = $modelSlug;

            foreach ($model->getSliderPrincipal() as $sliders) {

                $media = $sliders->getImage();
                $item = [];
                if ($media) {
                    $provider = $this->container->get($media->getProviderName());
                    $sliderUrl = $baseUrl . $provider->generatePublicUrl($media, 'reference');
                    if ($media->getProviderName() == 'sonata.media.provider.image') {
                        $item = ['type' => 'image', 'url' => $sliderUrl, 'providerReference' => null];
                    } elseif ($media->getProviderName() == 'sonata.media.provider.youtube') {
                        $item = ['type' => 'video', 'url' => $sliderUrl, 'providerReference' => $media->getProviderReference()];
                    }
                    $data['slides'][] =  $item;
                }
            }

            $selectedVehicle = $em->getRepository(Vehicles::class)->getSelectedVehicle($modelSlug, $vehicleSlug);

            $imageUrl = $colorName = $vehicleImageUrl = $vehiclePdfUrl = $vehicleDocsUrl = null;

            if ($selectedVehicle) {

                if ($selectedVehicle->getColor()) {
                    $media = $selectedVehicle->getColor()->getLogo();
                    $provider = $this->container->get($media->getProviderName());
                    $imageUrl = $baseUrl . $provider->generatePublicUrl($media, 'reference');
                    $colorName = $selectedVehicle->getColor()->getColor();
                }

                if ($selectedVehicle->getImage()) {
                    $vehicleMedia = $selectedVehicle->getImage();
                    $providerVehicle = $this->container->get($vehicleMedia->getProviderName());
                    $vehicleImageUrl = $baseUrl . $providerVehicle->generatePublicUrl($vehicleMedia, 'reference');
                }

                if ($selectedVehicle->getSubModel()->getFicheTechnique()) {
                    $vehicleFicheTechnique = $selectedVehicle->getSubModel()->getFicheTechnique();
                    $providerTechnique = $this->container->get($vehicleFicheTechnique->getProviderName());
                    $vehiclePdfUrl = $baseUrl . $providerTechnique->generatePublicUrl($vehicleFicheTechnique, 'reference');
                }

                if ($selectedVehicle->getSubModel()->getOtheDocs()) {
                    $vehicleOtherDocs = $selectedVehicle->getSubModel()->getOtheDocs();
                    $providerDocs = $this->container->get($vehicleOtherDocs->getProviderName());
                    $vehicleDocsUrl = $baseUrl . $providerDocs->generatePublicUrl($vehicleOtherDocs, 'reference');
                }

                $data['vehicle']['id'] = $selectedVehicle->getId();
                $data['vehicle']['name'] = $selectedVehicle->getName();
                $data['vehicle']['slug'] = $selectedVehicle->getSlug();
                $data['vehicle']['image'] = $vehicleImageUrl;
                $data['vehicle']['color']['name'] = $colorName;
                $data['vehicle']['color']['icon'] = $imageUrl;
                $data['vehicle']['price'] = $selectedVehicle->getPrice() ? $formatter->format($selectedVehicle->getPrice()) : $formatter->format($selectedVehicle->getSubModel()->getPublicPrice());
                $data['vehicle']['price_description'] = $layer->getPriceText();
                $data['vehicle']['reduced_price'] = $selectedVehicle->getPromoPrice() ? $formatter->format($selectedVehicle->getPromoPrice()) : $formatter->format($selectedVehicle->getSubModel()->getPromoPrice());
                $data['vehicle']['reduced_price_description'] = $layer->getPromoText();
                $data['vehicle']['garantie'] = $layer->getGarantie();
                $data['vehicle']['submodel_apartirde'] = $selectedVehicle->getSubModel()->getAPartirDe();

                $data['vehicle']['pdf'] = $vehicleDocsUrl;
                $data['vehicle']['pdf_label'] = $selectedVehicle->getSubModel()->getCtaOtherDocs();
                $data['vehicle']['pdf_lien'] = $selectedVehicle->getSubModel()->getOtherDocsLink();
                $data['vehicle']['pdf_lien_externe'] = $selectedVehicle->getSubModel()->getOtherDocsExternalLink();

                $data['vehicle']['fiche_technique_pdf'] = $vehiclePdfUrl;
                $data['vehicle']['fiche_technique_pdf_label'] = $selectedVehicle->getSubModel()->getCtaFicheTechnique();
                $data['vehicle']['fiche_technique_lien'] = $selectedVehicle->getSubModel()->getFicheTechniqueLink();
                $data['vehicle']['fiche_technique_lien_externe'] = $selectedVehicle->getSubModel()->getFicheTechniqueExternalLink();

                $data['vehicle']['description'] = $selectedVehicle->getSubModel()->getDescription();
                $data['vehicle']['new'] = $selectedVehicle->getNewTag();
                $data['vehicle']['selected'] = true;
                $data['vehicle']['subModel_id'] = null;
                if ($selectedVehicle->getSubModel()) {
                    $data['vehicle']['subModel_id'] = $selectedVehicle->getSubModel()->getId();
                }

            }

            $result = [];
            foreach ($model->getSubModels() as $key => $submodel) {
                $startDate = $submodel->getStartDate();
                $endDate = $submodel->getEndDate();
                $currentDate = new \DateTime();
                try {
                    if (($startDate <= $currentDate) && ($endDate >= $currentDate) && $submodel->isActive()) {
                        $result[$key] = $submodel;
                    }
                } catch(\Exception $e) {
                    continue;
                }
            }

            if (is_array($result)) {
                usort($result, function($a, $b) {
                    return strcmp($a->getPosition(), $b->getPosition());
                });

                foreach ($result as $key => $submodel) {

                    $data['submodels'][$key]['id'] = $submodel->getId();
                    $data['submodels'][$key]['name'] = $submodel->getName();
                    $data['submodels'][$key]['slug'] = $submodel->getSlug();
                    $data['submodels'][$key] = $submodel->setRedirectionValuesToArray($data['submodels'][$key]);
                    $price = $submodel->getPublicPrice();
                    $promo = $submodel->getPromoPrice();
                    $desc = $submodel->getDescription();
                    $data['submodels'][$key]['vehicles'] = [];

                    $vehicleCollection = $em->getRepository(Vehicles::class)->getVehiclesBySubModel($submodel);

                    if ($vehicleCollection) {

                        foreach ($vehicleCollection as $vehicle) {

                            $imageUrl = $colorName = $vehicleImageUrl = null;

                            if ($vehicle->getColor()) {
                                $media = $vehicle->getColor()->getLogo();
                                $provider = $this->container->get($media->getProviderName());
                                $imageUrl = $baseUrl . $provider->generatePublicUrl($media, 'reference');
                                $colorName = $vehicle->getColor()->getColor();
                            }
                            if ($vehicle->getImage()) {
                                $vehicleMedia = $vehicle->getImage();
                                $providerVehicle = $this->container->get($vehicleMedia->getProviderName());
                                $vehicleImageUrl = $baseUrl . $providerVehicle->generatePublicUrl($vehicleMedia, 'reference');
                            }

                            $vehicles['id'] = $vehicle->getId();
                            $vehicles['name'] = $vehicle->getName();
                            $vehicles['slug'] = $vehicle->getSlug();
                            $vehicles['image'] = $vehicleImageUrl;
                            $vehicles['color']['name'] = $colorName;
                            $vehicles['color']['icon'] = $imageUrl;
                            $vehicles['price'] = $vehicle->getPrice() ? $formatter->format($vehicle->getPrice()) : $formatter->format($price);
                            $vehicles['price_description'] = $layer->getPriceText();
                            $vehicles['reduced_price'] = $vehicle->getPromoPrice() ? $formatter->format($vehicle->getPromoPrice()) : $formatter->format($promo);
                            $vehicles['reduced_price_description'] = $layer->getPromoText();
                            $vehicles['submodel_apartirde'] = $selectedVehicle->getSubModel()->getAPartirDe();
                            $vehicles['garantie'] = $layer->getGarantie();
                            $vehicles['description'] = $desc;
                            $vehicles['new'] = $vehicle->getNewTag();

                            $vehicles['selected'] = false;

                            if ($selectedVehicle and $selectedVehicle->getId() == $vehicle->getId()) {
                                $vehicles['selected'] = true;
                            }

                            $vehicles['subModel_id'] = null;
                            if ($vehicle->getSubModel()) {
                                $vehicles['subModel_id'] = $vehicle->getSubModel()->getId();
                            }

                            $data['submodels'][$key]['vehicles'][] = $vehicles;

                        }
                    }
                }
            }
        }

        return $this->json($data);
    }

    /**
     * @Route("/api/vn/model/fiche-presentation/{modelSlug}", name="api_get_nv_list_model_presentation", requirements={"modelSlug": "[a-zA-Z0-9\-_]+"}, methods={"GET"})
     */
    public function listVnModelPresentationAction(Request $request, $modelSlug)
    {
        $data = [];

        $baseUrl = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();

        $model = $this->getDoctrine()->getRepository('HondaVehiclesBundle:Models')->findOneBy([
            'slug' => $modelSlug,
            'published' => true,
        ]);

        $data['presentation']['submodels'] = [];
        $data['technologie']['slides'] = [];
        $data['technologie']['features'] = [];
        $data['edito']['title'] = null;
        $data['edito']['description'] = null;
        $data['btn_label'] = null;
        $data['btn_link'] = null;

        if (!empty($model)) {

            foreach ($model->getSubModels() as $key => $submodel) {
                $startDate = $submodel->getStartDate();
                $endDate = $submodel->getEndDate();
                $currentDate = new \DateTime();
                $isValid = false;
                if (($startDate <= $currentDate) && ($endDate >= $currentDate) && $submodel->isActive()) {
                    $isValid = true;
                }
                if (!$isValid) {
                    continue;
                }
                $data['presentation']['submodels'][$key]['name'] = $submodel->getName();
                $data['presentation']['submodels'][$key]['properties'][] = [
                    'name' => 'Cylindrée (cm³)',
                    'value' => $submodel->getCylinder(),
                ];
                foreach ($submodel->getPropertyValues() as $propertyValue) {
                    $name = $propertyValue->getProperty()->getName();
                    $data['presentation']['submodels'][$key]['properties'][] = [
                        'name' => $name,
                        'value' => $propertyValue->getValue(),
                    ];
                }
                if (count($data['presentation']['submodels'][$key]['properties']) == 1) {
                    $properties = $this->getDoctrine()->getRepository('HondaVehiclesBundle:Property')->findAll();
                    foreach ($properties as $property) {
                        $data['presentation']['submodels'][$key]['properties'][] = [
                            'name' => $property->getName(),
                            'value' => null,
                        ];
                    }
                }
            }

            foreach ($model->getSliderTechnology() as $sliders) {
                $media = $sliders->getImage();
                $item = [];

                if ($media) {

                    $provider = $this->container->get($media->getProviderName());
                    $sliderUrl = $baseUrl . $provider->generatePublicUrl($media, 'reference');

                    if ($media->getProviderName() == 'sonata.media.provider.image') {
                        $item = ['type' => 'image', 'url' => $sliderUrl, 'providerReference' => null];

                    } elseif ($media->getProviderName() == 'sonata.media.provider.youtube') {
                        $item = ['type' => 'video', 'url' => $sliderUrl, 'providerReference' => $media->getProviderReference()];
                    }

                    $data['technologie']['slides'][] = $item;
                }

            }

            foreach ($model->getPictos() as $key => $features) {
                $picto = $features->getPicto();
                $provider1 = $this->container->get($picto->getProviderName());
                $pictoUrl = $baseUrl . $provider1->generatePublicUrl($picto, 'reference');
                $data['technologie']['features'][$key]['titre'] = $features->getName();
                $data['technologie']['features'][$key]['abr'] = $features->getLayer();
                $data['technologie']['features'][$key]['icon'] = $pictoUrl;
            }

            $data['edito']['description'] = $model->getDescription();
            $data['edito']['title'] = $model->getTitleDescription();
            $data['btn_link'] = $model->getLink();
            $data['btn_label'] = $model->getCtaLabel();
        }

        return $this->json($data);
    }

    /**
     * Liste vehicles neufs
     *
     * @Route("/api/vn/gamme/", name="api_get_list_categories", methods={"GET"})
     * @Route("/api/vn/gamme/{categorySlug}", name="api_get_list_by_category", requirements={"categorySlug": "[a-zA-Z0-9\-_]+|null"}, defaults={"categorySlug": null}, methods={"GET"})
     * @param Request $request
     * @param null $categorySlug
     * @return JsonResponse
     */
    public function listVnGammeAction(Request $request, $categorySlug = null)
    {
        $data = [];

        $baseUrl = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();
        $em = $this->getDoctrine()->getManager();
        $formatter =  $this->get(StringPriceFormatter::class);

        $description = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['slug' => 'vehicles-neuves']);

        $data['page_description'] = null;
        if ($description) {
            $data['page_title'] = $description->getTitle();
            $data['page_description'] = $description->getText();
        }

        $layer = $this->getDoctrine()->getRepository(Layer::class)->findOneBy(['slug' => Layer::SLUG_ID]);

        $data['price_description'] = '';
        $data['reduced_price_description'] = '';
        $data['stock_description'] = '';
        $data['garantie'] = '';

        if ($layer) {
            $data['price_description'] = $layer->getPriceText();
            $data['reduced_price_description'] = $layer->getPromoText();
            $data['stock_description'] = $layer->getStockText();
            $data['garantie'] = $layer->getGarantie();
            if ($layer->getFileTarifs()) {
                $tarifsCategory = $layer->getFileTarifs();
                $provider2 = $this->container->get($tarifsCategory->getProviderName());
                $tarifsImageUrl = $baseUrl . $provider2->generatePublicUrl($tarifsCategory, 'reference');
            }
        }

        $categories = $em->getRepository(Category::class)->getCategories($categorySlug);
        $categoryCollection = [];
        $category = [];

        if(count($categories)) {

            foreach ($categories as $categoryItem) {

                if ($categoryItem->getImage()) {

                    $mediaCategory = $categoryItem->getImage();
                    $provider = $this->container->get($mediaCategory->getProviderName());
                    $categoryImageUrl = $baseUrl . $provider->generatePublicUrl($mediaCategory, 'reference');
                }

                if ($categoryItem->getFile()) {

                    $brochureCategory = $categoryItem->getFile();
                    $provider1 = $this->container->get($brochureCategory->getProviderName());
                    $brochureImageUrl = $baseUrl . $provider1->generatePublicUrl($brochureCategory, 'reference');
                }

                $category['category'] =  $categoryItem->getName();
                $category['category_slug'] =  $categoryItem->getSlug();
                $category['category_id'] =  $categoryItem->getId();
                $category['description'] =  $categoryItem->getDescription();
                $category['image_category'] =  $categoryImageUrl ?? null;
                $category['tarif_link'] =  $tarifsImageUrl ?? null;
                $category['brochure_link'] =  $brochureImageUrl ?? null;

                $category['models'] = [];
                $category = $categoryItem->setRedirectionValuesToArray($category);

                $modelCollection = [];
                $modelItem = [];

                $models = $em->getRepository(Models::class)->getModelsByCategory($categoryItem);

                if (count($models)) {

                    foreach ($models as $model) {

                        $modelItem['slug'] = $model->getSlug();

                        $subModelCollection = [];
                        $subModelItem = [];

                        if (count($model->getSubModels())) {

                            foreach ($model->getSubModels() as $v => $submodel) {

                                $subModelItem['sub_model_id'] = $submodel->getId();
                                $subModelItem['submodel_name'] = $submodel->getName();
                                $subModelItem['cylinder'] = $submodel->getCylinder();

                                $subModelItem['dispo_colors'] = [];
                                $subModelItem['vehicles_ids'] = [];

                                $subModelItem['submodel_ids'][] = $submodel->getId();
                                $subModelItem['driving_licence'] = $submodel->getLicense();
                                $subModelItem['price'] = $formatter->format($submodel->getPublicPrice());
                                $subModelItem['reduced_price'] = $formatter->format($submodel->getPromoPrice());
                                $subModelItem['reference'] = $submodel->getRef();
                                $subModelItem['image']['url'] = null;
                                $subModelItem['image']['srcsets'] = [];
                                $subModelItem['vehicle_slug'] = null;
                                $subModelItem['vehicle_name'] = null;
                                $subModelItem['vehicle_id'] = null;
                                $subModelItem = $submodel->setRedirectionValuesToArray($subModelItem);
                                if ($submodel->getVehicles()) {

                                    foreach ($submodel->getVehicles() as $v => $vehicle) {

                                        if ($vehicle->isPublished()) {

                                            if ($vehicle->getImage()) {

                                                $mediaModel = $vehicle->getImage();
                                                $provider3 = $this->container->get($mediaModel->getProviderName());
                                                $modelImageUrl = $baseUrl . $provider3->generatePublicUrl($mediaModel, 'reference');
                                                $smallImageUrl = $baseUrl . $provider3->generatePublicUrl($mediaModel, 'small');
                                                $mediumImageUrl = $baseUrl . $provider3->generatePublicUrl($mediaModel, 'medium');
                                                $bigImageUrl = $baseUrl . $provider3->generatePublicUrl($mediaModel, 'big');
                                                $srcSets = [$smallImageUrl, $mediumImageUrl, $bigImageUrl];
                                            }

                                            if ($v == 0 || $vehicle->getSelected()) {
                                                $subModelItem['image']['url'] = $modelImageUrl ?? null;
                                                $subModelItem['image']['srcsets'] = $srcSets ?? [];
                                                $subModelItem['vehicle_slug'] = $vehicle->getSlug();
                                                $subModelItem['vehicle_name'] = $vehicle->getName();
                                                $subModelItem['vehicle_id'] = $vehicle->getId();
                                            }

                                            $imageUrl = null;
                                            $colorName = null;
                                            $vehiclesId = [];

                                            if ($vehicle->getColor()) {

                                                $media = $vehicle->getColor()->getLogo();
                                                $provider4 = $this->container->get($media->getProviderName());
                                                $imageUrl = $baseUrl . $provider4->generatePublicUrl($media, 'reference');
                                                $colorName = $vehicle->getColor()->getColor();
                                                $colorId = $vehicle->getColor()->getId();

                                                if ($colorId) {
                                                    $colors['color'] = $colorName;
                                                    $colors['icon'] = $imageUrl;
                                                }

                                                $subModelItem['dispo_colors'][] = $colors;
                                            }

                                            $vehiclesId['vehicles_id'] = $vehicle->getId();
                                            $vehiclesId['color'] = $colorName;
                                            $vehiclesId['icon'] = $imageUrl;
                                            $subModelItem['vehicles_ids'][] = $vehiclesId;

                                        }
                                    }
                                }

                                $subModelCollection[] = $subModelItem;
                            }
                        }

                        $modelItem['submodels'] = $subModelCollection;
                        $modelCollection[] = $modelItem;
                    }
                }

                $category['models'] = $modelCollection;

                $categoryCollection[] = $category;
            }

            $data['categories'] = $categoryCollection;

        }

        return $this->json($data);
    }

    /**
     * @Route("/api/vn/fiche/{vehicleId}", name="api_get_fiche_vehicle", requirements={"vehicleId": "\d+"}, methods={"GET"})
     * @param Request $request
     * @param $vehicleId
     * @return JsonResponse
     */
    public function ficheVehicleAction(Request $request, $vehicleId)
    {
        $data = [];
        $layer = $this->getDoctrine()->getRepository(Layer::class)->findOneBy(['slug' => Layer::SLUG_ID]);
        $vehicle = $this->getDoctrine()->getRepository(Vehicles::class)->getVehicleInfosById($vehicleId);
        $baseUrl = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();
        $formatter =  $this->get(StringPriceFormatter::class);

        if (!$vehicle) {
            throw $this->createNotFoundException();
        }

        $data['id'] = '';
        $data['name'] = '';
        $data['model_name'] = '';
        $data['kora_id'] = '';
        $data['image'] = '';
        $data['color']['name'] = '';
        $data['color']['icon'] = '';
        $data['price'] = '';
        $data['reduced_price'] = '';
        $data['submodel_apartirde'] = '';
        $data['pdf'] = '';
        $data['pdf_label'] = '';
        $data['pdf_lien'] = '';
        $data['pdf_lien_externe'] = '';

        $data['fiche_technique_pdf'] = '';
        $data['fiche_technique_pdf_label'] = '';

        $data['fiche_technique_lien'] = '';
        $data['fiche_technique_lien_externe'] = '';

        $data['description'] = '';
        $data['price_description'] = '';
        $data['reduced_price_description'] = '';
        $data['stock_description'] = '';
        $data['garantie'] = '';

        $mediaUrl = $this->get(MediaUrl::class);

        if ($vehicle->getColor()) {
            $media = $vehicle->getColor()->getLogo();
            $imageUrl = $baseUrl . $mediaUrl->getUrl($media);
            $colorName = $vehicle->getColor()->getColor();
        }

        if ($vehicleMedia = $vehicle->getImage()) {
            $vehicleImageUrl = $baseUrl . $mediaUrl->getUrl($vehicleMedia);
        }

        if ($vehicleFicheTechnique = $vehicle->getSubmodel()->getFicheTechnique()) {
            $vehiclePdfUrl = $baseUrl . $mediaUrl->getUrl($vehicleFicheTechnique);
        }

        if ($vehicleOtherDocs = $vehicle->getSubmodel()->getOtheDocs()) {
            $vehicleDocsUrl = $baseUrl . $mediaUrl->getUrl($vehicleOtherDocs);
        }

        if ($vehicle->getSubmodel() && $vehicle->getSubmodel()->getModel()) {
            $model = $vehicle->getSubmodel()->getModel();
            $modelName = $model->getName();
            $koraId = $model->getKoraId();
        }

        if ($vehicle) {
            $data['id'] = $vehicle->getId();
            $data['name'] = $vehicle->getName();
            $data['model_name'] = isset($modelName) ? $modelName : null;
            $data['kora_id'] = isset($koraId) ? $koraId : null;
            $data['image'] = isset($vehicleImageUrl) ? $vehicleImageUrl : '';
            $data['color']['name'] = isset($colorName) ? $colorName : '';
            $data['color']['icon'] = isset($imageUrl) ? $imageUrl : '';
            $data['price'] = $vehicle->getPrice() ? $formatter->format($vehicle->getPrice()) : $formatter->format($vehicle->getPublicPrice());
            $data['reduced_price'] = $vehicle->getPromoPrice() ? $formatter->format($vehicle->getPromoPrice()) : $formatter->format($vehicle->getSubmodel()->getPromoPrice());
            $data['submodel_apartirde'] = $vehicle->getSubmodel()->getApartirDe();

            $data['pdf'] = isset($vehicleDocsUrl) ? $vehicleDocsUrl : '';
            $data['pdf_label'] = $vehicle->getSubmodel()->getCtaOtherDocs();
            $data['pdf_lien'] = $vehicle->getSubmodel()->getOtherDocsLink();
            $data['pdf_lien_externe'] = $vehicle->getSubmodel()->getOtherDocsExternalLink();

            $data['fiche_technique_pdf'] = isset($vehiclePdfUrl) ? $vehiclePdfUrl : '';
            $data['fiche_technique_pdf_label'] = $vehicle->getSubmodel()->getCtaFicheTechnique();
            $data['fiche_technique_lien'] = $vehicle->getSubmodel()->getFicheTechniqueLink();
            $data['fiche_technique_lien_externe'] = $vehicle->getSubmodel()->getFicheTechniqueExternalLink();

            $data['description'] = $vehicle->getSubmodel()->getDescription();
        }

        if ($layer) {
            $data['price_description'] = $layer->getPriceText();
            $data['reduced_price_description'] = $layer->getPromoText();
            $data['stock_description'] = $layer->getStockText();
            $data['garantie'] = $layer->getGarantie();
        }

        return $this->json($data);
    }

    /**
     * @Route("/api/vn/list/submodel")
     *
     */
    public function listSubModelsAction()
    {

        $collection = $this->getDoctrine()->getRepository(SubModels::class)->findBy(['published' => true]);

        $data = [];

        foreach($collection as $key => $subModel) {

            $data[$key]['id'] = $subModel->getId();
            $data[$key]['title'] = $subModel->getName();

            $data[$key]['kora_id'] = null;
            $data[$key]['model'] = null;

            if ($subModel->getModel()) {
                $data[$key]['kora_id'] = $subModel->getModel()->getKoraId();
                $data[$key]['model'] = $subModel->getModel()->getName();
            }
        }

        return $this->json($data);

    }

    /**
     * @Route("/api/vn/modele-selectione/{subModelId}", name="modele_selectione", requirements={"subModelId": "\d+"})
     * @param $subModelId
     * @return JsonResponse
     */
    public function modeleSelectioneAction($subModelId)
    {
        $data = [];
        $subModel = $this->getDoctrine()->getRepository(SubModels::class)->getSubModelById($subModelId);

        if ($subModel) {

            $data['id'] = $subModel->getId();
            $data['kora_id'] = $subModel->getModel() ? $subModel->getModel()->getKoraId() : null;
            $data['label_prive'] = $subModel->getRef();
            $data['label_public'] = $subModel->getName();
            $data['image'] = null;
            $data['ordre'] = null;
            $data['status'] = null;

            $data['categories'] = [];

            if ($model = $subModel->getModel()) {

                if ($category = $model->getCategory()) {

                    $categoryData = [];
                    $categoryData['nom'] = $category->getName();
                    $categoryData['image'] = $this->getMediaUrl($category->getImage());
                    $categoryData['order'] = $category->getPosition();

                    $data['categories'][] = $categoryData;
                }

            }

            $data['couleurs'] = [];
            $data['groupes'] = [];
            $data['prix_ttc'] = $subModel->getPublicPrice();
            $data['couleurChoisis'] = 'rouge';
        }

        return $this->json($data);
    }

    /**
     * @Route("/api/vn/moto-selectionee/{vehicleId}", name="moto_selectionee", requirements={"vehicleId": "\d+"})
     * @param $vehicleId
     * @return JsonResponse
     */
    public function motoSelectioneeAction($vehicleId)
    {
        $data = [];
        $vehicle = $this->getDoctrine()->getRepository(Vehicles::class)->getVehicleById($vehicleId);

        if ($vehicle) {

            $data['id'] = (int) $vehicle->getId();

            //$data['submodel_id'] =  null;
            $data['kora_id'] = null;
            $data['label_prive'] = null;
            $data['label_public'] = null;

            if ($subModel = $vehicle->getSubModel()) {
                //$data['submodel_id'] = $vehicle->getSubModel()->getId();
                $data['kora_id'] = $subModel->getModel() ? (int) $subModel->getModel()->getKoraId() : null;

                $data['label_prive'] = $subModel->getRef();
                $data['label_public'] = $subModel->getName();
            }

            $data['image'] = null;
            $data['ordre'] = null;
            $data['status'] = null;

            $data['categories'] = [];

            if ($vehicle->getSubModel() && $vehicle->getSubModel()->getModel() && $model = $vehicle->getSubModel()->getModel()) {

                if ($category = $model->getCategory()) {

                    $categoryData = [];
                    $categoryData['nom'] = $category->getName();
                    $categoryData['image'] = $this->getMediaUrl($category->getImage());
                    $categoryData['order'] = $category->getPosition();

                    $data['categories'][] = $categoryData;
                }
            }

            $data['couleurs'] = [];
            $data['groupes'] = [];
            $data['prix_ttc'] = $vehicle->getSubModel() ? $vehicle->getSubModel()->getPublicPrice() : null;

            $data['couleurChoisis'] = ['label' => null, 'code' => null, 'visuel' => null, ];

            if ($vehicle->getColor()) {

                $data['couleurChoisis'] = [
                    'id' =>  (int) $vehicle->getColor()->getKoraColorId(),
                    'label' => $vehicle->getColor()->getKoraColorName(),
                    'code' => $vehicle->getColor()->getKoraColorCode(),
                    'visuel' => null,
                ];
            }

        }

        return $this->json($data);
    }

    /**
     * @param $media
     * @param string $reference
     * @return string
     */
    protected function getMediaUrl($media, $reference = 'reference')
    {
        if (!$media) {
            return null;
        }

        $baseUrl = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();
        $provider = $this->container->get($media->getProviderName());
        $imageUrl = $baseUrl . $provider->generatePublicUrl($media, 'reference');

        return $imageUrl;
    }
}
