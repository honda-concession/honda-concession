<?php

namespace Honda\Bundle\VehiclesBundle\Command;


use Honda\Bundle\VehiclesBundle\Manager\SubModelsManager;
use Honda\Bundle\VehiclesBundle\Manager\VehicleManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class ExpiredCommand
 * @package Honda\Bundle\VehiclesBundle\Command
 */
class ExpiredCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'honda:expired';
    
    
    protected function configure()
    {
        $this
            ->setDescription('Deactive les données expirées')
        ;
    }
    
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Début désactivation vehicules neufs expirés</info>');
    
        $counter = $this->getContainer()->get(VehicleManager::class)->disabledVehicles();
        $output->writeln(sprintf('<info>%s de vehicules ont été désactivés</info>', $counter));
    
        $output->writeln('<info>Fin désactivation vehicules neufs expirés</info>');
        
    
        $output->writeln('<info>Début désactivation sous models expirés</info>');
    
        $counter = $this->getContainer()->get(SubModelsManager::class)->disabledSubModels();
        $output->writeln(sprintf('<info>%s de sous models ont été désactivés</info>', $counter));
    
        $output->writeln('<info>Fin désactivation sous models expirés</info>');
        
    }
}