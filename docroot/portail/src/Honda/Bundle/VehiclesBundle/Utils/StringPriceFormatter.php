<?php

namespace Honda\Bundle\VehiclesBundle\Utils;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class StringPriceFormatter
 * @package Honda\Bundle\VehiclesBundle\Utils
 */
class StringPriceFormatter
{
    /**
     * @var RequestStack
     */
    protected $requestStack;
    
    protected $numberFormatter;

    protected $formatType;
    
    /**
     * StringPriceFormatter constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }
    
    /**
     * @param $priceAsString
     * @return bool|string
     */
    public function format($priceAsString)
    {
        $locale = $this->requestStack->getCurrentRequest()->getLocale();
        
        $formatter = new \NumberFormatter($locale, $this->getNumberFormatter());
        
        return $formatter->format($this->sanitize($priceAsString), $this->getFormatType());
    }
    
    /**
     * @return mixed
     */
    public function getNumberFormatter()
    {
        return $this->numberFormatter ? $this->numberFormatter : \NumberFormatter::DECIMAL;
    }
    
    /**
     * @param mixed $numberFormatter
     * @return StringPriceFormatter
     */
    public function setNumberFormatter($numberFormatter)
    {
        $this->numberFormatter = $numberFormatter;
        
        return $this;
    }
    
    /**
     * @param $priceAsString
     * @return string
     */
    protected function sanitize($priceAsString)
    {
        $priceAsString = trim($priceAsString);
        $priceAsString = preg_replace('/\s+/', '', $priceAsString);
        
        return (float) $priceAsString;
    }
    
    /**
     * @return mixed
     */
    public function getFormatType()
    {
        return $this->formatType ? $this->formatType : \NumberFormatter::TYPE_DEFAULT;
    }
    
    /**
     * @param mixed $formatType
     * @return StringPriceFormatter
     */
    public function setFormatType($formatType)
    {
        $this->formatType = $formatType;
        
        return $this;
    }
    
    
}