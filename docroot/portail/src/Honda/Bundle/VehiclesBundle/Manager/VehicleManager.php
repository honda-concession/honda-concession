<?php

namespace Honda\Bundle\VehiclesBundle\Manager;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\Bundle\VehiclesBundle\Entity\Vehicles;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class VehicleManager
{

    /**
     * @var ObjectManager
     */
    protected $em;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository|\Honda\Bundle\VehiclesBundle\Repository\VehiclesRepository
     */
    protected $repository;

    /**
     * VehicleManager constructor.
     * @param $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Vehicles::class);
    }

    /**
     * @return int
     */
    public function disabledVehicles()
    {
        $vehicles = $this->repository->getExpiredVehicles();

        foreach ($vehicles as $vehicle) {
            $vehicle->setPublished(false);
        }

        $this->em->flush();

        return count($vehicles);
    }
}
