<?php

namespace Honda\Bundle\VehiclesBundle\Manager;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\Bundle\VehiclesBundle\Entity\SubModels;
use Honda\Bundle\VehiclesBundle\Entity\Vehicles;


/**
 * Class SubModelsManager
 * @package Honda\Bundle\VehiclesBundle\Manager
 */
class SubModelsManager
{
    
    /**
     * @var ObjectManager
     */
    protected $em;
    
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository|\Honda\Bundle\VehiclesBundle\Repository\VehiclesRepository
     */
    protected $repository;
    
    /**
     * SubModelsManager constructor.
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(SubModels::class);
    }
    
    /**
     * @return int
     */
    public function disabledSubModels()
    {
        $collection = $this->repository->getExpiredSubModels();
        
        foreach ($collection as $item) {
            $item->setActive(false);
        }
        
        $this->em->flush();
        
        return count($collection);
    }
    
    
}
