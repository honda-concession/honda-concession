<?php

namespace Honda\Bundle\VehiclesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class PictosAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name', null, array(
                    'label' => 'Nom'
                ))

        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('picto', 'string', [
                    'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'
                ])
                ->add('name', null, array(
                    'label' => 'Nom (contenu du layer)'
                ))
                ->add('layer', null, array(
                    'label' => 'Texte sous le picto'
                ))
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name', null, array(
                    'label' => 'Nom (contenu du layer)'
                ))
                ->add('layer', null, array(
                    'label' => 'Texte sous le picto'
                ))
                ->add('picto', 'sonata_type_model_list', [
                    'required' => false,
                    'label' => 'Image',
                    'by_reference' => false,
                        ], [
                    'link_parameters' => [
                        'context' => 'image',
                    ]
                ])
        ;
    }
    
    protected function configureBatchActions($actions)
    {

        unset($actions['delete']);

        return $actions;
    }

}
