<?php

namespace Honda\Bundle\VehiclesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\UnitOfWork;

class SubModelPropertyAdmin extends AbstractAdmin
{

    public $last_position = 0;
    private $positionService;
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name', null, array(
                    'label' => 'Caractéristiques principales'
                ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('name', null, array(
                    'label' => 'Caractéristiques principales'
                ))
                ->add('position', 'actions', array(
                    'label' => 'Position',
                    'actions' => array(
                        'move' => array('template' => 'PixSortableBehaviorBundle:Default:_sort_drag_drop.html.twig', 'enable_top_bottom_buttons' => false),
                )))
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;

    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->with('Sous Modèle', ['class' => 'col-md-12'])
                ->add('name', null, array(
                    'label' => 'Nom'
                ))
                ->end()
        ;
    }

    protected function configureBatchActions($actions)
    {
        unset($actions['delete']);

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }
}
