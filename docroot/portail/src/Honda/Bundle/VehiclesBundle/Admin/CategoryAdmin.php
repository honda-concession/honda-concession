<?php

namespace Honda\Bundle\VehiclesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Honda\Bundle\CommonBundle\Admin\Traits\RedirectionAdminTrait;

class CategoryAdmin extends AbstractAdmin
{

    use RedirectionAdminTrait;

    public $last_position = 0;
    private $positionService;
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name', null, array(
                    'label' => 'Nom de la catégorie'
                ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('position', 'actions', array(
                    'label' => 'Position',
                    'actions' => array(
                        'move' => array('template' => 'PixSortableBehaviorBundle:Default:_sort_drag_drop.html.twig'),
                )))
                ->add('name', null , array (
                    'label' => 'Nom des catégories'
                ))
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->with('Catégories', ['class' => 'col-md-9'])
                    ->add('name', null , array (
                        'label' => 'Nom de la catégorie'
                    ))
                    ->add('description', 'textarea' , array (
                        'label' => 'Texte de présentation'
                    ))
                    ->add('image', 'sonata_type_model_list', [
                        'label' => 'Image',
                        'by_reference' => false,
                        'required' => true,
                            ], [
                        'link_parameters' => [
                            'context' => 'image',
                        ]
                    ])
                    ->add('subGroups', 'sonata_type_model', [
                        'multiple' => true,
                        'required' => false,
                        'by_reference' => false,
                        'label' => 'Groupes',
                        'property' => 'name',
                        'btn_add' => false
                    ])
//                ->add('fileTarifs', 'sonata_type_model_list', [
//                    'label' => 'Fichier Tarifs',
//                    'by_reference' => false,
//                    'required' => true,
//                        ], [
//                    'link_parameters' => [
//                        'context' => 'file',
//                    ]
//                ])
                ->end();
            $this->addRedirectionBlock($formMapper);
            $formMapper
                ->with('Brochure', ['class' => 'col-md-9'])
                ->add('file', 'sonata_type_model_list', [
                    'label' => 'Fichier',
                    'by_reference' => false,
                    'required' => true,
                        ], [
                    'link_parameters' => [
                        'context' => 'file',
                    ]
                ])
                ->add('labelCta' , null, array (
                    'label' => 'Intitulé du bouton'
                ))
                ->end()
        ;
    }

    protected function configureBatchActions($actions)
    {

        unset($actions['delete']);

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }
}
