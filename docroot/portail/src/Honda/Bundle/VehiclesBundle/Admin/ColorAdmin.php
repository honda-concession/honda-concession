<?php

namespace Honda\Bundle\VehiclesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;

class ColorAdmin extends AbstractAdmin
{

    use ActivationAdminTrait;

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('logo', 'string', [
                    'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'
                ])
                ->add('color', null, array(
                    'label' => 'Couleur'
                ))
                ->add('code', null, array(
                    'label' => 'Code',
                ))
                ->add('koraColorName', null, array(
                    'label' => 'Kora nom couleur',
                ))
                ->add('koraColorCode', null, array(
                    'label' => 'Kora code couleur',
                ))
                ->add('koraColorId', null, array(
                    'label' => 'Kora id couleur',
                ))
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('color', null, array(
                    'label' => 'Couleur'
                ))
                ->add('code', null, array(
                    'label' => 'Code',
                    'required' => false
                ))
                ->add('koraColorName', null, array(
                    'label' => 'Kora nom couleur',
                    'required' => false,
                ))
                ->add('koraColorCode', null, array(
                    'label' => 'Kora code couleur',
                    'required' => false,
                ))
                ->add('koraColorId', null, array(
                    'label' => 'Kora id couleur',
                    'required' => false,
                ))
                ->add('logo', 'sonata_type_model_list', [
                    'label' => 'Logo',
                    'required' => false,
                        ], [
                    'link_parameters' => [
                        'context' => 'image',
                        'filter' => [
                            'context' => [
                                'value' => 'image'
                            ]
                        ]
                    ]
                ])
        ;
    }
}
