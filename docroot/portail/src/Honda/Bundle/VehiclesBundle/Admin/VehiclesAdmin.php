<?php

namespace Honda\Bundle\VehiclesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type As Type;
use Sonata\AdminBundle\Route\RouteCollection;

use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;
use Honda\Bundle\CommonBundle\Admin\Traits\RedirectionAdminTrait;
use Honda\Bundle\VehiclesBundle\Entity\Vehicles;

class VehiclesAdmin extends AbstractAdmin
{

    use ActivationAdminTrait;
    use RedirectionAdminTrait;

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name', null, array(
                    'label' => 'Nom'
                ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('position_', 'actions', array(
                    'actions' => array(
                        'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig')
                    )
                ))
                ->add('image', 'string', [
                    'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'
                ])
                ->add('name', null, array(
                    'label' => 'Nom'
                ))
                ->add('subModel', null, array(
                    'label' => 'Sous Modèle',
                    'sortable' => 'subModel.name',
                ))
                ->add('price', null, array(
                    'label' => 'Prix'
                ))
                ->add('promoPrice', null, array(
                    'label' => 'Prix promo'
                ))
                ->add('license', null, array(
                    'label' => 'License'
                ))
                ->add('published', null, ['editable' => true])
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();
        $price = $promoPrice = $license = null;
        if($subject->getSubmodel()){
            $price = $subject->getSubmodel()->getPublicPrice();
            $promoPrice =  $subject->getSubmodel()->getPromoPrice();
            $license =  $subject->getSubmodel()->getLicense();
        }

        $formMapper
            ->with('Fiche moto', array('class' => 'col-md-8'))
                ->add('name', null, array(
                    'label' => 'Nom'
                ))
                ->add('subModel', null , array(
                    'label' => 'Sous modèle',
                ))
                ->add('image', 'sonata_type_model_list', [
                    'required' => false,
                    'label' => 'Image',
                    'by_reference' => false,
                        ], [
                    'link_parameters' => [
                        'context' => 'image',
                    ]
                ])
                ->add('price', null, array(
                    'label' => 'Prix',
                    'data' => $subject->getPrice() ? $subject->getPrice() : $price,
                    'help' => 'Le prix sera automatiquement généré après la soumission de la fiche.'
                ))
                ->add('promoPrice', null, array(
                    'label' => 'Prix promo',
                    'data' => $subject->getPromoPrice() ? $subject->getPromoPrice() :  $promoPrice,
                    'help' => 'Le prix promo sera automatiquement généré après la soumission de la fiche.'
                ))
                ->add('license', null, array(
                    'label' => 'License',
                    'data' => $subject->getLicense() ? $subject->getLicense() :  $license,
                    'help' => 'La license sera automatiquement généré après la soumission de la fiche.'
                ))
                ->add('color', 'sonata_type_model', array(
                    'label' => 'Couleur'
                ))
                ->add('newTag', null, array(
                    'label' => 'Nouveau'
                ))
                ->add('selected', null, array(
                    'label' => 'Mise en avant par défaut'
                ))
            ->end()
            ->with('Option de publication', array('class' => 'col-md-4'))
                ->add('created', 'sonata_type_date_picker', array(
                    'label' => 'Date de création',
                    'attr' => array(
                        'readonly' => true,
                    ),
                    'format'=>'dd/MM/yyyy HH:mm:ss',
                    'datepicker_use_button' => false,
                    'required' => false
                ))
                ->add('updated', 'sonata_type_date_picker', array(
                    'label' => 'Date mise à jour',
                    'attr' => array(
                        'readonly' => true,
                    ),
                    'format'=>'dd/MM/yyyy HH:mm:ss',
                    'datepicker_use_button' => false,
                    'required' => false
                ))
                ->add('startDate', 'sonata_type_datetime_picker', array(
                    'label' => 'Date de début de publication',
                    'format' => 'dd/MM/yyyy HH:mm:ss',
                ))
                ->add('endDate', 'sonata_type_datetime_picker', array(
                    'label' => 'Date de fin de publication',
                    'format' => 'dd/MM/yyyy HH:mm:ss',
                ))
                ->add('published', null, [
                        'label' => 'Publié',
                        'required' => false,
                    ]
                )
            ->end();
            $this->addRedirectionBlock($formMapper);

        $builder = $formMapper->getFormBuilder();

        $formModifier = function (FormInterface $form, $subModel = null) {
//                $subject = $this->getSubject();
//                        if($subject->getSubmodel()){
//            $price = $subject->getSubmodel()->getPublicPrice();
//            $promoPrice =  $subject->getSubmodel()->getPromoPrice();
//        }
//                $form->add('price', null, array(
//                'data' => $subject->getPrice() ? $subject->getPrice() : $price
//
//            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                // this would be your entity, i.e. SportMeetup
                $data = $event->getData();
                $formModifier($event->getForm(), $data);
            }
        );
        $builder->get('subModel')->addEventListener(
                    FormEvents::POST_SUBMIT,
                    function (FormEvent $event) use ($formModifier) {
                        // It's important here to fetch $event->getForm()->getData(), as
                        // $event->getData() will get you the client data (that is, the ID)
                        $subModel = $event->getForm()->getData();

                        // since we've added the listener to the child, we'll have to pass on
                        // the parent to the callback functions!
                        $formModifier($event->getForm()->getParent(), $subModel);

                    }
                );

    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return '@HondaVehicles/CRUD/edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($object)
    {
        if (empty($object->getPrice())) {
            $object->setPrice($object->getSubModel()->getPublicPrice());
        }
        if (empty($object->getPromoPrice())) {
            $object->setPromoPrice($object->getSubModel()->getPromoPrice());
        }
        if (empty($object->getLicense())) {
            $object->setLicense($object->getSubModel()->getLicense());
        }
    }

    public function preUpdate($object)
    {
        if (empty($object->getPrice())) {
            $object->setPrice($object->getSubModel()->getPublicPrice());
        }
        if (empty($object->getPromoPrice())) {
            $object->setPromoPrice($object->getSubModel()->getPromoPrice());
        }
        if (empty($object->getLicense())) {
            $object->setLicense($object->getSubModel()->getLicense());
        }
    }
}
