<?php

namespace Honda\Bundle\VehiclesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class LayerAdmin extends AbstractAdmin
{
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->add('priceText')
            ->add('promoText')
            ->add('stockText')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('priceText', null, array(
                'label' => 'Texte du layer (À partie de) (1)'
            ))
            ->add('promoText', null, array(
                'label' => 'Texte du layer (Promotion) (2)'
            ))
            ->add('stockText', null, array(
                'label' => 'Texte du layer (En stock) (3)'
            ))
            ->add('garantie', null, array(
                'label' => 'Texte du layer ( Extension de garantie) (4) '
            ))
            ->add('fileTarifs', 'sonata_type_model_list', [
                    'label' => 'Fichier Tarifs',
                    'by_reference' => false,
                    'required' => true,
                        ], [
                    'link_parameters' => [
                        'context' => 'file',
                    ]
                ])
        ;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('list')
            ->remove('delete')
            ->remove('create')
        ;
    }

    /**
     * @param array $actions
     * @return array
     */
    protected function configureBatchActions($actions)
    {

        unset($actions['delete']);

        return $actions;
    }

}
