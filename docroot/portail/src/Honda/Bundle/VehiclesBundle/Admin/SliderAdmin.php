<?php

namespace Honda\Bundle\VehiclesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\CoreBundle\Validator\ErrorElement;

class SliderAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('image', 'string', array('template' => '@SonataMedia/MediaAdmin/list_image.html.twig'))
                ->add('text')
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('image', 'sonata_type_model_list', [
                    'required' => true,
                    'label' => 'Media',
                    'by_reference' => true,
                        ], [
                    'link_parameters' => [
                        'context' => 'slider',
                    ]
                ])
               /*->add('linkExternal', null, array (
                    'label' => 'Lien Externe'
                ))*/
        ;
    }
    
    protected function configureBatchActions($actions)
    {

        unset($actions['delete']);

        return $actions;
    }
    
}
