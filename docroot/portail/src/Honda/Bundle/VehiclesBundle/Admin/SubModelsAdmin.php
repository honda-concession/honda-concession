<?php

namespace Honda\Bundle\VehiclesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\UnitOfWork;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;
use Honda\Bundle\CommonBundle\Admin\Traits\RedirectionAdminTrait;
use Honda\Bundle\VehiclesBundle\Entity\Property;
use Honda\Bundle\VehiclesBundle\Entity\PropertyValue;
use Honda\Bundle\VehiclesBundle\Entity\SubModels;
use Honda\Bundle\VehiclesBundle\Builder\SubModelContractor;

/**
 * Class SubModelsAdmin
 * @package Honda\Bundle\VehiclesBundle\Admin
 */
class SubModelsAdmin extends AbstractAdmin
{

    use ActivationAdminTrait,
        RedirectionAdminTrait;

    private $propertyValues;
    public $last_position = 0;
    private $positionService;
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, ['label' => 'Sous modèle']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('position', 'actions', array(
                    'label' => 'Position',
                    'actions' => array(
                        'move' => array('template' => 'PixSortableBehaviorBundle:Default:_sort_drag_drop.html.twig'),
                )))
                ->add('name', null, array(
                    'label' => 'Sous modèle'
                ))
                ->add('model', null, array(
                    'label' => 'Modèle',
                    'sortable' => 'model.name',
                ))
                ->add('publicPrice', null, array(
                    'label' => 'Prix',
                ))
                ->add('promoPrice', null, array(
                    'label' => 'Prix promo'
                ))
                ->add('license', null, array(
                    'label' => 'License'
                ))
                ->add('active', null, ['editable' => true])
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();
        $subModelId = $subject->getId();

        $formMapper
                ->with('Sous Modèle', ['class' => 'col-md-8'])
                    ->add('name', null, array(
                        'label' => 'Nom'
                    ))
                    ->add('model', null, array(
                        'label' => 'Modèle',
                        'required' => true
                    ))
                    ->add('ref', null, array(
                        'label' => 'Référence'
                    ))
                    ->add('publicPrice', null, array(
                        'label' => 'Prix',
                        'required' => true
                    ))
                    ->add('promoPrice', null, array(
                        'label' => 'Prix promo'
                    ))
                    ->add('aPartirDe', null, array(
                        'label' => 'Extension de garantie à partir de',
                        'required' => false,
                    ))
                    ->add('license', null, array(
                        'label' => 'Licence'
                    ))
                    ->add('description', 'ckeditor', array(
                        'label' => 'Texte de présentation'
                    ))
                ->end()
                ->with('Meta', array('class' => 'col-md-3'))
                ->add('created', 'sonata_type_datetime_picker', array(
                    'label' => 'Date de publication',
                    'format' => 'dd/MM/yyyy HH:mm:ss',
                ))
                ->add('updated', 'sonata_type_datetime_picker', array(
                    'label' => 'Date mise à jour',
                    'format' => 'dd/MM/yyyy HH:mm:ss',
                ))
                ->add('startDate', 'sonata_type_datetime_picker', array(
                    'label' => 'Date de début de publication',
                    'format' => 'dd/MM/yyyy HH:mm:ss',
                ))
                ->add('endDate', 'sonata_type_datetime_picker', array(
                    'label' => 'Date de fin de publication',
                    'format' => 'dd/MM/yyyy HH:mm:ss',
                ))
                ->add('active', null, array(
                    'label' => 'Actif'
                ))
                ->end();
            $this->addRedirectionBlock($formMapper);
            $formMapper
                ->with('Bouton 1', array('class' => 'col-md-8'))
                    ->add('ficheTechnique', 'sonata_type_model_list', [
                        'required' => false,
                        'label' => 'Fichier',
                        'by_reference' => false,
                            ], [
                        'link_parameters' => [
                            'context' => 'file',
                        ]
                    ])
                    ->add('ficheTechniqueLink')
                    ->add('ficheTechniqueExternalLink')
                    ->add('ctaFicheTechnique', null, array(
                        'label' => 'Intitulé du bouton'
                    ))
                ->end()
                ->with('Bouton 2', array('class' => 'col-md-8'))
                ->add('otheDocs', 'sonata_type_model_list', [
                    'required' => false,
                    'label' => 'Fichier',
                    'by_reference' => false,
                        ], [
                    'link_parameters' => [
                        'context' => 'file',
                    ]
                ])
                ->add('otherDocsLink')
                ->add('otherDocsExternalLink')
                ->add('ctaOtherDocs', null, array(
                    'label' => 'Intitulé du bouton'
                ))
                ->end()
                ->with('Caractéristiques principales', array('class' => 'col-md-8'))
                    ->add('cylinder', null, [
                        'label' => 'Cylindrée (cm³)'
                    ])
        ;
        $builder = $formMapper->getFormBuilder();
        $formModifier = function (FormInterface $form, $subModel = null) {};
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                // this would be your entity, i.e. SportMeetup
                $data = $event->getData();
                $formModifier($event->getForm(), $data);
            }
        );
        $em = $this->getModelManager()->getEntityManager($this->getClass());
        $properties = $em->getRepository(Property::class)->findBy([], ['position' => 'ASC']);
        $subModel = null;
        if ($subModelId) {
            $subModel = $em->getRepository(SubModels::class)->find($subModelId);
        }
        foreach ($properties as $property) {
            $propertyValue = $em->getRepository(PropertyValue::class)->findOneBy([
                'subModel' => $subModelId,
                'property' => $property
            ]);
            if (!$propertyValue) {
                $propertyValue = new PropertyValue();
            }
            $formMapper->add($property->getFieldName(), null, [
                'label' => $property->getName(),
                'mapped' => false,
                'attr' => [
                    'value' => $propertyValue->getValue()
                ]
            ]);
            $builder->get($property->getFieldName())->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($formModifier, $propertyValue, $property, $subModel, $em) {
                    // It's important here to fetch $event->getForm()->getData(), as
                    // $event->getData() will get you the client data (that is, the ID)
                    $value = $event->getForm()->getData();
                    $propertyValue
                        ->setValue($value)
                        ->setProperty($property);

                    // since we've added the listener to the child, we'll have to pass on
                    // the parent to the callback functions!
                    $formModifier($event->getForm()->getParent(), $subModel);
                }
            );
            $this->propertyValues[] = $propertyValue;
        }
        $formMapper->end();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    /**
     * @inheritdoc
     */
    public function prePersist($object)
    {
        if (is_array($this->propertyValues)) {
            $object->setPropertyValues($this->propertyValues);
        }
    }

    public function preUpdate($object)
    {
        if (is_array($this->propertyValues)) {
            $object->setPropertyValues($this->propertyValues);
        }
        $em = $this->getModelManager()->getEntityManager($this->getClass());
        $original = $em->getUnitOfWork()->getOriginalEntityData($object);

        foreach ($object->getVehicles() as $vehicle) {
            if ($vehicle->getPrice() == $original['publicPrice'] || $vehicle->getPrice() == null) {
                $vehicle->setPrice($object->getPublicPrice());
            }
            if ($vehicle->getPromoPrice() == $original['promoPrice'] || $vehicle->getPromoPrice() == null) {
                $vehicle->setPromoPrice($object->getPromoPrice());
            }
            if ($vehicle->getLicense() == $original['license'] || $vehicle->getLicense() == null)  {
                $vehicle->setLicense($object->getLicense());
            }
        }
    }

    public function postUpdate($object)
    {
        $sql = "update sub_models sm inner join (
            select id, @rownum := @rownum+1 as rank from sub_models, (select @rownum := 0) r order by position asc) v on sm.id=v.id
        set sm.position = rank;";
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        $stmt = $em->getConnection()->query($sql);
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        if ($this->isGranted('LIST')) {
            $menu->addChild('Listes véhicules', [
                'uri' => $admin->generateUrl('honda_vehicles.admin.vehicles.list', ['id' => $id])
            ]);
        }
    }
}
