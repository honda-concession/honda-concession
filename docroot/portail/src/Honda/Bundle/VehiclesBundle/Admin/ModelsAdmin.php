<?php

namespace Honda\Bundle\VehiclesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;

use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;
use Honda\Bundle\CommonBundle\Admin\Traits\RedirectionAdminTrait;

class ModelsAdmin extends AbstractAdmin
{

    use ActivationAdminTrait,
        RedirectionAdminTrait;

    public $last_position = 0;
    private $positionService;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array(
                'label' => 'Nom'
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('position', 'actions', array(
                'label' => 'Position',
                'actions' => array(
                    'move' => array('template' => 'PixSortableBehaviorBundle:Default:_sort_drag_drop.html.twig'),
            )))
            ->add('name', null, array(
                'label' => 'Modèle'
            ))
            ->add('category', null, array(
                'label' => 'Catégorie'
            ))
            ->add('published', null, ['editable' => true])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Modèle',  ['class' => 'col-md-9'])
                ->add('name', null, array(
                    'label' => 'Nom'
                ))
                ->add('category', null, array(
                    'label' => 'Catégorie'
                ))
                ->add('sliderPrincipal', 'sonata_type_collection', array(
                        'required' => true,
                        'label' => 'Slider Principal',
                        'by_reference' => true,
                        'constraints' => new \Symfony\Component\Validator\Constraints\Valid(),
                        'help' => '',
                    ), array(
                        'edit' => 'inline',
                        'sortable' => 'pos',
                        'inline' => 'table',
                        'allow_delete' => true,
                        'delete_empty' => true,
                    )
                )
                ->add('sliderTechnology', 'sonata_type_collection', array(
                        'required' => true,
                        'label' => 'Slider Technologie',
                        'constraints' => new \Symfony\Component\Validator\Constraints\Valid(),
                        'help' => '',
                    ), array(
                        'edit' => 'inline',
                        'sortable' => 'pos',
                        'inline' => 'table',
                        'allow_delete' => true,
                        'delete_empty' => true,
                    )
                )
                ->add('pictos', 'sonata_type_model', [
                    'label' => 'Pictos',
                    'required' => false,
                    'btn_add' => false,
                    'multiple' => true,
                    'by_reference' => false,
                    'sortable'     => true,
                ])
                ->add('titleDescription', null, array(
                    'label' => 'Titre de la présentation'
                ))
                ->add('description', 'ckeditor', array(
                    'label' => 'Bloc de texte'
                ))
                ->add('ctaLabel', null, array(
                    'label' => 'Intitulé du bouton'
                ))
                ->add('link', null, array(
                    'label' => 'Lien'
                ))
                ->add('category', null, array(
                    'label' => 'Category'
                ))
                ->add('additionalText', null, [
                    'label' => 'Texte supplémentaire',
                    'required' => false,
                ])
                ->add('koraId', null, array(
                    'label' => 'Identifiant Kora',
                    'required' => false,
                ))
            ->end()
        ;
        $this->addRedirectionBlock($formMapper);
    }

    /**
     * @inheritdoc
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        foreach ($object->getSliderPrincipal() as $slider) {
            $slider->setModelPrincipal($object);
        }

        foreach ($object->getSliderTechnology() as $slider) {
            $slider->setModelTechnology($object);
        }
    }

    /**
     * @inheritdoc
     */
    public function preUpdate($object)
    {
        parent::preUpdate($object);
        foreach ($object->getSliderPrincipal() as $slider) {
            $slider->setModelPrincipal($object);
        }

        foreach ($object->getSliderTechnology() as $slider) {
            $slider->setModelTechnology($object);
        }
    }

    /**
     * @inheritdoc
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
                ->with('description')
                ->assertNotBlank()
                ->end()
                ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

}
