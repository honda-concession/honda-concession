<?php

namespace Honda\Bundle\VehiclesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\RedirectionTrait;

/**
 * SubModels
 *
 * @ORM\Table(name="sub_models")
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesBundle\Repository\SubModelsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SubModels
{

    use ActivationTrait,
        RedirectionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=255)
     */
    private $ref;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="license", type="string", length=255 , nullable=true)
     */
    private $license;

    /**
     * @var string
     *
     * @ORM\Column(name="public_price", type="string", length=255 , nullable=true)
     */
    private $publicPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_price", type="string", length=255 , nullable=true)
     */
    private $promoPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default":"0"})
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity="Models", inversedBy="subModels")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $model;

    /**
     * @ORM\OneToMany(targetEntity="Vehicles", mappedBy="subModel")
     *
     */
    private $vehicles;

    /**
     * @var integer
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", length=255)
    */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="ctafichetechnique", type="string", length=255 , nullable=true)
     */
    private $ctaFicheTechnique;

    /**
     * @var string
     *
     * @ORM\Column(name="fiche_technique_link", type="string", length=255 , nullable=true)
     */
    private $ficheTechniqueLink;

    /**
     * @ORM\Column(name="fiche_technique_external_link", type="boolean", options={"default": 0})
     */
    private $ficheTechniqueExternalLink = false;

    /**
     * @var string
     *
     * @ORM\Column(name="ctaotherdocs", type="string", length=255 , nullable=true)
     */
    private $ctaOtherDocs;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL",nullable=true)
     */
    private $ficheTechnique;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL",nullable=true)
     */
    private $otheDocs;

    /**
     * @var string
     *
     * @ORM\Column(name="other_docs_link", type="string", length=255 , nullable=true)
     */
    private $otherDocsLink;

    /**
     * @ORM\Column(name="other_docs_external_link", type="boolean", options={"default": 0})
     */
    private $otherDocsExternalLink = false;

    /**
     * @var string
     *
     * @ORM\Column(name="cylinder", type="string", length=255 , nullable=true)
     */
    private $cylinder;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug",length=255, type="string", unique=true)
     */
    private $slug;

    /**
     * Properties associated with sub model
     *
     * @var Array Collection
     * @ORM\OneToMany(targetEntity="PropertyValue", mappedBy="subModel", cascade={"persist"})
     */
    private $propertyValues;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    protected $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    protected $endDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    protected $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @var string
     *
     * @ORM\Column(name="apartir_de", type="string", length=255, nullable=true)
     */
    private $aPartirDe;

    public function __construct()
    {
        $this->vehicles = new ArrayCollection();
        $this->properties = new ArrayCollection();
        $this->created = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return SubModels
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set $ef.
     *
     * @param string $ref
     *
     * @return Models
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref.
     *
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set active.
     *
     * @param int $active
     *
     * @return SubModels
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    public function isActive()
    {
        return $this->active === true;
    }

    /**
     * Set license.
     *
     * @param string $license
     *
     * @return SubModels
     */
    public function setLicense($license)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license.
     *
     * @return string
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set publicPrice.
     *
     * @param string $publicPrice
     *
     * @return SubModels
     */
    public function setPublicPrice($publicPrice)
    {
        $this->publicPrice = $publicPrice;

        return $this;
    }

    /**
     * Get publicPrice.
     *
     * @return string
     */
    public function getPublicPrice()
    {
        return $this->publicPrice;
    }

    /**
     * Set promoPrice.
     *
     * @param string $promoPrice
     *
     * @return SubModels
     */
    public function setPromoPrice($promoPrice)
    {
        $this->promoPrice = $promoPrice;

        return $this;
    }

    /**
     * Get promoPrice.
     *
     * @return string
     */
    public function getPromoPrice()
    {
        return $this->promoPrice;
    }

    /**
     * Set model.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Models|null $model
     *
     * @return SubModels
     */
    public function setModel(\Honda\Bundle\VehiclesBundle\Entity\Models $model = null)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model.
     *
     * @return \Honda\Bundle\VehiclesBundle\Entity\Models|null
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Add vehicle.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Vehicles $vehicle
     *
     * @return SubModels
     */
    public function addVehicle(\Honda\Bundle\VehiclesBundle\Entity\Vehicles $vehicle)
    {
        $this->vehicles[] = $vehicle;

        return $this;
    }

    /**
     * Remove vehicle.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Vehicles $vehicle
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVehicle(\Honda\Bundle\VehiclesBundle\Entity\Vehicles $vehicle)
    {
        return $this->vehicles->removeElement($vehicle);
    }

    /**
     * Get vehicles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return SubModels
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Gets the value of position.
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the value of position.
     *
     * @param integer $position the position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get collection
     *
     * @return  Array
     */
    public function getPropertyValues()
    {
        return $this->propertyValues;
    }

    /**
     * Set collection
     *
     * @param  Array  $propertyValues  Collection
     *
     * @return  self
     */
    public function setPropertyValues(Array $propertyValues)
    {
        foreach ($propertyValues as $propertyValue) {
            $propertyValue->setSubModel($this);
        }
        $this->propertyValues = $propertyValues;

        return $this;
    }

    /**
     * Set ctaFicheTechnique.
     *
     * @param string|null $ctaFicheTechnique
     *
     * @return SubModels
     */
    public function setCtaFicheTechnique($ctaFicheTechnique = null)
    {
        $this->ctaFicheTechnique = $ctaFicheTechnique;

        return $this;
    }

    /**
     * Get ctaFicheTechnique.
     *
     * @return string|null
     */
    public function getCtaFicheTechnique()
    {
        return $this->ctaFicheTechnique;
    }

    /**
     * Set ctaOtherDocs.
     *
     * @param string|null $ctaOtherDocs
     *
     * @return SubModels
     */
    public function setCtaOtherDocs($ctaOtherDocs = null)
    {
        $this->ctaOtherDocs = $ctaOtherDocs;

        return $this;
    }

    /**
     * Get ctaOtherDocs.
     *
     * @return string|null
     */
    public function getCtaOtherDocs()
    {
        return $this->ctaOtherDocs;
    }

    /**
     * Set ficheTechnique.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $ficheTechnique
     *
     * @return SubModels
     */
    public function setFicheTechnique(\Application\Sonata\MediaBundle\Entity\Media $ficheTechnique = null)
    {
        $this->ficheTechnique = $ficheTechnique;

        return $this;
    }

    /**
     * Get ficheTechnique.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getFicheTechnique()
    {
        return $this->ficheTechnique;
    }

    /**
     * Set otheDocs.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $otheDocs
     *
     * @return SubModels
     */
    public function setOtheDocs(\Application\Sonata\MediaBundle\Entity\Media $otheDocs = null)
    {
        $this->otheDocs = $otheDocs;

        return $this;
    }

    /**
     * Get otheDocs.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getOtheDocs()
    {
        return $this->otheDocs;
    }

    /**
     * Set cylinder.
     *
     * @param string $cylinder
     *
     * @return SubModels
     */
    public function setCylinder($cylinder)
    {
        $this->cylinder = $cylinder;

        return $this;
    }

    /**
     * Get cylinder.
     *
     * @return string
     */
    public function getCylinder()
    {
        return $this->cylinder;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return SubModels
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return SubModels
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return News
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return News
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdateBase()
    {
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
    }

    /**
     * @return string
     */
    public function getAPartirDe()
    {
        return $this->aPartirDe;
    }

    /**
     * @param string $aPartirDe
     * @return Layer
     */
    public function setAPartirDe($aPartirDe)
    {
        $this->aPartirDe = $aPartirDe;

        return $this;
    }

    /**
     * @return string
     */
    public function getOtherDocsLink()
    {
        return $this->otherDocsLink;
    }

    /**
     * @param string $otherDocsLink
     */
    public function setOtherDocsLink($otherDocsLink)
    {
        $this->otherDocsLink = $otherDocsLink;
    }

    /**
     * @return mixed
     */
    public function getOtherDocsExternalLink()
    {
        return $this->otherDocsExternalLink;
    }

    /**
     * @param mixed $otherDocsExternalLink
     * @return SubModels
     */
    public function setOtherDocsExternalLink($otherDocsExternalLink)
    {
        $this->otherDocsExternalLink = $otherDocsExternalLink;

        return $this;
    }

    /**
     * @return string
     */
    public function getFicheTechniqueLink()
    {
        return $this->ficheTechniqueLink;
    }

    /**
     * @param string $ficheTechniqueLink
     * @return SubModels
     */
    public function setFicheTechniqueLink($ficheTechniqueLink)
    {
        $this->ficheTechniqueLink = $ficheTechniqueLink;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFicheTechniqueExternalLink()
    {
        return $this->ficheTechniqueExternalLink;
    }

    /**
     * @param mixed $ficheTechniqueExternalLink
     * @return SubModels
     */
    public function setFicheTechniqueExternalLink($ficheTechniqueExternalLink)
    {
        $this->ficheTechniqueExternalLink = $ficheTechniqueExternalLink;

        return $this;
    }
}
