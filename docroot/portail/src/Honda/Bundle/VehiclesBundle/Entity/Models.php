<?php

namespace Honda\Bundle\VehiclesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\RedirectionTrait;

/**
 * Models
 *
 * @ORM\Table(name="models")
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesBundle\Repository\ModelsRepository")
 */
class Models
{

    use ActivationTrait;
    use RedirectionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="engine", type="string", length=255 , nullable=true)
     */
    private $engine;

    /**
     * @var string
     *
     * @ORM\Column(name="title_description", type="string" , nullable=true)
     */
    private $titleDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text" , nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="cta", type="string", length=255 , nullable=true)
     */
    private $ctaLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255 , nullable=true)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="models")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="SubModels", mappedBy="model")
     *
     */
    private $subModels;

    /**
     * @ORM\OneToMany(targetEntity="Slider" , mappedBy="modelPrincipal",cascade={"persist"}, fetch="LAZY", orphanRemoval=true)
     *
     */
    private $sliderPrincipal;

    /**
     * @ORM\OneToMany(targetEntity="Slider" , mappedBy="modelTechnology", cascade={"persist"}, fetch="LAZY" , orphanRemoval=true)
     *
     */
    private $sliderTechnology;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Pictos" , cascade={"persist"})
     *
     */
    private $pictos;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug",length=128, type="string", unique=true)
     */
    private $slug;

    /**
     * @var integer
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", length=255)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="kora_id", type="string" , nullable=true)
     */
    private $koraId;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_text", type="string" , nullable=true)
     */
    private $additionalText;

    public function __construct()
    {
        $this->subModels = new ArrayCollection();
        $this->sliderPrincipal = new ArrayCollection();
        $this->sliderTechnology = new ArrayCollection();
        $this->pictos = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Models
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set engine.
     *
     * @param string $engine
     *
     * @return Models
     */
    public function setEngine($engine)
    {
        $this->engine = $engine;

        return $this;
    }

    /**
     * Get engine.
     *
     * @return string
     */
    public function getEngine()
    {
        return $this->engine;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Models
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return Models
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set category.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Category|null $category
     *
     * @return Models
     */
    public function setCategory(\Honda\Bundle\VehiclesBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \Honda\Bundle\VehiclesBundle\Entity\Category|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add subModel.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\SubModels $subModel
     *
     * @return Models
     */
    public function addSubModel(\Honda\Bundle\VehiclesBundle\Entity\SubModels $subModel)
    {
        $this->subModels[] = $subModel;

        return $this;
    }

    /**
     * Remove subModel.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\SubModels $subModel
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSubModel(\Honda\Bundle\VehiclesBundle\Entity\SubModels $subModel)
    {
        return $this->subModels->removeElement($subModel);
    }

    /**
     * Get subModels.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubModels()
    {
        return $this->subModels;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

    /**
     * Add sliderPrincipal.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Slider $sliderPrincipal
     *
     * @return Models
     */
    public function addSliderPrincipal(\Honda\Bundle\VehiclesBundle\Entity\Slider $sliderPrincipal)
    {
        $this->sliderPrincipal[] = $sliderPrincipal;
        $sliderPrincipal->setModelPrincipal($this);


        return $this;
    }

    /**
     * Remove sliderPrincipal.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Slider $sliderPrincipal
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSliderPrincipal(\Honda\Bundle\VehiclesBundle\Entity\Slider $sliderPrincipal)
    {
        return $this->sliderPrincipal->removeElement($sliderPrincipal);
    }

    /**
     * Get sliderPrincipal.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSliderPrincipal()
    {
        return $this->sliderPrincipal;
    }

    /**
     * Set sliderPrincipal.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Slider|null $sliderPrincipal
     *
     * @return Models
     */
    public function setSliderPrincipal(\Honda\Bundle\VehiclesBundle\Entity\Slider $sliderPrincipal = null)
    {
        $this->sliderPrincipal = $sliderPrincipal;

        return $this;
    }

    /**
     * Add sliderTechnology.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Slider $sliderTechnology
     *
     * @return Models
     */
    public function addSliderTechnology(\Honda\Bundle\VehiclesBundle\Entity\Slider $sliderTechnology)
    {
        $this->sliderTechnology[] = $sliderTechnology;
        $sliderTechnology->setModelTechnology($this);

        return $this;
    }

    /**
     * Remove sliderTechnology.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Slider $sliderTechnology
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSliderTechnology(\Honda\Bundle\VehiclesBundle\Entity\Slider $sliderTechnology)
    {
        return $this->sliderTechnology->removeElement($sliderTechnology);
    }

    /**
     * Get sliderTechnology.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSliderTechnology()
    {
        return $this->sliderTechnology;
    }

    /**
     * Set sliderTechnology.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Slider|null $sliderTechnology
     *
     * @return Models
     */
    public function setSliderTechnology(\Honda\Bundle\VehiclesBundle\Entity\Slider $sliderTechnology = null)
    {
        $this->sliderTechnology = $sliderTechnology;

        return $this;
    }


    /**
     * Set pictos.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Pictos|null $pictos
     *
     * @return Models
     */
    public function setPictos(\Honda\Bundle\VehiclesBundle\Entity\Pictos $pictos = null)
    {
        $this->pictos = $pictos;

        return $this;
    }

    /**
     * Get pictos.
     *
     * @return \Honda\Bundle\VehiclesBundle\Entity\Pictos|null
     */
    public function getPictos()
    {
        return $this->pictos;
    }

    /**
     * Set ctaLabel.
     *
     * @param string|null $ctaLabel
     *
     * @return Models
     */
    public function setCtaLabel($ctaLabel = null)
    {
        $this->ctaLabel = $ctaLabel;

        return $this;
    }

    /**
     * Get ctaLabel.
     *
     * @return string|null
     */
    public function getCtaLabel()
    {
        return $this->ctaLabel;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Models
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Add picto.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Pictos $picto
     *
     * @return Models
     */
    public function addPicto(\Honda\Bundle\VehiclesBundle\Entity\Pictos $picto)
    {
        $this->pictos[] = $picto;

        return $this;
    }

    /**
     * Remove picto.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Pictos $picto
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePicto(\Honda\Bundle\VehiclesBundle\Entity\Pictos $picto)
    {
        return $this->pictos->removeElement($picto);
    }

    /**
     * Gets the value of position.
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the value of position.
     *
     * @param integer $position the position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Set koraId.
     *
     * @param string|null $koraId
     *
     * @return Models
     */
    public function setKoraId($koraId = null)
    {
        $this->koraId = $koraId;

        return $this;
    }

    /**
     * Get koraId.
     *
     * @return string|null
     */
    public function getKoraId()
    {
        return $this->koraId;
    }

    /**
     * @return string
     */
    public function getTitleDescription()
    {
        return $this->titleDescription;
    }

    /**
     * @param $titleDescription
     * @return $this
     */
    public function setTitleDescription($titleDescription)
    {
        $this->titleDescription = $titleDescription;

        return $this;
    }

    /**
     * Get the value of additionalText
     *
     * @return  string
     */
    public function getAdditionalText()
    {
        return $this->additionalText;
    }

    /**
     * Set the value of additionalText
     *
     * @param  string  $additionalText
     *
     * @return  self
     */
    public function setAdditionalText(string $additionalText = null)
    {
        $this->additionalText = $additionalText;

        return $this;
    }
}
