<?php

namespace Honda\Bundle\VehiclesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

use Honda\Bundle\CommonBundle\Entity\Traits\RedirectionTrait;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesBundle\Repository\CategoryRepository")
 */
class Category
{

    use RedirectionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $image;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $fileTarifs;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="label_cta", type="string", length=255, nullable=true)
     */
    private $labelCta;

    /**
     * @ORM\OneToMany(targetEntity="Models", mappedBy="category")
     *
     */
    private $models;

    /**
     * @var integer
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", length=255)
     */
    private $position;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug",length=128, type="string", unique=true)
     */
    private $slug;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\Honda\Bundle\DistributorBundle\Entity\SubGroup", cascade={"persist"})
     */
    private $subGroups;

    public function __construct()
    {
        $this->models = new ArrayCollection();
        $this->subGroups = new ArrayCollection();
    }

    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set file.
     *
     * @param string $file
     *
     * @return Category
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set labelCta.
     *
     * @param string $labelCta
     *
     * @return Category
     */
    public function setLabelCta($labelCta)
    {
        $this->labelCta = $labelCta;

        return $this;
    }

    /**
     * Get labelCta.
     *
     * @return string
     */
    public function getLabelCta()
    {
        return $this->labelCta;
    }

    /**
     * @return Collection|Models[]
     */
    public function getModels()
    {
        return $this->models;
    }

    /**
     * Set models
     *
     * @param ArrayCollection $models
     *
     * @return Category
     */
    public function setModels(ArrayCollection $models)
    {
        $this->models = $models;

        return $this;
    }

    /**
     * Add models
     *
     * @param Models $models
     *
     * @return Category
     */
    public function addModel(Models $models)
    {
        $this->models[] = $models;
        $models->setCategory($this);

        return $this;
    }

    /**
     * Remove models
     *
     * @param Models $models
     *
     * @return Category
     */
    public function removeModel(Models $models)
    {
        $this->models->removeElement($models);
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }


    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Category
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set fileTarifs.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $fileTarifs
     *
     * @return Category
     */
    public function setFileTarifs(\Application\Sonata\MediaBundle\Entity\Media $fileTarifs = null)
    {
        $this->fileTarifs = $fileTarifs;

        return $this;
    }

    /**
     * Get fileTarifs.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getFileTarifs()
    {
        return $this->fileTarifs;
    }

    /**
     * Gets the value of position.
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the value of position.
     *
     * @param integer $position the position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Set subGroups.
     *
     * @param \Doctrine\Common\Collections\Collection $subGroups
     *
     * @return self
     */
    public function setSubGroups($subGroups)
    {
        $this->subGroups = $subGroups;

        return $this;
    }

    /**
     * Get subGroups.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubGroups()
    {
        return $this->subGroups;
    }
}
