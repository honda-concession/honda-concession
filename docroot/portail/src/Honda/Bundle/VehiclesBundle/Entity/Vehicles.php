<?php

namespace Honda\Bundle\VehiclesBundle\Entity;

use Honda\Bundle\CommonBundle\Model\BaseModel;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\RedirectionTrait;

/**
 * Vehicles
 *
 * @ORM\Table(name="vehicles")
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesBundle\Repository\VehiclesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Vehicles extends BaseModel
{

    use ActivationTrait;
    use RedirectionTrait;

    /**
     * @var in
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255 , nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_price", type="string", length=255 , nullable=true)
     */
    private $promoPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="selected", type="boolean", nullable=true, options={"default":"0"})
     */
    private $selected;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="new_tag", type="boolean", nullable=false, options={"default":"0"})
     */
    private $newTag;

    /**
     * @ORM\ManyToOne(targetEntity="Color",fetch="EAGER")
     *
     */
    private $color;
    /**
     * @var string
     *
     * @ORM\Column(name="license", type="string" , nullable=true)
     */
    private $license;

    /**
     * @ORM\ManyToOne(targetEntity="SubModels", inversedBy="vehicles")
     * @ORM\JoinColumn(name="submodel_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $subModel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    protected $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    protected $endDate;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug",length=255, type="string", unique=true)
     */
    private $slug;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Vehicles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price.
     *
     * @param string $price
     *
     * @return Vehicles
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set promoPrice.
     *
     * @param string $promoPrice
     *
     * @return Vehicles
     */
    public function setPromoPrice($promoPrice)
    {
        $this->promoPrice = $promoPrice;

        return $this;
    }

    /**
     * Get promoPrice.
     *
     * @return string
     */
    public function getPromoPrice()
    {
        return $this->promoPrice;
    }

    /**
     * Set selected.
     *
     * @param int $selected
     *
     * @return Vehicles
     */
    public function setSelected($selected)
    {
        $this->selected = $selected;

        return $this;
    }

    /**
     * Get selected.
     *
     * @return int
     */
    public function getSelected()
    {
        return $this->selected;
    }

    /**
     * Set newTag.
     *
     * @param int $newTag
     *
     * @return Vehicles
     */
    public function setNewTag($newTag)
    {
        $this->newTag = $newTag;

        return $this;
    }

    /**
     * Get newTag.
     *
     * @return int
     */
    public function getNewTag()
    {
        return $this->newTag;
    }

    /**
     * Set license.
     *
     * @param int $license
     *
     * @return Vehicles
     */
    public function setLicense($license)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license.
     *
     * @return int
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set subModel.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\SubModels|null $subModel
     *
     * @return Vehicles
     */
    public function setSubModel(\Honda\Bundle\VehiclesBundle\Entity\SubModels $subModel = null)
    {
        $this->subModel = $subModel;

        return $this;
    }

    /**
     * Get subModel.
     *
     * @return \Honda\Bundle\VehiclesBundle\Entity\SubModels|null
     */
    public function getSubModel()
    {
        return $this->subModel;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

    /**
     * Set color.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Color|null $color
     *
     * @return Vehicles
     */
    public function setColor(\Honda\Bundle\VehiclesBundle\Entity\Color $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return \Honda\Bundle\VehiclesBundle\Entity\Color|null
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Vehicles
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Vehicles
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return Vehicles
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return Slider
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }
}
