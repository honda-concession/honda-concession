<?php

namespace Honda\Bundle\VehiclesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;

/**
 * Color
 *
 * @ORM\Table(name="color_vn")
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesBundle\Repository\ColorRepository")
 */
class Color
{

    use ActivationTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    /**
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(name="kora_color_name", type="string", length=255, nullable=true)
     */
    private $koraColorName;

    /**
     * @ORM\Column(name="kora_color_code", type="string", length=255, nullable=true)
     */
    private $koraColorCode;

    /**
     * @ORM\Column(name="kora_color_id", type="string", length=255, nullable=true)
     */
    private $koraColorId;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $logo;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set color.
     *
     * @param string $color
     *
     * @return Color
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set logo.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $logo
     *
     * @return Distributor
     */
    public function setLogo(\Application\Sonata\MediaBundle\Entity\Media $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getColor();
        } else {
            return '';
        }
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return Color
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getKoraColorCode()
    {
        return $this->koraColorCode;
    }

    /**
     * @param mixed $koraColorCode
     * @return Color
     */
    public function setKoraColorCode($koraColorCode)
    {
        $this->koraColorCode = $koraColorCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getKoraColorId()
    {
        return $this->koraColorId;
    }

    /**
     * @param mixed $koraColorId
     * @return Color
     */
    public function setKoraColorId($koraColorId)
    {
        $this->koraColorId = $koraColorId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getKoraColorName()
    {
        return $this->koraColorName;
    }

    /**
     * @param mixed $koraColorName
     * @return Color
     */
    public function setKoraColorName($koraColorName)
    {
        $this->koraColorName = $koraColorName;

        return $this;
    }
}
