<?php

namespace Honda\Bundle\VehiclesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Layer
 *
 * @ORM\Table(name="layer", indexes={@ORM\Index(name="slug_idx", columns={"slug"})})
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesBundle\Repository\LayerRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Layer
{

    const SLUG_ID = 'generic-text-for-promo';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="price_text", type="text")
     */
    private $priceText;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_text", type="text")
     */
    private $promoText;

    /**
     * @var string
     *
     * @ORM\Column(name="stock_text", type="text")
     */
    private $stockText;

    /**
     * @var string
     *
     * @ORM\Column(name="garantie", type="text")
     */
    private $garantie;
    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $fileTarifs;

    /**
     * @ORM\Column(unique=true)
     */
    private $slug = self::SLUG_ID;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set priceText.
     *
     * @param string $priceText
     *
     * @return Layer
     */
    public function setPriceText($priceText)
    {
        $this->priceText = $priceText;

        return $this;
    }

    /**
     * Get priceText.
     *
     * @return string
     */
    public function getPriceText()
    {
        return $this->priceText;
    }

    /**
     * Set promoText.
     *
     * @param string $promoText
     *
     * @return Layer
     */
    public function setPromoText($promoText)
    {
        $this->promoText = $promoText;

        return $this;
    }

    /**
     * Get promoText.
     *
     * @return string
     */
    public function getPromoText()
    {
        return $this->promoText;
    }

    /**
     * Set stockText.
     *
     * @param string $stockText
     *
     * @return Layer
     */
    public function setStockText($stockText)
    {
        $this->stockText = $stockText;

        return $this;
    }

    /**
     * Get stockText.
     *
     * @return string
     */
    public function getStockText()
    {
        return $this->stockText;
    }

    /**
     * Set garantie.
     *
     * @param string $garantie
     *
     * @return Layer
     */
    public function setGarantie($garantie)
    {
        $this->garantie = $garantie;

        return $this;
    }

    /**
     * Get garantie.
     *
     * @return string
     */
    public function getGarantie()
    {
        return $this->garantie;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set fileTarifs.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $fileTarifs
     *
     * @return Layer
     */
    public function setFileTarifs(\Application\Sonata\MediaBundle\Entity\Media $fileTarifs = null)
    {
        $this->fileTarifs = $fileTarifs;

        return $this;
    }

    /**
     * Get fileTarifs.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getFileTarifs()
    {
        return $this->fileTarifs;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {

        return 'Textes des layers';

    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        if (!$this->getSlug()) {
            $this->slug = self::SLUG_ID;
        }
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdateBase()
    {
        $this->slug = self::SLUG_ID;
    }
}
