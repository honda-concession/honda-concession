<?php

namespace Honda\Bundle\VehiclesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pictos
 *
 * @ORM\Table(name="pictos_vn")
 * @ORM\Entity(repositoryClass="Honda\Bundle\ServicesBundle\Repository\PictosRepository")
 */
class Pictos
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="layer", type="string", length=255)
     */
    private $layer;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $picto;

    /**
     * Constructor
     */
    public function __construct()
    {
        //$this->models = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Pictos
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set picto.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $picto
     *
     * @return Distributor
     */
    public function setPicto(\Application\Sonata\MediaBundle\Entity\Media $picto = null)
    {
        $this->picto = $picto;

        return $this;
    }

    /**
     * Get picto.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getPicto()
    {
        return $this->picto;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

    /**
     * Set layer.
     *
     * @param string $layer
     *
     * @return Pictos
     */
    public function setLayer($layer)
    {
        $this->layer = $layer;

        return $this;
    }

    /**
     * Get layer.
     *
     * @return string
     */
    public function getLayer()
    {
        return $this->layer;
    }
}
