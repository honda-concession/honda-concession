<?php

namespace Honda\Bundle\VehiclesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\Bundle\SliderBundle\Model\BaseSlider;

/**
 * Slider
 *
 * @ORM\Table(name="slider_models")
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesBundle\Repository\SliderRepository")
 */
class Slider extends BaseSlider
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Models", inversedBy="sliderPrincipal ")
     * @ORM\JoinColumn(name="slider_principal_id", referencedColumnName="id",nullable=true)
     */
    protected $modelPrincipal;
    
    /**
     * @ORM\ManyToOne(targetEntity="Models", inversedBy="sliderTechnology ")
     * @ORM\JoinColumn(name="slider_technology_id", referencedColumnName="id",nullable=true)
     */
    protected $modelTechnology;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modelPrincipal.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Models|null $modelPrincipal
     *
     * @return Slider
     */
    public function setModelPrincipal(\Honda\Bundle\VehiclesBundle\Entity\Models $modelPrincipal = null)
    {
        $this->modelPrincipal = $modelPrincipal;

        return $this;
    }

    /**
     * Get modelPrincipal.
     *
     * @return \Honda\Bundle\VehiclesBundle\Entity\Models|null
     */
    public function getModelPrincipal()
    {
        return $this->modelPrincipal;
    }

    /**
     * Set modelTechnology.
     *
     * @param \Honda\Bundle\VehiclesBundle\Entity\Models|null $modelTechnology
     *
     * @return Slider
     */
    public function setModelTechnology(\Honda\Bundle\VehiclesBundle\Entity\Models $modelTechnology = null)
    {
        $this->modelTechnology = $modelTechnology;

        return $this;
    }

    /**
     * Get modelTechnology.
     *
     * @return \Honda\Bundle\VehiclesBundle\Entity\Models|null
     */
    public function getModelTechnology()
    {
        return $this->modelTechnology;
    }
    
//    /**
//     * Objet to string
//     *
//     * @return string
//     */
//    public function __toString()
//    {
//        if ($this->getId()) {
//            return $this->getName();
//        } else {
//            return '';
//        }
//    }
}
