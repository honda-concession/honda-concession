<?php

namespace Honda\Bundle\VehiclesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropertyValue
 *
 * @ORM\Table(name="property_value")
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesBundle\Repository\PropertyValueRepository")
 */
class PropertyValue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="SubModels", inversedBy="subModels")
     * @ORM\JoinColumn(name="sub_model_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $subModel;

    /**
     * @ORM\ManyToOne(targetEntity="Property", inversedBy="properties")
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $property;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return SubModelPropertyValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get the value of property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set the value of property
     *
     * @return  self
     */
    public function setProperty(Property $property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get the value of subModel
     */
    public function getSubModel()
    {
        return $this->subModel;
    }

    /**
     * Set the value of subModel
     *
     * @return  self
     */
    public function setSubModel(SubModels $subModel)
    {
        $this->subModel = $subModel;

        return $this;
    }
}
