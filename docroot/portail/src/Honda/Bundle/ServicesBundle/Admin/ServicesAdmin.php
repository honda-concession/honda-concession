<?php

namespace Honda\Bundle\ServicesBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Honda\Bundle\CommonBundle\Admin\HomePublishableAdmin;
use Honda\Bundle\ServicesBundle\Entity\Pictos;
use Honda\Bundle\CommonBundle\Admin\Traits\RedirectionAdminTrait;
use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;

/**
 * Class ServicesAdmin
 * @package Honda\Bundle\ServicesBundle\Admin
 */
class ServicesAdmin extends HomePublishableAdmin
{

    use RedirectionAdminTrait,
        ActivationAdminTrait;

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
        parent::configureRoutes($collection);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('position_', 'actions', array(
                    'actions' => array(
                        'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig', ['enable_top_bottom_buttons' => true])
                    )
                ))
                ->add('title', null, array(
                    'label' => 'Nom'
                ))
                ->add('subGroup', null, array(
                    'label' => 'Sous groupe'
                ))
                ->add('startDate', 'datetime', ['format'=>'d/m/Y',  'label' => 'Date début de publication'])
                ->add('endDate', 'datetime', ['format'=>'d/m/Y',   'label' => 'Date fin de publication'])
                ->add('published', 'boolean', ['editable' => true])
                ->add('publishedOnHome', null, ['editable' => true])
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    /**
     * @return array
     */
    protected function getPictos()
    {
        $collection = [];

        $container = $this->getConfigurationPool()->getContainer();

        $em = $container->get('doctrine.orm.default_entity_manager');
        $pictos = $em->getRepository(Pictos::class)->findAll();

        foreach ($pictos as $picto) {

            $provider = $container->get($picto->getPicto()->getProviderName());
            $url =  $provider->generatePublicUrl($picto->getPicto(), 'reference');

            $collection[$url] = $picto;
        }

        return $collection;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $pictos = $this->getPictos();

        $formMapper
            ->with('Contenu', array('class' => 'col-md-8'))

            ->add('pictos', ChoiceType::class, [
                'required' => true,
                'expanded' => true,
                'attr' => ["class" => 'services-picto'],
                'label' => 'Pictos',
                'choices' => $pictos,
            ])
            ->add('title', null, array(
                'label' => 'Nom'
            ))
            ->add('image', 'sonata_type_model_list', [
                'label' => 'Image',
                'required' => true
                    ], [
                'link_parameters' => [
                    'context' => 'image',
                    'filter' => [
                        'context' => [
                            'value' => 'image'
                        ]
                    ]
                ]
            ])
            ->add('body', 'ckeditor', array(
                'label' => 'Contenu'
            ))
            ->end()
            ->with('Meta', array('class' => 'col-md-4'))
            ->add('created', 'sonata_type_datetime_picker', array(
                'label' => 'Date de création',
                'attr' => array(
//                        'readonly' => true,
                ),
                'format' => 'dd/MM/yyyy HH:mm:ss',
            ))
            ->add('updated', 'sonata_type_datetime_picker', array(
                'label' => 'Date de modification',
                'attr' => array(
//                        'readonly' => true,
                ),
                'format' => 'dd/MM/yyyy HH:mm:ss',
            ))
            ->add('startDate', DatePickerType::class, ['format'=>'dd/MM/yyyy H:mm:ss', 'label' => 'Date début de publication'])
            ->add('endDate', DatePickerType::class, ['format'=>'dd/MM/yyyy H:mm:ss', 'label' => 'Date fin de publication'])
            ->add('publishedOnHome', null, [
                'label' => 'Publier sur la page d\'accueil',
                'required' => false,
            ])
            /* TODO: Suppirmer plutard
            ->add('isPublished', null, [
                'label' => 'Publié',
                'required' => false,
                    ]
            )*/
            ->end();

        $formMapper
            ->with('Bouton', array('class' => 'col-md-8'))
            ->add('label', null, array(
                'label' => 'Libellé'
            ))
            ->add('file', 'sonata_type_model_list', [
                'label' => 'Pdf',
                'required' => false,
                    ], [
                'link_parameters' => [
                    'context' => 'file',
                    'filter' => [
                        'context' => [
                            'value' => 'file'
                        ]
                    ]
                ]
            ])
            ->add('link', null, array(
                'label' => 'Lien contact'
            ))
            ->end();
        $this->addRedirectionBlock($formMapper, 'col-md-4');
        $formMapper
            ->with('Permission', array('class' => 'col-md-8'))
            ->add('subGroups', 'sonata_type_model', [
                'multiple' => true,
                'required' => false,
                'by_reference' => false,
                'label' => 'Groupes',
                'property' => 'name',
                'btn_add' => false
            ])
            ->end()
        ;
    }
}
