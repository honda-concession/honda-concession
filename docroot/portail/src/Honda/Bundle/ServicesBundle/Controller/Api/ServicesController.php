<?php

namespace Honda\Bundle\ServicesBundle\Controller\Api;

use Honda\Bundle\ServicesBundle\Entity\Pictos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Honda\Bundle\ServicesBundle\Entity\Banner;
use Gedmo\Sluggable\Util\Urlizer;

class ServicesController extends Controller
{
    /**
     * @Route("/api/services/{distributorId}", name="api_get_concession_services", requirements={"distributorId": "\d+"}, methods={"GET"})
     * @Method("GET")
     *
     */
    public function servicesAction(Request $request, $distributorId)
    {
        $subGroupIds = $this->getDoctrine()->getManager()->getRepository(Distributor::class)->getSubGroupIds($distributorId);

        $services = $this->getDoctrine()->getRepository('HondaServicesBundle:Services')->getServices($subGroupIds);
        $banner  = $this->getDoctrine()->getRepository(Banner::class)->findOneBy(['id' => 1]);

        $mm = $this->container->get('sonata.media.manager.media');
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $data = [];
        $data['banner'] = $bannerUrl = $imageUrl = null;
        $data['services'] = [];

        if($banner->getImage()){
            $bannerImage = $banner->getImage();
            $providerImage = $this->container->get($bannerImage->getProviderName());
            $bannerUrl = $baseurl . $providerImage->generatePublicUrl($bannerImage, 'reference');
            $data['banner'] = $bannerUrl;
        }

        foreach ($services as $key => $service) {

            if (!$service->isPublishedOnHome()) {
                continue;
            }

            $media = $service->getPictos()->getPicto();
            $provider = $this->container->get($media->getProviderName());

            $file = $mm->findOneBy(array('id' => $service->getFile()));
            $pdfUrl = null;

            if ($file) {
                $fileProvider = $this->container->get($file->getProviderName());
                $pdfUrl = $baseurl . $fileProvider->generatePublicUrl($file, 'reference');;
            }

            $image = $service->getImage();
            $imageUrl = null;

            if ($image){
                $provider1 = $this->container->get($image->getProviderName());
                $imageUrl = $baseurl . $provider1->generatePublicUrl($image, 'reference');
            }

            $data['services'][$key]['id'] = $service->getId();
            $data['services'][$key]['block_uniqid'] = Urlizer::urlize($service->getTitle());
            $data['services'][$key]['title'] = $service->getTitle();
            $data['services'][$key]['icon']['type'] = $media->getExtension();
            $data['services'][$key]['icon']['value'] =  $baseurl . $provider->generatePublicUrl($media, 'reference');
            $data['services'][$key]['pdf'] = $pdfUrl;
            $data['services'][$key]['label'] = $service->getLabel();
            $data['services'][$key]['link'] = $service->getLink();
            $data['services'][$key]['content'] = $service->getBody();
            $data['services'][$key]['image'] = $imageUrl;
            $data['services'][$key]['is_default_service'] = true;
            $data['services'][$key]['from_portail'] = true;
            $data['services'][$key] = $service->setRedirectionValuesToArray($data['services'][$key]);
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/picto-services", name="api_get_pictos_services", methods={"GET"})
     * @Method("GET")
     *
     */
    public function pictoServicesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $pictos = $em->getRepository(Pictos::class)->findBy([], ['name' => 'ASC']);

        $data = [];
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        foreach ($pictos as $picto) {
            $media = $picto->getPicto();
            $provider = $this->container->get($media->getProviderName());

            $item = [];
            $item['id'] = $picto->getId();
            $item['name'] = $picto->getName();
            $item['picto'] = $baseurl . $provider->generatePublicUrl($media, 'reference');

            $data[] = $item;
        }

        return $this->json($data);
    }
}
