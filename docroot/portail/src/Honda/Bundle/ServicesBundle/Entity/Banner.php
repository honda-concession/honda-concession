<?php

namespace Honda\Bundle\ServicesBundle\Entity;

use Honda\Bundle\SliderBundle\Model\BaseSlider;
use Doctrine\ORM\Mapping as ORM;

/**
 * Banner
 *
 * @ORM\Table(name="banner_services")
 * @ORM\Entity(repositoryClass="Honda\Bundle\ServicesBundle\Repository\BannerRepository")
 */
class Banner extends BaseSlider
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;    

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
       
}
