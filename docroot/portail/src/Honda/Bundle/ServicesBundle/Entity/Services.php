<?php

namespace Honda\Bundle\ServicesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Honda\Bundle\CommonBundle\Model\BaseModel;
use Gedmo\Mapping\Annotation as Gedmo;
use Honda\Bundle\CommonBundle\Entity\Traits\StartDateEndDateTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\HomeActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\RedirectionTrait;

/**
 * Services
 *
 * @ORM\Table(name="services")
 * @ORM\Entity(repositoryClass="Honda\Bundle\ServicesBundle\Repository\ServicesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Services extends BaseModel
{

    use StartDateEndDateTrait,
        HomeActivationTrait,
        ActivationTrait,
        RedirectionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Pictos")
     */
    private $pictos;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\Honda\Bundle\DistributorBundle\Entity\SubGroup", cascade={"persist"})
     */
    private $subGroups;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media|null
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    public function __construct()
    {
        parent::__construct();
        $this->subGroups = new ArrayCollection();
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        $now = new \DateTime('now');

        return  $this->getStartDate() <= $now && $now <= $this->getEndDate();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Services
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return Services
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set file.
     *
     * @param MediaInterface|null $file
     *
     * @return Services
     */
    public function setFile(\Application\Sonata\MediaBundle\Entity\Media $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return MediaInterface|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return Services
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set label.
     *
     * @param string $label
     *
     * @return Services
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set pictos.
     *
     * @param int $pictos
     *
     * @return Services
     */
    public function setPictos($pictos)
    {
        $this->pictos = $pictos;

        return $this;
    }

    /**
     * Get pictos.
     *
     * @return int
     */
    public function getPictos()
    {
        return $this->pictos;
    }

    /**
     * Set subGroups.
     *
     * @param \Doctrine\Common\Collections\Collection $subGroups
     *
     * @return Services
     */
    public function setSubGroups($subGroups)
    {
        $this->subGroups = $subGroups;

        return $this;
    }

    /**
     * Get subGroups.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubGroups()
    {
        return $this->subGroups;
    }

    /**
     * Add subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return Services
     */
    public function addSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        $this->subGroups[] = $subGroup;

        return $this;
    }

    /**
     * Remove subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return Services
     */
    public function removeSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        $this->subGroups->removeElement($subGroup);

        return $this;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Distributor
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        } else {
            return '';
        }
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }
}
