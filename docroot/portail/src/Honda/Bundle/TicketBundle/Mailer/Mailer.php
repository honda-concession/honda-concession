<?php

namespace Honda\Bundle\TicketBundle\Mailer;

use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\User;
use Honda\Bundle\TicketBundle\Entity\TicketMessage;
use Honda\Bundle\TicketBundle\Entity\TicketWithAttachment;
use Honda\Bundle\TicketBundle\TicketEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Mailer
 */
class Mailer
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Mailer constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Send a notification by e-mail to the concerned users when a ticket has been created|modified|deleted.
     *
     * @param TicketWithAttachment $ticket
     * @param string $eventName
     * @return null
     */
    public function sendTicketNotificationEmailMessage(TicketWithAttachment $ticket, $eventName)
    {

        // Retrieve the creator
        /** @var User $creator */
        $userId = $ticket->getUserCreated();
        $userManager = $this->container->get('fos_user.user_manager');
        $creator = $userManager->findUserBy(array('id' => $userId));
        $username = $email = null;

        if ($creator) {
            $username = $creator->getUsername();
            $email = $creator->getEmail();
        }

        // Prepare the email according to the message type
        switch ($eventName) {
            case TicketEvents::TICKET_CREATE:
                $subject = $this->container->get('translator')->trans('emails.ticket.new.subject', array(
                    '%number%' => $ticket->getId(),
                    '%sender%' => $username,
                ));
                $templateHTML = $this->container->getParameter('honda_ticket.emails')['new_html'];
                $templateTxt = $this->container->getParameter('honda_ticket.emails')['new_txt'];
                break;
            case TicketEvents::TICKET_UPDATE:
                $lastUserId = $ticket->getLastUser();
                $lastUser = $userManager->findUserBy(array('id' => $lastUserId));
                $username = $lastUser->getUsername();
                
                $subject = $this->container->get('translator')->trans('emails.ticket.update.subject', array(
                    '%number%' => $ticket->getId(),
                    '%sender%' => $username,
                ));
                $templateHTML = $this->container->getParameter('honda_ticket.emails')['update_html'];
                $templateTxt = $this->container->getParameter('honda_ticket.emails')['update_txt'];

                break;
            default:
                return null;
        }

        /** @var TicketMessage $message */
        $message = $ticket->getMessages()->last();

        /** @var UserManager $userManager */
        $users = $userManager->findUsers();

        // Prepare the recipients
        // At least the ticket's owner must receive the notification
        $recipients = array();

        if ($message->getUser() !== $userId) {
            $recipients[] = $email;
        }

        // Add every user with the ROLE_TICKET_ADMIN role
        /** @var User $user */
       /* foreach ($users as $user) {
            if ($user->hasRole('ROLE_TICKET_ADMIN')) {
                if (!in_array($user->getEmail(), $recipients)
                 && $message->getUser() !== $user->getId()
                ) {
                    $recipients[] = $user->getEmail();
                }
            }
        }
       */
    
        $emailsTo = [];
 
        foreach ($recipients as $recipient) {
            $emailsTo[] = ['name' => $recipient, 'email' => $recipient];
        }
        
        if ($subject && count($emailsTo) && $templateHTML) {
    
            $args = array(
                'ticket' => $ticket,
                'sender' => $username
            );
            
            $emailTemplate = $this->container->get('templating')->render($templateHTML, $args);
            
            $emailData = [
                'subject' => $subject,
                'sender' => [
                    'name' => $this->container->getParameter('honda_ticket.sender')['sender_name'],
                    'email' => $this->container->getParameter('honda_ticket.sender')['sender_email']
                ],
                'to' => $emailsTo,
                'htmlContent' => $emailTemplate,
                'headers' => ['Content-Type' => 'text/html',  'charset' =>  'utf8'],
            ];
    
            $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail($emailData);
    
            try {
                $this->container->get('sendinblue_api.smtp_endpoint')->sendTransacEmail($sendSmtpEmail);
        
                return true;
            } catch (\SendinBlue\Client\ApiException $e) {
                echo 'Exception when calling SMTPApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
            }
            
        }
        
        return null;
    }


}
