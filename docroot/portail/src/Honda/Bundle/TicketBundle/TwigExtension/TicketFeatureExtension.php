<?php

namespace Honda\Bundle\TicketBundle\TwigExtension;

use Doctrine\ORM\EntityManager;
use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Honda\Bundle\TicketBundle\Component\TicketFeatures;

class TicketFeatureExtension extends \Twig_Extension
{
    private $ticketFeatures;
    
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param TicketFeatures $ticketFeatures
     */
    public function __construct(TicketFeatures $ticketFeatures, EntityManager $em)
    {
        $this->ticketFeatures = $ticketFeatures;
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('hasTicketFeature', [$this, 'hasFeature']),
            new \Twig_SimpleFunction('getDistributorDomainById', [$this, 'getDistributorDomainById']),
        ];
    }

    /**
     * @param string $feature
     *
     * @return bool|null
     */
    public function hasFeature($feature)
    {
        return $this->ticketFeatures->hasFeature($feature);
    }
    
    /**
     * @param $id
     */
    public function getDistributorDomainById($id = null)
    {
        if (!$id) {
            return;
        }
        
        $distributor = $this->em->getRepository(Distributor::class)->find($id);
        
        if ($distributor) {
            return $distributor->getWebsiteUrl();
        }
        
        return;
    }
    

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ticketFeature';
    }
}
