## Migrating from v1 to v2

Add your user class into your config.

```yaml
honda_ticket:
    user_class: AppBundle\Entity\User
```

```Honda\Bundle\TicketBundle\User\UserInterface``` has been replaced with ```Honda\Bundle\TicketBundle\Manager\UserManagerInterface```

Your user class needs to implement ```Honda\Bundle\TicketBundle\Model\UserInterface```

Roles are now checked against the User
