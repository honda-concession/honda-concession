## Migration from v2 to v3

Any reference to `TicketMessage` constants will need to use `TicketMessageInterface`.

Previously:

```
Honda\Bundle\TicketBundle\Entity\TicketMessage::STATUS_OPEN
Honda\Bundle\TicketBundle\Entity\TicketMessage::STATUS_CLOSED
```

Will now become:

```
Honda\Bundle\TicketBundle\Model\TicketMessageInterface::STATUS_OPEN
Honda\Bundle\TicketBundle\Model\TicketMessageInterface::STATUS_CLOSED
```

### `TicketManagerInterface` Changes

The interface ```Honda\Bundle\TicketBundle\Manager\TicketManagerInterface``` has been modified.

* Entity Manager needs passing in through `setEntityManager()`
* Translator needs passing in through `setTranslator()`
* `getTicketStatus()` and `getTicketPriority()` no longer need translator passing
* added `createMessage()` and `getMessageById()`
* `updateTicket()` now can take an optional message object.
