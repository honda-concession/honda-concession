<?php

namespace Honda\Bundle\TicketBundle\Entity;

use Honda\Bundle\TicketBundle\Entity\Traits\TicketFeature\MessageAttachmentTrait;
use Honda\Bundle\TicketBundle\Entity\Traits\TicketMessageTrait;
use Honda\Bundle\TicketBundle\Model\TicketFeature\MessageAttachmentInterface;
use Honda\Bundle\TicketBundle\Model\TicketMessageInterface;

/**
 * Ticket Message.
 */
class TicketMessageWithAttachment implements TicketMessageInterface, MessageAttachmentInterface
{
    use TicketMessageTrait;
    use MessageAttachmentTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
