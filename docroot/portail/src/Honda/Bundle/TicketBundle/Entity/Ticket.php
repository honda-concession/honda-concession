<?php

namespace Honda\Bundle\TicketBundle\Entity;

use Honda\Bundle\TicketBundle\Entity\Traits\TicketTrait;
use Honda\Bundle\TicketBundle\Model\TicketInterface;

/**
 * Ticket.
 */
class Ticket implements TicketInterface
{
    use TicketTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
