<?php

namespace Honda\Bundle\TicketBundle\Entity;

use Honda\Bundle\TicketBundle\Entity\Traits\TicketFeature\MessageAttachmentTrait;
use Honda\Bundle\TicketBundle\Entity\Traits\TicketTrait;
use Honda\Bundle\TicketBundle\Model\TicketFeature\TicketAttachmentInterface;
use Honda\Bundle\TicketBundle\Model\TicketInterface;


/**
 * Ticket.
 */
class TicketWithAttachment implements TicketInterface, TicketAttachmentInterface
{
    use TicketTrait;
    use MessageAttachmentTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
