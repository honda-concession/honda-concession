<?php

namespace Honda\Bundle\TicketBundle\Entity;

use Honda\Bundle\TicketBundle\Entity\Traits\TicketMessageTrait;
use Honda\Bundle\TicketBundle\Model\TicketMessageInterface;

/**
 * Ticket Message.
 */
class TicketMessage implements TicketMessageInterface
{
    use TicketMessageTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
