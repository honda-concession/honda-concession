<?php

namespace Honda\Bundle\TicketBundle\Controller;

use Honda\Bundle\TicketBundle\Entity\TicketMessageWithAttachment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Ticket Attachment controller.
 *
 * Download attachments
 */
class TicketAttachmentController extends Controller
{

    /**
     * Download attachment on message.
     *
     * @param int $ticketMessageId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function downloadAction($ticketMessageId)
    {
        $ticketManager = $this->get('honda_ticket.ticket_manager');
        $ticketMessage = $ticketManager->getMessageById($ticketMessageId);

        if (!$ticketMessage || !$ticketMessage instanceof TicketMessageWithAttachment) {
            throw $this->createNotFoundException($this->get('translator')->trans('ERROR_FIND_TICKET_ENTITY'));
        }

        // check permissions
        $userManager = $this->get('honda_ticket.user_manager');
        $userManager->hasPermission($userManager->getCurrentUser(), $ticketMessage->getTicket());

        $response = new BinaryFileResponse($this->getParameter('ticket_attachment') . $ticketMessage->getAttachmentName());
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $ticketMessage->getAttachmentName());
        $response->headers->set('Content-type', $ticketMessage->getAttachmentMimeType());

        return $response;

    }

}
