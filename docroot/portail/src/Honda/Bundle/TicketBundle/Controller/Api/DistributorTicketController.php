<?php

namespace Honda\Bundle\TicketBundle\Controller\Api;

use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Honda\Bundle\CommonBundle\Tools\Encryption\EncryptorAesEcb;
use Honda\Bundle\TicketBundle\Entity\TicketMessageWithAttachment;
use Honda\Bundle\TicketBundle\Model\TicketMessageInterface;
use Honda\Bundle\TicketBundle\Model\TicketInterface;
use Honda\Bundle\TicketBundle\Event\TicketEvent;
use Honda\Bundle\TicketBundle\TicketEvents;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class DistributorTicketController extends Controller
{

    /**
     * @Route("/api/ticket/list/{distributorId}", name="api_get_ticket_list", requirements={"distributorId": "\d+"}, methods={"GET"})
     * @Method("GET")
     *
     */
    public function indexAction(Request $request, $distributorId)
    {

        $ticketManager = $this->get('honda_ticket.ticket_manager');

        $tickets = $ticketManager->findTicketsBy(['distributor' => $distributorId], ['createdAt' => 'DESC']);


        $ticketState = $request->get('state', $this->get('translator')->trans('STATUS_OPEN'));
        $ticketPriority = $request->get('priority', null);

        $data = [];
        $data['ticketState'] = $ticketState;
        $data['ticketPriority'] = $ticketPriority;
        $data['ticket'] = [];
        foreach ($tickets as $key => $ticket) {
            $data['ticket'][$key]['ticket'] = $ticket->getId();
            $data['ticket'][$key]['subject'] = $ticket->getSubject();
            $data['ticket'][$key]['userCreated'] = $ticket->getUserCreated();
            $data['ticket'][$key]['userCreatedObject'] = $ticket->getUserCreatedObject()->getUsername();
            $data['ticket'][$key]['status'] = $ticket->getStatus();
            $data['ticket'][$key]['priority'] = $ticket->getPriority();
            $data['ticket'][$key]['statusString'] = $ticket->getStatusString();
            $data['ticket'][$key]['priorityString'] = $ticket->getPriorityString();
            $data['ticket'][$key]['updated'] = $ticket->getLastMessage();
            $data['ticket'][$key]['created'] = $ticket->getCreatedAt();
            $data['ticket'][$key]['distributor'] = $ticket->getDistributor();
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/ticket/{ticketId}/show/{distributorId}", name="api_get_ticket_message", requirements={"distributorId": "\d+","ticketId": "\d+"}, methods={"GET"})
     * @Method("GET")
     *
     */
    public function showAction($ticketId, $distributorId)
    {
        $ticketManager = $this->get('honda_ticket.ticket_manager');
        $tickets = $ticketManager->findTicketsBy(['id' => $ticketId, 'distributor' => $distributorId]);
        $data = [];

        if (!$tickets) {
            throw new NotFoundHttpException("Page not found.");
        }

        foreach ($tickets as $ticket) {
            $data['ticket']['id'] = $ticket->getId();
            $data['ticket']['subject'] = $ticket->getSubject();
            $data['ticket']['createdAt'] = $ticket->getCreatedAt();
            $data['ticket']['status'] = $ticket->getStatus();
            $data['ticket']['priority'] = $ticket->getPriority();
            $data['ticket']['statusString'] = $ticket->getStatusString();
            $data['ticket']['priorityString'] = $ticket->getPriorityString();
            $data['ticket']['userCreatedObject'] = $ticket->getUserCreatedObject()->getUsername();
            foreach ($ticket->getMessages() as $key => $message) {

                $data['messages'][$key]['message_id'] = $message->getId();
                $data['messages'][$key]['message'] = $message->getMessage();
                $data['messages'][$key]['statusString'] = $message->getStatusString();
                $data['messages'][$key]['status'] = $message->getStatus();
                $data['messages'][$key]['priority'] = $message->getPriority();
                $data['messages'][$key]['priorityString'] = $message->getPriorityString();
                $data['messages'][$key]['createdAt'] = $message->getCreatedAt();
                $data['messages'][$key]['user'] = $message->getUser();
                $data['messages'][$key]['userObject'] = $message->getUserObject()->getUsername();
                $data['messages'][$key]['attachmentName'] = $message->getAttachmentName();
                $data['messages'][$key]['attachmentSize'] = $message->getAttachmentSize();
            }
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/ticket/create", name="api_ticket_create", methods={"POST"})
     * @param array ['subject', 'message', 'priority', distributor, filename, size, mimetype]
     */
    public function createAction(Request $request)
    {

        $token = $request->headers->get('X-AUTH-TOKEN');
        $key = $this->container->getParameter('api_multisite_token');
        $encodeToken = EncryptorAesEcb::encode($request->getContent(), $key);

//        if ($token !== $encodeToken) {
//            throw new NotFoundHttpException("Page not found.");
//        }
    
        $data = json_decode($request->getContent(), true);

        $errors = $this->validateTicketInfos($data);
        
        if (!empty($errors)) {
            return new JsonResponse(['errors' => $errors], 400);
        }
        
        $ticketManager = $this->get('honda_ticket.ticket_manager');

        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->findUserBy(array('distributor' => $data['distributor']));
        $distributor = $this->getDoctrine()->getRepository(Distributor::class)->find($data['distributor']);

        if ($user) {

            $ticket = $ticketManager->createTicket();
            $message = $ticketManager->createMessage($ticket);
            $ticket->setLastUser($user->getId());
            $ticket->setUserCreated($user->getId());
            $ticket->setLastUser($user->getId());

            $ticket->setSubject($data['subject']);
            $ticket->setDistributor($data['distributor']);
        }

        $message->setMessage($data['message']);
        $message->setStatus(TicketMessageInterface::STATUS_OPEN);
        $message->setUser($user->getId());
        
        if (!empty($data['filename'])){
            $message->setAttachmentName($data['filename']);
            $message->setAttachmentSize($data['size']);
            $message->setAttachmentMimeType($data['mimetype']);
        }
        $ticket->addMessage($message);
        
        $ticketManager->updateTicket($ticket, $message);
        
        $this->dispatchTicketEvent(TicketEvents::TICKET_CREATE, $ticket);
        
        $data = [];
        
        $message = null;
        if (count($ticket->getMessages())) {
            $firstMessage = $ticket->getMessages()->first();
            $message = $firstMessage->getMessage();
        }
        
        $data['ticketId'] = $ticket->getId();
        $data['subject'] = $ticket->getSubject();
        $data['first_message'] = $message;
        $data['distributor_email'] = null;
        $data['distributor_code_sap'] = null;
        $data['distributor_email'] = null;
        
        if ($distributor) {
            $data['distributor_name'] = $distributor->getName();
            $data['distributor_code_sap'] = $distributor->getCodeSap();
            $data['distributor_email'] = $distributor->getEmail();
        }
        
        return new JsonResponse($data, 201);
    }

    /**
     * @Route("/api/ticket/{ticketId}/reply", name="api_ticket_reply", methods={"POST"})
     *
     * @param array ['ticketId','message', 'priority', condition, 'file', distributor, filename, size, mimetype]
     */
    public function replyAction(Request $request)
    {
        $token = $request->headers->get('X-AUTH-TOKEN');
        $key = $this->container->getParameter('api_multisite_token');
        $encodeToken = EncryptorAesEcb::encode($request->getContent(), $key);

        /*
        if ($token !== $encodeToken) {
            return new JsonResponse('Erreur resource', 400);
        }
        */
        $data = json_decode($request->getContent(), true);
        //$data = file_get_contents($filename);

        $errors = $this->validateTicketInfos($data);

        if (!empty($errors)) {
            return new JsonResponse(['errors' => $errors], 400);
        }

        $ticketManager = $this->get('honda_ticket.ticket_manager');

        $ticket = $ticketManager->getTicketById($data['ticketId']);

        if (!$ticket) {
            throw new NotFoundHttpException("Page not found.");
        }

        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->findUserBy(array('distributor' => $data['distributor']));
        $distributor = $this->getDoctrine()->getRepository(Distributor::class)->find($data['distributor']);

        if ($user) {

            $message = $ticketManager->createMessage($ticket);
            $message->setMessage($data['message']);
            $message->setStatus($data['status']);
            $message->setPriority($data['priority']);
            $message->setUser($user->getId());
            $message->setAttachmentName($data['filename']);
            $message->setAttachmentSize($data['size']);
            $message->setAttachmentMimeType($data['mimetype']);
        }

        $ticketManager->updateTicket($ticket, $message);

        $data = [];
    
        $message = null;
        if (count($ticket->getMessages())) {
            $firstMessage = $ticket->getMessages()->first();
            $message = $firstMessage->getMessage();
        }
    
        $data['ticketId'] = $ticket->getId();
        $data['subject'] = $ticket->getSubject();
        $data['first_message'] = $message;
        $data['distributor_email'] = null;
        $data['distributor_code_sap'] = null;
        $data['distributor_email'] = null;
    
        if ($distributor) {
            $data['distributor_name'] = $distributor->getName();
            $data['distributor_code_sap'] = $distributor->getCodeSap();
            $data['distributor_email'] = $distributor->getEmail();
        }
        
        return new JsonResponse($data, 201);
    }

    /**
     * @Route("/api/ticket/attachment/{ticketMessageId}", name="api_download_attachment", requirements={"ticketMessageId": "\d+"}, methods={"GET"})
     *
     * @Method("GET")
     */
    public function downloadAction($ticketMessageId)
    {
        $ticketManager = $this->get('honda_ticket.ticket_manager');
        $ticketMessage = $ticketManager->getMessageById($ticketMessageId);

        if (!$ticketMessage || !$ticketMessage instanceof TicketMessageWithAttachment) {
            throw $this->createNotFoundException($this->get('translator')->trans('ERROR_FIND_TICKET_ENTITY'));
        }

        $response = new BinaryFileResponse($this->getParameter('ticket_attachment') . $ticketMessage->getAttachmentName());
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $ticketMessage->getAttachmentName());
        $response->headers->set('Content-type', $ticketMessage->getAttachmentMimeType());

        return $response;
    }

    /**
     * @Route("/api/attachment/save", name="api_attachment_save", methods={"POST"})
     *
     * @param array
     */
    public function saveFileAction(Request $request)
    {

        $file = $request->files->get('fileContent');

        if ($file) {
            $fileName = $file->getClientOriginalName();
            try {
                $file->move(
                        $this->getParameter('ticket_attachment'), $fileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
        }


        return new JsonResponse(['message' => 'Fichier sauvegardé'], 201);
    }

    private function dispatchTicketEvent($ticketEvent, TicketInterface $ticket)
    {
        $event = new TicketEvent($ticket);
        $this->get('event_dispatcher')->dispatch($ticketEvent, $event);
    }

    /**
     * Validation distributor informations
     */
    protected function validateTicketInfos($data)
    {
        $errors = [];

        if (!isset($data['message'])) {
            $errors['message'] = "Le champs message est requis";
        }
        if (!isset($data['priority'])) {
            $errors['priority'] = "Le champs priorité est requis";
        }

        return $errors;
    }

}
