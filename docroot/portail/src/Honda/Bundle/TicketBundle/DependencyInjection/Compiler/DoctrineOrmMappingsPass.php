<?php

namespace Honda\Bundle\TicketBundle\DependencyInjection\Compiler;

use Honda\Bundle\TicketBundle\DependencyInjection\HondaTicketExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class DoctrineOrmMappingsPass extends \Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass
{
    public function __construct($driver = null, array $namespaces = [], $managerParameters = [], $enabledParameter = false, array $aliasMap = [])
    {
        parent::__construct($driver, $namespaces, $managerParameters, $enabledParameter, $aliasMap);
    }

    public function process(ContainerBuilder $container)
    {
        $bundleDirectory = HondaTicketExtension::bundleDirectory();
        $namespaces      = [];

        if (
            'Honda\Bundle\TicketBundle\Entity\TicketWithAttachment' === $container->getParameter('honda_ticket.model.ticket.class')
            ||
            'Honda\Bundle\TicketBundle\Entity\TicketMessageWithAttachment' === $container->getParameter('honda_ticket.model.message.class')
        ) {
            $namespaces[realpath($bundleDirectory.'/Resources/config/doctrine/model/attachment')] = 'Honda\Bundle\TicketBundle\Entity';
        } elseif (
            'Honda\Bundle\TicketBundle\Entity\Ticket' === $container->getParameter('honda_ticket.model.ticket.class')
            ||
            'Honda\Bundle\TicketBundle\Entity\TicketMessage' === $container->getParameter('honda_ticket.model.message.class')
        ) {
            $namespaces[realpath($bundleDirectory.'/Resources/config/doctrine/model/plain')] = 'Honda\Bundle\TicketBundle\Entity';
        }

        $arguments        = [$namespaces, '.orm.xml'];
        $locator          = new Definition('Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator', $arguments);
        $this->driver     = new Definition('Doctrine\ORM\Mapping\Driver\XmlDriver', [$locator]);
        $this->namespaces = $namespaces;

        parent::process($container);
    }
}
