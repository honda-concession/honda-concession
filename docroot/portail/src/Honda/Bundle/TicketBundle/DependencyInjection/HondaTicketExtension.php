<?php

namespace Honda\Bundle\TicketBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class HondaTicketExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(self::bundleDirectory().'/Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('honda_ticket.model.user.class', $config['user_class']);
        $container->setParameter('honda_ticket.model.ticket.class', $config['ticket_class']);
        $container->setParameter('honda_ticket.model.message.class', $config['message_class']);

        $container->setParameter('honda_ticket.features', $config['features']);
        $container->setParameter('honda_ticket.templates', $config['templates']);
        
        $container->setParameter('honda_ticket.sender', $config['sender']);
        $container->setParameter('honda_ticket.emails', $config['emails']);

        $bundles = $container->getParameter('kernel.bundles');
        // Remove "file_upload_subscriber" definition if VichUploaderBundle is not registered
        if (!isset($bundles['VichUploaderBundle'])) {
            $container->removeDefinition('honda_ticket.file_upload_subscriber');
        }
    }

    public static function bundleDirectory()
    {
        return realpath(__DIR__.'/..');
    }
}
