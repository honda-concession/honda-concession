<?php

namespace Honda\Bundle\TicketBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $treeBuilder->root('honda_ticket')
            ->children()
                ->scalarNode('user_class')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('ticket_class')->cannotBeEmpty()->defaultValue('Honda\Bundle\TicketBundle\Entity\Ticket')->end()
                ->scalarNode('message_class')->cannotBeEmpty()->defaultValue('Honda\Bundle\TicketBundle\Entity\TicketMessage')->end()
                ->arrayNode('features')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('attachment')->defaultTrue()->end()
                    ->end()
                ->end()
                ->arrayNode('templates')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('index')->defaultValue('HondaTicketBundle:Ticket:index.html.twig')->end()
                        ->scalarNode('new')->defaultValue('HondaTicketBundle:Ticket:new.html.twig')->end()
                        ->scalarNode('prototype')->defaultValue('HondaTicketBundle:Ticket:prototype.html.twig')->end()
                        ->scalarNode('show')->defaultValue('HondaTicketBundle:Ticket:show.html.twig')->end()
                        ->scalarNode('show_attachment')->defaultValue('HondaTicketBundle:Ticket:show_attachment.html.twig')->end()
                        ->scalarNode('macros')->defaultValue('HondaTicketBundle:Macros:macros.html.twig')->end()
                    ->end()
                ->end()
                ->arrayNode('sender')
                    ->children()
                        ->scalarNode('sender_email')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('sender_name')->isRequired()->cannotBeEmpty()->end()
                    ->end()
                ->end()
                ->arrayNode('emails')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('new_html')->defaultValue('HondaTicketBundle:Emails:ticket.new.html.twig')->end()
                        ->scalarNode('new_txt')->defaultValue('HondaTicketBundle:Emails:ticket.new.txt.twig')->end()
                        ->scalarNode('update_html')->defaultValue('HondaTicketBundle:Emails:ticket.update.html.twig')->end()
                        ->scalarNode('update_txt')->defaultValue('HondaTicketBundle:Emails:ticket.update.txt.twig')->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
