<?php

namespace Honda\Bundle\TicketBundle;

final class TicketEvents
{
    /**
     * The honda.ticket.create event is thrown each time an ticket is created
     * in the system.
     *
     * The honda.ticket.update event is thrown each time an ticket is updated
     * in the system.
     *
     * The honda.ticket.delete event is thrown each time an ticket is deleted
     * in the system.
     *
     * The event listeners receives an
     * Honda\Bundle\TicketBundle\Event\TicketEvent instance.
     *
     * @var string
     */
    const TICKET_CREATE = 'honda.ticket.create';
    const TICKET_UPDATE = 'honda.ticket.update';
    const TICKET_DELETE = 'honda.ticket.delete';
}
