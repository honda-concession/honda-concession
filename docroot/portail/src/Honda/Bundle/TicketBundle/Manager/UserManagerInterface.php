<?php

namespace Honda\Bundle\TicketBundle\Manager;

use Honda\Bundle\TicketBundle\Model\TicketInterface;
use Honda\Bundle\TicketBundle\Model\UserInterface;

interface UserManagerInterface
{
    public function getCurrentUser();

    public function getUserById($userId);

    public function hasRole(UserInterface $user, $role);

    /**
     * @param \Honda\Bundle\TicketBundle\Model\UserInterface|string $user
     * @param TicketInterface                                           $ticket
     */
    public function hasPermission($user, TicketInterface $ticket);
}
