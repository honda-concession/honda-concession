<?php

namespace Honda\Bundle\FrontBundle\Menu;


use Symfony\Component\Routing\RouterInterface;


class Builder
{
    
    /**
     * @var RouterInterface
     */
    protected $router;

    
    /**
     * Builder constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }
    
    /**
     * @return array
     */
    public function siteMap()
    {
        $urls = [];
        
        $urls[] = [
            'routeName' => 'page_slug',
            'name' => 'Motos et scooters d\'occasion',
            'url' =>  $this->router->generate('page_slug', ['path' => '/'], RouterInterface::ABSOLUTE_URL),
            'children' => [],
            'sitemap_children' => []
        ];
    
        $urls[] = [
            'routeName' => 'page_slug',
            'name' => 'Concessionnaires',
            'url' =>  $this->router->generate('page_slug', ['path' => '/'], RouterInterface::ABSOLUTE_URL),
            'children' => [],
            'sitemap_children' => []
        ];
    
        $urls[] = [
            'routeName' => 'page_slug',
            'name' => 'Label Honda',
            'url' =>  $this->router->generate('page_slug', ['path' => '/label-honda'], RouterInterface::ABSOLUTE_URL),
            'children' => [],
            'sitemap_children' => []
        ];
    
        $urls[] = [
            'routeName' => '',
            'name' => 'Honda moto France',
            'url' =>  'https://www.honda.fr/',
            'children' => [],
            'sitemap_children' => []
        ];
        
        $urls[] = [
            'routeName' => 'page_slug',
            'name' => 'Vie privée et cookies',
            'url' =>  $this->router->generate('page_slug', ['path' => '/cookies'], RouterInterface::ABSOLUTE_URL),
            'children' => [],
            'sitemap_children' => []
        ];
    
        $urls[] = [
            'routeName' => 'page_slug',
            'name' => 'Mentions légales',
            'url' =>  $this->router->generate('page_slug', ['path' => '/mentions-legales'], RouterInterface::ABSOLUTE_URL),
            'children' => [],
            'sitemap_children' => []
        ];
    
    
        $urls[] = [
            'routeName' => 'page_slug',
            'name' => 'Contact',
            'url' =>  $this->router->generate('page_slug', ['path' => '/plan-du-site'], RouterInterface::ABSOLUTE_URL) . '#form',
            'children' => [],
            'sitemap_children' => []
        ];
        
        return $urls;
    }
}
