<?php

namespace Honda\Bundle\FrontBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Honda\Bundle\CommonBundle\Entity\SeoContent;

class DefaultController extends Controller
{
    /**
     * @Route("api/page-description")
     */
    public function homDescriptionAction(Request $request, $brand = null, $region = null)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(SeoContent::class);
        if ($region) {
            $seoContent['landing'] = $repository->findOneByType('Landing Page - Region');
        }
        if ($brand) {
            $seoContent['landing'] = $repository->findOneByType('Landing Page - Marque');
        }
        if (!($brand || $region)) {
            if ($request->get('pathInfo') == '/concessionnaire') {
                $content = $repository->findOneByType('Homepage / Concession');
                $seoContent['concession']['content'] = $content;
                $seoContent['concession']['altContent'] = $content;
            } else {
                $seoContent['vo']['content'] = $repository->findOneByType('Homepage');
                $seoContent['vo']['altContent'] = $repository->findOneByType('Homepage / Concession');
            }
        }

        return $this->render('HondaFrontBundle:Block:description.html.twig', array(
            'seoContent' => $seoContent,
            'brand' => $brand,
            'region' => $region
        ));
    }
}
