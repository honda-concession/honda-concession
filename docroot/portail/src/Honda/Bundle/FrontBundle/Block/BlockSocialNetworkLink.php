<?php

namespace Honda\Bundle\FrontBundle\Block;

use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\Bundle\DistributorBundle\Entity\SocialNetwork;

class BlockSocialNetworkLink extends AbstractAdminBlockService
{

    /**
     * ObjectManager
     */
    protected $em;
    
    /**
     * @param string $name
     * @param EngineInterface $templating
     * @param ObjectManager $em
     */
    public function __construct($name, EngineInterface $templating, ObjectManager $em)
    {
        parent::__construct($name, $templating);
        $this->em = $em;
    }
    
    
    public function configureOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'title' => 'Block Social network link',
        ));
    }

    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $socialNetwork = $this->em->getRepository(SocialNetwork::class)->find(SocialNetwork::UNIQ_ID);
        
        return $this->renderResponse('HondaFrontBundle:Block:block_social_network_link.html.twig', [
            'block' => $blockContext->getBlock(),
            'socialNetwork' => $socialNetwork,
        ], $response);
    }
    
    /**
     * @return array
     */
    public function getRegions()
    {
        return array_flip(Region::REGIONS);
    }

}
