<?php

namespace Honda\Bundle\FrontBundle\Block;

use Honda\Bundle\VehiclesOccasionBundle\Entity\Slider;
use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Honda\Bundle\SearchBundle\Model\SearchSelectData;
use Doctrine\Common\Persistence\ObjectManager;

use Honda\Bundle\DistributorBundle\Model\Region;

class BlockSearch extends AbstractAdminBlockService
{
    /**
     * @var SearchSelectBuilder
     */
    protected $searchSelectBuilder;
    
    /**
     * ObjectManager
     */
    protected $em;
    
    /**
     * @param string $name
     * @param EngineInterface $templating
     * @param SearchSelectData $searchSelectData
     * @param ObjectManager $em
     */
    public function __construct($name, EngineInterface $templating, SearchSelectData $searchSelectData, ObjectManager $em)
    {
        parent::__construct($name, $templating);
        $this->searchSelectData = $searchSelectData;
        $this->em = $em;
    }
    
    
    public function configureOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'title' => 'Block Search',
        ));
    }

    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $types = $this->searchSelectData->getSelectData('types');
        $slidersVo = $this->em->getRepository(Slider::class)->getActivesSliders();
    
        $marks = $this->searchSelectData->getMarks();
        
        return $this->renderResponse('HondaFrontBundle:Block:block_search.html.twig', [
            'block' => $blockContext->getBlock(),
            'types' => $types,
            'marks' => $marks,
            'slidersVo' => $slidersVo,
            'regions' => $this->getRegions(),
        ], $response);
    }
    
    /**
     * @return array
     */
    public function getRegions()
    {
        return array_flip(Region::REGIONS);
    }

}
