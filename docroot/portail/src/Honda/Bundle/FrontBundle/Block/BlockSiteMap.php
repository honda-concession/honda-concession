<?php

namespace Honda\Bundle\FrontBundle\Block;

use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Honda\Bundle\FrontBundle\Menu\Builder;

use Symfony\Component\OptionsResolver\OptionsResolver;


class BlockSiteMap extends AbstractAdminBlockService
{

    protected $builder;
    
    /**
     * @param string $name
     * @param EngineInterface $templating
     * @param Builder $builder
     */
    public function __construct($name, EngineInterface $templating, Builder $builder)
    {
        parent::__construct($name, $templating);
        $this->builder = $builder;
    }

    
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'url' => false,
            'title' => 'Block Plan du site',
            'template' => '@HondaFront/Block/block_sitemap.html.twig',
        ]);
        
    }
    
    /**
     * @param BlockContextInterface $blockContext
     * @param Response|null $response
     * @return Response
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $sitemap = $this->builder->siteMap();
        
        return $this->renderResponse($blockContext->getTemplate(), [
            'block' => $blockContext->getBlock(),
            'sitemap' => $sitemap,
        ], $response);
    }

}
