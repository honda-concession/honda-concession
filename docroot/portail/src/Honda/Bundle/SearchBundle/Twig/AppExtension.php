<?php

namespace Honda\Bundle\SearchBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;


class AppExtension extends AbstractExtension
{
    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('preg_match', [$this, 'pregMatch']),
        ];
    }
    
    /**
     * @param $pattern
     * @param $string
     * @param array $matches
     * @param int $flags
     * @return string
     */
    public function pregMatch($pattern, $string, $matches = [], $flags = PREG_OFFSET_CAPTURE)
    {
    
        $pattern =  sprintf('/%s/', $pattern);
        
        return preg_match( $pattern, $string);
    }
}