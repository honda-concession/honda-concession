<?php

namespace Honda\Bundle\SearchBundle\Model;


class DistributorSearch
{

    /**
     * @var string
     */
    public $name;
    
    /**
     * @var string
     *
     */
    public $locationRetailer;
 
    /**
     * @var string
     */
    public $region;
     
    /**
     * @var string
     */
    public $department;
    
    /**
     * @var string
     *
     */
    public $location;
    
    /**
     * @var string
     *
     */
    public $longitude;
    
    /**
     * @var string
     *
     */
    public $latitude;
    /**
     * @var string
     *
     */
    public $accuracy;
    
    /**
     * @var string
     *
     */
    public $agentOfficiel;
    
    /**
     * @var string
     *
     */
    public $specialistCross;
    
    /**
     * @var string
     *
     */
    public $distributorMotor;
    
    /**
     * @var
     */
    public $sortByName;
    
    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Get region.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }
    
    /**
     * Get department.
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }
    
    /**
     * Get agentOfficiel.
     *
     * @return bool
     */
    public function getAgentOfficiel()
    {
        return $this->agentOfficiel;
    }
    
    /**
     * Get specialistCross.
     *
     * @return bool
     */
    public function getSpecialistCross()
    {
        return $this->specialistCross;
    }
    
    /**
     * Get distributorMotor.
     *
     * @return bool
     */
    public function getDistributorMotor()
    {
        return $this->distributorMotor;
    }
    
    /**
     * Get locationRetailer.
     *
     * @return string
     */
    public function getLocationRetailer()
    {
        return $this->locationRetailer;
    }
    
    /**
     * Get accuracy.
     *
     * @return string
     */
    public function getAccuracy()
    {
        return $this->accuracy;
    }
    
    /**
     * Get latitude.
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }
    
    /**
     * Get longitude.
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
    /**
     * Get location.
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->getLatitude() . ','. $this->getLongitude();
    }
    
    /**
     * @return mixed
     */
    public function getSortByName()
    {
        return $this->sortByName;
    }
    
    /**
     * @param mixed $sortByName
     */
    public function setSortByName($sortByName)
    {
        $this->sortByName = $sortByName;
    }
    
}
