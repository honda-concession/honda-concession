<?php

namespace Honda\Bundle\SearchBundle\Model;

use Doctrine\Common\Persistence\ObjectManager;
use Gedmo\Sluggable\Util\Urlizer;
use Honda\Bundle\CommonBundle\Search\Client;
use Honda\Bundle\VehiclesOccasionBundle\Entity\Brand;

/**
 * Class SearchSelectData
 * @package Honda\Bundle\SearchBundle\Model
 */
class SearchSelectData
{
    
    const INDEX = 'honda';
    const TYPE = 'vehiclead';
    
    /**
     * @var Client $client
     */
    protected $client;
    
    /**
     * @var ObjectManager
     */
    protected $em;
    
    /**
     * SearchSelectBuilder constructor.
     * @param $host
     * @param $port
     * @param ObjectManager $em
     */
    public function __construct($host, $port, ObjectManager $em)
    {
        
        $elasticaClient = new Client([
            'host' => $host,
            'port' => $port
        ]);
    
        $this->client = $elasticaClient;
        $this->em = $em;
    }
    
    /**
     * @param $selectName
     * @param int $size
     * @return array
     */
    public function getSelectData($selectName, $size = 1000000)
    {
        $collection = [];
        $termName = 'items';
    
        $termsAgg = new \Elastica\Aggregation\Terms($termName);
        
        if ($selectName == 'types') {
            
            $termsAgg->setField('categoryPortail.raw');
            $termName = 'types';
            
        } elseif ($selectName == 'marks') {
            
            $termsAgg->setField('markPortail.raw');
            $termName = 'marks';
            
        } else {
            throw new \InvalidArgumentException(sprintf('%s select name not support', $selectName));
        }
    
        $termsAgg->setName($termName);
        $termsAgg->setSize($size);
    
        $query = new \Elastica\Query();
        $query->addAggregation($termsAgg);
        
        $index = $this->client->getIndex(self::INDEX);
        
        try {
            $selectDatas = $index->search($query)->getAggregation($termName);
        } catch (\Elastica\Exception\InvalidException $e) {
            $selectDatas = [];
        }
        
        if (isset($selectDatas['buckets']) && is_array($selectDatas['buckets'])) {
        
            $existed = [];
            
            foreach ($selectDatas['buckets'] as $item) {
                
                if (isset($item['key']) && !isset($existed[$item['key']])) {
                    $collection[$item['key']] = $item['key'];
                    
                    $existed[$item['key']] = $item['key'];
                }
            }
        }
    
        /*
        if ($selectName == 'marks') {
    
            dump($collection); exit;
        }
        */
        
        return $collection;
    }
    
    /**
     * @param string $brandName
     * @param int $size
     * @return array
     */
    public function getSelectModelsDataByBrand($brandName, $size = 1000000)
    {
        $index = $this->client->getIndex(self::INDEX);
        
        $query = new \Elastica\Query();
    
        $boolQuery = new \Elastica\Query\BoolQuery();
    
        $termsAgg = new \Elastica\Aggregation\Terms('models');
        $termsAgg->setField('modelPortail.raw');
        $termsAgg->setSize($size);
        $termsAgg->setOrder('_term', 'asc');
        
        $query->addAggregation($termsAgg);
        
        $term = new \Elastica\Query\Term();
        $term->setTerm('markPortail.raw', $brandName);
    
        $boolQuery->addFilter($term);
    
        $query->setQuery($boolQuery);
    
        $collection = [];
        $resultSet = $index->search($query);
        
        if ($resultSet->hasAggregations()) {
            
            foreach ($resultSet->getAggregations() as $aggregation => $result) {
                
                if (isset($result['buckets']) && is_array($result['buckets'])) {
                    
                    foreach ($result['buckets'] as $bucket) {
                        $collection[$bucket['key']] = ucfirst($bucket['key']);
                    }
                }
            }
        
        }
        
        return $collection;
    }

    /**
     * @return array
     */
    public function getMarks()
    {
        $marks = ['tops' => [], 'others' => []];
        $topBrandsOrder = Brand::TOP_BRAND_ORDER;
        $topBrandNames = $this->em->getRepository(Brand::class)->getTopBrandNames();
        $tops = [];

        foreach ($topBrandsOrder as $topBrandOrder) {
            foreach ($topBrandNames as  $field => $topBrandName) {
                if (isset($topBrandName['name']) &&  Urlizer::urlize($topBrandName['name']) == Urlizer::urlize($topBrandOrder)) {
                    continue;
                }
                if (count($this->getSelectModelsDataByBrand($topBrandName['name'])) > 0 ) {
                    $tops[$topBrandName['name']] = $topBrandName['name'];
                }
            }
        }

        $esBrands = $this->getSelectData('marks');

        $otherMarks = array_diff($esBrands, array_map('strtolower', $tops));
        sort($otherMarks);

        $marks = ['tops' => $tops, 'others' => $otherMarks];

        return $marks;
    }
}
