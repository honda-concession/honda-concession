<?php

namespace Honda\Bundle\SearchBundle\Repository;

use FOS\ElasticaBundle\Repository;
use Honda\Bundle\SearchBundle\Model\DistributorSearch;
use Elastica\Query\BoolQuery;
use Elastica\Query\MatchAll;
use Elastica\Query\Match;
use Elastica\Query\Terms;
use Elastica\Query\GeoDistance;
use Elastica\Query\Range;
use Elastica\Query\FunctionScore;
use Elastica\Query;

/**
 * SearchRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SearchRepository extends Repository
{

    public function searchDistributor(DistributorSearch $search, $page, $size, $host, $port)
    {
        $boolQuery = new BoolQuery();

        if ($search->getLocationRetailer()) {
            $localisation = $search->getLocationRetailer();

            switch ($localisation) {
                case '-none-':
                    $query = new MatchAll();
                    $boolQuery->addMust($query);
                    break;
                case 'region':
                    if ($search->getRegion() != null && $search != '') {
                        $boolQuery->addMust(new Terms('region', array($search->getRegion())));
                    } else {
                        $query = new MatchAll();
                        $boolQuery->addMust($query);
                    }
                    break;
                case 'department':
                    if ($search->getDepartment() != null && $search != '') {
                        $departments = $search->getDepartment();
                        $departmentChunks = explode(',', ltrim($departments, 0));
                        $boolQuery->addMust(new Terms('department', array_map('trim', $departmentChunks)));
                    } else {
                        $query = new MatchAll();
                        $boolQuery->addMust($query);
                    }
                    break;
                default:
                    $locationChunks = explode(',', $localisation);
                    $latChunk = str_replace(array('"', '}', '{'), '', $locationChunks[0]);
                    $lat = explode(':', trim($latChunk));

                    $longChunk = str_replace(array('"', '}', '{'), '', $locationChunks[1]);
                    $long = explode(':', trim($longChunk));

                    $filter = new GeoDistance('location', array('lat' => $lat[1], 'lon' => $long[1]), $search->getAccuracy());
                    $boolQuery->addMust($filter);
                    break;
            }

            if ($search->getAgentOfficiel() != null) {
                $boolQuery->addMust(new Terms('agentOfficiel', array(true)));
            }

            if ($search->getSpecialistCross() != null) {
                $boolQuery->addMust(new Terms('specialistCross', array(true)));
            }

            if ($search->getDistributorMotor() != null) {
                $boolQuery->addMust(new Terms('distributorMotor', array(true)));
            }
           
            $hits = new Query($boolQuery);
            //$hits->setSort(array('name' => array('order' => 'asc')));

            $hits->setFrom(($page - 1));
            $hits->setSize($size);
             
            $results['hits'] = $this->find($hits);
        }
        
        $elasticaClient = new \Elastica\Client(array(
            'host' => $host,
            'port' => $port
        ));
        $elasticaIndex = $elasticaClient->getIndex('honda_portail');
        $elasticaType = $elasticaIndex->getType('distributor');

        $elasticaSearch = new \Elastica\Search($elasticaClient);

        $countResults = $elasticaSearch->addIndex($elasticaIndex)->addType($elasticaType)->search($boolQuery)->getTotalHits();
        
        $results['count'] = $countResults;

        return $results;
    }

    public function searchVehicles($elasticaType, $request, $page, $size)
    {
        $boolQuery = new BoolQuery();

        if ($request->query->get('location-vehicle')) {

            $localisation = $request->query->get('location-vehicle');
            $region = $request->query->get('select-region-vehicle');
            $department = $request->query->get('select-departement-vehicle');
            $accuracy = $request->query->get('select-accuracy-vehicle');
            $brand = $request->query->get('brand');
            $type = $request->query->get('type');
            $model = $request->query->get('model');
            $engineMin = $request->query->get('engine-min');
            $engineMax = $request->query->get('engine-max');
            $priceMin = $request->query->get('price-min');
            $priceMax = $request->query->get('price-max');
            $permiA2 = $request->query->get('annonces-v');
            $demoVehicle = $request->query->get('special-v');

            switch ($localisation) {
                case '-none-':
                    $query = new MatchAll();
                    $boolQuery->addMust($query);
                    break;
                case 'region':
                    if ($region != null) {
                        $boolQuery->addMust(new Terms('distributor.region', array($region)));
                    } else {
                        $query = new MatchAll();
                        $boolQuery->addMust($query);
                    }
                    break;
                case 'department':
                    if ($department != null) {
                        $departments = $department;
                        $departmentChunks = explode(',', trim($departments));
                        $boolQuery->addMust(new Terms('distributor.department', array_map('trim', $departmentChunks)));
                    } else {
                        $query = new MatchAll();
                        $boolQuery->addMust($query);
                    }
                    break;
                default:
                    $locationChunks = explode(',', $localisation);
                    $latChunk = str_replace(array('"', '}', '{'), '', $locationChunks[0]);
                    $lat = explode(':', trim($latChunk));

                    $longChunk = str_replace(array('"', '}', '{'), '', $locationChunks[1]);
                    $long = explode(':', trim($longChunk));

                    $filter = new GeoDistance('distributor.location', array('lat' => $lat[1], 'lon' => $long[1]), $accuracy);
                    $boolQuery->addMust($filter);
                    break;
            }

            $query = new Match();

            if ($brand != null) {
                $boolQuery->addMust(new Terms('markPortail', array($brand)));
            }

            if ($type != null) {
                $boolQuery->addMust(new Terms('categoryPortail', array($type)));
            }

            if ($model != null) {
                $boolQuery->addMust(new Terms('modelPortail', array($model)));
            }

            if ($permiA2 != null) {
                $boolQuery->addMust(new Terms('drivingLicenseA2', array(true)));
            }

            if ($demoVehicle != null) {
                $boolQuery->addMust(new Terms('demoVehicle', array(true)));
            }

            if ($engineMin != null && $engineMax != null) {
                $boolQuery->addMust(new Range('cylinder', array('gte' => $engineMin, 'lte' => $engineMax, "boost" => 2.0)));
            }

            if ($priceMin != null && $priceMax != null) {
                $boolQuery->addMust(new Range('price', array('gte' => $priceMin, 'lte' => $priceMax, "boost" => 2.0)));
            }
        } else {
            $query = new MatchAll();
            $boolQuery->addMust($query);
        }

        $seed = time() . rand(10000, 20000);
        $randomScore = new FunctionScore();
        $randomScore->setRandomScore($seed);
        $boolQuery->addShould($randomScore);

        $hits = new Query($boolQuery);
        $hits->setFrom(($page - 1));
        $hits->setSize($size);


        $totalHits = $elasticaType->search($boolQuery)->getTotalHits();
        $results = $elasticaType->search($hits)->getResults();

        $data['hits'] = $results;
        $data['count'] = $totalHits;

        return $data;
        
        //https://int.portail.honda-occasions.kora.pro/search/vehicles/results?location-vehicle=-none-&engine-min=70.00&engine-max=2300.00&price-min=0.00&price
    }

}
