<?php

namespace Honda\Bundle\SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Honda\Bundle\DistributorBundle\Model\Region;
use Honda\Bundle\CommonBundle\Entity\SeoContent;

class VoController extends Controller
{

    /**
     * @Route("/occasion/{brand}", name="vo_brand_show", defaults={"brand": null})
     * @Route("/occasion/region/{region}", name="vo_region_show", defaults={"region": null})
     * @Route("/occasion/{brand}/{model}", name="vo_branch_model_show", defaults={"brand": null, "model": null})
     * @param Request $request
     * @param null $brand
     * @param null $region
     * @param null $model
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $brand = null, $region = null, $model = null)
    {
        $request->attributes->set('mark', $brand);
        $request->attributes->set('region', $region);
        $request->attributes->set('model', $model);
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(SeoContent::class);
        $seoPage = $this->container->get('sonata.seo.page');
        if ($brand) {
            $seoContent = $repository->findOneByType('Landing Page - Marque');
            $metaDescription = str_replace('[marque]', ucfirst(strtolower($brand)), $seoContent->getMetaDescription());
            $metaKeywords = str_replace('[marque]', ucfirst(strtolower($brand)), $seoContent->getMetaKeywords());
            $seoPage
                ->setTitle(str_replace('[marque]', ucfirst(strtolower($brand)), $seoContent->getTitle()))
                ->addMeta('name', 'description', $metaDescription)
                ->addMeta('keywords', 'keywords', $metaKeywords)
            ;
        } elseif ($region) {
            $regionName = $this->getRegions()[$region] ?? null;
            $seoContent = $repository->findOneByType('Landing Page - Region');
            $metaDescription = str_replace('[Région]', ucfirst(strtolower($regionName)), $seoContent->getMetaDescription());
            $metaKeywords = str_replace('[Région]', ucfirst(strtolower($regionName)), $seoContent->getMetaKeywords());
            $seoPage
                ->setTitle(str_replace('[Région]', ucfirst(strtolower($regionName)), $seoContent->getTitle()))
                ->addMeta('name', 'description', $metaDescription)
                ->addMeta('keywords', 'keywords', $metaKeywords)
            ;
        }

        return $this->render('@HondaSearch/Vo/show.html.twig');
    }

    /**
     * @return array|null
     *
     */
    protected function getRegions()
    {
        return array_flip(Region::REGIONS);
    }

}
