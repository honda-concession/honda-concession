<?php

namespace Honda\Bundle\SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DistributorController extends Controller
{

    /**
     * @Route("/concessionnaire", name="concessionnaire")
     */
    public function showAction(Request $request)
    {
        return $this->render('@HondaSearch/Distributor/show.html.twig', []);
    }
}
