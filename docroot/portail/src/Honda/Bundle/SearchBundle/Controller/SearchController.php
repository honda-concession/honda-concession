<?php

namespace Honda\Bundle\SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Honda\Bundle\SearchBundle\Model\DistributorSearch;
use Honda\Bundle\SearchBundle\Entity\Statistics;
use Honda\Bundle\SearchBundle\Model\SearchSelectData;

/**
 * @Route("/search", service="honda_search.controller.search")
 */
class SearchController extends Controller
{

    /**
     * @param Request $request
     * @Method({"GET"})
     * @Route("/distributors-search/{page}/{size}", defaults={"page"="1","size"="10"})
     *
     * @return array
     */
    public function searchDistributorsAction(Request $request, $page, $size)
    {
        $results = array();
        $limit = 10;
        $distributors = $this->findDistributors($request, $page, $size);

        foreach ($distributors['hits'] as $distributor) {
            $results[] = $distributor;
        }
        
        $counts = $distributors['count'];

        $query = '"location-retailer":"' . $request->get('location-retailer') . '",'
                . '"select-accuracy-retailer":"' . $request->get('select-accuracy-retailer') . '",'
                . '"select-region-retailer":"' . $request->get('select-region-retailer') . '",'
                . '"select-departement-retailer":"' . $request->get('select-departement-retailer') . '",'
                . '"official-c":"' . $request->get('official-c') . '",'
                . '"specialist-c":"' . $request->get('specialist-c') . '",'
                . '"bike-retailer-c":"' . $request->get('bike-retailer-c') . '",'
                . '"sort-retailer-c":"' . $request->get('sort-retailer-c') . '"'
        ;

        $end = false;
        if (($page + $size) > $counts) {
            $end = true;
        }

        return new JsonResponse([
            'html' => $this->renderView('HondaSearchBundle:Ajax:distributor.html.twig', array(
                "data" => $results,
                'count' => $counts,
                'page' => $page,
                'size' => $size,
                'limit' => $limit,
                'request' => $query,
                'end' => $end
                    )
            ),
            "endReached" => true,
        ]);
    }

    /**
     * @param Request $request
     * @Method({"GET"})
     * @Route("/search-vehicles/{page}/{size}", defaults={"page"="1","size"="10"})
     *
     * @return array
     */
    public function searchVehiclesAction(Request $request, $page, $size)
    {
        $vehiclesAds = $this->findVehicles($request, $page, $size);

        $counts = $vehiclesAds['count'];
        $models = [];
        $vehicles = [];
        $limit = 10;
        foreach ($vehiclesAds['hits'] as $result) {
            foreach ($result->getHit() as $data) {
                if (is_array($data)) {

                    $vehicles[] = $data;
                }
            }
        }

        $query = '"location-vehicle":"' . $request->get('location-vehicle') . '",'
                . '"select-accuracy-vehicle":"' . $request->get('select-accuracy-vehicle') . '",'
                . '"select-region-vehicle":"' . $request->get('select-region-vehicle') . '",'
                . '"select-departement-vehicle":"' . $request->get('select-departement-vehicle') . '",'
                . '"brand":"' . $request->get('brand') . '",'
                . '"type":"' . $request->get('type') . '",'
                . '"model":"' . $request->get('model') . '",'
                . '"engine-min":"' . $request->get('engine-min') . '",'
                . '"engine-max":"' . $request->get('engine-max') . '",'
                . '"price-min":"' . $request->get('price-min') . '",'
                . '"price-max":"' . $request->get('price-max') . '",'
                . '"annonces-v":"' . $request->get('annonces-v') . '",'
                . '"special-v":"' . $request->get('special-v') . '"'
        ;

        $end = false;
        if (($page + $size) > $counts) {
            $end = true;
        }

        if ($request->get('location-vehicle')) {
            foreach ($vehicles as $vehicle) {
                $models['name'] = $vehicle['title'];
                $models['link'] = 'https://' . $vehicle['distributor']['domain'] . '/occasion/' . $vehicle['slug'];
                $models['slug'] = $vehicle['slug'];
                $this->insertModels($models);
            }
            return new JsonResponse([
                'html' => $this->renderView('HondaSearchBundle:Ajax:vehicles.html.twig', array(
                    'data' => $vehicles,
                    'count' => $counts,
                    'page' => $page,
                    'size' => $size,
                    'limit' => $limit,
                    'request' => $query,
                    'end' => $end
                        )
                ),
                "endReached" => true,
            ]);
        } else {
            $query = '"location-vehicle":"-none-"';

            return $this->render('HondaSearchBundle:Ajax:vehicles.html.twig', array(
                        'data' => $vehicles,
                        'count' => $counts,
                        'page' => $page,
                        'size' => $size,
                        'limit' => $limit,
                        'request' => $query,
                        'end' => $end
                            )
            );
        }
    }

    /**
     * @param Request $request
     * @Route("/vehicles/results")
     * 
     * @return array
     */
    public function countSearchvehiclesResultAction(Request $request)
    {

        $results = $this->findVehicles($request, $page = 1, $size = 10);
        $validBrands = [];
        if (is_array($results)) {
            foreach ($results as $result) {
                if (is_array($result)) {
                    $obj = end($result);
                } elseif (is_object($result)) {
                    $obj = $result;
                } else {
                    continue;
                }
            }
            if ($obj) {
                if ($source = $obj->getSource()) {
                    if (isset($source['markPortail'])) {
                        $validBrands = $source['markPortail'];
                    }
                }
            }
        }
        if ($request->get('location-vehicle')) {
            return new JsonResponse([
                'results' => $results['count'],
                'validBrand' => $validBrands,
            ]);
        } else {
            return $this->render('HondaSearchBundle:Default:search-count-v.html.twig', [
                'results' => $results['count'],
                'validBrand' => $validBrands,
            ]);
        }
    }

    /**
     * @param Request $request
     * @Route("/distributors/results")
     * 
     * @return array
     */
    public function countSearchDistributorsResultAction(Request $request)
    {

        $results = $this->findDistributors($request, $page = 1, $size = 10);
        if ($request->get('location-retailer')) {
            return new JsonResponse([
                'results' => $results['count']
            ]);
        } else {
            return $this->render('HondaSearchBundle:Default:search-count-c.html.twig', array(
                        'results' => $results['count']
                            )
            );
        }
    }
    
    /**
     * @param $request
     * @param $page
     * @param $size
     * @param bool $sortByName
     * @return mixed
     */
    public function findDistributors($request, $page, $size)
    {
        $distributorSearch = new DistributorSearch();
        $distributorSearch->locationRetailer = $request->query->get('location-retailer');
        $distributorSearch->accuracy = $request->query->get('select-accuracy-retailer');
        $distributorSearch->region = $request->query->get('select-region-retailer');
        $distributorSearch->department = $request->query->get('select-departement-retailer');
        $distributorSearch->agentOfficiel = $request->query->get('official-c');
        $distributorSearch->specialistCross = $request->query->get('specialist-c');
        $distributorSearch->distributorMotor = $request->query->get('bike-retailer-c');
        $distributorSearch->sortByName = $request->query->get('sort-retailer-c');

        $elasticaManager = $this->container->get('fos_elastica.manager');
        $host = $this->getParameter('elasticsearch_host');
        $port = $this->getParameter('elasticsearch_port');

        $distributors = $elasticaManager->getRepository('HondaDistributorBundle:Distributor')->searchDistributor($distributorSearch, $page, $size, $host, $port);

        return $distributors;
    }

    /**
     * @param $request
     * 
     * @return array
     */
    public function findVehicles($request, $page, $size)
    {
        $elasticaClient = new \Honda\Bundle\CommonBundle\Search\Client(array(
            'host' => $this->getParameter('elasticsearch_host'),
            'port' => $this->getParameter('elasticsearch_port')
        ));
        
        $elasticaIndex = $elasticaClient->getIndex('honda');
        $elasticaType = $elasticaIndex->getType('vehiclead');
        
        $elasticaManager = $this->container->get('fos_elastica.manager');
        $vehiclesAds = $elasticaManager->getRepository('HondaDistributorBundle:Distributor')->searchVehicles($elasticaType, $request, $page, $size);
        
        return $vehiclesAds;
    }

    /**
     * @param $models
     * 
     * @return array
     */
    public function insertModels($models)
    {
        $em = $this->getDoctrine()->getManager();
        $model = $em->getRepository(Statistics::class)->findOneBy(['slug' => $models['slug']]);

        if ($model) {
            $model->setCount($model->getCount() + 1);
            $em->persist($model);
            $em->flush();
        } else {
            $stats = new Statistics();
            $stats->setModel($models['name']);
            $stats->setLink($models['link']);
            $stats->setSlug($models['slug']);
            $stats->setCount(1);
            $em->persist($stats);
            $em->flush();
        }
    }
    
    /**
     * @param Request $request
     * @return JsonResponse
     * @Method("GET")
     * @Route("/models-options", name="models_options")
     *
     */
    public function modelsListOptionstAction(Request $request)
    {
        $searchSelectData = $this->get(SearchSelectData::class);
        $modelsDataFromEs = $searchSelectData->getSelectModelsDataByBrand($request->query->get('brand'));
    
        $results = [];
        $default = ['label' => 'Sélectionner', 'value' => ''];

        foreach ($modelsDataFromEs as $modelNameToken => $modelName) {
            
            $item = [];
            
            $item['label'] = ucfirst($modelNameToken);
            $item['value'] = $modelNameToken;
            
            $results[] = $item;
        }
        
        asort($results);
        
        array_unshift($results, $default);
        
        return $this->json($results);
    }

    /**
     * @Route("/brand-options")
     * 
     * @return array
     */
    public function brandListOptionsAction()
    {

        $brands = $this->getDoctrine()->getRepository('HondaVehiclesOccasionBundle:Brand')->getAll();

        return $this->render('HondaSearchBundle:Default:brands.html.twig', array(
                    'brands' => $brands
                        )
        );
    }

    /**
     * @Route("/top-search")
     * 
     * @return array
     */
    public function topSearchAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Statistics::class);
        $results = $repo->createQueryBuilder('m')
                ->orderBy('max(m.count)', 'DESC')
                ->groupBy('m.id')
                ->setMaxResults( 8 )
                ->getQuery()
                ->getResult();
        
        return $this->render('HondaSearchBundle:Default:topsearch.html.twig', array(
                    'results' => $results
                        )
        );
    }
    
    /**
     * @Route("/type-options")
     * 
     * @return array
     */
    public function typeListOptionsAction()
    {

        $types = $this->getDoctrine()->getRepository('HondaVehiclesOccasionBundle:CategoryVo')->getAll();

        return $this->render('HondaSearchBundle:Default:type.html.twig', array(
                    'types' => $types
                        )
        );
    }

}
