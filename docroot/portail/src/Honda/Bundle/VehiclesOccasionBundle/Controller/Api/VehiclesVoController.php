<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Controller\Api;

use Honda\Bundle\VehiclesOccasionBundle\Entity\Banner;
use Honda\Bundle\VehiclesBundle\Entity\SubModels;
use Honda\Bundle\VehiclesOccasionBundle\Entity\Color;
use Honda\Bundle\VehiclesOccasionBundle\Entity\Infos;
use Honda\Bundle\VehiclesOccasionBundle\Entity\Products;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VehiclesVoController extends Controller
{
    /**
     * @Route("/api/vo/list/brand")
     * @Method("GET")
     *
     */
    public function listBrandAction()
    {
        $brands = $this->getDoctrine()->getRepository('HondaVehiclesOccasionBundle:Brand')->findBy(
            [],
            ['topBrand' => 'DESC', 'name' => 'ASC']
        );

        $data = [];
        foreach($brands as $key => $brand){
            $data[$key]['id'] = $brand->getId();
            $data[$key]['title'] = $brand->getName();
            $data[$key]['slug'] = $brand->getSlug();
            $data[$key]['topmark'] = $brand->getTopBrand();
        }

        return new JsonResponse($data);

    }

    /**
     * @Route("/api/vo/list/submodel")
     * @Method("GET")
     *
     */
    public function listSubModelsAction()
    {

        $collection = $this->getDoctrine()->getRepository(SubModels::class)->findAll();

        $data = [];
        foreach($collection as $key => $subModel){
            $data[$key]['id'] = $subModel->getId();
            $data[$key]['title'] = $subModel->getName();
        }

        return $this->json($data);
    }

    /**
     * @Route("/api/vo/details/model/{modelName}")
     * @Method("GET")
     */
    public function modelDetailsAction($modelName)
    {
        $model = $this->getDoctrine()->getRepository('HondaVehiclesOccasionBundle:Models')->findOneByName($modelName);
        if (!$model) {
            return new JsonResponse([]);
        }

        return new JsonResponse([
            'id' => $model->getId(),
            'name' => $model->getName(),
            'cylinder' => $model->getCylinder(),
            'category' => $model->getCategory() ? $model->getCategory()->getName() : null,
        ]);
    }

    /**
     * @Route("/api/vo/list/model")
     * @Method("GET")
     *
     */
    public function listModelsAction()
    {

        $models = $this->getDoctrine()->getRepository('HondaVehiclesOccasionBundle:Models')->findByPublished(true);

        $data = [];
        foreach($models as $key => $model){
            $data[$key]['id'] = $model->getId();
            $data[$key]['title'] = $model->getName();
            $data[$key]['slug'] = $model->getSlug();
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/vo/list/category")
     * @Method("GET")
     *
     */
    public function listCategoryAction()
    {

        $categories = $this->getDoctrine()->getRepository('HondaVehiclesOccasionBundle:CategoryVo')->findBy(
            [],
            ['name' => 'ASC']
        );

        $data = [];
        foreach($categories as $key => $category){
            $data[$key]['id'] = $category->getId();
            $data[$key]['category'] = $category->getName();
            $data[$key]['category_slug'] = $category->getSlug();
        }

        return new JsonResponse($data);

    }

    /**
     * @param $brand
     * @Method("GET")
     * @Route("api/vo/list/model/{brand}")
     *
     * @return array
     */
    public function modelsListOptionstAction($brand)
    {

        $em = $this->getDoctrine()->getManager();
        $modelsList = $em
                ->getRepository('HondaVehiclesOccasionBundle:Models')
                ->createQueryBuilder('m')
                ->leftJoin('m.brand', 'b')
                ->where('b.name LIKE :brandName')
                ->orderBy('m.name', 'ASC')
                ->setParameter('brandName', $brand)
                ->getQuery()
                ->getResult();

        $results = [];
        $uniq = [];
        foreach ($modelsList as $key => $models) {

            $name = $models->getName();

            if (isset($uniq[$name])) {
                continue;
            }

            $results[$key]['label'] = $name;
            $results[$key]['value'] = $name;

            $uniq[$name] = $name;
        }

        return new JsonResponse($results);
    }

    /**
     * @Route("/api/vo/list/banner")
     * @Method("GET")
     *
     */
    public function voListBannerAction(Request $request)
    {
        $banner  = $this->getDoctrine()->getRepository(Banner::class)->findOneBy(['id' => 1]);

        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        if (!$banner) {
            throw new NotFoundHttpException();
        }

        $data = [];
        $data['banner'] = $bannerUrl = null;
        $data['link'] =  $banner->getLink();
        $data['externalLink'] =  $banner->getLinkExternal();

        if($banner->getImage()){
            $bannerImage = $banner->getImage();
            $providerImage = $this->container->get($bannerImage->getProviderName());
            $bannerUrl = $baseurl . $providerImage->generatePublicUrl($bannerImage, 'reference');
            $data['banner'] = $bannerUrl;
        }

        return new JsonResponse($data);
    }

    /**
     *  @Route("/api/vo/list/color", name="api_get_colors", methods={"GET"})
     */
    public function getColorsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $colors = $em->getRepository(Color::class)->getColors();

        return $this->json($colors);
    }

    /**
     *  @Route("/api/vo/infos-complementaires", name="api_get_infos_complementaires", methods={"GET"})
     */
    public function getInfosComplementairesAction()
    {
        $data = [];
        $em = $this->getDoctrine()->getManager();

        $collection = $em->getRepository(Infos::class)->findAll([], ['name', 'ASC']);

        foreach ($collection as $item) {
            $data[$item->getName()] = $item->getName();
        }

        return $this->json($data);
    }

    /**
     *  @Route("/api/vo/plus-produits", name="api_get_plus_produits", methods={"GET"})
     */
    public function getPlusProduitsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $data = $em->getRepository(Products::class)->findAll([], ['name', 'ASC']);

        return $this->json($data);
    }
}
