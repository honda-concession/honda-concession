<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Controller\Admin;

use Honda\Bundle\VehiclesOccasionBundle\Http\VehicleAdGateway;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CRUDController;


class VehicleAdAdminController extends CRUDController
{
    
    /**
     * @return Response
     */
    public function listAction()
    {
        $vehicleAds = $this->get(VehicleAdGateway::class)->getVehicleAds();
        
       //dump($vehicleAds); exit;
        
        return $this->renderWithExtraParams('HondaVehiclesOccasionBundle:Admin:sonata_vehicleAd_list.html.twig', [
            'vehicleAds' => $vehicleAds,
        ]);
    }

}
