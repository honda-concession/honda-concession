<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ExtractController
 * @package Honda\Bundle\VehiclesOccasionBundle\Controller\Admin
 */
class ExtractCRUDController extends CRUDController
{
    public function listAction()
    {
        return $this->renderWithExtraParams('@HondaVehiclesOccasion/Admin/extract.html.twig', []);
    }

    public function generateAction()
    {
        $application = new Application($this->get('kernel'));
        $application->setAutoExit(false);
        $input = new ArrayInput(array(
            'command' => 'honda:occasion:export',
            '--env' => 'prod'
         ));

        $output = new NullOutput();
        $application->run($input, $output);

        return new RedirectResponse($this->admin->generateUrl('list', []));
    }

    public function downloadAction()
    {
        $exportPath = realpath($this->get('kernel')->getRootDir().'/../web/extract');
        $filename = $this->getParameter('winteam_filename');
        $finder = new Finder();
        $finder->files()
            ->in($exportPath)
            ->name('*.csv')
            ->filter(function(\SplFileInfo $file) use ($filename) {
                $pattern = "/^{$filename}$/";

                return (bool)preg_match($pattern, $file->getFilename());
            });

        foreach($finder as $file) {
            return $this->download($file->getFilename());
        }

        return new Response('file not found');
    }

    private function download(string $filename)
    {
        $file = realpath($this->get('kernel')->getRootDir().'/../web/extract')."/{$filename}";
        $response = new BinaryFileResponse($file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }
}
