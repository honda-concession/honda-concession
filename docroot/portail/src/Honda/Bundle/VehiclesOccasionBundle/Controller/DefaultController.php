<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DefaultController extends Controller
{
    /**
     * @Route("/extract/winteam", name="extract_winteam")
     */
    public function indexAction()
    {
        try {
            $filename = $this->getParameter('winteam_filename');
            $file = realpath($this->container->get('kernel')->getRootDir().'/../web/extract')."/{$filename}";
            $response = new BinaryFileResponse($file);
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $response;
    }
}
