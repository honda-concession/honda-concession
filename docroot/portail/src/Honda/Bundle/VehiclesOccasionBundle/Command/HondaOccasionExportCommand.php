<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Honda\Bundle\CommonBundle\Helper\AES;

class HondaOccasionExportCommand extends ContainerAwareCommand
{

    private $delimiter = ';';
    private $imageSeparator = '*****';
    private $description = 'Creating vehicle ads';
    private $directory = 'web/extract';
    private $filename = 'winteam.csv';
    private $exportPath;

    protected function configure()
    {
        $this
            ->setName('honda:occasion:export')
            ->setDescription($this->description)
            ->addArgument('directory', InputArgument::OPTIONAL, 'Directory to extract')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        if ($directory = $input->getArgument('directory')) {
            $this->directory = $directory;
        }
        $io->title("{$this->description} into {$this->directory}");
        $client = $this->getContainer()->get('fos_elastica.client.default');
        $index = $client->getIndex('honda');
        $type = $index->getType('vehiclead');
        $count = $type->count();
        if ($count < 0) {
            $io->error('No vehicle ads found');
            return;
        }
        $query = [
            'from' => 0,
            'size' => $count,
        ];
        $results = $type->search($query)->getResults();
        $this->filename = $this->getContainer()->getParameter('winteam_filename');
        $this->exportPath = realpath($this->getContainer()->get('kernel')->getRootDir().'/..')."/{$this->directory}/{$this->filename}";
        if (!empty($results)) {
            $this->writeCsv(self::getHeaders(), $results, $io);
        }
    }

    private function writeCsv($headers, $results, $io)
    {
        $handle = fopen($this->exportPath, 'w');
        fputcsv($handle, $headers, $this->delimiter);
        $io->progressStart(count($results));
        $logger = $this->getContainer()->get('logger');
        foreach ($results as $result) {
            try {
                $adsUrl = null;
                $images = null;
                $createdAt = null;
                $releaseDate = null;
                $updatedAt = null;
                $distributor = null;
                $vehicleAd = $result->getSource();
                if (isset($vehicleAd['distributor'])) {
                    $distributor = $this->getDistributor($vehicleAd['distributor']['distributorPortailId']);
                    if (!$distributor || !$distributor->isVehicleAdExportActivated()) {
                        continue;
                    }
                }
                if (isset($vehicleAd['vehicleAdGalleries'])) {
                    $images = $this->getImages($vehicleAd['vehicleAdGalleries'], $distributor ? $distributor : $vehicleAd['distributor']);
                    $videos = $this->getVideoUrl($vehicleAd['vehicleAdGalleries']);
                }
                if (isset($vehicleAd['slug'])) {
                    $adsUrl = $this->getAdsUrl($vehicleAd['slug'], $distributor ? $distributor : $vehicleAd['distributor']);
                }
                $values = [
                    $vehicleAd['id'],
                    self::strip($vehicleAd['title']),
                    self::strip($vehicleAd['markPortail']),
                    self::strip($vehicleAd['modelPortail']),
                    self::strip($vehicleAd['modelYear']),
                    self::strip($vehicleAd['price']),
                    self::strip($vehicleAd['km']),
                    self::strip($vehicleAd['description']),
                    self::strip($vehicleAd['vehicleAdDesc']),
                    self::strip($vehicleAd['color']),
                    self::strip($vehicleAd['specEnergie']),
                    self::strip($vehicleAd['specBoiteVitesse']),
                    self::strip($vehicleAd['options']),
                    trim($vehicleAd['infosComplementaires']) == 'Première main' ? 1 : 0,
                    isset($vehicleAd['plusProduits']) ? $this->isPermitA2($vehicleAd['plusProduits']) : 0,
                    trim($vehicleAd['infosComplementaires']) == 'Véhicle de démonstration' ? 1 : 0,
                    self::strip($vehicleAd['refPa']),
                    self::strip($vehicleAd['warrant']),
                    $distributor ? self::strip($distributor->getName()) : null,
                    $distributor ? $distributor->getId() : $vehicleAd['id'],
                    $distributor ? self::strip($distributor->getDcmsid()) : null,
                    isset($vehicleAd['releaseDate']) ? self::strip($this->getFormattedDate($vehicleAd['releaseDate'])) : null,
                    self::strip($vehicleAd['cylinder']),
                    self::strip($vehicleAd['categoryPortail']),
                    self::strip($images),
                    self::strip($videos),
                    self::strip($adsUrl),
                    '1',
                    isset($vehicleAd['createdAt']) ? self::strip($this->getFormattedDate($vehicleAd['createdAt'])) : null,
                    isset($vehicleAd['updatedAt']) ? self::strip($this->getFormattedDate($vehicleAd['updatedAt'])) : null,
                    $distributor ? $this->getStatus($vehicleAd, $distributor) : 0,
                ];
                fputcsv($handle, $values, $this->delimiter);
            } catch(\Exception $e) {
                $logger->error($e->getMessage());
                continue;
            } finally {
                $io->progressAdvance();
            }
        }
        fclose($handle);
        $this->addQuotes($this->exportPath);
        $io->progressFinish();
    }

    private function getDistributor($distributorPortailId)
    {
        $container = $this->getContainer();
        $entityManager = $container->get('doctrine.orm.entity_manager');

        return $entityManager->getRepository(Distributor::class)->find($distributorPortailId);
    }

    private function getAdsUrl($slug, $distributor = null)
    {
        if ($distributor instanceof Distributor) {
            return "https://{$distributor->getWebsiteUrl()}/occasion/{$slug}";
        } elseif (is_array($distributor)) {
            return "https://{$distributor['domain']}{$slug}";
        }

        return $slug;
    }

    private function getImages($vehicleGalleries, $distributor = null)
    {
        $images = [];
        if ($mainImage = $this->getMainImage($vehicleGalleries, $distributor)) {
            $images[] = $mainImage;
        }
        foreach($vehicleGalleries as $vehicleGallery) {
            if (!$vehicleGallery['isMainImage'] && $vehicleGallery['mediaUrl']) {
                $images[] = $distributor ? $this->getImageUrl($vehicleGallery['mediaUrl'], $distributor) : $vehicleGallery['mediaUrl'];
            }
        }

        return implode($this->imageSeparator, $images);
    }

    private function getMainImage($vehicleGalleries, $distributor = null)
    {
        foreach ($vehicleGalleries as $vehicleGallery) {
            if ($vehicleGallery['isMainImage'] && $vehicleGallery['mediaUrl']) {
                return $distributor ? $this->getImageUrl($vehicleGallery['mediaUrl'], $distributor) : $vehicleGallery['mediaUrl'];
            }
        }

        return null;
    }

    private function getVideoUrl($vehicleGalleries) {
        $videoUrl = [];
        foreach ($vehicleGalleries as $vehicleGallery) {
            $path = $vehicleGallery['reference'];
            if (filter_var($path, FILTER_VALIDATE_URL) && preg_match('/youtube\.com\/watch/', $path)) {
                $videoUrl[] = $path;
            }
        }

        return implode($this->imageSeparator, $videoUrl);
    }

    private function getImageUrl(string $path, $distributor = null)
    {
        if (filter_var($path, FILTER_VALIDATE_URL)) {
            return null;
        }
        if ($distributor instanceof Distributor) {
            $path = "https://{$distributor->getWebsiteUrl()}{$path}";
        } elseif (is_array($distributor)) {
            $infos = $distributor['infos'];
            $path = "https://{$infos->url}/{$path}";
        }
        $path = preg_replace('/_medium/', '_big', $path);

        return $path;
    }

    private function isPermitA2(array $infos)
    {
        foreach ($infos as $info) {
            return $info === 'Permis A2' ? 1 : 0;
        }

        return 0;
    }

    private function getFormattedDate($dateAsString, $format = 'Y-m-d H:i:s')
    {
        $datetime = \DateTime::createFromFormat(\DateTime::ISO8601, $dateAsString);
        if ($datetime instanceof \DateTime) {
            return $datetime->format($format);
        }

        return null;
    }

    private function getStatus($vehicleAd, Distributor $distributor)
    {
        $diffusions = $distributor->getDiffusion();
        if (!$diffusions) {
            return 0;
        }
        foreach($diffusions as $diffusion) {
            $site = $diffusion->getSites();
            if (!$site) {
                continue;
            }
            if ($site->getName() != 'Le Bon Coin') {
                continue;
            }
            if ($diffusion->getQuotas() > 0) {
                return $vehicleAd['publishedOnLeBonCoin'] ? 1 : 0;
            }
        }

        return 0;
    }

    private function addQuotes($path)
    {
        $content = '';
        if ($handle = fopen($path, 'r')) {
            while(!feof($handle)) {
                $line = fgets($handle);
                if ($line) {
                    $content .= preg_replace('/(")?;(")?/', '";"', '"'.preg_replace('/(\r?\n)+/', "\"\r\n", $line));
                }
            }

            fclose($handle);
        }
        file_put_contents($path, $content);
    }

    private static function getHeaders()
    {
        return [
            'Id',
            'Titre',
            'Marque',
            'Modéle',
            'Année',
            'Prix',
            'Km',
            'Description',
            'Description Annonce',
            'Couleur',
            'Energie',
            'Boîte de vitesse',
            'Options',
            'Première main',
            'Permis',
            'Véhicle de démonstration',
            'Référence',
            'Garantie',
            'Concessionnaire',
            'Concess Id',
            'DCMS ID',
            'Mise en circulation',
            'Cylindree',
            'Catégorie',
            'Image',
            'Video',
            'Lien',
            'LEBONCOIN',
            'Date création',
            'Date de mise a jour',
            'Statut',
        ];
    }

    private static function strip($text)
    {
        $strippedText = trim(strip_tags(html_entity_decode($text)));
        $strippedText = str_replace(';', null, $strippedText);

        return preg_replace('/(\r?\n)+/', '|', $strippedText);
    }

    private function createZip()
    {
        $zip = new \ZipArchive();
        if ($zip->open(preg_replace('/csv$/', 'zip', $this->exportPath), \ZipArchive::CREATE) === TRUE) {
            $zip->addFile($this->exportPath, $this->filename);
            $zip->setEncryptionIndex(0, \ZipArchive::EM_AES_256, 'PjLq4dVpFupp4ShL');
            $zip->close();

            unlink($this->exportPath);

            return true;
        }

        return false;
    }
}
