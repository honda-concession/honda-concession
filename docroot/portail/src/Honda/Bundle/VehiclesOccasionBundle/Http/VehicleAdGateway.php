<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Http;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;

class VehicleAdGateway
{

    /**
     * @var array
     */
    private $endpoints;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $env;

    /**
     * @var Client
     */
    private $client;

    public function __construct($endpoints, $key, $env)
    {
        $this->endpoints = $endpoints;
        $this->key = $key;
        $this->env = $env;
        $this->client = new Client(['timeout' => 30]);
    }

    /**
     * List site distributor
     *
     * @param bool $displayArray
     * @return array
     */
    public function getVehicleAds($displayArray = false)
    {

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        try {
            $res = $this->client->request('GET', $this->endpoints['vehicleAd_list'], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }

        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {

            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, $displayArray);

                return $responseData;
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
        }

        return [];
    }
}
