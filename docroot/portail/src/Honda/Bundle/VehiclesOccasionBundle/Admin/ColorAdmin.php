<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;

class ColorAdmin extends AbstractAdmin
{

    use ActivationAdminTrait;

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null , array (
                    'label' => 'Nom de la couleur'
                ))
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null , array (
                    'label' => 'Nom de la couleur'
                ))
        ;
    }
}
