<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class BannerAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('image', 'string', [
                    'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig',
                    'label' => 'Banniere',
                ])
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('image', 'sonata_type_model_list', [
                    'label' => 'Image',
                    'by_reference' => true,
                    'required' => false,
                    'btn_delete' => false,
                        ], [
                    'link_parameters' => [
                        'context' => 'image',
                    ]
                      
                ])
                ->add('link')
                ->add('linkExternal')
        ;
    }
    
    protected function configureBatchActions($actions)
    {

        unset($actions['delete']);

        return $actions;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list')->remove('delete')->remove('create');
    }
    
    
    public function toString($object)
    {
        return "Images sur accueil VO des concessionnaires";
    }
}