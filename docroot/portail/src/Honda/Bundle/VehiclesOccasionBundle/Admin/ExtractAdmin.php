<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class ExtractAdmin
 * @package Honda\Bundle\VehiclesOccasionBundle\Admin
 */
class ExtractAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'extract';
    protected $baseRouteName = 'extract';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        $collection->add('download');
        $collection->add('generate');
    }
}
