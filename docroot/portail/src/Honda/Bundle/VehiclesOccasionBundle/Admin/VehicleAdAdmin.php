<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class VehicleAdAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'vehicle-ads';
    protected $baseRouteName = 'vehicle_ads';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
}
