<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Admin;

use Symfony\Component\Form\Extension\Core\Type As Type;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;


class SliderAdmin extends AbstractAdmin
{

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        
        $listMapper
            ->add('position_', 'actions', array(
                'actions' => array(
                    'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig')
                )
            ))
            ->add('media', 'string', array('template' => '@SonataMedia/MediaAdmin/list_image.html.twig'))
            ->add('startDate', 'datetime', ['format'=>'d/m/Y'])
            ->add('endDate', 'datetime', ['format'=>'d/m/Y'])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
      
        $formMapper
            ->with('Media',  ['class' => 'col-md-8'])
                ->add('media', ModelListType::class, [
                    'label' => 'Media',
                    'required' => false,
                ], [
                        'link_parameters' => ['context' => 'slider_vo']
                    ]
                )
                ->add('link', Type\TextType::class, ['required' => false] )
                ->add('linkExternal', Type\CheckboxType::class, ['required' => false] )
            ->end()
            
            ->with('Durée', ['class' => 'col-md-4'])
                ->add('startDate', DatePickerType::class, ['format'=>'dd/MM/yyyy H:mm:ss'])
                ->add('endDate', DatePickerType::class, ['format'=>'dd/MM/yyyy H:mm:ss', 'required' => false])
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('startDate', 'datetime', ['format'=>'d/m/Y H:i:s'])
            ->add('endDate', 'datetime', ['format'=>'d/m/Y H:i:s'])
            ->add('url')
            ->add('createdAt', 'datetime', ['format'=>'d/m/Y H:i:s'])
            ->add('updatedAt', 'datetime', ['format'=>'d/m/Y H:i:s'])
        ;
    }
}
