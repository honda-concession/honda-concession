<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Validator\Constraints as Assert;

use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;

class ModelsAdmin extends AbstractAdmin
{

    use ActivationAdminTrait;

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name', null, array(
                    'label' => 'Modèle'
                ))

        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('name', null , array (
                    'label' => 'Nom du modèle'
                ))
                ->add('brand', null , array (
                    'label' => 'Marque'
                ))
                ->add('cylinder', null, array(
                    'label' => 'Cylindrée'
                ))
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->with('Modèles', array('class' => 'col-md-12'))
                ->add('name', null, array (
                    'label' => 'Modèle'
                ))
                ->add('brand', null , array (
                    'label' => 'Marque',
                    'required' => true
                ))
                ->add('category', null , array (
                    'label' => 'Type'
                ))
                ->add('cylinder', null, array(
                    'attr' => ["class" => "cylinder-formfield"],
                    'label' => 'Cylindrée',
                    'sonata_help' => '<span>cm³</span>',
                ))
                ->end()
        ;
    }

    /**
     * @inheritdoc
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
                ->with('cylinder')
                ->assertNotBlank()
                ->addConstraint(new Assert\Regex('/^[0-9]*$/'))
                ->end()
                ;
    }

}
