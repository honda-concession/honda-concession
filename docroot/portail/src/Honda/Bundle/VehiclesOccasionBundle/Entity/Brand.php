<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;

/**
 * Brand
 *
 * @ORM\Table(name="brand")
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesOccasionBundle\Repository\BrandRepository")
 */
class Brand
{

    use ActivationTrait;

    const TOP_BRAND_ORDER = ['Honda', 'Yamaha', 'Suzuki', 'Kawasaki', 'KTM', 'MBK', 'Peugeot', 'Aprilia', 'BMW', 'Ducati', 'Harley-Davidsion', 'Piaggio', 'Triumph'];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

   /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug",length=128, type="string", unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="top_brand", type="boolean", nullable=true, options={"default":"0"})
     */
    protected $topBrand;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return SubModels
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

    /**
     * @return string
     */
    public function getTopBrand()
    {
        return $this->topBrand;
    }

    /**
     * @param string $topBrand
     * @return Brand
     */
    public function setTopBrand($topBrand)
    {
        $this->topBrand = $topBrand;

        return $this;
    }

}
