<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;

/**
 * Models
 *
 * @ORM\Table(name="models_vo")
 * @ORM\Entity(repositoryClass="Honda\Bundle\VehiclesOccasionBundle\Repository\ModelsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Models
{

    use ActivationTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="CategoryVo")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="cylinder", type="string", length=255)
     */
    private $cylinder;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug",length=128, type="string")
     */
    private $slug;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Models
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

    /**
     * Set brand.
     *
     * @param \Honda\Bundle\VehiclesOccasionBundle\Entity\Brand|null $brand
     *
     * @return Models
     */
    public function setBrand(\Honda\Bundle\VehiclesOccasionBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return \Honda\Bundle\VehiclesOccasionBundle\Entity\Brand|null
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set category.
     *
     * @param \Honda\Bundle\VehiclesOccasionBundle\Entity\CategoryVo|null $category
     *
     * @return Models
     */
    public function setCategory(\Honda\Bundle\VehiclesOccasionBundle\Entity\CategoryVo $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \Honda\Bundle\VehiclesOccasionBundle\Entity\CategoryVo|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set cylinder.
     *
     * @param string $cylinder
     *
     * @return Models
     */
    public function setCylinder($cylinder)
    {
        $this->cylinder = $cylinder;

        return $this;
    }

    /**
     * Get cylinder.
     *
     * @return string
     */
    public function getCylinder()
    {
        return $this->cylinder;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @ORM\PrePersist
     */
    public function setPublishedValue()
    {
        $this->published = true;
    }
}
