<?php

namespace Honda\Bundle\VehiclesOccasionBundle\Repository;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryVoRepository extends \Doctrine\ORM\EntityRepository
{
    
    /**
     * @return mixed
     */
    public function getAll()
    {
        $qb = $this->createQueryBuilder('this');
    
        $qb
            ->orderBy('this.name', 'ASC')
            ;
        
        return  $qb->getQuery()->getResult();
    }
    
}
