<?php

namespace Honda\Bundle\DistributorBundle\Doctrine;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Mapping as ORM;

use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Honda\Bundle\DistributorBundle\Http\DistributorGateway;
use Honda\Bundle\DistributorBundle\Model\SubGroupManager;
use Honda\Bundle\CommonBundle\EventListener\Encryption;

/**
 * Class DistributorListener
 * @package Honda\Bundle\DistributorBundle\Doctrine
 */
class DistributorListener
{
    
    protected $subGroupManager;
    
    /**
     * @var DistributorGateway
     */
    protected $distributorGateway;
    
    protected $encryption;

    /**
     * DistributorListener constructor.
     * @param SubGroupManager $subGroupManager
     */
    public function __construct(SubGroupManager $subGroupManager, DistributorGateway $distributorGateway, Encryption $encryption)
    {
        $this->subGroupManager = $subGroupManager;
        $this->distributorGateway = $distributorGateway;
        $this->encryption = $encryption;
    }
    
    /**
     * @param Distributor $distributor
     * @param PreFlushEventArgs $event
     *
     * @ORM\PreFlush()
     */
    public function setDefaultSubGroup(Distributor $distributor,  PreFlushEventArgs $event)
    {
        $defaultSubGroup = $this->subGroupManager->getDefaultSubGroup();
        if ($defaultSubGroup) {
            $distributor->addSubGroup($defaultSubGroup);
        }
    }
    
    /**
     * @param Distributor $distributor
     * @param LifecycleEventArgs $event
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     *
     */
    public function updateDistributorSite(Distributor $distributor, LifecycleEventArgs $event)
    {
        $data = [];
        $this->encryption->decryptEntity($distributor);
        $data['id'] = $distributor->getId();
        $data['name'] = $distributor->getName();
        $data['address'] = $distributor->getAddress1();
        $data['other_address'] = $distributor->getAddress2();
        $data['cp'] = $distributor->getCodePostal();
        $data['town'] = $distributor->getCity();
        $data['lat'] = $distributor->getLatitude();
        $data['lng'] = $distributor->getLongitude();
        $data['location'] = $distributor->getLocation();
        $data['department'] = $distributor->getDepartment();
        $data['region'] = $distributor->getRegion();
        $data['url'] = $distributor->getWebsiteUrl();
        $data['phone'] = $distributor->getTelephone();
        $data['fax'] = $distributor->getFax();
        $data['mail'] = $distributor->getEmail();
        $data['dcmsid'] = $distributor->getDcmsid();
        $data['gaCode'] = $distributor->getGaCode();
        $data['dataStudioId'] = $distributor->getDataStudioId();

        $this->distributorGateway->getUpdateDistributor($distributor->getId(), $data);
    }
}
