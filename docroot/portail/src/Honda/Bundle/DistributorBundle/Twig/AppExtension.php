<?php

namespace Honda\Bundle\DistributorBundle\Twig;

use Doctrine\Common\Persistence\ObjectManager;
use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;


class AppExtension extends AbstractExtension
{
    
    /**
     * @var ObjectManager
     */
    protected $em;
    
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }
    
    public function getFunctions()
    {
        return [
            new TwigFunction('getDistributorName', [$this, 'getDistributorName']),
        ];
    }
    
    /**
     * @param $distributorId
     * @return string
     */
    public function getDistributorName($distributorId)
    {
        $repo = $this->em->getRepository(Distributor::class);
        
        $distributor = $repo->find($distributorId);
        
        if ($distributor) {
            return $distributor->getName();
        }
        
        return '';
    }
}