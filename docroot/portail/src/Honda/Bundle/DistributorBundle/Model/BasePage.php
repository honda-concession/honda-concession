<?php

namespace Honda\Bundle\DistributorBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * BasePage
 * 
 */
class BasePage
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    protected $text;

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return BasePage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return BasePage
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
    
    public function __construct($title)
    {
        $this->title = $title;
    }

}
