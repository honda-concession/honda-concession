<?php

namespace Honda\Bundle\DistributorBundle\Model;

class Region
{
    const REGIONS = [
        'Auvergne-Rhône-Alpes' => 'auvergne-rhone-alpes',
        'Bourgogne-Franche-Comté'=> 'bourgogne-franche-comte',
        'Bretagne' => 'bretagne',
        'Centre-Val de Loire' => 'centre-val-de-loire',
        'Corse' => 'corse',
        'Grand Est' => 'grand-est',
        'Hauts-de-France' => 'hauts-de-france',
        'Île-de-France' => 'ile-de-france',
        'Normandie' => 'normandie',
        'Nouvelle-Aquitaine' => 'nouvelle-aquitaine',
        'Occitanie' => 'occitanie',
        'Pays de la Loire' => 'pays-de-la-loire',
        'Provence-Alpes-Côte d\'Azur' => 'provence-alpes-cote-d-azur',
        'Guadeloupe' => 'guadeloupe',
        'Guyane' => 'guyane',
        'Martinique' => 'martinique',
		'Mayotte' => 'mayotte',
        'La Réunion' => 'la-reunion'
    ];
}
