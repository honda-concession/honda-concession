<?php

namespace Honda\Bundle\DistributorBundle\Model;

use Doctrine\ORM\EntityManager;

use Honda\Bundle\DistributorBundle\Entity\SubGroup;

/**
 * Class SubGroupManager
 * @package Honda\Bundle\DistributorBundle\Model
 */
class SubGroupManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * @return SubGroup
     */
    public function getDefaultSubGroup()
    {
        $defaultSubGroup = $this->em->getRepository(SubGroup::class)->findOneBy(['name' => SubGroup::SOUS_GROUPE_TOUS]);
   
        /* Marche pas
        if (!$defaultSubGroup) {
            
            $defaultSubGroup = new SubGroup();
            $defaultSubGroup->setName(SubGroup::SOUS_GROUPE_TOUS);
            $this->em->persist($defaultSubGroup);
            $this->em->flush();
        }
        */
        
        return $defaultSubGroup;
    }
    
    
}
