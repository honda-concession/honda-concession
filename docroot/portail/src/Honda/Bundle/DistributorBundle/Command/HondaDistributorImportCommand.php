<?php

namespace Honda\Bundle\DistributorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

use Honda\Bundle\CommonBundle\Helper\AES;
use Honda\Bundle\DistributorBundle\Entity\Distributor;

class HondaDistributorImportCommand extends ContainerAwareCommand
{

    private $entityManager;
    private $repository;
    private $aes;

    /**
     * @inheritDoc
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('honda:distributor:import')
            ->setDescription('Import distributor')
        ;
    }

    /**
     * @inheritDoc
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->aes = new AES(null, $this->getContainer()->getParameter('AES_KEY'), 256);
        $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->repository = $this->entityManager->getRepository(Distributor::class);
        $distributors = $this->getDistributors();
        $progressBar = new ProgressBar($output, count($distributors));
        foreach($distributors as $distributor) {
            $this->persistDistributor($distributor);
            $progressBar->advance();
        }

        $this->entityManager->flush();
        $progressBar->finish();
    }

    /**
     * persist distributor
     *
     * @param \stdClass $remoteContent
     * @return self
     */
    private function persistDistributor(\stdClass $remoteContent)
    {
        $distributor = $this->repository->findOneByDcmsid($this->aes->encryptData($remoteContent->dcms_id));
        if (!$distributor) {
            $distributor = new Distributor();
        }
        $distributor
            ->setName(trim($remoteContent->nom_social))
            ->setDcmsid($remoteContent->dcms_id)
            ->setAddress1($remoteContent->adresse)
            ->setLongitude($remoteContent->position->longitude)
            ->setLatitude($remoteContent->position->latitude)
            ->setCodePostal($remoteContent->cp)
            ->setCity($remoteContent->ville)
            ->setPublished($remoteContent->statut)
            ->setCadreRouge(false)
            ->setSubscriptionPayment(false)
            ->setAgentOfficiel(false)
            ->setSpecialistCross(false)
            ->setDistributorMotor(false)
        ;
        if (property_exists($remoteContent, 'url') && $remoteContent->url) {
            $distributor->setWebsiteUrl($remoteContent->url);
        }
        if (property_exists($remoteContent, 'department')) {
            $department = $distributor->department->code;
            $department = substr($code_postal,0,2);
            $distributor->setDepartment($department);
        }
        if (is_array($remoteContent->groups) && count($remoteContent->groups) > 0) {
            foreach ($remoteContent->groups as $group) {
                switch($group->id){
                    case 1:
                        $distributor->setDistributorMotor(true);
                        break;
                    case 2:
                        $distributor
                            ->setCadreRouge(true)
                            ->setSpecialistCross(true)
                        ;
                        break;
                    case 3:
                        $distributor->setAgentOfficiel(true);
                        break;
                }
            }
        }
        $infos = $this->getAdditionalInfoByDcmsId($remoteContent->dcms_id);
        if (is_array($infos->services) && count($infos->services) > 0) {
            foreach ($infos->services as $service) {
                if (isset($service->tel) && !empty($service->tel)) {
                    $distributor->setTelephone($service->tel);
                }
                if (isset($service->fax) && !empty($service->fax)) {
                    $distributor->setFax($service->fax);
                }
                if (isset($service->email) && !empty($service->email)) {
                    $distributor->setEmail($service->email);
                }
            }
        }
        $codeDepartment = substr($remoteContent->cp, 0, 2);
        $position = strpos($remoteContent->cp, '97');
        if ($position && $position === 0) {
            $codeDepartment = substr($remoteContent->cp, 0, 3);
        }
        foreach (self::getRegions() as $region => $department) {
            $departments = explode(',', $department);
            if (array_search($codeDepartment, $departments)) {
                $distributor->setRegion($region);
                break;
            }
        }
        $distributor->setCreated((new \DateTime())->setTimestamp(date(strtotime($remoteContent->date_creation))));
        $distributor->setUpdated((new \DateTime())->setTimestamp(date(strtotime($remoteContent->date_maj))));

        $this->entityManager->persist($distributor);

        return $this;
    }

    /**
     * get distributor
     *
     * @return \stdClass
     */
    private function getDistributors()
    {
        extract($this->getDistributorParameters());

        return $this->getContentByUrl("{$formEndpoint}/moto/dealers?X-CALLEE={$bloomKey}");
    }

    /**
     * get additional content by dcmd id
     *
     * @param string $dcmsId
     * @return \stdClass
     */
    private function getAdditionalInfoByDcmsId($dcmsId)
    {
        extract($this->getDistributorParameters());
        $url = "{$formEndpoint}/moto/dealer/{$dcmsId}.json?X-CALLEE={$bloomKey}";

        return $this->getContentByUrl($url);
    }

    /**
     * fetch content
     *
     * @param string $url
     * @return \stdClass
     */
    private function getContentByUrl($url)
    {
        return json_decode(file_get_contents($url));
    }

    private function getDistributorParameters()
    {
        $container = $this->getContainer();

        return [
            'formEndpoint' => $container->getParameter('form_endpoint'),
            'bloomKey' => $container->getParameter('bloom_key'),
        ];
    }

    /**
     * get regions list
     *
     * @return array
     */
    private static function getRegions()
    {
        $regions = array();
        $regions['Alsace'] = '67,68';
        $regions['Aquitaine'] = '24,33,40,47,64';
        $regions['Auvergne'] = '03,15,43,63';
        $regions['Basse-Normandie'] = '14,50,61';
        $regions['Bourgogne'] = '21,58,71,89';
        $regions['Bretagne'] = '22,29,35,56';
        $regions['Centre'] = '18,28,36,37,41,45';
        $regions['Champagne-Ardenne'] = '08,10,51,52';
        $regions['Corse'] = '2A,2B,20';
        $regions['Franche-Comté'] = '25,39,70,90';
        $regions['Haute-Normandie'] = '27,76';
        $regions['Ile-de-France'] = '75,77,78,91,92,93,94,95';
        $regions['Languedoc-Roussillon'] = '11,30,34,48,66';
        $regions['Limousin'] = '19,23,87';
        $regions['Lorraine'] = '54,55,57,88';
        $regions['Midi-Pyrénées'] = '09,12,31,32,46,65,81,82';
        $regions['Nord-Pas-de-Calais'] = '59,62';
        $regions['Pays de la Loire'] = '44,49,53,72,85';
        $regions['Picardie'] = '02,60,80';
        $regions['Poitou-Charentes'] = '16,17,79,86';
        $regions['Provence-Alpes-Côte-d\'Azur'] = '04,05,06,13,83,84';
        $regions['Rhône-Alpes'] = '01,07,26,38,42,69,73,74';
        $regions['DOM-TOM'] = '971,972,973,974,975,976,984,986,987,988';
        $regions['Principauté de Monaco'] = '980';

        return $regions;
    }
}
