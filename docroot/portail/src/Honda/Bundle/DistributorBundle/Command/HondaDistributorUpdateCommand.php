<?php

namespace Honda\Bundle\DistributorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use Honda\Bundle\DistributorBundle\Entity\Distributor;

class HondaDistributorUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('honda:distributor:update')
            ->setDescription('Activate / deactivate distributor by on the duration of its activity')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title("Update distributor status");
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository(Distributor::class);
        $distributors = $repository->findAll();
        $datetime = new \DateTime();
        $io->progressStart(count($distributors));
        foreach ($distributors as $distributor) {
            if ($distributor->getActivationStart() < $datetime) {
                $isActive = true;
            } else {
                $isActive = false;
            }
            if ($distributor->getActivationEnd() > $datetime) {
                $isActive = true;
            } else {
                $isActive = false;
            }
            $distributor->setActive($isActive);
            $em->persist($distributor);
            $io->progressAdvance();
        }
        $em->flush();
        $io->progressFinish();
    }

}
