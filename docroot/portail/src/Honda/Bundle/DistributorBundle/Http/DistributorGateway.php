<?php

namespace Honda\Bundle\DistributorBundle\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Honda\Bundle\CommonBundle\Tools\Encryption\EncryptorAesEcb;
use Symfony\Component\HttpFoundation\Response;

class DistributorGateway
{
    /**
     * @var array
     */
    private $endpoints;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $env;

    /**
     * @var Client
     */
    private $client;
    
    /**
     * DistributorGateway constructor.
     * @param $endpoints
     * @param $key
     * @param $env
     */
    public function __construct($endpoints, $key, $env)
    {
        $this->endpoints = $endpoints;
        $this->key = $key;
        $this->env = $env;
        $this->client = new Client(['timeout'  => 20]);
    }

    /**
     * update distributor
     *
     * @param $id integer
     * @param $data array
     * @return boolean
     */
    public function getUpdateDistributor($id, $data)
    {
        $token = EncryptorAesEcb::encode(json_encode($data), $this->key);
        
        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }
        
        try {
            
            $res = $this->client->request('POST', $this->endpoints['distributor_update'] .'/'. $id, [
                'headers' => ['X-AUTH-TOKEN' => $token],
                'verify' => $sslVerification,
                'body' => \GuzzleHttp\json_encode($data)
            ]);
            
            if ($res->getStatusCode() == Response::HTTP_NO_CONTENT) {
                return true;
            }
            
        } catch (ClientException $e) {
        } catch (ServerException $e) {
        }
        
        return true;
    }
    
    
}