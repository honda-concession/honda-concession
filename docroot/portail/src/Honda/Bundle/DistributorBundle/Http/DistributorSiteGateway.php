<?php

namespace Honda\Bundle\DistributorBundle\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Cocur\Slugify\Slugify;
use Symfony\Component\HttpFoundation\Response;
use Honda\Bundle\CommonBundle\Tools\Encryption\EncryptorAesEcb;

class DistributorSiteGateway
{

    /**
     * @var array
     */
    private $endpoints;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $env;

    /**
     * @var Client
     */
    private $client;

    public function __construct($endpoints, $key, $env)
    {
        $this->endpoints = $endpoints;
        $this->key = $key;
        $this->env = $env;
        $this->client = new Client(['timeout' => 30]);
    }

    /**
     * List site distributor
     *
     * @return array
     */
    public function getSites()
    {
        $timestamp = strtotime("now");

        $token = EncryptorAesEcb::encode($timestamp, $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        $sites = [];

        try {
            $res = $this->client->request('GET', $this->endpoints['site_list'] . '/' . $timestamp, [
                'headers' => ['X-AUTH-TOKEN' => $token],
                'verify' => $sslVerification
            ]);

            $sites = json_decode($res->getBody()->getContents(), true);
        } catch (ClientException $e) {

        }

        return $sites;
    }

    /**
     * get site distributor
     *
     * @return array
     */
    public function getSite($distributorId)
    {
        $timestamp = strtotime("now");

        $token = EncryptorAesEcb::encode($timestamp, $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        $site = [];
        $res = $this->client->request('GET', $this->endpoints['get_list'] . '/' . $distributorId . '/' . $timestamp, [
            'headers' => ['X-AUTH-TOKEN' => $token, 'Accept' => 'application/json'],
            'verify' => $sslVerification,
        ]);

        $site = json_decode($res->getBody()->getContents(), true);

        return $site;
    }

    /**
     * @param $site
     * @return array|mixed|\Psr\Http\Message\ResponseInterface
     */
    public function create($site)
    {
        $distributor = $site['distributor'];
        $slugify = new Slugify();

        $params = [
            'name' => $slugify->slugify($distributor->getName(), '_'),
            'host' => $site['site'],
            'locale' => $site['locale'],
            'status' => $site['status'],
            'cid' => $distributor->getId(),
            'email' => $site['username'],
            'password' => $site['password'],
        ];

        $token = EncryptorAesEcb::encode(json_encode($params), $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        $res = [];

        try {
            $res = $this->client->request('POST', $this->endpoints['site_create'], [
                'headers' => ['X-AUTH-TOKEN' => $token],
                'verify' => $sslVerification,
                'body' => json_encode($params)
            ]);

            $res = json_decode($res->getBody()->getContents(), true);
        } catch (ClientException $e) {

        }

        return $res;
    }

    /**
     * @param $site array
     * @return bool
     */
    public function update($site)
    {
        $params = [
            'siteName' => $site['siteName'],
            'host' => $site['site'],
            'status' => $site['status'],
            'distributorId' => $site['distributorId'],
            'locale' => $site['locale'],
            'email' => $site['username'],
            'password' => $site['password'],
            'gtmCode' => $site['gtmCode'],
        ];

        $token = EncryptorAesEcb::encode(json_encode($params), $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        try {
            $res = $this->client->request('POST', $this->endpoints['site_update'] . '/' . $site['distributorId'], [
                'headers' => ['X-AUTH-TOKEN' => $token],
                'verify' => $sslVerification,
                'body' => json_encode($params)
            ]);

            if ($res->getStatusCode() == Response::HTTP_NO_CONTENT) {
                return true;
            }
        } catch (ClientException $e) {
            if (strstr($e->getMessage(), 'Distributor not found')) {
                return $this->create($site);
            }
        }

        return false;
    }

}
