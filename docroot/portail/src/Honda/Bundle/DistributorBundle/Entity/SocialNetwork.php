<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SocialNetwork
 *
 * @ORM\Table(name="social_network")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\SocialNetworkRepository")
 */
class SocialNetwork
{
    
    const UNIQ_ID = 1;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lien_fb", type="string", length=255, nullable=true)
     */
    private $lienFb;
    
    /**
     * @var string
     *
     * @ORM\Column(name="lien_inst", type="string", length=255, nullable=true)
     */
    private $lienInsta;
    
    /**
     * @var string
     *
     * @ORM\Column(name="lien_twitter", type="string", length=255, nullable=true)
     */
    private $lienTwitter;
    
    /**
     * @var string
     *
     * @ORM\Column(name="lien_youtube", type="string", length=255, nullable=true)
     */
    private $lienYoutube;
    
    /**
     * @var string
     *
     * @ORM\Column(name="lien_snapchat", type="string", length=255, nullable=true)
     */
    private $lienSnapchat;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lienFb.
     *
     * @param string $lienFb
     *
     * @return SocialNetwork
     */
    public function setLienFb($lienFb)
    {
        $this->lienFb = $lienFb;

        return $this;
    }

    /**
     * Get lienFb.
     *
     * @return string
     */
    public function getLienFb()
    {
        return $this->lienFb;
    }
    
        /**
     * Set lienInsta.
     *
     * @param string $lienInsta
     *
     * @return SocialNetwork
     */
    public function setLienInsta($lienInsta)
    {
        $this->lienInsta = $lienInsta;

        return $this;
    }

    /**
     * Get lienInsta.
     *
     * @return string
     */
    public function getLienInsta()
    {
        return $this->lienInsta;
    }
    
        /**
     * Set lienTwitter.
     *
     * @param string $lienTwitter
     *
     * @return SocialNetwork
     */
    public function setLienTwitter($lienTwitter)
    {
        $this->lienTwitter = $lienTwitter;

        return $this;
    }

    /**
     * Get lienTwitter.
     *
     * @return string
     */
    public function getLienTwitter()
    {
        return $this->lienTwitter;
    }
    
        /**
     * Set lienYoutube.
     *
     * @param string $lienYoutube
     *
     * @return SocialNetwork
     */
    public function setLienYoutube($lienYoutube)
    {
        $this->lienYoutube = $lienYoutube;

        return $this;
    }

    /**
     * Get lienYoutube.
     *
     * @return string
     */
    public function getLienYoutube()
    {
        return $this->lienYoutube;
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        return 'Réseaux Sociaux';
    }
    
    /**
     * @return string
     */
    public function getLienSnapchat()
    {
        return $this->lienSnapchat;
    }
    
    /**
     * @param string $lienSnapchat
     * @return SocialNetwork
     */
    public function setLienSnapchat($lienSnapchat)
    {
        $this->lienSnapchat = $lienSnapchat;
    
        return $this;
    }
}
