<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logos
 *
 * @ORM\Table(name="logos_accesories")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\LogosRepository")
 */
class Logos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $logo;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="logos")
     * @ORM\JoinColumn(name="category_logo_id", referencedColumnName="id")
     */
    private $category;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Logos
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logo.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $logo
     *
     * @return Category
     */
    public function setLogo(\Application\Sonata\MediaBundle\Entity\Media $logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set category.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Category|null $category
     *
     * @return Logos
     */
    public function setCategory(\Honda\Bundle\DistributorBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \Honda\Bundle\DistributorBundle\Entity\Category|null
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }
}
