<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\SubscriptionTrait;
use Honda\Bundle\CommonBundle\Model\BaseModel;
use Honda\Bundle\DistributorBundle\Entity\Schedule;
use Honda\Bundle\DistributorBundle\Entity\SubGroup;

/**
 * Distributor
 *
 * @ORM\Table(name="distributor")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\DistributorRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\EntityListeners({"Honda\Bundle\DistributorBundle\Doctrine\DistributorListener"})
 * @UniqueEntity("name")
 */
class Distributor extends BaseModel
{

    use ActivationTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="dcmsid", type="string", length=255, nullable=true)
     */
    private $dcmsid;

    /**
     * @var string
     *
     * @ORM\Column(name="code_sap", type="string", length=255, nullable=true)
     */
    private $codeSap;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="address1", type="string", length=255, nullable=true)
     */
    private $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="code_postal", type="string", length=255, nullable=true)
     */
    private $codePostal;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    protected $location;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=255, nullable=true)
     */
    protected $siret;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="department", type="string", length=255, nullable=true)
     */
    private $department;

    /**
     * @ORM\OneToMany(targetEntity="Schedule" , mappedBy="distributor", cascade={"all"}, orphanRemoval=true)
     */
    private $schedule;

    /**
     * @ORM\OneToMany(targetEntity="Diffusion" , mappedBy="distributor", cascade={"all"}, orphanRemoval=true)
     */
    private $diffusion;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="websiteUrl", type="string", length=255, nullable=true)
     */
    private $websiteUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="websiteBoUrl", type="string", length=255, nullable=true)
     */
    private $websiteBoUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="sub_domain", type="string", length=255, nullable=true)
     */
    private $subDomain;

    /**
     * @var string
     *
     * @ORM\Column(name="commercial_url", type="string", length=255, nullable=true)
     */
    private $commercialUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="admin", type="string", length=255, nullable=true)
     */
    private $admin;

    /**
     * @var string
     *
     * @ORM\Column(name="pswd", type="string", length=255, nullable=true)
     */
    private $pswd;

    /**
     * @var string
     *
     * @ORM\Column(name="otherUrl", type="string", length=255, nullable=true)
     */
    private $otherUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="url_winteam", type="string", length=255, nullable=true)
     */
    private $urlWinteam;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $logo;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image;

    /**
     * @ORM\OneToOne(targetEntity="TemplateColor")
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="statsUrl", type="string", length=255, nullable=true)
     */
    private $statsUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="gaCode", type="string", length=255, nullable=true)
     */
    private $gaCode;

    /**
     * @var string
     *
     * @ORM\Column(name="gtmCode", type="string", length=255, nullable=true)
     */
    private $gtmCode;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255, nullable=true)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(name="insert_picture", type="string", length=255, nullable=true)
     */
    protected $insertPicture;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $kbis;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $contract;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $guide;

    /**
     * @var string
     *
     * @ORM\Column(name="addSurname", type="string", length=255 , nullable=true)
     */
    private $addSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="addName", type="string", length=255, nullable=true)
     */
    private $addName;

    /**
     * @var string
     *
     * @ORM\Column(name="addAddress1", type="string", length=255, nullable=true)
     */
    private $addAddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="addAddress2", type="string", length=255, nullable=true)
     */
    private $addAddress2;

    /**
     * @var string
     *
     * @ORM\Column(name="addCodePostal", type="string", length=255, nullable=true)
     */
    private $addCodePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="addCity", type="string", length=255, nullable=true)
     */
    private $addCity;

    /**
     * @var string
     *
     * @ORM\Column(name="addTelephone", type="string", length=255, nullable=true)
     */
    private $addTelephone;

    /**
     * @var string
     *
     * @ORM\Column(name="addEmail", type="string", length=255, nullable=true)
     */
    private $addEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="addStartDate", type="datetime", length=255, nullable=true)
     */
    private $addStartDate;

    /**
     * @var string
     *
     * @ORM\Column(name="addEndDate", type="datetime", length=255, nullable=true)
     */
    private $addEndDate;

    /**
     * @var string
     *
     * @ORM\Column(name="addRemainingDays", type="string", length=255, nullable=true)
     */
    private $addRemainingDays;

    /**
     * @var string
     *
     * @ORM\Column(name="cadre_rouge", type="boolean", nullable=false, options={"default":"0"})
     */
    private $cadreRouge;

    /**
     * @var string
     *
     * @ORM\Column(name="subscription_payment", type="boolean", nullable=false, options={"default":"0"})
     */
    private $subscriptionPayment;

    /**
     * @var string
     *
     * @ORM\Column(name="agent", type="boolean", nullable=false, options={"default":"0"})
     */
    private $agentOfficiel;

    /**
     * @var string
     *
     * @ORM\Column(name="specialist_cross", type="boolean", nullable=false, options={"default":"0"})
     */
    private $specialistCross;

    /**
     * @var string
     *
     * @ORM\Column(name="distributor_motor", type="boolean", nullable=false, options={"default":"0"})
     */
    private $distributorMotor;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $logoCadreRouge;

    /**
     *
     * @ORM\ManyToMany(targetEntity="SubGroup", inversedBy="distributors", cascade={"persist"})
     */
    private $subGroups;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="activation_start", type="datetime", nullable=true)
     */
    private $activationStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="activation_end", type="datetime", nullable=true)
     */
    private $activationEnd;

    /**
     * activate vehicle ad export
     * @var bool
     *
     * @ORM\Column(name="activation_vehicle_ad_export", type="boolean", options={"default": 1})
     */
    private $activationVehicleAdExport = true;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="SubscriptionDistributor" , mappedBy="distributor", cascade={"all"}, orphanRemoval=true)
     */
    private $subscriptions;

    /**
     * @var string
     *
     * @ORM\Column(name="data_studio_id", type="string", length=255, nullable=true)
     */
    private $dataStudioId;

    public function __construct()
    {
        parent::__construct();
        $this->subGroups = new ArrayCollection();
        $this->schedule = new ArrayCollection();
        $this->diffusion = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getGoogleMapLink()
    {
        if ($this->getLatitude() || $this->getLongitude()) {
            return sprintf('https://www.google.com/maps/dir/%s', $this->getLatitude() . ',' . $this->getLongitude());
        }

        return '';
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        if (!$this->getLocation()) {
            $this->location = $this->latitude . ',' . $this->longitude;
        }
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdateBase()
    {
        $this->location = $this->latitude . ',' . $this->longitude;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Distributor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set codeSap.
     *
     * @param string $codeSap
     *
     * @return Distributor
     */
    public function setCodeSap($codeSap)
    {
        $this->codeSap = $codeSap;

        return $this;
    }

    /**
     * Get codeSap.
     *
     * @return string
     */
    public function getCodeSap()
    {
        return $this->codeSap;
    }

    /**
     * Set address1.
     *
     * @param string $address1
     *
     * @return Distributor
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1.
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2.
     *
     * @param string $address2
     *
     * @return Distributor
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2.
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set codePostal.
     *
     * @param string $codePostal
     *
     * @return Distributor
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal.
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set region.
     *
     * @param string $region
     *
     * @return Distributor
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Distributor
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set longitude.
     *
     * @param string $longitude
     *
     * @return Distributor
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude.
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude.
     *
     * @param string $latitude
     *
     * @return Distributor
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude.
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set department.
     *
     * @param string $department
     *
     * @return Distributor
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department.
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set schedule.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Schedule|null $schedule
     *
     * @return Distributor
     */
    public function setSchedule(Schedule $schedule = null)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule.
     *
     * @return \Honda\Bundle\DistributorBundle\Entity\Schedule|null
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Set diffusion.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Diffusion|null $diffusion
     *
     * @return Distributor
     */
    public function setDiffusion(Diffusion $diffusion = null)
    {
        $this->diffusion = $diffusion;

        return $this;
    }

    /**
     * Get diffusion.
     *
     * @return \Honda\Bundle\DistributorBundle\Entity\Diffusion|null
     */
    public function getDiffusion()
    {
        return $this->diffusion;
    }

    /**
     * Set subGroups.
     *
     * @param \Doctrine\Common\Collections\Collection $subGroups
     *
     * @return Distributor
     */
    public function setSubGroups($subGroups)
    {
        $this->subGroups = $subGroups;

        return $this;
    }

    /**
     * Get subGroups.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubGroups()
    {
        return $this->subGroups;
    }

    /**
     * Add subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return Distributor
     */
    public function addSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        if (!$this->subGroups->contains($subGroup)) {
            $this->subGroups[] = $subGroup;
            $subGroup->addDistributor($this);
        }

        return $this;
    }

    /**
     * Remove subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return Distributor
     */
    public function removeSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        $subGroup->removeDistributor($this);
        $this->subGroups->removeElement($subGroup);

        return $this;
    }

    /**
     * Set websiteUrl.
     *
     * @param string $websiteUrl
     *
     * @return Distributor
     */
    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    /**
     * Get websiteUrl.
     *
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    /**
     * Set otherUrl.
     *
     * @param string $otherUrl
     *
     * @return Distributor
     */
    public function setOtherUrl($otherUrl)
    {
        $this->otherUrl = $otherUrl;

        return $this;
    }

    /**
     * Get otherUrl.
     *
     * @return string
     */
    public function getOtherUrl()
    {
        return $this->otherUrl;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Distributor
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return Distributor
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax.
     *
     * @param string $fax
     *
     * @return Distributor
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set logo.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $logo
     *
     * @return Distributor
     */
    public function setLogo(\Application\Sonata\MediaBundle\Entity\Media $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Distributor
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set color.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\TemplateColor|null $color
     *
     * @return Distributor
     */
    public function setColor(\Honda\Bundle\DistributorBundle\Entity\TemplateColor $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return \Honda\Bundle\DistributorBundle\Entity\TemplateColor|null
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add schedule.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Schedule $schedule
     *
     * @return Distributor
     */
    public function addSchedule(\Honda\Bundle\DistributorBundle\Entity\Schedule $schedule)
    {
        $this->schedule[] = $schedule;
        $schedule->setDistributor($this);

        return $this;
    }

    /**
     * Remove schedule.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Schedule $schedule
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSchedule(\Honda\Bundle\DistributorBundle\Entity\Schedule $schedule)
    {
        return $this->schedule->removeElement($schedule);
    }

    /**
     * Add diffusion.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Diffusion $diffusion
     *
     * @return Distributor
     */
    public function addDiffusion(\Honda\Bundle\DistributorBundle\Entity\Diffusion $diffusion)
    {
        $diffusion->setDistributor($this);
        $this->diffusion[] = $diffusion;

        return $this;
    }

    /**
     * Remove diffusion.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Diffusion $diffusion
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDiffusion(\Honda\Bundle\DistributorBundle\Entity\Diffusion $diffusion)
    {
        return $this->diffusion->removeElement($diffusion);
    }

    /**
     * Set statsUrl.
     *
     * @param string $statsUrl
     *
     * @return Distributor
     */
    public function setStatsUrl($statsUrl)
    {
        $this->statsUrl = $statsUrl;

        return $this;
    }

    /**
     * Get statsUrl.
     *
     * @return string
     */
    public function getStatsUrl()
    {
        return $this->statsUrl;
    }

    /**
     * Set gaCode.
     *
     * @param string $gaCode
     *
     * @return Distributor
     */
    public function setGaCode($gaCode)
    {
        $this->gaCode = $gaCode;

        return $this;
    }

    /**
     * Get gaCode.
     *
     * @return string
     */
    public function getGaCode()
    {
        return $this->gaCode;
    }

    /**
     * Set login.
     *
     * @param string $login
     *
     * @return Distributor
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return Distributor
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set addSurname.
     *
     * @param string $addSurname
     *
     * @return Distributor
     */
    public function setAddSurname($addSurname)
    {
        $this->addSurname = $addSurname;

        return $this;
    }

    /**
     * Get addSurname.
     *
     * @return string
     */
    public function getAddSurname()
    {
        return $this->addSurname;
    }

    /**
     * Set addName.
     *
     * @param string $addName
     *
     * @return Distributor
     */
    public function setAddName($addName)
    {
        $this->addName = $addName;

        return $this;
    }

    /**
     * Get addName.
     *
     * @return string
     */
    public function getAddName()
    {
        return $this->addName;
    }

    /**
     * Set addAddress1.
     *
     * @param string $addAddress1
     *
     * @return Distributor
     */
    public function setAddAddress1($addAddress1)
    {
        $this->addAddress1 = $addAddress1;

        return $this;
    }

    /**
     * Get addAddress1.
     *
     * @return string
     */
    public function getAddAddress1()
    {
        return $this->addAddress1;
    }

    /**
     * Set addAddress2.
     *
     * @param string $addAddress2
     *
     * @return Distributor
     */
    public function setAddAddress2($addAddress2)
    {
        $this->addAddress2 = $addAddress2;

        return $this;
    }

    /**
     * Get addAddress2.
     *
     * @return string
     */
    public function getAddAddress2()
    {
        return $this->addAddress2;
    }

    /**
     * Set addCodePostal.
     *
     * @param string $addCodePostal
     *
     * @return Distributor
     */
    public function setAddCodePostal($addCodePostal)
    {
        $this->addCodePostal = $addCodePostal;

        return $this;
    }

    /**
     * Get addCodePostal.
     *
     * @return string
     */
    public function getAddCodePostal()
    {
        return $this->addCodePostal;
    }

    /**
     * Set addCity.
     *
     * @param string $addCity
     *
     * @return Distributor
     */
    public function setAddCity($addCity)
    {
        $this->addCity = $addCity;

        return $this;
    }

    /**
     * Get addCity.
     *
     * @return string
     */
    public function getAddCity()
    {
        return $this->addCity;
    }

    /**
     * Set addTelephone.
     *
     * @param string $addTelephone
     *
     * @return Distributor
     */
    public function setAddTelephone($addTelephone)
    {
        $this->addTelephone = $addTelephone;

        return $this;
    }

    /**
     * Get addTelephone.
     *
     * @return string
     */
    public function getAddTelephone()
    {
        return $this->addTelephone;
    }

    /**
     * Set addEmail.
     *
     * @param string $addEmail
     *
     * @return Distributor
     */
    public function setAddEmail($addEmail)
    {
        $this->addEmail = $addEmail;

        return $this;
    }

    /**
     * Get addEmail.
     *
     * @return string
     */
    public function getAddEmail()
    {
        return $this->addEmail;
    }

    /**
     * Set addStartDate.
     *
     * @param string $addStartDate
     *
     * @return Distributor
     */
    public function setAddStartDate($addStartDate)
    {
        $this->addStartDate = $addStartDate;

        return $this;
    }

    /**
     * Get addStartDate.
     *
     * @return string
     */
    public function getAddStartDate()
    {
        return $this->addStartDate;
    }

    /**
     * Set addRemainingDays.
     *
     * @param string $addRemainingDays
     *
     * @return Distributor
     */
    public function setAddRemainingDays($addRemainingDays)
    {
        $this->addRemainingDays = $addRemainingDays;

        return $this;
    }

    /**
     * Get addRemainingDays.
     *
     * @return string
     */
    public function getAddRemainingDays()
    {
        return $this->addRemainingDays;
    }

    /**
     * Set kbis.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $kbis
     *
     * @return Distributor
     */
    public function setKbis(\Application\Sonata\MediaBundle\Entity\Media $kbis = null)
    {
        $this->kbis = $kbis;

        return $this;
    }

    /**
     * Get kbis.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getKbis()
    {
        return $this->kbis;
    }

    /**
     * Set contract.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $contract
     *
     * @return Distributor
     */
    public function setContract(\Application\Sonata\MediaBundle\Entity\Media $contract = null)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * Set guide.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $guide
     *
     * @return Distributor
     */
    public function setGuide(\Application\Sonata\MediaBundle\Entity\Media $guide = null)
    {
        $this->guide = $guide;

        return $this;
    }

    /**
     * Get guide.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getGuide()
    {
        return $this->guide;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

    /**
     * Set addEndDate.
     *
     * @param string $addEndDate
     *
     * @return Distributor
     */
    public function setAddEndDate($addEndDate)
    {
        $this->addEndDate = $addEndDate;

        return $this;
    }

    /**
     * Get addEndDate.
     *
     * @return string
     */
    public function getAddEndDate()
    {
        return $this->addEndDate;
    }

    /**
     * Set cadreRouge.
     *
     * @param bool $cadreRouge
     *
     * @return Distributor
     */
    public function setCadreRouge($cadreRouge)
    {
        $this->cadreRouge = $cadreRouge;

        return $this;
    }

    /**
     * Get cadreRouge.
     *
     * @return bool
     */
    public function getCadreRouge()
    {
        return $this->cadreRouge;
    }

    /**
     * Set logoCadreRouge.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $logoCadreRouge
     *
     * @return Distributor
     */
    public function setLogoCadreRouge(\Application\Sonata\MediaBundle\Entity\Media $logoCadreRouge = null)
    {
        $this->logoCadreRouge = $logoCadreRouge;

        return $this;
    }

    /**
     * Get logoCadreRouge.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getLogoCadreRouge()
    {
        return $this->logoCadreRouge;
    }

    /**
     * Set description.
     *
     * @param text $description
     *
     * @return Distributor
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get addEndDate.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get location.
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set location.
     *
     * @param string $location
     *
     * @return Distributor
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Set agentOfficiel.
     *
     * @param bool $agentOfficiel
     *
     * @return Distributor
     */
    public function setAgentOfficiel($agentOfficiel)
    {
        $this->agentOfficiel = $agentOfficiel;

        return $this;
    }

    /**
     * Get agentOfficiel.
     *
     * @return bool
     */
    public function getAgentOfficiel()
    {
        return $this->agentOfficiel;
    }

    /**
     * Set specialistCross.
     *
     * @param bool $specialistCross
     *
     * @return Distributor
     */
    public function setSpecialistCross($specialistCross)
    {
        $this->specialistCross = $specialistCross;

        return $this;
    }

    /**
     * Get specialistCross.
     *
     * @return bool
     */
    public function getSpecialistCross()
    {
        return $this->specialistCross;
    }

    /**
     * Set distributorMotor.
     *
     * @param bool $distributorMotor
     *
     * @return Distributor
     */
    public function setDistributorMotor($distributorMotor)
    {
        $this->distributorMotor = $distributorMotor;

        return $this;
    }

    /**
     * Get distributorMotor.
     *
     * @return bool
     */
    public function getDistributorMotor()
    {
        return $this->distributorMotor;
    }

    /**
     * Set urlWinteam.
     *
     * @param string $urlWinteam
     *
     * @return Distributor
     */
    public function setUrlWinteam($urlWinteam)
    {
        $this->urlWinteam = $urlWinteam;

        return $this;
    }

    /**
     * Get urlWinteam.
     *
     * @return string
     */
    public function getUrlWinteam()
    {
        return $this->urlWinteam;
    }

    /**
     * Set subscriptionPayment.
     *
     * @param bool $subscriptionPayment
     *
     * @return Distributor
     */
    public function setSubscriptionPayment($subscriptionPayment)
    {
        $this->subscriptionPayment = $subscriptionPayment;

        return $this;
    }

    /**
     * Get subscriptionPayment.
     *
     * @return bool
     */
    public function getSubscriptionPayment()
    {
        return $this->subscriptionPayment;
    }


    /**
     * Set websiteBoUrl.
     *
     * @param string $websiteBoUrl
     *
     * @return Distributor
     */
    public function setWebsiteBoUrl($websiteBoUrl)
    {
        $this->websiteBoUrl = $websiteBoUrl;

        return $this;
    }

    /**
     * Get websiteBoUrl.
     *
     * @return string
     */
    public function getWebsiteBoUrl()
    {
        return $this->websiteBoUrl;
    }

    /**
     * Set admin.
     *
     * @param string $admin
     *
     * @return Distributor
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin.
     *
     * @return string
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set pswd.
     *
     * @param string $pswd
     *
     * @return Distributor
     */
    public function setPswd($pswd)
    {
        $this->pswd = $pswd;

        return $this;
    }

    /**
     * Get pswd.
     *
     * @return string
     */
    public function getPswd()
    {
        return $this->pswd;
    }

    /**
     * Set dcmsid.
     *
     * @param string|null $dcmsid
     *
     * @return Distributor
     */
    public function setDcmsid($dcmsid = null)
    {
        $this->dcmsid = $dcmsid;

        return $this;
    }

    /**
     * Get dcmsid.
     *
     * @return string|null
     */
    public function getDcmsid()
    {
        return $this->dcmsid;
    }

    /**
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param string $siret
     * @return Distributor
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get the value of activationStart
     *
     * @return  \DateTime
     */
    public function getActivationStart()
    {
        return $this->activationStart;
    }

    /**
     * Set the value of activationStart
     *
     * @param  \DateTime  $activationStart
     *
     * @return  self
     */
    public function setActivationStart(\DateTime $activationStart = null)
    {
        $this->activationStart = $activationStart;

        return $this;
    }

    /**
     * Get the value of activationEnd
     *
     * @return  \DateTime
     */
    public function getActivationEnd()
    {
        return $this->activationEnd;
    }

    /**
     * Set the value of activationEnd
     *
     * @param  \DateTime  $activationEnd
     *
     * @return  self
     */
    public function setActivationEnd(\DateTime $activationEnd = null)
    {
        $this->activationEnd = $activationEnd;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInsertPicture()
    {
        return $this->insertPicture;
    }

    /**
     * @param mixed $insertPicture
     */
    public function setInsertPicture($insertPicture)
    {
        $this->insertPicture = $insertPicture;
    }

    /**
     * Get the value of subDomain
     *
     * @return  string
     */
    public function getSubDomain()
    {
        return $this->subDomain;
    }

    /**
     * Set the value of subDomain
     *
     * @param  string  $subDomain
     *
     * @return  self
     */
    public function setSubDomain(?string $subDomain)
    {
        $this->subDomain = $subDomain;

        return $this;
    }

    /**
     * Get the value of commercialUrl
     *
     * @return  string
     */
    public function getCommercialUrl()
    {
        return $this->commercialUrl;
    }

    /**
     * Set the value of commercialUrl
     *
     * @param  string  $commercialUrl
     *
     * @return  self
     */
    public function setCommercialUrl(?string $commercialUrl)
    {
        $this->commercialUrl = $commercialUrl;

        return $this;
    }

    /**
     * Get the value of gtmCode
     *
     * @return  string
     */
    public function getGtmCode()
    {
        return $this->gtmCode;
    }

    /**
     * Set the value of gtmCode
     *
     * @param  string  $gtmCode
     *
     * @return  self
     */
    public function setGtmCode(?string $gtmCode)
    {
        $this->gtmCode = $gtmCode;

        return $this;
    }

    /**
     * Get activate vehicle ad export
     *
     * @return  bool
     */
    public function getActivationVehicleAdExport()
    {
        return $this->activationVehicleAdExport;
    }

    /**
     * Set activate vehicle ad export
     *
     * @param  bool  $activationVehicleAdExport  activate vehicle ad export
     *
     * @return  self
     */
    public function setActivationVehicleAdExport(bool $activationVehicleAdExport)
    {
        $this->activationVehicleAdExport = $activationVehicleAdExport;

        return $this;
    }

    public function isVehicleAdExportActivated()
    {
        return $this->getActivationVehicleAdExport() == true;
    }

    /**
     * Get the value of subscriptions
     *
     * @return  ArrayCollection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * Set the value of subscriptions
     *
     * @param  ArrayCollection  $subscriptions
     *
     * @return  self
     */
    public function setSubscriptions(ArrayCollection $subscriptions)
    {
        $this->subscriptions = $subscriptions;

        return $this;
    }

    /**
     * Get the value of dataStudioId
     *
     * @return  string
     */
    public function getDataStudioId()
    {
        return $this->dataStudioId;
    }

    /**
     * Set the value of dataStudioId
     *
     * @param  string  $dataStudioId
     *
     * @return  self
     */
    public function setDataStudioId(?string $dataStudioId)
    {
        $this->dataStudioId = $dataStudioId;

        return $this;
    }
}
