<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SubGroup
 *
 * @ORM\Table(name="sub_group", indexes={@ORM\Index(name="name_idx", columns={"name"})})
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\SubGroupRepository")
 * @UniqueEntity("name")
 */
class SubGroup
{
    
    const SOUS_GROUPE_TOUS = 'Tous';
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;
    
    /**
     * Many Groups have Many Users.
     * 
     * @ORM\ManyToMany(targetEntity="Distributor" , mappedBy="subGroups")
     */
    private $distributors;
    
    public $totalDistributors;
  
    public function __construct()
    {
        $this->distributors = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return SubGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add distributor.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Distributor $distributor
     *
     * @return SubGroup
     */
    public function addDistributor(\Honda\Bundle\DistributorBundle\Entity\Distributor $distributor)
    {
        $this->distributors[] = $distributor;
        $distributor->addSubGroup($this);

        return $this;
    }

    /**
     * Remove distributor.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Distributor $distributor
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDistributor(\Honda\Bundle\DistributorBundle\Entity\Distributor $distributor)
    {
        $this->distributors->removeElement($distributor);
        $distributor->removeSubGroup($this); 

        return $this;
    }

    /**
     * Get distributors.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDistributors()
    {
        return $this->distributors;
    }
    
    /**
     * Set active.
     *
     * @param $distributors
     *
     * @return SubGroup
     */
    public function setDistributors($distributors)
    {
        $this->distributors = $distributors;
        
        return $this;
    }
    
    public function getTotalDistributors()
    {
        return count($this->getDistributors());
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return 'Sous Groupe';
        }
    }

}
