<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\Bundle\DistributorBundle\Model\BasePage;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Category
 *
 * @ORM\Table(name="category_accesories")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\CategoryRepository")
 */
class Category
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\OneToMany(targetEntity="Logos", mappedBy="category")
     *
     */
    private $logos;

    /**
     * @ORM\OneToOne(targetEntity="Pictos")
     */
    private $pictos;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return BasePage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->logos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add logo.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Logos $logo
     *
     * @return Category
     */
    public function addLogo(\Honda\Bundle\DistributorBundle\Entity\Logos $logo)
    {
        $this->logos[] = $logo;

        return $this;
    }

    /**
     * Remove logo.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Logos $logo
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLogo(\Honda\Bundle\DistributorBundle\Entity\Logos $logo)
    {
        return $this->logos->removeElement($logo);
    }

    /**
     * Get logos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogos()
    {
        return $this->logos;
    }

    /**
     * Get the value of position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set the value of position
     *
     * @return  self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        } else {
            return '';
        }
    }

    /**
     * Set pictos.
     *
     * @param int $pictos
     *
     * @return Services
     */
    public function setPictos($pictos)
    {
        $this->pictos = $pictos;

        return $this;
    }

    /**
     * Get pictos.
     *
     * @return int
     */
    public function getPictos()
    {
        return $this->pictos;
    }
}
