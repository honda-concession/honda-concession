<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Metadata
 *
 * @ORM\Table(name="metadata")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\MetadataRepository")
 */
class Metadata
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="key_name", type="string", length=255)
     */
    private $keyName;

    /**
     * @var string
     *
     * @ORM\Column(name="value_name", type="string", length=255)
     */
    private $valueName;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyName.
     *
     * @param string $keyName
     *
     * @return Metadata
     */
    public function setKeyName($keyName)
    {
        $this->keyName = $keyName;

        return $this;
    }

    /**
     * Get keyName.
     *
     * @return string
     */
    public function getKeyName()
    {
        return $this->keyName;
    }

    /**
     * Set valueName.
     *
     * @param string $valueName
     *
     * @return Metadata
     */
    public function setValueName($valueName)
    {
        $this->valueName = $valueName;

        return $this;
    }

    /**
     * Get valueName.
     *
     * @return string
     */
    public function getValueName()
    {
        return $this->valueName;
    }
}
