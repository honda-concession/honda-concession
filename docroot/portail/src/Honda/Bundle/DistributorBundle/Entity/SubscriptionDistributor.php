<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;

/**
 * MenuDistributor
 *
 * @ORM\Table(name="subscription_distributor")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\SubscriptionDistributorRepository")
 */
class SubscriptionDistributor
{
    use ActivationTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="settlement", type="boolean", options={"default":"0"})
     */
    private $settlement;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_id", type="string", length=255, nullable=false)
     */
    private $billingId;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $invoice;

    /**
     * @var string
     *
     * @ORM\Column(name="period", type="string", length=255, nullable=false)
     */
    private $period;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=false)
     */
    private $endDate;

    /**
     * @var float
     *
     * @ORM\Column(name="price_with_tax", type="float", nullable=false)
     */
    private $priceWithTax = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="price_without_tax", type="float", nullable=false)
     */
    private $priceWithoutTax = 0;

    /**
     * @var Distributor
     *
     * @ORM\ManyToOne(targetEntity="Distributor", inversedBy="subscriptions")
     * @ORM\JoinColumn(name="distributor_id", referencedColumnName="id")
     */
    protected $distributor;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return MenuDistributor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of settlement
     *
     * @return  bool
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * Set the value of settlement
     *
     * @param  bool  $settlement
     *
     * @return  self
     */
    public function setSettlement(bool $settlement)
    {
        $this->settlement = $settlement;

        return $this;
    }

    /**
     * Get the value of billingId
     *
     * @return  string
     */
    public function getBillingId()
    {
        return $this->billingId;
    }

    /**
     * Set the value of billingId
     *
     * @param  string  $billingId
     *
     * @return  self
     */
    public function setBillingId(string $billingId)
    {
        $this->billingId = $billingId;

        return $this;
    }

    /**
     * Get the value of period
     *
     * @return  string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set the value of period
     *
     * @param  string  $period
     *
     * @return  self
     */
    public function setPeriod(string $period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get the value of invoice
     *
     * @return  \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set the value of invoice
     *
     * @param  \Application\Sonata\MediaBundle\Entity\Media  $invoice
     *
     * @return  self
     */
    public function setInvoice(\Application\Sonata\MediaBundle\Entity\Media $invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get the value of distributor
     *
     * @return  Distributor
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * Set the value of distributor
     *
     * @param  Distributor  $distributor
     *
     * @return  self
     */
    public function setDistributor(Distributor $distributor)
    {
        $this->distributor = $distributor;

        return $this;
    }

    /**
     * Get the value of startDate
     *
     * @return  \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set the value of startDate
     *
     * @param  \DateTime  $startDate
     *
     * @return  self
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get the value of endDate
     *
     * @return  \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set the value of endDate
     *
     * @param  \DateTime  $endDate
     *
     * @return  self
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get the value of priceWithTax
     *
     * @return  float
     */
    public function getPriceWithTax()
    {
        return $this->priceWithTax;
    }

    /**
     * Set the value of priceWithTax
     *
     * @param  float  $priceWithTax
     *
     * @return  self
     */
    public function setPriceWithTax(float $priceWithTax)
    {
        $this->priceWithTax = $priceWithTax;

        return $this;
    }

    /**
     * Get the value of priceWithoutTax
     *
     * @return  float
     */
    public function getPriceWithoutTax()
    {
        return $this->priceWithoutTax;
    }

    /**
     * Set the value of priceWithoutTax
     *
     * @param  float  $priceWithoutTax
     *
     * @return  self
     */
    public function setPriceWithoutTax(float $priceWithoutTax)
    {
        $this->priceWithoutTax = $priceWithoutTax;

        return $this;
    }
}
