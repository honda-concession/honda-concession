<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pictos
 *
 * @ORM\Table(name="pictos_accessory")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\PictosRepository")
 */
class Pictos
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $picto;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Pictos
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set picto.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $picto
     *
     * @return Distributor
     */
    public function setPicto(\Application\Sonata\MediaBundle\Entity\Media $picto = null)
    {
        $this->picto = $picto;

        return $this;
    }

    /**
     * Get picto.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getPicto()
    {
        return $this->picto;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

}
