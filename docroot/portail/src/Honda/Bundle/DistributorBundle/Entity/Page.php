<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\Bundle\DistributorBundle\Model\BasePage;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\PageRepository")
 */
class Page extends BasePage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
     * @ORM\Column(name="slug",length=128, type="string", unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255,nullable=true)
     */
    private $label;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL",nullable=true)
     */
    private $pdf;

    public function __construct()
    {
        parent::__construct($this->title);
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        } else {
            return '';
        }
    }


    /**
     * Set label.
     *
     * @param string $label
     *
     * @return Page
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set pdf.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $pdf
     *
     * @return Page
     */
    public function setPdf(\Application\Sonata\MediaBundle\Entity\Media $pdf = null)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getPdf()
    {
        return $this->pdf;
    }
}
