<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diffusion
 *
 * @ORM\Table(name="diffusion")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\DiffusionRepository")
 */
class Diffusion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ExternalSites")
     */
    private $sites;

    /**
     * @var int
     *
     * @ORM\Column(name="quotas", type="integer")
     */
    private $quotas;

    /**
     * @ORM\ManyToOne(targetEntity="Distributor", inversedBy="diffusion")
     * @ORM\JoinColumn(name="distributor_id", referencedColumnName="id")
     */
    protected $distributor;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sites.
     *
     * @param int $sites
     *
     * @return Diffusion
     */
    public function setSites($sites)
    {
        $this->sites = $sites;

        return $this;
    }

    /**
     * Get sites.
     *
     * @return int
     */
    public function getSites()
    {
        return $this->sites;
    }

    /**
     * Set quotas.
     *
     * @param int $quotas
     *
     * @return Diffusion
     */
    public function setQuotas($quotas)
    {
        $this->quotas = $quotas;

        return $this;
    }

    /**
     * Get quotas.
     *
     * @return int
     */
    public function getQuotas()
    {
        return $this->quotas;
    }

    /**
     * Get distributor
     *
     * @return string
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * Set distributor
     *
     * @param string $distributor
     *
     * @return Schedule
     */
    public function setDistributor(Distributor $distributor)
    {
        $this->distributor = $distributor;

        return $this;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return (string) $this->getQuotas();
        } else {
            return 'Sites de diffusion';
        }
    }
}
