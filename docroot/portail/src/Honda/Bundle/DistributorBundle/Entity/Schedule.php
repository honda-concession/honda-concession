<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Doctrine\ORM\Mapping as ORM;

/**
 * Distributor
 *
 * @ORM\Table(name="schedule")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\ScheduleRepository")
 */
class Schedule
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Distributor", inversedBy="schedule")
     * @ORM\JoinColumn(name="distributor_id", referencedColumnName="id")
     */
    protected $distributor;

    /**
     * @ORM\ManyToOne(targetEntity="Days", inversedBy="schedule")
     *  @ORM\JoinColumn(name="day_id", referencedColumnName="id")
     */
    protected $days;

    /**
     * @ORM\Column(type="time",nullable=true)
     */
    protected $startAt;

    /**
     * @ORM\Column(type="time",nullable=true)
     */
    protected $endAt;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $close;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get distributor
     *
     * @return string
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * Get startAt
     *
     * @return string
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set distributor
     *
     * @param string $distributor
     *
     * @return Schedule
     */
    public function setDistributor($distributor)
    {
        $this->distributor = $distributor;
        return $this;
    }

    /**
     * Get endAt
     *
     * @return string
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Get close
     *
     * @return boolean
     */
    public function getClose()
    {
        return $this->close;
    }

    /**
     * Set startAt
     *
     * @param string $startAt
     *
     * @return Schedule
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;
        return $this;
    }

    /**
     * Set endAt
     *
     * @param string $endAt
     *
     * @return Schedule
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;
        return $this;
    }

    /**
     * Set close
     *
     * @param string $close
     *
     * @return Schedule
     */
    public function setClose($close)
    {
        $this->close = $close;
        return $this;
    }

    /**
     * Set days.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\Days|null $days
     *
     * @return Schedule
     */
    public function setDays(\Honda\Bundle\DistributorBundle\Entity\Days $days = null)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days.
     *
     * @return \Honda\Bundle\DistributorBundle\Entity\Days|null
     */
    public function getDays()
    {
        return $this->days;
    }

}
