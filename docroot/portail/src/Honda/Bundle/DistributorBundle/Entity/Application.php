<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Application
 *
 * @ORM\Table(name="application")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\ApplicationRepository")
 */
class Application
{
    
    const UNIQUE_ID = 1;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lien_android", type="string", length=255)
     */
    private $lienAndroid;

    /**
     * @var string
     *
     * @ORM\Column(name="lien_ios", type="string", length=255)
     */
    private $lienIos;

    public function __toString()
    {
        return 'Liens Applications mobiles';
    }
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lienAndroid.
     *
     * @param string $lienAndroid
     *
     * @return Application
     */
    public function setLienAndroid($lienAndroid)
    {
        $this->lienAndroid = $lienAndroid;

        return $this;
    }

    /**
     * Get lienAndroid.
     *
     * @return string
     */
    public function getLienAndroid()
    {
        return $this->lienAndroid;
    }

    /**
     * Set lienIos.
     *
     * @param string $lienIos
     *
     * @return Application
     */
    public function setLienIos($lienIos)
    {
        $this->lienIos = $lienIos;

        return $this;
    }

    /**
     * Get lienIos.
     *
     * @return string
     */
    public function getLienIos()
    {
        return $this->lienIos;
    }
}
