<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\Bundle\DistributorBundle\Entity\Schedule;

/**
 * Days
 *
 * @ORM\Table(name="days")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\DaysRepository")
 */
class Days
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * 
     * @ORM\OneToMany(targetEntity="Schedule", mappedBy="days")
     * 
     */
    private $schedule;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Days
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->schedule = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add schedule.
     *
     * @param Schedule $schedule
     *
     * @return Days
     */
    public function addSchedule(Schedule $schedule)
    {
        $this->schedule[] = $schedule;
        $schedule->setDays($this);

        return $this;
    }

    /**
     * Remove schedule.
     *
     * @param Schedule $schedule
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSchedule(Schedule $schedule)
    {
        return $this->schedule->removeElement($schedule);
    }

    /**
     * Get schedule.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Set schedule.
     *
     * @param Schedule $schedule
     * 
     * @return $schedule
     */
    public function setSchedule(Schedule $schedule)
    {
        $this->schedule = $schedule;

        return $this;
    }

}
