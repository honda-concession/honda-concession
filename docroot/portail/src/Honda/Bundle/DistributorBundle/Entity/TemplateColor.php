<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TemplateColor
 *
 * @ORM\Table(name="template_color")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\TemplateColorRepository")
 */
class TemplateColor
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;
    

    /**
     * @var string
     *
     * @ORM\Column(name="main_logo", type="text")
     */
    private $mainLogo;
    
    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * 
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $slogan;
    
    /**
     * @var string
     *
     * @ORM\Column(name="set_default", type="boolean", nullable=true ,options={"default":"0"})
     */
    private $default;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return TemplateColor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slogan.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $slogan
     *
     * @return TemplateColor
     */
    public function setSlogan(\Application\Sonata\MediaBundle\Entity\Media $slogan = null)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getSlogan()
    {
        return $this->slogan;
    }
    
    /**
     * Set mainLogo.
     *
     * @param string $mainLogo
     *
     * @return TemplateColor
     */
    public function setMainLogo($mainLogo)
    {
        $this->mainLogo = $mainLogo;

        return $this;
    }

    /**
     * Get mainLogo.
     *
     * @return string
     */
    public function getMainLogo()
    {
        return $this->mainLogo;
    }
    
    /**
     * Set color.
     *
     * @param string $color
     *
     * @return TemplateColor
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
    
    /**
     * Set default.
     *
     * @param string|null $default
     *
     * @return TemplateColor
     */
    public function setDefault($default = null)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default.
     *
     * @return string|null
     */
    public function getDefault()
    {
        return $this->default;
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

}
