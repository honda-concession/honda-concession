<?php

namespace Honda\Bundle\DistributorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Honda\Bundle\DistributorBundle\Model\BasePage;

/**
 * Accessories
 *
 * @ORM\Table(name="accessories")
 * @ORM\Entity(repositoryClass="Honda\Bundle\DistributorBundle\Repository\AccessoriesRepository")
 * @UniqueEntity("type")
 */
class Accessories
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * 
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $pdf;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    private $type;

    /**
     * publier
     * @var boolean
     *
     * @ORM\Column(name="is_published", type="boolean", options={"default": 0})
     */
    private $isPublished = false;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label.
     *
     * @param string $label
     *
     * @return Accessories
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set pdf.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $pdf
     *
     * @return Accessories
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Accessories
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set isPublished.
     *
     * @param bool $isPublished
     *
     * @return Accessories
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished.
     *
     * @return bool
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        return 'Accessoires';
    }
}
