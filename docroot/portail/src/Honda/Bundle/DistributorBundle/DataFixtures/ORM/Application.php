<?php

namespace Honda\Bundle\DistributorBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Honda\Bundle\DistributorBundle\Entity\Application as Entity;

class Application extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Set loading order.
     *
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->getClassMetadata(Entity::class)->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);

        $entity = new Entity();
        $entity
            ->setId(1)
            ->setLienAndroid("https://play.google.com/store/apps/details?id=com.hondaeu.africatwin&hl=en")
            ->setLienIos("https://itunes.apple.com/gb/app/honda-motorcycles-experience/id1076347617?mt=8");

        $manager->persist($entity);
        $manager->flush();
    }
}
