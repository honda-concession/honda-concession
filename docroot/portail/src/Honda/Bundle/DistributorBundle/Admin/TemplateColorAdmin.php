<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TemplateColorAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('name')
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name')
                ->add('color', ChoiceType::class, array(
                    'choices' => array(
                        'Rouge' => 'rouge',
                        'Noir' => 'noir',
                    ))
                )
                ->add('mainLogo', 'ckeditor', [
                    'label' => 'Logo Svg path'
                ])
                ->add('slogan', 'sonata_type_model_list', [
                    'label' => 'Footer logo',
                    'required' => true,
                        ], [
                    'link_parameters' => [
                        'context' => 'default',
                        'filter' => [
                            'context' => [
                                'value' => 'default'
                            ]
                        ]
                    ]
                ])
                ->add('default', null, array('required' => false, "label" => "Appliquer par défaut"))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('name')
        ;
    }
  
}
