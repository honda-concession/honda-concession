<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class PageAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('slug', null, array(
                    'label' => 'Identifiant'
                ))
                ->add('title', null, array(
                    'label' => 'Page'
                ))
                ->add('text', 'html', array(
                    'label' => 'Description',
                    'strip' => true
                ))
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->with('Page info')
                ->add('title', null, array(
                    'label' => 'Titre'
                ))
                ->add('text', 'ckeditor', array(
                    'label' => 'Texte'
                ))
                ->end()
                // ->with('Document')
                // ->add('pdf', 'sonata_type_model_list', [
                //     'label' => 'Pdf',
                //     'required' => false,
                //         ], [
                //     'link_parameters' => [
                //         'context' => 'Default',
                //         'filter' => [
                //             'context' => [
                //                 'value' => 'Default'
                //             ]
                //         ]
                //     ]
                // ])
                // ->add('label', null, array(
                //     'label' => 'Libellé'
                // ))
                ->end()

        ;
    }

    protected function configureBatchActions($actions)
    {

        unset($actions['delete']);

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete','create');
    }

}
