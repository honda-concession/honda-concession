<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PartnersAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('name')
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name')
                ->add('logo', 'sonata_type_model_list', [
                    'label' => 'Logo',
                    'required' => false,
                        ], [
                    'link_parameters' => [
                        'context' => 'Default',
                        'filter' => [
                            'context' => [
                                'value' => 'Default'
                            ]
                        ]
                    ]
                ])
                ->add('type', ChoiceType::class, array(
                    'choices' => array(
                        'Casque' => 'casque',
                        'Vètements' => 'vetement',
                    ))
                )
        ;
    }

}
