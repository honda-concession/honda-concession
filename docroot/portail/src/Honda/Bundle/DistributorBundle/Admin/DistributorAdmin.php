<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Cocur\Slugify\Slugify;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type As Type;

use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;
use Honda\Bundle\DistributorBundle\Form\Type\DepartmentType;
use Honda\Bundle\DistributorBundle\Form\Type\RegionType;

class DistributorAdmin extends AbstractAdmin
{

    use ActivationAdminTrait;

    protected $parentAssociationMapping = 'subGroups';

    private $siteGateway;

    public function setSiteGateway($siteGateway)
    {
        $this->siteGateway = $siteGateway;

        return $this;
    }

    public function toString($object)
    {
        return $object instanceof Distributor ? "un nouveau site concessionaire" : $object->getName();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name', null, array(
                    'label' => 'Nom'
                ))
                ->add('published', null, array(
                    'label' => 'Activer'
                ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        if (!$this->isChild()) {
            $listMapper
                    ->addIdentifier('id', null, array(
                        'label' => 'Id'
                    ))
                    ->add('created', 'datetime', ['format'=>'d/m/Y', 'label' => 'Date création'])
                    ->addIdentifier('name', null, array(
                        'label' => 'Nom'
                    ))
                    ->add('codeSap', null, array(
                        'label' => 'Code SAP'
                    ))
                    ->add('websiteUrl', null, array(
                        'label' => 'URL'
                    ))
                    ->add('cadreRouge', null, array(
                        'editable' => true,
                        'label' => 'Cadre Rouge'
                    ))
                    ->add('subscriptionPayment', null, array(
                        'editable' => true,
                        'label' => 'Paiement abonnement '
                    ))
                    ->add('urlWinteam', null, array(
                        'label' => 'URL Winteam'
                    ))
                    ->add('activationVehicleAdExport', null, [
                        'label' => 'Export Winteam',
                        'editable' => true
                    ])
                    ->add('diffusion', null, array(
                        'label' => 'Quota de diffusion LeBonCoin',
                    ))
                    ->add('subGroups', 'sonata_type_model', array(
                        'multiple' => true,
                        'required' => false,
                        'by_reference' => false,
                        'label' => 'Groupes',
                        'property' => 'name'
                    ))
                    ->add('published', null, array(
                        'editable' => true,
                        "label" => "Activer"
                    ))
                    ->add('_action', null, [
                        'actions' => [
                            'edit' => [],
                            'delete' => [],
                        ],
                    ])
            ;
        } else {
            $listMapper
                    ->add('name');
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        //if (!$this->isChild()) {
            $formMapper
                    ->tab('Informations Génerales')
                        ->with(false, array('class' => 'col-md-8'))
                            ->add('name', null, array(
                                'label' => 'Nom de la concession'
                            ))
                            ->add('dcmsid', null, array(
                                'label' => 'DCMS Id'
                            ))
                            ->add('codeSap', null, array(
                                'label' => 'Code SAP',
                                'required' => false,
                            ))
                            ->add('address1',null, array(
                                'label' => 'Adresse 1'
                            ))
                            ->add('address2', null, array(
                                'label' => 'Adresse 2'
                            ))
                            ->add('codePostal', null, array(
                                'label' => 'Code Postal',
                                'required' => true
                            ))
                            ->add('city', null, array(
                                'label' => 'Ville',
                                'required' => true
                            ))
                            ->add('department', DepartmentType::class, array(
                                'placeholder' => 'Choissisez un département',
                                'label' => 'Département *',
                            ))
                            ->add('region', RegionType::class, array(
                                'placeholder' => 'Choissisez une région',
                                'label' => 'Région *'
                            ))
                            ->add('latitude')
                            ->add('longitude')
                        ->end()
                        ->with('Option de publication', array('class' => 'col-md-4'))
                            ->add('created', 'sonata_type_date_picker', array(
                                'label' => 'Date de création',
                                'format'=>'dd/MM/yyyy HH:mm:ss',
                                'attr' => array(
                                    'readonly' => true,
                                ),
                                'datepicker_use_button' => false,
                                'required' => false
                            ))
                            ->add('updated', 'sonata_type_date_picker', array(
                                'label' => 'Date mise à jour',
                                'format'=>'dd/MM/yyyy HH:mm:ss',
                                'attr' => array(
                                    'readonly' => true,
                                ),
                                'datepicker_use_button' => false,
                                'required' => false
                            ))
                        ->end()
                        ->with('Activation', array('class' => 'col-md-4'))
                            ->add('activation_start', 'sonata_type_date_picker', array(
                                'label' => 'Début',
                                'format'=>'dd/MM/yyyy HH:mm:ss',
                                'datepicker_use_button' => false,
                                'required' => false
                            ))
                            ->add('activation_end', 'sonata_type_date_picker', array(
                                'label' => 'Fin',
                                'format'=>'dd/MM/yyyy HH:mm:ss',
                                'datepicker_use_button' => false,
                                'required' => false
                            ))
                        ->end()
                        ->with('Winteam', ['class' => 'col-md-4'])
                            ->add('activationVehicleAdExport', null, [
                                'label' => 'Autoriser l’export des Petites annonces',
                                'required' => false,
                            ])
                        ->end()
                        ->with('Présentation portail', array('class' => 'col-md-8'))
                            ->add('description', 'ckeditor', array(
                                'label' => false
                            ))
                        ->end()
                    ->end()
                    ->tab('Informations Complémentaires')
                        ->with(false, array('class' => 'col-md-12', 'description' => 'URL du site, login et mot de passe, sont modifiables dans « Gestion des sites ».'))
                            ->add('websiteUrl', null, array(
                                'label' => 'URL du site',
                                'attr' => array(
                                    'readonly' => true,
                                ),
                            ))
                            ->add('websiteBoUrl', null, array(
                                'label' => 'URL d\'accès au back office',
                                'attr' => array(
                                    'readonly' => true,
                                ),
                            ))
                            ->add('admin', null, array(
                                'label' => 'Login',
                                'attr' => array(
                                    'readonly' => true,
                                ),
                            ))
                            ->add('pswd', null, array(
                                'label' => 'Mot de passe',
                                'attr' => array(
                                    'readonly' => true,
                                ),
                            ))
                            ->add('otherUrl', null, array(
                                'label' => 'Autre URL'
                            ))
                            ->add('email', null, array(
                                'label' => 'E-mail'
                            ))
                            ->add('telephone', null, array(
                                'label' => 'Téléphone'
                            ))
                            ->add('fax')
                            ->add('agentOfficiel',null, array(
                                'label' => 'Agent officiel'
                            ))
                            ->add('specialistCross',null, array(
                                'label' => 'Spécialiste cross'
                            ))
                            ->add('distributorMotor',null, array(
                                'label' => 'Concessionnaire moto'
                            ))
                        ->end()
                    ->end()
                    ->tab('Apparance')
                        ->with(false, array('class' => 'col-md-12'))
                            ->add('logo', 'sonata_type_model_list', [
                                'label' => 'Logo',
                                'required' => false,
                                    ], [
                                'link_parameters' => [
                                    'context' => 'default',
                                    'filter' => [
                                        'context' => [
                                            'value' => 'default'
                                        ]
                                    ]
                                ]
                            ])
                            /* TODO: Remove later
                            ->add('image', 'sonata_type_model_list', [
                                'label' => 'Image',
                                'required' => false,
                                    ], [
                                'link_parameters' => [
                                    'context' => 'default',
                                    'filter' => [
                                        'context' => [
                                            'value' => 'default'
                                        ]
                                    ]
                                ]
                            ])*/
                            ->add('color', 'sonata_type_model', [
                                'label' => 'Modèles',
                                'required' => false,
                                'multiple' => false,
                                'by_reference' => false,
                                'btn_add' => false,
                                    ]
                            )
                        ->end()
                    ->end()
                    ->tab('Documents')
                        ->with(false, array('class' => 'col-md-12'))
                            ->add('kbis', 'sonata_type_model_list', [
                                'label' => 'Kbis',
                                'required' => false,
                                    ], [
                                'link_parameters' => [
                                    'context' => 'default',
                                    'filter' => [
                                        'context' => [
                                            'value' => 'default'
                                        ]
                                    ]
                                ]
                            ])
                            ->add('contract', 'sonata_type_model_list', [
                                'label' => 'Contract',
                                'required' => false,
                                    ], [
                                'link_parameters' => [
                                    'context' => 'default',
                                    'filter' => [
                                        'context' => [
                                            'value' => 'default'
                                        ]
                                    ]
                                ]
                            ])
                            ->add('guide', 'sonata_type_model_list', [
                                'label' => 'Guide',
                                'required' => false,
                                    ], [
                                'link_parameters' => [
                                    'context' => 'default',
                                    'filter' => [
                                        'context' => [
                                            'value' => 'default'
                                        ]
                                    ]
                                ]
                            ])
                        ->end()
                    ->end()
                    ->tab('Statistiques')
                        ->with(false, array('class' => 'col-md-12'))
                            ->add('statsUrl')
                            ->add('gaCode')
                            ->add('gtmCode')
                            ->add('login')
                            ->add('password', null, array(
                                'label' => 'Mot de passe',
                            ))
                            ->add('dataStudioId', null, [
                                'label' => 'DataStudio ID'
                            ])
                        ->end()
                    ->end()
                    ->tab('Facturation et Abonnement')
                        ->with(false, array('class' => 'col-md-8'))
                            ->add('addSurname', 'text', array(
                                'label' => 'Nom',
                                'required' => false
                            ))
                            ->add('addName', 'text', array(
                                'label' => 'Prénom',
                                'required' => false
                            ))
                            ->add('addAddress1', 'text', array(
                                'label' => 'Adresse 1',
                                'required' => false
                            ))
                            ->add('addAddress2', 'text', array(
                                'label' => 'Adresse 2',
                                'required' => false
                            ))
                            ->add('addCodePostal', 'text', array(
                                'label' => 'Code Postal',
                                'required' => false
                            ))
                            ->add('addCity', 'text', array(
                                'label' => 'Ville',
                                'required' => false
                            ))
                            ->add('addTelephone', 'text', array(
                                'label' => 'Téléphone',
                                'required' => false
                            ))
                            ->add('addEmail', 'text', array(
                                'label' => 'E-mail',
                                'required' => false
                            ))
                        ->end()
                        ->with('Durée de l\'abonnement', array('class' => 'col-md-4'))
                            ->add('addStartDate', 'sonata_type_date_picker', array(
                                'label' => 'Date de début',
                                'required' => false,
                                'format'=>'dd/MM/yyyy HH:mm:ss',
                            ))
                            ->add('addEndDate', 'sonata_type_date_picker', array(
                                'label' => 'Date de fin',
                                'required' => false,
                                'format'=>'dd/MM/yyyy HH:mm:ss',
                            ))
                            ->add('addRemainingDays', 'text', array(
                                'label' => 'Jour restant',
                                'required' => false,
                                'attr' => array(
                                    'readonly' => true,
                                    'disabled' => true,
                                ),
                            ))
                            ->add('subscriptionPayment', null, array(
                                'label' => 'Paiement abonnement'
                            ))
                            ->add('published', null, array('required' => false, "label" => "Active"))
                        ->end()
                    ->end()
                    ->tab('Cadre Rouge')
                        ->with(false, array('class' => 'col-md-12'))
                            ->add('cadreRouge', null, array('required' => false, "label" => "Oui"))
                            ->end()
                        ->end()
                    ->tab('Sites de diffusion')
                        ->with(false, array('class' => 'col-md-12'))
                            ->add('urlWinteam', null, array(
                                    'label' => 'URL Winteam'
                                ))
                            ->add('diffusion', 'sonata_type_collection', array(
                                    'required' => false,
                                    'by_reference' => true,
                                    'label' => 'Sites de diffusion',
                                    'help' => ''
                                        ), array(
                                    'edit' => 'inline',
                                    'sortable' => 'pos',
                                    'inline' => 'table',
                                        )
                                )
                            ->end()
                        ->end()
                    ->tab('Groupes')
                        ->with(false, array('class' => 'col-md-12'))
                            ->add('subGroups', null, array(
                                'label' => 'Sous groupe'
                            ))
                        ->end()
                    ->end();
        //}
    }

    public function preUpdate($distributor)
    {
        foreach($distributor->getDiffusion() as $diffusion) {
            $diffusion->setDistributor($distributor);
        }
        parent::preUpdate($distributor);
    }

    /**
     * @inheritdoc
     */
    public function postUpdate($distributor)
    {
        parent::postUpdate($distributor);
        $site = [];
        try {
            $site = $this->siteGateway->getSite($distributor->getId());
        } catch (\Exception $e) {

        } finally {
            $slugify = new Slugify();
            $site['siteName'] = $slugify->slugify($distributor->getName(), '_');
            $site['status'] = $distributor->isPublished();
            $site['site'] = $distributor->getWebsiteUrl();
            $site['cid'] = $distributor->getId();
            $site['distributorId'] = $distributor->getId();
            $site['distributor'] = $distributor;
            $site['locale'] = 'fr';
            $site['username'] = $distributor->getAdmin();
            $site['password'] = $distributor->getPswd();
            $site['gtmCode'] = $distributor->getGtmCode();

            $this->siteGateway->update($site);
        }
    }

    protected function configureBatchActions($actions)
    {
        $actions['published'] = [
            'ask_confirmation' => false,
            'label' => 'Publier',
        ];
        $actions['unpublished'] = [
            'ask_confirmation' => false,
            'label' => 'Dépublier',
        ];
        unset($actions['delete']);

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }
}
