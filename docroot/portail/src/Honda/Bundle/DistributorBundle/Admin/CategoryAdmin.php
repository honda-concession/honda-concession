<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Honda\Bundle\DistributorBundle\Entity\Pictos;

class CategoryAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('position', 'actions', array(
                'actions' => array(
                    'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig')
                )
            ))
            ->addIdentifier('title', null, array(
                'label' => 'Catégories',
            ))
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @return array
     */
    protected function getPictos()
    {
        $collection = [];

        $container = $this->getConfigurationPool()->getContainer();

        $em = $container->get('doctrine.orm.default_entity_manager');
        $pictos = $em->getRepository(Pictos::class)->findAll();

        foreach ($pictos as $picto) {

            $provider = $container->get($picto->getPicto()->getProviderName());
            $url =  $provider->generatePublicUrl($picto->getPicto(), 'reference');

            $collection[$url] = $picto;
        }

        return $collection;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $pictos = $this->getPictos();

        $formMapper
            ->with('Contenu', array('class' => 'col-md-8'))
                ->add('title', 'text', array(
                    'label' => 'Nom'
                ))
                ->add('pictos', ChoiceType::class, [
                    'required' => true,
                    'expanded' => true,
                    'attr' => ["class" => 'services-picto'],
                    'label' => 'Pictos',
                    'choices' => $pictos,
                ])
            ->end();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
        parent::configureRoutes($collection);
    }
}
