<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Doctrine\ORM\EntityRepository;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Knp\Menu\ItemInterface as MenuItemInterface;

use Honda\Bundle\DistributorBundle\Entity\Distributor;

class SubGroupAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name' , null, array(
                'label' => 'Nom du groupe'
            ))
            ->add('totalDistributors', null, array(
                'label' => 'Nombre de concessions'
            ))
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'label' => 'Nom du nouveau groupe'
            ))
            ->add('distributors', EntityType::class, array (
                'label' => 'Liste des concessionaires',
                'class' => Distributor::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->orderBy('d.name', 'ASC');
                },
                'required' => false,
                'multiple' => true,
                'by_reference' => false,
                'choice_label' => 'name',
                'choice_value' => 'id',
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
        ;
    }

    protected function configureBatchActions($actions)
    {
        unset($actions['delete']);

        return $actions;
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit','show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        if ($this->isGranted('EDIT')) {
            $menu->addChild('Modifier les concessionaires', [
                'uri' => $admin->generateUrl('edit', ['id' => $id])
            ]);
        }

        if ($this->isGranted('LIST')) {
            $menu->addChild('Liste des concessionaires', [
                'uri' => $admin->generateUrl('honda_distributor.admin.distributor.list', ['id' => $id])
            ]);
        }
    }

}
