<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;


class SocialNetworkAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('lienFb')
                ->add('lienInsta')
                ->add('lienTwitter')
                ->add('lienYoutube')
                ->add('lienSnapchat')
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('lienFb', null, array(
                    'label' => 'Lien Facebook'
                ))
                ->add('lienInsta', null, array(
                    'label' => 'Lien Instagram'
                ))
                ->add('lienTwitter', null, array(
                    'label' => 'Lien Twitter'
                ))
                ->add('lienYoutube', null, array(
                    'label' => 'Lien Youtube'
                ))
                ->add('lienSnapchat', null, array(
                    'label' => 'Lien Snapchat'
                ))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list')->remove('delete')->remove('create');
    }

}
