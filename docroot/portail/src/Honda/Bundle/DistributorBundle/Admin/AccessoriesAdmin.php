<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AccessoriesAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Accessoires')
                ->add('label', null, array (
                    'required' => false,
                    'label' => 'Libellé'
                ))
                ->add('pdf', 'sonata_type_model_list', [
                    'label' => 'Pdf',
                    'required' => false,
                        ], [
                    'link_parameters' => [
                        'context' => 'file',
                        'filter' => [
                            'context' => [
                                'value' => 'file'
                            ]
                        ]
                    ]
                ])
                ->add('type', ChoiceType::class, [
                    'label' => 'Type',
                    'choices' => [
                        'Accessoires Moto' => 'moto',
                        'Equipements du motard' => 'equipements',
                        'Vêtements Honda' => 'vetement'
                    ]
                ])
                ->add('isPublished', null, [
                        'label' => 'Publié',
                        'required' => false,
                    ]
                )
            ->end()
        ;
    }

    protected function configureBatchActions($actions)
    {
        
        unset($actions['delete']);
        
        return $actions;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list')->remove('delete')->remove('create');
    }

    
}
