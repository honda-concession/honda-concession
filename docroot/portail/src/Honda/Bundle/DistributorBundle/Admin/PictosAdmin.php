<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PictosAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('picto', 'string', [
                    'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig',
                    'label' => 'Picto'
                ])
            ->add('name', null, array(
                'label' => 'Nom'
            ))
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'label' => 'Intitulé du picto'
            ))
            ->add('picto', 'sonata_type_model_list', [
                'label' => 'Picto',
                    ], [
                'link_parameters' => [
                    'context' => 'icon',
                    'filter' => [
                        'context' => [
                            'value' => 'icon'
                        ]
                    ]
                ]
            ])
        ;
    }

}
