<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ApplicationAdmin extends AbstractAdmin
{
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('lienAndroid')
            ->add('lienIos')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('lienAndroid')
            ->add('lienIos')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list')->remove('delete')->remove('create');
    }

}
