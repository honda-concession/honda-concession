<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\CoreBundle\Form\Type\CollectionType;

class ScheduleAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('startAt')
                ->add('endAt')
                ->add('close')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('startAt')
                ->add('endAt')
                ->add('close')
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('days', 'sonata_type_model', [
                    'label' => 'Jour',
                    'required' => false,
                    'btn_add' => false,
                ])
                ->add('startAt', null, array(
                    'label' => 'Ouverture'
                ))
                ->add('endAt', null, array(
                    'label' => 'Fermeture'
                ))
                ->add('close', null, array(
                    'label' => 'Fermer'
                ))
        ;
    }

}
