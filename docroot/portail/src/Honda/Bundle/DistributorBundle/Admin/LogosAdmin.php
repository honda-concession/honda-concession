<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class LogosAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('logo','string', [
                'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'
                ])
            ->add('name', null, array(
                'label' => 'Marque'
            ))
            ->add('category', null, array(
                'label' => 'Catégorie'
            ))
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('logo', 'sonata_type_model_list', [
                'label' => 'Image',
                'required' => false,
                    ], [
                'link_parameters' => [
                    'context' => 'accessories',
                    'filter' => [
                        'context' => [
                            'value' => 'accessories'
                        ]
                    ]
                ]
            ])
            ->add('category', 'sonata_type_model', [
                    'label' => 'Catégorie',
                    'required' => false,
                    'multiple' => false,
                    'by_reference' => false,
                    'btn_add' => false,
                ])
            ->add('name', null, array(
                'label' => 'Intitulé du logo'
            ))
        ;
    }

}
