<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints As Constraints;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Honda\Bundle\DistributorBundle\Entity\SubscriptionDistributor;
use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;

class SubscriptionDistributorAdmin extends AbstractAdmin
{
    use ActivationAdminTrait;

    protected $baseRoutePattern = 'subscription';
    protected $baseRouteName = 'subscription';

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('id')
                ->add('distributor.name', null, ['label' => 'Nom'])
                ->add('published', null, ['label' => 'Publié', 'editable' => true,])
                ->add('billingId', null, ['template' => 'SonataMediaBundle:MediaAdmin:invoice.html.twig'])
                ->add('priceWithoutTax', null, ['label' => 'Montant HT (€)'])
                ->add('priceWithTax', null, ['label' => 'Montant TTC (€)'])
                ->add('period', null)
                ->add('settlement', null, ['editable' => true,])
                ->add('_action', 'actions', [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $years = [];

        foreach (array_reverse(range(date('Y'), 2050)) as $year) {
            $years[$year] = $year;
        }
        $formMapper
            ->add('distributor', EntityType::class, [
                    'label' => 'Concessionnaire',
                    'class' => Distributor::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                            ->orderBy('d.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'constraints' => [new Constraints\NotBlank()],
                    'attr' => ['class' => 'distributor-select'],
                ]
            )
            ->add('period', ChoiceType::class, [
                'choices' => $years,
            ])
            ->add('startDate', 'sonata_type_date_picker', array(
                'label' => 'Date de début',
                'required' => false,
                'format'=>'dd/MM/yyyy HH:mm:ss',
            ))
            ->add('endDate', 'sonata_type_date_picker', array(
                'label' => 'Date de fin',
                'required' => false,
                'format'=>'dd/MM/yyyy HH:mm:ss',
            ))
            ->add('billingId')
            ->add('invoice', 'sonata_type_model_list', [
                'required' => false,
                    ], [
                'link_parameters' => [
                    'context' => 'file',
                    'filter' => [
                        'context' => [
                            'value' => 'file'
                        ]
                    ]
                ]
            ])
            ->add('priceWithoutTax', null, ['label' => 'Montant HT (€)'])
            ->add('priceWithTax', null, ['label' => 'Montant TTC (€)'])
            ->add('settlement')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $dataGridMapper)
    {
        $dataGridMapper
            ->add('distributor.name', null, array(
                'label' => 'Nom de concessionnaire'
            ))
            ->add('settlement', null, array(
                'label' => 'Réglé'
            ))
        ;
    }

    public function toString($object)
    {
        return $object instanceof SubscriptionDistributor ? "la facture du {$object->getDistributor()}" : "nouvelle facture";
    }
}
