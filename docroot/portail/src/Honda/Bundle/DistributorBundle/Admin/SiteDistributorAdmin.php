<?php

namespace Honda\Bundle\DistributorBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;

class SiteDistributorAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'distributor-sites';
    protected $baseRouteName = 'distributor_sites';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        $collection->add('create');
        $collection->add('confirm');
        $collection->add('editSite', $this->getRouterIdParameter().'/edit-site', [], [], [], '', [], []);
    }
}
