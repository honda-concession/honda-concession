<?php

namespace Honda\Bundle\DistributorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Honda\Bundle\DistributorBundle\Model\Region;

class RegionType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => Region::REGIONS,
            'required' => false,
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

}
