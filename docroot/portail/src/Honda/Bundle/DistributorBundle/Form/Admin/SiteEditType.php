<?php

namespace Honda\Bundle\DistributorBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type As Type;
use Symfony\Component\Validator\Constraints As Constraints;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Honda\Bundle\DistributorBundle\Entity\Distributor;

/**
 * Class SiteEditType
 * @package Honda\Bundle\DistributorBundle\Form\Admin
 */
class SiteEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('site', Type\TextType::class, [
                    'label' => 'URL finale sans https://',
                    'constraints' => [
                        new Constraints\NotBlank(),
                        new Constraints\Regex('/^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,8}(:[0-9]{1,5})?(\/.*)?$/'),
                    ]
                ]
            )
            ->add('subDomain', Type\TextType::class, [
                'label' => 'Sous-domaine (facultatif – pour information)',
                'required' => false,
            ])
            ->add('commercialSite', Type\TextType::class, [
                'label' => 'URL commerciale (facultatif – pour information)',
                'required' => false,
            ])
            ->add('username', Type\EmailType::class, [
                'label' => 'Utilisateur',
                'attr' => [
                    'placeholder' => 'distributor@domain.com'
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Email()
                ]
            ])
            ->add('password', Type\TextType::class, [
                'label' => 'Mot de passe',
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Length(['min' => 3])
                ]
            ])
            ->add('status', Type\CheckboxType::class, [
                    'label' => 'Actif',
                    'required' => false,
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'csrf_protection' => false,
            'label' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_distributorbundle_admin_site_edit';
    }
}
