<?php

namespace Honda\Bundle\DistributorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Honda\Bundle\DistributorBundle\Entity\Application;

class DefaultController extends Controller
{
    public function applicationLinkAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Application::class);
        $applicationLink = $repository->findOneBy([]);

        return $this->render('HondaDistributorBundle:Default:applicationLink.html.twig', compact('applicationLink'));
    }
}
