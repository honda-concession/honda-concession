<?php

namespace Honda\Bundle\DistributorBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\AdminBundle\Controller\CRUDController;
use Honda\Bundle\DistributorBundle\Form\Admin\SiteType;
use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Honda\Bundle\DistributorBundle\Form\Admin\SiteEditType;
use Cocur\Slugify\Slugify;
use GuzzleHttp\Exception\ClientException;

class SiteDistributorAdminController extends CRUDController
{

    /**
     * Admin Distributor Site
     */
    public function listAction()
    {
        $client = $this->container->get('honda_distributor.site_gateway');

        $sites = $client->getSites();

        return $this->renderWithExtraParams('HondaDistributorBundle:Admin:sonata_site_distributor_list.html.twig', [
                'sites' => $sites
            ]
        );
    }

    /**
     * Create site
     */
    public function createAction()
    {
        $request = $this->getRequest();

        $site = $request->query->all();

        $form = $this->createForm(SiteType::class, $site);

        if ($request->isMethod('GET')) {
            if (isset($site['distributor'])) {
                $distributor = $this->getDoctrine()->getRepository(Distributor::class)->find($site['distributor']);
                if ($distributor) {
                    $form->get('distributor')->setData($distributor);
                }
            }
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $site = $form->getData();
            $site['distributor'] = $site['distributor']->getId();

            return $this->redirect($this->generateUrl('distributor_sites_confirm', $site));
        }

        return $this->renderWithExtraParams('HondaDistributorBundle:Admin:sonata_site_distributor_create.html.twig', [
                'form' => $form->createView(),
                'create' => true,
            ]
        );
    }

    /**
     * Create site
     */
    public function confirmAction()
    {
        $request = $this->getRequest();

        $site = $request->query->all();

        $repo = $this->getDoctrine()->getRepository(Distributor::class);

        $distributor = $repo->find($site['distributor'] ?? 0);

        if (!$request->isMethod('POST')) {

            $form = $this->createForm(SiteType::class, $site);

            $form->submit($site);

            if (!$form->isValid() && !$distributor) {
                $this->addFlash(
                        'error', 'Données invalides'
                );

                return $this->redirect($this->generateUrl('distributor_sites_create', $site));
            }
        } else {

            $form = $this->createForm(SiteType::class, $site);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $client = $this->container->get('honda_distributor.site_gateway');
                $res = $client->create($form->getData());
                $data = $form->getData();
                $distributorId = $data['distributor']->getId();
                $em = $this->getDoctrine()->getManager();
                $distributor = $em->getRepository(Distributor::class)->find($distributorId);

                $distributor
                    ->setPublished($site['status'])
                    ->setWebsiteUrl($site['site'])
                    ->setSubDomain(isset($site['subDomain']) ? $site['subDomain']:'')
                    ->setCommercialUrl(isset($site['commercialSite']) ? $site['commercialSite']:'')
                    ->setAdmin($site['username'])
                    ->setPswd($site['password'])
                ;

                $em->persist($distributor);
                $em->flush();

                /*$successfullyRegistered = $this->register($data['username'], $data['username'], $data['password'], $distributorId);

                if ($successfullyRegistered) {
                    // the user is now registered !
                } else {
                    // the user exists already !
                }*/

                if (empty($res) || isset($res['errors'])) {

                    if (isset($res['errors'])) {

                        foreach ($res['errors'] as $key => $error) {
                            $this->addFlash('error', $error);
                        }
                    }

                    return $this->redirect($this->generateUrl('distributor_sites_create', $site));
                } else {

                    $this->addFlash(
                            'success', 'Site du concessionnaire a été bien créé.'
                    );
                    return $this->redirect($this->generateUrl('distributor_sites_list'));
                }
            }
        }

        return $this->renderWithExtraParams('HondaDistributorBundle:Admin:sonata_site_distributor_confirm.html.twig', [
            'create' => true,
            'form' => $form->createView(),
            'distributor' => $distributor,
            'site' => $site
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editSiteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $distributor = $em->getRepository(Distributor::class)->find($id);

        if (!$distributor) {
            throw new NotFoundHttpException();
        }

        $client = $this->container->get('honda_distributor.site_gateway');

        $site = $this->getSite($distributor);

        $form = $this->createForm(SiteEditType::class, $site);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $site = $form->getData();

            if (preg_match('/concession\.honda-occasions\.kora\.pro$/', $site['site'])) {
                $site['siteName'] = 'distributor';
            }

            if ($client->update($site)) {

                $distributor
                    ->setPublished($site['status'])
                    ->setWebsiteUrl($site['site'])
                    ->setSubDomain($site['subDomain'])
                    ->setCommercialUrl($site['commercialSite'])
                    ->setAdmin($site['username'])
                    ->setPswd($site['password'])
                ;

                $em->persist($distributor);
                $em->flush();
            }

            $this->addFlash(
                    'success', 'Site du concessionnaire a été correctement mis à jour.'
            );

            return $this->redirect($this->generateUrl('distributor_sites_list'));
        }

        return $this->renderWithExtraParams('HondaDistributorBundle:Admin:sonata_site_distributor_create.html.twig', [
            'create' => false,
            'form' => $form->createView()
        ]);
    }

    private function getSite($distributor)
    {
        $client = $this->container->get('honda_distributor.site_gateway');
        $tmp = $site = $client->getSite($distributor->getId());

        $site['subDomain'] = $distributor->getSubDomain();
        $site['commercialSite'] = $distributor->getCommercialUrl();
        $site['username'] = $distributor->getAdmin();
        $site['password'] = $distributor->getPswd();
        $slugify = new Slugify();
        $site['siteName'] = $slugify->slugify($distributor->getName(), '_');
        $site['host'] = $distributor->getWebsiteUrl();
        $site['active'] = true;

        unset($site['host']);
        unset($site['active']);

        $site['site'] = $tmp['host'] ?? '';
        $site['status'] = (boolean) $tmp['active'];

        return $site;
    }

    /**
     * This method registers an user in the database manually.
     *
     * @return boolean User registered / not registered
     * */
    private function register($email, $username, $password , $distributorId)
    {
        $userManager = $this->get('fos_user.user_manager');

        $email_exist = $userManager->findUserByEmail($email);

        // Check if the user exists to prevent Integrity constraint violation error in the insertion
        if ($email_exist) {
            return false;
        }

        $user = $userManager->createUser();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setEmailCanonical($email);
        // $user->setLocked(0); // don't lock the user
        $user->setEnabled(1); // enable the user or enable it later with a confirmation token in the email
        // this method will encrypt the password with the default settings :)
        $user->setPlainPassword($password);
        $user->setDistributor($distributorId);
        $userManager->updateUser($user);

        return true;
    }
}
