<?php

namespace Honda\Bundle\DistributorBundle\Controller\Api;

use Honda\Bundle\DistributorBundle\Model\Departement;
use Honda\Bundle\DistributorBundle\Model\Region;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Honda\Bundle\DistributorBundle\Entity\Application;
use Honda\Bundle\CommonBundle\Service\MediaUrl;
use Honda\Bundle\CommonBundle\Tools\Encryption\EncryptorAesEcb;

class DistributorController extends Controller
{

    /**
     * @Route("/api/distributor/{id}", methods={"GET","POST"})
     *
     */
    public function distributorAction(Request $request, $id)
    {

        $token = $request->headers->get('X-AUTH-TOKEN');

        $em = $this->getDoctrine()->getManager();

        $distributor = $em->getRepository('HondaDistributorBundle:Distributor')->find($id);

        if ($request->isMethod(Request::METHOD_GET)) {

            $this->checkToken($token, $id);

            $data = [];
            $data['id'] = $distributor->getId();
            $data['name'] = $distributor->getName();
            $data['address'] = $distributor->getAddress1();
            $data['other_address'] = $distributor->getAddress2();
            $data['cp'] = $distributor->getCodePostal();
            $data['town'] = $distributor->getCity();
            $data['lat'] = $distributor->getLatitude();
            $data['lng'] = $distributor->getLongitude();
            $data['location'] = $distributor->getLocation();
            $data['department'] = $distributor->getDepartment();
            $data['region'] = $distributor->getRegion();
            $data['url'] = $distributor->getWebsiteUrl();
            $data['phone'] = $distributor->getTelephone();
            $data['fax'] = $distributor->getFax();
            $data['mail'] = $distributor->getEmail();
            $data['dcmsid'] = $distributor->getDcmsid();
            $data['gaCode'] = $distributor->getGaCode();
            $data['gtmCode'] = $distributor->getGtmCode();
            $data['pswd'] = $distributor->getPswd();
            $data['dataStudioId'] = $distributor->getDataStudioId();
            $data['diffusion'] = [];
            foreach ($distributor->getDiffusion() as $diffusion) {
                if ($site = $diffusion->getSites()) {
                    $data['diffusion'][$site->getName()] = $diffusion->getQuotas();
                }
            }

            return new JsonResponse($data);

        } elseif($request->isMethod(Request::METHOD_POST)) {

            $content = $request->getContent();

            $data = json_decode($content, true);

            $this->checkToken($token, implode('-',(array)$data));

            $mapping = [
                'name' => 'Name',
                'address' => 'Address1',
                'other_address' => 'Address2',
                'cp' => 'CodePostal',
                'town' => 'City',
                'lat' => 'Latitude',
                'lng' => 'Longitude',
                'location' => 'Location',
                'department' => 'Department',
                'region' => 'Region',
                'url' => 'WebsiteUrl',
                'phone' => 'Telephone',
                'fax' => 'Fax',
                'insertPicture' => 'InsertPicture'
            ];

            foreach ($data as $key => $value) {
                if (isset($mapping[$key]))
                    call_user_func([$distributor, 'set'.$mapping[$key]], $value);
            }

            $em->persist($distributor);
            $em->flush();

            return new JsonResponse();
        }
    }

    /**
     * @Route("/api/accessories/logos/{cat_id}",requirements={"cat_id": "\d+"})
     * @Method("GET")
     *
     */
    public function listHtmletLogosAction(Request $request, $cat_id)
    {

        $logos = $this->getDoctrine()->getRepository('HondaDistributorBundle:Logos')->findBy(array('category' => $cat_id));
        $mm = $this->container->get('sonata.media.manager.media');
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $data = [];

        foreach ($logos as $key => $logo) {
            $media = $mm->findOneBy(array('id' => $logo->getLogo()));
            $provider = $this->container->get($media->getProviderName());
            $imageUrl = $baseurl . $provider->generatePublicUrl($media, 'reference');
            $data[$key]['id'] = $logo->getId();
            $data[$key]['url'] = $imageUrl;
            $data[$key]['mark'] = '';
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/page/seo/{slug}")
     * @Method("GET")
     *
     */
    public function seoPageAction($slug)
    {
        $seo = $this->getDoctrine()->getRepository('HondaDistributorBundle:Page')->findOneBy(array('slug' => $slug));

        $data = [];

        if($seo) {
            $data['title'] = $seo->getTitle();
            $data['text'] = $seo->getText();
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/mobile-links", name="api_get_mobile_links", requirements={"id": "1"})
     * @Method("GET")
     *
     */
    public function applicationLinkAction()
    {

        $link = $this->getDoctrine()->getRepository('HondaDistributorBundle:Application')->find(1);

        $data = [];

        $data['android'] = $link ? $link->getLienAndroid() : null;
        $data['ios'] = $link ? $link->getLienIos() : null;

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/social-network", name="api_get_social", requirements={"id": "1"})
     * @Method("GET")
     *
     */
    public function socialLinkAction()
    {

        $link = $this->getDoctrine()->getRepository('HondaDistributorBundle:Application')->find(1);

        $data = [];

        $data['facebook'] = $link ? $link->getLienFb() : null;
        $data['instagram'] = $link ? $link->getLienInsta() : null;
        $data['twitter'] = $link ? $link->getLienTwitter() : null;
        $data['youtube'] = $link ? $link->getLienYoutube() : null;

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/accessories/category")
     * @Method("GET")
     *
     */
    public function accessoriesCategory(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository('HondaDistributorBundle:Category')->findAll();
        $data = [];
        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $mediaManager = $this->container->get('sonata.media.manager.media');
        foreach ($categories as $category) {
            $item = [];
            $item['id'] = $category->getId();
            $item['title'] = $category->getTitle();
            $item['icon'] = null;
            $media = null;
            if ($picto = $category->getPictos()) {
                if ($media = $picto->getPicto()) {
                    $provider = $this->container->get($media->getProviderName());
                    $item['icon'] = $baseUrl . $provider->generatePublicUrl($media, 'reference');
                }
            }
            $item['logos'] = [];
            $logo = [];
            foreach ($category->getLogos() as $logo) {
                if (!$logo->getLogo()) {
                    continue;
                }
                $logoItem['id'] = $logo->getId();
                $media = $mediaManager->findOneBy(array('id' => $logo->getLogo()->getId()));
                if ($media) {
                    try {
                        $provider = $this->container->get($media->getProviderName());
                        $imageUrl = $baseUrl . $provider->generatePublicUrl($media, 'reference');
                        $logoItem['url'] = $imageUrl;
                    } catch(\Exception $e) {
                        continue;
                    }
                }
                $item['logos'][] = $logoItem;
            }
            $data[] = $item;
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/accessories-catalogue/{type}", requirements={"id": "moto|equipements|vetement"})
     * @Method("GET")
     *
     */
    public function accessoriesLinkAction($type)
    {
        $link = $this->getDoctrine()->getRepository('HondaDistributorBundle:Accessories')->findOneBy(['type' => $type, 'isPublished' => true]);
        $baseUrl = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();

        $data = [];

        if ($link) {
            $data['label'] = $link->getLabel();
            $data['link'] = null;
            if($link && $link->getPdf()){
                $data['link'] = $baseUrl . $this->get(MediaUrl::class)->getUrl($link->getPdf(), 'reference');
            }
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/regions")
     * @Method("GET")
     *
     */
    public function regionAction()
    {
        return new JsonResponse(Region::REGIONS);
    }

    /**
     * @Route("/api/departements")
     * @Method("GET")
     *
     */
    public function departementsAction()
    {
        return new JsonResponse(Departement::DEPARTEMENTS);
    }

    public function checkToken($token, $content)
    {
        $key = $this->container->getParameter('api_multisite_token');

        $encodeToken = EncryptorAesEcb::encode($content, $key);

        if($token !== $encodeToken) {
            throw new NotFoundHttpException("Page not found.");
        }
    }
}
