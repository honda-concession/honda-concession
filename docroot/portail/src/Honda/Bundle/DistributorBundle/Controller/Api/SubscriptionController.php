<?php

namespace Honda\Bundle\DistributorBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Honda\Bundle\DistributorBundle\Entity\Application;
use Honda\Bundle\CommonBundle\Service\MediaUrl;
use Honda\Bundle\CommonBundle\Tools\Encryption\EncryptorAesEcb;

class SubscriptionController extends Controller
{
    /**
     * @Route("/api/subscription/{id}", methods={"POST"})
     */
    public function listAction(Request $request, $id)
    {
        $data = [];
        $em = $this->getDoctrine()->getManager();
        $distributor = $em->getRepository('HondaDistributorBundle:Distributor')->find($id);
        $baseUrl = $request->getSchemeAndHttpHost();

        if($request->isMethod(Request::METHOD_POST)) {
            foreach ($distributor->getSubscriptions() as $subscription) {
                if (!$subscription->isPublished()) {
                    continue;
                }
                $invoiceUrl = null;
                if ($media = $subscription->getInvoice()) {
                    $invoiceUrl = $baseUrl . $this->get(MediaUrl::class)->getUrl($media, 'reference');
                }
                $data[] = [
                    'reference' => $subscription->getBillingId(),
                    'invoice' => $invoiceUrl,
                    'year' => $subscription->getPeriod(),
                    'period' => "{$subscription->getStartDate()->format('d/m/Y')} - {$subscription->getEndDate()->format('d/m/Y')}",
                    'price' => $subscription->getPriceWithTax(),
                    'settlement' => $subscription->getSettlement(),
                ];
            }
        }

        return new JsonResponse($data);
    }
}