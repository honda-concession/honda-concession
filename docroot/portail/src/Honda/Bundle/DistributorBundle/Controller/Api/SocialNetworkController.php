<?php

namespace Honda\Bundle\DistributorBundle\Controller\Api;

use Honda\Bundle\DistributorBundle\Entity\SocialNetwork;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class SocialNetworkController extends Controller
{
    /**
     * @Route("/api/social/network/links", methods={"GET"})
     */
    public function distributorAction()
    {
        $em = $this->getDoctrine()->getManager();
    
        $socialNetwork = $em->getRepository(SocialNetwork::class)->find(SocialNetwork::UNIQ_ID);
        
        if (!$socialNetwork) {
            return $this->json([
                    'facebook' => '',
                    'instagram' => '',
                    'twitter' => '',
                    'youtube' => '',
                    'snapchat' => '',
                ]
            );
        }
        
        return $this->json([
                'facebook' => $socialNetwork->getLienFb(),
                'instagram' => $socialNetwork->getLienInsta(),
                'twitter' => $socialNetwork->getLienTwitter(),
                'youtube' => $socialNetwork->getLienYoutube(),
                'snapchat' => $socialNetwork->getLienSnapchat(),
            ]
        );
        
    }

}
