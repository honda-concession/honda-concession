<?php

namespace Honda\Bundle\DistributorBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Honda\Bundle\DistributorBundle\Entity\Application;
use Symfony\Component\Routing\Annotation\Route;


class ApplicationController extends Controller
{
    /**
     * @Route("/api/applications/links", methods={"GET"})
     */
    public function distributorAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $application = $em->getRepository(Application::class)->find(Application::UNIQUE_ID);
        
        if (!$application) {
            return $this->json(['android' => '', 'ios' => '']);
        }
        
        return $this->json(['android' => $application->getLienAndroid(), 'ios' => $application->getLienIos()] );
        
    }

}
