<?php

namespace Honda\Bundle\SliderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class BannerAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('image', 'string', [
                    'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig',
                    'label' => 'Banniere'
                ])
                ->add('text', null,[
                    'label' => 'Texte'
                ])
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->with('Contenu', array('class' => 'col-md-8'))
                ->add('image', 'sonata_type_model_list', [
                    'label' => 'Image',
                    'by_reference' => false,
                    'required' => false,
                        ], [
                    'link_parameters' => [
                        'context' => 'image',
                    ]
                ])
                ->add('text' , null,[
                    'label' => 'Texte'
                ])
                ->add('sliders' , 'sonata_type_collection',[
                    'label' => 'Sliders',
                    'by_reference' => true,
                    ], [
                        'edit' => 'inline',
                        'sortable' => 'pos',
                        'inline' => 'table',
                    ]
                )
                ->end()
                ->with('Durée', array('class' => 'col-md-4'))
                    ->add('activation_start', 'sonata_type_date_picker', array(
                        'label' => 'Date début d\'activation',
                        'format'=>'dd/MM/yyyy HH:mm:ss',
                        'datepicker_use_button' => false,
                        'required' => false
                    ))
                    ->add('activation_end', 'sonata_type_date_picker', array(
                        'label' => 'Date fin d\'activation',
                        'format'=>'dd/MM/yyyy HH:mm:ss',
                        'datepicker_use_button' => false,
                        'required' => false
                    ))
                ->end()
//                ->with('Meta data', array('class' => 'col-md-4'))
//                ->add('created', 'sonata_type_date_picker', array(
//                    'label' => 'Creation date',
//                    'attr' => array(
//                        'readonly' => true,
//                    ),
//                ))
//                ->add('updated', 'sonata_type_date_picker', array(
//                    'label' => 'Last Modified',
//                    'attr' => array(
//                        'readonly' => true,
//                    )
//                ))
//                ->end()
        ;
    }
    
    protected function configureBatchActions($actions)
    {

        unset($actions['delete']);

        return $actions;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list')->remove('delete')->remove('create');
    }
    
        /**
     * @inheritdoc
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        foreach ($object->getSliders() as $slider) {
            $slider->setBanner($object);
        }
    }

    /**
     * @inheritdoc
     */
    public function preUpdate($object)
    {
        parent::preUpdate($object);
        foreach ($object->getSliders() as $slider) {
            $slider->setBanner($object);
        }
    }

}