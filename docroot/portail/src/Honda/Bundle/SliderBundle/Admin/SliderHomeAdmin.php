<?php

namespace Honda\Bundle\SliderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class SliderHomeAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('image', 'sonata_type_model_list', [
                    'required' => true,
                    'label' => 'Image',
                    'by_reference' => true,
                    'btn_delete' => false,
                    'btn_edit' => false
                        ], [
                    'link_parameters' => [
                        'context' => 'image',
                        'filter' => [
                            'context' => [
                                'value' => 'image'
                            ]
                        ]
                    ]
                        ]
                )
        ;
    }

}
