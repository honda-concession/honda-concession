<?php

namespace Honda\Bundle\SliderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;

use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;

class SliderAdmin extends AbstractAdmin
{

    use ActivationAdminTrait;

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array(
                'label' => 'Nom de l\'image'
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('position_', 'actions', array(
                'actions' => array(
                    'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig')
                )
            ))
            ->add('media', 'string', [
                'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig',
                'label' => 'Vignette'
            ])
            ->add('name', null, array(
                'label' => 'Nom de l\'image'
            ))
            ->add('subGroup', null, array(
                'label' => 'Nom du groupe'
            ))
            ->add('published', null, ['editable' => true])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Slider', array('class' => 'col-md-8'))
                ->add('media', ModelListType::class, [
                    'label' => 'Media',
                    'required' => false,
                    ], [
                            'link_parameters' => ['context' => 'slider']
                        ]
                )
                ->add('imageYoutube', ModelListType::class, [
                    'label' => 'Image à utiliser lors de l\'utilisation d\'une vidéo youtube',
                    'required' => false,
                ], [
                        'link_parameters' => ['context' => 'image']
                    ]
                )
                ->add('name', null, array(
                    'label' => 'Nom',
                    'required' => true
                ))
                ->add('link', null, array(
                    'label' => 'Lien'
                ))
                ->add('linkExternal', null, [])
                ->add('subGroups', 'sonata_type_model', [
                    'multiple' => true,
                    'required' => false,
                    'by_reference' => false,
                    'label' => 'Groupes',
                    'property' => 'name',
                    'btn_add' => false
                ])
            ->end()
            ->with('Activation', array('class' => 'col-md-3'))
                ->add('startDate', 'sonata_type_datetime_picker', array(
                    'label' => 'Date de début de publication',
                    'format' => 'dd/MM/yyyy HH:mm:ss',
                ))
                ->add('endDate', 'sonata_type_datetime_picker', array(
                    'label' => 'Date de fin de publication',
                    'format' => 'dd/MM/yyyy HH:mm:ss',
                ))
                ->add('published', null, [
                    'label' => 'Publié',
                    'required' => false,
                    ]
                )
            ->end()
        ;
    }
}
