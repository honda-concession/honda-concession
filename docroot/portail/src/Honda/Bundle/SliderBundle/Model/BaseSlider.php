<?php

namespace Honda\Bundle\SliderBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Honda\Bundle\CommonBundle\Model\BaseModel;

/**
 * Slider
 * @ORM\MappedSuperclass
 * 
 */
abstract class BaseSlider extends BaseModel
{

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * 
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL",nullable=true)
     */
    protected $image;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    protected $text;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    protected $link;
    
    /**
     * @var string
     *
     * @ORM\Column(name="link_externe", type="boolean", nullable=true, options={"default":"0"})
     */
    protected $linkExternal;

    /**
     * Set image.
     *
     * @param MediaInterface $image $image
     *
     * @return Slider
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return MediaInterface
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return Slider
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return SliderPortail
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
    
    public function getLinkExternal()
    {
        return $this->linkExternal;
    }

    public function setLinkExternal($linkExternal)
    {
        $this->linkExternal = $linkExternal;
        return $this;
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        return 'Slider';
    }

}
