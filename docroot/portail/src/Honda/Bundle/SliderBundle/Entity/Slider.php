<?php

namespace Honda\Bundle\SliderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

use Honda\Bundle\SliderBundle\Model\BaseSlider;
use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;

/**
 * Slider
 *
 * @ORM\Table(name="slider")
 * @ORM\Entity(repositoryClass="Honda\Bundle\SliderBundle\Repository\SliderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Slider extends BaseSlider
{

    use ActivationTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", nullable=true)
     */
    protected $video;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\Honda\Bundle\DistributorBundle\Entity\SubGroup", cascade={"persist"})
     */
    private $subGroups;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    protected $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    protected $endDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    protected $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="media", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $media;

    /**
     *
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="image_youtube", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $imageYoutube;

    public function __construct()
    {
        parent::__construct();
        $this->subGroups = new ArrayCollection();
        $this->created = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     *
     * @return
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Slider
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set video.
     *
     * @param string $video
     *
     * @return Slider
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video.
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set subGroups.
     *
     * @param \Doctrine\Common\Collections\Collection $subGroups
     *
     * @return Slider
     */
    public function setSubGroups($subGroups)
    {
        $this->subGroups = $subGroups;

        return $this;
    }

    /**
     * Get subGroups.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubGroups()
    {
        return $this->subGroups;
    }

    /**
     * Add subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return Slider
     */
    public function addSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        $this->subGroups[] = $subGroup;

        return $this;
    }

    /**
     * Remove subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return Slider
     */
    public function removeSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        $this->subGroups->removeElement($subGroup);

        return $this;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return SubModels
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return SubModels
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return News
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return News
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdateBase()
    {
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return Slider
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get the value of imageYoutube
     *
     * @return  \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImageYoutube()
    {
        return $this->imageYoutube;
    }

    /**
     * Set the value of imageYoutube
     *
     * @param  \Application\Sonata\MediaBundle\Entity\Media  $imageYoutube
     *
     * @return  self
     */
    public function setImageYoutube(\Application\Sonata\MediaBundle\Entity\Media $imageYoutube)
    {
        $this->imageYoutube = $imageYoutube;

        return $this;
    }
}
