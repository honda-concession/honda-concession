<?php

namespace Honda\Bundle\SliderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\Bundle\SliderBundle\Model\BaseSlider;

/**
 * SliderHome
 *
 * @ORM\Table(name="slider_home")
 * @ORM\Entity(repositoryClass="Honda\Bundle\SliderBundle\Repository\SliderHomeRepository")
 */
class SliderHome extends BaseSlider
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Banner", inversedBy="sliders")
     * @ORM\JoinColumn(name="slider_banner_id", referencedColumnName="id",nullable=true)
     */
    protected $banner;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
        /**
     * Get banner.
     *
     * @return string
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * Set banner.
     *
     * @param $banner
     *
     * @return Slider
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
        return $this;
    }


}
