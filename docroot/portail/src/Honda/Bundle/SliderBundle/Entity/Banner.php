<?php

namespace Honda\Bundle\SliderBundle\Entity;

use Honda\Bundle\SliderBundle\Model\BaseSlider;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Banner
 *
 * @ORM\Table(name="banner")
 * @ORM\Entity(repositoryClass="Honda\Bundle\SliderBundle\Repository\BannerRepository")
 */
class Banner extends BaseSlider
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="SliderHome" , mappedBy="banner",cascade={"all"}, fetch="LAZY", orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $sliders;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="activation_start", type="datetime", nullable=false)
     */
    private $activationStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="activation_end", type="datetime", nullable=false)
     */
    private $activationEnd;

    public function __construct()
    {
        parent::__construct();
        $this->sliders = new ArrayCollection();
    }
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add slider.
     *
     * @param \Honda\Bundle\SliderBundle\Entity\SliderHome $slider
     *
     * @return News
     */
    public function addSlider($slider)
    {
        $this->sliders[] = $slider;
        $slider->setBanner($this);
        return $this;
    }

    /**
     * Remove slider.
     *
     * @param \Honda\Bundle\SliderBundle\Entity\SliderHome $slider
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSlider(\Honda\Bundle\SliderBundle\Entity\SliderHome $slider)
    {
        return $this->sliders->removeElement($slider);
    }

    /**
     * Get slider.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSliders()
    {
        return $this->sliders;
    }

    /**
     * Set slider.
     *
     * @param $sliders
     *
     * @return Banner
     */
    public function setSliders($sliders)
    {
        $this->sliders = $sliders;

        return $this;
    }

    /**
     * Get the value of activationStart
     *
     * @return  \DateTime
     */
    public function getActivationStart()
    {
        return $this->activationStart;
    }

    /**
     * Set the value of activationStart
     *
     * @param  \DateTime  $activationStart
     *
     * @return  self
     */
    public function setActivationStart(\DateTime $activationStart)
    {
        $this->activationStart = $activationStart;

        return $this;
    }

    /**
     * Get the value of activationEnd
     *
     * @return  \DateTime
     */
    public function getActivationEnd()
    {
        return $this->activationEnd;
    }

    /**
     * Set the value of activationEnd
     *
     * @param  \DateTime  $activationEnd
     *
     * @return  self
     */
    public function setActivationEnd(\DateTime $activationEnd)
    {
        $this->activationEnd = $activationEnd;

        return $this;
    }
}
