<?php

namespace Honda\Bundle\SliderBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Honda\Bundle\CommonBundle\Service\MediaUrl;
use Honda\Bundle\DistributorBundle\Entity\Distributor;

class SliderController extends Controller
{

    const POST_ORIGIN_CONCESSION = 'concession';
    const POST_ORIGIN_PORTAIL = 'portail';

    /**
     * @Route("/api/slider/{distributorId}", name="api_get_concession_sliders", requirements={"distributorId": "\d+"}, methods={"GET"})
     * @Method("GET")
     * @param Request $request
     * @param $distributorId
     * @return JsonResponse
     */
    public function sliderAction(Request $request, $distributorId)
    {
        $all = $request->query->get('all', false);

        $subGroupIds = $this->getDoctrine()->getManager()->getRepository(Distributor::class)->getSubGroupIds($distributorId);

        $sliders = $this->getDoctrine()->getRepository('HondaSliderBundle:Slider')->getSliders($subGroupIds, $all);
        $baseUrl = $request->getSchemeAndHttpHost();

        $data = [];
        if (count($sliders)) {
            foreach ($sliders as $key => $slider) {
                if (!$slider->getMedia()) {
                    continue;
                }
                if ($slider->getMedia()->getProviderName() == 'sonata.media.provider.image') {
                    $data[$key]['type'] = 'image';
                    $data[$key]['origin'] = self::POST_ORIGIN_PORTAIL;
                    $data[$key]['url'] = $slider->getLink();
                    $data[$key]['external_link'] = $slider->getLinkExternal();
                    $data[$key]['title'] = $slider->getMedia()->getName();
                    $data[$key]['images'] = [];
                    if ($slider->getMedia()) {
                        $data[$key]['images']['ref'] = $baseUrl . $this->get(MediaUrl::class)->getUrl($slider->getMedia(), 'reference');
                        $data[$key]['images']['srcsets'] = [
                            $baseUrl . $this->get(MediaUrl::class)->getUrl($slider->getMedia(), 'small'),
                            $baseUrl . $this->get(MediaUrl::class)->getUrl($slider->getMedia(), 'medium'),
                            $baseUrl . $this->get(MediaUrl::class)->getUrl($slider->getMedia(), 'big')
                        ];
                    }
                } elseif ($slider->getMedia()->getProviderName() == 'sonata.media.provider.youtube') {
                    $data[$key]['type'] = 'video';
                    $data[$key]['origin'] = self::POST_ORIGIN_PORTAIL;
                    $data[$key]['title'] = $slider->getMedia()->getName();
                    $data[$key]['video'] = [];
                    if ($slider->getMedia()) {
                        $data[$key]['video']['image'] = $baseUrl . $this->get(MediaUrl::class)->getUrl($slider->getMedia(), 'reference');
                        $data[$key]['video']['providerReference'] = $slider->getMedia()->getProviderReference();
                        $data[$key]['video']['url'] = $slider->getMedia()->getUrl();
                    }
                    if ($imageYoutube = $slider->getImageYoutube()) {
                        $data[$key]['imageYoutube'] = $baseUrl.$this->get(MediaUrl::class)->getUrl($imageYoutube, 'reference');
                    }
                }
            }
        }

        return new JsonResponse($data);
    }

}
