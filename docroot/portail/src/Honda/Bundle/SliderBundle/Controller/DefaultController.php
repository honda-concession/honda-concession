<?php

namespace Honda\Bundle\SliderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Honda\Bundle\SliderBundle\Entity\Banner;

class DefaultController extends Controller
{
    /**
     * Banner
     * 
     */
    public function bannerAction()
    {

        $em = $this->getDoctrine()->getManager();
        $bannerRepo = $em->getRepository(Banner::class);
        $banner = $bannerRepo->findOneBy(['id' => 1]);

        return $this->render('HondaSliderBundle:Portail:banner.html.twig', array(
                    "banner" => $banner
        ));
    }

}
