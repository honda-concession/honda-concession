<?php

namespace Honda\Bundle\CommonBundle\Command;

use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Honda\Bundle\VehiclesBundle\Entity\Category;
use Honda\Bundle\VehiclesBundle\Entity\Color;
use Honda\Bundle\VehiclesBundle\Entity\Vehicles;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\PropertyAccess\PropertyAccess;


class ImportKoraCommand extends ContainerAwareCommand
{
    
    
    const URL_KORA = 'https://services-honda.preprod.kora.pro/';
    const X_CALLEE = '25410755-b33c-4b87-9e9f-9c674fdb';
    
    protected function configure()
    {
        $this
            ->setName('honda:import-kora')
            ->setDescription('Import new vehicles')
            ->setHelp('This command allows you to import vn...')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Vous allez procéder à l\'importation des informations de Kora </info>');
        //TODO: En attente d'information
        //$this->importDealers();
        //$this->importVn();
    }
    
    /**
     * @return bool
     */
    protected function importDealers()
    {
        $container = $this->getContainer();
    
        $configs = ['timeout'  => 10];
    
        $client = new Client($configs);
    
        $sslVerification = false;
        
        $endpoint = self::URL_KORA . 'moto/dealers?X-CALLEE=' . self::X_CALLEE  ;
        
        try {
            $res = $client->request('GET', $endpoint, ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
    
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
        
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);
                $em = $container->get('doctrine.orm.entity_manager');
                
                foreach ($responseData as $data) {
    
                    $propertyAccessor = PropertyAccess::createPropertyAccessor();
                    
                    $distributor = $em->getRepository(Distributor::class)->findOneBy(['dcmsid' => $data['dcms_id']]);
                    
                    if (!$distributor) {
                        
                        $distributor = new Distributor();
                        $distributor
                            ->setName($propertyAccessor->getValue($data, '[nom_social]'))
                            ->setDcmsid($data['dcms_id'])
                            ->setAddAddress1($propertyAccessor->getValue($data, '[adresse]'))
                            ->setCity($propertyAccessor->getValue($data, '[ville]'))
                            ->setCodePostal($propertyAccessor->getValue($data, '[cp]'))
                            ->setLongitude($propertyAccessor->getValue($data, '[position][longitude]'))
                            ->setLatitude($propertyAccessor->getValue($data, '[position][latitude]'))
                            ->setWebsiteUrl($propertyAccessor->getValue($data, '[url]'))
                        ;

                        if (isset($data['departement']) && $data['departement']) {
                            $distributor->setDepartment($propertyAccessor->getValue($data, '[departement][nom]'));
                        }
                        
                        if ($distributor->getLatitude() && $distributor->getLongitude()) {
                            $distributor->setLocation($distributor->getLatitude() .','. $distributor->getLongitude());
                        }
                        
                        //$em->persist($distributor);
                    }
    
                }
                
                //$em->flush();
                
                return true;
            
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
        }
    
        return false;
    }
    
    public function importVn()
    {
        $container = $this->getContainer();
    
        $configs = ['timeout'  => 10];
    
        $client = new Client($configs);
    
        $sslVerification = false;
    
        $endpoint = self::URL_KORA . 'moto/catalogue?X-CALLEE=' . self::X_CALLEE  ;
    
        try {
            $res = $client->request('GET', $endpoint, ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
    
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
        
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);
                $em = $container->get('doctrine.orm.entity_manager');
    
                $categoryRepository = $em->getRepository(Category::class);
                $colorRepository = $em->getRepository(Color::class);
                $vehicleRepository = $em->getRepository(Vehicles::class);
                
                foreach ($responseData as $data) {
                    //dump($data); exit;
                    $propertyAccessor = PropertyAccess::createPropertyAccessor();
        
                    $vehicle = $vehicleRepository->findOneBy(['nom' => $data['nom']]);
                    
                    if (!$vehicle) {
    
                        $vehicle = new Vehicles();
    
                        /*
                        $dataCategories = $propertyAccessor->getValue($data, '[categories]');
    
                        // Ajout des catégories
                        if (count($dataCategories)) {
        
                            foreach ($dataCategories  as $dataCategory) {
            
                                $category = $categoryRepository->findOneBy(['name' => $dataCategory['nom']]);
            
                                if (!$category) {
                                    $category = new Category();
                                    $category->setName($dataCategory['nom']);
                                }
    
                                
                            }
        
                        }*/
                        
                        
                        //Ajout couleur
                        
                    }
                    
                    
                }
            
                //$em->flush();
            
                return true;
            
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
        }
    
        return false;
    }
    
}
