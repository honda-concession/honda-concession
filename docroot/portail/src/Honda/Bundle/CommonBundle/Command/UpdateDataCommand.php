<?php

namespace Honda\Bundle\CommonBundle\Command;

use Honda\Bundle\ServicesBundle\Entity\Services;
use Honda\Bundle\VehiclesBundle\Entity\Category;
use Honda\Bundle\VehiclesBundle\Entity\Models;
use Honda\Bundle\VehiclesBundle\Entity\SubModels;
use Honda\Bundle\VehiclesBundle\Entity\Vehicles;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Console\Input\InputArgument;

class UpdateDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('honda:update-data')
            ->setDescription('Update existing data.')
            ->addArgument('run', InputArgument::REQUIRED, 'enable explicity command run')
            ->setHelp('Update existing data...')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
    
        $run = $input->getArgument('run');
        
        if ($run != 'run') {
            throw new \InvalidArgumentException('Argument "run" is required value "run"');
        }
        
        $output->writeln('<info>Début création d\'un slug </info>');
        
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
    
        $output->writeln('<info>Début Slug Vehicules</info>');
        $vehicles = $em->getRepository(Vehicles::class)->findAll();
        
        foreach ($vehicles as $position => $vehicle) {
            
            if (!$vehicle->getSlug()) {
                
                $slug = Urlizer::urlize($vehicle->getName());
                $vehicle->setSlug($slug);
                $output->writeln( sprintf('<info>Vehicule traité "%s", slug "%s"</info>', $vehicle->getName(), $slug));
            }
    
            //$vehicle->setPosition($position);
            //$output->writeln( sprintf('<info>Vehicule position traité "%s", slug "%s"</info>', $vehicle->getName(), $vehicle->getPosition()));
        }
        
        $output->writeln('<info>Fin Slug Vehicules</info>');
    
    
    
    
        $output->writeln('<info>Début Slug models</info>');
        $models = $em->getRepository(Models::class)->findAll();
    
        foreach ($models as $position => $model) {
        
            if (!$model->getSlug()) {
            
                $slug = Urlizer::urlize($model->getName());
                $model->setSlug($slug);
                $output->writeln( sprintf('<info>model traité "%s", slug "%s"</info>', $model->getName(), $slug));
            }
    
            //$model->setPosition($position);
            //$output->writeln( sprintf('<info>model position traité "%s", position "%s"</info>', $model->getName(), $model->getPosition()));
        }
        
        
    
        $output->writeln('<info>Début Slug Submodels</info>');
        $submodels = $em->getRepository(SubModels::class)->findAll();
    
        foreach ($submodels as $position => $submodel) {
        
            if (!$submodel->getSlug()) {
            
                $slug = Urlizer::urlize($vehicle->getName());
                $submodel->setSlug($slug);
                $output->writeln( sprintf('<info>Submodel traité "%s", slug "%s"</info>', $submodel->getName(), $slug));
            }
    
            //$submodel->setPosition($position);
            //$output->writeln( sprintf('<info>Submodel position traité "%s", position "%s"</info>', $vehicle->getName(), $vehicle->getPosition()));
        }
    
    
    
    
        $output->writeln('<info>Début Slug Catégories</info>');
        $categories = $em->getRepository(Category::class)->findAll();
    
        foreach ($categories as $position => $category) {
        
            if (!$category->getSlug()) {
            
                $slug = Urlizer::urlize($category->getName());
                $category->setSlug($slug);
                $output->writeln( sprintf('<info>Category traité "%s", slug "%s"</info>', $category->getName(), $slug));
            }
    
            //$category->setPosition($position);
            //$output->writeln( sprintf('<info>Category position traité "%s", position "%s"</info>', $category->getName(), $category->getPosition()));
        }
    
    
    
        /*
        $output->writeln('<info>Début Position services</info>');
        $services = $em->getRepository(Services::class)->findAll();
    
        foreach ($services as $position => $service) {
        
            $service->setPosition($position);
            $output->writeln( sprintf('<info>service position traité "%s", position "%s"</info>', $service->getTitle(), $service->getPosition()));
        }
        */
        
        
        $em->flush();
        
        
    
        $output->writeln('<info>Fin Slug Submodel</info>');
        
    
        $output->writeln('<info>Fin création d\'un slug </info>');
    }
}
