<?php

namespace Honda\Bundle\CommonBundle\Helper;

/**
 * Class AES
 *
 * @package App\Service\AES
 */
class AES
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $data;

    /**
     * @var string
     */
    protected $method;

    /**
     * Available OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING
     *
     * @var int $options
     */
    protected $options = 0;


    /**
     * AES constructor.
     *
     * @param string $data
     * @param string $key
     * @param int    $blockSize
     * @param string $mode
     *
     * @throws \Exception
     */
    public function __construct($data = null, $key = null, $blockSize = null, $mode = 'CBC')
    {
        $this->setData($data);
        $this->setKey($key);
        $this->setMethod($blockSize, $mode);
    }


    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * CBC 128 192 256
     * CBC-HMAC-SHA1 128 256
     * CBC-HMAC-SHA256 128 256
     * CFB 128 192 256
     * CFB1 128 192 256
     * CFB8 128 192 256
     * CTR 128 192 256
     * ECB 128 192 256
     * OFB 128 192 256
     * XTS 128 256
     *
     * @param int    $blockSize
     * @param string $mode
     *
     * @throws \Exception
     */
    public function setMethod($blockSize, $mode = 'CBC')
    {
        if (192 === $blockSize && in_array('', ['CBC-HMAC-SHA1', 'CBC-HMAC-SHA256', 'XTS'])) {
            $this->method = null;
            throw new \Exception('Invalid block size and mode combination!');
        }

        $this->method = 'AES-'.$blockSize.'-'.$mode;
    }

    /**
     * @return bool
     */
    public function validateParams()
    {
        if ($this->data !== null && $this->method !== null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return string
     *
     * @throws \Exception
     */
    public function encrypt()
    {
        if ($this->validateParams()) {
            return trim(openssl_encrypt($this->data, $this->method, $this->key, $this->options, $this->getIV()));
        } else {
            return null;
        }
    }

    /**
     * @param string $data
     *
     * @return string
     *
     * @throws \Exception
     */
    public function encryptData($data)
    {
        $this->data = $data;

        return $this->encrypt();
    }

    /**
     * @return string
     *
     * @throws \Exception
     */
    public function decrypt()
    {
        if ($this->validateParams()) {
            $ret = openssl_decrypt($this->data, $this->method, $this->key, $this->options, $this->getIV());

            return trim($ret);
        } else {
            throw new \Exception('Invalid params!');
        }
    }

    /**
     * @param string $data
     *
     * @return string
     *
     * @throws \Exception
     */
    public function decryptData($data)
    {
        $this->data = $data;

        return $this->decrypt();
    }

    /**
     * @return string
     */
    protected function getIV()
    {
        return 'TestPMI 12345678';
    }
}
