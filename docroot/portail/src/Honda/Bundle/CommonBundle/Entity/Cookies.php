<?php

namespace Honda\Bundle\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\Bundle\CommonBundle\Entity\Questions;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cookies
 *
 * @ORM\Table(name="cookies")
 * @ORM\Entity(repositoryClass="Honda\Bundle\CommonBundle\Repository\CookiesRepository")
 */
class Cookies
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="vie_privee_intro", type="text")
     */
    private $viePriveeIntro;
    
    /**
     * @ORM\OneToMany(targetEntity="Questions" , mappedBy="cookieQuestion",cascade={"all"}, orphanRemoval=true)
     */
    private $viePriveeQuestions;

    /**
     * @var string
     *
     * @ORM\Column(name="cookies_intro", type="text")
     */
    private $cookiesIntro;
    
    /**
     * @ORM\OneToMany(targetEntity="Questions" , mappedBy="cookieQuestion2",cascade={"all"}, orphanRemoval=true)
     */
    private $cookiesQuestions;


    public function __construct()
    {
        $this->viePriveeQuestions = new ArrayCollection();
        $this->cookiesQuestions = new ArrayCollection();
    }
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Cookies
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set message.
     *
     * @param string $message
     *
     * @return Cookies
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message.
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set viePriveeIntro.
     *
     * @param string $viePriveeIntro
     *
     * @return Cookies
     */
    public function setViePriveeIntro($viePriveeIntro)
    {
        $this->viePriveeIntro = $viePriveeIntro;

        return $this;
    }

    /**
     * Get viePriveeIntro.
     *
     * @return string
     */
    public function getViePriveeIntro()
    {
        return $this->viePriveeIntro;
    }

    /**
     * Set cookiesIntro.
     *
     * @param string $cookiesIntro
     *
     * @return Cookies
     */
    public function setCookiesIntro($cookiesIntro)
    {
        $this->cookiesIntro = $cookiesIntro;

        return $this;
    }

    /**
     * Get cookiesIntro.
     *
     * @return string
     */
    public function getCookiesIntro()
    {
        return $this->cookiesIntro;
    }
    
    /**
     * Set viePriveeQuestions.
     *
     * @param \Honda\Bundle\CommonBundle\Entity\Questions|null $viePriveeQuestions
     *
     * @return Cookies
     */
    public function setViePriveeQuestions(Questions $viePriveeQuestions = null)
    {
        $this->viePriveeQuestions = $viePriveeQuestions;

        return $this;
    }

    /**
     * Get viePriveeQuestions.
     *
     * @return \Honda\Bundle\CommonBundle\Entity\Questions|null
     */
    public function getViePriveeQuestions()
    {
        return $this->viePriveeQuestions;
    }
    
    /**
     * Add viePriveeQuestions.
     *
     * @param \Honda\Bundle\CommonBundle\Entity\Questions $viePriveeQuestions
     *
     * @return Cookies
     */
    public function addViePriveeQuestions(\Honda\Bundle\CommonBundle\Entity\Questions $viePriveeQuestions)
    {
        $this->viePriveeQuestions[] = $viePriveeQuestions;
        $viePriveeQuestions->setCookieQuestion($this);

        return $this;
    }

    /**
     * Remove viePriveeQuestions.
     *
     * @param \Honda\Bundle\CommonBundle\Entity\Questions $viePriveeQuestions
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeViePriveeQuestions(\Honda\Bundle\CommonBundle\Entity\Questions $viePriveeQuestions)
    {
        return $this->viePriveeQuestions->removeElement($viePriveeQuestions);
    }
    
    /**
     * Set cookiesQuestions.
     *
     * @param \Honda\Bundle\CommonBundle\Entity\Questions|null $cookiesQuestions
     *
     * @return Cookies
     */
    public function setCookiesQuestions(Questions $cookiesQuestions = null)
    {
        $this->cookiesQuestions = $cookiesQuestions;

        return $this;
    }

    /**
     * Get cookiesQuestions.
     *
     * @return \Honda\Bundle\CommonBundle\Entity\Questions|null
     */
    public function getCookiesQuestions()
    {
        return $this->cookiesQuestions;
    }
    
    /**
     * Add cookiesQuestions.
     *
     * @param \Honda\Bundle\CommonBundle\Entity\Questions $cookiesQuestions
     *
     * @return Cookies
     */
    public function addCookiesQuestions(\Honda\Bundle\CommonBundle\Entity\Questions $cookiesQuestions)
    {
        $this->cookiesQuestions[] = $cookiesQuestions;
        $cookiesQuestions->setCookieQuestion2($this);

        return $this;
    }

    /**
     * Remove cookiesQuestions.
     *
     * @param \Honda\Bundle\CommonBundle\Entity\Questions $cookiesQuestions
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCookiesQuestions(\Honda\Bundle\CommonBundle\Entity\Questions $cookiesQuestions)
    {
        return $this->cookiesQuestions->removeElement($cookiesQuestions);
    }
    
     /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        return 'Vie privée et cookies';
    }
}
