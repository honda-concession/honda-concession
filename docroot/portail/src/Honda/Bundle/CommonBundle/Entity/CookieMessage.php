<?php

namespace Honda\Bundle\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cookie
 *
 * @ORM\Table(name="cookie_message")
 * @ORM\Entity(repositoryClass="Honda\Bundle\CommonBundle\Repository\CookieMessageRepository")
 */
class CookieMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message.
     *
     * @param string $message
     *
     * @return Cookie
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message.
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        return 'Message cookies';
    }
}
