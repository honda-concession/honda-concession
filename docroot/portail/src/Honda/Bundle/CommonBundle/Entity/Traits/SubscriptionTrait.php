<?php

namespace Honda\Bundle\CommonBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait SubscriptionTrait
 * @package Honda\Bundle\CommonBundle\Entity\Traits
 */
trait SubscriptionTrait
{
    /**
     * @var bool
     *
     * @ORM\Column(name="settlement", type="boolean", options={"default":"0"})
     */
    private $settlement;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_id", type="string", length=255, nullable=true)
     */
    private $billingId;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $invoice;

    /**
     * @var string
     *
     * @ORM\Column(name="period", type="string", length=255, nullable=true)
     */
    private $period;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscription_start", type="datetime", nullable=true)
     */
    private $subscriptionStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscription_end", type="datetime", nullable=true)
     */
    private $subscriptionEnd;

    /**
     * @var float
     *
     * @ORM\Column(name="subscription_price", type="float", nullable=true)
     */
    private $subscriptionPrice = 0;

    /**
     * Get the value of settlement
     *
     * @return  bool
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * Set the value of settlement
     *
     * @param  bool  $settlement
     *
     * @return  self
     */
    public function setSettlement(bool $settlement)
    {
        $this->settlement = $settlement;

        return $this;
    }

    /**
     * Get the value of billingId
     *
     * @return  string
     */
    public function getBillingId()
    {
        return $this->billingId;
    }

    /**
     * Set the value of billingId
     *
     * @param  string  $billingId
     *
     * @return  self
     */
    public function setBillingId(string $billingId)
    {
        $this->billingId = $billingId;

        return $this;
    }

    /**
     * Get the value of period
     *
     * @return  string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set the value of period
     *
     * @param  string  $period
     *
     * @return  self
     */
    public function setPeriod(string $period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get the value of invoice
     *
     * @return  \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set the value of invoice
     *
     * @param  \Application\Sonata\MediaBundle\Entity\Media  $invoice
     *
     * @return  self
     */
    public function setInvoice(\Application\Sonata\MediaBundle\Entity\Media $invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get the value of subscriptionStart
     *
     * @return  \DateTime
     */
    public function getSubscriptionStart()
    {
        return $this->subscriptionStart;
    }

    /**
     * Set the value of subscriptionStart
     *
     * @param  \DateTime  $subscriptionStart
     *
     * @return  self
     */
    public function setSubscriptionStart(\DateTime $subscriptionStart)
    {
        $this->subscriptionStart = $subscriptionStart;

        return $this;
    }

    /**
     * Get the value of subscriptionEnd
     *
     * @return  \DateTime
     */
    public function getSubscriptionEnd()
    {
        return $this->subscriptionEnd;
    }

    /**
     * Set the value of subscriptionEnd
     *
     * @param  \DateTime  $subscriptionEnd
     *
     * @return  self
     */
    public function setSubscriptionEnd(\DateTime $subscriptionEnd)
    {
        $this->subscriptionEnd = $subscriptionEnd;

        return $this;
    }

    /**
     * Get the value of subscriptionPrice
     *
     * @return  float
     */
    public function getSubscriptionPrice()
    {
        return $this->subscriptionPrice;
    }

    /**
     * Set the value of subscriptionPrice
     *
     * @param  float  $subscriptionPrice
     *
     * @return  self
     */
    public function setSubscriptionPrice(float $subscriptionPrice)
    {
        $this->subscriptionPrice = $subscriptionPrice;

        return $this;
    }
}
