<?php

namespace Honda\Bundle\CommonBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Trait ActivationTrait
 * @package Honda\Bundle\CommonBundle\Entity\Traits
 */
trait ActivationTrait
{
    /**
     * published on home
     * @var bool
     *
     * @ORM\Column(name="published", type="boolean", options={"default": 0})
     */
    protected $published = false;

    /**
     * Get published on home
     *
     * @return  bool
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set published on home
     *
     * @param  bool  $published  published on home
     *
     * @return  self
     */
    public function setPublished(bool $published)
    {
        $this->published = $published;

        return $this;
    }

    public function isPublished()
    {
        return $this->published;
    }
}
