<?php

namespace Honda\Bundle\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Questions
 *
 * @ORM\Table(name="questions")
 * @ORM\Entity(repositoryClass="Honda\Bundle\CommonBundle\Repository\QuestionsRepository")
 */
class Questions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text")
     */
    private $answer;

    /**
     * @ORM\ManyToOne(targetEntity="Cookies", inversedBy="viePriveeQuestions ")
     * @ORM\JoinColumn(name="vie_cookie_id", referencedColumnName="id")
     */
    protected $cookieQuestion;
    
    /**
     * @ORM\ManyToOne(targetEntity="Cookies", inversedBy="cookiesQuestions ")
     * @ORM\JoinColumn(name="cookie_id", referencedColumnName="id")
     */
    protected $cookieQuestion2;
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question.
     *
     * @param string $question
     *
     * @return Questions
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer.
     *
     * @param string $answer
     *
     * @return Questions
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer.
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }
    
    /**
     * Get cookieQuestion
     *
     * @return string
     */
    public function getCookieQuestion()
    {
        return $this->cookieQuestion;
    }


    /**
     * Set cookieQuestion
     *
     * @param string $cookieQuestion
     *
     * @return Questions
     */
    public function setCookieQuestion($cookieQuestion)
    {
        $this->cookieQuestion = $cookieQuestion;
        return $this;
    }
    
    /**
     * Get cookieQuestion2
     *
     * @return string
     */
    public function getCookieQuestion2()
    {
        return $this->cookieQuestion2;
    }


    /**
     * Set cookieQuestion2
     *
     * @param string $cookieQuestion2
     *
     * @return Questions
     */
    public function setCookieQuestion2($cookieQuestion2)
    {
        $this->cookieQuestion2 = $cookieQuestion2;
        return $this;
    }
}
