<?php

namespace Honda\Bundle\CommonBundle\Builder;

use Honda\CommonBundle\Entity\Distributor;
use Honda\CommonBundle\Entity\News;
use Honda\CommonBundle\Entity\VehicleAd;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Honda\CommonBundle\Model\Middleware\PortailWrapper;
use Symfony\Component\Routing\RouterInterface;
use Honda\CommonBundle\Model\Middleware\GenericGateway;
use Honda\CommonBundle\Model\AbstractBaseManager;

/**
 * Class SiteMapBuilder
 * @package Honda\CommonBundle\Builder
 */
class SiteMapBuilder
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var PortailWrapper
     */
    protected $portailWrapper;

    /**
     * @var Array
     */
    protected $endpoints;

    /**
     * @var ObjectManager
     */
    protected $em;

    /**
     * Builder constructor.
     * @param RouterInterface $router
     * @param PortailWrapper $portailWrapper
     * @param $endpoints
     * @param ObjectManager $em
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = $this->container->get('router');
        $this->em = $this->container->get('doctrine.orm.entity_manager');
    }

    /**
     * @param Distributor|null $distributor
     * @return array
     */
    public function run()
    {
        $urls = [];

        $urls[] = [
            'loc' => $this->router->generate('page_slug', ['path' => '/'], RouterInterface::ABSOLUTE_URL),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => 0.9
        ];
        $urls[] = [
            'loc' => $this->router->generate('page_slug', ['path' => '/label-honda'], RouterInterface::ABSOLUTE_URL),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => 0.8
        ];
        $urls[] = [
            'loc' => $this->router->generate('page_slug', ['path' => '/cookies'], RouterInterface::ABSOLUTE_URL),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => 0.8
        ];
        $urls[] = [
            'loc' => $this->router->generate('page_slug', ['path' => '/mentions-legales'], RouterInterface::ABSOLUTE_URL),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => 0.8
        ];
        $urls[] = [
            'loc' => $this->router->generate('page_slug', ['path' => '/plan-du-site'], RouterInterface::ABSOLUTE_URL),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => 0.8
        ];

        return $urls;
    }
}
