<?php

namespace Honda\Bundle\CommonBundle\Block\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\CoreBundle\Model\Metadata;

class ButtonBlockService extends AbstractAdminBlockService 
{
    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'link' => null,
            'target' => false,
            'label' => null,
            'template' => 'HondaCommonBundle:Block:button_block.html.twig'
        ));
    }

	/**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('link', 'text', array('label' => 'Url', 'required' => true)),
                array('target', 'checkbox', array('label' => 'Ouvrir dans un nouvel onglet', 'required' => false)),
                array('label', 'text', array('label' => 'Label', 'required' => true))
            ),
        ));
    }


	/**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        return $this->renderResponse($blockContext->getTemplate(), array(
            'block' => $blockContext->getBlock(),
            'settings' => $blockContext->getSettings(),
        ), $response);
    }

	/**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Button';
    }


    /**
     * {@inheritdoc}
     */
    public function getBlockMetadata($code = null)
    {
        return new Metadata($this->getName(), (!is_null($code) ? $code : $this->getName()), false, 'SonataBlockBundle', array(
            'class' => 'fa fa-link',
        ));
    }
}