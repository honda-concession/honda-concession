<?php

namespace Honda\Bundle\CommonBundle\Block\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sonata\CoreBundle\Model\ManagerInterface;
use Sonata\CoreBundle\Model\Metadata;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;

class ImageLeftWidgetBlockService extends BaseImageWidgetBlockService 
{
    /**
     * {@inheritdoc}
     */
    public function __construct($name, EngineInterface $templating, ContainerInterface $container, ManagerInterface $mediaManager)
    {
        parent::__construct($name, $templating, $container, $mediaManager);

        $this->context = "image";
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        parent::configureSettings($resolver);

        $resolver->setDefault('template', 'HondaCommonBundle:Block:image_left_text_right_widget_block.html.twig');

    }

	/**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Image Left - Text Right';
    }

}