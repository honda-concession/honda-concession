<?php

namespace Honda\Bundle\CommonBundle\Search;


use FOS\ElasticaBundle\Elastica\Client as BaseClient;
use Elastica\Request;
use Elastica\Exception\ExceptionInterface;
use Elastica\Response;


/**
 * Override Client for ES server error handler
 */
class Client extends BaseClient
{
    /**
     * {@inheritdoc}
     */
    public function request($path, $method = Request::GET, $data = [], array $query = [], $contentType = Request::DEFAULT_CONTENT_TYPE)
    {
        try {
            return parent::request($path, $method, $data, $query, $contentType );
        } catch (ExceptionInterface $e) {
            if ($this->_logger) {
                $this->_logger->error('Failed to send a request to ElasticSearch', array(
                    'exception' => $e->getMessage(),
                    'path' => $path,
                    'method' => $method,
                    'data' => $data,
                    'query' => $query
                ));
            }
            //TODO: Prévoir une notification par mail
            return new Response('{"took":0,"timed_out":false,"hits":{"total":0,"max_score":0,"hits":[]}}');
        }
    }
}


