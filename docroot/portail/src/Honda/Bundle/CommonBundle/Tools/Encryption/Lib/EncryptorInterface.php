<?php

namespace Honda\Bundle\CommonBundle\Tools\Encryption\Lib;

interface EncryptorInterface
{
    /**
     * Encode function
     *
     * @param string $value
     *
     * @return string
     */
    public static function encode($value, $key);

    /**
     * Decode function
     *
     * @param string $value
     *
     * @return string
     */
    public static function decode($value, $key);

    /**
     * to string function
     *
     * @return string
     */
    public static function getName();
}