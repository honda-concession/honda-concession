<?php

namespace Honda\Bundle\CommonBundle\Model;

use Doctrine\ORM\Mapping as ORM;


/**
 * BaseModel
 * @ORM\HasLifecycleCallbacks()
 */
abstract class BaseModel
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    protected $updated;

    public function __construct()
    {
        $this->created = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return BaseEntity
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return BaseEntity
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdateBase()
    {
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
    }
}
