<?php

namespace Honda\Bundle\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class CookiesAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('title')
                ->add('message')
                ->add('viePriveeIntro')
                ->add('cookiesIntro')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('title')
                ->add('message')
                ->add('viePriveeIntro')
                ->add('cookiesIntro')
                ->add('_action', null, [
                    'actions' => [
                        'show' => [],
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('title', null, array(
                    'label' => 'Titre'
                ))
                ->add('message')
                ->add('viePriveeIntro','ckeditor', array(
                   'label' => 'Vie Privée Intro' 
                ))
                ->add('viePriveeQuestions', 'sonata_type_collection', array(
                    'required' => true,
                    'label' => 'Questions Vie Privée',
                    'help' => '',
                    'type_options' => array(
                        // Prevents the "Delete" option from being displayed
                        'delete' => true
                    )
                        ), array(
                    'edit' => 'inline',
                    'sortable' => 'pos',
                    'inline' => 'table',
                        )
                )
                ->add('cookiesIntro','ckeditor', array(
                    'label' => 'Cookies Intro'
                ))
                ->add('cookiesQuestions', 'sonata_type_collection', array(
                    'required' => true,
                    'label' => 'Questions Cookies',
                    'help' => '',
                    'type_options' => array(
                        // Prevents the "Delete" option from being displayed
                        'delete' => true
                    )
                        ), array(
                    'edit' => 'inline',
                    'sortable' => 'pos',
                    'inline' => 'table',
                        )
                )


        ;
    }

    /**
     * @inheritdoc
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        foreach ($object->getViePriveeQuestions() as $questions) {
            $questions->setCookieQuestion($object);
        }

        foreach ($object->getCookiesQuestions() as $questions) {
            $questions->setCookieQuestion2($object);
        }
    }

    /**
     * @inheritdoc
     */
    public function preUpdate($object)
    {
        parent::preUpdate($object);
        foreach ($object->getViePriveeQuestions() as $questions) {
            $questions->setCookieQuestion($object);
        }

        foreach ($object->getCookiesQuestions() as $questions) {
            $questions->setCookieQuestion2($object);
        }
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list')->remove('delete')->remove('create');
    }
    
    protected function configureBatchActions($actions)
    {

        unset($actions['delete']);

        return $actions;
    }

}
