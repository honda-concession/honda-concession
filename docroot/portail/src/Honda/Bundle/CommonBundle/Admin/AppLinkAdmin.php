<?php

namespace Honda\Bundle\CommonBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;


/**
 * Class AppLinkAdmin
 * @package Honda\MainBundle\Admin
 */
class AppLinkAdmin extends AbstractAdmin
{
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('list')
            ->remove('delete')
            ->remove('create')
        ;
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('googlePlayLink', null, ['label' => 'Lien Google play'])
            ->add('appleStoreLink', null, ['label' => 'Lien Apple store'])
        ;
    }
    
    protected function configureBatchActions($actions)
    {
        unset($actions['delete']);
        
        return $actions;
    }
    
   
}
