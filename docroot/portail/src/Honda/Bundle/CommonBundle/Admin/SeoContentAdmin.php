<?php

namespace Honda\Bundle\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

/**
 * Class SeoContentAdmin
 * @package Honda\Bundle\CommonBundle\Admin
 */
class SeoContentAdmin extends AbstractAdmin
{

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'type',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null, [
                'label' => 'Titre',
                'required' => true
            ])
            ->add('metaDescription', 'textarea', [
                'label' => 'Meta description'
            ])
            ->add('metaKeywords', 'textarea', [
                'label' => 'Mots-clés'
            ])
            ->add('description', 'ckeditor', [
                'label' => 'Description',
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('type', null, [
                    'label' => 'Identifiant'
                ])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('type', null, [
                    'label' => 'Identifiant'
                ])
                ->add('_action', null, [
                    'actions' => [
                        'show' => [],
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('delete')
            ->remove('create')
        ;
    }

}
