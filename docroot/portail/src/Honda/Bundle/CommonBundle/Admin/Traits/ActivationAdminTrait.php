<?php

namespace Honda\Bundle\CommonBundle\Admin\Traits;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

trait ActivationAdminTrait
{
    protected function configureBatchActions($actions)
    {
        $actions['published'] = [
            'ask_confirmation' => false,
            'label' => 'Publier',
        ];
        $actions['unpublished'] = [
            'ask_confirmation' => false,
            'label' => 'Dépublier',
        ];

        return $actions;
    }
}
