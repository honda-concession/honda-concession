<?php

namespace Honda\Bundle\CommonBundle\Admin\Traits;

use Sonata\AdminBundle\Form\FormMapper;

/**
 * Trait RedirectionAdminTrait
 * @package Honda\Bundle\NewsBundle\Admin
 */
trait RedirectionAdminTrait
{
    public function addRedirectionBlock(FormMapper $formMapper, $cssClass = 'col-md-3')
    {
        $formMapper
            ->with('Redirection', ['class' => $cssClass])
                ->add('redirectUrl', 'url', [
                    'label' => 'Page vers laquelle rediriger',
                    'required' => false,
                ])
                ->add('redirectStatus', null, [
                    'label' => 'Activer la redirection',
                    'required' => false,
                ])
                ->add('hideInNavigation', null, [
                    'label' => 'Cacher dans la navigation',
                    'required' => false,
                ])
                ->add('hideInSitemap', null, [
                    'label' => 'Cacher dans le plan du site',
                    'required' => false,
                ])
            ->end();

        return $this;
    }
}
