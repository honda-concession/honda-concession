<?php

namespace Honda\Bundle\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class TicketEmailsAdmin
 * @package Honda\Bundle\CommonBundle\Admin
 */
class TicketEmailsAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'setting-ticket-emails';
    protected $baseRouteName = 'setting-ticket-emails';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
