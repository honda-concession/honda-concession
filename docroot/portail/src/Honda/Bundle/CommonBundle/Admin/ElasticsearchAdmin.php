<?php

namespace Honda\Bundle\CommonBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class ElasticsearchAdmin
 * @package Honda\Bundle\CommonBundle\Admin
 */
class ElasticsearchAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'elasticsearch-command';
    protected $baseRouteName = 'elasticsearch_config';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
