<?php

namespace Honda\Bundle\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class HomePublishableAdmin extends AbstractAdmin
{
    protected function configureBatchActions($actions)
    {
        $actions['publishOnHome'] = [
            'ask_confirmation' => false,
            'label' => 'Publier sur la page d\'accueil',
        ];

        return $actions;
    }

    /**
     * @inheritdoc
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('publishOnHome', $this->getRouterIdParameter() . '/publishOnHome');
    }
}
