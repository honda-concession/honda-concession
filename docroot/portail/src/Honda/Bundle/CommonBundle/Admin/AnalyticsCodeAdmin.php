<?php

namespace Honda\Bundle\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class AnalyticsCodeAdmin
 * @package Honda\Bundle\CommonBundle\Admin
 */
class AnalyticsCodeAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'analytics-code';
    protected $baseRouteName = 'analytics-code';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['edit']);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('value', 'text', ['label' => 'GA Code'])
        ;
    }

}
