<?php

namespace Honda\Bundle\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class StatsAdmin
 * @package Honda\Bundle\CommonBundle\Admin
 */
class StatsAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'stats';
    protected $baseRouteName = 'stats';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
