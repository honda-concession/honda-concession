<?php

namespace Honda\Bundle\CommonBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Honda\Bundle\CommonBundle\Helper\AES;

use ReflectionClass;
use ReflectionMethod;

/**
 * Generic encryption class for encrypting all entities
 */
class Encryption
{
    /**
     * Encryption class
     *
     * @var AES
     */
    private $aes;

    /**
     * Pass encryption key
     * @param string $key
     */
    public function __construct($key)
    {
        $this->aes = new AES(null, $key, 256);
    }

    /**
     * call when updating an entity
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($this->isValidEntity($entity)) {
            $this->encryptEntity($entity);
        }
        $this->addUpdatedAt($entity);
    }

    /**
     * call after the entity has been updated
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($this->isValidEntity($entity)) {
            $this->decryptEntity($entity);
        }
    }

    /**
     * call after the entity has been created
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($this->isValidEntity($entity)) {
            $this->decryptEntity($entity);
        }
    }

    /**
     * call when persisting an entity
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($this->isValidEntity($entity)) {
            $this->encryptEntity($entity);
        }
    }

    /**
     * call when loading an entity
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($this->isValidEntity($entity)) {
            $this->decryptEntity($entity);
        }
    }

    /**
     * encrypt entity
     *
     * @param mixed $entity
     * @return void
     */
    public function encryptEntity($entity)
    {
        $this->process($entity, 'encrypt');
    }

    /**
     * decrypt entity
     *
     * @param mixed $entity
     * @return void
     */
    public function decryptEntity($entity)
    {
        $this->process($entity, 'decrypt');
    }

    /**
     * check if entity if valid for encryption
     *
     * @param mixed $entity
     * @return boolean
     */
    private function isValidEntity($entity)
    {
        return (
            ($entity instanceof Distributor)
        );
    }

    private function addUpdatedAt($entity)
    {
        $reflectionClass = new ReflectionClass($entity);
        if ($reflectionClass->hasMethod('getUpdatedAt')) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * process encryption
     *
     * @param mixed $entity
     * @param string $methodType
     * @return void
     */
    private function process($entity, $methodType)
    {
        $reflectionClass = new ReflectionClass($entity);
        foreach ($reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if (preg_match('/^(add|set|__)/', $method->name)) {
                continue;
            }
            if (preg_match('/^(getId|getName|getCreatedAt|getLocation|__setInitialized)$/', $method->name)) {
                if ($method->name === 'getName') {
                    $this->doOperation($reflectionClass, $entity, $method, 'decrypt');
                }
                continue;
            }
            if (preg_match('/^get/', $method->name)) {
                $this->doOperation($reflectionClass, $entity, $method, $methodType);
            }
        }
        if ($methodType == 'decrypt') {
            foreach ($reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                if (preg_match('/^get/', $method->name)) {
                    $value = $method->invoke($entity);
                    if (is_string($value) && preg_match('/==$/', $value)) {
                        $setterMethodName = preg_replace('/^get/', 'set', $method->name);
                        if (!$reflectionClass->hasMethod($setterMethodName)) {
                            continue;
                        }
                        $setterMethod = $reflectionClass->getMethod($setterMethodName);
                        $setterMethod->invoke($entity, null);
                    }
                }
            }
        }
    }

    private function doOperation($reflectionClass, $entity, $method, $methodType)
    {
        $value = $method->invoke($entity);
        $setterMethodName = preg_replace('/^get/', 'set', $method->name);
        if (!$reflectionClass->hasMethod($setterMethodName)) {
            return false;
        }
        $setterMethod = $reflectionClass->getMethod($setterMethodName);
        $encryptionClass = new ReflectionClass($this->aes);
        $encryptionMethod = $encryptionClass->getMethod("{$methodType}Data");
        if (is_string($value)) {
            $decryptedValue = $encryptionMethod->invoke($this->aes, $value);
            if (strlen($decryptedValue) > 0) {
                $setterMethod->invoke($entity, $decryptedValue);
            }
        }

        return true;
    }
}
