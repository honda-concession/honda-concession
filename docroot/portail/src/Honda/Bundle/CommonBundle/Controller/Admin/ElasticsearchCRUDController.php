<?php

namespace Honda\Bundle\CommonBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Honda\Bundle\CommonBundle\Tools\Encryption\EncryptorAesEcb;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Class ElasticsearchCRUDController
 * @package Honda\Bundle\CommonBundle\Controller\Admin
 */
class ElasticsearchCRUDController extends CRUDController
{
    
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();
        $action = $request->query->get('action', null);
        
        if ($action) {
            
            switch ($action) {
                case 'concession' :
                    $response = $this->getResponseEsConcession();
    
                    if ($response) {
                        $this->addFlash(
                            'success', 'Le serveur de recherche concession a été ré-actualisé'
                        );
                        
                        return $this->redirect($this->generateUrl('elasticsearch_config_list'));
                    }
                    break;
                case 'portail' :
                    $response = $this->getResponseEsPortail();
    
                    if ($response) {
                        $this->addFlash(
                            'success', 'Le serveur de recherche portail a été ré-actualisé'
                        );
                        
                        return $this->redirect($this->generateUrl('elasticsearch_config_list'));
                    }
                    break;
            }
        }
        
        return $this->renderWithExtraParams('@HondaCommon/Admin/sonata_es_list.html.twig', [
            ]
        );
    }
    
    /**
     * @return mixed
     */
    protected function getResponseEsConcession()
    {
        $env = $this->get('kernel')->getEnvironment(); ;
        $sslVerification = false;
    
        if ($env != "dev") {
            $sslVerification = true;
        }
        
        $timestamp = time();
        
        $client = new Client(['timeout' => 30]);
        $endpoint = $this->getParameter('endpoints')['es_concession'];
        $key = $this->getParameter('api_multisite_token');
        $token = EncryptorAesEcb::encode($timestamp, $key);

        try {
            $res = $client->request('POST', $endpoint .'/'. $timestamp  , [
                    'verify' => $sslVerification,
                    'headers' => ['X-AUTH-TOKEN' => $token],
                ]
            );
        } catch (RequestException $e) {
        }
        
        
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
        
            try {
                $body = $res->getBody();
           
                return true;
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){
            }
        }
    
        return;
    }
    
    /**
     * @return bool
     */
    public function getResponseEsPortail()
    {
        
        $application = new Application($this->get('kernel'));
        $application->setAutoExit(false);
        
        $env = $this->get('kernel')->getEnvironment();
        
        $input = new ArrayInput([
            'command' => 'fos:elastica:populate',
            '--env' => $env,
        ]);
    
        $output = new BufferedOutput();
        $application->run($input, $output);
        
        $content = $output->fetch();
    
        return true;
    }
}
