<?php

namespace Honda\Bundle\CommonBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;


/**
 * Class StatsCRUDController
 * @package Honda\Bundle\CommonBundle\Controller\Admin
 */
class StatsCRUDController extends CRUDController
{
    
    public function listAction()
    {
        return $this->renderWithExtraParams('@HondaCommon/Admin/sonata_page_stats.html.twig', []);
    }
}
