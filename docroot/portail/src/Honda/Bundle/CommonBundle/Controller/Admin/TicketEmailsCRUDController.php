<?php

namespace Honda\Bundle\CommonBundle\Controller\Admin;

use Honda\Bundle\CommonBundle\Entity\Setting;
use Honda\Bundle\CommonBundle\Form\Type\SettingType;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class TicketEmailsCRUDController
 * @package Honda\Bundle\CommonBundle\Controller\Admin
 */
class TicketEmailsCRUDController extends CRUDController
{
    
    public function listAction()
    {
        $request = $this->getRequest();
        
        $em = $this->getDoctrine()->getManager();
        $setting = $em->getRepository(Setting::class)->findOneBy(['key' => 'ticket-emails-notification']);
        
        if (!$setting) {
            throw new NotFoundHttpException();
        }
        
        $form = $this->createForm(SettingType::class, $setting);
    
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
        
            $object = $form->getData();
        
            $em->flush();
    
            $this->addFlash(
                'success', 'Les emails ont été correctement ajoutés.'
            );
            return $this->redirect($this->generateUrl('setting-ticket-emails_list'));
        }
        
        return $this->renderWithExtraParams('@HondaCommon/Admin/sonata_ticket_emails.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
