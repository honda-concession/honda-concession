<?php

namespace Honda\Bundle\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Honda\Bundle\CommonBundle\Entity\Cookies;
use Sonata\BlockBundle\Block\BlockLoaderChain;
use Honda\Bundle\CommonBundle\Builder\SiteMapBuilder;

class DefaultController extends Controller
{

    /**
     * @Route("/vie-privee-et-cookies", name="cookies")
     */
    public function cookiesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $cookiesRepo = $em->getRepository(Cookies::class);
        $cookies = $cookiesRepo->findOneBy(['id' => 1]);

        return $this->render('HondaCommonBundle:Default:cookies.html.twig', array(
                    "cookies" => $cookies
        ));
    }

    /**
     * @Route("/plan-du-site", name="sitemap")
     */
    public function sitePlanAction()
    {
        return $this->render('HondaCommonBundle:Default:sitemap.html.twig', []);
    }

    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function siteMapAction()
    {
        $siteMapBuilder = $this->get(SiteMapBuilder::class);
        $content = $this->renderView('@HondaCommon/Core/sitemap.xml.twig', [
            'urls' => $siteMapBuilder->run()
        ]);

        $response = new Response($content, 200);
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
