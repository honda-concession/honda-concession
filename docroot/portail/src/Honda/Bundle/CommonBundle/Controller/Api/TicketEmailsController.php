<?php

namespace Honda\Bundle\CommonBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Honda\Bundle\CommonBundle\Entity\Setting;


class TicketEmailsController extends Controller
{
    /**
     * @Route("/api/emails-notification", name="api_emails_notification_get")
     */
    public function getAction()
    {
        $em = $this->getDoctrine()->getManager();
        $setting = $em->getRepository(Setting::class)->findOneBy(['key' => 'ticket-emails-notification']);
    
        if (!$setting) {
            throw new NotFoundHttpException();
        }
    
        $emails = [];
        $value = $setting->getValue();
        
        $explodesEmails = explode(',', $value);
        
        if (is_array($explodesEmails)) {
            
            foreach ($explodesEmails as $email) {
                
                $email = trim($email);
                    
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    
                    $emails[] = $email;
                }
            }
        }
        
        return $this->json($emails);
    }
    
}
