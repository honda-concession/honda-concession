<?php

namespace Honda\Bundle\CommonBundle\Controller\Api;

use Honda\Bundle\CommonBundle\Entity\CookieMessage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class CookieMessageController extends Controller
{
    /**
     * @Route("/api/cookie-message", name="cookie_message_api_get")
     */
    public function getAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(CookieMessage::class);
        $cookieMessage = $repo->findOneBy(['id' => 1]);
        
        $data = ['message' => ''];
        
        if ($cookieMessage) {
            $data = ['message' => $cookieMessage->getMessage()];
        }
        
        return $this->json($data);
        
    }
    
}
