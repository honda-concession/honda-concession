<?php

namespace Honda\Bundle\CommonBundle\Twig\Extension;

use Doctrine\ORM\EntityManager;

class DatabaseGlobalsExtension extends \Twig_Extension
{

   protected $em;

   public function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   public function getGlobals()
   {
      return array (
              "cookieMessage" => $this->em->getRepository('HondaCommonBundle:CookieMessage')->find(1),
      );
   }

   public function getName()
   {
      return "HondaCommonBundle:DatabaseGlobalsExtension";
   }
}
