<?php

namespace Honda\Bundle\CommonBundle\Form\Type;

use Honda\Bundle\CommonBundle\Entity\Setting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type As Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


/**
 * Class SettingType
 * @package Honda\Bundle\CommonBundle\Form\Admin
 */
class SettingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('value', Type\TextareaType::class, [
                    'label' => 'Emails',
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Callback(array($this, 'validateEmails')),
                    ),
                ]
            )
         ;
    }
    
    public function validateEmails($value, ExecutionContextInterface $context)
    {
        $form = $context->getRoot();
        $setting = $form->getData();
    
        $explode = explode(',', $setting->getValue());
        
        if ($setting->getValue() and count($explode)) {
            
            $explode = array_map(
                function ($item) {
                    return is_string($item) ? trim($item) : $item;
                },
                $explode
            );
    
            $errorFounded = false;
    
            foreach ($explode as $postEmail) {
                if (false === filter_var($postEmail, FILTER_VALIDATE_EMAIL)) {
                    $errorFounded = true;
                    break;
                }
            }
    
            if ($errorFounded) {
                $context
                    ->buildViolation('Mails invalides, verifier les mails saisis')
                    ->addViolation();
            }
        }
      
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Setting::class,
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_setting';
    }
    
}