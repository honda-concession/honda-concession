<?php

namespace Honda\Bundle\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManagerInterface;

class GroupType extends AbstractType
{
    
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $subGroupChoices = array('Tous' => -1);
        $choices = $this->entityManager->getRepository('HondaDistributorBundle:SubGroup')->findAll();
        foreach ($choices as $choice){
            $subGroupChoices[$choice->getName()] = $choice->getId();
        }
        
        $resolver->setDefaults([
            'choices' => $subGroupChoices,
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
