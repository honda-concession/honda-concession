<?php

namespace Honda\Bundle\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\Bundle\SliderBundle\Model\BaseSlider;

/**
 * Slider
 *
 * @ORM\Table(name="slider_news")
 * @ORM\Entity(repositoryClass="Honda\Bundle\NewsBundle\Repository\SliderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Slider extends BaseSlider
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="News", inversedBy="sliders")
     * @ORM\JoinColumn(name="slider_news_id", referencedColumnName="id",nullable=true)
     */
    protected $news;

    /**
     * @var int
     *
     * @ORM\Column(name="pos", type="integer", nullable=true)
     */
    private $pos;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->pos = 99999;
    }
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set news.
     *
     * @param \Honda\Bundle\NewsBundle\Entity\News|null $news
     *
     * @return Slider
     */
    public function setNews(\Honda\Bundle\NewsBundle\Entity\News $news = null)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news.
     *
     * @return \Honda\Bundle\NewsBundle\Entity\News|null
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     * @return Slider
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPos()
    {
        return $this->pos;
    }
}
