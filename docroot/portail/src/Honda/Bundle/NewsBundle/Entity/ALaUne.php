<?php

namespace Honda\Bundle\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

use Honda\Bundle\CommonBundle\Entity\Traits\StartDateEndDateTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\HomeActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\RedirectionTrait;
use Honda\Bundle\NewsBundle\Entity\CategoryALaUne;

/**
 * ALaUne
 *
 * @ORM\Table(name="a_la_une")
 * @ORM\Entity(repositoryClass="Honda\Bundle\NewsBundle\Repository\ALaUneRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ALaUne
{

    use StartDateEndDateTrait,
        HomeActivationTrait,
        ActivationTrait,
        RedirectionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="text_mea", type="string", length=255, nullable=true)
     */
    private $textMea;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_price", type="string", length=255, nullable=true)
     */
    private $promoPrice;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="link_label", type="string", length=255, nullable=true)
     */
    private $linkLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * open link in new tab
     * @var bool
     *
     * @ORM\Column(name="link_open_in_new_tab", type="boolean", options={"default": 0})
     */
    private $linkOpenInNewTab;

    /**
     *
     * @ORM\ManyToOne(targetEntity="CategoryALaUne", inversedBy="aLaUne")
     * @ORM\JoinColumn(name="category_alaune_id", referencedColumnName="id")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $category;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\Honda\Bundle\DistributorBundle\Entity\SubGroup", cascade={"persist"})
     */
    private $subGroups;

    /**
     * @var integer
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    protected $updated;

    /**
     * ALaUne constructor.
     */
    public function __construct()
    {
        $this->created = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
        $this->linkLabel = 'Voir plus';
        $this->subGroups = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return ALaUne
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subtitle.
     *
     * @param string $subtitle
     *
     * @return ALaUne
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle.
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set textMea
     *
     * @param string $textMea
     *
     * @return ALaUne
     */
    public function setTextMea($textMea)
    {
        $this->textMea = $textMea;

        return $this;
    }

    /**
     * Get textMea
     *
     * @return string
     */
    public function getTextMea()
    {
        return $this->textMea;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return ALaUne
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set linkLabel
     *
     * @param string $linkLabel
     *
     * @return ALaUne
     */
    public function setLinkLabel($linkLabel)
    {
        $this->linkLabel = $linkLabel;

        return $this;
    }

    /**
     * Get linkLabel
     *
     * @return string
     */
    public function getLinkLabel()
    {
        return $this->linkLabel;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return ALaUne
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(CategoryALaUne $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        } else {
            return '';
        }
    }

    /**
     * Set subGroups.
     *
     * @param \Doctrine\Common\Collections\Collection $subGroups
     *
     * @return ALaUne
     */
    public function setSubGroups($subGroups)
    {
        $this->subGroups = $subGroups;

        return $this;
    }

    /**
     * Get subGroups.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubGroups()
    {
        return $this->subGroups;
    }

    /**
     * Add subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return ALaUne
     */
    public function addSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        $this->subGroups[] = $subGroup;

        return $this;
    }

    /**
     * Remove subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return ALaUne
     */
    public function removeSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        $this->subGroups->removeElement($subGroup);

        return $this;
    }

    public function getPromoPrice()
    {
        return $this->promoPrice;
    }

    public function setPromoPrice($promoPrice)
    {
        $this->promoPrice = $promoPrice;
        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return BaseEntity
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return BaseEntity
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdateBase()
    {
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
    }

    /**
     * Get open link in new tab
     *
     * @return  bool
     */
    public function getLinkOpenInNewTab()
    {
        return $this->linkOpenInNewTab;
    }

    /**
     * Set open link in new tab
     *
     * @param  bool  $linkOpenInNewTab  open link in new tab
     *
     * @return  self
     */
    public function setLinkOpenInNewTab(bool $linkOpenInNewTab)
    {
        $this->linkOpenInNewTab = $linkOpenInNewTab;

        return $this;
    }
}
