<?php

namespace Honda\Bundle\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Honda\Bundle\NewsBundle\Entity\ALaUne;

/**
 * CategoryALaUne
 *
 * @ORM\Table(name="category_a_la_une")
 * @ORM\Entity(repositoryClass="Honda\Bundle\NewsBundle\Repository\CategoryALaUneRepository")
 */
class CategoryALaUne
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity="ALaUne", mappedBy="category")
     * 
     */
    private $aLaUne;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_published", type="boolean", options={"default": 0})
     */
    private $isPublished = false;

    public function __construct()
    {
        $this->aLaUne = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return CategoryALaUne
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return CategoryALaUne
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return Collection|ALaUne[]
     */
    public function getALaUne()
    {
        return $this->aLaUne;
    }

    public function setALaUne($aLaUne)
    {
        $this->aLaUne = $aLaUne;
        return $this;
    }

    public function addALaUne(ALaUne $aLaUne)
    {
 
        $this->aLaUne[] = $aLaUne;
        $aLaUne->setCategory($this);

        return $this;
    }

    public function removeALaUne(ALaUne $aLaUne)
    {

        $this->aLaUne->removeElement($aLaUne);

        return $this;
    }

    /**
     * Set isPublished.
     *
     * @param bool $isPublished
     *
     * @return CategoryALaUne
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished.
     *
     * @return bool
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }
    
}
