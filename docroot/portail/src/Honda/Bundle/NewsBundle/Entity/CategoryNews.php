<?php

namespace Honda\Bundle\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Honda\Bundle\NewsBundle\Entity\News;

/**
 * CategoryNews
 *
 * @ORM\Table(name="category_news")
 * @ORM\Entity(repositoryClass="Honda\Bundle\NewsBundle\Repository\CategoryNewsRepository")
 */
class CategoryNews
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;
    
    /**
     * @ORM\OneToMany(targetEntity="News", mappedBy="category")
     */
    private $news;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_published", type="boolean", options={"default": 0})
     */
    private $isPublished = false;

    public function __construct()
    {
        $this->news = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return CategoryNews
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return CategoryNews
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return Collection|News[]
     */
    public function getNews()
    {
        return $this->news;
    }

    public function setNews($news)
    {
        $this->news = $news;
        return $this;
    }

    public function addNews(News $news)
    {
        if (!$this->news->contains($news)) {
            $this->news->add($news);
            $news->setCategoryNews($this);
        }
        return $this;
    }

    public function removeNews(News $news)
    {
        if ($this->news->contains($news)) {
            $this->news->removeElement($news);
        }
        return $this;
    }

    /**
     * Set isPublished.
     *
     * @param bool $isPublished
     *
     * @return CategoryNews
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished.
     *
     * @return bool
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getName();
        } else {
            return '';
        }
    }

}
