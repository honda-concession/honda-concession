<?php

namespace Honda\Bundle\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Honda\Bundle\NewsBundle\Entity\CategoryNews;
use Honda\Bundle\CommonBundle\Entity\Traits\HomeActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\ActivationTrait;
use Honda\Bundle\CommonBundle\Entity\Traits\RedirectionTrait;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="Honda\Bundle\NewsBundle\Repository\NewsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class News
{

    use HomeActivationTrait;
    use ActivationTrait;
    use RedirectionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="maintitle", type="string", length=255)
     */
    private $mainTitle;

    /**
     * @Gedmo\Slug(fields={"mainTitle"})
     * @ORM\Column(unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="description2", type="text", nullable=true)
     */
    private $description2;

    /**
     * @var string
     *
     * @ORM\Column(name="description3", type="text", nullable=true)
     */
    private $description3;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    protected $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    protected $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="CategoryNews", inversedBy="news", cascade={"persist"})
     * @ORM\JoinColumn(name="category_news_id", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Slider" , mappedBy="news",cascade={"all"}, fetch="LAZY", orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @ORM\OrderBy({"pos" = "ASC"})
     */
    private $sliders;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\Honda\Bundle\DistributorBundle\Entity\SubGroup", cascade={"persist"})
     */
    private $subGroups;

    /**
     * @var integer
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", length=255)
     */
    private $position;

        /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $moreInfoPdf;

    /**
     * @var string
     *
     * @ORM\Column(name="more_info_link", type="string", length=255, nullable=true)
     */
    private $moreInfoLink;

    /**
     * @var boolean
     *
     * @ORM\Column(name="more_info_link_in_new_windows", type="boolean", nullable=true)
     */
    private $moreInfoLinkInNewWindow = false;

    /**
     * @var string
     *
     * @ORM\Column(name="more_infor_label", type="string", length=255, nullable=true)
     */
    private $moreInfoLabel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    protected $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    protected $created;

    public function __construct()
    {
        $this->created = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));

        $this->subGroups = new ArrayCollection();
        $this->sliders = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return News
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description2
     *
     * @param string $description2
     *
     * @return News
     */
    public function setDescription2($description2)
    {
        $this->description2 = $description2;

        return $this;
    }

    /**
     * Get description2
     *
     * @return string
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * Set description3
     *
     * @param string $description3
     *
     * @return News
     */
    public function setDescription3($description3)
    {
        $this->description3 = $description3;

        return $this;
    }

    /**
     * Get description3
     *
     * @return string
     */
    public function getDescription3()
    {
        return $this->description3;
    }

    /**
     * Set image
     *
     * @param MediaInterface $image
     *
     * @return Product
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return MediaInterface
     */
    public function getImage()
    {
        return $this->image;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(CategoryNews $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getMainTitle();
        } else {
            return '';
        }
    }

    /**
     * Set mainTitle.
     *
     * @param string $mainTitle
     *
     * @return News
     */
    public function setMainTitle($mainTitle)
    {
        $this->mainTitle = $mainTitle;

        return $this;
    }

    /**
     * Get mainTitle.
     *
     * @return string
     */
    public function getMainTitle()
    {
        return $this->mainTitle;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return News
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return News
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Add slider.
     *
     * @param \Honda\Bundle\NewsBundle\Entity\Slider $slider
     *
     * @return News
     */
    public function addSlider($slider)
    {
        $this->sliders[] = $slider;
        $slider->setNews($this);
        return $this;
    }

    /**
     * Remove slider.
     *
     * @param \Honda\Bundle\NewsBundle\Entity\Slider $slider
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSlider(\Honda\Bundle\NewsBundle\Entity\Slider $slider)
    {
        return $this->sliders->removeElement($slider);
    }

    /**
     * Get slider.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSliders()
    {
        return $this->sliders;
    }

    /**
     * Set slider.
     *
     * @param $slider
     *
     * @return News
     */
    public function setSliders($sliders)
    {
        $this->sliders = $sliders;

        return $this;
    }

    /**
     * Set subGroups.
     *
     * @param \Doctrine\Common\Collections\Collection $subGroups
     *
     * @return News
     */
    public function setSubGroups($subGroups)
    {
        $this->subGroups = $subGroups;

        return $this;
    }

    /**
     * Get subGroups.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubGroups()
    {
        return $this->subGroups;
    }

    /**
     * Add subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return News
     */
    public function addSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        $this->subGroups[] = $subGroup;

        return $this;
    }

    /**
     * Remove subGroup.
     *
     * @param \Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup
     *
     * @return News
     */
    public function removeSubGroup(\Honda\Bundle\DistributorBundle\Entity\SubGroup $subGroup)
    {
        $this->subGroups->removeElement($subGroup);

        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return News
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return News
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdateBase()
    {
        $this->updated = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
    }

    /**
     * Get the value of moreInfopdf
     *
     * @return  \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMoreInfoPdf()
    {
        return $this->moreInfoPdf;
    }

    /**
     * Set the value of moreInfopdf
     *
     * @param  \Application\Sonata\MediaBundle\Entity\Media  $moreInfopdf
     *
     * @return  self
     */
    public function setMoreInfoPdf(\Application\Sonata\MediaBundle\Entity\Media $moreInfoPdf = null)
    {
        $this->moreInfoPdf = $moreInfoPdf;

        return $this;
    }

    /**
     * Get the value of moreInfolink
     *
     * @return  string
     */
    public function getMoreInfoLink()
    {
        return $this->moreInfoLink;
    }

    /**
     * Set the value of moreInfolink
     *
     * @param  string  $moreInfolink
     *
     * @return  self
     */
    public function setMoreInfoLink(?string $moreInfoLink)
    {
        $this->moreInfoLink = $moreInfoLink;

        return $this;
    }

    /**
     * Get the value of moreInfoLabel
     *
     * @return  string
     */
    public function getMoreInfoLabel()
    {
        return $this->moreInfoLabel;
    }

    /**
     * Set the value of moreInfoLabel
     *
     * @param  string  $moreInfoLabel
     *
     * @return  self
     */
    public function setMoreInfoLabel(?string $moreInfoLabel)
    {
        $this->moreInfoLabel = $moreInfoLabel;

        return $this;
    }

    /**
     * Get the value of moreInfoLinkInNewWindow
     *
     * @return  boolean
     */
    public function getMoreInfoLinkInNewWindow()
    {
        return $this->moreInfoLinkInNewWindow;
    }

    /**
     * Set the value of moreInfoLinkInNewWindow
     *
     * @param  boolean  $moreInfoLinkInNewWindow
     *
     * @return  self
     */
    public function setMoreInfoLinkInNewWindow(bool $moreInfoLinkInNewWindow)
    {
        $this->moreInfoLinkInNewWindow = $moreInfoLinkInNewWindow;

        return $this;
    }
}
