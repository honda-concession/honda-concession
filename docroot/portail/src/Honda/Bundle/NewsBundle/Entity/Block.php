<?php

namespace Honda\Bundle\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Block
 *
 * @ORM\Table(name="block")
 * @ORM\Entity(repositoryClass="Honda\Bundle\NewsBundle\Repository\BlockRepository")
 */
class Block
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;
    
    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     */
    protected $label;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * 
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $pdf;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ctalabel", type="string", length=255)
     */
    private $ctaLabel;
    
    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;
    
    /**
     * @ORM\ManyToOne(targetEntity="News", inversedBy="block")
     * @ORM\JoinColumn(name="news_id", referencedColumnName="id", nullable=true)
     */
    private $news;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Block
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return Block
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set label.
     *
     * @param string $label
     *
     * @return Block
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set ctaLabel.
     *
     * @param string $ctaLabel
     *
     * @return Block
     */
    public function setCtaLabel($ctaLabel)
    {
        $this->ctaLabel = $ctaLabel;

        return $this;
    }

    /**
     * Get ctaLabel.
     *
     * @return string
     */
    public function getCtaLabel()
    {
        return $this->ctaLabel;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return Block
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set pdf.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $pdf
     *
     * @return Block
     */
    public function setPdf(\Application\Sonata\MediaBundle\Entity\Media $pdf = null)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getPdf()
    {
        return $this->pdf;
    }
    
    /**
     * Objet to string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        } else {
            return '';
        }
    }


    /**
     * Set news.
     *
     * @param \Honda\Bundle\NewsBundle\Entity\News|null $news
     *
     * @return Block
     */
    public function setNews(\Honda\Bundle\NewsBundle\Entity\News $news = null)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news.
     *
     * @return \Honda\Bundle\NewsBundle\Entity\News|null
     */
    public function getNews()
    {
        return $this->news;
    }
}
