<?php

namespace Honda\Bundle\NewsBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;

use Honda\Bundle\CommonBundle\Admin\HomePublishableAdmin;
use Honda\Bundle\CommonBundle\Admin\Traits\RedirectionAdminTrait;
use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;

class ALaUneAdmin extends HomePublishableAdmin
{

    use RedirectionAdminTrait,
        ActivationAdminTrait;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('position', 'actions', array(
                    'label' => 'Position',
                    'actions' => array(
                        'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig')
                    )))
                ->add('image', 'string', [
                    'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'
                ])
                ->add('title', null, [
                    'label' => 'Titre'
                        ]
                )
                ->add('subGroups.name', null, [
                    'label' => 'Groupes'
                        ]
                )
                ->add('published', null, ['editable' => true])
                ->add('publishedOnHome', null, ['editable' => true])
                ->add('startDate', 'datetime', ['format' => 'd/m/Y', 'label' => 'Date début'])
                ->add('endDate', 'datetime', ['format' => 'd/m/Y', 'label' => 'Date fin'])
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('A La Une', array('class' => 'col-md-9'))
                ->add('category', 'sonata_type_model', [
                    'label' => 'Catégorie',
                    'required' => true,
                    'multiple' => false,
                    'by_reference' => false,
                    'btn_add' => false,
                        ]
                )
                ->add('title', null, array(
                    'label' => 'Titre'
                ))
                ->add('textMea', null, array(
                    'label' => 'Cylindrée'
                ))
                ->add('subtitle', null, array(
                    'label' => 'Soustitre'
                ))
                ->add('promoPrice', null, array(
                    'label' => 'Prix promo'
                ))
                ->add('linkLabel', null, array(
                    'label' => 'Label Bouton',
                    'required' => false
                ))
                ->add('link', null, array(
                    'label' => 'Lien Bouton',
                    'required' => false
                ))
                ->add('linkOpenInNewTab', null, [
                    'label' => 'Ouvrir dans une nouvelle fenêtre',
                    'required' => false,
                ])
                ->add('image', 'sonata_type_model_list', [
                    'label' => 'Image',
                    'required' => false,
                        ], [
                    'link_parameters' => [
                        'context' => 'alaune',
                        'filter' => [
                            'context' => [
                                'value' => 'alaune'
                            ]
                        ]
                    ]
                ])
                ->add('subGroups', 'sonata_type_model', [
                    'multiple' => true,
                    'required' => false,
                    'by_reference' => false,
                    'label' => 'Groupes',
                    'property' => 'name',
                    'btn_add' => false
                ])
            ->end()
            ->with('Activation', array('class' => 'col-md-3'))
                ->add('startDate', DateTimePickerType::class, ['format'=>'dd/MM/yyyy H:mm:ss', 'label' => 'Date de début de publication',])
                ->add('endDate', DateTimePickerType::class, [ 'format'=>'dd/MM/yyyy H:mm:ss', 'label' => 'Date de fin de publication',])
                ->add('published', null, [
                        'label' => 'Publié',
                        'required' => false,
                    ]
                )
                ->add('publishedOnHome', null, [
                    'label' => 'Publier sur la page d\'accueil',
                    'required' => false,
                ])
            ->end();
        $this->addRedirectionBlock($formMapper);
    }

    /**
     * @inheritdoc
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
        parent::configureRoutes($collection);
    }
}
