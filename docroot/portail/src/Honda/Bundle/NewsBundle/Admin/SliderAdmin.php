<?php

namespace Honda\Bundle\NewsBundle\Admin;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SliderAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('image')
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('text', TextType::class)
            ->add('image', 'sonata_type_model_list', [
                    'required' => true,
                    'label' => 'Image',
                    'by_reference' => true,
                    'btn_delete' => false,
                    'btn_edit' => false
                ], [
                    'link_parameters' => [
                        'context' => 'news_slider',
                        'filter' => [
                            'context' => [
                                'value' => 'news_slider'
                            ]
                        ]
                    ]
                ]
            )
        ;
    }
}
