<?php

namespace Honda\Bundle\NewsBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Route\RouteCollection;

use Honda\Bundle\CommonBundle\Admin\HomePublishableAdmin;
use Honda\Bundle\CommonBundle\Admin\Traits\RedirectionAdminTrait;
use Honda\Bundle\CommonBundle\Admin\Traits\ActivationAdminTrait;

class NewsAdmin extends HomePublishableAdmin
{
    use RedirectionAdminTrait,
        ActivationAdminTrait;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->add('position', 'actions', array(
                'actions' => array(
                    'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig')
                )
            ))
            ->add('mainTitle', null, array(
                'label' => 'Titre'
            ))
            ->add('startDate', 'datetime', [
                'format' => 'd/m/Y H:i:s',
                'label' => 'Date de début de publication'
                    ]
            )
            ->add('endDate', 'datetime', [
                'format' => 'd/m/Y H:i:s',
                'label' => 'Date de fin de publication'
                    ]
            )
            ->add('created', 'datetime', [
                'format' => 'd/m/Y H:i:s',
                'label' => 'Date de publication'
                    ]
            )
            ->add('published', null, array('editable' => true))
            ->add('publishedOnHome', null, ['editable' => true])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->with('Actualité', array('class' => 'col-md-9'))
                    ->add('mainTitle', null, array(
                        'label' => 'Titre'
                    ))
                    ->add('category', 'sonata_type_model', [
                        'label' => 'Category',
                        'required' => true,
                        'multiple' => false,
                        'by_reference' => true,
                        'btn_add' => false,
                            ]
                    )
                    ->add('image', 'sonata_type_model_list', [
                        'label' => 'Image principale',
                        'required' => true,
                            ], [
                        'link_parameters' => [
                            'context' => 'image',
                            'filter' => [
                                'context' => [
                                    'value' => 'image'
                                ]
                            ]
                        ]
                            ]
                    )
                    ->add('description', CKEditorType::class, [
                        'label' => 'Accroche',
                        'config_name' => 'simple'
                            ]
                    )
                    ->add('description2', CKEditorType::class, [
                        'label' => 'Contenu de l\'actualité avant slideshow',
                        'required' => false,
                        'config_name' => 'default',
                        'config' => [
                            'stylesSet' => 'eidtor_styles'
                        ]
                            ]
                    )
                    ->add('sliders', 'sonata_type_collection', [
                        'label' => 'Slideshow',
                        'required' => true,
                        'by_reference' => true,
                            ], [
                        'edit' => 'inline',
                        'sortable' => 'pos',
                        'inline' => 'table',
                            ]
                    )
                    ->add('description3', CKEditorType::class, [
                        'label' => 'Contenu de l\'actualité après slideshow',
                        'required' => false,
                        'config_name' => 'default',
                        'config' => [
                            'stylesSet' => 'eidtor_styles'
                        ]
                            ]
                    )
                    ->add('subGroups', 'sonata_type_model', [
                        'multiple' => true,
                        'required' => false,
                        'by_reference' => false,
                        'label' => 'Groupes',
                        'property' => 'name',
                        'btn_add' => false
                    ])
                ->end();

        $formMapper
                ->with('Activation', array('class' => 'col-md-3'))
                    ->add('startDate', 'sonata_type_datetime_picker', array(
                        'label' => 'Date de début de publication',
                        'format' => 'dd/MM/yyyy HH:mm:ss',
                    ))
                    ->add('endDate', 'sonata_type_datetime_picker', array(
                        'label' => 'Date de fin de publication',
                        'format' => 'dd/MM/yyyy HH:mm:ss',
                    ))
                    ->add('published', null, array(
                        'label' => 'Actif'
                    ))
                    ->add('publishedOnHome', null, [
                        'label' => 'Publier sur la page d\'accueil',
                        'required' => false,
                    ])
                ->end();
        $this->addRedirectionBlock($formMapper);
        $formMapper
                ->with('Bouton', ['class' => 'col-md-9'])
                    ->add('moreInfoLabel', null, [
                        'label' => 'Libellé',
                        'required' => false,
                    ])
                    ->add('moreInfoPdf', 'sonata_type_model_list', [
                        'label' => 'Pdf',
                        'required' => false,
                    ], [
                        'link_parameters' => [
                            'context' => 'file',
                            'filter' => [
                                'context' => [
                                    'value' => 'file'
                                ]
                            ]
                        ]
                    ])
                    ->add('moreInfoLink', null, [
                        'label' => 'Lien',
                        'required' => false
                    ])
                    ->add('moreInfoLinkInNewWindow', null, [
                        'label' => 'Ouvrir le lien dans une nouvelle fenêtre',
                        'required' => false,
                    ])
                ->end();
    }

    /**
     * @inheritdoc
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        foreach ($object->getSliders() as $slider) {
            $slider->setNews($object);
        }
    }

    /**
     * @inheritdoc
     */
    public function preUpdate($object)
    {
        parent::preUpdate($object);
        foreach ($object->getSliders() as $slider) {
            $slider->setNews($object);
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }
}
