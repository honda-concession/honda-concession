<?php

namespace Honda\Bundle\NewsBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;


class BlockAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null, array(
                'label' => 'Titre'
            ))
            ->add('text',null, array(
                'label' => 'Texte'
            ))
            ->add('pdf','sonata_type_model_list', [
                    'label' => 'Document(PDF)',
                    'required' => true,
                    'by_reference' => true,
                        ], [
                    'link_parameters' => [
                        'context' => 'file',
                        'filter' => [
                            'context' => [
                                'value' => 'file'
                            ]
                        ]
                    ]
                ])
            ->add('label',null, array(
                'label' => 'Intitulé du doc'
            ))
            ->add('link',null, array(
                'label' => 'Lien'
            ))
            ->add('ctaLabel',null, array(
                'label' => 'Intitulé du bouton'
            ))

        ;
    }

}
