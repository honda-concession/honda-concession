<?php

namespace Honda\Bundle\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;


class NewsController extends Controller
{
    /**
     * Pour le SEO, URL Canonical
     *
     * @Route("/actualites/{slug}", name="actualites")
     * @param Request $request
     * @param sting $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $slug)
    {
        $news = $this->getNews($slug);

        if (empty($news)) {
            throw $this->createNotFoundException();
        }

        return $this->render('HondaNewsBundle:News:show.html.twig', [
            'article' => $news,
        ]);
    }

    /**
     * @param $slug
     * @return array|mixed
     */
    protected function getNews($slug)
    {
        $client = new Client();
        $sslVerification = false;
        $env = $this->get('kernel')->getEnvironment();

        if ($env != "dev") {
            $sslVerification = true;
        }

        $endpoint = $this->generateUrl('api_get_news', ['slug' => $slug], UrlGeneratorInterface::ABSOLUTE_URL);

        try {
            $res = $client->request('GET', $endpoint, ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }

        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {

            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json);

                return $responseData;
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
        }

        return [];
    }

}
