<?php

namespace Honda\Bundle\NewsBundle\Controller\Api;

use Honda\Bundle\CommonBundle\Service\MediaUrl;
use Honda\Bundle\DistributorBundle\Entity\Distributor;
use Honda\Bundle\NewsBundle\Entity\News;
use Honda\Bundle\NewsBundle\Entity\CategoryALaUne;
use Honda\Bundle\NewsBundle\Entity\CategoryNews;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class NewsController
 * @package Honda\Bundle\NewsBundle\Controller\Api
 *
 */
class NewsController extends Controller
{

    /**
     * @Route("/api/news/{distributorId}/{filter}", name="api_get_news_list", requirements={"filter": "\d+", "distributorId": "\d+"}, methods={"GET"})
     * @param $distributorId
     * @param null $filter
     * @return JsonResponse
     */
    public function getNewsListAction($distributorId, $filter = null)
    {
        $baseUrl = $this->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();
        $data = [];

        $request = $this->get('request_stack')->getCurrentRequest();

        $all = $request->query->get('all', false);

        $subGroupIds = $this->getDoctrine()->getManager()->getRepository(Distributor::class)->getSubGroupIds($distributorId);

        $news = $this->getDoctrine()->getManager()->getRepository(News::class)->getNews($subGroupIds, $filter, $all);

        foreach ($news as $key => $itemNews) {
            if (!$itemNews->isPublishedOnHome()) {
                continue;
            }
            $data[$key]['id'] = $itemNews->getId();
            $data[$key]['title'] = $itemNews->getMainTitle();
            $data[$key]['type'] = $itemNews->getCategory() ? $itemNews->getCategory()->getId() : '';
            $data[$key]['image'] = $itemNews->getImage() ? $baseUrl . $this->get(MediaUrl::class)->getUrl($itemNews->getImage(), 'big') : '' ;
            $data[$key]['slug'] = $itemNews->getSlug();
            $data[$key]['description1'] = $itemNews->getDescription();
            $data[$key]['active'] = $itemNews->isPublished();
            $data[$key]['startDate'] = $itemNews->getStartDate()->getTimestamp();
            $data[$key]['created'] = $itemNews->getCreated()->getTimestamp();
            $data[$key] = $itemNews->setRedirectionValuesToArray($data[$key]);
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/news-item/{slug}", name="api_get_news", requirements={"slug": "[a-zA-Z0-9\-]+"}, methods={"GET"})
     */
    public function getNewsAction(Request $request, $slug)
    {
        $news = $this->getDoctrine()->getManager()->getRepository(News::class)->findOneBy(['slug' => $slug, 'published' => true]);
        $data = [];

        if($news) {

            $baseUrl = $request->getSchemeAndHttpHost();

            $slidesData = [];

            $slides = $news->getSliders();

            if(!empty($slides)) {
                foreach ($slides as $slide) {
                    $slidesData[] = [
                        'title' => $slide->getText(),
                        'image_big' => $slide->getImage() ? $baseUrl . $this->get(MediaUrl::class)->getUrl($slide->getImage(), 'big') : null,
                        'image_medium' => $slide->getImage() ? $baseUrl . $this->get(MediaUrl::class)->getUrl($slide->getImage(), 'medium') : null,
                        'image_small' => $slide->getImage() ? $baseUrl . $this->get(MediaUrl::class)->getUrl($slide->getImage(), 'small') : null
                    ];
                }
            }
            $item = [
                'id' => $news->getId(),
                'title' => $news->getMainTitle(),
                'slug' => $news->getSlug(),
                'image' => $news->getImage() ? $baseUrl . $this->get(MediaUrl::class)->getUrl($news->getImage(), 'big') : null,
                'startDate' => $news->getStartDate()->getTimestamp(),
                'description1' => $news->getDescription(),
                'description2' => $news->getDescription2(),
                'description3' => $news->getDescription3(),
                'slides' => $slidesData,
                'moreInfo' => [
                    'label' => $news->getMoreInfoLabel(),
                    'link' => $news->getMoreInfoPdf() ? $baseUrl . $this->get(MediaUrl::class)->getUrl($news->getMoreInfoPdf()) : $news->getMoreInfoLink(),
                    'openInNewWindow' => $news->getMoreInfoLinkInNewWindow(),
                ]
            ];

            $data = $news->setRedirectionValuesToArray($item);
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/a-la-une/{distributorId}/{filter}", name="api_get_a_la_une", requirements={"filter": "\d+", "distributorId": "\d+"}, methods={"GET"})
     */
    public function aLaUneAction($distributorId, $filter = null)
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $all = $request->query->get('all', false);

        $subGroupIds = $this->getDoctrine()->getManager()->getRepository(Distributor::class)->getSubGroupIds($distributorId);

        $aLaUnes = $this->getDoctrine()->getRepository('HondaNewsBundle:ALaUne')->getALaUnes($subGroupIds, $filter, $all);

        $data = [];

        $this->mediaManager = $this->container->get('sonata.media.manager.media');

        $baseUrl = $request->getSchemeAndHttpHost();

        foreach ($aLaUnes as $aLaUne) {

            if(!$aLaUne->isPublishedOnHome()) {
                continue;
            }

            $media = $aLaUne->getImage();

            $mediaData = [];

            if($media) {
                $providerName = $media->getProviderName();
                $provider = $this->container->get($providerName);
                $format = $provider->getFormatName($media, 'big');
                $url = $provider->generatePublicUrl($media, $format);
                $providerReference = $media->getProviderReference();

                $mediaData = [
                    'providerName' => $providerName,
                    'providerReference' => $providerReference,
                    'url' => $baseUrl.$url
                ];
            }

            $type = null;

            $category = $aLaUne->getCategory();

            if($category) {
                $type = $category->getId();
            }

            $item = [
                'title' => $aLaUne->getTitle(),
                'subtitle' => $aLaUne->getSubtitle(),
                'textMea' => $aLaUne->getTextMea(),
                'promoPrice' => $aLaUne->getPromoPrice(),
                'link' => $aLaUne->getLink(),
                'linkLabel' => $aLaUne->getLinkLabel(),
                'linkOpenInNewTab' => $aLaUne->getLinkOpenInNewTab(),
                'media' => $mediaData,
                'type' => $type,
                'published' => $aLaUne->isPublished(),
                'created' => $aLaUne->getCreated()->getTimestamp()
            ];

            $data[] = $aLaUne->setRedirectionValuesToArray($item);
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/categories/a-la-une", name="api_get_categories_a_la_une", methods={"GET"})
     */
    public function categoriesALaUneAction()
    {
        $data = [];

        $cats = $this->getDoctrine()->getManager()->getRepository(CategoryALaUne::class)->findBy(['isPublished' => true], ['position' => 'ASC']);

        foreach ($cats as $cat) {
            array_push($data, [
                'id' => $cat->getId(),
                'name' => $cat->getName()
            ]);
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/categories/news", name="api_get_categories_news", methods={"GET"})
     */
    public function categoriesNewsAction()
    {
        $data = [];

        $cats = $this->getDoctrine()->getManager()->getRepository(CategoryNews::class)->findBy(['isPublished' => true], ['position' => 'ASC']);

        foreach ($cats as $cat) {
            array_push($data, [
                'id' => $cat->getId(),
                'name' => $cat->getName()
            ]);
        }

        return new JsonResponse($data);
    }
}
