<?php

namespace Honda\Bundle\MenuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Honda\Bundle\MenuBundle\Entity\Footer;
use Honda\Bundle\MenuBundle\Entity\Menu;
use Honda\Bundle\DistributorBundle\Entity\TemplateColor;
use Honda\Bundle\CommonBundle\Entity\Setting;

class DefaultController extends Controller
{
    public function menuAction()
    {
        $em = $this->getDoctrine()->getManager();
        $menuRepo = $em->getRepository(Menu::class);
        $menu = $menuRepo->findBy([], ['position' => 'ASC']);

        $templateColorRepo = $em->getRepository(TemplateColor::class);
        $logo = $templateColorRepo->findOneBy(['default' => 1]);

        return $this->render('HondaMenuBundle:Default:menu.html.twig', array(
                    'menu' => $menu,
                    'logo' => $logo
        ));
    }

    public function footerAction()
    {
        $em = $this->getDoctrine()->getManager();
        $templateColorRepo = $em->getRepository(TemplateColor::class);
        $logo = $templateColorRepo->findOneBy(['default' => 1]);

        $footerRepo = $em->getRepository(Footer::class);
        $footerLeft = $footerRepo->findBy(['direction' => 'gauche'], ['position' => 'ASC']);
        $footerRight = $footerRepo->findBy(['direction' => 'droite'], ['position' => 'ASC']);

        $settingRepository = $em->getRepository(Setting::class);
        $setting = $settingRepository->findOneByKey('ga-code');

        return $this->render('HondaMenuBundle:Default:footer.html.twig', array(
            'right' => $footerRight,
            'left' => $footerLeft,
            'logo' => $logo,
            'gaCode' => $setting ? $setting->getValue() : null,
        ));
    }
}
