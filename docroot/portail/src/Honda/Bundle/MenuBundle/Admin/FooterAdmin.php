<?php

namespace Honda\Bundle\MenuBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Honda\Bundle\MenuBundle\Entity\Footer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FooterAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'footer';

    public $last_position = 0;

    private $positionService;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public function toString($object){

        return $object instanceof Footer
            ? "Menu Footer"
            : 'Ajouter au menu';
    }

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('position')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('_action', 'actions', array(
                    'label' => 'Position',
                    'actions' => array(
                        'move' => array('template' => 'PixSortableBehaviorBundle:Default:_sort_drag_drop.html.twig' ,'enable_top_bottom_buttons' => false),
            )))
        ;
        $listMapper->addIdentifier('name', null, array('label' => 'Nom'));
        $listMapper
                ->add('_actions', 'actions', array(
                    'label' => 'Actions',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array())))
        ;

    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ajouter un menu', array('class' => 'col-md-12'))
            ->add('name', null, array(
                'label' => 'Menu'
            ))
            ->add('path', null, array(
                'label' => 'Chemin',
                'required' => true,
            ))
            ->add('external', null, array('required' => false, "label" => "Ouverture dans un nouvel onglet"))
            ->add('direction', ChoiceType::class, array(
                    'choices' => array(
                        'Gauche' => 'gauche',
                        'Droite' => 'droite',
                    ))
                )
           
            ->end()
        ;
    }

    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }
}
