<?php

namespace Honda\Bundle\MenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Menu
 *
 * @ORM\Table(name="menu")
 * @ORM\Entity(repositoryClass="Honda\Bundle\MenuBundle\Repository\MenuRepository")
 */
class Menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;
    
    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="text")
     */
    private $icon;
    
    /**
     * @var integer
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", length=255)
     */
    private $position;
    
    /**
     * @var string
     *
     * @ORM\Column(name="external", type="boolean", nullable=true ,options={"default":"0"})
     */
    private $external;

    /**
     * display menu item only for mobile
     *
     * @var bool
     * @ORM\Column(name="mobile_only", type="boolean", nullable=true ,options={"default":"0"})
     */
    private $mobileOnly;

    /**
     * Gets the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the value of position.
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the value of position.
     *
     * @param integer $position the position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }
    
    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Footer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path.
     *
     * @param string $path
     *
     * @return Footer
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Set icon.
     *
     * @param string $icon
     *
     * @return Menu
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
    
    /**
     * Set external.
     *
     * @param string|null $external
     *
     * @return Footer
     */
    public function setExternal($external = null)
    {
        $this->external = $external;

        return $this;
    }

    /**
     * Get external.
     *
     * @return string|null
     */
    public function getExternal()
    {
        return $this->external;
    }


    /**
     * Get display menu item only for mobile
     *
     * @return  bool
     */
    public function getMobileOnly()
    {
        return $this->mobileOnly;
    }

    /**
     * Set display menu item only for mobile
     *
     * @param  bool  $mobileOnly  display menu item only for mobile
     *
     * @return  self
     */
    public function setMobileOnly(bool $mobileOnly)
    {
        $this->mobileOnly = $mobileOnly;

        return $this;
    }
}
