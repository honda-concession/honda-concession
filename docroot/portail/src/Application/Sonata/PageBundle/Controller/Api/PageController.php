<?php

namespace Application\Sonata\PageBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\DatagridBundle\Pager\PagerInterface;

use Honda\Bundle\CommonBundle\Entity\Cookies;

class PageController extends Controller
{
    /**
     * @Route("/api/page/{slug}", methods={"GET"}, name="page_api_get")
     */
    public function getAction(Request $request)
    {
        $slug = $request->get('slug');
        if ($slug == 'cookies') {
            $em = $this->getDoctrine()->getManager();
            $cookies = $em->getRepository(Cookies::class)->findAll();
            if (count($cookies) >= 1) {
                $cookie = end($cookies);
                $questions = [];
                foreach($cookie->getCookiesQuestions() as $question) {
                    $questions[] = [
                        'question' => $question->getQuestion(),
                        'answer' => $question->getAnswer(),
                        'isCookieQuestion' => true,
                    ];
                }
                foreach($cookie->getViePriveeQuestions() as $question) {
                    $questions[] = [
                        'question' => $question->getQuestion(),
                        'answer' => $question->getAnswer(),
                        'isCookieQuestion' => false,
                    ];
                }

                return $this->json([
                    'status' => 'success',
                    'content' => [
                        'title' => $cookie->getTitle(),
                        'message' => $cookie->getMessage(),
                        'viePriveeIntro' => $cookie->getViePriveeIntro(),
                        'cookiesIntro' => $cookie->getCookiesIntro(),
                        'questions' => $questions
                    ]
                ]);
            }
        }
        $page = $this->get('sonata.page.manager.page')->findOneBy(['slug' => $slug]);
        if ($blocks = $page->getBlocks()) {
            if ($children = $blocks[0]->getChildren()) {
                $child = $children[0];
                if ($child) {
                    return $this->json([
                        'status' => 'success',
                        'content' => $child->getSettings()['rawContent']
                    ]);
                }
            }
        }

        return $this->json(['status' => 'failed']);
    }
}
