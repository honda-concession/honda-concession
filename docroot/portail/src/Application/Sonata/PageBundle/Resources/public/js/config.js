window.config = {
  "urls": {
    "css": "css/",
    "dyn": "media/dyn/",
    "img": "media/img/",
    "fonts": "media/fonts/",
    "config": "config/",
    "js": "js/"
  },
  "mq": {
    "touch": "(max-width: 64em)",
    "tablet": "(min-width: 48em)",
    "desktop": {
      "low": "(min-width: 64em)",
      "large": "(min-width: 80em)"
    }
  },
  "rendered": false,
  "dark": "#000000",
  "light": "#ffffff",
  "color": "$dark",
  "grey1": "#3a3a3a",
  "grey2": "#585858",
  "grey3": "#e6e5e9",
  "grey4": "#4f4f4f",
  "grey5": "#959595",
  "grey6": "#d2d2d4",
  "grey7": "#f2f2f2",
  "grey8": "#777777",
  "grey9": "#b2b2b2",
  "red1": "#cc0000",
  "red2": "#e82727",
  "red3": "#dd0111",
  "red4": "#ef1f03",
  "blue1": "#2c83ee",
  "facebook": "#3b5999",
  "twitter": "#55acee",
  "youtube": "#cd201f",
  "instagram": "#e4405f",
  "bg-color": "$grey3",
  "font-sans-serif": "\"proxima-nova\",sans-serif",
  "font-sans-serif-condensed": "\"proxima-nova-condensed\",\"proxima-nova\",sans-serif",
  "font-serif": "Georgia, Times, \"Times New Roman\", serif",
  "font-family": "$font-sans-serif",
  "ff": "$font-sans-serif",
  "ff-c": "$font-sans-serif-condensed",
  "font-size": 14,
  "font-root-size": 16,
  "fw-light": 300,
  "fw-reg": 400,
  "fw-bold": 700,
  "anim-dur": ".2s",
  "anim-easing": "ease-out",
  "anim-delay": ".2s",
  "conf": {
    "templates": {
      "video": "<iframe src=\"${src}\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>"
    }
  },
  "l10n": null
};