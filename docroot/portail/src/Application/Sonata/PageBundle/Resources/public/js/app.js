(function () {
	'use strict';

	var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var _global = createCommonjsModule(function (module) {
	// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
	var global = module.exports = typeof window != 'undefined' && window.Math == Math
	  ? window : typeof self != 'undefined' && self.Math == Math ? self
	  // eslint-disable-next-line no-new-func
	  : Function('return this')();
	if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef
	});

	var hasOwnProperty = {}.hasOwnProperty;
	var _has = function (it, key) {
	  return hasOwnProperty.call(it, key);
	};

	var _fails = function (exec) {
	  try {
	    return !!exec();
	  } catch (e) {
	    return true;
	  }
	};

	// Thank's IE8 for his funny defineProperty
	var _descriptors = !_fails(function () {
	  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
	});

	var _core = createCommonjsModule(function (module) {
	var core = module.exports = { version: '2.5.7' };
	if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef
	});
	var _core_1 = _core.version;

	var _isObject = function (it) {
	  return typeof it === 'object' ? it !== null : typeof it === 'function';
	};

	var _anObject = function (it) {
	  if (!_isObject(it)) throw TypeError(it + ' is not an object!');
	  return it;
	};

	var document$1 = _global.document;
	// typeof document.createElement is 'object' in old IE
	var is = _isObject(document$1) && _isObject(document$1.createElement);
	var _domCreate = function (it) {
	  return is ? document$1.createElement(it) : {};
	};

	var _ie8DomDefine = !_descriptors && !_fails(function () {
	  return Object.defineProperty(_domCreate('div'), 'a', { get: function () { return 7; } }).a != 7;
	});

	// 7.1.1 ToPrimitive(input [, PreferredType])

	// instead of the ES6 spec version, we didn't implement @@toPrimitive case
	// and the second argument - flag - preferred type is a string
	var _toPrimitive = function (it, S) {
	  if (!_isObject(it)) return it;
	  var fn, val;
	  if (S && typeof (fn = it.toString) == 'function' && !_isObject(val = fn.call(it))) return val;
	  if (typeof (fn = it.valueOf) == 'function' && !_isObject(val = fn.call(it))) return val;
	  if (!S && typeof (fn = it.toString) == 'function' && !_isObject(val = fn.call(it))) return val;
	  throw TypeError("Can't convert object to primitive value");
	};

	var dP = Object.defineProperty;

	var f = _descriptors ? Object.defineProperty : function defineProperty(O, P, Attributes) {
	  _anObject(O);
	  P = _toPrimitive(P, true);
	  _anObject(Attributes);
	  if (_ie8DomDefine) try {
	    return dP(O, P, Attributes);
	  } catch (e) { /* empty */ }
	  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
	  if ('value' in Attributes) O[P] = Attributes.value;
	  return O;
	};

	var _objectDp = {
		f: f
	};

	var _propertyDesc = function (bitmap, value) {
	  return {
	    enumerable: !(bitmap & 1),
	    configurable: !(bitmap & 2),
	    writable: !(bitmap & 4),
	    value: value
	  };
	};

	var _hide = _descriptors ? function (object, key, value) {
	  return _objectDp.f(object, key, _propertyDesc(1, value));
	} : function (object, key, value) {
	  object[key] = value;
	  return object;
	};

	var id = 0;
	var px = Math.random();
	var _uid = function (key) {
	  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
	};

	var _redefine = createCommonjsModule(function (module) {
	var SRC = _uid('src');
	var TO_STRING = 'toString';
	var $toString = Function[TO_STRING];
	var TPL = ('' + $toString).split(TO_STRING);

	_core.inspectSource = function (it) {
	  return $toString.call(it);
	};

	(module.exports = function (O, key, val, safe) {
	  var isFunction = typeof val == 'function';
	  if (isFunction) _has(val, 'name') || _hide(val, 'name', key);
	  if (O[key] === val) return;
	  if (isFunction) _has(val, SRC) || _hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
	  if (O === _global) {
	    O[key] = val;
	  } else if (!safe) {
	    delete O[key];
	    _hide(O, key, val);
	  } else if (O[key]) {
	    O[key] = val;
	  } else {
	    _hide(O, key, val);
	  }
	// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
	})(Function.prototype, TO_STRING, function toString() {
	  return typeof this == 'function' && this[SRC] || $toString.call(this);
	});
	});

	var _aFunction = function (it) {
	  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
	  return it;
	};

	// optional / simple context binding

	var _ctx = function (fn, that, length) {
	  _aFunction(fn);
	  if (that === undefined) return fn;
	  switch (length) {
	    case 1: return function (a) {
	      return fn.call(that, a);
	    };
	    case 2: return function (a, b) {
	      return fn.call(that, a, b);
	    };
	    case 3: return function (a, b, c) {
	      return fn.call(that, a, b, c);
	    };
	  }
	  return function (/* ...args */) {
	    return fn.apply(that, arguments);
	  };
	};

	var PROTOTYPE = 'prototype';

	var $export = function (type, name, source) {
	  var IS_FORCED = type & $export.F;
	  var IS_GLOBAL = type & $export.G;
	  var IS_STATIC = type & $export.S;
	  var IS_PROTO = type & $export.P;
	  var IS_BIND = type & $export.B;
	  var target = IS_GLOBAL ? _global : IS_STATIC ? _global[name] || (_global[name] = {}) : (_global[name] || {})[PROTOTYPE];
	  var exports = IS_GLOBAL ? _core : _core[name] || (_core[name] = {});
	  var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
	  var key, own, out, exp;
	  if (IS_GLOBAL) source = name;
	  for (key in source) {
	    // contains in native
	    own = !IS_FORCED && target && target[key] !== undefined;
	    // export native or passed
	    out = (own ? target : source)[key];
	    // bind timers to global for call from export context
	    exp = IS_BIND && own ? _ctx(out, _global) : IS_PROTO && typeof out == 'function' ? _ctx(Function.call, out) : out;
	    // extend global
	    if (target) _redefine(target, key, out, type & $export.U);
	    // export
	    if (exports[key] != out) _hide(exports, key, exp);
	    if (IS_PROTO && expProto[key] != out) expProto[key] = out;
	  }
	};
	_global.core = _core;
	// type bitmap
	$export.F = 1;   // forced
	$export.G = 2;   // global
	$export.S = 4;   // static
	$export.P = 8;   // proto
	$export.B = 16;  // bind
	$export.W = 32;  // wrap
	$export.U = 64;  // safe
	$export.R = 128; // real proto method for `library`
	var _export = $export;

	var _meta = createCommonjsModule(function (module) {
	var META = _uid('meta');


	var setDesc = _objectDp.f;
	var id = 0;
	var isExtensible = Object.isExtensible || function () {
	  return true;
	};
	var FREEZE = !_fails(function () {
	  return isExtensible(Object.preventExtensions({}));
	});
	var setMeta = function (it) {
	  setDesc(it, META, { value: {
	    i: 'O' + ++id, // object ID
	    w: {}          // weak collections IDs
	  } });
	};
	var fastKey = function (it, create) {
	  // return primitive with prefix
	  if (!_isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
	  if (!_has(it, META)) {
	    // can't set metadata to uncaught frozen object
	    if (!isExtensible(it)) return 'F';
	    // not necessary to add metadata
	    if (!create) return 'E';
	    // add missing metadata
	    setMeta(it);
	  // return object ID
	  } return it[META].i;
	};
	var getWeak = function (it, create) {
	  if (!_has(it, META)) {
	    // can't set metadata to uncaught frozen object
	    if (!isExtensible(it)) return true;
	    // not necessary to add metadata
	    if (!create) return false;
	    // add missing metadata
	    setMeta(it);
	  // return hash weak collections IDs
	  } return it[META].w;
	};
	// add metadata on freeze-family methods calling
	var onFreeze = function (it) {
	  if (FREEZE && meta.NEED && isExtensible(it) && !_has(it, META)) setMeta(it);
	  return it;
	};
	var meta = module.exports = {
	  KEY: META,
	  NEED: false,
	  fastKey: fastKey,
	  getWeak: getWeak,
	  onFreeze: onFreeze
	};
	});
	var _meta_1 = _meta.KEY;
	var _meta_2 = _meta.NEED;
	var _meta_3 = _meta.fastKey;
	var _meta_4 = _meta.getWeak;
	var _meta_5 = _meta.onFreeze;

	var _library = false;

	var _shared = createCommonjsModule(function (module) {
	var SHARED = '__core-js_shared__';
	var store = _global[SHARED] || (_global[SHARED] = {});

	(module.exports = function (key, value) {
	  return store[key] || (store[key] = value !== undefined ? value : {});
	})('versions', []).push({
	  version: _core.version,
	  mode: _library ? 'pure' : 'global',
	  copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
	});
	});

	var _wks = createCommonjsModule(function (module) {
	var store = _shared('wks');

	var Symbol = _global.Symbol;
	var USE_SYMBOL = typeof Symbol == 'function';

	var $exports = module.exports = function (name) {
	  return store[name] || (store[name] =
	    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : _uid)('Symbol.' + name));
	};

	$exports.store = store;
	});

	var def = _objectDp.f;

	var TAG = _wks('toStringTag');

	var _setToStringTag = function (it, tag, stat) {
	  if (it && !_has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
	};

	var f$1 = _wks;

	var _wksExt = {
		f: f$1
	};

	var defineProperty = _objectDp.f;
	var _wksDefine = function (name) {
	  var $Symbol = _core.Symbol || (_core.Symbol = _library ? {} : _global.Symbol || {});
	  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: _wksExt.f(name) });
	};

	var toString = {}.toString;

	var _cof = function (it) {
	  return toString.call(it).slice(8, -1);
	};

	// fallback for non-array-like ES3 and non-enumerable old V8 strings

	// eslint-disable-next-line no-prototype-builtins
	var _iobject = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
	  return _cof(it) == 'String' ? it.split('') : Object(it);
	};

	// 7.2.1 RequireObjectCoercible(argument)
	var _defined = function (it) {
	  if (it == undefined) throw TypeError("Can't call method on  " + it);
	  return it;
	};

	// to indexed object, toObject with fallback for non-array-like ES3 strings


	var _toIobject = function (it) {
	  return _iobject(_defined(it));
	};

	// 7.1.4 ToInteger
	var ceil = Math.ceil;
	var floor = Math.floor;
	var _toInteger = function (it) {
	  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
	};

	// 7.1.15 ToLength

	var min = Math.min;
	var _toLength = function (it) {
	  return it > 0 ? min(_toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
	};

	var max = Math.max;
	var min$1 = Math.min;
	var _toAbsoluteIndex = function (index, length) {
	  index = _toInteger(index);
	  return index < 0 ? max(index + length, 0) : min$1(index, length);
	};

	// false -> Array#indexOf
	// true  -> Array#includes



	var _arrayIncludes = function (IS_INCLUDES) {
	  return function ($this, el, fromIndex) {
	    var O = _toIobject($this);
	    var length = _toLength(O.length);
	    var index = _toAbsoluteIndex(fromIndex, length);
	    var value;
	    // Array#includes uses SameValueZero equality algorithm
	    // eslint-disable-next-line no-self-compare
	    if (IS_INCLUDES && el != el) while (length > index) {
	      value = O[index++];
	      // eslint-disable-next-line no-self-compare
	      if (value != value) return true;
	    // Array#indexOf ignores holes, Array#includes - not
	    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
	      if (O[index] === el) return IS_INCLUDES || index || 0;
	    } return !IS_INCLUDES && -1;
	  };
	};

	var shared = _shared('keys');

	var _sharedKey = function (key) {
	  return shared[key] || (shared[key] = _uid(key));
	};

	var arrayIndexOf = _arrayIncludes(false);
	var IE_PROTO = _sharedKey('IE_PROTO');

	var _objectKeysInternal = function (object, names) {
	  var O = _toIobject(object);
	  var i = 0;
	  var result = [];
	  var key;
	  for (key in O) if (key != IE_PROTO) _has(O, key) && result.push(key);
	  // Don't enum bug & hidden keys
	  while (names.length > i) if (_has(O, key = names[i++])) {
	    ~arrayIndexOf(result, key) || result.push(key);
	  }
	  return result;
	};

	// IE 8- don't enum bug keys
	var _enumBugKeys = (
	  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
	).split(',');

	// 19.1.2.14 / 15.2.3.14 Object.keys(O)



	var _objectKeys = Object.keys || function keys(O) {
	  return _objectKeysInternal(O, _enumBugKeys);
	};

	var f$2 = Object.getOwnPropertySymbols;

	var _objectGops = {
		f: f$2
	};

	var f$3 = {}.propertyIsEnumerable;

	var _objectPie = {
		f: f$3
	};

	// all enumerable object keys, includes symbols



	var _enumKeys = function (it) {
	  var result = _objectKeys(it);
	  var getSymbols = _objectGops.f;
	  if (getSymbols) {
	    var symbols = getSymbols(it);
	    var isEnum = _objectPie.f;
	    var i = 0;
	    var key;
	    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
	  } return result;
	};

	// 7.2.2 IsArray(argument)

	var _isArray = Array.isArray || function isArray(arg) {
	  return _cof(arg) == 'Array';
	};

	var _objectDps = _descriptors ? Object.defineProperties : function defineProperties(O, Properties) {
	  _anObject(O);
	  var keys = _objectKeys(Properties);
	  var length = keys.length;
	  var i = 0;
	  var P;
	  while (length > i) _objectDp.f(O, P = keys[i++], Properties[P]);
	  return O;
	};

	var document$2 = _global.document;
	var _html = document$2 && document$2.documentElement;

	// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])



	var IE_PROTO$1 = _sharedKey('IE_PROTO');
	var Empty = function () { /* empty */ };
	var PROTOTYPE$1 = 'prototype';

	// Create object with fake `null` prototype: use iframe Object with cleared prototype
	var createDict = function () {
	  // Thrash, waste and sodomy: IE GC bug
	  var iframe = _domCreate('iframe');
	  var i = _enumBugKeys.length;
	  var lt = '<';
	  var gt = '>';
	  var iframeDocument;
	  iframe.style.display = 'none';
	  _html.appendChild(iframe);
	  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
	  // createDict = iframe.contentWindow.Object;
	  // html.removeChild(iframe);
	  iframeDocument = iframe.contentWindow.document;
	  iframeDocument.open();
	  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
	  iframeDocument.close();
	  createDict = iframeDocument.F;
	  while (i--) delete createDict[PROTOTYPE$1][_enumBugKeys[i]];
	  return createDict();
	};

	var _objectCreate = Object.create || function create(O, Properties) {
	  var result;
	  if (O !== null) {
	    Empty[PROTOTYPE$1] = _anObject(O);
	    result = new Empty();
	    Empty[PROTOTYPE$1] = null;
	    // add "__proto__" for Object.getPrototypeOf polyfill
	    result[IE_PROTO$1] = O;
	  } else result = createDict();
	  return Properties === undefined ? result : _objectDps(result, Properties);
	};

	// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)

	var hiddenKeys = _enumBugKeys.concat('length', 'prototype');

	var f$4 = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
	  return _objectKeysInternal(O, hiddenKeys);
	};

	var _objectGopn = {
		f: f$4
	};

	// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window

	var gOPN = _objectGopn.f;
	var toString$1 = {}.toString;

	var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
	  ? Object.getOwnPropertyNames(window) : [];

	var getWindowNames = function (it) {
	  try {
	    return gOPN(it);
	  } catch (e) {
	    return windowNames.slice();
	  }
	};

	var f$5 = function getOwnPropertyNames(it) {
	  return windowNames && toString$1.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(_toIobject(it));
	};

	var _objectGopnExt = {
		f: f$5
	};

	var gOPD = Object.getOwnPropertyDescriptor;

	var f$6 = _descriptors ? gOPD : function getOwnPropertyDescriptor(O, P) {
	  O = _toIobject(O);
	  P = _toPrimitive(P, true);
	  if (_ie8DomDefine) try {
	    return gOPD(O, P);
	  } catch (e) { /* empty */ }
	  if (_has(O, P)) return _propertyDesc(!_objectPie.f.call(O, P), O[P]);
	};

	var _objectGopd = {
		f: f$6
	};

	// ECMAScript 6 symbols shim





	var META = _meta.KEY;



















	var gOPD$1 = _objectGopd.f;
	var dP$1 = _objectDp.f;
	var gOPN$1 = _objectGopnExt.f;
	var $Symbol = _global.Symbol;
	var $JSON = _global.JSON;
	var _stringify = $JSON && $JSON.stringify;
	var PROTOTYPE$2 = 'prototype';
	var HIDDEN = _wks('_hidden');
	var TO_PRIMITIVE = _wks('toPrimitive');
	var isEnum = {}.propertyIsEnumerable;
	var SymbolRegistry = _shared('symbol-registry');
	var AllSymbols = _shared('symbols');
	var OPSymbols = _shared('op-symbols');
	var ObjectProto = Object[PROTOTYPE$2];
	var USE_NATIVE = typeof $Symbol == 'function';
	var QObject = _global.QObject;
	// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
	var setter = !QObject || !QObject[PROTOTYPE$2] || !QObject[PROTOTYPE$2].findChild;

	// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
	var setSymbolDesc = _descriptors && _fails(function () {
	  return _objectCreate(dP$1({}, 'a', {
	    get: function () { return dP$1(this, 'a', { value: 7 }).a; }
	  })).a != 7;
	}) ? function (it, key, D) {
	  var protoDesc = gOPD$1(ObjectProto, key);
	  if (protoDesc) delete ObjectProto[key];
	  dP$1(it, key, D);
	  if (protoDesc && it !== ObjectProto) dP$1(ObjectProto, key, protoDesc);
	} : dP$1;

	var wrap = function (tag) {
	  var sym = AllSymbols[tag] = _objectCreate($Symbol[PROTOTYPE$2]);
	  sym._k = tag;
	  return sym;
	};

	var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
	  return typeof it == 'symbol';
	} : function (it) {
	  return it instanceof $Symbol;
	};

	var $defineProperty = function defineProperty(it, key, D) {
	  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
	  _anObject(it);
	  key = _toPrimitive(key, true);
	  _anObject(D);
	  if (_has(AllSymbols, key)) {
	    if (!D.enumerable) {
	      if (!_has(it, HIDDEN)) dP$1(it, HIDDEN, _propertyDesc(1, {}));
	      it[HIDDEN][key] = true;
	    } else {
	      if (_has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
	      D = _objectCreate(D, { enumerable: _propertyDesc(0, false) });
	    } return setSymbolDesc(it, key, D);
	  } return dP$1(it, key, D);
	};
	var $defineProperties = function defineProperties(it, P) {
	  _anObject(it);
	  var keys = _enumKeys(P = _toIobject(P));
	  var i = 0;
	  var l = keys.length;
	  var key;
	  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
	  return it;
	};
	var $create = function create(it, P) {
	  return P === undefined ? _objectCreate(it) : $defineProperties(_objectCreate(it), P);
	};
	var $propertyIsEnumerable = function propertyIsEnumerable(key) {
	  var E = isEnum.call(this, key = _toPrimitive(key, true));
	  if (this === ObjectProto && _has(AllSymbols, key) && !_has(OPSymbols, key)) return false;
	  return E || !_has(this, key) || !_has(AllSymbols, key) || _has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
	};
	var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
	  it = _toIobject(it);
	  key = _toPrimitive(key, true);
	  if (it === ObjectProto && _has(AllSymbols, key) && !_has(OPSymbols, key)) return;
	  var D = gOPD$1(it, key);
	  if (D && _has(AllSymbols, key) && !(_has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
	  return D;
	};
	var $getOwnPropertyNames = function getOwnPropertyNames(it) {
	  var names = gOPN$1(_toIobject(it));
	  var result = [];
	  var i = 0;
	  var key;
	  while (names.length > i) {
	    if (!_has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
	  } return result;
	};
	var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
	  var IS_OP = it === ObjectProto;
	  var names = gOPN$1(IS_OP ? OPSymbols : _toIobject(it));
	  var result = [];
	  var i = 0;
	  var key;
	  while (names.length > i) {
	    if (_has(AllSymbols, key = names[i++]) && (IS_OP ? _has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
	  } return result;
	};

	// 19.4.1.1 Symbol([description])
	if (!USE_NATIVE) {
	  $Symbol = function Symbol() {
	    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
	    var tag = _uid(arguments.length > 0 ? arguments[0] : undefined);
	    var $set = function (value) {
	      if (this === ObjectProto) $set.call(OPSymbols, value);
	      if (_has(this, HIDDEN) && _has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
	      setSymbolDesc(this, tag, _propertyDesc(1, value));
	    };
	    if (_descriptors && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
	    return wrap(tag);
	  };
	  _redefine($Symbol[PROTOTYPE$2], 'toString', function toString() {
	    return this._k;
	  });

	  _objectGopd.f = $getOwnPropertyDescriptor;
	  _objectDp.f = $defineProperty;
	  _objectGopn.f = _objectGopnExt.f = $getOwnPropertyNames;
	  _objectPie.f = $propertyIsEnumerable;
	  _objectGops.f = $getOwnPropertySymbols;

	  if (_descriptors && !_library) {
	    _redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
	  }

	  _wksExt.f = function (name) {
	    return wrap(_wks(name));
	  };
	}

	_export(_export.G + _export.W + _export.F * !USE_NATIVE, { Symbol: $Symbol });

	for (var es6Symbols = (
	  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
	  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
	).split(','), j = 0; es6Symbols.length > j;)_wks(es6Symbols[j++]);

	for (var wellKnownSymbols = _objectKeys(_wks.store), k = 0; wellKnownSymbols.length > k;) _wksDefine(wellKnownSymbols[k++]);

	_export(_export.S + _export.F * !USE_NATIVE, 'Symbol', {
	  // 19.4.2.1 Symbol.for(key)
	  'for': function (key) {
	    return _has(SymbolRegistry, key += '')
	      ? SymbolRegistry[key]
	      : SymbolRegistry[key] = $Symbol(key);
	  },
	  // 19.4.2.5 Symbol.keyFor(sym)
	  keyFor: function keyFor(sym) {
	    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
	    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
	  },
	  useSetter: function () { setter = true; },
	  useSimple: function () { setter = false; }
	});

	_export(_export.S + _export.F * !USE_NATIVE, 'Object', {
	  // 19.1.2.2 Object.create(O [, Properties])
	  create: $create,
	  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
	  defineProperty: $defineProperty,
	  // 19.1.2.3 Object.defineProperties(O, Properties)
	  defineProperties: $defineProperties,
	  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
	  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
	  // 19.1.2.7 Object.getOwnPropertyNames(O)
	  getOwnPropertyNames: $getOwnPropertyNames,
	  // 19.1.2.8 Object.getOwnPropertySymbols(O)
	  getOwnPropertySymbols: $getOwnPropertySymbols
	});

	// 24.3.2 JSON.stringify(value [, replacer [, space]])
	$JSON && _export(_export.S + _export.F * (!USE_NATIVE || _fails(function () {
	  var S = $Symbol();
	  // MS Edge converts symbol values to JSON as {}
	  // WebKit converts symbol values to JSON as null
	  // V8 throws on boxed symbols
	  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
	})), 'JSON', {
	  stringify: function stringify(it) {
	    var args = [it];
	    var i = 1;
	    var replacer, $replacer;
	    while (arguments.length > i) args.push(arguments[i++]);
	    $replacer = replacer = args[1];
	    if (!_isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
	    if (!_isArray(replacer)) replacer = function (key, value) {
	      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
	      if (!isSymbol(value)) return value;
	    };
	    args[1] = replacer;
	    return _stringify.apply($JSON, args);
	  }
	});

	// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
	$Symbol[PROTOTYPE$2][TO_PRIMITIVE] || _hide($Symbol[PROTOTYPE$2], TO_PRIMITIVE, $Symbol[PROTOTYPE$2].valueOf);
	// 19.4.3.5 Symbol.prototype[@@toStringTag]
	_setToStringTag($Symbol, 'Symbol');
	// 20.2.1.9 Math[@@toStringTag]
	_setToStringTag(Math, 'Math', true);
	// 24.3.3 JSON[@@toStringTag]
	_setToStringTag(_global.JSON, 'JSON', true);

	// getting tag from 19.1.3.6 Object.prototype.toString()

	var TAG$1 = _wks('toStringTag');
	// ES3 wrong here
	var ARG = _cof(function () { return arguments; }()) == 'Arguments';

	// fallback for IE11 Script Access Denied error
	var tryGet = function (it, key) {
	  try {
	    return it[key];
	  } catch (e) { /* empty */ }
	};

	var _classof = function (it) {
	  var O, T, B;
	  return it === undefined ? 'Undefined' : it === null ? 'Null'
	    // @@toStringTag case
	    : typeof (T = tryGet(O = Object(it), TAG$1)) == 'string' ? T
	    // builtinTag case
	    : ARG ? _cof(O)
	    // ES3 arguments fallback
	    : (B = _cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
	};

	// 19.1.3.6 Object.prototype.toString()

	var test = {};
	test[_wks('toStringTag')] = 'z';
	if (test + '' != '[object z]') {
	  _redefine(Object.prototype, 'toString', function toString() {
	    return '[object ' + _classof(this) + ']';
	  }, true);
	}

	var symbol = _core.Symbol;

	var smoothscroll = createCommonjsModule(function (module, exports) {
	/* smoothscroll v0.4.0 - 2018 - Dustan Kasten, Jeremias Menichelli - MIT License */
	(function () {

	  // polyfill
	  function polyfill() {
	    // aliases
	    var w = window;
	    var d = document;

	    // return if scroll behavior is supported and polyfill is not forced
	    if (
	      'scrollBehavior' in d.documentElement.style &&
	      w.__forceSmoothScrollPolyfill__ !== true
	    ) {
	      return;
	    }

	    // globals
	    var Element = w.HTMLElement || w.Element;
	    var SCROLL_TIME = 468;

	    // object gathering original scroll methods
	    var original = {
	      scroll: w.scroll || w.scrollTo,
	      scrollBy: w.scrollBy,
	      elementScroll: Element.prototype.scroll || scrollElement,
	      scrollIntoView: Element.prototype.scrollIntoView
	    };

	    // define timing method
	    var now =
	      w.performance && w.performance.now
	        ? w.performance.now.bind(w.performance)
	        : Date.now;

	    /**
	     * indicates if a the current browser is made by Microsoft
	     * @method isMicrosoftBrowser
	     * @param {String} userAgent
	     * @returns {Boolean}
	     */
	    function isMicrosoftBrowser(userAgent) {
	      var userAgentPatterns = ['MSIE ', 'Trident/', 'Edge/'];

	      return new RegExp(userAgentPatterns.join('|')).test(userAgent);
	    }

	    /*
	     * IE has rounding bug rounding down clientHeight and clientWidth and
	     * rounding up scrollHeight and scrollWidth causing false positives
	     * on hasScrollableSpace
	     */
	    var ROUNDING_TOLERANCE = isMicrosoftBrowser(w.navigator.userAgent) ? 1 : 0;

	    /**
	     * changes scroll position inside an element
	     * @method scrollElement
	     * @param {Number} x
	     * @param {Number} y
	     * @returns {undefined}
	     */
	    function scrollElement(x, y) {
	      this.scrollLeft = x;
	      this.scrollTop = y;
	    }

	    /**
	     * returns result of applying ease math function to a number
	     * @method ease
	     * @param {Number} k
	     * @returns {Number}
	     */
	    function ease(k) {
	      return 0.5 * (1 - Math.cos(Math.PI * k));
	    }

	    /**
	     * indicates if a smooth behavior should be applied
	     * @method shouldBailOut
	     * @param {Number|Object} firstArg
	     * @returns {Boolean}
	     */
	    function shouldBailOut(firstArg) {
	      if (
	        firstArg === null ||
	        typeof firstArg !== 'object' ||
	        firstArg.behavior === undefined ||
	        firstArg.behavior === 'auto' ||
	        firstArg.behavior === 'instant'
	      ) {
	        // first argument is not an object/null
	        // or behavior is auto, instant or undefined
	        return true;
	      }

	      if (typeof firstArg === 'object' && firstArg.behavior === 'smooth') {
	        // first argument is an object and behavior is smooth
	        return false;
	      }

	      // throw error when behavior is not supported
	      throw new TypeError(
	        'behavior member of ScrollOptions ' +
	          firstArg.behavior +
	          ' is not a valid value for enumeration ScrollBehavior.'
	      );
	    }

	    /**
	     * indicates if an element has scrollable space in the provided axis
	     * @method hasScrollableSpace
	     * @param {Node} el
	     * @param {String} axis
	     * @returns {Boolean}
	     */
	    function hasScrollableSpace(el, axis) {
	      if (axis === 'Y') {
	        return el.clientHeight + ROUNDING_TOLERANCE < el.scrollHeight;
	      }

	      if (axis === 'X') {
	        return el.clientWidth + ROUNDING_TOLERANCE < el.scrollWidth;
	      }
	    }

	    /**
	     * indicates if an element has a scrollable overflow property in the axis
	     * @method canOverflow
	     * @param {Node} el
	     * @param {String} axis
	     * @returns {Boolean}
	     */
	    function canOverflow(el, axis) {
	      var overflowValue = w.getComputedStyle(el, null)['overflow' + axis];

	      return overflowValue === 'auto' || overflowValue === 'scroll';
	    }

	    /**
	     * indicates if an element can be scrolled in either axis
	     * @method isScrollable
	     * @param {Node} el
	     * @param {String} axis
	     * @returns {Boolean}
	     */
	    function isScrollable(el) {
	      var isScrollableY = hasScrollableSpace(el, 'Y') && canOverflow(el, 'Y');
	      var isScrollableX = hasScrollableSpace(el, 'X') && canOverflow(el, 'X');

	      return isScrollableY || isScrollableX;
	    }

	    /**
	     * finds scrollable parent of an element
	     * @method findScrollableParent
	     * @param {Node} el
	     * @returns {Node} el
	     */
	    function findScrollableParent(el) {
	      var isBody;

	      do {
	        el = el.parentNode;

	        isBody = el === d.body;
	      } while (isBody === false && isScrollable(el) === false);

	      isBody = null;

	      return el;
	    }

	    /**
	     * self invoked function that, given a context, steps through scrolling
	     * @method step
	     * @param {Object} context
	     * @returns {undefined}
	     */
	    function step(context) {
	      var time = now();
	      var value;
	      var currentX;
	      var currentY;
	      var elapsed = (time - context.startTime) / SCROLL_TIME;

	      // avoid elapsed times higher than one
	      elapsed = elapsed > 1 ? 1 : elapsed;

	      // apply easing to elapsed time
	      value = ease(elapsed);

	      currentX = context.startX + (context.x - context.startX) * value;
	      currentY = context.startY + (context.y - context.startY) * value;

	      context.method.call(context.scrollable, currentX, currentY);

	      // scroll more if we have not reached our destination
	      if (currentX !== context.x || currentY !== context.y) {
	        w.requestAnimationFrame(step.bind(w, context));
	      }
	    }

	    /**
	     * scrolls window or element with a smooth behavior
	     * @method smoothScroll
	     * @param {Object|Node} el
	     * @param {Number} x
	     * @param {Number} y
	     * @returns {undefined}
	     */
	    function smoothScroll(el, x, y) {
	      var scrollable;
	      var startX;
	      var startY;
	      var method;
	      var startTime = now();

	      // define scroll context
	      if (el === d.body) {
	        scrollable = w;
	        startX = w.scrollX || w.pageXOffset;
	        startY = w.scrollY || w.pageYOffset;
	        method = original.scroll;
	      } else {
	        scrollable = el;
	        startX = el.scrollLeft;
	        startY = el.scrollTop;
	        method = scrollElement;
	      }

	      // scroll looping over a frame
	      step({
	        scrollable: scrollable,
	        method: method,
	        startTime: startTime,
	        startX: startX,
	        startY: startY,
	        x: x,
	        y: y
	      });
	    }

	    // ORIGINAL METHODS OVERRIDES
	    // w.scroll and w.scrollTo
	    w.scroll = w.scrollTo = function() {
	      // avoid action when no arguments are passed
	      if (arguments[0] === undefined) {
	        return;
	      }

	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0]) === true) {
	        original.scroll.call(
	          w,
	          arguments[0].left !== undefined
	            ? arguments[0].left
	            : typeof arguments[0] !== 'object'
	              ? arguments[0]
	              : w.scrollX || w.pageXOffset,
	          // use top prop, second argument if present or fallback to scrollY
	          arguments[0].top !== undefined
	            ? arguments[0].top
	            : arguments[1] !== undefined
	              ? arguments[1]
	              : w.scrollY || w.pageYOffset
	        );

	        return;
	      }

	      // LET THE SMOOTHNESS BEGIN!
	      smoothScroll.call(
	        w,
	        d.body,
	        arguments[0].left !== undefined
	          ? ~~arguments[0].left
	          : w.scrollX || w.pageXOffset,
	        arguments[0].top !== undefined
	          ? ~~arguments[0].top
	          : w.scrollY || w.pageYOffset
	      );
	    };

	    // w.scrollBy
	    w.scrollBy = function() {
	      // avoid action when no arguments are passed
	      if (arguments[0] === undefined) {
	        return;
	      }

	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0])) {
	        original.scrollBy.call(
	          w,
	          arguments[0].left !== undefined
	            ? arguments[0].left
	            : typeof arguments[0] !== 'object' ? arguments[0] : 0,
	          arguments[0].top !== undefined
	            ? arguments[0].top
	            : arguments[1] !== undefined ? arguments[1] : 0
	        );

	        return;
	      }

	      // LET THE SMOOTHNESS BEGIN!
	      smoothScroll.call(
	        w,
	        d.body,
	        ~~arguments[0].left + (w.scrollX || w.pageXOffset),
	        ~~arguments[0].top + (w.scrollY || w.pageYOffset)
	      );
	    };

	    // Element.prototype.scroll and Element.prototype.scrollTo
	    Element.prototype.scroll = Element.prototype.scrollTo = function() {
	      // avoid action when no arguments are passed
	      if (arguments[0] === undefined) {
	        return;
	      }

	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0]) === true) {
	        // if one number is passed, throw error to match Firefox implementation
	        if (typeof arguments[0] === 'number' && arguments[1] === undefined) {
	          throw new SyntaxError('Value could not be converted');
	        }

	        original.elementScroll.call(
	          this,
	          // use left prop, first number argument or fallback to scrollLeft
	          arguments[0].left !== undefined
	            ? ~~arguments[0].left
	            : typeof arguments[0] !== 'object' ? ~~arguments[0] : this.scrollLeft,
	          // use top prop, second argument or fallback to scrollTop
	          arguments[0].top !== undefined
	            ? ~~arguments[0].top
	            : arguments[1] !== undefined ? ~~arguments[1] : this.scrollTop
	        );

	        return;
	      }

	      var left = arguments[0].left;
	      var top = arguments[0].top;

	      // LET THE SMOOTHNESS BEGIN!
	      smoothScroll.call(
	        this,
	        this,
	        typeof left === 'undefined' ? this.scrollLeft : ~~left,
	        typeof top === 'undefined' ? this.scrollTop : ~~top
	      );
	    };

	    // Element.prototype.scrollBy
	    Element.prototype.scrollBy = function() {
	      // avoid action when no arguments are passed
	      if (arguments[0] === undefined) {
	        return;
	      }

	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0]) === true) {
	        original.elementScroll.call(
	          this,
	          arguments[0].left !== undefined
	            ? ~~arguments[0].left + this.scrollLeft
	            : ~~arguments[0] + this.scrollLeft,
	          arguments[0].top !== undefined
	            ? ~~arguments[0].top + this.scrollTop
	            : ~~arguments[1] + this.scrollTop
	        );

	        return;
	      }

	      this.scroll({
	        left: ~~arguments[0].left + this.scrollLeft,
	        top: ~~arguments[0].top + this.scrollTop,
	        behavior: arguments[0].behavior
	      });
	    };

	    // Element.prototype.scrollIntoView
	    Element.prototype.scrollIntoView = function() {
	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0]) === true) {
	        original.scrollIntoView.call(
	          this,
	          arguments[0] === undefined ? true : arguments[0]
	        );

	        return;
	      }

	      // LET THE SMOOTHNESS BEGIN!
	      var scrollableParent = findScrollableParent(this);
	      var parentRects = scrollableParent.getBoundingClientRect();
	      var clientRects = this.getBoundingClientRect();

	      if (scrollableParent !== d.body) {
	        // reveal element inside parent
	        smoothScroll.call(
	          this,
	          scrollableParent,
	          scrollableParent.scrollLeft + clientRects.left - parentRects.left,
	          scrollableParent.scrollTop + clientRects.top - parentRects.top
	        );

	        // reveal parent in viewport unless is fixed
	        if (w.getComputedStyle(scrollableParent).position !== 'fixed') {
	          w.scrollBy({
	            left: parentRects.left,
	            top: parentRects.top,
	            behavior: 'smooth'
	          });
	        }
	      } else {
	        // reveal element in viewport
	        w.scrollBy({
	          left: clientRects.left,
	          top: clientRects.top,
	          behavior: 'smooth'
	        });
	      }
	    };
	  }

	  {
	    // commonjs
	    module.exports = { polyfill: polyfill };
	  }

	}());
	});
	var smoothscroll_1 = smoothscroll.polyfill;

	if (!Element.prototype.matches) Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;

	if (!Element.prototype.closest) Element.prototype.closest = function (s) {
	    var el = this;
	    if (!document.documentElement.contains(el)) return null;
	    do {
	        if (el.matches(s)) return el;
	        el = el.parentElement || el.parentNode;
	    } while (el !== null && el.nodeType === 1);
	    return null;
	};

	if (typeof Object.assign != 'function') {
	  // Must be writable: true, enumerable: false, configurable: true
	  Object.defineProperty(Object, "assign", {
	    value: function assign(target, varArgs) {

	      if (target == null) {
	        // TypeError if undefined or null
	        throw new TypeError('Cannot convert undefined or null to object');
	      }

	      var to = Object(target);

	      for (var index = 1; index < arguments.length; index++) {
	        var nextSource = arguments[index];

	        if (nextSource != null) {
	          // Skip over if undefined or null
	          for (var nextKey in nextSource) {
	            // Avoid bugs when hasOwnProperty is shadowed
	            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
	              to[nextKey] = nextSource[nextKey];
	            }
	          }
	        }
	      }
	      return to;
	    },
	    writable: true,
	    configurable: true
	  });
	}

	function promiseFinally(callback) {
	  var constructor = this.constructor;
	  return this.then(
	    function(value) {
	      return constructor.resolve(callback()).then(function() {
	        return value;
	      });
	    },
	    function(reason) {
	      return constructor.resolve(callback()).then(function() {
	        return constructor.reject(reason);
	      });
	    }
	  );
	}

	// Store setTimeout reference so promise-polyfill will be unaffected by
	// other code modifying setTimeout (like sinon.useFakeTimers())
	var setTimeoutFunc = setTimeout;

	function noop() {}

	// Polyfill for Function.prototype.bind
	function bind(fn, thisArg) {
	  return function() {
	    fn.apply(thisArg, arguments);
	  };
	}

	function Promise$1(fn) {
	  if (!(this instanceof Promise$1))
	    throw new TypeError('Promises must be constructed via new');
	  if (typeof fn !== 'function') throw new TypeError('not a function');
	  this._state = 0;
	  this._handled = false;
	  this._value = undefined;
	  this._deferreds = [];

	  doResolve(fn, this);
	}

	function handle(self, deferred) {
	  while (self._state === 3) {
	    self = self._value;
	  }
	  if (self._state === 0) {
	    self._deferreds.push(deferred);
	    return;
	  }
	  self._handled = true;
	  Promise$1._immediateFn(function() {
	    var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
	    if (cb === null) {
	      (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
	      return;
	    }
	    var ret;
	    try {
	      ret = cb(self._value);
	    } catch (e) {
	      reject(deferred.promise, e);
	      return;
	    }
	    resolve(deferred.promise, ret);
	  });
	}

	function resolve(self, newValue) {
	  try {
	    // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
	    if (newValue === self)
	      throw new TypeError('A promise cannot be resolved with itself.');
	    if (
	      newValue &&
	      (typeof newValue === 'object' || typeof newValue === 'function')
	    ) {
	      var then = newValue.then;
	      if (newValue instanceof Promise$1) {
	        self._state = 3;
	        self._value = newValue;
	        finale(self);
	        return;
	      } else if (typeof then === 'function') {
	        doResolve(bind(then, newValue), self);
	        return;
	      }
	    }
	    self._state = 1;
	    self._value = newValue;
	    finale(self);
	  } catch (e) {
	    reject(self, e);
	  }
	}

	function reject(self, newValue) {
	  self._state = 2;
	  self._value = newValue;
	  finale(self);
	}

	function finale(self) {
	  if (self._state === 2 && self._deferreds.length === 0) {
	    Promise$1._immediateFn(function() {
	      if (!self._handled) {
	        Promise$1._unhandledRejectionFn(self._value);
	      }
	    });
	  }

	  for (var i = 0, len = self._deferreds.length; i < len; i++) {
	    handle(self, self._deferreds[i]);
	  }
	  self._deferreds = null;
	}

	function Handler(onFulfilled, onRejected, promise) {
	  this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
	  this.onRejected = typeof onRejected === 'function' ? onRejected : null;
	  this.promise = promise;
	}

	/**
	 * Take a potentially misbehaving resolver function and make sure
	 * onFulfilled and onRejected are only called once.
	 *
	 * Makes no guarantees about asynchrony.
	 */
	function doResolve(fn, self) {
	  var done = false;
	  try {
	    fn(
	      function(value) {
	        if (done) return;
	        done = true;
	        resolve(self, value);
	      },
	      function(reason) {
	        if (done) return;
	        done = true;
	        reject(self, reason);
	      }
	    );
	  } catch (ex) {
	    if (done) return;
	    done = true;
	    reject(self, ex);
	  }
	}

	Promise$1.prototype['catch'] = function(onRejected) {
	  return this.then(null, onRejected);
	};

	Promise$1.prototype.then = function(onFulfilled, onRejected) {
	  var prom = new this.constructor(noop);

	  handle(this, new Handler(onFulfilled, onRejected, prom));
	  return prom;
	};

	Promise$1.prototype['finally'] = promiseFinally;

	Promise$1.all = function(arr) {
	  return new Promise$1(function(resolve, reject) {
	    if (!arr || typeof arr.length === 'undefined')
	      throw new TypeError('Promise.all accepts an array');
	    var args = Array.prototype.slice.call(arr);
	    if (args.length === 0) return resolve([]);
	    var remaining = args.length;

	    function res(i, val) {
	      try {
	        if (val && (typeof val === 'object' || typeof val === 'function')) {
	          var then = val.then;
	          if (typeof then === 'function') {
	            then.call(
	              val,
	              function(val) {
	                res(i, val);
	              },
	              reject
	            );
	            return;
	          }
	        }
	        args[i] = val;
	        if (--remaining === 0) {
	          resolve(args);
	        }
	      } catch (ex) {
	        reject(ex);
	      }
	    }

	    for (var i = 0; i < args.length; i++) {
	      res(i, args[i]);
	    }
	  });
	};

	Promise$1.resolve = function(value) {
	  if (value && typeof value === 'object' && value.constructor === Promise$1) {
	    return value;
	  }

	  return new Promise$1(function(resolve) {
	    resolve(value);
	  });
	};

	Promise$1.reject = function(value) {
	  return new Promise$1(function(resolve, reject) {
	    reject(value);
	  });
	};

	Promise$1.race = function(values) {
	  return new Promise$1(function(resolve, reject) {
	    for (var i = 0, len = values.length; i < len; i++) {
	      values[i].then(resolve, reject);
	    }
	  });
	};

	// Use polyfill for setImmediate for performance gains
	Promise$1._immediateFn =
	  (typeof setImmediate === 'function' &&
	    function(fn) {
	      setImmediate(fn);
	    }) ||
	  function(fn) {
	    setTimeoutFunc(fn, 0);
	  };

	Promise$1._unhandledRejectionFn = function _unhandledRejectionFn(err) {
	  if (typeof console !== 'undefined' && console) {
	    console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
	  }
	};

	var globalNS = (function() {
	  // the only reliable means to get the global object is
	  // `Function('return this')()`
	  // However, this causes CSP violations in Chrome apps.
	  if (typeof self !== 'undefined') {
	    return self;
	  }
	  if (typeof window !== 'undefined') {
	    return window;
	  }
	  if (typeof global !== 'undefined') {
	    return global;
	  }
	  throw new Error('unable to locate global object');
	})();

	if (!globalNS.Promise) {
	  globalNS.Promise = Promise$1;
	} else if (!globalNS.Promise.prototype['finally']) {
	  globalNS.Promise.prototype['finally'] = promiseFinally;
	}

	(function(self) {

	  if (self.fetch) {
	    return
	  }

	  var support = {
	    searchParams: 'URLSearchParams' in self,
	    iterable: 'Symbol' in self && 'iterator' in Symbol,
	    blob: 'FileReader' in self && 'Blob' in self && (function() {
	      try {
	        new Blob();
	        return true
	      } catch(e) {
	        return false
	      }
	    })(),
	    formData: 'FormData' in self,
	    arrayBuffer: 'ArrayBuffer' in self
	  };

	  if (support.arrayBuffer) {
	    var viewClasses = [
	      '[object Int8Array]',
	      '[object Uint8Array]',
	      '[object Uint8ClampedArray]',
	      '[object Int16Array]',
	      '[object Uint16Array]',
	      '[object Int32Array]',
	      '[object Uint32Array]',
	      '[object Float32Array]',
	      '[object Float64Array]'
	    ];

	    var isDataView = function(obj) {
	      return obj && DataView.prototype.isPrototypeOf(obj)
	    };

	    var isArrayBufferView = ArrayBuffer.isView || function(obj) {
	      return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1
	    };
	  }

	  function normalizeName(name) {
	    if (typeof name !== 'string') {
	      name = String(name);
	    }
	    if (/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(name)) {
	      throw new TypeError('Invalid character in header field name')
	    }
	    return name.toLowerCase()
	  }

	  function normalizeValue(value) {
	    if (typeof value !== 'string') {
	      value = String(value);
	    }
	    return value
	  }

	  // Build a destructive iterator for the value list
	  function iteratorFor(items) {
	    var iterator = {
	      next: function() {
	        var value = items.shift();
	        return {done: value === undefined, value: value}
	      }
	    };

	    if (support.iterable) {
	      iterator[Symbol.iterator] = function() {
	        return iterator
	      };
	    }

	    return iterator
	  }

	  function Headers(headers) {
	    this.map = {};

	    if (headers instanceof Headers) {
	      headers.forEach(function(value, name) {
	        this.append(name, value);
	      }, this);
	    } else if (Array.isArray(headers)) {
	      headers.forEach(function(header) {
	        this.append(header[0], header[1]);
	      }, this);
	    } else if (headers) {
	      Object.getOwnPropertyNames(headers).forEach(function(name) {
	        this.append(name, headers[name]);
	      }, this);
	    }
	  }

	  Headers.prototype.append = function(name, value) {
	    name = normalizeName(name);
	    value = normalizeValue(value);
	    var oldValue = this.map[name];
	    this.map[name] = oldValue ? oldValue+','+value : value;
	  };

	  Headers.prototype['delete'] = function(name) {
	    delete this.map[normalizeName(name)];
	  };

	  Headers.prototype.get = function(name) {
	    name = normalizeName(name);
	    return this.has(name) ? this.map[name] : null
	  };

	  Headers.prototype.has = function(name) {
	    return this.map.hasOwnProperty(normalizeName(name))
	  };

	  Headers.prototype.set = function(name, value) {
	    this.map[normalizeName(name)] = normalizeValue(value);
	  };

	  Headers.prototype.forEach = function(callback, thisArg) {
	    for (var name in this.map) {
	      if (this.map.hasOwnProperty(name)) {
	        callback.call(thisArg, this.map[name], name, this);
	      }
	    }
	  };

	  Headers.prototype.keys = function() {
	    var items = [];
	    this.forEach(function(value, name) { items.push(name); });
	    return iteratorFor(items)
	  };

	  Headers.prototype.values = function() {
	    var items = [];
	    this.forEach(function(value) { items.push(value); });
	    return iteratorFor(items)
	  };

	  Headers.prototype.entries = function() {
	    var items = [];
	    this.forEach(function(value, name) { items.push([name, value]); });
	    return iteratorFor(items)
	  };

	  if (support.iterable) {
	    Headers.prototype[Symbol.iterator] = Headers.prototype.entries;
	  }

	  function consumed(body) {
	    if (body.bodyUsed) {
	      return Promise.reject(new TypeError('Already read'))
	    }
	    body.bodyUsed = true;
	  }

	  function fileReaderReady(reader) {
	    return new Promise(function(resolve, reject) {
	      reader.onload = function() {
	        resolve(reader.result);
	      };
	      reader.onerror = function() {
	        reject(reader.error);
	      };
	    })
	  }

	  function readBlobAsArrayBuffer(blob) {
	    var reader = new FileReader();
	    var promise = fileReaderReady(reader);
	    reader.readAsArrayBuffer(blob);
	    return promise
	  }

	  function readBlobAsText(blob) {
	    var reader = new FileReader();
	    var promise = fileReaderReady(reader);
	    reader.readAsText(blob);
	    return promise
	  }

	  function readArrayBufferAsText(buf) {
	    var view = new Uint8Array(buf);
	    var chars = new Array(view.length);

	    for (var i = 0; i < view.length; i++) {
	      chars[i] = String.fromCharCode(view[i]);
	    }
	    return chars.join('')
	  }

	  function bufferClone(buf) {
	    if (buf.slice) {
	      return buf.slice(0)
	    } else {
	      var view = new Uint8Array(buf.byteLength);
	      view.set(new Uint8Array(buf));
	      return view.buffer
	    }
	  }

	  function Body() {
	    this.bodyUsed = false;

	    this._initBody = function(body) {
	      this._bodyInit = body;
	      if (!body) {
	        this._bodyText = '';
	      } else if (typeof body === 'string') {
	        this._bodyText = body;
	      } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
	        this._bodyBlob = body;
	      } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
	        this._bodyFormData = body;
	      } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
	        this._bodyText = body.toString();
	      } else if (support.arrayBuffer && support.blob && isDataView(body)) {
	        this._bodyArrayBuffer = bufferClone(body.buffer);
	        // IE 10-11 can't handle a DataView body.
	        this._bodyInit = new Blob([this._bodyArrayBuffer]);
	      } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
	        this._bodyArrayBuffer = bufferClone(body);
	      } else {
	        throw new Error('unsupported BodyInit type')
	      }

	      if (!this.headers.get('content-type')) {
	        if (typeof body === 'string') {
	          this.headers.set('content-type', 'text/plain;charset=UTF-8');
	        } else if (this._bodyBlob && this._bodyBlob.type) {
	          this.headers.set('content-type', this._bodyBlob.type);
	        } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
	          this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
	        }
	      }
	    };

	    if (support.blob) {
	      this.blob = function() {
	        var rejected = consumed(this);
	        if (rejected) {
	          return rejected
	        }

	        if (this._bodyBlob) {
	          return Promise.resolve(this._bodyBlob)
	        } else if (this._bodyArrayBuffer) {
	          return Promise.resolve(new Blob([this._bodyArrayBuffer]))
	        } else if (this._bodyFormData) {
	          throw new Error('could not read FormData body as blob')
	        } else {
	          return Promise.resolve(new Blob([this._bodyText]))
	        }
	      };

	      this.arrayBuffer = function() {
	        if (this._bodyArrayBuffer) {
	          return consumed(this) || Promise.resolve(this._bodyArrayBuffer)
	        } else {
	          return this.blob().then(readBlobAsArrayBuffer)
	        }
	      };
	    }

	    this.text = function() {
	      var rejected = consumed(this);
	      if (rejected) {
	        return rejected
	      }

	      if (this._bodyBlob) {
	        return readBlobAsText(this._bodyBlob)
	      } else if (this._bodyArrayBuffer) {
	        return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer))
	      } else if (this._bodyFormData) {
	        throw new Error('could not read FormData body as text')
	      } else {
	        return Promise.resolve(this._bodyText)
	      }
	    };

	    if (support.formData) {
	      this.formData = function() {
	        return this.text().then(decode)
	      };
	    }

	    this.json = function() {
	      return this.text().then(JSON.parse)
	    };

	    return this
	  }

	  // HTTP methods whose capitalization should be normalized
	  var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT'];

	  function normalizeMethod(method) {
	    var upcased = method.toUpperCase();
	    return (methods.indexOf(upcased) > -1) ? upcased : method
	  }

	  function Request(input, options) {
	    options = options || {};
	    var body = options.body;

	    if (input instanceof Request) {
	      if (input.bodyUsed) {
	        throw new TypeError('Already read')
	      }
	      this.url = input.url;
	      this.credentials = input.credentials;
	      if (!options.headers) {
	        this.headers = new Headers(input.headers);
	      }
	      this.method = input.method;
	      this.mode = input.mode;
	      if (!body && input._bodyInit != null) {
	        body = input._bodyInit;
	        input.bodyUsed = true;
	      }
	    } else {
	      this.url = String(input);
	    }

	    this.credentials = options.credentials || this.credentials || 'omit';
	    if (options.headers || !this.headers) {
	      this.headers = new Headers(options.headers);
	    }
	    this.method = normalizeMethod(options.method || this.method || 'GET');
	    this.mode = options.mode || this.mode || null;
	    this.referrer = null;

	    if ((this.method === 'GET' || this.method === 'HEAD') && body) {
	      throw new TypeError('Body not allowed for GET or HEAD requests')
	    }
	    this._initBody(body);
	  }

	  Request.prototype.clone = function() {
	    return new Request(this, { body: this._bodyInit })
	  };

	  function decode(body) {
	    var form = new FormData();
	    body.trim().split('&').forEach(function(bytes) {
	      if (bytes) {
	        var split = bytes.split('=');
	        var name = split.shift().replace(/\+/g, ' ');
	        var value = split.join('=').replace(/\+/g, ' ');
	        form.append(decodeURIComponent(name), decodeURIComponent(value));
	      }
	    });
	    return form
	  }

	  function parseHeaders(rawHeaders) {
	    var headers = new Headers();
	    // Replace instances of \r\n and \n followed by at least one space or horizontal tab with a space
	    // https://tools.ietf.org/html/rfc7230#section-3.2
	    var preProcessedHeaders = rawHeaders.replace(/\r?\n[\t ]+/g, ' ');
	    preProcessedHeaders.split(/\r?\n/).forEach(function(line) {
	      var parts = line.split(':');
	      var key = parts.shift().trim();
	      if (key) {
	        var value = parts.join(':').trim();
	        headers.append(key, value);
	      }
	    });
	    return headers
	  }

	  Body.call(Request.prototype);

	  function Response(bodyInit, options) {
	    if (!options) {
	      options = {};
	    }

	    this.type = 'default';
	    this.status = options.status === undefined ? 200 : options.status;
	    this.ok = this.status >= 200 && this.status < 300;
	    this.statusText = 'statusText' in options ? options.statusText : 'OK';
	    this.headers = new Headers(options.headers);
	    this.url = options.url || '';
	    this._initBody(bodyInit);
	  }

	  Body.call(Response.prototype);

	  Response.prototype.clone = function() {
	    return new Response(this._bodyInit, {
	      status: this.status,
	      statusText: this.statusText,
	      headers: new Headers(this.headers),
	      url: this.url
	    })
	  };

	  Response.error = function() {
	    var response = new Response(null, {status: 0, statusText: ''});
	    response.type = 'error';
	    return response
	  };

	  var redirectStatuses = [301, 302, 303, 307, 308];

	  Response.redirect = function(url, status) {
	    if (redirectStatuses.indexOf(status) === -1) {
	      throw new RangeError('Invalid status code')
	    }

	    return new Response(null, {status: status, headers: {location: url}})
	  };

	  self.Headers = Headers;
	  self.Request = Request;
	  self.Response = Response;

	  self.fetch = function(input, init) {
	    return new Promise(function(resolve, reject) {
	      var request = new Request(input, init);
	      var xhr = new XMLHttpRequest();

	      xhr.onload = function() {
	        var options = {
	          status: xhr.status,
	          statusText: xhr.statusText,
	          headers: parseHeaders(xhr.getAllResponseHeaders() || '')
	        };
	        options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL');
	        var body = 'response' in xhr ? xhr.response : xhr.responseText;
	        resolve(new Response(body, options));
	      };

	      xhr.onerror = function() {
	        reject(new TypeError('Network request failed'));
	      };

	      xhr.ontimeout = function() {
	        reject(new TypeError('Network request failed'));
	      };

	      xhr.open(request.method, request.url, true);

	      if (request.credentials === 'include') {
	        xhr.withCredentials = true;
	      } else if (request.credentials === 'omit') {
	        xhr.withCredentials = false;
	      }

	      if ('responseType' in xhr && support.blob) {
	        xhr.responseType = 'blob';
	      }

	      request.headers.forEach(function(value, name) {
	        xhr.setRequestHeader(name, value);
	      });

	      xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit);
	    })
	  };
	  self.fetch.polyfill = true;
	})(typeof self !== 'undefined' ? self : undefined);

	(function(){var k,l="function"==typeof Object.defineProperties?Object.defineProperty:function(a,b,d){a!=Array.prototype&&a!=Object.prototype&&(a[b]=d.value);},m="undefined"!=typeof window&&window===this?this:"undefined"!=typeof commonjsGlobal&&null!=commonjsGlobal?commonjsGlobal:this;function n(){n=function(){};m.Symbol||(m.Symbol=p);}var p=function(){var a=0;return function(b){return "jscomp_symbol_"+(b||"")+a++}}();
	function r(){n();var a=m.Symbol.iterator;a||(a=m.Symbol.iterator=m.Symbol("iterator"));"function"!=typeof Array.prototype[a]&&l(Array.prototype,a,{configurable:!0,writable:!0,value:function(){return u(this)}});r=function(){};}function u(a){var b=0;return v(function(){return b<a.length?{done:!1,value:a[b++]}:{done:!0}})}function v(a){r();a={next:a};a[m.Symbol.iterator]=function(){return this};return a}function w(a){r();n();r();var b=a[Symbol.iterator];return b?b.call(a):u(a)}var x;
	if("function"==typeof Object.setPrototypeOf)x=Object.setPrototypeOf;else{var z;a:{var A={o:!0},B={};try{B.__proto__=A;z=B.o;break a}catch(a){}z=!1;}x=z?function(a,b){a.__proto__=b;if(a.__proto__!==b)throw new TypeError(a+" is not extensible");return a}:null;}var C=x;function D(){this.g=!1;this.c=null;this.m=void 0;this.b=1;this.l=this.s=0;this.f=null;}function E(a){if(a.g)throw new TypeError("Generator is already running");a.g=!0;}D.prototype.h=function(a){this.m=a;};
	D.prototype.i=function(a){this.f={u:a,v:!0};this.b=this.s||this.l;};D.prototype["return"]=function(a){this.f={"return":a};this.b=this.l;};function F(a,b,d){a.b=d;return {value:b}}function G(a){this.w=a;this.j=[];for(var b in a)this.j.push(b);this.j.reverse();}function H(a){this.a=new D;this.A=a;}H.prototype.h=function(a){E(this.a);if(this.a.c)return I(this,this.a.c.next,a,this.a.h);this.a.h(a);return J(this)};
	function K(a,b){E(a.a);var d=a.a.c;if(d)return I(a,"return"in d?d["return"]:function(a){return {value:a,done:!0}},b,a.a["return"]);a.a["return"](b);return J(a)}H.prototype.i=function(a){E(this.a);if(this.a.c)return I(this,this.a.c["throw"],a,this.a.h);this.a.i(a);return J(this)};
	function I(a,b,d,c){try{var e=b.call(a.a.c,d);if(!(e instanceof Object))throw new TypeError("Iterator result "+e+" is not an object");if(!e.done)return a.a.g=!1,e;var f=e.value;}catch(g){return a.a.c=null,a.a.i(g),J(a)}a.a.c=null;c.call(a.a,f);return J(a)}function J(a){for(;a.a.b;)try{var b=a.A(a.a);if(b)return a.a.g=!1,{value:b.value,done:!1}}catch(d){a.a.m=void 0,a.a.i(d);}a.a.g=!1;if(a.a.f){b=a.a.f;a.a.f=null;if(b.v)throw b.u;return {value:b["return"],done:!0}}return {value:void 0,done:!0}}
	function L(a){this.next=function(b){return a.h(b)};this["throw"]=function(b){return a.i(b)};this["return"]=function(b){return K(a,b)};r();this[Symbol.iterator]=function(){return this};}function M(a,b){var d=new L(new H(b));C&&C(d,a.prototype);return d}
	if("undefined"===typeof FormData||!FormData.prototype.keys){var N=function(a,b,d){if(2>arguments.length)throw new TypeError("2 arguments required, but only "+arguments.length+" present.");return b instanceof Blob?[a+"",b,void 0!==d?d+"":"string"===typeof b.name?b.name:"blob"]:[a+"",b+""]},O=function(a){if(!arguments.length)throw new TypeError("1 argument required, but only 0 present.");return [a+""]},P=function(a){var b=w(a);a=b.next().value;b=b.next().value;a instanceof Blob&&(a=new File([a],b,{type:a.type,
	lastModified:a.lastModified}));return a},Q="object"===typeof window?window:"object"===typeof self?self:this,R=Q.FormData,S=Q.XMLHttpRequest&&Q.XMLHttpRequest.prototype.send,T=Q.Request&&Q.fetch;n();var U=Q.Symbol&&Symbol.toStringTag,V=new WeakMap,W=Array.from||function(a){return [].slice.call(a)};U&&(Blob.prototype[U]||(Blob.prototype[U]="Blob"),"File"in Q&&!File.prototype[U]&&(File.prototype[U]="File"));try{new File([],"");}catch(a){Q.File=function(b,d,c){b=new Blob(b,c);c=c&&void 0!==c.lastModified?
	new Date(c.lastModified):new Date;Object.defineProperties(b,{name:{value:d},lastModifiedDate:{value:c},lastModified:{value:+c},toString:{value:function(){return "[object File]"}}});U&&Object.defineProperty(b,U,{value:"File"});return b};}var X=function(a){V.set(this,Object.create(null));if(!a)return this;a=w(W(a.elements));for(var b=a.next();!b.done;b=a.next())if(b=b.value,b.name&&!b.disabled)if("file"===b.type)for(var d=w(b.files),c=d.next();!c.done;c=d.next())this.append(b.name,c.value);else if("select-multiple"===
	b.type||"select-one"===b.type)for(d=w(W(b.options)),c=d.next();!c.done;c=d.next())c=c.value,!c.disabled&&c.selected&&this.append(b.name,c.value);else"checkbox"===b.type||"radio"===b.type?b.checked&&this.append(b.name,b.value):this.append(b.name,b.value);};k=X.prototype;k.append=function(a,b,d){var c=V.get(this);c[a]||(c[a]=[]);c[a].push([b,d]);};k["delete"]=function(a){delete V.get(this)[a];};k.entries=function b(){var d=this,c,e,f,g,h,q;return M(b,function(b){switch(b.b){case 1:c=V.get(d),f=new G(c);
	case 2:var t;a:{for(t=f;0<t.j.length;){var y=t.j.pop();if(y in t.w){t=y;break a}}t=null;}if(null==(e=t)){b.b=0;break}g=w(c[e]);h=g.next();case 5:if(h.done){b.b=2;break}q=h.value;return F(b,[e,P(q)],6);case 6:h=g.next(),b.b=5;}})};k.forEach=function(b,d){for(var c=w(this),e=c.next();!e.done;e=c.next()){var f=w(e.value);e=f.next().value;f=f.next().value;b.call(d,f,e,this);}};k.get=function(b){var d=V.get(this);return d[b]?P(d[b][0]):null};k.getAll=function(b){return (V.get(this)[b]||[]).map(P)};k.has=function(b){return b in
	V.get(this)};k.keys=function d(){var c=this,e,f,g,h,q;return M(d,function(d){1==d.b&&(e=w(c),f=e.next());if(3!=d.b){if(f.done){d.b=0;return}g=f.value;h=w(g);q=h.next().value;return F(d,q,3)}f=e.next();d.b=2;})};k.set=function(d,c,e){V.get(this)[d]=[[c,e]];};k.values=function c(){var e=this,f,g,h,q,y;return M(c,function(c){1==c.b&&(f=w(e),g=f.next());if(3!=c.b){if(g.done){c.b=0;return}h=g.value;q=w(h);q.next();y=q.next().value;return F(c,y,3)}g=f.next();c.b=2;})};X.prototype._asNative=function(){for(var c=
	new R,e=w(this),f=e.next();!f.done;f=e.next()){var g=w(f.value);f=g.next().value;g=g.next().value;c.append(f,g);}return c};X.prototype._blob=function(){for(var c="----formdata-polyfill-"+Math.random(),e=[],f=w(this),g=f.next();!g.done;g=f.next()){var h=w(g.value);g=h.next().value;h=h.next().value;e.push("--"+c+"\r\n");h instanceof Blob?e.push('Content-Disposition: form-data; name="'+g+'"; filename="'+h.name+'"\r\n',"Content-Type: "+(h.type||"application/octet-stream")+"\r\n\r\n",h,"\r\n"):e.push('Content-Disposition: form-data; name="'+
	g+'"\r\n\r\n'+h+"\r\n");}e.push("--"+c+"--");return new Blob(e,{type:"multipart/form-data; boundary="+c})};n();r();X.prototype[Symbol.iterator]=function(){return this.entries()};X.prototype.toString=function(){return "[object FormData]"};U&&(X.prototype[U]="FormData");[["append",N],["delete",O],["get",O],["getAll",O],["has",O],["set",N]].forEach(function(c){var e=X.prototype[c[0]];X.prototype[c[0]]=function(){return e.apply(this,c[1].apply(this,W(arguments)))};});S&&(XMLHttpRequest.prototype.send=function(c){c instanceof
	X?(c=c._blob(),this.setRequestHeader("Content-Type",c.type),S.call(this,c)):S.call(this,c);});if(T){var Y=Q.fetch;Q.fetch=function(c,e){e&&e.body&&e.body instanceof X&&(e.body=e.body._blob());return Y(c,e)};}Q.FormData=X;}})();

	/**
	 * Checks if `value` is the
	 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
	 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
	 * @example
	 *
	 * _.isObject({});
	 * // => true
	 *
	 * _.isObject([1, 2, 3]);
	 * // => true
	 *
	 * _.isObject(_.noop);
	 * // => true
	 *
	 * _.isObject(null);
	 * // => false
	 */
	function isObject(value) {
	  var type = typeof value;
	  return value != null && (type == 'object' || type == 'function');
	}

	var isObject_1 = isObject;

	/** Detect free variable `global` from Node.js. */
	var freeGlobal = typeof commonjsGlobal == 'object' && commonjsGlobal && commonjsGlobal.Object === Object && commonjsGlobal;

	var _freeGlobal = freeGlobal;

	/** Detect free variable `self`. */
	var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

	/** Used as a reference to the global object. */
	var root = _freeGlobal || freeSelf || Function('return this')();

	var _root = root;

	/**
	 * Gets the timestamp of the number of milliseconds that have elapsed since
	 * the Unix epoch (1 January 1970 00:00:00 UTC).
	 *
	 * @static
	 * @memberOf _
	 * @since 2.4.0
	 * @category Date
	 * @returns {number} Returns the timestamp.
	 * @example
	 *
	 * _.defer(function(stamp) {
	 *   console.log(_.now() - stamp);
	 * }, _.now());
	 * // => Logs the number of milliseconds it took for the deferred invocation.
	 */
	var now = function() {
	  return _root.Date.now();
	};

	var now_1 = now;

	/** Built-in value references. */
	var Symbol$1 = _root.Symbol;

	var _Symbol = Symbol$1;

	/** Used for built-in method references. */
	var objectProto = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$1 = objectProto.hasOwnProperty;

	/**
	 * Used to resolve the
	 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var nativeObjectToString = objectProto.toString;

	/** Built-in value references. */
	var symToStringTag = _Symbol ? _Symbol.toStringTag : undefined;

	/**
	 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @returns {string} Returns the raw `toStringTag`.
	 */
	function getRawTag(value) {
	  var isOwn = hasOwnProperty$1.call(value, symToStringTag),
	      tag = value[symToStringTag];

	  try {
	    value[symToStringTag] = undefined;
	  } catch (e) {}

	  var result = nativeObjectToString.call(value);
	  {
	    if (isOwn) {
	      value[symToStringTag] = tag;
	    } else {
	      delete value[symToStringTag];
	    }
	  }
	  return result;
	}

	var _getRawTag = getRawTag;

	/** Used for built-in method references. */
	var objectProto$1 = Object.prototype;

	/**
	 * Used to resolve the
	 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var nativeObjectToString$1 = objectProto$1.toString;

	/**
	 * Converts `value` to a string using `Object.prototype.toString`.
	 *
	 * @private
	 * @param {*} value The value to convert.
	 * @returns {string} Returns the converted string.
	 */
	function objectToString(value) {
	  return nativeObjectToString$1.call(value);
	}

	var _objectToString = objectToString;

	/** `Object#toString` result references. */
	var nullTag = '[object Null]',
	    undefinedTag = '[object Undefined]';

	/** Built-in value references. */
	var symToStringTag$1 = _Symbol ? _Symbol.toStringTag : undefined;

	/**
	 * The base implementation of `getTag` without fallbacks for buggy environments.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @returns {string} Returns the `toStringTag`.
	 */
	function baseGetTag(value) {
	  if (value == null) {
	    return value === undefined ? undefinedTag : nullTag;
	  }
	  return (symToStringTag$1 && symToStringTag$1 in Object(value))
	    ? _getRawTag(value)
	    : _objectToString(value);
	}

	var _baseGetTag = baseGetTag;

	/**
	 * Checks if `value` is object-like. A value is object-like if it's not `null`
	 * and has a `typeof` result of "object".
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
	 * @example
	 *
	 * _.isObjectLike({});
	 * // => true
	 *
	 * _.isObjectLike([1, 2, 3]);
	 * // => true
	 *
	 * _.isObjectLike(_.noop);
	 * // => false
	 *
	 * _.isObjectLike(null);
	 * // => false
	 */
	function isObjectLike(value) {
	  return value != null && typeof value == 'object';
	}

	var isObjectLike_1 = isObjectLike;

	/** `Object#toString` result references. */
	var symbolTag = '[object Symbol]';

	/**
	 * Checks if `value` is classified as a `Symbol` primitive or object.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
	 * @example
	 *
	 * _.isSymbol(Symbol.iterator);
	 * // => true
	 *
	 * _.isSymbol('abc');
	 * // => false
	 */
	function isSymbol$1(value) {
	  return typeof value == 'symbol' ||
	    (isObjectLike_1(value) && _baseGetTag(value) == symbolTag);
	}

	var isSymbol_1 = isSymbol$1;

	/** Used as references for various `Number` constants. */
	var NAN = 0 / 0;

	/** Used to match leading and trailing whitespace. */
	var reTrim = /^\s+|\s+$/g;

	/** Used to detect bad signed hexadecimal string values. */
	var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

	/** Used to detect binary string values. */
	var reIsBinary = /^0b[01]+$/i;

	/** Used to detect octal string values. */
	var reIsOctal = /^0o[0-7]+$/i;

	/** Built-in method references without a dependency on `root`. */
	var freeParseInt = parseInt;

	/**
	 * Converts `value` to a number.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to process.
	 * @returns {number} Returns the number.
	 * @example
	 *
	 * _.toNumber(3.2);
	 * // => 3.2
	 *
	 * _.toNumber(Number.MIN_VALUE);
	 * // => 5e-324
	 *
	 * _.toNumber(Infinity);
	 * // => Infinity
	 *
	 * _.toNumber('3.2');
	 * // => 3.2
	 */
	function toNumber(value) {
	  if (typeof value == 'number') {
	    return value;
	  }
	  if (isSymbol_1(value)) {
	    return NAN;
	  }
	  if (isObject_1(value)) {
	    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
	    value = isObject_1(other) ? (other + '') : other;
	  }
	  if (typeof value != 'string') {
	    return value === 0 ? value : +value;
	  }
	  value = value.replace(reTrim, '');
	  var isBinary = reIsBinary.test(value);
	  return (isBinary || reIsOctal.test(value))
	    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
	    : (reIsBadHex.test(value) ? NAN : +value);
	}

	var toNumber_1 = toNumber;

	/** Error message constants. */
	var FUNC_ERROR_TEXT = 'Expected a function';

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeMax = Math.max,
	    nativeMin = Math.min;

	/**
	 * Creates a debounced function that delays invoking `func` until after `wait`
	 * milliseconds have elapsed since the last time the debounced function was
	 * invoked. The debounced function comes with a `cancel` method to cancel
	 * delayed `func` invocations and a `flush` method to immediately invoke them.
	 * Provide `options` to indicate whether `func` should be invoked on the
	 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
	 * with the last arguments provided to the debounced function. Subsequent
	 * calls to the debounced function return the result of the last `func`
	 * invocation.
	 *
	 * **Note:** If `leading` and `trailing` options are `true`, `func` is
	 * invoked on the trailing edge of the timeout only if the debounced function
	 * is invoked more than once during the `wait` timeout.
	 *
	 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
	 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
	 *
	 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
	 * for details over the differences between `_.debounce` and `_.throttle`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Function
	 * @param {Function} func The function to debounce.
	 * @param {number} [wait=0] The number of milliseconds to delay.
	 * @param {Object} [options={}] The options object.
	 * @param {boolean} [options.leading=false]
	 *  Specify invoking on the leading edge of the timeout.
	 * @param {number} [options.maxWait]
	 *  The maximum time `func` is allowed to be delayed before it's invoked.
	 * @param {boolean} [options.trailing=true]
	 *  Specify invoking on the trailing edge of the timeout.
	 * @returns {Function} Returns the new debounced function.
	 * @example
	 *
	 * // Avoid costly calculations while the window size is in flux.
	 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
	 *
	 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
	 * jQuery(element).on('click', _.debounce(sendMail, 300, {
	 *   'leading': true,
	 *   'trailing': false
	 * }));
	 *
	 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
	 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
	 * var source = new EventSource('/stream');
	 * jQuery(source).on('message', debounced);
	 *
	 * // Cancel the trailing debounced invocation.
	 * jQuery(window).on('popstate', debounced.cancel);
	 */
	function debounce(func, wait, options) {
	  var lastArgs,
	      lastThis,
	      maxWait,
	      result,
	      timerId,
	      lastCallTime,
	      lastInvokeTime = 0,
	      leading = false,
	      maxing = false,
	      trailing = true;

	  if (typeof func != 'function') {
	    throw new TypeError(FUNC_ERROR_TEXT);
	  }
	  wait = toNumber_1(wait) || 0;
	  if (isObject_1(options)) {
	    leading = !!options.leading;
	    maxing = 'maxWait' in options;
	    maxWait = maxing ? nativeMax(toNumber_1(options.maxWait) || 0, wait) : maxWait;
	    trailing = 'trailing' in options ? !!options.trailing : trailing;
	  }

	  function invokeFunc(time) {
	    var args = lastArgs,
	        thisArg = lastThis;

	    lastArgs = lastThis = undefined;
	    lastInvokeTime = time;
	    result = func.apply(thisArg, args);
	    return result;
	  }

	  function leadingEdge(time) {
	    // Reset any `maxWait` timer.
	    lastInvokeTime = time;
	    // Start the timer for the trailing edge.
	    timerId = setTimeout(timerExpired, wait);
	    // Invoke the leading edge.
	    return leading ? invokeFunc(time) : result;
	  }

	  function remainingWait(time) {
	    var timeSinceLastCall = time - lastCallTime,
	        timeSinceLastInvoke = time - lastInvokeTime,
	        timeWaiting = wait - timeSinceLastCall;

	    return maxing
	      ? nativeMin(timeWaiting, maxWait - timeSinceLastInvoke)
	      : timeWaiting;
	  }

	  function shouldInvoke(time) {
	    var timeSinceLastCall = time - lastCallTime,
	        timeSinceLastInvoke = time - lastInvokeTime;

	    // Either this is the first call, activity has stopped and we're at the
	    // trailing edge, the system time has gone backwards and we're treating
	    // it as the trailing edge, or we've hit the `maxWait` limit.
	    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
	      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
	  }

	  function timerExpired() {
	    var time = now_1();
	    if (shouldInvoke(time)) {
	      return trailingEdge(time);
	    }
	    // Restart the timer.
	    timerId = setTimeout(timerExpired, remainingWait(time));
	  }

	  function trailingEdge(time) {
	    timerId = undefined;

	    // Only invoke if we have `lastArgs` which means `func` has been
	    // debounced at least once.
	    if (trailing && lastArgs) {
	      return invokeFunc(time);
	    }
	    lastArgs = lastThis = undefined;
	    return result;
	  }

	  function cancel() {
	    if (timerId !== undefined) {
	      clearTimeout(timerId);
	    }
	    lastInvokeTime = 0;
	    lastArgs = lastCallTime = lastThis = timerId = undefined;
	  }

	  function flush() {
	    return timerId === undefined ? result : trailingEdge(now_1());
	  }

	  function debounced() {
	    var time = now_1(),
	        isInvoking = shouldInvoke(time);

	    lastArgs = arguments;
	    lastThis = this;
	    lastCallTime = time;

	    if (isInvoking) {
	      if (timerId === undefined) {
	        return leadingEdge(lastCallTime);
	      }
	      if (maxing) {
	        // Handle invocations in a tight loop.
	        timerId = setTimeout(timerExpired, wait);
	        return invokeFunc(lastCallTime);
	      }
	    }
	    if (timerId === undefined) {
	      timerId = setTimeout(timerExpired, wait);
	    }
	    return result;
	  }
	  debounced.cancel = cancel;
	  debounced.flush = flush;
	  return debounced;
	}

	var debounce_1 = debounce;

	var pubsub = createCommonjsModule(function (module, exports) {
	/*
	Copyright (c) 2010,2011,2012,2013,2014 Morgan Roderick http://roderick.dk
	License: MIT - http://mrgnrdrck.mit-license.org

	https://github.com/mroderick/PubSubJS
	*/
	(function (root, factory){

	    var PubSub = {};
	    root.PubSub = PubSub;

	    var define = root.define;

	    factory(PubSub);

	    // AMD support
	    if (typeof define === 'function' && define.amd){
	        define(function() { return PubSub; });

	        // CommonJS and Node.js module support
	    } else {
	        if (module !== undefined && module.exports) {
	            exports = module.exports = PubSub; // Node.js specific `module.exports`
	        }
	        exports.PubSub = PubSub; // CommonJS module 1.1.1 spec
	        module.exports = exports = PubSub; // CommonJS
	    }

	}(( typeof window === 'object' && window ) || commonjsGlobal, function (PubSub){

	    var messages = {},
	        lastUid = -1;

	    function hasKeys(obj){
	        var key;

	        for (key in obj){
	            if ( obj.hasOwnProperty(key) ){
	                return true;
	            }
	        }
	        return false;
	    }

	    /**
		 *	Returns a function that throws the passed exception, for use as argument for setTimeout
		 *	@param { Object } ex An Error object
		 */
	    function throwException( ex ){
	        return function reThrowException(){
	            throw ex;
	        };
	    }

	    function callSubscriberWithDelayedExceptions( subscriber, message, data ){
	        try {
	            subscriber( message, data );
	        } catch( ex ){
	            setTimeout( throwException( ex ), 0);
	        }
	    }

	    function callSubscriberWithImmediateExceptions( subscriber, message, data ){
	        subscriber( message, data );
	    }

	    function deliverMessage( originalMessage, matchedMessage, data, immediateExceptions ){
	        var subscribers = messages[matchedMessage],
	            callSubscriber = immediateExceptions ? callSubscriberWithImmediateExceptions : callSubscriberWithDelayedExceptions,
	            s;

	        if ( !messages.hasOwnProperty( matchedMessage ) ) {
	            return;
	        }

	        for (s in subscribers){
	            if ( subscribers.hasOwnProperty(s)){
	                callSubscriber( subscribers[s], originalMessage, data );
	            }
	        }
	    }

	    function createDeliveryFunction( message, data, immediateExceptions ){
	        return function deliverNamespaced(){
	            var topic = String( message ),
	                position = topic.lastIndexOf( '.' );

	            // deliver the message as it is now
	            deliverMessage(message, message, data, immediateExceptions);

	            // trim the hierarchy and deliver message to each level
	            while( position !== -1 ){
	                topic = topic.substr( 0, position );
	                position = topic.lastIndexOf('.');
	                deliverMessage( message, topic, data, immediateExceptions );
	            }
	        };
	    }

	    function messageHasSubscribers( message ){
	        var topic = String( message ),
	            found = Boolean(messages.hasOwnProperty( topic ) && hasKeys(messages[topic])),
	            position = topic.lastIndexOf( '.' );

	        while ( !found && position !== -1 ){
	            topic = topic.substr( 0, position );
	            position = topic.lastIndexOf( '.' );
	            found = Boolean(messages.hasOwnProperty( topic ) && hasKeys(messages[topic]));
	        }

	        return found;
	    }

	    function publish( message, data, sync, immediateExceptions ){
	        var deliver = createDeliveryFunction( message, data, immediateExceptions ),
	            hasSubscribers = messageHasSubscribers( message );

	        if ( !hasSubscribers ){
	            return false;
	        }

	        if ( sync === true ){
	            deliver();
	        } else {
	            setTimeout( deliver, 0 );
	        }
	        return true;
	    }

	    /**
		 *	PubSub.publish( message[, data] ) -> Boolean
		 *	- message (String): The message to publish
		 *	- data: The data to pass to subscribers
		 *	Publishes the the message, passing the data to it's subscribers
		**/
	    PubSub.publish = function( message, data ){
	        return publish( message, data, false, PubSub.immediateExceptions );
	    };

	    /**
		 *	PubSub.publishSync( message[, data] ) -> Boolean
		 *	- message (String): The message to publish
		 *	- data: The data to pass to subscribers
		 *	Publishes the the message synchronously, passing the data to it's subscribers
		**/
	    PubSub.publishSync = function( message, data ){
	        return publish( message, data, true, PubSub.immediateExceptions );
	    };

	    /**
		 *	PubSub.subscribe( message, func ) -> String
		 *	- message (String): The message to subscribe to
		 *	- func (Function): The function to call when a new message is published
		 *	Subscribes the passed function to the passed message. Every returned token is unique and should be stored if
		 *	you need to unsubscribe
		**/
	    PubSub.subscribe = function( message, func ){
	        if ( typeof func !== 'function'){
	            return false;
	        }

	        // message is not registered yet
	        if ( !messages.hasOwnProperty( message ) ){
	            messages[message] = {};
	        }

	        // forcing token as String, to allow for future expansions without breaking usage
	        // and allow for easy use as key names for the 'messages' object
	        var token = 'uid_' + String(++lastUid);
	        messages[message][token] = func;

	        // return token for unsubscribing
	        return token;
	    };

	    /**
		 *	PubSub.subscribeOnce( message, func ) -> PubSub
		 *	- message (String): The message to subscribe to
		 *	- func (Function): The function to call when a new message is published
		 *	Subscribes the passed function to the passed message once
		**/
	    PubSub.subscribeOnce = function( message, func ){
	        var token = PubSub.subscribe( message, function(){
	            // before func apply, unsubscribe message
	            PubSub.unsubscribe( token );
	            func.apply( this, arguments );
	        });
	        return PubSub;
	    };

	    /* Public: Clears all subscriptions
		 */
	    PubSub.clearAllSubscriptions = function clearAllSubscriptions(){
	        messages = {};
	    };

	    /*Public: Clear subscriptions by the topic
		*/
	    PubSub.clearSubscriptions = function clearSubscriptions(topic){
	        var m;
	        for (m in messages){
	            if (messages.hasOwnProperty(m) && m.indexOf(topic) === 0){
	                delete messages[m];
	            }
	        }
	    };

	    /* Public: removes subscriptions.
		 * When passed a token, removes a specific subscription.
		 * When passed a function, removes all subscriptions for that function
		 * When passed a topic, removes all subscriptions for that topic (hierarchy)
		 *
		 * value - A token, function or topic to unsubscribe.
		 *
		 * Examples
		 *
		 *		// Example 1 - unsubscribing with a token
		 *		var token = PubSub.subscribe('mytopic', myFunc);
		 *		PubSub.unsubscribe(token);
		 *
		 *		// Example 2 - unsubscribing with a function
		 *		PubSub.unsubscribe(myFunc);
		 *
		 *		// Example 3 - unsubscribing a topic
		 *		PubSub.unsubscribe('mytopic');
		 */
	    PubSub.unsubscribe = function(value){
	        var descendantTopicExists = function(topic) {
	                var m;
	                for ( m in messages ){
	                    if ( messages.hasOwnProperty(m) && m.indexOf(topic) === 0 ){
	                        // a descendant of the topic exists:
	                        return true;
	                    }
	                }

	                return false;
	            },
	            isTopic    = typeof value === 'string' && ( messages.hasOwnProperty(value) || descendantTopicExists(value) ),
	            isToken    = !isTopic && typeof value === 'string',
	            isFunction = typeof value === 'function',
	            result = false,
	            m, message, t;

	        if (isTopic){
	            PubSub.clearSubscriptions(value);
	            return;
	        }

	        for ( m in messages ){
	            if ( messages.hasOwnProperty( m ) ){
	                message = messages[m];

	                if ( isToken && message[value] ){
	                    delete message[value];
	                    result = value;
	                    // tokens are unique, so we can just stop here
	                    break;
	                }

	                if (isFunction) {
	                    for ( t in message ){
	                        if (message.hasOwnProperty(t) && message[t] === value){
	                            delete message[t];
	                            result = true;
	                        }
	                    }
	                }
	            }
	        }

	        return result;
	    };
	}));
	});
	var pubsub_1 = pubsub.PubSub;

	/* global module */

	/**
	 * @exports listener
	 * @constructor
	**/
	function Listener() {

	  /**
	   * Object to store datas
	   * @property {object} data - Default data
	   * @property {number} data.delta - Scroll amount delta
	   * @property {string} data.direction - Scroll direction [up|down]
	   * @property {number} data.pos - Scroll position in pixels
	  **/
	  var data = {
	    delta: 0,
	    direction: 'down',
	    pos: null
	  };

	  /**
	   * Set up all event listeners
	   * @method
	  **/
	  function bind() {
	    // Scroll event
	    window.addEventListener('scroll', scroll, false);

	    // Resize event
	    window.addEventListener('resize', debounce_1(resize, 150));
	  }

	  /**
	   * Stores all DOM elements and assign default properties
	   * @method
	  **/
	  function dom() {
	    data.width = window.innerWidth;
	    data.height = window.innerHeight;
	  }

	  /**
	   * Inits the module with necessary functions
	   * @method
	  **/
	  function init() {

	    scroll();
	    resize();

	    dom();
	    bind();
	  }

	  /**
	   * Get scroll position
	   * @method
	  **/
	  function getPos() {
	    set();

	    return {
	      direction: data.direction,
	      pos: data.pos
	    };
	  }

	  /**
	   * Triggers resize event with all corresponding data
	   * @method
	  **/
	  function resize() {
	    set();

	    var width = window.innerWidth;
	    var height = window.innerHeight;
	    var params = {
	      direction: data.direction,
	      pos: data.pos,
	      delta: data.delta,
	      height: false,
	      width: false
	    };

	    if (!data.width || width !== data.width) {
	      params.width = width;
	    }

	    if (!data.height || height !== data.height) {
	      params.height = height;
	    }

	    isTouch();

	    // Trigger custom event
	    pubsub.publish('resize', params);

	    // Store new dimensions
	    data.height = height;
	    data.width = width;
	  }

	  /**
	   * Trigger custom scroll event with necessary data
	   * @method
	  **/
	  function scroll() {

	    set();

	    // Trigger custom event
	    pubsub.publish('scroll', {
	      direction: data.direction,
	      pos: data.pos,
	      delta: data.delta
	    });
	  }

	  /**
	   * Check if device is (probably) a touch device
	   * @method
	  **/
	  function isTouch() {
	    // detect touch devices
	    if (!!("ontouchstart" in window || window.navigator && window.navigator.msPointerEnabled && window.MSGesture || window.DocumentTouch && document instanceof DocumentTouch) && window.matchMedia(window.config.mq.touch).matches) {
	      document.documentElement.classList.add('touch');
	      document.documentElement.classList.remove('no-touch');
	      window.istouch = true;
	    } else {
	      document.documentElement.classList.add('no-touch');
	      document.documentElement.classList.remove('touch');
	      window.istouch = false;
	    }
	  }

	  /**
	   * Store scroll data
	   * @method
	  **/
	  function set() {
	    var pos = document.documentElement.scrollTop || document.body.scrollTop;
	    var direction = 'down';

	    if (data.pos === pos) {
	      return;
	    }

	    if (data.pos > pos) {
	      direction = 'up';
	    }

	    if (direction === data.direction) {
	      data.delta += Math.abs(data.pos - pos);
	    } else {
	      data.delta = 0;
	    }

	    // Store position
	    data.direction = direction;
	    data.pos = pos;
	  }

	  // start module
	  pubsub.subscribe('app.start', init);

	  return {
	    getPos: getPos
	  };
	}

	Listener();

	(function (ElementProto) {
	  ElementProto.listen = function listen(event, selector, callback, options) {

	    var element = this;

	    function Event(src) {

	      // Event object
	      if (src && src.type) {
	        this.originalEvent = src;
	        this.type = src.type;

	        // Events bubbling up the document may have been marked as prevented
	        // by a handler lower down the tree; reflect the correct value.
	        this.isDefaultPrevented = src.defaultPrevented || src.defaultPrevented === undefined &&

	        // Support: Android<4.0
	        src.returnValue === false ? true : false;

	        // Event type
	      } else {
	        this.type = src;
	      }

	      for (var key in src) {
	        // cleanup the new event object
	        if (key === key.toUpperCase() || 'function' === typeof src[key]) {
	          continue;
	        }

	        this[key] = src[key];
	      }
	    }

	    Event.prototype = {
	      constructor: Event,
	      isDefaultPrevented: false,
	      isPropagationStopped: false,
	      isImmediatePropagationStopped: false,

	      preventDefault: function preventDefault() {
	        var e = this.originalEvent;

	        this.isDefaultPrevented = true;

	        if (e) {
	          e.preventDefault();
	        }
	      },

	      stopPropagation: function stopPropagation() {
	        var e = this.originalEvent;

	        this.isPropagationStopped = true;

	        if (e) {
	          e.stopPropagation();
	        }
	      },

	      stopImmediatePropagation: function stopImmediatePropagation() {
	        var e = this.originalEvent;

	        this.isImmediatePropagationStopped = true;

	        if (e) {
	          e.stopImmediatePropagation();
	        }

	        this.stopPropagation();
	      }
	    };

	    function delegate(root, event, selector, callback, options) {

	      var delegate = void 0;

	      if ('string' === typeof selector) {
	        delegate = true;
	      } else {
	        options = callback;
	        callback = selector;
	      }

	      function fixEvent(event, extend) {
	        // Create a writable copy of the event object
	        var originalEvent = event;
	        event = new Event(originalEvent);

	        // Support: Safari 6-8+
	        // Target should not be a text node (#504, #13143)
	        if (event.target.nodeType === 3) {
	          event.target = event.target.parentNode;
	        }

	        for (var key in extend) {
	          event[key] = extend[key];
	        }

	        return event;
	      }

	      // unbind
	      function off() {
	        root.removeEventListener(event, testMatch, options);
	      }

	      // event handler
	      function testMatch(e) {

	        // if theres no delegation send the event directly
	        if (!delegate) {
	          callback(fixEvent(e, { currentTarget: e.currentTarget }));

	          if (options && options.once) {
	            off();
	          }

	          return;
	        }

	        // find the closest match for the passed selector
	        var match = e.target.closest(selector);

	        if (!match) {
	          return;
	        }

	        // fire the callback
	        callback(fixEvent(e, { currentTarget: match }));

	        if (options && options.once) {
	          off();
	        }
	      }

	      // bind
	      root.addEventListener(event, testMatch, options);

	      // return passed parameters and the unbind method
	      return {
	        event: event,
	        options: options,
	        selector: selector,
	        unbind: off
	      };
	    }

	    delegate(element, event, selector, callback, options);
	  };
	})(window.Element.prototype);

	window.Element.prototype.qsa = function querySelectorAllToArray(selector) {
	  var selection = this.querySelectorAll(selector);

	  return Array.prototype.slice.call(selection);
	};

	Document.prototype.qsa = function querySelectorAllToArray(selector) {
	  var selection = this.querySelectorAll(selector);

	  return Array.prototype.slice.call(selection);
	};

	window.Element.prototype.nodeIndex = function index() {
	  return Array.from(this.parentNode.children).indexOf(this);
	};

	if (!("objectFit" in document.documentElement.style)) {
	  document.qsa('.o-fit').forEach(function (imgParent) {

	    if (imgParent.querySelector('img') != null) {
	      imgParent.style.backgroundImage = 'url(' + imgParent.querySelector('img').src + ')';
	      imgParent.classList.add('object-fit');
	    }
	  });
	}

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	/**
	 * Slider
	 * @constructor
	 * @requires pubsub-js
	 */
	var Slider = function () {
	  function Slider() {
	    _classCallCheck(this, Slider);

	    this.init = this.init.bind(this);

	    this.init();
	  }

	  _createClass(Slider, [{
	    key: 'init',
	    value: function init(dom) {

	      dom = dom || document;

	      this.sliders = dom.qsa('[data-sliderConfig]');

	      this.sliders.forEach(function (slider) {

	        var config = JSON.parse(slider.dataset.sliderconfig);

	        console.log(config['id']);

	        if (config['pagination']) {
	          config.pagination = {
	            el: slider.querySelector('.swiper-pagination'),
	            clickable: true,
	            renderBullet: function renderBullet(index, className) {
	              return '<span class="' + className + '">' + (index + 1) + '</span>';
	            }
	          };
	        }
	        if (config['breakpoints']) {
	          config.breakpoints = {
	            320: {
	              slidesPerView: 1,
	              spaceBetween: 0
	            },
	            600: {
	              slidesPerView: 1,
	              spaceBetween: 10
	            },
	            1024: {
	              slidesPerView: 2,
	              spaceBetween: 10
	            }
	          };
	        }
	        if (config['navigation']) {
	          config.navigation = {
	            nextEl: '.bt-slider.next',
	            prevEl: '.bt-slider.prev'
	          };
	        }
	        if (config['navigation-full-height']) {
	          config.navigation = {
	            nextEl: '.custom-button.next',
	            prevEl: '.custom-button.prev'
	          };
	        }

	        if (config['navigation-mob']) {
	          config.navigation = {
	            nextEl: '.custom-button_' + config['id'] + '.next',
	            prevEl: '.custom-button_' + config['id'] + '.prev'
	          };
	        }

	        if (config["external-nav"]) {
	          config.on = {
	            slideChange: function slideChange() {
	              if (slider.sliderDom) {
	                var slides = slider.closest('[data-sliderContainer]').qsa('[data-slideTo]');
	                slides.forEach(function (slide) {
	                  slide.classList.remove('active');
	                });
	                var currentSlide = slider.closest('[data-sliderContainer]').querySelector('[data-slideTo="' + parseInt(slider.sliderDom.realIndex + 1, 10) + '"]');
	                if (currentSlide) {
	                  currentSlide.classList.add('active');
	                }
	              }
	            }
	          };
	        }
	        if (!config['external-nav']) {
	          config.on = {};
	          config.on.init = function () {
	            if (this.isBeginning && this.isEnd) {
	              slider.classList.add('all-visible');
	            }
	          };

	          config.on.resize = function (swiper) {
	            if (this.isBeginning && this.isEnd) {
	              slider.classList.add('all-visible');
	            } else {
	              slider.classList.remove('all-visible');
	            }
	          };
	        }

	        window.requestAnimationFrame(function () {
	          slider.sliderDom = new Swiper(slider, config);
	        });

	        if (config.autoplay) {
	          document.addEventListener('visibilitychange', function () {
	            if (!slider.sliderDom) {
	              return;
	            }
	            if (document.hidden) {
	              slider.sliderDom.autoplay.stop();
	            } else {
	              slider.sliderDom.autoplay.start();
	            }
	          });
	        }
	      });

	      var controls = document.querySelector('[data-slider-controls]');

	      if (!document.querySelector('[data-slideTo]')) {
	        return;
	      }
	      controls.qsa('[data-slideTo]').forEach(function (control) {

	        control.addEventListener('click', function (e) {

	          var trigger = e.currentTarget,
	              slide = +trigger.dataset.slideto,
	              slider = trigger.closest('[data-sliderContainer]').querySelector('[data-sliderConfig]');

	          slider.sliderDom.slideTo(slide);
	        });
	      });
	    }
	  }]);

	  return Slider;
	}();

	var sliders = new Slider();

	var config = {
	  headers: {
	    'X-Requested-With': 'XMLHttpRequest'
	  },
	  params: {
	    credentials: 'same-origin'
	  },
	  timeout: 30000
	};

	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

	/**
	 * create a custom response object based on the fetch response object
	 * @param {*} data - data parsed by the pass method
	 * @param {object} response - fetch response object
	 * @return {object} custom response object
	 */
	function createResponse(data, response) {
	  var ok = response.ok,
	      redirected = response.redirected,
	      status = response.status,
	      statusText = response.statusText,
	      type = response.type,
	      url = response.url;

	  var headers = {};

	  var _iteratorNormalCompletion = true;
	  var _didIteratorError = false;
	  var _iteratorError = undefined;

	  try {
	    for (var _iterator = response.headers.entries()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	      var _ref = _step.value;

	      var _ref2 = _slicedToArray(_ref, 2);

	      var name = _ref2[0];
	      var value = _ref2[1];

	      headers[name] = value;
	    }
	  } catch (err) {
	    _didIteratorError = true;
	    _iteratorError = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion && _iterator.return) {
	        _iterator.return();
	      }
	    } finally {
	      if (_didIteratorError) {
	        throw _iteratorError;
	      }
	    }
	  }

	  return {
	    headers: headers,
	    ok: ok,
	    redirected: redirected,
	    status: status,
	    statusText: statusText,
	    type: type,
	    url: url,
	    data: data
	  };
	}

	/**
	 * do the fetch call
	 * @param {string} url - url to fetch
	 * @param {object} params - fetch paramerters object
	 * @param {object} options - one time configuration of the fetch request
	 * @return {Promise} Promise object containing the formated response
	 */
	function fetch(url) {
	  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

	  // merge params
	  params = Object.assign({}, config.params, params);

	  if (!params.headers) {
	    params.headers = {};
	  }

	  // merge headers
	  params.headers = Object.assign({}, config.headers, params.headers);

	  // create a promise that can be rejected by the timeout
	  return new Promise(function (resolve, reject) {
	    var rejected = false;
	    // fail when theres a timeout or not internet connection
	    var browserReject = function browserReject(error) {
	      rejected = true;

	      reject({
	        status: error ? 0 : 599,
	        statusText: error ? error.message : 'Network Connect Timeout Error',
	        url: url
	      });
	    };

	    var timeout = window.setTimeout(browserReject, options.timeout || config.timeout);

	    // fetch the url and resolve or reject the current promise based on its resolution
	    window.fetch(url, params).then(function (res) {
	      if (rejected) {
	        return;
	      }

	      resolve(res);
	    }).catch(browserReject).then(function () {
	      window.clearTimeout(timeout);
	    });
	  })
	  // check validity of the response
	  .then(function (response) {
	    return pass(response, params, options.parse);
	  });
	}

	/**
	 * check respone allow the use of `then` and `catch` based on the value of the success key
	 * @param {object} response - fetch response object
	 * @param {object} params - param object used to trigger the call
	 * @return {Promise} Promise object containing the formated response
	 */
	function pass(response, params) {
	  var shouldParse = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

	  if (!shouldParse) {
	    return response;
	  }

	  var contentType = response.headers.get('content-type');
	  var parsing = void 0;

	  if (contentType) {
	    contentType = contentType.split(';')[0];
	  }

	  switch (contentType) {
	    case 'application/json':
	      parsing = response.json();
	      break;
	    case 'multipart/form-data':
	      parsing = response.formData();
	      break;
	    case 'application/octet-stream':
	      parsing = response.blob();
	      break;
	    default:
	      parsing = response.text();
	  }

	  return parsing.then(function (data) {
	    var formatedResponse = createResponse(data, response);

	    if (!response.ok) {
	      return Promise.reject(formatedResponse);
	    }

	    return formatedResponse;
	  });
	}

	var escape = window.encodeURIComponent;

	function queryfy(params) {
	  return Object.keys(params).map(function (key) {
	    if (Array.isArray(params[key])) {
	      return params[key].map(function (value) {
	        return escape(key) + '=' + escape(value);
	      }).join('&');
	    }

	    return escape(key) + '=' + escape(params[key]);
	  }).join('&');
	}

	/**
	 * GET
	 * @param {string} url -the url to fetch
	 * @param {object} params - the fetch API param object
	 * @param {object} options - one time configuration of the fetch request
	 * @return {promise} the fetch promise
	 */
	function get(url) {
	  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};


	  params.method = 'get';

	  if (params.data) {
	    var search = url.split('?')[1];

	    if (search) {
	      url += '&' + queryfy(params.data);
	    } else {
	      url += '?' + queryfy(params.data);
	    }

	    delete params.data;
	  }

	  return fetch(url, params, options);
	}

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

	/**
	 * SEND
	 * @param {string} url -the url to fetch
	 * @param {object} params - the fetch API param object
	 * @param {object} options - one time configuration of the fetch request
	 * @return {promise} the fetch promise
	 */
	function send(url) {
	  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

	  // const multipart = params.headers && params.headers[ 'Content-Type' ] && params.headers[ 'Content-Type' ].toLowerCase().indexOf( 'multipart/form-data' ) > -1;

	  var currentContentType = void 0;
	  var format = true;

	  if (params.headers) {
	    Object.keys(params.headers).some(function (header) {
	      var headerName = header.toLowerCase();

	      if (headerName !== 'content-type') {
	        return;
	      }

	      currentContentType = params.headers[header].toLowerCase().split(';')[0];

	      // multipart = contentType === 'multipart/form-data';
	      // json = contentType === 'application/json';

	      return true;
	    });
	  } else {
	    params.headers = {};
	  }

	  if (currentContentType === 'multipart/form-data' || currentContentType === 'application/octet-stream') {
	    format = false;
	  }

	  if (format && params.data) {
	    if ('append' in params.data.__proto__ || 'type' in params.data.__proto__) {
	      format = false;

	      if (params.data.type && !currentContentType) {
	        params.headers['content-type'] = params.data.type;
	      }
	    } else if (!currentContentType && _typeof(params.data) === 'object') {
	      params.headers['content-type'] = 'application/json;charset=UTF-8';
	    }
	  }

	  // merge params
	  params = Object.assign({}, {
	    // default to post
	    method: 'post'
	  }, params);

	  if (params.data) {
	    // stringify the JSON data if the data is not multipart
	    params.body = format ? JSON.stringify(params.data) : params.data;
	    delete params.data;
	  }

	  return fetch(url, params, options);
	}

	var _slicedToArray$1 = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

	function toJSON(form) {
	  var stringOnly = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	  var data = new FormData(form);
	  var json = {};

	  var _iteratorNormalCompletion = true;
	  var _didIteratorError = false;
	  var _iteratorError = undefined;

	  try {
	    for (var _iterator = data[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	      var _ref = _step.value;

	      var _ref2 = _slicedToArray$1(_ref, 2);

	      var name = _ref2[0];
	      var value = _ref2[1];


	      if (stringOnly && typeof value !== 'string') {
	        continue;
	      }

	      // don't store empty file inputs
	      if (value.constructor.name === 'File' && value.size === 0) {
	        continue;
	      }

	      if (json[name]) {
	        // push the value
	        if (Array.isArray(json[name])) {
	          json[name].push(value);

	          continue;
	        }

	        // transform into an array
	        json[name] = [json[name], value];

	        continue;
	      }

	      // create pair
	      json[name] = value;
	    }
	  } catch (err) {
	    _didIteratorError = true;
	    _iteratorError = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion && _iterator.return) {
	        _iterator.return();
	      }
	    } finally {
	      if (_didIteratorError) {
	        throw _iteratorError;
	      }
	    }
	  }

	  return json;
	}

	var escape$1 = window.encodeURIComponent;

	function toQuery(form) {
	  var params = toJSON(form, true);

	  return Object.keys(params).map(function (key) {
	    if (Array.isArray(params[key])) {
	      return params[key].map(function (value) {
	        return escape$1(key) + '=' + escape$1(value);
	      }).join('&');
	    }

	    return escape$1(key) + '=' + escape$1(params[key]);
	  }).join('&');
	}

	function hasFile(form) {
	  var elements = Array.from(form.elements);

	  return elements.some(function (element) {
	    return element.type === 'file' && element.files.length > 0;
	  });
	}

	var formUtils = { toJSON: toJSON, toQuery: toQuery, hasFile: hasFile };

	/**
	 * Get the form data and use fetch based on the action and method attributes
	 * @param {HTMLFormElement} form - the form to submit asynchronously
	 * @param {object} params - the fetch API param object
	 * @param {object} options - one time configuration of the fetch request
	 * @return {Promise} Promise object containing the formated response
	 */
	function form(form) {
	  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

	  var callMethod = send;
	  var contentType = form.enctype;

	  if (!params.header) {
	    params.header = {};
	  }

	  if (form.method && !params.method) {
	    params.method = form.method;
	  }

	  if (contentType && !params.header['Content-Type']) {
	    params.header['Content-Type'] = contentType;
	  }

	  if (params.method === 'get') {
	    callMethod = get;
	  }

	  if (formUtils.hasFile(form)) {
	    if (!params.header) {
	      params.header = {};
	    }

	    params.header['Content-Type'] = 'multipart/form-data';

	    params.data = new FormData(form);
	  } else {
	    params.data = formUtils.toJSON(form);
	  }

	  return callMethod(form.action, params, options);
	}

	var fetcher = { get: get, send: send, form: form, config: config };

	var _createClass$1 = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck$1(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	/**
	 * Handle custom selects behaviour
	 * @constructor
	 */

	var Selects = function () {

	  /**
	   * Inits the module with necessary functions
	   * @method
	  **/
	  function Selects() {
	    _classCallCheck$1(this, Selects);

	    this.refresh = this.refresh.bind(this);
	    this.bind = this.bind.bind(this);
	    this.enable = this.enable.bind(this);
	    this.link = this.link.bind(this);
	    // this.create = this.create.bind(this);
	    this.change = this.change.bind(this);
	    this.refresh();
	  }

	  _createClass$1(Selects, [{
	    key: 'bind',
	    value: function bind(dom) {

	      dom = dom || document;

	      document.body.listen('change', '.custom-select select', this.change);
	      document.body.listen('change', 'select[data-enable]', this.enable);
	      document.body.listen('change', 'select[data-link]', this.link);
	    }
	  }, {
	    key: 'link',
	    value: function link(e) {

	      var selected = e.target.querySelectorAll('option')[e.target.selectedIndex],
	          value = selected.dataset.link ? selected.dataset.link : e.target.value;

	      if (/^#/.test(value)) {
	        window.scroll({
	          behavior: 'smooth',
	          top: document.querySelector(value).offsetTop - (document.querySelector('header[role="banner"]') ? document.querySelector('header[role="banner"]').offsetHeight : 0) - 10
	        });
	      } else {
	        window.location.href = value;
	      }
	    }
	  }, {
	    key: 'populate',
	    value: function populate(target, url, params) {
	      var _this = this;

	      fetcher.get(url, { data: params }).then(function (body) {
	        if (body.data.length == 0) {
	          return;
	        }

	        var html = '',
	            i = 0;

	        for (; i < body.data.length; i++) {
	          html += '<option value="' + body.data[i].value + '">' + body.data[i].label + '</option>';
	        }

	        target.querySelector('select').innerHTML = html;
	        window.requestAnimationFrame(function () {
	          _this.change(target.querySelector('select'));
	          pubsub.publish('searchafter', target.closest('form'));
	        });
	      });
	    }
	  }, {
	    key: 'enable',
	    value: function enable(e, close) {
	      var _this2 = this;

	      var trigger = e.originalEvent ? e.currentTarget : e,
	          target = document.getElementById(trigger.dataset.enable);
	      var params = {};

	      if (trigger.value.length > 0) {
	        if (target.querySelector('[disabled]')) {
	          target.classList.add('active');
	          target.querySelector('.std-field.disabled').classList.remove('disabled');
	          target.querySelector('[disabled]').removeAttribute('disabled');
	        }
	        if (trigger.dataset.populate) {

	          params[trigger.name] = trigger.value;
	          this.populate(target, trigger.dataset.populate, params);
	        }
	      } else if (trigger.value.length === 0 || close) {
	        target.classList.remove('active');
	        target.querySelector('.std-field').classList.add('disabled');
	        target.querySelector('select').setAttribute('disabled', true);

	        if (target.querySelector('[data-enable]')) {
	          this.enable(target.querySelector('[data-enable]'), true);
	        }
	        if (trigger.dataset.populate) {
	          target.querySelector('select option').selected = true;
	          window.requestAnimationFrame(function () {
	            _this2.change(target.querySelector('select'));
	          });
	        }

	        this.populate(target, trigger.dataset.populate, params);
	      }
	    }
	  }, {
	    key: 'change',
	    value: function change(e) {

	      var select = e.target ? e.target : e,
	          label = select.parentNode.querySelector('label'),
	          optIndex = select.selectedIndex,
	          selected = select.querySelectorAll('option')[optIndex];

	      label.textContent = selected.textContent;
	      if (optIndex == 0 && selected.value.length == 0) {
	        select.classList.add('placeholder');
	      } else {
	        select.classList.remove('placeholder');
	      }

	      if (selected.dataset.class) {
	        label.classList.add(selected.dataset.class);
	        label.dataset.class = selected.dataset.class;
	      } else if (label.dataset.class) {
	        label.classList.remove(label.dataset.class);
	        label.removeAttribute('data-class');
	      }

	      if (selected.classList.contains('disabled')) {
	        label.classList.add('disabled');
	      } else {
	        label.classList.remove('disabled');
	      }
	    }
	  }, {
	    key: 'refresh',
	    value: function refresh(dom) {
	      dom = dom || document;

	      if (!dom.querySelector('.custom-select select')) {
	        return;
	      }

	      // this.create( dom );

	      document.qsa('.custom-select select').forEach(function (select) {
	        if (select.selectedIndex === 0 && select.querySelector('option').value.length == 0) {
	          select.classList.add('placeholder');
	        }
	      });

	      this.bind(dom);
	    }
	  }]);

	  return Selects;
	}();

	var select = new Selects();

	var _createClass$2 = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck$2(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	/**
	 * Range
	 * @constructor
	 * @requires pubsub-js
	 */

	var Range = function () {

	  /**
	   * Inits the module with necessary functions
	   * @method
	  **/
	  function Range() {
	    _classCallCheck$2(this, Range);

	    if (!document.querySelector('[data-range]')) {
	      return;
	    }

	    this.bind = this.bind.bind(this);
	    this.init = this.init.bind(this);
	    this.resetSlider = this.resetSlider.bind(this);

	    this.init();
	  }

	  _createClass$2(Range, [{
	    key: 'bind',
	    value: function bind() {

	      document.body.listen('keydown', '[data-handle]', function (e) {
	        var trigger = e.currentTarget,
	            range = trigger.closest('[data-range]'),
	            index = trigger.dataset.handle,
	            step = range.noUiSlider.steps()[index],
	            intercept = [37, 38, 39, 40].indexOf(e.which) !== -1;

	        var value = Number(range.noUiSlider.get()[index]);

	        if (!intercept) {
	          return;
	        }

	        // right
	        if (e.which === 37) {
	          value = value - step[0];
	        }
	        // down
	        if (e.which === 40) {
	          e.preventDefault();
	          value = value - step[0] * 5;
	        }

	        // left
	        if (e.which === 39) {
	          value = value + step[1];
	        }
	        // up
	        if (e.which === 38) {
	          e.preventDefault();
	          value = value + step[1] * 5;
	        }

	        var values = [index == 0 ? value : null, index == 1 ? value : null];

	        if (intercept) {
	          range.noUiSlider.set(values);
	        }
	      });
	    }
	  }, {
	    key: 'init',
	    value: function init() {

	      var rangeItems = document.qsa('[data-range]'),
	          defaultData = {
	        connect: [false, true, false],
	        behaviour: 'tap'
	      };

	      rangeItems.forEach(function (rangeItem) {

	        var data = JSON.parse(rangeItem.dataset.range),
	            val = (data.prefix ? data.suffix : '') + '${value}' + (data.suffix ? data.suffix : '');

	        data = Object.assign(data, defaultData);

	        data.tooltips = [{
	          to: function to(value) {
	            return val.replace('${value}', Math.round(value).toLocaleString(window.locale));
	          }
	        }, {
	          to: function to(value) {
	            return val.replace('${value}', Math.round(value).toLocaleString(window.locale));
	          }
	        }];

	        data.pips = {
	          mode: 'range',
	          density: data.pipsDensity,
	          format: {
	            to: function to(value) {
	              return val.replace('${value}', Math.round(value).toLocaleString(window.locale));
	            }
	          }
	        };

	        var range = noUiSlider.create(rangeItem, data);

	        range.on('update', function (values) {
	          document.querySelector('input[name="' + rangeItem.id + '-min"]').value = values[0];
	          document.querySelector('input[name="' + rangeItem.id + '-max"]').value = values[1];
	        });
	        range.on('change', function () {

	          $('.portail .reset-search').addClass('active');
	          if (rangeItem.closest('form')) {
	            pubsub.publish('search', rangeItem.closest('form'));
	          }
	        });
	      });

	      this.bind();
	    }
	  }, {
	    key: 'resetSlider',
	    value: function resetSlider() {
	      var engineSlider = document.getElementById('engine');
	      var PriceSlider = document.getElementById('price');

	      engineSlider.noUiSlider.reset();
	      PriceSlider.noUiSlider.reset();
	    }
	  }]);

	  return Range;
	}();

	var range = new Range();

	var _createClass$3 = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck$3(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var logoPosition = 0;
	var isOccasion = true;
	var headerHeight = 0;
	var _this;
	/**
	 * Misc
	 * @constructor
	 * @requires pubsub-js
	 * @requires fetcher
	 * @requires form-utils
	 */

	var Misc = function () {

	  /**
	   * Inits the module with necessary functions
	   * @method
	  **/
	  function Misc() {
	    _classCallCheck$3(this, Misc);

	    this._storageHandler = this._storageHandler.bind(this);
	    this._fixHeader = this._fixHeader.bind(this);
	    this._scroll = this._scroll.bind(this);
	    this._submit = this._submit.bind(this);
	    this._load = this._load.bind(this);
	    this._popinVideo = this._popinVideo.bind(this);
	    this._bind = this._bind.bind(this);
	    this._serviceScroll = this._serviceScroll.bind(this);
	    this._searchtab = this._searchtab.bind(this);
	    this._resetForm = this._resetForm.bind(this);
	    this.showResetBtn = this.showResetBtn.bind(this);
	    this.printPage = this.printPage.bind(this);
	    this.openTab = this.openTab.bind(this);

	    this._bind();
	    this._fixHeader();

	    _this = this;
	  }

	  _createClass$3(Misc, [{
	    key: '_bind',
	    value: function _bind() {

	      document.body.listen('click', '[data-storage]', this._storageHandler);
	      document.body.listen('click', '[data-scroll]', this._scroll);
	      document.body.listen('change', '[data-submit]', this._submit);
	      document.body.listen('click', '[data-load-more]', this._load);
	      document.body.listen('click', 'a[data-load], button[data-load]', this._load);
	      document.body.listen('change', 'select[data-load]', this._load);
	      document.body.listen('submit', 'form[data-load]', this._load);
	      document.body.listen('click', '[data-video]:not([data-thumb])', this._popinVideo);
	      document.body.listen('click', '#search-tabs button', this._searchtab);
	      document.body.listen('click', '.reset-search', this._resetForm);
	      document.body.listen('change', '.search-contents form select', this.showResetBtn);
	      document.body.listen('change', '.search-contents form input', this.showResetBtn);
	      document.body.listen('click', '.print-btn', this.printPage);
	      document.body.listen('click', '.concession-tab', this.openTab);
	      document.body.listen('click', '.occasion-tab', this.openTab);
	      // document.body.listen('change', '#agreed', this._storageHandler);


	      pubsub.subscribe('search', this._submit);
	      pubsub.subscribe('searchafter', this._submit);

	      if (document.cookie.indexOf('legalAccepted') !== -1 && document.getElementById('cookies')) {
	        document.getElementById('cookies').parentNode.removeChild(document.getElementById('cookies'));
	      }

	      // console.log($('#cookies').innerHeight())
	      logoPosition = $('#cookies').innerHeight();
	      headerHeight = $('.portail header').innerHeight();
	      $('.portail .hero').css({ 'marginTop': headerHeight + 'px' });

	      if (document.querySelector('#cookies') != null) {

	        // $('.main-logo').css({'top':logoPosition+'px'})
	        if (document.querySelector('#cookies').getAttribute("aria-hidden") == "false") {

	          $('.main-logo.desktop').css({ 'position': 'absolute' });
	          $('.retailer-infos .wp img').css({ 'position': 'absolute' });
	          $('.portail header').css({ 'position': 'relative' });
	          $('.portail .hero').css({ 'marginTop': 0 });
	        } else {
	          $('.main-logo.desktop').css({ 'position': 'fixed' });
	          $('.retailer-infos .wp img').css({ 'position': 'fixed' });
	        }
	      }

	      if ($(window).innerWidth() >= 1024) {
	        var retailerLogoHeight = $('.retailer-infos .wp img').innerHeight();
	        var retailerPosition = (92 - retailerLogoHeight) / 2;
	        $('.retailer-infos .wp img').css({ 'top': retailerPosition + "px" });
	      }

	      $('#search-tab-1 select').each(function () {

	        if ($(this).attr('data-value') != undefined) {
	          var currLoc = $(this).attr('data-value');
	          $(this).val(currLoc);
	          select.change($(this)[0]);
	        }
	      });

	      setTimeout(function () {
	        if ($('#location-vehicle').prop("selectedIndex") > 0) {
	          toggle._toggleHandler($('#location-vehicle')[0]);
	          var currRegion = $('#location-vehicle').attr('option-value');

	          $('#select-region-vehicle').val(currRegion);

	          select.change($('#select-region-vehicle')[0]);
	        }

	        if ($('.portail .macro-search #brand').prop("selectedIndex") > 0) {
	          select.enable($('.portail .macro-search #brand')[0]);
	        }
	      }, 500);

	      if ($('.portail .search-contents').hasClass('concession')) {
	        $("#search-tabs button[aria-controls = 'search-tab-2']").trigger('click');
	        setTimeout(function () {
	          toggle._toggleHandler($("#search-tabs button[aria-controls = 'search-tab-2']")[0]);
	          $('#search-tab-2 button').trigger('click');

	          $('html, body').animate({
	            scrollTop: $('.macro-search-results').offset().top - 100
	          }, 1000);
	        }, 300);
	      }

	      this._serviceScroll(location.hash);
	    }
	  }, {
	    key: 'locate',
	    value: function locate(form) {

	      if (navigator.geolocation) {

	        var prevent = function prevent(e) {
	          e.preventDefault();
	          e.stopPropagation();
	        };
	        var pos = {};

	        // disable form
	        form.classList.add('loading');
	        form.addEventListener('submit', prevent);

	        // geolocate
	        navigator.geolocation.getCurrentPosition(
	        // success
	        function (position) {
	          // console.log( "User accepted the request for Geolocation and it worked:", position );
	          window.userLocation = position.coords;
	          for (var key in position.coords) {
	            pos[key] = position.coords[key];
	          }
	          document.qsa('[data-position]').forEach(function (input) {
	            input.value = JSON.stringify(pos);
	          });

	          // re-enable
	          form.classList.remove('loading');
	          form.removeEventListener('submit', prevent);
	          pubsub.publish('search', form);
	        },
	        // error
	        function (error) {
	          switch (error.code) {
	            case error.PERMISSION_DENIED:
	              // console.log( "User denied the request for Geolocation." );
	              break;
	            case error.POSITION_UNAVAILABLE:
	              // console.log( "Location information is unavailable." );
	              break;
	            case error.TIMEOUT:
	              // console.log( "The request to get user location timed out." );
	              break;
	            case error.UNKNOWN_ERROR:
	              // console.log( "An unknown error occurred." );
	              break;
	          }

	          form.qsa('[data-locate]').forEach(function (option) {
	            var parentSelect = option.closest('select');
	            option.setAttribute('disabled', true);
	            parentSelect.querySelector('option:not([disabled])').selected = 'selected';
	            select.change(option.closest('select'));
	            toggle.close({ id: option.getAttribute('aria-controls') });
	          });

	          // re-enable
	          form.classList.remove('loading');
	          form.removeEventListener('submit', prevent);
	        });
	      }
	    }
	  }, {
	    key: '_scroll',
	    value: function _scroll(e) {

	      var trigger = e.originalEvent ? e.currentTarget : e,
	          smooth = { behavior: 'smooth' },
	          scrollData = trigger.dataset && trigger.dataset.scroll ? Object.assign(JSON.parse(trigger.dataset.scroll), smooth) : smooth;

	      var id = void 0;

	      if (e.originalEvent) {
	        e.preventDefault();

	        if (trigger.href) {
	          id = trigger.getAttribute('href');
	        }
	      } else {
	        id = trigger.href;
	      }

	      if (!("top" in scrollData) && trigger.href) {
	        if (!document.querySelector(id)) {

	          return;
	        }
	        // document.querySelector(trigger.getAttribute('href')).scrollIntoView(smooth);
	        scrollData.top = document.querySelector(id).offsetTop - (document.querySelector('header[role="banner"]') ? document.querySelector('header[role="banner"]').offsetHeight : 0) - 10;
	      }

	      window.scroll(scrollData);
	    }
	  }, {
	    key: '_serviceScroll',
	    value: function _serviceScroll(e) {
	      //setTimeout(function(){

	      // console.log("test")

	      if (e.length > 0) {
	        $('html, body').animate({
	          scrollTop: $(e).offset().top - 55
	        }, 1000);
	        //  },2000)

	        $(e).find('.bt-serv').attr('aria-expanded', true);
	        $(e).find('.content').attr('aria-hidden', false);
	      }
	    }
	  }, {
	    key: '_searchtab',
	    value: function _searchtab(e) {
	      var pageTitle = "";
	      $('#vehicles-grid').html("");

				var titleEl = $('.seo-vo');
				if (titleEl.length == 0) {
					var titleEl = $('.seo-concession');
				}

	      if (e.target.attributes['aria-controls'].nodeValue == "search-tab-2") {
	        $('.wp-st .carousel-container').hide();

	        isOccasion = false;
	        pageTitle = titleEl.attr('data-alt-title');
	      } else {
	        $('.wp-st .carousel-container').show();

	        isOccasion = true;
	        pageTitle = titleEl.attr('data-title');
	      }

	      document.title = pageTitle;
	    }
	  }, {
	    key: 'openTab',
	    value: function openTab(e) {
	      e.preventDefault();
	      var pageTitle = "";

	      $('header .bt-menu').trigger('click');

	      switch (e.target.classList.value) {
	        case "occasion-tab":
	          toggle._toggleHandler($("#search-tabs button[aria-controls = 'search-tab-1']")[0]);
	          $('.wp-st .carousel-container').show();
	          isOccasion = true;
	          pageTitle = $('.seo-vo').attr('data-title');
	          break;

	        case "concession-tab":
	          toggle._toggleHandler($("#search-tabs button[aria-controls = 'search-tab-2']")[0]);
	          $('.wp-st .carousel-container').hide();
	          isOccasion = false;
	          pageTitle = $('.seo-vo').attr('data-alt-title');
	          break;
	      }

	      document.title = pageTitle;
	    }
	  }, {
	    key: '_resetForm',
	    value: function _resetForm(e) {
	      e.preventDefault();
	      // $('.reset-search').hide()

	      // console.log(range)

	      range.resetSlider();

	      if ($('#location-vehicle').prop("selectedIndex") > 0) {
	        $('#search-tab-1 select option:first').prop('selected', true);
	        toggle._toggleHandler($('#location-vehicle')[0]);
	      }

	      if ($('#location-retailer').prop("selectedIndex") > 0) {
	        $('#search-tab-2 select option:first').prop('selected', true);
	        toggle._toggleHandler($('#location-retailer')[0]);
	      }

	      $('.macro-search form').each(function () {
	        $(this)[0].reset();
	      });

	      $('.macro-search form select option:first').prop('selected', true);
	      $('.macro-search form select').each(function () {

	        select.change($(this)[0]);

	        if (isOccasion) {
	          _this._submit('', $('#search-tab-1')[0]);
	        } else {
	          _this._submit('', $('#search-tab-2')[0]);
	        }
	      });
	    }
	  }, {
	    key: 'printPage',
	    value: function printPage() {
	      window.print();
	    }
	  }, {
	    key: 'showResetBtn',
	    value: function showResetBtn() {
	      $('.portail .reset-search').addClass('active');
	    }
	  }, {
	    key: '_popinVideo',
	    value: function _popinVideo(e) {

	      var trigger = e.currentTarget,
	          video = window.config.conf.templates.video.replace('${src}', trigger.dataset.video);

	      document.body.insertAdjacentHTML('beforeend', '<div class="popin"><button class="bt-close" data-close></button><div class="video-wp">' + video + '</div></div>');

	      document.querySelector('.popin').addEventListener('click', function (e) {
	        if (!e.currentTarget.closest('[class*="-wp"]')) {
	          var popin = e.currentTarget.closest('.popin');
	          popin.parentNode.removeChild(popin);
	        }
	      });
	    }
	  }, {
	    key: '_storageHandler',
	    value: function _storageHandler(e) {

	      //const trigger = e.currentTarget,
	      // const trigger = document.getElementById('cookies'),
	      //data = trigger.dataset.storage;
	      var data = "legalAccepted=true";

	      console.log(data);
	      document.cookie += data;

	      console.log("cookie");
	    }
	  }, {
	    key: '_fixHeader',
	    value: function _fixHeader() {

	      logoPosition = $('#cookies').innerHeight();
	      var menu;

	      if ($('body').hasClass('portail')) {
	        menu = document.querySelector('header[role="banner"].fixable');
	      } else {
	        menu = document.querySelector('header[role="banner"].fixable.header-desktop');
	      }

	      var sticky = !!window.CSS && window.CSS.supports('position', 'sticky');

	      if (!menu || window.istouch) {
	        return;
	      }

	      var isAdded = false,
	          menuPosition = menu.getBoundingClientRect();

	      window.addEventListener('scroll', function () {

	        logoPosition = $('#cookies').innerHeight();
	        headerHeight = $('.portail header').innerHeight();
	        menuPosition = menu.getBoundingClientRect();
	        if (0 >= menuPosition.top && !isAdded) {
	          if (!sticky) {
	            menu.style.height = menu.offsetHeight + 'px';
	            menu.classList.add('fixed');
	            $('retailer-infos').addClass('fixed');
	          }
	          menu.classList.add('fix');
	          $('.portail header').removeClass('fix');
	          $('.retailer-wrap').addClass('fix');
	          $('.retailer-infos').addClass('fix');
	          isAdded = true;
	        } else if (0 < menuPosition.top && isAdded) {
	          if (!sticky) {
	            menu.style.height = '';
	            menu.classList.remove('fixed');
	            $('.retailer-infos').removeClass('fix');
	          }

	          $('.retailer-wrap').removeClass('fix');
	          menu.classList.remove('fix');
	          $('.retailer-infos').removeClass('fix');
	          isAdded = false;
	        }

	        if (window.pageYOffset > logoPosition) {
	          $('.main-logo.desktop').css({ 'position': 'fixed' });
	          $('.retailer-infos .wp img').css({ 'position': 'fixed' });
	          $('.portail header').css({ 'position': 'fixed' });
	          $('.portail .hero').css({ 'marginTop': headerHeight + 'px' });

	          $('header').addClass('mob-fixed');
	          $('.retailer-wrap').addClass('retailer-mob-fix');
	        } else {
	          $('.main-logo.desktop').css({ 'position': 'absolute' });
	          $('.retailer-infos .wp img').css({ 'position': 'absolute' });

	          if (document.querySelector('#cookies') != null) {
	            if (document.querySelector('#cookies').getAttribute("aria-hidden") == "true") {
	              $('.main-logo.desktop').css({ 'position': 'fixed' });
	              $('.retailer-infos .wp img').css({ 'position': 'fixed' });
	              $('.portail header').css({ 'position': 'fixed' });
	              $('.portail .hero').css({ 'marginTop': headerHeight + 'px' });
	              $('header').addClass('mob-fixed');
	              $('.retailer-wrap').addClass('retailer-mob-fix');
	            } else {
	              $('.portail header').css({ 'position': 'relative' });
	              $('.portail .hero').css({ 'marginTop': '0' });
	              $('header').removeClass('mob-fixed');
	              $('.retailer-wrap').removeClass('retailer-mob-fix');
	            }
	          } else {
	            $('.portail header').css({ 'position': 'fixed' });
	          }
	        }

	        if ($(window).innerWidth() >= 1024) {
	          if ($('.retailer-infos').hasClass('fix')) {
	            var retailerLogoHeightFix = $('.retailer-infos.fix .wp img').innerHeight();
	            var retailerPositionfix = (55 - retailerLogoHeightFix) / 2;
	            $('.retailer-infos.fix .wp img').css({ 'top': retailerPositionfix + "px" });
	          } else {
	            var retailerLogoHeight = $('.retailer-infos .wp img').innerHeight();
	            var retailerPosition = (92 - retailerLogoHeight) / 2;
	            $('.retailer-infos .wp img').css({ 'top': retailerPosition + "px" });
	          }
	        }
	      });
	    }
	  }, {
	    key: '_load',
	    value: function _load(e) {
	      var _this2 = this;

	      e.preventDefault();

	      var trigger = e.currentTarget,
	          mode = trigger.dataset.loadMore ? 'loadMore' : 'load';

	      var data = JSON.parse(trigger.dataset[mode]);

	      data.params = data.params || {};

	      // const target = document.querySelector('[data-load-content="'+ data.target +'"');
	      var target = document.querySelectorAll('[data-load-content="' + data.target + '"]')[0];

	      if (data.target == "product") {

	        // document.querySelectorAll('.block-product').classList.add('active')
	        $('.block-product').each(function () {
	          $(this).removeClass("active");
	        });

	        e.currentTarget.parentNode.classList.add('active');
	      }

	      if (trigger.form || trigger.hasAttribute('action')) {
	        var elt = trigger.hasAttribute('action') ? trigger : trigger.form;
	        data.params = Object.assign(formUtils.toJSON(elt), data.params);
	      }

	      if (trigger.dataset.params) {
	        data.params = Object.assign(JSON.parse(trigger.dataset.params), data.params);
	      }

	      for (var key in data.params) {
	        if (data.params[key].length == 0) {
	          delete data.params[key];
	        }
	      }

	      data.url = trigger.closest('form') ? trigger.closest('form').action : data.url || trigger.href;

	      target.classList.add('loading');

	      fetcher.get(data.url, { data: data.params }).then(function (body) {

	        setTimeout(function () {
	          new LazyLoad();console.log('lazy load');
	        }, 1000);

	        if (mode == 'load') {
	          target.innerHTML = body.data.html;
	        } else {
	          target.insertAdjacentHTML('beforeend', body.data.html);
	        }

	        if (body.data.endReached) {
	          trigger.classList.add('disabled');
	        }
	        if (data.slider) {
	          var sliderParams = JSON.parse(target.querySelector('[data-sliderconfig]').dataset.sliderconfig);

	          if (sliderParams.slidesPerView < target.querySelectorAll('.swiper-slide').length) {
	            sliders.init(target);
	            target.classList.remove('unslidered');
	          } else {
	            target.classList.add('unslidered');
	          }
	        }
	        if (body.data.params) {
	          trigger.dataset.params = body.data.params;
	        }
	        if (data.scroll) {
	          _this2._scroll({ href: '#' + target.id });
	        }
	        target.classList.remove('loading');
	      });
	    }
	  }, {
	    key: '_submit',
	    value: function _submit(e, elt) {

	      //  console.log("submit:",e);
	      // console.log(elt)
	      if (e.target && e.target.dataset.populate) {
	        return;
	      }

	      window.requestAnimationFrame(function () {

	        // do not submit if data-nosubmit option (case of location) -> triggered by this.locate
	        if (e.target && e.target.selectedIndex && e.target.querySelectorAll('option')[e.target.selectedIndex].hasAttribute('data-nosubmit')) {
	          return;
	        }

	        var trigger = elt || e.currentTarget;

	        var data = trigger.dataset.submit ? JSON.parse(trigger.dataset.submit) : null,
	            params = formUtils.toJSON(trigger);

	        data.url = data.url ? data.url : trigger.action;

	        for (var key in params) {
	          if (params[key].length == 0) {
	            delete params[key];
	          }
	        }

	        var request = fetcher.get(data.url, { data: params });

	        if (data.updateText) {

	          request.then(function (body) {

	            document.qsa('[data-text-update="' + data.updateText + '"]').forEach(function (updateElt) {
	              var bt = updateElt.closest('button');

	              if (body.data.results == 0) {
	                updateElt.parentNode.innerHTML = "Aucun résultat<span data-text-update=" + data.updateText + "></span>";

	                //"Aucun résultat";
	              } else {
	                if (isOccasion) {
	                  updateElt.parentNode.innerHTML = "Voir le(s) <span data-text-update=" + data.updateText + ">" + body.data.results + "</span> véhicules(s)";
	                } else {
	                  updateElt.parentNode.innerHTML = "Voir le(s) <span data-text-update=" + data.updateText + ">" + body.data.results + "</span> concession(s)";
	                }
	              }
	              if (body.data.html) {
	                document.querySelector('[data-load-content="' + JSON.parse(trigger.dataset.load).target + '"]').innerHTML = body.data.html;
	                if (body.data.results == 0 && bt) {
	                  bt.classList.add('no-results');
	                  bt.setAttribute('disabled', true);
	                }
	              } else if (bt) {
	                bt.classList.remove('no-results');
	                bt.removeAttribute('disabled');
	              }
	            });
	          });
	        }
	      });
	    }
	  }]);

	  return Misc;
	}();

	var misc = new Misc();

	var _createClass$4 = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck$4(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	/**
	 * Toggle
	 * @constructor
	 */

	var Toggle = function () {
	  /**
	   * Inits the module with necessary functions
	   * @method
	   **/
	  function Toggle() {
	    _classCallCheck$4(this, Toggle);

	    this._toggleHandler = this._toggleHandler.bind(this);
	    this.closeAll = this.closeAll.bind(this);
	    this.close = this.close.bind(this);
	    this.open = this.open.bind(this);

	    this._bind();
	  }

	  _createClass$4(Toggle, [{
	    key: "_bind",
	    value: function _bind() {
	      document.body.listen("click", "[data-toggle]", this._toggleHandler);
	      document.body.listen("change", "select[data-toggler]", this._toggleHandler);
	    }
	  }, {
	    key: "_toggleHandler",
	    value: function _toggleHandler(e) {
	      // const trigger = e.currentTarget,


	      var trigger = e.currentTarget ? e.currentTarget : e,
	          toggler = trigger.hasAttribute("data-toggler");

	      var data = {
	        id: toggler ? trigger.querySelectorAll("option")[trigger.selectedIndex].getAttribute("aria-controls") : trigger.getAttribute("aria-controls")
	      };

	      try {
	        var attr = toggler ? JSON.parse(trigger.dataset["toggler"]) : JSON.parse(trigger.dataset["toggle"]);data = Object.assign(attr, data);
	      } catch (e) {}

	      if (toggler && trigger.querySelectorAll("option")[trigger.selectedIndex].getAttribute("aria-controls") === "reset") {
	        data.id = trigger.closest("[data-tabs]").querySelector('[aria-hidden="false"]').id;
	        this.close(data);
	        return;
	      }

	      if (toggler && trigger.querySelectorAll("option")[trigger.selectedIndex].hasAttribute("data-locate")) {
	        misc.locate(trigger.form);
	      }

	      if (!toggler && trigger.getAttribute("aria-expanded") == "false" || toggler && trigger.querySelectorAll("option")[trigger.selectedIndex].getAttribute("aria-expanded") == "false") {
	        this.open(data);
	      } else if (!data.tabs || data.tabs && trigger.getAttribute("aria-expanded") == "false") {
	        this.close(data);
	      }
	    }
	  }, {
	    key: "open",
	    value: function open(data) {
	      var triggers = document.qsa('[aria-controls="' + data.id + '"]'),
	          targets = document.qsa('[data-toggle-targets="' + data.id + '"]').length > 0 ? document.qsa('[data-toggle-targets="' + data.id + '"]') : [document.getElementById(data.id)],
	          otherTab = data.tabs ? triggers[0].closest("[data-tabs]").querySelector('[aria-expanded="true"]') : null;

	      targets.forEach(function (target) {
	        target.setAttribute("aria-hidden", false);
	        target.classList.remove("toggle-on");

	        if (data.slider) {
	          sliders.init(target);
	        }
	        if (target.querySelector('[data-inactive]')) {
	          target.qsa('[data-inactive]').forEach(function (input) {
	            input.disabled = false;
	          });
	        }
	      });
	      triggers.forEach(function (trigger) {
	        trigger.setAttribute("aria-expanded", true);
	      });

	      if (data.body) {
	        document.documentElement.classList.add(data.body);
	      }

	      if (data.tabs && otherTab) {
	        this.close({ id: otherTab.getAttribute("aria-controls") });
	      } else if (!data.tabs && !data.keep) {
	        document.body.addEventListener("click", this.closeAll);
	      }
	      // document.body.addEventListener("click", this.closeAll);
	      console.log(data);

	      if (document.querySelector('#cookies') != null) {
	        if (document.querySelector('#cookies').getAttribute("aria-hidden") == "false") {
	          $('.header-mobile').css({ 'position': 'fixed' });

	          if ($('body').hasClass('portail')) {
	            $('header').css({ 'position': 'fixed' });
	          }
	        }
	      }
	    }
	  }, {
	    key: "close",
	    value: function close(data) {
	      var triggers = document.qsa('[aria-controls="' + data.id + '"]'),
	          targets = document.qsa('[data-toggle-targets="' + data.id + '"]').length > 0 ? document.qsa('[data-toggle-targets="' + data.id + '"]') : [document.getElementById(data.id)];

	      console.log(triggers);
	      targets.forEach(function (target) {
	        target.setAttribute("aria-hidden", true);
	        target.classList.remove("toggle-on");
	        if (target.querySelector('[data-inactive]')) {
	          target.qsa('[data-inactive]').forEach(function (input) {
	            input.disabled = true;
	          });
	        }
	      });
	      triggers.forEach(function (trigger) {
	        trigger.setAttribute("aria-expanded", false);
	      });

	      document.body.removeEventListener("click", this.closeAll);

	      if (data.body) {
	        document.documentElement.classList.remove(data.body);
	      }
	      if (document.querySelector('#cookies') != null) {
	        if (document.querySelector('#cookies').getAttribute("aria-hidden") == "true") {
	          console.log("cookie closed");
	          var headerHeight = $('.portail header').innerHeight();
	          // document.body.classList.remove('cookie-open')
	          $('.desktop.main-logo').css({ 'position': 'fixed' });
	          $('.portail header').css({ 'position': 'fixed', 'top': 0 });
	          $('.portail .hero').css({ 'marginTop': headerHeight + 'px' });
	        } else {
	          $('.header-mobile').css({ 'position': 'relative' });

	          if ($('body').hasClass('portail')) {
	            $('header').css({ 'position': 'relative' });
	          }
	        }
	      }
	    }
	  }, {
	    key: "closeAll",
	    value: function closeAll(e) {
	      var _this = this;

	      if (e.target.closest('[aria-hidden="false"]')) {
	        return;
	      }

	      var triggers = document.qsa('[aria-expanded="true"]');

	      triggers.forEach(function (trigger) {
	        var data = {
	          id: trigger.getAttribute("aria-controls")
	        };

	        try {
	          data = Object.assign(JSON.parse(trigger.dataset["toggle"]), data);
	        } catch (e) {}

	        if (data.tabs) {
	          return;
	        }

	        _this.close(data);
	        document.body.removeEventListener("click", _this.closeAll);
	      });
	    }
	  }]);

	  return Toggle;
	}();

	var toggle = new Toggle();

	var _createClass$5 = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck$5(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	/**
	 * Gallery
	 * @constructor
	 */

	var Gallery = function () {
	  function Gallery() {
	    _classCallCheck$5(this, Gallery);

	    this.bind = this.bind.bind(this);
	    this.activate = this.activate.bind(this);
	    this.nav = this.nav.bind(this);

	    this.bind();
	  }

	  _createClass$5(Gallery, [{
	    key: 'bind',
	    value: function bind() {

	      document.body.listen('click', '[data-gallery] [data-thumb]:not(.active)', this.activate);
	      document.body.listen('click', '[data-gallery] [data-controls]', this.nav);
	    }
	  }, {
	    key: 'nav',
	    value: function nav(e) {

	      var trigger = e.currentTarget,
	          dir = trigger.dataset.controls == 'prev' ? -1 : 1,
	          controls = trigger.closest('[data-gallery]').querySelector('.thumbnails'),
	          active = controls.querySelector('[data-thumb].active').parentNode;

	      console.log(active, dir);

	      if (dir < 0) {
	        if (active.previousElementSibling) {
	          console.log("1a");
	          active.previousElementSibling.querySelector('button').click();
	        } else {
	          console.log("2a");
	          controls.querySelector('li:last-child button').click();
	        }
	      }

	      if (dir > 0) {
	        if (active.nextElementSibling) {
	          console.log("1b");
	          active.nextElementSibling.querySelector('button').click();
	        } else {
	          console.log("2b");
	          controls.querySelector('li:first-child button').click();
	        }
	      }
	    }
	  }, {
	    key: 'activate',
	    value: function activate(e) {

	      e.preventDefault();

	      var trigger = e.currentTarget,
	          gallery = trigger.closest('[data-gallery]');

	      var viewer = gallery.querySelector('[data-viewer]'),
	          newViewer = viewer.cloneNode();

	      var container = viewer.parentNode,
	          video = container.querySelector('iframe');

	      if (container.classList.contains('show')) {
	        return;
	      }

	      // handle trigger button state
	      gallery.querySelector('[data-thumb].active').classList.remove('active');
	      trigger.classList.add('active');

	      // handle content
	      if (trigger.dataset.video) {
	        if (video) {
	          video.src = trigger.dataset.video;
	        } else {
	          container.insertAdjacentHTML('beforeend', window.config.conf.templates.video.replace('${src}', trigger.dataset.video));
	        }
	        return;
	      }

	      if (video) {
	        container.removeChild(viewer);
	        // remove video when done
	        viewer = video;
	      }

	      newViewer.src = trigger.querySelector('img').src;
	      newViewer.srcset = trigger.querySelector('img').srcset;

	      container.classList.add('show');
	      container.appendChild(newViewer);

	      viewer.addEventListener('transitionend', function () {
	        container.removeChild(viewer);
	        container.classList.remove('show');
	      }, { once: true });
	    }
	  }]);

	  return Gallery;
	}();

	new Gallery();

	// polyfills

	smoothscroll.polyfill();

	window.locale = navigator.languages ? navigator.languages[0] : navigator.language ? navigator.language : document.documentElement.dataset.locale;

	// Trigger app start custom event
	pubsub.publish('app.start');

	new LazyLoad();

	$('.carousel-cinquo').each(function () {

		console.log($(this).find('.swiper-slide').length);

		if ($(this).find('.swiper-slide').length == 1) {
			$(this).parent().find('.custom-button').addClass('hide-arrow');
			$(this).parent().find('.swiper-pagination').hide();
		}
		// console.log($(this).length)
	});

}());
//# sourceMappingURL=app.js.map
