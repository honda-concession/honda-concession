<?php

namespace Application\Sonata\PageBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

use Application\Sonata\PageBundle\Entity\Page;
use Honda\Bundle\CommonBundle\Entity\SeoContent;

class SeoOverrideEventListener
{
    private $container;

    /**
     * set service container
     *
     * @param mixed $container
     * @return self
     */
    public function setContainer($container)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Page) {
            $entityManager = $this->container->get('doctrine.orm.entity_manager');
            $repository = $entityManager->getRepository(SeoContent::class);
            $seoContent = null;
            if ($entity->getName() == 'Accueil') {
                $seoContent = $repository->findOneByType('Homepage');
            }
            if ($entity->getName() == 'concessionnaire') {
                $seoContent = $repository->findOneByType('Homepage / Concession');
            }
            if ($seoContent) {
                $entity->setTitle($seoContent->getTitle());
                $entity->setMetaDescription($seoContent->getMetaDescription());
                $entity->setMetaKeyword($seoContent->getMetaKeywords());
            }
        }
    }
}
