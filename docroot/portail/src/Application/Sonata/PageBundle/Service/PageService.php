<?php

namespace Application\Sonata\PageBundle\Service;

use Sonata\PageBundle\Page\Service\DefaultPageService as BasePageService;
use Sonata\PageBundle\Model\PageInterface;

class PageService extends BasePageService
{

    /**
     * @inheritDoc
     */
    protected function updateSeoPage(PageInterface $page)
    {
        if (!$this->seoPage) {
            return;
        }
        if ($page->getTitle()) {
            $this->seoPage->addTitle($page->getTitle());
        }

        if ($page->getMetaDescription()) {
            $this->seoPage->addMeta('name', 'description', $page->getMetaDescription());
        }

        if ($page->getMetaKeyword()) {
            $this->seoPage->addMeta('name', 'keywords', $page->getMetaKeyword());
        }

        $this->seoPage->addMeta('property', 'og:type', 'article');
        $this->seoPage->addHtmlAttributes('prefix', 'og: http://ogp.me/ns#');
    }
}
