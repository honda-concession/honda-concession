<?php

namespace Application\Sonata\MediaBundle\Provider;

use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\FileProvider as BaseFileProvider;

class FileProvider extends BaseFileProvider
{
    /**
     * @inheritDoc
     */
    protected function generateMediaUniqId(MediaInterface $media)
    {
        $name = $media->getName();
        if (substr($media->getName(), -3) == 'pdf') {
            return uniqid().'_'.substr($media->getName(), 0, strlen($name) - 4);
        }

        return parent::generateMediaUniqId($media);
    }
}
