<?php

namespace Application\Sonata\MediaBundle\Provider;

use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\YouTubeProvider as BaseVideoProvider;
use Sonata\MediaBundle\Provider\MediaProviderInterface;

class YouTubeProvider extends BaseVideoProvider
{
    /**
     * @inheritDoc
     */
    protected function fixBinaryContent(MediaInterface $media)
    {
        if (!$media->getBinaryContent()) {
            return;
        }

        if (11 === \strlen($media->getBinaryContent())) {
            return;
        }

        if (preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\#\?&\"'>]+(\?(((rel|autoplay)\=(1|0))|&)+)?)/", $media->getBinaryContent(), $matches)) {
            $media->setBinaryContent($matches[1]);
        }
    }

    /**
     * @inheritDoc
     */
    public function getReferenceUrl(MediaInterface $media)
    {
        $reference = $media->getProviderReference();
        $reference = preg_replace('/\?.*$/', null, $reference);

        return sprintf('https://www.youtube.com/watch?v=%s', $reference);
    }

    /**
     * {@inheritdoc}
     */
    public function getReferenceFile(MediaInterface $media)
    {
        $key = $this->generatePrivateUrl($media, MediaProviderInterface::FORMAT_REFERENCE);
        $referenceFile = $this->getFilesystem()->get($key, true);
        $metadata = $this->metadata ? $this->metadata->get($media, $referenceFile->getName()) : [];
        $referenceFile->setContent($this->browser->get($this->getReferenceImage($media))->getContent(), $metadata);


        return $referenceFile;
    }
}
