<?php

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read https://symfony.com/doc/current/setup.html#checking-symfony-application-configuration-and-setup
// for more information
//umask(0000);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || (isset($_SERVER['HTTP_X_FORWARDED_FOR']) &&  !(in_array($_SERVER['HTTP_X_FORWARDED_FOR'], ['172.19.0.1', '172.18.0.1'], true)))
    || !(in_array(@$_SERVER['REMOTE_ADDR'], ['78.231.223.49', '172.19.0.12', '127.0.0.1', '172.18.0.4', '172.18.0.1', '172.18.0.2', '172.18.0.3', '172.18.0.5', '172.19.0.2', '172.19.0.5', '172.19.0.4', '172.19.0.3', '172.19.0.11', '172.19.0.8', '172.19.0.7', '::1'], true) || PHP_SAPI === 'cli-server')
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

//require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../sites/autoload/sites.php';
Debug::enable();

$kernel = new MultisiteKernel('dev', true);
$kernel->setSite($site);

if (PHP_VERSION_ID < 70000) {
    $kernel->loadClassCache();
}
$request = Request::createFromGlobals();

$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
