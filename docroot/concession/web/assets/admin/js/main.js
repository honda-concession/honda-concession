function setValueFromPortail(value)
{
    var url = window.portailApiUrl + '/vo/details/model/' + value;
    $.ajax({
        url: url,
        method: 'GET',
    }).done(function(response){
        console.log(response);
        var elCylinder = $("input[id$='_cylinder']");
        var elType = $("select[id$='_categoryPortail']");
        var match = document.URL.match('create$');
        //if (elCylinder.val().length == 0 || (match && match.length > 0)) {
            elCylinder.val(response.cylinder);
        //}
        //if (elType.val().length == 0 || (match && match.length > 0)) {
            elType.val(response.category).trigger('change');
        //}
    });
}

(function($) {
    $(function() {
        $('a.btn-danger').click(function(){
            $(window).unbind('beforeunload');
        });
    });
})(jQuery);

(function($) {

    $(function() {
        $('.model-vo').parent().parent().append('<a class="model-creation-btn"  href="/admin/support/list">Démander la création d’un modèle </a>');
    });

})(jQuery);



(function($) {

    $(function() {

        var listes = $('.services-picto li span.control-label__text');

        $.each(listes, function() {
            var tmp = $(this).html().trim();
            var img = $('<img/>');
            var label = $(this).closest('label').removeClass('required');

            img.attr('src', tmp);
            img.attr('width', 50);
            $(this).html(img);

        });

    });

})(jQuery);


// Checkbox image pincipale
(function($) {

    $(function() {

        var checkboxes = $('.galleriesVo').find('input.galleryVo');

        checkboxes.click(function(e) {

            var self = $(this),
                parent = self.parent();

            if (parent.hasClass("checked")) {

                $.each(checkboxes, function() {
                    $(this)[0].checked = false;
                    $(this).parent().removeClass('checked');
                });

                self[0].checked = true;
                parent.addClass('checked');

                console.log('checked');
            }
        });


        // Paraille que le precedent mais cette fois çi pour les labels
        var checkboxesLabels = $('.galleriesVo').find('label.labelGalleryVo');
        checkboxesLabels.click(function(e) {

            var self = $(this).find('input[type=checkbox]'),
                parent = self.parent();

            if (parent.hasClass("checked")) {

                $.each(checkboxesLabels.find('input[type=checkbox]'), function() {
                    $(this)[0].checked = false;
                    $(this).parent().removeClass('checked');
                });

                self[0].checked = true;
                parent.addClass('checked');

                console.log('checked');
            }
        });

    });

})(jQuery);



(function($) {

    $(function() {

        $('.iCheck-helper').click(function() {
            var self = $(this);

            self.prev().trigger('click');
        });

        $('.inventory-checkbox').click(function() {

            var self = $(this),
                fields = {},
                checkValue = 1;

            // TODO: à modifier j'ai inversé les valeurs lorsque le checkox est checker je met à 0 et non 1,
            if (self.is(":checked")) {
                checkValue = 0;
            }

            fields = {'inStock': checkValue};

            var req = $.ajax({
                url: self.data('url'),
                method: 'POST',
                data: fields,
               /* beforeSend: function () {
                    self.parent().append($(spinContent).show());
                }*/
            });

            req.done(function(res) {
                var span = self.parents().closest('td.sonata-ba-list-field').prev().find('span');

                if (1 === checkValue) {
                    span.removeClass('label-danger').addClass('label-success').html('oui');
                }

                if (0 === checkValue) {
                    span.addClass('label-danger').removeClass('label-success').html('non');
                }
            });

            req.always(function() {
                //self.parent().find('.spin').remove();
            });

            console.log('event');
        });
    });

})(jQuery);


(function($) {

    $(function() {
        $('.youtube-link').parents().closest('.form-group').prepend('<p>Ou</p>');
        $('.video-link').parents().closest('.form-group').prepend('<p>Ou</p>');
    });

})(jQuery);


(function($) {

    $(function() {

        $('.onTest-checkbox').click(function() {

            var self = $(this),
                fields = {},
                checkValue = 1;

            // TODO: à modifier j'ai inversé les valeurs lorsque le checkox est checker je met à 0 et non 1,
            if (self.is(":checked")) {
                checkValue = 0;
            }

            fields = {'onTest': checkValue};

            var req = $.ajax({
                url: self.data('url'),
                method: 'POST',
                data: fields,
                /* beforeSend: function () {
                     self.parent().append($(spinContent).show());
                 }*/
            });

            req.done(function(res) {

                var span = self.parents().closest('td.sonata-ba-list-field').prev().find('span');

                if (1 === checkValue) {
                    span.removeClass('label-danger').addClass('label-success').html('oui');
                }

                if (0 === checkValue) {
                    span.addClass('label-danger').removeClass('label-success').html('non');
                }

            });

            req.always(function() {
                //self.parent().find('.spin').remove();
            });

            console.log('event');
        });
    });

})(jQuery);


(function($) {

    $(function() {

        var sortElt = $("#section-sortable");
        var url = sortElt.data('sort-url');

        $("#section-sortable").sortable({
            axis: 'y',
            cursor: "move",
            update: function() {
                var data = $("#section-sortable").sortable("serialize");


                $.ajax({
                    data: data,
                    type: 'POST',
                    url: url
                });

                console.log(data);

            }
        });
       // $( "#section-sortable" ).disableSelection();
    });

})(jQuery);


// Cascading select mark -> models
(function($) {

    $(function() {


        var populateModelSelect = function(modelSelect, markName, url, modelSavedInBdd) {

            var endPoint = url + '/'+ markName;
            var modelSavedInBdd = modelSavedInBdd || null;

            var req = $.ajax({
                url: endPoint,
                method: 'GET',
                // beforeSend: function () {
                //     self.parent().append($(spinContent).show());
                // }
            });

            req.done(function(res) {

                var options = [];

                modelSelect.empty().removeAttr('disabled');
                var option = $('<option />');
                option.attr({ 'value': '' }).text('Choississez un modèle');
                options.push(option);
                jQuery.each(res, function() {

                    var option = $('<option/>');
                    option.attr({ 'value': this.value }).text(this.label);

                    if (modelSavedInBdd === this.value) {
                        option.attr('selected', 'selected')
                    }

                    options.push(option);
                });

                modelSelect.html(options);
                modelSelect.change(function(e){
                    var value = e.val;
                    setValueFromPortail(value);
                    console.log('change model trigger');
                });
                //modelSelect.val(modelSavedInBdd).trigger('change');
                //modelSelect.change();
            });

            return req;
        };


        $('.marksVo').change(function (item) {

            var self = $(this),
                url = self.data('url'),
                modelSelect = $('select.modelsVo'),
                mark = item.val;

            populateModelSelect(modelSelect, mark, url);

            console.log('change mark');
            /*
            req.always(function() {
                self.parent().find('.spin').remove();
            });
            */
        });


        var markSelect = $('select.marksVo'),
            modelSelect = $('select.modelsVo'),
            selectedMark = markSelect.find(':selected'),
            url = markSelect.data('url'),
            markName = selectedMark.val(),
            modelSavedInBdd = modelSelect.data('modelportail');

        if (markName) {
            populateModelSelect(modelSelect, markName, url, modelSavedInBdd);
        }

    });

})(jQuery);

(function($) {
    $(function() {
        setValueFromPortail($('select.modelsVo').data('modelportail'));
    });
})(jQuery);

(function($) {

    var $collectionHolder;

    var $addButton = $('<button type="button" class="add_link btn btn-success">Ajouter un contenu</button>');
    var $newLinkLi = $('<li></li>').append($addButton);


    jQuery(document).ready(function() {

        $collectionHolder = $('ul.slide');

        $collectionHolder.find('li').each(function() {
            addFormDeleteLink($(this));
        });

        $collectionHolder.append($newLinkLi);
        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addButton.on('click', function(e) {
            addForm($collectionHolder, $newLinkLi);
        });

        $('.image-field .form-control-file').on('change', function(event){
            $(this).after('<p><strong>'+ $(this).val().split('\\').pop() +'</strong></p>');
        });

    });

    function addForm($collectionHolder, $newLinkLi) {
        var prototype = $collectionHolder.data('prototype');

        var index = $collectionHolder.data('index');

        var newForm = prototype;
        newForm = newForm.replace(/__name__/g, index);

        $collectionHolder.data('index', index + 1);

        var $newFormLi = $('<li></li>').append(newForm);
        $newLinkLi.before($newFormLi);

        addFormDeleteLink($newFormLi);

        $('.image-field .form-control-file', $newFormLi).on('change', function(event){
            $(this).after('<p><strong>'+ $(this).val().split('\\').pop() +'</strong></p>');
        });

    }

    function addFormDeleteLink($formLi) {
        var $removeFormButton = $('<p class="remove-link-p"><a class="remove_link btn btn-danger btn-xs" href="#">Supprimer</a></p>');
        $formLi.append($removeFormButton);

        $removeFormButton.on('click', function(e) {
            e.preventDefault();
            $formLi.remove();
        });
    }

})(jQuery);
