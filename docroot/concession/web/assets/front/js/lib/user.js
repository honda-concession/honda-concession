class User
{
    constructor(firstname, lastname){
        this.firstname = firstname;
        this.lastname = lastname;
    }

    sayName() {
        return `${this.firstname} ${this.lastname}`;
    }

}

export default User;
