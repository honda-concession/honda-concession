import HMotoForm from 'h-moto-form';
import {Provenance, URL} from 'h-moto-form/dist/SHonda.umd';
import Vue from 'vue';


let vm = new Vue({
    delimiters: ['${', '}'],
    el: '#app-form',
    data: function() {
        return {
            provenance: 'DEFAULT',
        }
    },
    methods: {
        setProvenance: function(provenance) {
            this.provenance = provenance
        },
        getProvenance: function() {
            return this.provenance
        }
    },
    components: {
       HMotoForm
    }
});


window.appHondaVue = vm.$children[0] || null;
window.appHondaVueObj = vm;
