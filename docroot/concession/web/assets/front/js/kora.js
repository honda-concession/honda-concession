function setFormValue(koraIdxForTest, koraIdxForDispoStock) {
    console.log('id pour le test ' + koraIdxForTest.length);
    if (koraIdxForTest.length) {
        console.log('SetDispoEssai', koraIdxForTest);
        appHondaVue.SetDispoEssai(koraIdxForTest);
    }

    console.log('id pour le stock ' + koraIdxForDispoStock.length);
    if (koraIdxForDispoStock.length) {
        console.log('SetDispoStock', koraIdxForDispoStock);
        appHondaVue.SetDispoStock(koraIdxForDispoStock);
    }
}

(function($) {

    $(function() {
        var elKora = $('#kora');
        var origin = elKora.data('origin');
        var koraIdxForTest = elKora.data('koraidxfortest');
        var koraIdxForDispoStock = elKora.data('koraidxforsetdispostock');
        var value = 3
        if ((appHondaVueObj !== undefined) && (appHondaVueObj !== null)) {
            if (origin == 'VN') {
                setFormValue(koraIdxForTest, koraIdxForDispoStock);
            }
            if (origin == 'VO') {
                setTimeout(function(){
                    appHondaVue.SetCurrentOccasion($('.product-specs').data('specification'));
                }, 500);
            }
            appHondaVueObj.setProvenance(origin);
            console.log(appHondaVueObj.getProvenance());
        }
        $('.std-field #object.bt-locate').change(function(e) {
            if (e.currentTarget.selectedIndex == value) {
                setFormValue(koraIdxForTest, koraIdxForDispoStock);
            }
        });
        $('a.dispo-essai').click(function(){
            setFormValue(koraIdxForTest, koraIdxForDispoStock);
        });
    });


})(jQuery);

(function($) {

    $(function() {

        $('.bt-contact').click(function() {

            if ((appHondaVueObj !== undefined) && (appHondaVueObj !== null)) {

                appHondaVueObj.setProvenance("VN_STICKY");

                console.log('Click Sticky');
                console.log(appHondaVueObj.getProvenance());
            }

        });

    });

})(jQuery);


(function($) {

    function SetCurrentModeleData(url, appHondaVueObj, action) {

        var req = $.ajax({
            url: url,
            method: 'GET',
            beforeSend: function () {
            }
        });

        req.done(function(res) {

            if ('stock' === action) {
                console.log('SetCurrentModele pour vehicle selectionné en stock ou hors stock');
            } else if ('essai' === action) {
                console.log('SetCurrentModele pour vehicle selectionné en essai');
            }

            console.log('kora Id ----------------' + res.kora_id);

            var model = appHondaVue.GetModeleById(res.kora_id);

            console.log('Données kora----------------');
            console.log(model);
            console.log('Données kora----------------');

            if (model) {
                res.label_public = model.label_public;
                res.label_prive = model.label_prive;
                res.couleurs = model.couleurs;
                res.groupes = model.groupes;
                res.categories = model.categories;
                res.id = model.id;
            }

           // delete res.submodel_id;

            console.log('Données Fusionnées-------------');
            console.log(res);
            console.log('Données Fusionnées-------------');

            appHondaVue.SetCurrentModele(res);
        });
    }

    $(function() {

        // Pour les stocks
        $('.provenance').click(function() {

            history.replaceState(null, null, $(this).data('jsurlreplace'));
            console.log('Update history to' +  $(this).data('jsurlreplace'));

            var self = $(this);

            if ((appHondaVueObj !== undefined) && (appHondaVueObj !== null)) {

                appHondaVueObj.setProvenance($(this).data('provenance'));
                console.log($(this).data('provenance'));

                SetCurrentModeleData(self.data('vehicleslectionnerurl'), appHondaVueObj, 'stock');
            }
        });

        // Pour les Essais
        $('.dispo-essai').click(function(e) {

            e.preventDefault();

            var self = $(this);

            if ((appHondaVue !== undefined) && (appHondaVue !== null)) {

                appHondaVueObj.setProvenance($(this).data('provenance'));
                console.log(self.data('provenance'));

                SetCurrentModeleData(self.data('url'), appHondaVueObj, 'essai');
            }
        });

    });

})(jQuery);


