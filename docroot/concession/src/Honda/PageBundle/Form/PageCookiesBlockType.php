<?php

namespace Honda\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type As Type;

use Honda\PageBundle\Entity\PageCookiesBlock;

class PageCookiesBlockType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('blockType', Type\ChoiceType::class, [
                    'choices' => [
                        PageCookiesBlock::BLOCK_VIE_PRIVE => PageCookiesBlock::BLOCK_VIE_PRIVE,
                        PageCookiesBlock::BLOCK_COOKIES => PageCookiesBlock::BLOCK_COOKIES,
                    ],
                    'label' => 'Type de contenu (Ce contenu  s\'affichera dans le bloc.)',
                ]
            )
            ->add('title')
            ->add('content', Type\TextareaType::class, ['attr' => ['rows' => 5]] )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Honda\PageBundle\Entity\PageCookiesBlock'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_pagebundle_page_cookies_block';
    }


}
