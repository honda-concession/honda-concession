<?php

namespace Honda\PageBundle\Form;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Sonata\MediaBundle\Form\Type\MediaType;


class PageDynamicFieldSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }
    
    public function preSetData(FormEvent $event)
    {
        $page = $event->getData();
        $template = $page->getPageBlock()->getTemplate();
        
        $form = $event->getForm();
    
        $form->remove('image');

        $blockCommonTmps = [];
        
        if ($template === 'honda-label.html.twig') {
    
            $form
                ->add('hondaLabelSlides', CollectionType::class,
                    [
                        'entry_type' => HondaLabelSlideType::class,
                        'entry_options' => array('label' => false),
                        'label' => '',
                        'attr' => array(
                            'class' => 'my-selector',
                        ),
                        'required' => false,
                        'allow_extra_fields' => true,
                        'allow_add'    => true,
                        'allow_delete' => true,
                        'prototype'    => true,
                        'by_reference' => false,
                    ],[]
                )
                ->add('btnLink')
                ->add('btnLabel');
            
        } elseif ($template === 'privacy-and-cookies.html.twig') {
            
            $form
                ->add('pageCookiesBlocks', CollectionType::class,
                    [
                        'entry_type' => PageCookiesBlockType::class,
                        'entry_options' => array('label' => false),
                        'label' => '',
                        'attr' => array(
                            'class' => 'my-selector',
                        ),
                        'required' => false,
                        'allow_extra_fields' => true,
                        'allow_add'    => true,
                        'allow_delete' => true,
                        'prototype'    => true,
                        'by_reference' => false,
                    ],[]
                )
                ->add('description1', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, ['label' => 'Description (Vie privée)', 'required' => false])
                ->add('description2', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, ['label' => 'Description (Cookies)', 'required' => false]);
            
        } elseif ($template === 'banniere-page-label-honda.html.twig' || $template === 'banniere-page-service.html.twig') {
            
            $form
                ->remove('title')
                ->remove('content')
                ->remove('metaDescription')
                ->remove('metaKeywords')
                ;
            
            $form->add('image', MediaType::class, [
                'required' => false,
                'provider' => 'sonata.media.provider.image',
                'context'  => 'page',
                'attr' => ['class' => 'image-field']
            ]);
            
            $form
                ->add('ficheBanniereLink')
                ->add('ficheBanniereExternalLink', null, ['label' => 'External Link'])
                ;
        } elseif (in_array($template, $blockCommonTmps)) {
            
            $form
                ->add('title', null, ['required' => false])
                ->remove('metaDescription')
                ->remove('metaKeywords')
            ;
            
        }
   
    }
}

