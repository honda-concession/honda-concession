<?php

namespace Honda\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type As Type;
use Honda\PageBundle\Form\PageDynamicFieldSubscriber;
use Honda\PageBundle\Validation\ValidationGroupResolver;

/**
 * Class PageType
 * @package Honda\PageBundle\Form
 */
class PageType extends AbstractType
{
    
    
    private $groupResolver;
    
    public function __construct(ValidationGroupResolver $groupResolver)
    {
        $this->groupResolver = $groupResolver;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('content', CKEditorType::class, ['required' => false])
            ->add('metaDescription', Type\TextareaType::class, ['required' => false, 'attr' => ['help' => 'Saisissez ici la description de la page pour le SEO']])
            ->add('metaKeywords', Type\TextareaType::class, ['required' => false, 'attr' => ['help' => 'Saisissez ici les mots pour le réferencement(SEO)']])            
        ;
        
        $builder->addEventSubscriber(new PageDynamicFieldSubscriber());
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Honda\PageBundle\Entity\Page',
            'validation_groups' => $this->groupResolver,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_pagebundle_page';
    }


}
