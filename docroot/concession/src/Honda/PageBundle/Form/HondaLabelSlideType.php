<?php

namespace Honda\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\MediaBundle\Form\Type\MediaType;

class HondaLabelSlideType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', MediaType::class, [
                'label' => '',
                'provider' => 'sonata.media.provider.image',
                'context'  => 'slider_label_honda',
                'attr' => ['class' => 'image-field']
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Honda\PageBundle\Entity\HondaLabelSlide'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_pagebundle_honda_label_slide';
    }


}
