<?php

namespace Honda\PageBundle\Repository;


/**
 * Class PageCookiesBlockRepository
 * @package Honda\PageBundle\Repository
 */
class PageCookiesBlockRepository extends \Doctrine\ORM\EntityRepository
{
    
    /**
     * @param $pageId
     * @param $blockType
     * @return mixed
     */
    public function getCookiesPageBlock($pageId, $blockType)
    {
        $qb =  $this->createQueryBuilder('this');
        $qb
            ->leftJoin('this.page', 'page')
            ->where('page = :page')
            ->andWhere('this.blockType LIKE :blockType')
            ->setParameter('page', $pageId)
            ->setParameter('blockType', $blockType)
        ;
        
        return $qb->getQuery()->getResult();
    }
    
    
}
