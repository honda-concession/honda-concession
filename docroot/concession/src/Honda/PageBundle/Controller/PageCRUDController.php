<?php

namespace Honda\PageBundle\Controller;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Model\Admin\GenericFormHandler;
use Honda\PageBundle\Entity\Page;
use Honda\PageBundle\Entity\PageBlock;
use Honda\PageBundle\Model\Admin\PageManager;
use Honda\PageBundle\Form\PageType;
use Honda\PageBundle\Form\HondaLabelSlideType;
use Honda\PageBundle\Entity\HondaLabelSlide;


class PageCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $pageBlocks = $this->getDoctrine()->getManager()->getRepository(PageBlock::class)->getPageBlocks(PageBlock::TYPE_PAGE);
        
        return $this->renderWithExtraParams('@HondaPage/CRUD/sonata_page_block_list.html.twig', [
            'pageBlocks' => $pageBlocks,
            'contentType' => PageBlock::TYPE_PAGE,
        ]);
    }
    
    /**
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id = null)
    {
        $request = $this->getRequest();
        
        $id = $request->get($this->admin->getIdParameter());
        $pageBlock = $this->admin->getObject($id);
        
        if (!$pageBlock) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id: %s', $id));
        }
    
        $this->admin->checkAccess('edit', $pageBlock);
    
        $page = $this->get(PageManager::class)->getPage($this->getDistributor(), $pageBlock);
        $form = $this->createForm(PageType::class, $page);
        
        if ($this->get(GenericFormHandler::class)->handlerForm($request, $form)) {
            $this->addFlash('sonata_flash_success', 'Le contenu a été correctement mis à jour.');
            return $this->redirectToRoute($this->admin->getBaseRouteName().'_edit', ['id' => $pageBlock->getId()]);
        }
        
        return $this->renderWithExtraParams('@HondaPage/CRUD/sonata_page_block_edit.html.twig', [
            'pageBlock' => $pageBlock,
            'page' => $page,
            'form' => $form->createView(),
        ]);
    }
}
