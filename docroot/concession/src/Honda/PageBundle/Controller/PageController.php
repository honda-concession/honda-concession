<?php

namespace Honda\PageBundle\Controller;

use Honda\MainBundle\Menu\Builder;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Honda\PageBundle\Entity\PageBlock;
use Honda\PageBundle\Model\Admin\PageManager;
use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;
use Honda\MainBundle\Model\Middleware\GenericGateway;

class PageController extends AbstractFrontController
{

    /**
     * redirection url to
     *
     * @Route("/redirect-to/{url}", name="redirect_to")
     */
    public function redirectToAction($url)
    {
        return $this->redirect(urldecode($url));
    }

    /**
     * @Route("/{path}", name="page_show")
     */
    public function showAction($path)
    {
        $distributor = $this->getDistributor();

        $page = $this->get(PageManager::class)->getPageContent($distributor, $path);

        if (!$page) {
            throw $this->createNotFoundException('Page indisponible.');
        }

        $breadcrumbs = [
            [   'name' => $page->getPageBlock()->getTitle() ,
                'url' => $this->generateUrl('page_show', ['path' => $page->getPageBlock()->getPath()]), 'last' => true
            ],
        ];
        $pageBlock = $page->getPageBlock();
        $template = $pageBlock->getTemplate();
        $overrideContent = null;

        switch ($template) {
            case 'legal-notice.html.twig':
                $overrideContent = $this->makeApiRequest('mentions-legales');
                $mainClassBgColor = 'bg-light';
                break;
            case 'privacy-and-cookies.html.twig':
                $overrideContent = $this->makeApiRequest('cookies');
                $mainClassBgColor = '';
                break;
            case 'sitemap.html.twig':
                $mainClassBgColor = 'bg-grey';
                $sitemap = $this->get(Builder::class)->mainMenu($distributor, true);
                break;
            default:
                $mainClassBgColor = 'bg-white';
                break;
        }
        if ($overrideContent) {
            $page->setContent($overrideContent);
        }

        return $this->render($this->getTemplate($pageBlock), [
            'page' => $page,
            'breadcrumbs' => $breadcrumbs,
            'mainClassBgColor' =>  $mainClassBgColor ?? 'bg-white',
            'sitemap' =>  $sitemap ?? null,
        ]);
    }

    /**
     * @param PageBlock|null $pageBlock
     * @return string
     */
    protected function getTemplate(PageBlock $pageBlock = null)
    {
        $templateName = PageBlock::DEFAULT_TEMPLATE;

        if ($pageBlock) {
            $templateName = $pageBlock->getTemplate();
        }

        $fullTemplateName = sprintf('@HondaPage/Page/%s', $templateName);

        if (!$this->get('templating')->exists($fullTemplateName)) {
            $fullTemplateName = '@HondaPage/Page/' . PageBlock::DEFAULT_TEMPLATE;
        }

        return $fullTemplateName;
    }

    private function makeApiRequest($slug)
    {
        $endpoints = $this->getParameter('endpoints.page');
        $gateway = $this->get(GenericGateway::class);
        $response = $gateway->getData($endpoints['get'] . "/{$slug}");
        if ($response && $response->status == 'success') {
            return $response->content;
        }

        return null;
    }
}
