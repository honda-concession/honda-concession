<?php

namespace Honda\PageBundle\Controller;

use Honda\MainBundle\Model\Admin\GenericFormHandler;
use Honda\PageBundle\Entity\Page;
use Honda\PageBundle\Entity\PageBlock;
use Honda\PageBundle\Model\Admin\PageManager;
use Honda\PageBundle\Form\PageType;
use Honda\PageBundle\Form\HondaLabelSlideType;
use Honda\PageBundle\Entity\HondaLabelSlide;


class BlockCRUDController extends PageCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $pageBlocks = $this->getDoctrine()->getManager()->getRepository(PageBlock::class)->getPageBlocks(PageBlock::TYPE_BLOCK);
        
        return $this->renderWithExtraParams('@HondaPage/CRUD/sonata_page_block_list.html.twig', [
            'pageBlocks' => $pageBlocks,
            'contentType' => PageBlock::TYPE_BLOCK,
        ]);
    }
}
