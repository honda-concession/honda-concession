<?php

namespace Honda\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\MainBundle\Entity\Traits\DistributorTrait;
use Honda\MainBundle\Entity\Traits\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="Honda\PageBundle\Repository\PageRepository")
 */
class Page
{
    use DistributorTrait, TimestampableTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"general"})
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="metaDescription", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="metaKeywords", type="text", nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="description1", type="text", nullable=true)
     */
    private $description1;

    /**
     * @var string
     *
     * @ORM\Column(name="description2", type="text", nullable=true)
     */
    private $description2;

    /**
     * @var string
     *
     * @ORM\Column(name="btn_link", type="string", length=255, nullable=true)
     */
    private $btnLink;

    /**
     * @var string
     *
     * @ORM\Column(name="btn_label", type="string", length=255, nullable=true)
     */
    private $btnLabel;
    
    /**
     * @ORM\ManyToOne(targetEntity="Honda\PageBundle\Entity\PageBlock", inversedBy="pages", cascade={"all"}, fetch="LAZY")
     */
    protected $pageBlock;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="image", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $image;
    
    /**
     * @ORM\OneToMany(targetEntity="Honda\PageBundle\Entity\HondaLabelSlide", mappedBy="page", cascade={"all"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    protected $hondaLabelSlides;
    
    /**
     * @ORM\OneToMany(targetEntity="Honda\PageBundle\Entity\PageCookiesBlock", mappedBy="page", cascade={"all"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    protected $pageCookiesBlocks;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="fiche_banniere_link", type="string", length=255 , nullable=true)
     */
    private $ficheBanniereLink;
    
    /**
     * @ORM\Column(name="fiche_banniere_external_link", type="boolean", options={"default": 0})
     */
    private $ficheBanniereExternalLink = false;
    
    
    public function __toString()
    {
        return '';
    }
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return Page
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set metaDescription.
     *
     * @param string|null $metaDescription
     *
     * @return Page
     */
    public function setMetaDescription($metaDescription = null)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription.
     *
     * @return string|null
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeywords.
     *
     * @param string|null $metaKeywords
     *
     * @return Page
     */
    public function setMetaKeywords($metaKeywords = null)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords.
     *
     * @return string|null
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set description1.
     *
     * @param string|null $description1
     *
     * @return Page
     */
    public function setDescription1($description1 = null)
    {
        $this->description1 = $description1;

        return $this;
    }

    /**
     * Get description1.
     *
     * @return string|null
     */
    public function getDescription1()
    {
        return $this->description1;
    }

    /**
     * Set description2.
     *
     * @param string|null $description2
     *
     * @return Page
     */
    public function setDescription2($description2 = null)
    {
        $this->description2 = $description2;

        return $this;
    }

    /**
     * Get description2.
     *
     * @return string|null
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * Set btnLink.
     *
     * @param string $btnLink
     *
     * @return Page
     */
    public function setBtnLink($btnLink)
    {
        $this->btnLink = $btnLink;

        return $this;
    }

    /**
     * Get btnLink.
     *
     * @return string
     */
    public function getBtnLink()
    {
        return $this->btnLink;
    }

    /**
     * Set btnLabel.
     *
     * @param string $btnLabel
     *
     * @return Page
     */
    public function setBtnLabel($btnLabel)
    {
        $this->btnLabel = $btnLabel;

        return $this;
    }

    /**
     * Get btnLabel.
     *
     * @return string
     */
    public function getBtnLabel()
    {
        return $this->btnLabel;
    }

    /**
     * Set pageBlock.
     *
     * @param \Honda\PageBundle\Entity\PageBlock|null $pageBlock
     *
     * @return Page
     */
    public function setPageBlock(\Honda\PageBundle\Entity\PageBlock $pageBlock = null)
    {
        $this->pageBlock = $pageBlock;

        return $this;
    }

    /**
     * Get pageBlock.
     *
     * @return \Honda\PageBundle\Entity\PageBlock|null
     */
    public function getPageBlock()
    {
        return $this->pageBlock;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Page
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hondaLabelSlides = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add hondaLabelSlide.
     *
     * @param \Honda\PageBundle\Entity\HondaLabelSlide $hondaLabelSlide
     *
     * @return Page
     */
    public function addHondaLabelSlide(\Honda\PageBundle\Entity\HondaLabelSlide $hondaLabelSlide)
    {
        $hondaLabelSlide->setPage($this);
        $this->hondaLabelSlides[] = $hondaLabelSlide;

        return $this;
    }

    /**
     * Remove hondaLabelSlide.
     *
     * @param \Honda\PageBundle\Entity\HondaLabelSlide $hondaLabelSlide
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeHondaLabelSlide(\Honda\PageBundle\Entity\HondaLabelSlide $hondaLabelSlide)
    {
        $hondaLabelSlide->setPage(null);
        return $this->hondaLabelSlides->removeElement($hondaLabelSlide);
    }

    /**
     * Get hondaLabelSlides.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHondaLabelSlides()
    {
        return $this->hondaLabelSlides;
    }

    /**
     * Add pageCookiesBlock.
     *
     * @param \Honda\PageBundle\Entity\PageCookiesBlock $pageCookiesBlock
     *
     * @return Page
     */
    public function addPageCookiesBlock(\Honda\PageBundle\Entity\PageCookiesBlock $pageCookiesBlock)
    {
        $pageCookiesBlock->setPage($this);
        $this->pageCookiesBlocks[] = $pageCookiesBlock;

        return $this;
    }

    /**
     * Remove pageCookiesBlock.
     *
     * @param \Honda\PageBundle\Entity\PageCookiesBlock $pageCookiesBlock
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePageCookiesBlock(\Honda\PageBundle\Entity\PageCookiesBlock $pageCookiesBlock)
    {
        $pageCookiesBlock->setPage(null);
        return $this->pageCookiesBlocks->removeElement($pageCookiesBlock);
    }

    /**
     * Get pageCookiesBlocks.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPageCookiesBlocks()
    {
        return $this->pageCookiesBlocks;
    }
    
    /**
     * @return string
     */
    public function getFicheBanniereLink()
    {
        return $this->ficheBanniereLink;
    }
    
    /**
     * @param string $ficheBanniereLink
     * @return Page
     */
    public function setFicheBanniereLink($ficheBanniereLink)
    {
        $this->ficheBanniereLink = $ficheBanniereLink;
        
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getFicheBanniereExternalLink()
    {
        return $this->ficheBanniereExternalLink;
    }
    
    /**
     * @param mixed $ficheBanniereExternalLink
     * @return Page
     */
    public function setFicheBanniereExternalLink($ficheBanniereExternalLink)
    {
        $this->ficheBanniereExternalLink = $ficheBanniereExternalLink;
        
        return $this;
    }
}
