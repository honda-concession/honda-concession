<?php

namespace Honda\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\MainBundle\Entity\Traits\DistributorTrait;
use Honda\MainBundle\Entity\Traits\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Page
 *
 * @ORM\Table(name="page_cookies_block")
 * @ORM\Entity(repositoryClass="Honda\PageBundle\Repository\PageCookiesBlockRepository")
 */
class PageCookiesBlock
{
    
    const BLOCK_VIE_PRIVE = 'BLOCK_VIE_PRIVE';
    const BLOCK_COOKIES = 'BLOCK_COOKIES';
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;
    
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;
    
    /**
     * @var string
     *
     * @ORM\Column(name="block_type", type="string", length=255, nullable=true)
     */
    private $blockType;
    
    /**
     * @ORM\ManyToOne(targetEntity="Honda\PageBundle\Entity\Page", inversedBy="pageCookiesBlocks")
     */
    protected $page;
    

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return PageCookiesBlock
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return PageCookiesBlock
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set page.
     *
     * @param \Honda\PageBundle\Entity\Page|null $page
     *
     * @return PageCookiesBlock
     */
    public function setPage(\Honda\PageBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page.
     *
     * @return \Honda\PageBundle\Entity\Page|null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set blockType.
     *
     * @param string|null $blockType
     *
     * @return PageCookiesBlock
     */
    public function setBlockType($blockType = null)
    {
        $this->blockType = $blockType;

        return $this;
    }

    /**
     * Get blockType.
     *
     * @return string|null
     */
    public function getBlockType()
    {
        return $this->blockType;
    }
}
