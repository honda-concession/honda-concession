<?php

namespace Honda\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="honda_label_slide")
 * @ORM\Entity(repositoryClass="Honda\PageBundle\Repository\HondaLabelSlideRepository")
 */
class HondaLabelSlide
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Honda\PageBundle\Entity\Page", inversedBy="hondaLabelSlides")
     */
    protected $page;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"})
     */
    protected $image;
    
    
    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;
        
        return $this;
    }
    
    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }
    

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page.
     *
     * @param \Honda\PageBundle\Entity\Page|null $page
     *
     * @return HondaLabelSlide
     */
    public function setPage(\Honda\PageBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page.
     *
     * @return \Honda\PageBundle\Entity\Page|null
     */
    public function getPage()
    {
        return $this->page;
    }
}
