<?php

namespace Honda\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PageBlock
 *
 * @ORM\Table(name="page_block", indexes={@ORM\Index(name="path_idx", columns={"path"})})
 * @ORM\Entity(repositoryClass="Honda\PageBundle\Repository\PageBlockRepository")
 */
class PageBlock
{
    
    const TYPE_PAGE = 'PAGE';
    const TYPE_BLOCK = 'BLOCK';

    const DEFAULT_TEMPLATE = 'default.html.twig';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255, nullable=true)
     */
    private $template;
    
    /**
     * @var string
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="help_text", type="string", length=255, nullable=true)
     */
    private $helpText;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="Honda\PageBundle\Entity\Page", mappedBy="pageBlock")
     */
    protected $pages;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set path.
     *
     * @param string|null $path
     *
     * @return PageBlock
     */
    public function setPath($path = null)
    {
        $this->path = $path;
        
        return $this;
    }
    
    /**
     * Get path.
     *
     * @return string|null
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return PageBlock
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set template.
     *
     * @param string|null $template
     *
     * @return PageBlock
     */
    public function setTemplate($template = null)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template.
     *
     * @return string|null
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page.
     *
     * @param \Honda\PageBundle\Entity\Page $page
     *
     * @return PageBlock
     */
    public function addPage(\Honda\PageBundle\Entity\Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page.
     *
     * @param \Honda\PageBundle\Entity\Page $page
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePage(\Honda\PageBundle\Entity\Page $page)
    {
        return $this->pages->removeElement($page);
    }

    /**
     * Get pages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Set helpText.
     *
     * @param string|null $helpText
     *
     * @return PageBlock
     */
    public function setHelpText($helpText = null)
    {
        $this->helpText = $helpText;

        return $this;
    }

    /**
     * Get helpText.
     *
     * @return string|null
     */
    public function getHelpText()
    {
        return $this->helpText;
    }

    /**
     * Set type.
     *
     * @param string|null $type
     *
     * @return PageBlock
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
}
