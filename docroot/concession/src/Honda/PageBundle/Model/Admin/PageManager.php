<?php

namespace Honda\PageBundle\Model\Admin;


use Honda\MainBundle\Model\AbstractBaseManager;
use Honda\MainBundle\Entity\Distributor;
use Honda\PageBundle\Entity\Page;
use Honda\PageBundle\Entity\PageBlock;


/**
 * Class PageManager
 * @package Honda\PageBundle\Model\Admin
 */
class PageManager extends AbstractBaseManager
{
    
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\PageBundle\Repository\PageRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(Page::class);
    }
    
    /**
     * @param Distributor $distributor
     */
    public function createAllPagesForDistributor(Distributor $distributor)
    {
        $pageBlocks = $this->getEntityManager()->getRepository(PageBlock::class)->findAll();
        
        foreach ($pageBlocks as $pageBlock) {
            $this->getPage($distributor, $pageBlock);
        }
    }
    
    
    /**
     * Si page n'hesiste pas on la crée
     * @param Distributor $distributor
     * @param PageBlock $pageBlock
     * @return Page
     */
    public function getPage(Distributor $distributor, PageBlock $pageBlock)
    {
        $page = $this->getRepository()->getOnePageByPageBlockAndDistributor($distributor, $pageBlock);
    
        if (!$page) {
            
            $page = new Page();
            $page
                ->setDistributor($distributor)
                ->setPageBlock($pageBlock)
            ;
    
            $this->save($page);
        }
        
        return $page;
    }
    
    /**
     * @param Distributor $distributor
     * @param $path
     * @return mixed|null
     */
    public function getPageContent(Distributor $distributor, $path)
    {
        return $this->getRepository()->getPage($distributor, $path);
    }
    
    
}
