<?php

namespace Honda\PageBundle\Validation;

use Honda\PageBundle\Entity\PageBlock;
use Symfony\Component\Form\FormInterface;


class ValidationGroupResolver
{
    
    /**
     * @param FormInterface $form
     * @return array
     */
    public function __invoke(FormInterface $form)
    {
        $groups = [];
        
        $page = $form->getData();
        
        if ($pageBlock = $page->getPageBlock()) {
        
            if ($pageBlock->getType() === PageBlock::TYPE_PAGE) {
                $groups = ['Default', 'general'];
            }
        }
        
        return $groups;
    }
}
