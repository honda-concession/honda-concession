<?php

namespace Honda\PageBundle\Twig;

use Doctrine\ORM\EntityManager;
use Honda\PageBundle\Entity\Page;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\RuntimeExtensionInterface;

/**
 * Class PageRuntime
 * @package Honda\MainBundle\Twig
 */
class PageRuntime implements RuntimeExtensionInterface
{

    /**
     * @var EntityManager
     */
    protected $em;
    
    /**
     * @var RequestStack
     */
    protected $requestStack;
    
    
    /**
     * PageRuntime constructor.
     * @param EntityManager $em
     * @param RequestStack $requestStack
     */
    public function __construct(EntityManager $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }
    
    /**
     * @param $slug
     * @return mixed|void
     */
    public function renderBlockHtml($slug)
    {
        $pageRepo = $this->em->getRepository(Page::class);
        $distributor = $this->requestStack->getCurrentRequest()->attributes->get('distributor');
        
        $page = $pageRepo->getBlock($distributor, $slug);
        
        return $page ?? null;
    }
    
}
