<?php

namespace Honda\PageBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;


class PageExtension extends AbstractExtension
{
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('render_block_html', array(PageRuntime::class, 'renderBlockHtml')),
        );
    }
    
    
}
