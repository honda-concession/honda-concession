<?php

namespace Honda\PageBundle\Block;

use Doctrine\ORM\EntityManager;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class PageCookiesBlock
 * @package Honda\PageBundle\Block
 */
class PageCookiesBlock extends AbstractBlockService
{
    
    const PATH = 'vie-privee-et-cookies';
    
    /**
     * @var EntityManager
     */
    protected $em;
    
    /**
     * @var RequestStack
     */
    protected $requestStack;
    
    /**
     * CookiesPrivateLifeBlock constructor.
     * @param null $name
     * @param EngineInterface|null $templating
     * @param EntityManager $em
     */
    public function __construct($name = null, EngineInterface $templating = null, EntityManager $em, RequestStack $requestStack)
    {
        parent::__construct($name, $templating);
        
        $this->em = $em;
        $this->requestStack = $requestStack;
    }
    
    
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'template' => '@HondaPage/Block/page_cookies_block.html.twig',
        ));
    
        $resolver->setRequired(['block_type', 'page_id']);
    }
    
    
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();
        
        $contentBlocks = $this->em->getRepository(\Honda\PageBundle\Entity\PageCookiesBlock::class)
            ->getCookiesPageBlock($settings['page_id'] , $settings['block_type']);
        
        return $this->renderResponse($blockContext->getTemplate(), array(
            'block'     => $blockContext->getBlock(),
            'settings'  => $settings,
            'contentBlocks' => $contentBlocks,
        ), $response);
    }
    
    public function getName()
    {
        return 'Block_cookies';
    }
    
    
}
