<?php

namespace Honda\PageBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;


class PageAdmin extends AbstractAdmin
{
    
    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName, $path)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->baseRoutePattern = 'content-manager-'.$path;
        $this->baseRouteName = 'content-manager-'.$path;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        $collection->add('edit',$this->getRouterIdParameter().'/edit', [], [], [], '', [], []);
    }

}
