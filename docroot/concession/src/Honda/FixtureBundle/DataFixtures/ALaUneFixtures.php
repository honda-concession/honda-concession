<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\ALaUne;
use Honda\MainBundle\Model\Front\AlaUneManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Model\MediaManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;


/**
 * Class ALaUneFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class ALaUneFixtures extends Fixture implements DependentFixtureInterface, ContainerAwareInterface
{
    
    use ContainerAwareTrait,  Traits\DataDirectoryTrait;
    
    
    protected $distributor;
    protected $mediaManager;
    protected $alaUneManager;
    protected $type;
    
    public function load(ObjectManager $manager)
    {
        $this->alaUneManager = $this->container->get(AlaUneManager::class);
        $types = $this->alaUneManager->getAlaUneCategories();
        $this->type = empty($types) ? null : $types[0]['id'];

        $this->createObject1($manager);
        $this->createObject2($manager);
        $this->createObject3($manager);
    }
    
    /**
     * @param ObjectManager $manager
     */
    private function createObject1(ObjectManager $manager)
    {
        $this->mediaManager = $this->container->get('sonata.media.manager.media');
        
        $media = new Media();
        $media->setBinaryContent($this->getData()['img1']);
        $media->setContext('alaune');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1)->getId());
    
        $this->mediaManager->save($media);
        
        $aLaUne = new ALaUne();
        $aLaUne
            ->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1))
            ->setTitle('CB1000R Neo Sport')
            ->setSubtitle('Sous titre')
            ->setTextMea('<p>description1</p>')
            ->setLink('http://yahoo.fr')
            ->setType($this->type)
            ->setMedia($media)
            ->setPublished(true)
            ->setLinkOpenInNewTab(true)
        ;
    
        $manager->persist($aLaUne);
        $manager->flush();
    }
    
    /**
     * @param ObjectManager $manager
     */
    private function createObject2(ObjectManager $manager)
    {

        $aLaUne = new ALaUne();
        $aLaUne
            ->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1))
            ->setTitle('titre1')
            ->setSubtitle('Sous titre')
            ->setTextMea('<p>description1</p>')
            ->setLink('http://yahoo.fr')
            ->setType($this->type)
            ->setLinkOpenInNewTab(true)
        ;
    
        $manager->persist($aLaUne);
        $manager->flush();
    }
    
    
    /**
     * @param ObjectManager $manager
     */
    private function createObject3(ObjectManager $manager)
    {
        
        $this->mediaManager = $this->container->get('sonata.media.manager.media');
        
        $media = new Media();
        $media->setBinaryContent('https://www.youtube.com/watch?v=VInF-xhXSDw');
        $media->setContext('alaune');
        $media->setProviderName('sonata.media.provider.youtube');
        $media->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1)->getId());
        
        $this->mediaManager->save($media);
        
        
        $aLaUne = new ALaUne();
        $aLaUne
            ->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1))
            ->setTitle('CB1000R Neo cafe')
            ->setSubtitle('Nouvelle moto café')
            ->setTextMea('<p>description1</p>')
            ->setLink('http://yahoo.fr')
            ->setType($this->type)
            ->setMedia($media)
            ->setPublished(true)
            ->setLinkOpenInNewTab(true)
        ;
        
        $manager->persist($aLaUne);
        $manager->flush();
    }
    
    
    public function getData()
    {
        $dir = $this->getDataDirectory();
        $imgDir = $dir . '/img';
        return [
            'img1' => $imgDir . '/media-01-1024.jpg',
        ];
    }
    
    
    public function getDependencies()
    {
        return array(
            DistributorFixtures::class,
        );
    }
    
}
