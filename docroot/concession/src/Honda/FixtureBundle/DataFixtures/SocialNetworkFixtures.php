<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Honda\MainBundle\Entity\SocialNetworkLink;


/**
 * Class ServicesFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class SocialNetworkFixtures extends Fixture implements DependentFixtureInterface
{
    
    
    public function load(ObjectManager $manager)
    {
        $distributor = $this->getReference(DistributorFixtures::DISTRIBUTOR__1);
        
        $socialNetworkLink = new SocialNetworkLink();
        $socialNetworkLink
            ->setDistributor($distributor)
            ->setYoutube('https://www.youtube.com/honda')
            ->setFacebook('https://www.facebook.com/honda.france/')
            ->setTwitter('https://twitter.com/honda')
        ;
        
        $manager->persist($socialNetworkLink);
        $manager->flush();
    }
    
    public function getDependencies()
    {
        return array(
            DistributorFixtures::class,
        );
    }
    
}
