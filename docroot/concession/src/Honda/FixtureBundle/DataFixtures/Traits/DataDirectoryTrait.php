<?php

namespace Honda\FixtureBundle\DataFixtures\Traits;


trait DataDirectoryTrait {
    
    /**
     * @return string
     */
    public function getDataDirectory()
    {
        return  __DIR__ . "/../../Resources/data";
    }
}
