<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Model\MediaManager;
use Honda\MainBundle\Entity\Accessory;


/**
 * Class AccessoryFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class AccessoryFixtures extends Fixture  implements DependentFixtureInterface, ContainerAwareInterface
{
    
    use ContainerAwareTrait,  Traits\DataDirectoryTrait;
    
    
    protected $distributor;
    protected $mediaManager;
    
    
    public function load(ObjectManager $manager)
    {
    
        $this->distributor = $this->getReference(DistributorFixtures::DISTRIBUTOR__1);
        $this->mediaManager = $this->container->get('sonata.media.manager.media');
    
        $media = new Media();
        $media->setBinaryContent($this->getData()['accessory1']);
        $media->setContext('accessory');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
    
        $this->mediaManager->save($media);
        
        $accessory = new Accessory();
        $accessory
            ->setAccessoryBlock($this->getReference(AccessoryBlockFixtures::ACCESSORYBlOCK_1))
            ->setImage($media)
            ->setDistributor($this->distributor)
            ->setTitle('Accessoires motos')
            ->setDescription('Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque quaerat quae repellendus nemo, harum maxime doloribus minus sint vero praesentium quo. Libero debitis blanditiis, atque consequatur optio deleniti dolores dicta. Libero ea quo quae a omnis quas, dolorum itaque laudantium, architecto tempora et cum ipsa ab asperiores similique vitae dolore nam ipsum?')
            ->setLink('http://yahoo.fr')
        ;
        
        
        $logos1 = [['id' => 1, 'url' => 'https://logo.clearbit.com/skype.com', 'mark' => 'skype'], ['id' => 2, 'url' => 'https://logo.clearbit.com/honda.com', 'mark' => 'honda']];
        $logos2 = [['id' => 1, 'url' => 'https://logo.clearbit.com/skype.com', 'mark' => 'skype']];
        
    
        $accessory1 = new Accessory();
        $accessory1
            ->setDescriptionAccessoryHelmet('Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem quod aliquam veritatis tempora ad labore voluptatem quos asperiores dolore debitis, officia reiciendis porro repellat hic vitae repudiandae ab, necessitatibus quia?')
            ->setDescriptionAccessoryClothing('description')
            ->setHelmetLogos($logos2)
            ->setClothingLogos($logos1)
            ->setAccessoryBlock($this->getReference(AccessoryBlockFixtures::ACCESSORYBlOCK_2))
            ->setDistributor($this->distributor)
            ->setImage($media)
            ->setTitle('ÉQUIPEMENT DU MOTARD')
            ->setDescription('Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque quaerat quae repellendus nemo, harum maxime doloribus minus sint vero praesentium quo. Libero debitis blanditiis, atque consequatur optio deleniti dolores dicta. Libero ea quo quae a omnis quas, dolorum itaque laudantium, architecto tempora et cum ipsa ab asperiores similique vitae dolore nam ipsum?')
            ->setLink('http://google.fr')
        ;
        
        $manager->persist($accessory);
        $manager->persist($accessory1);
        
        $manager->flush();
    }
    
    public function getData()
    {
        $dir = $this->getDataDirectory();
        $imgDir = $dir . '/img';
        return [
            'accessory1' => $imgDir . '/accessory.png',
        ];
    }
    
    
    public function getDependencies()
    {
        return array(
            AccessoryBlockFixtures::class,
            DistributorFixtures::class,
        );
    }
    
}

