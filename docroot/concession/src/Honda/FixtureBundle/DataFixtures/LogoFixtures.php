<?php


namespace Honda\FixtureBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Model\MediaManager;


/**
 * Class LogoFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class LogoFixtures extends Fixture  implements DependentFixtureInterface, ContainerAwareInterface
{
    
    use ContainerAwareTrait, Traits\DataDirectoryTrait;
    
    public function load(ObjectManager $manager)
    {
        $distributor = $this->getReference(DistributorFixtures::DISTRIBUTOR__1);
        $mediaManager = $this->container->get('sonata.media.manager.media');
    
        if (file_exists($this->getData())) {
  
            $media = new Media();
            $media->setBinaryContent($this->getData());
            $media->setContext('logo');
            $media->setProviderName('sonata.media.provider.image');
            $media->setDistributor($distributor->getId());
    
            $mediaManager->save($media);
    
            $distributor->setLogo($media);
        }
        
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        $dir = $this->getDataDirectory();
        
        return $dir . '/img/star-bike.png';
    }
    
    public function getDependencies()
    {
        return array(
            DistributorFixtures::class,
        );
    }
}
