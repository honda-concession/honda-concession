<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Honda\MainBundle\Entity\Service;
use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Model\MediaManager;


/**
 * Class ServicesFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class ServicesFixtures extends Fixture implements DependentFixtureInterface, ContainerAwareInterface
{
    
    use ContainerAwareTrait,  Traits\DataDirectoryTrait;
    
    
    protected $distributor;
    protected $mediaManager;
    
    public function load(ObjectManager $manager)
    {
        $this->mediaManager = $this->container->get('sonata.media.manager.media');
     
        $icon = new Media();
        $icon->setBinaryContent($this->getData()['picto1']);
        $icon->setContext('service_icon');
        $icon->setProviderName('sonata.media.provider.image');
        $icon->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1)->getId());
    
        $this->mediaManager->save($icon);
    
    
        $pdf = new Media();
        $pdf->setBinaryContent($this->getData()['pdf']);
        $pdf->setContext('service_pdf');
        $pdf->setProviderName('sonata.media.provider.file');
        $pdf->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1)->getId());
    
        $this->mediaManager->save($pdf);
        
        $service4 = new Service();
        $service4
            ->setTitle('service concessionaire 1')
            //->setActive(true)
            ->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1))
            ->setIcon($icon)
            ->setPdf($pdf)
            ->setContent('<p>Contenu</p>')
        ;
        

        $manager->persist($service4);
    
        $manager->flush();
    }
    
    public function getData()
    {
        $dir = $this->getDataDirectory();
        $imgDir = $dir . '/img';
        $pdfDir = $dir . '/pdf';
        
        return [
            'picto1' => $imgDir . '/service_icon1.jpg',
            'iconsvg' => $imgDir . '/service_icon2.svg',
            'pdf' => $pdfDir . '/service.pdf',
        ];
    }
    
    
    public function getDependencies()
    {
        return array(
            DistributorFixtures::class,
        );
    }
    
}
