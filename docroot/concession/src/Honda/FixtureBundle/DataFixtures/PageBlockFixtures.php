<?php


namespace Honda\FixtureBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Sonata\MediaBundle\Model\MediaManager;
use Honda\PageBundle\Entity\PageBlock;


/**
 * Class PageBlockFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class PageBlockFixtures extends Fixture
{
    
    public function load(ObjectManager $manager)
    {
        $pageBlock2 = new PageBlock();
        $pageBlock2->setTitle('Plan du site');
        $pageBlock2->setHelpText('Contenu de la page "Plan du site"');
        $pageBlock2->setTemplate('sitemap.html.twig');
        $pageBlock2->setType(PageBlock::TYPE_PAGE);
    
        $pageBlock3 = new PageBlock();
        $pageBlock3->setTitle('Vie privée et cookies');
        $pageBlock3->setHelpText('Contenu de la page "Vie privée et cookies"');
        $pageBlock3->setTemplate('privacy-and-cookies.html.twig');
        $pageBlock3->setType(PageBlock::TYPE_PAGE);
    
        $pageBlock4 = new PageBlock();
        $pageBlock4->setTitle('Mentions légales');
        $pageBlock4->setHelpText('Contenu de la page "Mentions légales"');
        $pageBlock4->setTemplate('legal-notice.html.twig');
        $pageBlock4->setType(PageBlock::TYPE_PAGE);
        
        $pageBlock5 = new PageBlock();
        $pageBlock5->setTitle('Label Honda');
        $pageBlock5->setHelpText('Contenu de la page "Label Honda"');
        $pageBlock5->setTemplate('honda-label.html.twig');
        $pageBlock5->setType(PageBlock::TYPE_PAGE);
    
        $pageBlock6 = new PageBlock();
        $pageBlock6->setTitle('Bannière - Page Label Honda');
        $pageBlock6->setHelpText('Modifie l\'image du label Honda');
        $pageBlock6->setTemplate('banniere-page-label-honda.html.twig');
        $pageBlock6->setType(PageBlock::TYPE_BLOCK);
        
        $manager->persist($pageBlock2);
        $manager->persist($pageBlock3);
        $manager->persist($pageBlock4);
        $manager->persist($pageBlock5);
        $manager->persist($pageBlock6);
        
        $manager->flush();
    }
    
}
