<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;

use FOS\UserBundle\Doctrine\UserManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Class UserFixtures
 * @package DataFixtures
 */
class UserFixtures extends Fixture  implements DependentFixtureInterface
{
    
    use ContainerAwareTrait;
    
    const SUPER_ADMIN_USER = 'super-admin-user';
    const DISTRIBUTOR_USER = 'distributor-user';
    
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $this->createSuperAdmin($userManager);
        $this->createDistributorUser($userManager);
        $this->createPreviewUser($userManager);
    }
    
    /**
     * @param UserManager $userManager
     */
    private function createSuperAdmin(UserManager $userManager)
    {
        
        $user = $userManager->createUser();
        
        $user
            ->setUsername('admin')
            ->setEmail('yves@monkees.fr')
            ->setPlainPassword('admin')
            ->setEnabled(true)
            ->setRoles(array('ROLE_ROOT', 'SONATA'))
        ;
        
        $userManager->updateUser($user, true);
        
        $this->setReference(self::SUPER_ADMIN_USER, $user);
        
    }
    
    /**
     * @param UserManager $userManager
     */
    private function createDistributorUser(UserManager $userManager)
    {
        
        $user = $userManager->createUser();
        
        $user
            ->setUsername('distributor')
            ->setEmail('yves+1@monkees.fr')
            ->setPlainPassword('distributor')
            ->setEnabled(true)
            ->setRoles(array('ROLE_DISTRIBUTOR', 'SONATA'))
            ->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1))
        ;
        
        $userManager->updateUser($user, true);
        
        $this->setReference(self::DISTRIBUTOR_USER, $user);
    }
    
    
    /**
     * @param UserManager $userManager
     */
    private function createPreviewUser(UserManager $userManager)
    {
        
        $user = $userManager->createUser();
        
        $user
            ->setUsername('preview')
            ->setEmail('yves+3@monkees.fr')
            ->setPlainPassword('preview')
            ->setEnabled(true)
            ->setRoles(array('ROLE_DISTRIBUTOR', 'SONATA', 'ROLE_PREVIEW'))
        ;
        
        $userManager->updateUser($user, true);
    }
    
    public function getDependencies()
    {
        return array(
            DistributorFixtures::class,
        );
    }
}
