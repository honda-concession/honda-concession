<?php


namespace Honda\FixtureBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Model\MediaManager;
use Honda\MainBundle\Entity\Slider;



/**
 * Class SliderFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class SliderFixtures extends Fixture  implements  DependentFixtureInterface, ContainerAwareInterface
{
    
    use ContainerAwareTrait,  Traits\DataDirectoryTrait;
    
    protected $distributor;
    protected $mediaManager;
    
    public function load(ObjectManager $manager)
    {
        $this->distributor = $this->getReference(DistributorFixtures::DISTRIBUTOR__1);
        $this->mediaManager = $this->container->get('sonata.media.manager.media');
        
        $this->slider1($manager);
        $this->slider2($manager);
        $this->slider3($manager);
    
    }
    
    
    private function slider1(ObjectManager $manager)
    {
        $media = new Media();
        $media->setBinaryContent($this->getData()['slider1']);
        $media->setContext('slider');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());

        $this->mediaManager->save($media);
        
        $slider = new Slider();
        $slider
            ->setStartDate((new \DateTime())->sub(new \DateInterval('P10D')))
            ->setEndDate((new \DateTime())->add(new \DateInterval('P1M')))
            ->setDistributor($this->distributor)
            ->setUrl('http://yahoo.fr')
            ->setMedia($media)
        ;
    
        $manager->persist($slider);
        $manager->flush();
        
    }
    
    private function slider2(ObjectManager $manager)
    {
        $media = new Media();
        $media->setBinaryContent($this->getData()['slider2']);
        $media->setContext('slider');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
        
        $this->mediaManager->save($media);
        
        $slider = new Slider();
        $slider
            ->setStartDate((new \DateTime())->sub(new \DateInterval('P10D')))
            ->setEndDate((new \DateTime())->add(new \DateInterval('P1M')))
            ->setDistributor($this->distributor)
            ->setUrl('http://gmail.fr')
            ->setMedia($media)
        ;
    
        $manager->persist($slider);
        $manager->flush();
    }
    
    
    private function slider3(ObjectManager $manager)
    {
        $media = new Media();
        $media->setBinaryContent($this->getData()['slider3']);
        $media->setContext('slider');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
        
        $this->mediaManager->save($media);
        
        $slider = new Slider();
        $slider
            ->setStartDate((new \DateTime())->sub(new \DateInterval('P10D')))
            ->setEndDate((new \DateTime())->add(new \DateInterval('P1M')))
            ->setDistributor($this->distributor)
            ->setUrl('http://yahoo.fr')
            ->setMedia($media)
        ;
        
        $manager->persist($slider);
        $manager->flush();
        
    }
    
    
    /**
     *
     *  TODO: Pas utiliser
     * @param ObjectManager $manager
     * @throws \Exception
     */
    private function slider4(ObjectManager $manager)
    {
        $media = new Media();
        $media->setBinaryContent('https://www.youtube.com/watch?v=VInF-xhXSDw');
        $media->setContext('slider');
        $media->setProviderName('sonata.media.provider.youtube');
        $media->setDistributor($this->distributor->getId());
        
        $this->mediaManager->save($media);
        
        $slider = new Slider();
        $slider
            ->setStartDate((new \DateTime())->sub(new \DateInterval('P10D')))
            ->setEndDate((new \DateTime())->add(new \DateInterval('P1M')))
            ->setDistributor($this->distributor)
            ->setMedia($media)
        ;
        
        $manager->persist($slider);
        $manager->flush();
        
    }
    
    
    public function getData()
    {
        $dir = $this->getDataDirectory();
        $imgDir = $dir . '/img';
        return [
            'slider1' => $imgDir . '/hero-01-1024.jpg',
            'slider2' => $imgDir . '/hero-02-1024.jpg',
            'slider3' => $imgDir . '/hero-03-1024.jpg',
        ];
    }
    
    
    public function getDependencies()
    {
        return array(
            DistributorFixtures::class,
        );
    }
}
