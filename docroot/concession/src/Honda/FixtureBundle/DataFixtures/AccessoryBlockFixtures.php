<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

use Honda\MainBundle\Entity\AccessoryBlock;


/**
 * Class AccessoryBlockFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class AccessoryBlockFixtures extends Fixture
{
    
    const ACCESSORYBlOCK_1 = 'ACCESSORYBlOCK_1';
    const ACCESSORYBlOCK_2 = 'ACCESSORYBlOCK_2';
    
    use ContainerAwareTrait;
    
    public function load(ObjectManager $manager)
    {
        $accessoryBlock1 = new AccessoryBlock();
        $accessoryBlock1->setTitle('Accessoires motos');
    
        $accessoryBlock2 = new AccessoryBlock();
        $accessoryBlock2->setTitle('Equipement du motard');
        
        $manager->persist($accessoryBlock1);
        $manager->persist($accessoryBlock2);
    
    
        $this->setReference(self::ACCESSORYBlOCK_1, $accessoryBlock1);
        $this->setReference(self::ACCESSORYBlOCK_2, $accessoryBlock2);
        
        $manager->flush();
    }
  
}
