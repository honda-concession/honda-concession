<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\Section;


/**
 * Class SectionFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class SectionFixtures extends Fixture
{
    
    public function load(ObjectManager $manager)
    {

        $section = new Section();
        $section
            ->setTitle('A la une')
            ->setSectionKey('alaune')
            ->setTemplateName('block_alaune.html.twig')
            ;
    
        $section1 = new Section();
        $section1
            ->setTitle('Dernières occassions')
            ->setSectionKey('last-ad')
            ->setTemplateName('block_last-ad.html.twig')
        ;
    
    
        $section2 = new Section();
        $section2
            ->setTitle('Actualités et évènements')
            ->setSectionKey('news-events')
            ->setTemplateName('block_news-events.html.twig')
        ;
    
        $section3 = new Section();
        $section3
            ->setTitle('Services')
            ->setSectionKey('services')
            ->setTemplateName('block_services.html.twig')
        ;
    
    
        $manager->persist($section);
        $manager->persist($section1);
        $manager->persist($section2);
        $manager->persist($section3);
    
        $manager->flush();
    }
    
}
