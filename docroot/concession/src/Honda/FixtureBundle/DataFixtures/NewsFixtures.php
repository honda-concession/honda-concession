<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\News;
use Honda\MainBundle\Model\Front\NewsManager;
use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Model\MediaManager;

/**
 * Class NewsFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class NewsFixtures extends Fixture implements DependentFixtureInterface, ContainerAwareInterface
{
    
    use ContainerAwareTrait,  Traits\DataDirectoryTrait;
    
    
    protected $distributor;
    protected $mediaManager;
    protected $newsManager;
    protected $type;
    
    public function load(ObjectManager $manager)
    {
        $this->distributor = $this->getReference(DistributorFixtures::DISTRIBUTOR__1);
        $this->mediaManager = $this->container->get('sonata.media.manager.media');
        $this->newsManager = $this->container->get(NewsManager::class);
        $types = $this->newsManager->getNewsCategories();
        $this->type = empty($types) ? null : $types[0]['id'];

        $this->createNews1($manager);
        $this->createNews2($manager);
        $this->createNews3($manager);
        $this->createNews4($manager);
        $this->createNews5($manager);
    }
    
    /**
     * @param ObjectManager $manager
     */
    private function createNews1(ObjectManager $manager)
    {
    
        $media = new Media();
        $media->setBinaryContent($this->getData()['news1']);
        $media->setContext('news');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
        $this->mediaManager->save($media);
        
        $news = new News();
        $news
            ->setDistributor($this->distributor)
            ->setTitle('Nouvelle Honda Africa Twin Adventure à l\'essai')
            ->setDescription1('<p>Et olim licet otiosae sint tribus pacataeque centuriae et nulla suffragiorum certamina set Pompiliani redierit securitas temporis, per omnes tamen quotquot sunt partes terrarum, ut domina suscipitur et regina et</p>')
            ->setDescription2('<p>description2</p>')
            ->setStartDate((new \DateTime())->sub(new \DateInterval('P2D')))
            ->setEndDate((new \DateTime())->add(new \DateInterval('P1M')))
            ->setType($this->type)
            //->setActive(true)
            //->setIsOnHomepage(true)
            ->setImage($media)
        ;
    
        $manager->persist($news);
        $manager->flush();
    }
    
    /**
     * @param ObjectManager $manager
     */
    private function createNews2(ObjectManager $manager)
    {
        $news = new News();
        $news
            ->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1))
            ->setTitle('Actualité 2')
            ->setDescription1('<p>Et olim licet otiosae sint tribus pacataeque centuriae et nulla suffragiorum certamina set Pompiliani redierit securitas temporis, per omnes tamen quotquot sunt partes terrarum, ut domina suscipitur et regina et</p>')
            ->setDescription2('<p>description2</p>')
            ->setStartDate((new \DateTime())->sub(new \DateInterval('P10D')))
            ->setEndDate((new \DateTime())->add(new \DateInterval('P1M')))
            ->setType($this->type)
            //->setActive(false)
        ;
        
        $manager->persist($news);
        $manager->flush();
    }
    
    /**
     * @param ObjectManager $manager
     */
    private function createNews3(ObjectManager $manager)
    {
    
        $media = new Media();
        $media->setBinaryContent($this->getData()['news2']);
        $media->setContext('news');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
        $this->mediaManager->save($media);
        
        
        $news = new News();
        $news
            ->setDistributor($this->distributor)
            ->setTitle('Nouvelle Honda America Adventure à l\'essai')
            ->setDescription1('<p>Et olim licet otiosae sint tribus pacataeque centuriae et nulla suffragiorum certamina set Pompiliani redierit securitas temporis, per omnes tamen quotquot sunt partes terrarum, ut domina suscipitur et regina et</p>')
            ->setDescription2('<p>description2</p>')
            ->setStartDate((new \DateTime())->sub(new \DateInterval('P10D')))
            ->setEndDate((new \DateTime())->add(new \DateInterval('P1M')))
            ->setType($this->type)
            //->setActive(true)
            //->setIsOnHomepage(true)
            ->setImage($media)
        ;
        
        $manager->persist($news);
        $manager->flush();
    }
    
    /**
     * @param ObjectManager $manager
     */
    private function createNews4(ObjectManager $manager)
    {
        $media = new Media();
        $media->setBinaryContent($this->getData()['news3']);
        $media->setContext('news');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
        $this->mediaManager->save($media);
        
        $news = new News();
        $news
            ->setDistributor($this->distributor)
            ->setTitle('Nouvelle Honda Europe Adventure')
            ->setDescription1('<p>Et olim licet otiosae sint tribus pacataeque centuriae et nulla suffragiorum certamina set Pompiliani redierit securitas temporis, per omnes tamen quotquot sunt partes terrarum, ut domina suscipitur et regina et</p>')
            ->setDescription2('<p>description2</p>')
            ->setStartDate((new \DateTime())->sub(new \DateInterval('P5D')))
            ->setEndDate((new \DateTime())->add(new \DateInterval('P1M')))
            ->setType($this->type)
            //->setActive(true)
            //->setIsOnHomepage(true)
            ->setImage($media)
        ;
        
        $manager->persist($news);
        $manager->flush();
    }
    
    
    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    private function createNews5(ObjectManager $manager)
    {
        $media = new Media();
        $media->setBinaryContent($this->getData()['news3']);
        $media->setContext('news');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
        $this->mediaManager->save($media);
        
        $news = new News();
        $news
            ->setDistributor($this->distributor)
            ->setTitle('Nouvelle Honda Asia Context')
            ->setDescription1('<p>Et olim licet otiosae sint tribus pacataeque centuriae et nulla suffragiorum certamina set Pompiliani redierit securitas temporis, per omnes tamen quotquot sunt partes terrarum, ut domina suscipitur et regina et</p>')
            ->setDescription2('<p>description2</p>')
            ->setStartDate((new \DateTime())->sub(new \DateInterval('P1D')))
            ->setEndDate((new \DateTime())->add(new \DateInterval('P1M')))
            ->setType($this->type)
            //->setActive(true)
            ->setImage($media)
        ;
        
        $manager->persist($news);
        $manager->flush();
    }
    
    
    
    public function getData()
    {
        $dir = $this->getDataDirectory();
        $imgDir = $dir . '/img';
        return [
            'news1' => $imgDir . '/news1.jpg',
            'news2' => $imgDir . '/news2.jpg',
            'news3' => $imgDir . '/news3.jpg',
        ];
    }
    
    
    public function getDependencies()
    {
        return array(
            DistributorFixtures::class,
        );
    }
    
}
