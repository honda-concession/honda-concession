<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\Distributor;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Class UserFixtures
 * @package DataFixtures
 */
class DistributorFixtures extends Fixture implements DependentFixtureInterface
{
    
    const DISTRIBUTOR__1 = 'distributor_1';
    
    
    public function load(ObjectManager $manager)
    {
        $this->createDistributor1($manager);
    }
    
    /**
     * @param ObjectManager $manager
     */
    private function createDistributor1(ObjectManager $manager)
    {
        
        $distributor = new Distributor();
    
        $distributor
            ->setBgColor('')
            ->setDistributorPortailId(12)
            ->setPhone('0556874512')
            ->setInfos([])
            ->setDomain('distributor.concession.honda')
            ->setTimetable('Lundi au vendred 9h-11h')
            ->setActive(true)
        ;
    
        $manager->persist($distributor);
        $manager->flush();
    
        $this->setReference(self::DISTRIBUTOR__1, $distributor);
        
    }
    
    public function getDependencies()
    {
        return array(
            SectionFixtures::class,
        );
    }
    
}
