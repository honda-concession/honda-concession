<?php


namespace Honda\FixtureBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Honda\MainBundle\Entity\VehicleAd;
use Honda\MainBundle\Entity\VehicleAdGallery;
use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Model\MediaManager;


/**
 * Class VehicleAdFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class VehicleAdFixtures extends Fixture  implements DependentFixtureInterface, ContainerAwareInterface
{
    
    use ContainerAwareTrait,  Traits\DataDirectoryTrait;
    
    
    protected $distributor;
    protected $mediaManager;
    
    public function load(ObjectManager $manager)
    {
        $this->distributor = $this->getReference(DistributorFixtures::DISTRIBUTOR__1);
        $this->mediaManager = $this->container->get('sonata.media.manager.media');
        
        $this->created1($manager);
        $this->created2($manager);
        $this->created3($manager);
        $this->createdMulti($manager);
    }
    
    /**
     * @param ObjectManager $manager
     */
    private function created1(ObjectManager $manager)
    {
    
        $media = new Media();
        $media->setBinaryContent($this->getData()['vehicleAd1']);
        $media->setContext('vehicleAd');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
        $this->mediaManager->save($media);
    
        $media1 = new Media();
        $media1->setBinaryContent($this->getData()['vehicleAd11']);
        $media1->setContext('vehicleAd');
        $media1->setProviderName('sonata.media.provider.image');
        $media1->setDistributor($this->distributor->getId());
        $this->mediaManager->save($media1);
    
        $media2 = new Media();
        $media2->setBinaryContent($this->getData()['vehicleAd12']);
        $media2->setContext('vehicleAd');
        $media2->setProviderName('sonata.media.provider.image');
        $media2->setDistributor($this->distributor->getId());
        $this->mediaManager->save($media2);
        
        $vehicleAdGallery = new VehicleAdGallery();
        $vehicleAdGallery
            ->setImage($media)
            ->setIsMainImage(true)
        ;
    
        $vehicleAdGallery1 = new VehicleAdGallery();
        $vehicleAdGallery1
            ->setImage($media1)
        ;
    
        $vehicleAdGallery2 = new VehicleAdGallery();
        $vehicleAdGallery2
            ->setImage($media2)
        ;
        
        $vehicleAd = new VehicleAd();
        $vehicleAd
            ->setDistributor($this->distributor)
            ->setTitle('HONDA CB 1000 1000 R ABS')
            ->setColor('blanc verni')
            ->setCylinder(900)
            ->setDescription("HONDA CB 1000 1000 R ABS 9 800 €  2017 - 3 900 km ")
            ->setDistributorDescription('Velit aliquip non enim cupidatat tempor aute dolor dolor consequat ullamco duis labore fugiat sint. Sit tempor laboris anim ex nostrud quis exercitation esse nulla. Irure sit aliqua eu eiusmod ex nostrud laborum occaecat nulla mollit enim irure fugiat. Et dolor sint proident excepteur velit esse in cillum eu incididunt voluptate officia nulla. Nostrud qui anim incididunt excepteur deserunt. ')
            ->setKm("3900")
            ->setPrice("9800")
            ->setWarrant("8")
            ->setFirstHand(true)
            ->setOptions("ABS, capot de selle, clignotants goutte d'eau, support de plaque")
            ->setModelYear("2017")
            ->setCategoryPortail('sport')
            ->setModelPortail('Sport')
            ->setMarkPortail('Honda')
            ->setReleaseDate(new \DateTime())
            ->setDrivingLicenseA2(true)
            ->setPublished(true)
        ;
    
        $vehicleAd->addVehicleAdGallery($vehicleAdGallery);
        $vehicleAd->addVehicleAdGallery($vehicleAdGallery1);
        $vehicleAd->addVehicleAdGallery($vehicleAdGallery2);
        
        $manager->persist($vehicleAd);
    
        $manager->flush();
        
    }
    
    
    /**
     * @param ObjectManager $manager
     */
    private function created2(ObjectManager $manager)
    {
        $media = new Media();
        $media->setBinaryContent($this->getData()['vehicleAd2']);
        $media->setContext('vehicleAd');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
    
        $this->mediaManager->save($media);
        
        
        $vehicleAdGallery = new VehicleAdGallery();
        $vehicleAdGallery
            ->setImage($media)
            ->setIsMainImage(true)
        ;
        
        
        $vehicleAd = new VehicleAd();
        $vehicleAd
            ->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1))
            ->setTitle('HONDA DN-01')
            ->setColor('noir verni')
            ->setCylinder(300)
            ->setDescription("HONDA DN-01 noir verni 7cv")
            ->setDistributorDescription('Velit aliquip non enim cupidatat tempor aute dolor dolor consequat ullamco duis labore fugiat sint. Sit tempor laboris anim ex nostrud quis exercitation esse nulla. Irure sit aliqua eu eiusmod ex nostrud laborum occaecat nulla mollit enim irure fugiat. Et dolor sint proident excepteur velit esse in cillum eu incididunt voluptate officia nulla. Nostrud qui anim incididunt excepteur deserunt. ')
            ->setKm("44600")
            ->setPrice("3700")
            ->setWarrant("N.C.")
            ->setFirstHand(true)
            ->setOptions("ABS, répartiteur de freinage, top case, coupe circuit")
            ->setModelYear("2009")
            ->setCategoryPortail('scooter')
            ->setModelPortail('scooter')
            ->setMarkPortail('Honda')
            ->setReleaseDate(new \DateTime())
            ->setPublished(true)
        ;
    
        $vehicleAd->addVehicleAdGallery($vehicleAdGallery);
        
        $manager->persist($vehicleAd);
        
        $manager->flush();
    }
    
    
    
    /**
     * @param ObjectManager $manager
     */
    private function created3(ObjectManager $manager)
    {
        $media = new Media();
        $media->setBinaryContent($this->getData()['vehicleAd3']);
        $media->setContext('vehicleAd');
        $media->setProviderName('sonata.media.provider.image');
        $media->setDistributor($this->distributor->getId());
        
        $this->mediaManager->save($media);
        
        
        $vehicleAdGallery = new VehicleAdGallery();
        $vehicleAdGallery
            ->setImage($media)
            ->setIsMainImage(true)
        ;
        
        
        $vehicleAd = new VehicleAd();
        $vehicleAd
            ->setDistributor($this->distributor)
            ->setTitle('HONDA CB 1000')
            ->setColor('blanc')
            ->setCylinder(150)
            ->setDescription("HONDA CB 1000 blanc 9cv")
            ->setKm("29634 ")
            ->setPrice("5990")
            ->setWarrant("N.C.")
            ->setFirstHand(false)
            ->setOptions('CB - 1000 R ABS')
            ->setModelYear("2009")
            ->setCategoryPortail('scooter')
            ->setModelPortail('moto')
            ->setMarkPortail('Honda')
            ->setReleaseDate(new \DateTime())
            ->setPublished(true)
        ;
        
        $vehicleAd->addVehicleAdGallery($vehicleAdGallery);
        
        $manager->persist($vehicleAd);
        
        $manager->flush();
    }
    
    
    
    
    /**
     * @param ObjectManager $manager
     */
    private function createdMulti(ObjectManager $manager)
    {
        $titles = ['HONDA XL VARADERO 1000', 'HONDA GL GOLDWING 1500', 'HONDA CB 1000', 'HONDA CROSSTOURER'];
        $images = $this->getImages();
        
        for ($i = 1; $i <=13; $i++) {
    
            $randKey = array_rand($titles, 1);
            $imageRandKey = array_rand($images, 1);
            $index = $i;
            
            $media = new Media();
            $media->setBinaryContent($images[$imageRandKey]);
            $media->setContext('vehicleAd');
            $media->setProviderName('sonata.media.provider.image');
            $media->setDistributor($this->distributor->getId());
    
            $this->mediaManager->save($media);
    
    
            $vehicleAdGallery = new VehicleAdGallery();
            $vehicleAdGallery
                ->setImage($media)
                ->setIsMainImage(true)
            ;
            
            $vehicleAd = new VehicleAd();
            $vehicleAd
                ->setDistributor($this->distributor)
                ->setTitle($titles[$randKey])
                ->setColor('blanc ' .  $index)
                ->setCylinder(150)
                ->setDescription("HONDA CB 1000 blanc 9cv "  . $index)
                ->setKm("29634 ")
                ->setPrice("5990")
                ->setWarrant("N.C.")
                ->setFirstHand(false)
                ->setOptions('CB - 1000 R ABS ' .  $index)
                ->setModelYear("2009")
                ->setCategoryPortail('scooter')
                ->setModelPortail('moto')
                ->setMarkPortail('Honda')
                ->setReleaseDate(new \DateTime())
                ->setPublished(true)
            ;
    
            $vehicleAd->addVehicleAdGallery($vehicleAdGallery);
    
            $manager->persist($vehicleAd);
        }
        
        
        $manager->flush();
    }
    
    
    protected function getImages()
    {
        $dir = $this->getDataDirectory();
        $imgDir = $dir . '/img';
        
        return [
            $imgDir . '/moto-1.jpg',
            $imgDir . '/moto-1-1.jpg',
            $imgDir . '/moto-1-2.jpg',
            $imgDir . '/moto-2.jpg',
            $imgDir . '/moto-3.jpg',
        ];
    }
    
    
    public function getData()
    {
        $dir = $this->getDataDirectory();
        $imgDir = $dir . '/img';
        return [
            'vehicleAd1' => $imgDir . '/moto-1.jpg',
            'vehicleAd11' => $imgDir . '/moto-1-1.jpg',
            'vehicleAd12' => $imgDir . '/moto-1-2.jpg',
            'vehicleAd2' => $imgDir . '/moto-2.jpg',
            'vehicleAd3' => $imgDir . '/moto-3.jpg',
        ];
    }
    
    
    public function getDependencies()
    {
        return array(
            DistributorFixtures::class,
        );
    }
}
