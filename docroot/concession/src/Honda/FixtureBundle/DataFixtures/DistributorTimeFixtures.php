<?php


namespace Honda\FixtureBundle\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\DistributorTime;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


/**
 * Class DistributorTimeFixtures
 * @package Honda\FixtureBundle\DataFixtures
 */
class DistributorTimeFixtures extends Fixture implements DependentFixtureInterface
{
    
    
    public function load(ObjectManager $manager)
    {
        
        $distributorTime = new DistributorTime();
        $distributorTime
            ->setTitle('Lundi matin')
            ->setStartHour(new \DateTime('1970-01-01 08:30'))
            ->setEndHour(new \DateTime('1970-01-01 12:30'))
            ->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1))
            ->setPosition(0)
        ;
        
        $manager->persist($distributorTime);
    
        $distributorTime1 = new DistributorTime();
        $distributorTime1
            ->setTitle('Lundi apres midi')
            ->setStartHour(new \DateTime('1970-01-01 14:30'))
            ->setEndHour(new \DateTime('1970-01-01 18:30'))
            ->setDistributor($this->getReference(DistributorFixtures::DISTRIBUTOR__1))
            ->setPosition(1)
        ;
    
        $manager->persist($distributorTime1);
        
        $manager->flush();
    }
 
    
    public function getDependencies()
    {
        return array(
            DistributorFixtures::class,
        );
    }
    
}
