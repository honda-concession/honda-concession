<?php

namespace Honda\MainBundle\Form\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Honda\MainBundle\Form\Type\DepartmentType;
use Honda\MainBundle\Form\Type\RegionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints As Constraints;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type As Type;
use Sonata\MediaBundle\Form\Type\MediaType;

/**
 * Class DistributorType
 * @package Honda\MainBundle\Form\Admin
 */
class DistributorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', Type\HiddenType::class, [
                    'attr' => ['readonly' => true]
                ]
            )
            ->add('name', Type\TextType::class, [
                    'label' => 'Nom de votre concession',
                    'constraints' => [new Constraints\NotBlank()]
                ]
            )
            ->add('address', Type\TextareaType::class, [
                    'label' => 'Adresse postale',
                    'attr' => [
                        'rows' => 3
                    ],
                    'constraints' => [
                        new Constraints\NotBlank(),
                    ]
                ]
            )
            ->add('other_address', Type\TextareaType::class, [
                    'label' => 'Complément d’adresse postale',
                    'required' => false,
                    'attr' => [
                        'rows' => 3
                    ],
                ]
            )
            ->add('descriptionHome', Type\TextareaType::class, [
                    'label' => 'Description',
                    'required' => false,
                ]
            )
            ->add('cp', Type\TextType::class, [
                    'label' => 'Code postal',
                    'constraints' => [new Constraints\NotBlank()]
                ]
            )
            ->add('town', Type\TextType::class, [
                    'label' => 'Ville',
                    'constraints' => [new Constraints\NotBlank()]
                ]
            )
            ->add('department', DepartmentType::class, [
                    'label' => 'Département',
                    'constraints' => [new Constraints\NotBlank()]
                ]
            )
            ->add('region', RegionType::class, [
                    'label' => 'Région',
                    'constraints' => [new Constraints\NotBlank()]
                ]
            )
            ->add('lat', Type\TextType::class, [
                    'label' => 'Latitude',
                    'constraints' => [new Constraints\NotBlank()]
                ]
            )
            ->add('lng', Type\TextType::class, [
                    'label' => 'Longitude',
                    'constraints' => [new Constraints\NotBlank()]
                ]
            )
            ->add('url', Type\TextType::class, [
                    'label' => 'Url de votre site',
                    'attr' => ['readonly' => true],
                    'constraints' => [
                        new Constraints\NotBlank(),
                        new Constraints\Regex('/^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,8}(:[0-9]{1,5})?(\/.*)?$/'),
                    ]
                ]
            )
            ->add('mail', Type\EmailType::class, [
                    'label' => 'Email',
                    'attr' => [
                        'readonly' => true
                    ]
                ]
            )
            ->add('phone', Type\TextType::class, [
                    'label' => 'Téléphone',
                    'required' => false
                ]
            )
            ->add('fax', Type\TextType::class, [
                    'label' => 'Fax',
                    'required' => false
                ]
            )
            ->add('timetable', Type\TextareaType::class)
            ->add('gaCode', Type\TextType::class, [ 'required' => false, 'attr' => ['readonly' => true]])
            
            ->add('insertPicture', MediaType::class, array(
                'label' => 'Image de présentation',
                'required' => false,
                'provider' => 'sonata.media.provider.image',
                'context'  => 'insertPicture'
            ))
        
        
        ;
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'csrf_protection' => false,
            'label' => false
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_distributorbundle_admin_infos';
    }
    
}
