<?php

namespace Honda\MainBundle\Form\Admin;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Component\Form\Extension\Core\Type As Type;

/**
 * Class SupportMessageType
 * @package Honda\MainBundle\Form\Admin
 */
class SupportMessageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextareaType::class, ['label' => 'Message', 'required' => true])
            ->add('priority', Type\ChoiceType::class, ['choices' => ['basse'=> 20, 'moyenne'=> 21,'haute'=> 22], 'label' => 'Priorité'])
            ->add('status', Type\ChoiceType::class, ['choices' => ['ouvert'=> 10, 'en cours'=> 11,'informations demandées'=> 12,'en attente' => 13,'résolu' => 14,'fermé' => 15], 'label' => 'Condition'])
            ->add('file', FileType::class, ['label' => 'Pièce jointe' , 'required' => false])
            ->add('distributor_portail_id', Type\HiddenType::class, [])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_mainbundle_admin_support_message';
    }
    
    
}
