<?php

namespace Honda\MainBundle\Form\Admin;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class DistributorSeoContentType
 * @package Honda\MainBundle\Form\Admin
 */
class DistributorSeoContentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seoContent', SeoContentType::class, [
                    'data_class' => 'Honda\MainBundle\Entity\SeoContent'
                ]
            )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Honda\MainBundle\Entity\Distributor'
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_distributorbundle_admin_distributor_seo_content';
    }
    
}
