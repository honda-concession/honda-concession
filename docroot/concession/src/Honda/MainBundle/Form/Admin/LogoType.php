<?php

namespace Honda\MainBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\MediaBundle\Form\Type\MediaType;

/**
 * Class LogoType
 * @package Honda\MainBundle\Form\Admin
 */
class LogoType extends AbstractType
{
  
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('logo', MediaType::class, [
                'provider' => 'sonata.media.provider.image',
                'context'  => 'logo',
                'attr' => ['class' => 'image-field']
            ]);
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Honda\MainBundle\Entity\Distributor'
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_mainbundle_admin_logo';
    }
    
}
