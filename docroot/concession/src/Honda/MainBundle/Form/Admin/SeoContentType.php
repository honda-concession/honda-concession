<?php

namespace Honda\MainBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type As Type;


/**
 * Class SeoContentType
 * @package Honda\MainBundle\Form\Admin
 */
class SeoContentType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descriptionHome', Type\TextareaType::class, [
                'label' => 'SEO Accueil',
                'required' => false,
            ])
            ->add('descriptionPageVO', Type\TextareaType::class, [
                'label' => 'SEO - Accueil véhicules d\'occasion',
                'required' => false,
            ] )
            ->add('descriptionPageFicheVo', Type\TextareaType::class, [
                'label' => 'SEO - Fiche véhicule d\'occasion',
                'required' => false,
            ] )
            ->add('descriptionPageServices', Type\TextareaType::class, [
                'label' => 'SEO - Page Services',
                'required' => false,
            ])
            ->add('descriptionPageAccessoires', Type\TextareaType::class, [
                'label' => 'SEO - Page Accessoires',
                'required' => false,
            ] )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Honda\MainBundle\Entity\SeoContent'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_mainbundle_admin_seo_content';
    }
}
