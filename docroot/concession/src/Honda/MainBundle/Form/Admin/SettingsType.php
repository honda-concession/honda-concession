<?php

namespace Honda\MainBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Sonata\MediaBundle\Form\Type\MediaType;

/**
 * Class SettingsType
 * @package Honda\MainBundle\Form\Admin
 */
class SettingsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bgColor', ColorType::class)
            ->add('phone')
            
            ->add('logo', MediaType::class, array(
                'provider' => 'sonata.media.provider.image',
                'context'  => 'default'
            ));
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Honda\MainBundle\Entity\Distributor'
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_mainbundle_admin_settings';
    }
    
    
}
