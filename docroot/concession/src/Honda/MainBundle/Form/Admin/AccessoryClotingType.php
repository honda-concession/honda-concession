<?php

namespace Honda\MainBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type As Type;
use Sonata\MediaBundle\Form\Type\MediaType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

/**
 * Class AccessoryClotingType
 * @package Honda\MainBundle\Form\Admin
 */
class AccessoryClotingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', CKEditorType::class, ['required' => false, 'label' => 'Présentation', 'attr' => ['rows' => 6]])
            ->add('linkLabel', null, ['label' => 'Label du lien vers le catalogue Honda. Ce label n\'est pas modifiable', 'attr' => ['readonly' => 'readonly']])
            ->add('link', null, ['label' => 'Lien vers le catalogue Honda. Ce lien n’est pas modifiable', 'attr' => ['readonly' => 'readonly']])
            ->add('pdfLabel', null, ['label' => 'Label de votre catalogue (PDF) - Facultatif'])
            ->add('pdf', MediaType::class, [
                'label' => 'Votre catalogue (PDF) - Facultatif',
                'provider' => 'sonata.media.provider.file',
                'context' => 'accessory',
            ])
            ->add('image', MediaType::class, [
                'provider' => 'sonata.media.provider.image',
                'context'  => 'accessory',
                'attr' => ['class' => 'image-field']
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Honda\MainBundle\Entity\Accessory'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_mainbundle_admin_accessory';
    }
}
