<?php

namespace Honda\MainBundle\Form\Admin;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Component\Form\Extension\Core\Type As Type;

/**
 * Class SupportType
 * @package Honda\MainBundle\Form\Admin
 */
class SupportType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject', null, ['label' => 'Objet de votre demande'])
            ->add('message', TextareaType::class, ['label' => 'Message', 'required' => true])
            ->add('priority', Type\ChoiceType::class, ['choices' => ['basse'=> 20, 'moyenne'=> 21,'haute'=> 22], 'label' => 'Priorité'])
            ->add('file', FileType::class, ['label' => 'Pièce jointe' , 'required' => false])
            ->add('distributor_portail_id', Type\HiddenType::class, [])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'honda_mainbundle_admin_support';
    }
    
    
}
