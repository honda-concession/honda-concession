<?php

namespace Honda\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Honda\MainBundle\Model\Middleware\GenericGateway;

class RegionType extends AbstractType
{
    
    /**
     * @var GenericGateway
     */
    protected $genericGateway;
    
    /**
     * @var Array
     */
    protected $endPoints;
    
    /**
     * @param GenericGateway $genericGateway
     * @param array $endPoints
     */
    public function __construct(GenericGateway $genericGateway, $endPoints = [])
    {
        $this->genericGateway = $genericGateway;
        $this->endPoints = $endPoints;
    }
    
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $regions = $this->genericGateway->getData($this->endPoints['regions'], true);
        
        $resolver->setDefaults([
            'choices' => $regions,
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

}
