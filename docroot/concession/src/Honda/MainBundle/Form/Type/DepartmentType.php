<?php

namespace Honda\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Honda\MainBundle\Model\Middleware\GenericGateway;

class DepartmentType extends AbstractType
{
    
    /**
     * @var GenericGateway
     */
    protected $genericGateway;
    
    /**
     * @var Array
     */
    protected $endPoints;
    
    /**
     * @param GenericGateway $genericGateway
     * @param array $endPoints
     */
    public function __construct(GenericGateway $genericGateway, $endPoints = [])
    {
        $this->genericGateway = $genericGateway;
        $this->endPoints = $endPoints;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        
        $departements = $this->genericGateway->getData($this->endPoints['departements'], true);
        
        $resolver->setDefaults([
            'choices' => $departements,
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
