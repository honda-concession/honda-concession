<?php

namespace Honda\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class VoCheckboxType
 * @package Honda\MainBundle\Form\Type
 */
class VoCheckboxType extends AbstractType
{
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAttribute('checkboxes', $options['checkboxes'])
            ->setAttribute('object', $options['object'])
            ->setAttribute('checkboxes_type', $options['checkboxes_type'])
        ;
    }
    
    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['checkboxes'] = $options['checkboxes'];
        $view->vars['object'] = $options['object'];
        $view->vars['checkboxes_type'] = $options['checkboxes_type'];
    }
    
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'label' => false,
            'checkboxes' => [],
            'object' => null,
            'checkboxes_type' => null,
        ));
       
    }
    
   public function getBlockPrefix()
   {
       return 'vo_checkboxes';
   }
}
