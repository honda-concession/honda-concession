<?php

namespace Honda\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Honda\MainBundle\Model\Middleware\PortailWrapper;

/**
 * Class PictoServicesChoiceType
 * @package Honda\MainBundle\Form\Type
 */
class PictoServicesChoiceType extends AbstractType
{
    
    /**
     * @var PortailWrapper
     */
    protected $portailWrapper;
    
    /**
     * @var Array
     */
    protected $endPoints;
    
    /**
     * PictoServicesChoiceType constructor.
     * @param PortailWrapper $portailWrapper
     * @param array $endPoints
     */
    public function __construct(PortailWrapper $portailWrapper, $endPoints = [])
    {
        $this->portailWrapper = $portailWrapper;
        $this->endPoints = $endPoints;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $choices = [];
        
        foreach ($this->portailWrapper->getGenericGateway()->getData($this->endPoints['pictosService'], true) as $data) {
            $choices[$data['picto']] = $data['picto'];
        }
        
        $resolver->setDefaults(array(
            'choices' => $choices,
            'show_as_image' => true,
        ));
    }
    
   public function getBlockPrefix()
   {
       return 'pictos_services_choices';
   }
   
   public function getParent()
   {
       return ChoiceType::class;
   }
   
}
