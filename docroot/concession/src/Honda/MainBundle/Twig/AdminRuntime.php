<?php

namespace Honda\MainBundle\Twig;

use Honda\MainBundle\Entity\Distributor;
use Twig\Extension\RuntimeExtensionInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


/**
 * Class AdminRuntime
 * @package Honda\MainBundle\Twig
 */
class AdminRuntime implements RuntimeExtensionInterface
{

    /**
     * @var SessionInterface
     */
    protected $session;

    protected $securityTokenStorage;
    protected $securityContext;
    protected $securityAuthorizationChecker;

    /**
     * AdminRuntime constructor.
     * @param SessionInterface $session
     * @param TokenStorageInterface $securityTokenStorage
     * @param AuthorizationCheckerInterface $securityAuthorizationChecker
     */
    public function __construct(SessionInterface $session, TokenStorageInterface $securityTokenStorage, AuthorizationCheckerInterface $securityAuthorizationChecker)
    {
        $this->session = $session;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->securityAuthorizationChecker = $securityAuthorizationChecker;
    }

    /**
     * @return array
     */
    public function parserDistributorSession()
    {
        $token = $this->securityTokenStorage->getToken();

        if ($this->securityAuthorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {

            $distributor = $token->getUser()->getDistributor();

            if ($distributor instanceof Distributor) {

                return $distributor->getInfos();
            }
        }

        return false;
    }
}
