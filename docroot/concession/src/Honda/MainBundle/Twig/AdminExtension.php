<?php

namespace Honda\MainBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;


class AdminExtension extends AbstractExtension
{
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('parserDistributorSession', array(AdminRuntime::class, 'parserDistributorSession')),
            new TwigFunction('winteamQuota', [ContainerAwareRuntime::class, 'getWinteamQuota']),
            new TwigFunction('winteamPublication', [ContainerAwareRuntime::class, 'getWinteamPublication']),
        );
    }
    
}
