<?php

namespace Honda\MainBundle\Twig;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\VehicleAd;
use Honda\MainBundle\Model\Middleware\PortailWrapper;
use Twig\Extension\RuntimeExtensionInterface;
use Symfony\Component\Templating\EngineInterface;
use Honda\MainBundle\Menu\Builder;

/**
 * Class FrontRuntime
 * @package Honda\MainBundle\Twig
 */
class FrontRuntime implements RuntimeExtensionInterface
{
    /**
     * @var PortailWrapper
     */
    protected $portailWrapper;

    /**
     * @var Array
     */
    protected $endpoints;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var Builder
     */
    protected $builder;

    /**
     * FrontRuntime constructor.
     * @param PortailWrapper $portailWrapper
     */
    public function __construct(PortailWrapper $portailWrapper, $endpoints, EngineInterface $templating, Builder $builder)
    {
        $this->portailWrapper = $portailWrapper;
        $this->endpoints = $endpoints;
        $this->templating = $templating;
        $this->builder = $builder;
    }

    /**
     * @return array|mixed
     */
    public function menuItems()
    {
        $menuItems = $this->builder->mainMenu();

        return $menuItems;
    }

    /**
     * @param Distributor $distributor
     * @return string
     */
    public function themeBgColor($distributor)
    {
        return (!is_null($distributor) && $distributor->getBgColor()) ?  sprintf("class=%s", $distributor->getBgColor()) : null;
    }

    /**
     * @param Distributor $distributor
     * @param $distributorSearchedInfo
     * @return mixed|null
     */
    public function getDistributorInfo($distributor, $distributorSearchedInfo)
    {
        if (!$distributor instanceof Distributor) {
            return;
        }

        $infos = (array) $distributor->getInfos();

        if (count($infos) && isset($infos[$distributorSearchedInfo])) {
            return $infos[$distributorSearchedInfo];
        }

        return;
    }

    /**
     * @param array $portailSubModel
     * @param array $submodelKeys
     * @return bool
     */
    public function checkSubModelOnTest($portailSubModel, $submodelKeys = array())
    {
        if (is_array($portailSubModel)) {
            if (count(array_intersect($portailSubModel, $submodelKeys))) {
                return true;
            }
        } else {
            if (isset(array_flip($submodelKeys)[$portailSubModel])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $vehicles
     * @param array $inventoryKeys
     * @return array
     */
    public function checkInventory($vehicles = array(), $inventoryKeys = array())
    {
        $tmp = [];
        foreach ($vehicles as $vehicle) {

            $vehicle = (array) $vehicle;
            if (isset($vehicle['vehicles_id'])) {
                $tmp[$vehicle['vehicles_id']] = $vehicle;
            }
        }

        $intersect = array_intersect(array_keys($tmp), $inventoryKeys);

        if (count($intersect)) {

            $tab = [];
            foreach ($intersect as $intersectKey) {

                if (isset($tmp[$intersectKey])) {
                    $tab[] = $tmp[$intersectKey];
                }
            }

            return $tab;
        }

        return [];
    }

    /**
     * @param VehicleAd $vehicleAd
     * @return string
     */
    public function getContentMailto(VehicleAd $vehicleAd)
    {
        $body = $this->templating->render('@HondaMain/Front/VehicleAd/_mailto.txt.twig', ['vehicleAd' => $vehicleAd]);

        return sprintf('?subject=%s&body=%s',"J'ai vu ce véhicule d'occasion sur le portail des occasions Honda, jette un oeil.", $body);
    }

    /**
     * @param $categorySlug
     * @param $categorySlugToNameCollection
     * @return null
     */
    public function categoriesSlugToName($categorySlug, $categorySlugToNameCollection = [])
    {
        if (isset($categorySlugToNameCollection[$categorySlug]) && $categorySlugToNameCollection[$categorySlug]) {

            return $categorySlugToNameCollection[$categorySlug];
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCookieMessage()
    {
        $message = $this->portailWrapper->getGenericGateway()->getData($this->endpoints['cookieMessage'], true);

        if (is_array($message)) {

            return isset($message['message']) ? $message['message'] : null;
        }

        return null;
    }

    /**
     * @param submodel $
     * @return string
     */
    public function motoSelectionner($submodel)
    {
        return \GuzzleHttp\json_encode(['id' => 1]);
    }
}
