<?php

namespace Honda\MainBundle\Twig;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\VehicleAd;
use Twig\Extension\RuntimeExtensionInterface;
use Honda\MainBundle\Entity\Inventory;
use Honda\MainBundle\Entity\SubModelTest;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;

/**
 * Class FrontRuntime
 * @package Honda\MainBundle\Twig
 */
class ContainerAwareRuntime implements RuntimeExtensionInterface
{

    protected $container;

    private $templating;

    private $em;

    /**
     * ContainerAwareRuntime constructor.
     */
    public function __construct($container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->templating = $this->container->get('templating');
    }

    public function getKoraScript(array $currentModel = null)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $routeName = $request->get('_route');
        $default = 'DEFAULT';
        switch($routeName) {
            case 'new_vehicle_show':
            case 'new_vehicle_list':
                $default = 'VN';
                break;
            case 'vehicule_occasion_voir':
                $default = 'VO';
                break;
        }
        $koraIdxForSetDispoStock = $this->em->getRepository(Inventory::class)
                                            ->getKoraIdx($this->getDistributor(), $currentModel ? $propertyAccessor->getValue($currentModel, '[model_name]') : null);
        $koraIdxForTest = $this->em->getRepository(SubModelTest::class)
                                    ->getKoraIdx($this->getDistributor(), $currentModel ? $propertyAccessor->getValue($currentModel, '[model_name]') : null);

        return $this->templating->render('@HondaMain/Front/_kora_script.html.twig', compact(
            'koraIdxForSetDispoStock',
            'koraIdxForTest',
            'default'
        ));
    }

    public function getGtmScript()
    {
        if ($gtmCode = $this->getDistributor()->getGtmCode()) {
            return $this->renderGtmTemplate('_gtm_script', $gtmCode);
        }

        return null;
    }

    public function getGtmNoScript()
    {
        if ($gtmCode = $this->getDistributor()->getGtmCode()) {
            return $this->renderGtmTemplate('_gtm_no_script', $gtmCode);
        }

        return null;
    }

    public function getWinteamQuota()
    {
        if ($distributor = $this->getUserDistributor()) {
            $portailWrapper = $this->container->get('Honda\MainBundle\Model\Middleware\PortailWrapper');
            $distributorPortail = $portailWrapper->getDistributorGateway()->getDistributorInfos($distributor);
            $diffusion = $distributorPortail['diffusion'] ?? 0;
            $quota = $diffusion['Le Bon Coin'] ?? 0;

            return $quota;
        }

        return null;
    }

    public function getWinteamPublication()
    {
        if ($distributor = $this->getUserDistributor()) {
            $responses = $this->em->getRepository(VehicleAd::class)->findWinteamPublicationCount($distributor);
            $response = end($responses);

            return $response['count'];
        }

        return 0;
    }

    private function renderGtmTemplate($template, $gtmCode)
    {
        return $this->templating->render("@HondaMain/Front/{$template}.html.twig", [
            'gtmCode' => $gtmCode
        ]);
    }

    protected function getUserDistributor()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if ($user) {
            return $user->getDistributor();
        }

        return null;
    }

    protected function getDistributor()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $distributor = $request->attributes->get(AbstractFrontController::ACTIVE_DOMAIN_KEY);
        if ($distributor instanceof Distributor) {
            return $distributor;
        }

        throw new \Exception('Concessionaire non trouvé.');
    }
}
