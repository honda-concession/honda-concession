<?php

namespace Honda\MainBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class FrontExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return array(
            new TwigFunction('menuItems', array(FrontRuntime::class, 'menuItems')),
            new TwigFunction('checkSubModelOnTest', array(FrontRuntime::class, 'checkSubModelOnTest')),
            new TwigFunction('checkInventory', array(FrontRuntime::class, 'checkInventory')),
            new TwigFunction('displayPresentationTable', array(FrontRuntime::class, 'displayPresentationTable')),
            new TwigFunction('getContentMailto', array(FrontRuntime::class, 'getContentMailto')),
            new TwigFunction('categoriesSlugToName', array(FrontRuntime::class, 'categoriesSlugToName')),
            new TwigFunction('getCookieMessage', array(FrontRuntime::class, 'getCookieMessage')),
            new TwigFunction('motoSelectionner', array(FrontRuntime::class, 'motoSelectionner')),
            new TwigFunction('getDistributorInfo', array(FrontRuntime::class, 'getDistributorInfo')),
            new TwigFunction('gtmScript', array(ContainerAwareRuntime::class, 'getGtmScript')),
            new TwigFunction('gtmNoScript', array(ContainerAwareRuntime::class, 'getGtmNoScript')),
            new TwigFunction('koraScript', array(ContainerAwareRuntime::class, 'getKoraScript')),
        );
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('themeBgColor', array(FrontRuntime::class, 'themeBgColor')),
        );
    }
}
