<?php

namespace Honda\MainBundle\Mailer;

use SendinBlue\Client\Configuration;
use SendinBlue\Client\Api\SMTPApi;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Templating\EngineInterface;

use Honda\MainBundle\Model\Middleware\GenericGateway;

/**
 * Class AbstractMailer
 * @package BBY\MainBundle\Mailer
 */
abstract class AbstractMailer
{
    /**
     * @var
     */
    protected $mailer;

    /**
     * @translator
     */
    protected $translator;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var GenericGateway
     */
    protected $genericGateway;

    /**
     * @var array
     */
    protected $endpoints;

    /**
     * AbstractMailer constructor.
     * @param SMTPApi $mailer
     * @param TranslatorInterface $translator
     * @param EngineInterface $templating
     * @param GenericGateway $genericGateway
     * @param array $endpoints
     */
    public function __construct($apiKey, TranslatorInterface $translator, EngineInterface $templating, GenericGateway $genericGateway, $endpoints = array())
    {
        $this->translator = $translator;
        $this->templating = $templating;
        $this->genericGateway = $genericGateway;
        $this->endpoints = $endpoints;
        $config = Configuration::getDefaultConfiguration()->setApiKey('api-key', $apiKey);
        $this->mailer = new SMTPApi(null,$config);
    }

}
