<?php

namespace Honda\MainBundle\Mailer;

use SendinBlue\Client\Model\SendSmtpEmail;

use Honda\MainBundle\Entity\Distributor;


/**
 * Class Mailer
 * @package Honda\MainBundle\Mailer
 */
class Mailer extends AbstractMailer
{

    public function send(array $emailData)
    {
        $sendSmtpEmail = new SendSmtpEmail($emailData);
        try {
            $result = $this->mailer->sendTransacEmail($sendSmtpEmail);

            return true;
        } catch (\SendinBlue\Client\ApiException $e) {
            echo 'Exception when calling SMTPApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;die();
        }
    }

    /**
     * @param null $ticketObject
     * @param array $to
     * @param bool $isReply
     * @return bool|void
     * @throws \SendinBlue\Client\ApiException
     */
    public function messageNotificationCentral($ticketObject = null, $to = array(), $isReply = false)
    {
        $emails = $this->genericGateway->getData($this->endpoints['emailsNotification'], true);

        $ticketId = $ticketObject->ticketId ?? null;

        if (!$isReply) {
            $subject = 'Honda Motos, nouveau ticket crée';
            if ($ticketId) {
                $subject = 'Honda Motos / Ticket concessionnaire n° ' . $ticketId;
            }
        } else {
            $subject = 'Honda Motos / Ticket concessionnaire n° ' . $ticketId;
        }

        if (count($to)) {
            $emails = $to;
        }

        if (count($emails)) {
            $emailsTo = [];

            foreach ($emails as $email) {
                $emailsTo[] = ['name' => $email, 'email' => $email];
            }

            $emailData = [
                'subject' => $subject,
                'sender' => ['name' => 'Support Prodigious', 'email' => 'support.prodigious@notification.honda.fr'],
                'to' => $emailsTo,
                'htmlContent' => $this->templating->render('@HondaMain/Admin/Emails/central_notification.html.twig', ['ticketObject' => $ticketObject, 'isReply' => $isReply]),
                'headers' => ['Content-Type' => 'text/html',  'charset' =>  'utf8'],
            ];
            $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail($emailData);
            try {
                $this->mailer->sendTransacEmail($sendSmtpEmail);

                return true;
            } catch (\SendinBlue\Client\ApiException $e) {
                echo 'Exception when calling SMTPApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
            }
        }

        return;
    }
}
