<?php
namespace Honda\MainBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use FOS\ElasticaBundle\Event\TransformEvent;
use Honda\MainBundle\Entity\VehicleAdGallery;
use Honda\MainBundle\Entity\Distributor;

/**
 * Class PreTransformListener
 * @package Honda\MainBundle\EventListener
 */
class PreTransformListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var DistributorRepository
     */
    protected $distributorRepo;

    /**
     * @var array
     */
    protected $sites;

    /**
     * @var ManagerInterface
     */
    private $mediaManager;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->distributorRepo = $this->container->get('doctrine')->getRepository(Distributor::class);
        $this->sites = $this->getSites();
        $this->mediaManager = $this->container->get('sonata.media.manager.media');
    }

    public function doPreTransform(TransformEvent $event)
    {
        $object = $event->getObject();
        if($object instanceof VehicleAdGallery) {
            $media = $object->getImage();
            
            if(!empty($media)) {
                $providerName = $media->getProviderName();
                $provider = $this->container->get($providerName);
                $format = $provider->getFormatName($media, 'medium');
                $distributorId = $media->getDistributor();
                if(!empty($distributorId)) {
                    $distributor = $this->distributorRepo->find($distributorId);
                    if($distributor) {
                        $domain = $distributor->getDomain();
                        $uploadFolder = $this->sites[$domain] ?? 'app';
                        $meidaUrl = str_replace('/app/', '/app_'.$uploadFolder.'/', $provider->generatePublicUrl($media, $format));
                        $object->setMediaUrl($meidaUrl);

                        switch ($providerName) {
                            case 'sonata.media.provider.image':
                                $object->setMediaType('image');
                                $object->setReference($provider->getReferenceImage($media));
                                break;
                            case 'sonata.media.provider.youtube':
                                $object->setMediaType('video');
                                $object->setReference($provider->getReferenceUrl($media));
                                break;
                            default:
                                $object->setMediaUrl(null);
                                break;
                        }
                    }
                }
            }
        }

    }
    
    public static function getSubscribedEvents()
    {
        return array(
            TransformEvent::PRE_TRANSFORM => 'doPreTransform',
        );
    }

    public function getSites()
    {
        $sm = $this->container->get('multisite.manager');
        $sites = $sm->list();
        $arrFliped = [];
        foreach ($sites as $folder => $infos) {
            foreach ($infos as $info) {
                $arrFliped[$info['host']] = $folder;
            }
        }

        return $arrFliped;
    }
}
