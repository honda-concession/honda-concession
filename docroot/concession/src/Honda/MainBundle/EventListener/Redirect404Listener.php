<?php

namespace Honda\MainBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;

class Redirect404Listener
{
    /**
     * @var RouterInterface
     */
    private $router;

    private $redirectionUrls = [];

    /**
     * @var RouterInterface $router
     */
    public function __construct(RouterInterface $router, array $redirectionUrls)
    {
        $this->router = $router;
        $this->redirectionUrls = $redirectionUrls;
    }

    /**
     * @var GetResponseForExceptionEvent $event
     * @return null
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // If not a HttpNotFoundException ignore
        if (!$event->getException() instanceof NotFoundHttpException) {
            return;
        }
        // Create redirect response with url for the home page
        $response = new RedirectResponse($this->router->generate('homepage'), 301);

        $context = $this->router->getContext();
        foreach($this->redirectionUrls as $redirectionUrls) {
            $routeName = end($redirectionUrls);
            $originalUrl = key($redirectionUrls);
            if (preg_match("/{$originalUrl}/", $context->getPathInfo())) {
                $response = new RedirectResponse($this->router->generate($routeName), 301);
            }
        }

        // Set the response to be processed
        $event->setResponse($response);
    }
}
