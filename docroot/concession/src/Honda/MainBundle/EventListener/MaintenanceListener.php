<?php


namespace Honda\MainBundle\EventListener;

use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class MaintenanceListener
{
    
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
    
        if ($exception instanceof ServiceUnavailableHttpException) {
    
            $exception = new ServiceUnavailableHttpException('Some special exception');
            $event->setException($exception);
        }
    }
    
}
