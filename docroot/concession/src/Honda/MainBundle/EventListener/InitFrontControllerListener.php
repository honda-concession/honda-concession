<?php

namespace Honda\MainBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;

/**
 * Class InitFrontControllerListener
 * @package Honda\MainBundle\EventListener
 */
class InitFrontControllerListener
{
    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        
        $controller = $event->getController();
        
        if (!is_array($controller)) {
            return;
        }
        
        $controller = $controller[0];
        
        if (!$controller instanceof AbstractFrontController) {
            return;
        }

        if (method_exists($controller,  'initAction')) {
            $controller->initAction();
        } else {
            @trigger_error(sprintf('Method "initAction" is required'));
        }
        
    }
    
}
