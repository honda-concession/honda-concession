<?php
namespace Honda\MainBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

use Honda\MainBundle\Entity\Distributor;

/**
 * Listener for Request
 */
class RequestListener
{

    private $requestStack;
    private $em;
    private $session;
    private $securityTokenStorage;
    private $securityAuthorizationChecker;

    /**
     * set encryption class
     *
     * @param string $key
     */
    public function __construct(SessionInterface $session, TokenStorageInterface $securityTokenStorage, AuthorizationCheckerInterface $securityAuthorizationChecker, $em, $requestStack)
    {
        $this->session = $session;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->securityAuthorizationChecker = $securityAuthorizationChecker;
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    /**
     * handle event on kernel request
     *
     * @param GetResponseEvent $event
     * @return void
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $token = $this->securityTokenStorage->getToken();
        if ($token && $this->securityAuthorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $token->getUser();
            if ($user->hasRole('ROLE_ROOT')) {
                $user->setDistributor($this->getDistributor());
                $this->em->persist($user);
                $this->em->flush($user);
            }
        }
    }

    private function getDistributor()
    {
        $distributor = $this->em->getRepository(Distributor::class)->findOneByDomain($this->getCurrentDomain());

        return $distributor;
    }

    protected function getCurrentDomain()
    {
        $request = $this->requestStack->getCurrentRequest();

        return $request->getHttpHost();
    }
}
