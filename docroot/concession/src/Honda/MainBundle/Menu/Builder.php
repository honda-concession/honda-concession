<?php

namespace Honda\MainBundle\Menu;

use Symfony\Component\Routing\RouterInterface;

use Honda\MainBundle\Model\Middleware\PortailWrapper;
use Honda\MainBundle\Model\Front\NewsManager;
use Honda\MainBundle\Model\Front\ServiceManager;
use Honda\MainBundle\Model\Front\VehicleAdManager;
use Honda\MainBundle\Entity\Distributor;

class Builder
{

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var PortailWrapper
     */
    protected $portailWrapper;

    /**
     * @var Array
     */
    protected $endpoints;

    private $serviceManager;

    private $newsManager;

    private $vehicleAdManager;

    /**
     * Builder constructor.
     * @param RouterInterface $router
     * @param PortailWrapper $portailWrapper
     * @param $endpoints
     */
    public function __construct(RouterInterface $router, PortailWrapper $portailWrapper, $endpoints, ServiceManager $serviceManager, NewsManager $newsManager, VehicleAdManager $vehicleAdManager)
    {
        $this->router = $router;
        $this->portailWrapper = $portailWrapper;
        $this->endpoints = $endpoints;
        $this->serviceManager = $serviceManager;
        $this->newsManager = $newsManager;
        $this->vehicleAdManager = $vehicleAdManager;
    }

    /**
     * @param bool $siteMap
     * @return array
     */
    public function mainMenu(Distributor $distributor = null, $siteMap = false)
    {
        $urls = [];

        $children = [];

        $menuItems = $this->portailWrapper->getGenericGateway()->getData($this->endpoints['menus'], true);

        if ($siteMap) {
            $urls[] = [
                'routeName' => 'homepage',
                'name' => 'Accueil',
                'url' =>  $this->router->generate('homepage', [], RouterInterface::ABSOLUTE_URL),
                'children' => [],
                'sitemap_children' => []
            ];
        }

        foreach ($menuItems as $item) {
            $gammeTarifs = $this->portailWrapper->getGenericGateway()->getData($this->endpoints['gammeTarifs'] . '/' . $item['category_slug']);
            foreach ($gammeTarifs->categories as $category) {
                $modelChildren = [];
                foreach ($category->models as $model) {
                    foreach ($model->submodels as $submodel) {
                        $routeParameters = ['categorySlug' =>  $category->category_slug, 'modelSlug' => $model->slug, 'vehicleSlug' => $submodel->vehicle_slug];
                        $modelChildren[] = [
                            'routeName' => 'new_vehicle_list',
                            'name' => $submodel->submodel_name,
                            'url' =>  $this->router->generate('new_vehicle_show', $routeParameters, RouterInterface::ABSOLUTE_URL),
                            'children' => [],
                            'sitemap_children' => [],
                        ];
                    }
                }
                if ($category->hasRedirection) {
                    $children[] = [
                        'routeName' => 'redirect_to',
                        'name' => $category->category,
                        'url' =>  $this->router->generate('redirect_to', ['url' =>  urlencode($category->redirectionUrl)], RouterInterface::ABSOLUTE_URL),
                        'children' => [],
                        'sitemap_children' => [],
                    ];
                } else {
                    $children[] = [
                        'routeName' => 'new_vehicle_list',
                        'name' => $category->category,
                        'url' =>  $this->router->generate('new_vehicle_list', ['categorySlug' =>  $category->category_slug], RouterInterface::ABSOLUTE_URL),
                        'children' => $modelChildren,
                        'sitemap_children' => $modelChildren,
                    ];
                }
            }
        }

        $urls[] = [
            'routeName' => 'new_vehicle_list',
            'name' => 'Gamme et tarifs',
            'url' =>  $this->router->generate('new_vehicle_list', [], RouterInterface::ABSOLUTE_URL),
            'children' => $children,
            'sitemap_children' => $children
        ];

        $urls[] = [
            'routeName' => 'vehicule_occasion',
            'name' => 'Occasions',
            'url' =>  $this->router->generate('vehicule_occasion', [], RouterInterface::ABSOLUTE_URL),
            'children' => [],
            'sitemap_children' => $distributor ? $this->getVehicleOccasionUrls($distributor) : [],
        ];

        $urls[] = [
            'routeName' => 'services',
            'name' => 'Services',
            'url' =>  $this->router->generate('services', [], RouterInterface::ABSOLUTE_URL),
            'children' => [],
            'sitemap_children' => $distributor ? $this->getServiceUrls($distributor) : [],
        ];

        $urls[] = [
            'routeName' => 'accessoiries',
            'name' => 'Accessoires',
            'url' =>  $this->router->generate('accessoiries', [], RouterInterface::ABSOLUTE_URL),
            'children' => [],
            'sitemap_children' => [],
        ];

        $urls[] = [
            'routeName' => 'news',
            'name' => 'Actualités',
            'url' =>  $this->router->generate('news', [], RouterInterface::ABSOLUTE_URL),
            'children' => [],
            'sitemap_children' => $distributor ? $this->getNewsUrls($distributor) : [],
        ];

        if ($siteMap) {

            $urls[] = [
                'routeName' => 'page_show',
                'name' => 'Vie privée et cookies',
                'url' =>  $this->router->generate('page_show', ['path' => 'vie-privee-et-cookies'], RouterInterface::ABSOLUTE_URL),
                'children' => [],
                'sitemap_children' => [],
            ];

            $urls[] = [
                'routeName' => 'page_show',
                'name' => 'Mentions légales',
                'url' =>  $this->router->generate('page_show', ['path' => 'mentions-legales'], RouterInterface::ABSOLUTE_URL),
                'children' => [],
                'sitemap_children' => [],
            ];

            $urls[] = [
                'routeName' => 'page_show',
                'name' => 'Contact',
                'url' =>  $this->router->generate('page_show', ['path' => 'plan-du-site'], RouterInterface::ABSOLUTE_URL) . '#section-contact',
                'children' => [],
                'sitemap_children' => [],
            ];
        }

        return $urls;
    }

    private function getServiceUrls(Distributor $distributor)
    {
        $children = [];
        $services = $this->serviceManager->getServices($distributor);
        foreach ($services['services'] as $service) {
            $children[] = [
                'routeName' => 'services',
                'name' => $service['title'],
                'url' => $this->router->generate('services', [], RouterInterface::ABSOLUTE_URL),
                'children' => [],
                'sitemap_children' => [],
            ];
        }

        return $children;
    }

    private function getVehicleOccasionUrls(Distributor $distributor)
    {
        $children = [];
        $vehiclesAdCollection = $this->vehicleAdManager->getAllByDistributor($distributor);
        foreach ($vehiclesAdCollection as $vehicleAd) {
            $children[] = [
                'routeName' => 'vehicule_occasion_voir',
                'name' => $vehicleAd->getTitle(),
                'url' => $this->router->generate('vehicule_occasion_voir', ['slug' => $vehicleAd->getSlug()], RouterInterface::ABSOLUTE_URL),
                'children' => [],
                'sitemap_children' => [],
            ];
        }

        return $children;
    }

    private function getNewsUrls(Distributor $distributor)
    {
        $children = [];
        $resource = $this->newsManager->getNewsForPage($distributor, null, true);
        foreach ($resource['newsList'] as $item) {
            $url = null;
            $title = null;
            $news = $item['news'];
            if ($item['origin'] == NewsManager::POST_ORIGIN_CONCESSION) {
                if ($news->isHiddenInSitemap()) {
                    continue;
                }
                $url = $this->router->generate('news_show_article', ['slug' => $news->getSlug()], RouterInterface::ABSOLUTE_URL);
                if ($news->isRedirectionActive()) {
                    $url = $this->router->generate('redirect_to', ['url' => urlencode($news->getRedirectUrl())]);
                }
                $title = $news->getTitle();
            } else {
                if (!$news['showInSitemap']) {
                    continue;
                }
                $url = $this->router->generate('news_show_portail_article', ['slug' => $news['slug']], RouterInterface::ABSOLUTE_URL);
                if ($news['hasRedirection']) {
                    $url = $this->router->generate('redirect_to', ['url' => urlencode($news['redirectionUrl'])]);
                }
                $title = $news['title'];
            }
            $children[] = [
                'routeName' => 'news',
                'name' => $title,
                'url' => $url,
                'children' => [],
                'sitemap_children' => [],
            ];
        }

        return $children;
    }
}
