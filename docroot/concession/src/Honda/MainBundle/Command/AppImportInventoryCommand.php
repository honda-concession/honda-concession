<?php

namespace Honda\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Honda\MainBundle\Model\Middleware\InventoryGateway;
use Honda\MainBundle\Model\Admin\InventoryManager;

class AppImportInventoryCommand extends BaseImportCommand
{
    protected function configure()
    {
        $this
            ->setName('app:import-inventory')
            ->setDescription('import inventory from portail')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $vehiclesJson = $container->get(InventoryGateway::class)->getVehicles();
        $container->get(InventoryManager::class)->importNewVehicles($this->getDistributor(), $vehiclesJson);
        $output->writeln('<info>Mise à jour des inventaires terminés</info>');
    }

}
