<?php

namespace Honda\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use SendinBlue\Client\Model\SendSmtpEmailTo;

use Honda\MainBundle\Entity\Distributor;

class HondaSiteCheckCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('honda:site-check')
            ->setDescription('Check if site is up')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $senderEmail = $container->getParameter('mailer_user_fos');
        $mailingAddresses = $container->getParameter('error_mailing_address');
        $mailer = $container->get('Honda\MainBundle\Mailer\Mailer');
        $errors = [];
        $distributors = $this->getDistributors();
        $progressBar = new ProgressBar($output, count($distributors));
        foreach ($distributors as $distributor) {
            $output->writeln("Checking: {$distributor->getUrl()}");
            try {
                $content = file_get_contents($distributor->getUrl());
                if (strlen($content) == 0) {
                    $errors[] = $distributor->getUrl();
                }
            } catch(\Exception $e) {
                $context = $e->getContext();
                $errors[] = $context['distributor']->getUrl();
            } finally {
                $progressBar->advance();
            }
        }
        $progressBar->finish();
        if (count($errors) > 0) {
            $to = [];
            $htmlContent = implode("\n", $errors);
            foreach ($mailingAddresses as $mailingAddress) {
                $to[] = new SendSmtpEmailTo(['email' => $mailingAddress]);
            }
            $emailData = [
                'subject' => 'Honda: Les sites ne répondent plus',
                'sender' => ['name' => 'Support Prodigious', 'email' => $senderEmail],
                'to' => $to,
                'htmlContent' => nl2br("Les sites ne r&eacute;pondent plus.\n\nSite:\n{$htmlContent}"),
                'headers' => ['Content-Type' => 'text/html',  'charset' =>  'utf-8'],
            ];
            $mailer->send($emailData);
        }
    }

    private function getDistributors()
    {
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $repository = $entityManager->getRepository(Distributor::class);

        return $repository->findByActive(true);
    }

}
