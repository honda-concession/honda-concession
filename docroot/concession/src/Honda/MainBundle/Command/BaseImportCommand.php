<?php

namespace Honda\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Honda\MainBundle\Entity\Distributor;

Abstract class BaseImportCommand extends ContainerAwareCommand
{
    protected function getDistributor()
    {
        $container = $this->getContainer();
        $baseHost = $container->getParameter('base_host');

        return $container->get('doctrine.orm.entity_manager')->getRepository(Distributor::class)->getDistributorByDomain("%{$baseHost}");
    }
}
