<?php

namespace Honda\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Honda\MainBundle\Model\Admin\SubModelTestManager;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Honda\MainBundle\Entity\Distributor;

class AppImportSubmodelsCommand extends BaseImportCommand
{
    protected function configure()
    {
        $this
            ->setName('app:import-submodels')
            ->setDescription('import submodels from portail')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $json = $container->get(GenericGateway::class)->getData($container->getParameter('endpoints.vehicle')['submodels']);
        $container->get(SubModelTestManager::class)->importSubModels($this->getDistributor(), $json);
        $output->writeln('<info>Mise à jour des sous-modèles terminés</info>');
    }

}
