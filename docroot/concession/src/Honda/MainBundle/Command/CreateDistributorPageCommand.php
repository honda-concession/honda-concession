<?php

namespace Honda\MainBundle\Command;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Model\Admin\SectionManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;


class CreateDistributorPageCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('honda:create:distributor:page')
            ->setDescription('Create pages for distributor.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pageManager = $this->getContainer()->get('Honda\PageBundle\Model\Admin\PageManager');
        $sectionManager = $this->getContainer()->get('Honda\MainBundle\Model\Admin\SectionManager');
        $distributorManager = $this->getContainer()->get('Honda\MainBundle\Model\Admin\DistributorManager');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository(Distributor::class);
        $distributors = $repository->findAll();
        foreach($distributors as $distributor) {
            $pageManager->createAllPagesForDistributor($distributor);
            $sectionManager->createSectionOrder($distributor);
            $distributorManager->saveDistributorInfosFromPortail($distributor);
        }
    }
}
