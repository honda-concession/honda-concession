<?php

namespace Honda\MainBundle\Command;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Model\Admin\SectionManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;


class UserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('honda:create-user-distributor')
            ->setDescription('Create a new user for distributor.')
            ->setHelp('This command allows you to create a user...')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $output->writeln('<info>Vous allez procéder à la création d\'un nouveau concesionnaire </info>');
    
        $question = new ConfirmationQuestion('Continuez with this action (Y/n)?',true,'/^(y|j)/i');
        $helper = $this->getHelper('question');
        
        if ($helper->ask($input, $output, $question) <> 'y') {
            return;
        }
    
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $distributorRepository = $em->getRepository(Distributor::class);
        
        /**
         * Sous domain
         */
        $question = new Question('<question>Entrez le domaine(url) du concessionaire:</question>');
        $question->setNormalizer(function ($value) {
            return $value ? trim($value) : '';
        });
        $question->setValidator(function ($answer) use ($distributorRepository) {
            
            if (!$answer) {
                throw new InvalidArgumentException(sprintf('le domaine(url) du concessionaire est obligatoire'));
            }
            
            if ($distributorRepository->findOneBy(['domain' => $answer])) {
                throw new InvalidArgumentException(sprintf('Le domaine(url) du concessionaire "%s" existe déjà', $answer));
            }
        
            return $answer;
        });
        $question->setMaxAttempts(3);
        $distributorDomain = $helper->ask($input, $output, $question);
        
    
        /**
         * Central id
         */
        $question = new Question('<question>Entrez l\'identifiant central du concessionaire:</question>');
        $question->setNormalizer(function ($value) {
            return $value ? trim($value) : '';
        });
        $question->setValidator(function ($answer) use ($distributorRepository) {
            
            if (!$answer) {
                throw new InvalidArgumentException(sprintf('L\'identifiant central est obligatoire'));
            }
    
            if ($distributorRepository->findOneBy(['distributorPortailId' => $answer])) {
                throw new InvalidArgumentException(sprintf('L\'identifiant central "%s" exist déjà', $answer));
            }
            
            return $answer;
        });
        $question->setMaxAttempts(3);
        $distributorPortailId = $helper->ask($input, $output, $question);
        
        
        $userManager = $this->getContainer()->get('fos_user.user_manager');
        
        /**
         * Email
         */
        $question = new Question('<question>Entrez l\'email du concessionaire:</question>');
        $question->setValidator(function ($answer) {
            if (!$answer || !filter_var($answer, FILTER_VALIDATE_EMAIL)) {
                throw new InvalidArgumentException(sprintf('L\'email du concessionaire est incorrect'));
            }
        
            return $answer;
        });
        $question->setMaxAttempts(3);
        
        $email = $helper->ask($input, $output, $question);
    
        
        /**
         * Password
         */
        $question = new Question('<question>Entrez le mots de passe du concessionaire:</question>');
        $question->setValidator(function ($answer) {
            if (!$answer) {
                throw new InvalidArgumentException(sprintf('Le mots de passe du concessionaire est invalide'));
            } elseif ( strlen($answer) <= 3) {
                throw new InvalidArgumentException(sprintf('Le mots de passe du concessionaire requis plus de 3 caractères'));
            }
        
            return $answer;
        });
        $question->setMaxAttempts(3);
    
        $password = $helper->ask($input, $output, $question);
    
        
        if ($distributorDomain && $distributorPortailId && $email && $password) {
            
            $distributor = new Distributor();
            $distributor
                ->setDomain($distributorDomain)
                ->setDistributorPortailId($distributorPortailId)
                ;
            
            
            $user = $userManager->createUser();
            $user
                ->setDistributor($distributor)
                ->setUsername($email)
                ->setEmail($email)
                ->setPlainPassword($password)
                ->setEnabled(true)
                ->setRoles(array('ROLE_DISTRIBUTOR', 'SONATA'));
            ;
            
            $userManager->updateUser($user);
            
    
            $output->writeln( sprintf('<fg=green>Concessionaire %s a été bien créé </>', $distributor->getDomain()));
            
        } else {
            throw new \RuntimeException('Erreur, impossible de créer un concessionaire.');
        }
        
    }
}
