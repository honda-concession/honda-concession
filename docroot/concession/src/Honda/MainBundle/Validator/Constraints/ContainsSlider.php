<?php

namespace Honda\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class ContainsSlider extends Constraint
{
    public $message = 'The slider is incorrect or invalid.';
    
    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
}
