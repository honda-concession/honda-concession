<?php

namespace Honda\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class ContainsALaUne extends Constraint
{
    public $message = 'The ALaUne is incorrect or invalid.';
    
    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
}
