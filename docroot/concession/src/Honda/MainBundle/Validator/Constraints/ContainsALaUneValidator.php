<?php

namespace Honda\MainBundle\Validator\Constraints;

use Honda\MainBundle\Entity\ALaUne;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


/**
 * Class ContainsALaUneValidator
 * @package Honda\MainBundle\Validator\Constraints
 */
class ContainsALaUneValidator extends ConstraintValidator
{

    /**
     * @var ObjectManager
     */
    protected $em;
    
    /**
     * @var TokenStorageInterface
     */
    protected $securityTokenStorage;
    
    /**
     * @var string
     */
    public $message = 'The aLaUne is incorrect or invalid.';
    
    /**
     * ContainsALaUneValidator constructor.
     * @param ObjectManager $em
     * @param TokenStorageInterface $securityTokenStorage
     */
    public function __construct(ObjectManager $em, TokenStorageInterface $securityTokenStorage)
    {
        $this->em = $em;
        $this->securityTokenStorage = $securityTokenStorage;
    }
    
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $token = $this->securityTokenStorage->getToken();
        
        $distributor = $token->getUser()->getDistributor();
        
        // $aLaUnes = $this->em->getRepository(ALaUne::class)->findBy(['distributor' => $distributor]);
        
        // if (count($aLaUnes) >= 5) {
        //     $this->context->buildViolation('Vous avez atteint le nombre limite de slider à créer')->addViolation();
        // }
    
        if ((!$value->getMedia() && !$value->getLink())) {
            $this->context
                ->buildViolation('Vous deviez renseigner une image et son url ou une vidéo')
                ->addViolation()
            ;
        }
         
        if ($value->getMedia() || $value->getLink()) {

            if (!$value->getMedia()) {

                $this->context
                    ->buildViolation('Vous deviez renseigner une image ou une vidéo')
                    ->atPath('media')
                    ->addViolation();
            } elseif (!$value->getLink()) {
                
                $this->context
                    ->buildViolation('Vous deviez renseigner une url')
                    ->atPath('link')
                    ->addViolation()
                ;
            }
        }
            
       
    }
}
