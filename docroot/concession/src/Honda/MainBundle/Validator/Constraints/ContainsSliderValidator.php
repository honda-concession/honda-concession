<?php

namespace Honda\MainBundle\Validator\Constraints;

use Honda\MainBundle\Entity\Slider;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


/**
 * Class ContainsSliderValidator
 * @package Honda\MainBundle\Validator\Constraints
 */
class ContainsSliderValidator extends ConstraintValidator
{

    /**
     * @var ObjectManager
     */
    protected $em;
    
    /**
     * @var TokenStorageInterface
     */
    protected $securityTokenStorage;
    
    /**
     * @var string
     */
    public $message = 'The slider is incorrect or invalid.';
    
    /**
     * ContainsSliderValidator constructor.
     * @param ObjectManager $em
     * @param TokenStorageInterface $securityTokenStorage
     */
    public function __construct(ObjectManager $em, TokenStorageInterface $securityTokenStorage)
    {
        $this->em = $em;
        $this->securityTokenStorage = $securityTokenStorage;
    }
    
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $token = $this->securityTokenStorage->getToken();
        
        $distributor = $token->getUser()->getDistributor();
        
        $sliders = $this->em->getRepository(Slider::class)->findBy(['distributor' => $distributor]);
        
        if (count($sliders) >= 5) {
            $this->context->buildViolation('Vous avez atteint le nombre limite de slider à créer')->addViolation();
        }
    
        
        if ((!$value->getImage() && !$value->getUrl()) && !$value->getYoutubeUrl()) {
            $this->context
                ->buildViolation('Vous deviez renseigner une image et son url ou un lien youtube')
                ->addViolation()
            ;
        }
        
        if ($value->getYoutubeUrl()) {
            
            if ($value->getImage() || $value->getUrl()) {
                
                $this->context
                    ->buildViolation('Vous deviez renseigner soit une image et son url, soit un lien youtube')
                    ->addViolation()
                ;
            }
        } else {
            
            if ($value->getImage() || $value->getUrl()) {
    
                if (!$value->getImage()) {
    
                    $this->context
                        ->buildViolation('Vous deviez renseigner une image')
                        ->atPath('image')
                        ->addViolation();
                } elseif ( !$value->getUrl()) {
                    
                    $this->context
                        ->buildViolation('Vous deviez renseigner une url')
                        ->atPath('url')
                        ->addViolation()
                    ;
                }
            }
            
        }
       
    }
}
