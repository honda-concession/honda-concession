<?php

namespace Honda\MainBundle\Validator;

/**
 * Validator functions.
 */
class SiteValidator
{
    /**
     * Validates that the given site name is a valid format.
     *
     *
     * @param $name
     *
     * @return string
     */
    public static function validateSiteName($name)
    {
       
        if (empty($name) || !preg_match('/^[a-zA-Z0-9_]*$/', $name)) {
            return false;
        }

        $name = strtolower($name);

        // validate reserved keywords
        $reserved = self::getReservedWords();
        if (in_array($name, $reserved)) {
            return false;
        }

        return $name;
    }

    /**
     * Performs basic checks in host .
     *
     * @param string $host
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    public static function validateHost($host)
    {
        if (!preg_match('/^http:\/\/|(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/', $host)) {
            return false;
        }

        return $host;
    }

    /**
     * Validates that the given site folder is a valid format.
     *
     *
     * @param $name
     *
     * @return string
     */
    public static function validateSiteFolder($name)
    {
       
        if (empty($name) || !preg_match('/^[a-zA-Z0-9_]*$/', $name)) {
            return false;
        }

        $name = strtolower($name);

        return $name;
    }

    public static function getReservedWords()
    {
        return array(
            'app',
            '__CLASS__',
            '__DIR__',
            '__FILE__',
            '__LINE__',
            '__FUNCTION__',
            '__METHOD__',
            '__NAMESPACE__',
            '__TRAIT__',
        );
    }
}
