<?php

namespace Honda\MainBundle\Doctrine;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Model\Admin\SectionManager;
use Honda\MainBundle\Model\Admin\DistributorManager;
use Honda\PageBundle\Model\Admin\PageManager;

/**
 * Class DistributorListener
 * @package Honda\MainBundle\Doctrine
 */
class DistributorListener
{
    /**
     * SectionManager
     */
    protected $sectionManager;

    /**
     * @var DistributorManager
     */
    protected $distributorManager;

    /**
     * @var
     */
    protected $pageManager;

    /**
     * DistributorListener constructor.
     * @param SectionManager $sectionManager
     * @param DistributorManager $distributorManager
     * @param PageManager $pageManager
     */
    public function __construct(SectionManager $sectionManager, DistributorManager $distributorManager, PageManager $pageManager)
    {
        $this->sectionManager = $sectionManager;
        $this->distributorManager = $distributorManager;
        $this->pageManager = $pageManager;

    }

    /**
     * @param Distributor $distributor
     * @param LifecycleEventArgs $event
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function initialData(Distributor $distributor, LifecycleEventArgs $event)
    {
        $this->sectionManager->createSectionOrder($distributor);
        $this->distributorManager->saveDistributorInfosFromPortail($distributor);
        $this->pageManager->createAllPagesForDistributor($distributor);

    }
}
