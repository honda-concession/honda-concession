<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

use Application\Sonata\MediaBundle\Entity\Media;

/**
 * Slider
 *
 * @ORM\Table(name="slider")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\SliderRepository")
 *
 */
class Slider
{

    use Traits\DistributorTrait,
        Traits\TimestampableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @Assert\NotNull()
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     * @Assert\NotNull()
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var int
     *
     * @ORM\Column(name="external_link", type="boolean", options={"default": 0})
     */
    private $externalLink = false;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="media", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $media;

    /**
     *
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="image_youtube", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $imageYoutube;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Slider
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Slider
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Slider
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Slider
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getExternalLink()
    {
        return $this->externalLink;
    }

    /**
     * @param int $externalLink
     * @return Slider
     */
    public function setExternalLink($externalLink)
    {
        $this->externalLink = $externalLink;

        return $this;
    }

    /**
     * Get the value of imageYoutube
     *
     * @return  Media
     */
    public function getImageYoutube()
    {
        return $this->imageYoutube;
    }

    /**
     * Set the value of imageYoutube
     *
     * @param  Media  $imageYoutube
     *
     * @return  self
     */
    public function setImageYoutube(?Media $imageYoutube)
    {
        $this->imageYoutube = $imageYoutube;

        return $this;
    }

    /**
     * Set media
     *
     * @param Media $media
     *
     * @return
     */
    public function setMedia(?Media $media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return   'Slider: ' . $this->getId();
        }

        return '';
    }
}
