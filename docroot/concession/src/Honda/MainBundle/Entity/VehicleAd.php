<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use \JMS\Serializer\Annotation As JMS;

/**
 * VehicleAd
 *
 * @ORM\Table(name="vehicle_ad")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\VehicleAdRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 */
class VehicleAd
{
    use Traits\DistributorTrait,
        Traits\TimestampableTrait,
        Traits\PositionableTrait,
        Traits\HomeActivationTrait,
        Traits\ActivationTrait,
        Traits\RedirectionTrait,
        Traits\WinteamExportTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="price", type="integer", nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $price;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="km", type="string", length=255, nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $km;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="cylinder", type="integer", length=255, nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $cylinder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="first_hand", type="boolean", options={"default": 0})
     */
    private $firstHand = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="driving_license_a2", type="boolean", options={"default": 0})
     */
    private $drivingLicenseA2 = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="demo_vehicle", type="boolean", options={"default": 0})
     */
    private $demoVehicle = false;

    /**
     * Mise en avant
     * @var boolean
     *
     * @ORM\Column(name="garantie", type="boolean", options={"default": 0})
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $garantie = false;

    /**
     * @var string
     *
     * @ORM\Column(name="distributor_description", type="text", nullable=true)
     */
    private $distributorDescription;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="model_year", type="string", length=255, nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $modelYear;

    /**
     * Mise en circulation
     * @var \DateTime
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="release_date", type="datetime", nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     * @JMS\Type("DateTime<'d/m/Y'>")
     */
    private $releaseDate;

    /**
     * Garantie en mois
     * @var string
     *
     * @ORM\Column(name="warrant", type="string", length=255, nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $warrant;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="options", type="string", length=255, nullable=true)
     */
    private $options;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="model_portail", length=255, type="string", nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $modelPortail;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="category_portail", length=255, type="string", nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $categoryPortail;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="mark_portail", length=255, type="string", nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $markPortail;

    /**
     * @var string
     *
     * @ORM\Column(name="vehicleAdDesc", type="text", nullable=true)
     */
    private $vehicleAdDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="id_materiel", type="text", nullable=true)
     */
    private $idMateriel;

    /**
     * @var string
     *
     * @ORM\Column(name="spec_energie", type="text", nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $specEnergie;

    /**
     * @var string
     *
     * @ORM\Column(name="spec_boite_vitesse", type="text", nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $specBoiteVitesse;

    /**
     * @var string
     *
     * @ORM\Column(name="infos_complementaires", type="text", nullable=true)
     */
    private $infosComplementaires;

    /**
     * @var save as json array
     *
     * @ORM\Column(name="plus_produits", type="text", nullable=true)
     */
    private $plusProduits;

    /**
     *
     * @ORM\Column(name="pa_id", type="integer", nullable=true)
     */
    private $paId;

    /**
     *
     * @ORM\Column(name="ref_pa", length=255, type="string", nullable=true)
     */
    private $refPa;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Honda\MainBundle\Entity\VehicleAdGallery", orphanRemoval=true, fetch="EXTRA_LAZY", mappedBy="vehicleAd", cascade={"all"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     * @Jms\MaxDepth(3)
     */
    private $vehicleAdGalleries;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Honda\MainBundle\Entity\VehicleAdDescription", orphanRemoval=true, fetch="EXTRA_LAZY", mappedBy="vehicleAd", cascade={"all"})
     */
    private $vehicleAdDescriptions;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(unique=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $slug;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicleAdGalleries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->vehicleAdDescriptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return string
     */
    public function getFormattedIsPublished()
    {
        if ($this->isPublished()) {
            return 'oui';
        }
        return 'non';
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        $releaseDate = $this->getReleaseDate();
        $yearReleaseDate = null;

        if ($releaseDate instanceof \DateTime) {
            $yearReleaseDate = (int) $releaseDate->format('Y');
        }

        if ($yearReleaseDate && $yearReleaseDate < $this->getModelYear()) {

            $context->buildViolation('La date de sortie est inferieur à la date du modèle')
                ->atPath('releaseDate')
                ->addViolation();
        }

    }

    /**
     * @param $infosComplementaires
     * @return $this
     */
    public function setInfosComplementaires($infosComplementaires)
    {
        $this->infosComplementaires = $infosComplementaires;

        return $this;
    }

    /**
     * @return string
     */
    public function getInfosComplementaires()
    {
        return  $this->infosComplementaires;
    }

    /**
     * TODO: Remove later
     * @return mixed|null
     */
    public function getOneInfosComplementaire()
    {
        $infos = $this->getInfosComplementaires();
        $text = null;

        if (is_array($infos) && count($infos)) {
            $text = array_shift($infos);
        }

        return $text;
    }

    /**
     * @return save
     */
    public function getPlusProduits()
    {
        return  json_decode($this->plusProduits, true);
    }

    /**
     * @param save $plusProduits
     *
     * @return VehicleAd
     */
    public function setPlusProduits($plusProduits)
    {
        if (is_array($plusProduits)) {

            $this->plusProduits = json_encode($plusProduits);

            $errors = json_last_error();

            if ($errors) {
                throw new \RuntimeException('data is not a valid array');
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        }

        return '';
    }

    /**
     * For elasticsearch indexed
     * @return bool
     */
    public function indexed()
    {
        return $this->isPublished();
    }

    /**
     * Add vehicleAdGallery
     *
     * @param \Honda\MainBundle\Entity\VehicleAdGallery $vehicleAdGallery
     *
     * @return VehicleAd
     */
    public function addVehicleAdGallery(\Honda\MainBundle\Entity\VehicleAdGallery $vehicleAdGallery)
    {
        if (!$this->vehicleAdGalleries->contains($vehicleAdGallery)) {
            $vehicleAdGallery->setVehicleAd($this);
            $this->vehicleAdGalleries[] = $vehicleAdGallery;
        }

        return $this;
    }

    /**
     * Remove vehicleAdGallery
     *
     * @param \Honda\MainBundle\Entity\VehicleAdGallery $vehicleAdGallery
     */
    public function removeVehicleAdGallery(\Honda\MainBundle\Entity\VehicleAdGallery $vehicleAdGallery)
    {
        $this->vehicleAdGalleries->removeElement($vehicleAdGallery);
        $vehicleAdGallery->setVehicleAd(null);
    }

    /**
     * Get vehicleAdGalleries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicleAdGalleries()
    {
        return $this->vehicleAdGalleries;
    }

    /**
     * @return mixed|null
     */
    public function getMainImageGallery()
    {
        return isset($this->vehicleAdGalleries[0]) ? $this->vehicleAdGalleries[0] : null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return VehicleAd
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return VehicleAd
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return VehicleAd
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set km
     *
     * @param string $km
     *
     * @return VehicleAd
     */
    public function setKm($km)
    {
        $this->km = $km;

        return $this;
    }

    /**
     * Get km
     *
     * @return string
     */
    public function getKm()
    {
        return $this->km;
    }

    /**
     * Set cylinder
     *
     * @param string $cylinder
     *
     * @return VehicleAd
     */
    public function setCylinder($cylinder)
    {
        $this->cylinder = $cylinder;

        return $this;
    }

    /**
     * Get cylinder
     *
     * @return string
     */
    public function getCylinder()
    {
        return $this->cylinder;
    }

    /**
     * Set firstHand
     *
     * @param boolean $firstHand
     *
     * @return VehicleAd
     */
    public function setFirstHand($firstHand)
    {
        $this->firstHand = $firstHand;

        return $this;
    }

    /**
     * Get firstHand
     *
     * @return boolean
     */
    public function getFirstHand()
    {
        return $this->firstHand;
    }

    /**
     * Set drivingLicenseA2
     *
     * @param boolean $drivingLicenseA2
     *
     * @return VehicleAd
     */
    public function setDrivingLicenseA2($drivingLicenseA2)
    {
        $this->drivingLicenseA2 = $drivingLicenseA2;

        return $this;
    }

    /**
     * Get drivingLicenseA2
     *
     * @return boolean
     */
    public function getDrivingLicenseA2()
    {
        return $this->drivingLicenseA2;
    }

    /**
     * Set demoVehicle
     *
     * @param boolean $demoVehicle
     *
     * @return VehicleAd
     */
    public function setDemoVehicle($demoVehicle)
    {
        $this->demoVehicle = $demoVehicle;

        return $this;
    }

    /**
     * Get demoVehicle
     *
     * @return boolean
     */
    public function getDemoVehicle()
    {
        return $this->demoVehicle;
    }

    /**
     * Set garantie
     *
     * @param boolean $garantie
     *
     * @return VehicleAd
     */
    public function setGarantie($garantie)
    {
        $this->garantie = $garantie;

        return $this;
    }

    /**
     * Get garantie
     *
     * @return boolean
     */
    public function getGarantie()
    {
        return $this->garantie;
    }

    /**
     * Set distributorDescription
     *
     * @param string $distributorDescription
     *
     * @return VehicleAd
     */
    public function setDistributorDescription($distributorDescription)
    {
        $this->distributorDescription = $distributorDescription;

        return $this;
    }

    /**
     * Get distributorDescription
     *
     * @return string
     */
    public function getDistributorDescription()
    {
        return $this->distributorDescription;
    }

    /**
     * Set modelYear
     *
     * @param string $modelYear
     *
     * @return VehicleAd
     */
    public function setModelYear($modelYear)
    {
        $this->modelYear = $modelYear;

        return $this;
    }

    /**
     * Get modelYear
     *
     * @return string
     */
    public function getModelYear()
    {
        return $this->modelYear;
    }

    /**
     * Set releaseDate
     *
     * @param \DateTime $releaseDate
     *
     * @return VehicleAd
     */
    public function setReleaseDate($releaseDate)
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * Get releaseDate
     *
     * @return \DateTime
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * Set warrant
     *
     * @param string $warrant
     *
     * @return VehicleAd
     */
    public function setWarrant($warrant)
    {
        $this->warrant = $warrant;

        return $this;
    }

    /**
     * Get warrant
     *
     * @return float
     */
    public function getWarrant()
    {
        return (float) $this->warrant;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return VehicleAd
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set options
     *
     * @param string $options
     *
     * @return VehicleAd
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set modelPortail
     *
     * @param string $modelPortail
     *
     * @return VehicleAd
     */
    public function setModelPortail($modelPortail)
    {
        $this->modelPortail = $modelPortail;

        return $this;
    }

    /**
     * Get modelPortailSlug
     *
     * @return string
     */
    public function getModelPortail()
    {
        return $this->modelPortail;
    }

    /**
     * Set categoryPortailSlug
     *
     * @param string $categoryPortail
     *
     * @return VehicleAd
     */
    public function setCategoryPortail($categoryPortail)
    {
        $this->categoryPortail = $categoryPortail;

        return $this;
    }

    /**
     * Get categoryPortail
     *
     * @return string
     */
    public function getCategoryPortail()
    {
        return $this->categoryPortail;
    }

    /**
     * Set $markPortail
     *
     * @param string $markPortail
     *
     * @return VehicleAd
     */
    public function setMarkPortail($markPortail)
    {
        $this->markPortail = $markPortail;

        return $this;
    }

    /**
     * Get markPortail
     *
     * @return string
     */
    public function getMarkPortail()
    {
        return $this->markPortail;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return VehicleAd
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add vehicleAdDescription.
     *
     * @param \Honda\MainBundle\Entity\VehicleAdDescription $vehicleAdDescription
     *
     * @return VehicleAd
     */
    public function addVehicleAdDescription(\Honda\MainBundle\Entity\VehicleAdDescription $vehicleAdDescription)
    {
        $vehicleAdDescription->setVehicleAd($this);
        $this->vehicleAdDescriptions[] = $vehicleAdDescription;

        return $this;
    }

    /**
     * Remove vehicleAdDescription.
     *
     * @param \Honda\MainBundle\Entity\VehicleAdDescription $vehicleAdDescription
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVehicleAdDescription(\Honda\MainBundle\Entity\VehicleAdDescription $vehicleAdDescription)
    {
        $vehicleAdDescription->setVehicleAd(null);
        return $this->vehicleAdDescriptions->removeElement($vehicleAdDescription);
    }

    /**
     * Get vehicleAdDescriptions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicleAdDescriptions()
    {
        return $this->vehicleAdDescriptions;
    }

    public function getVehicleAdDesc()
    {
        return $this->vehicleAdDesc;
    }

    public function setVehicleAdDesc($vehicleAdDesc)
    {
        $this->vehicleAdDesc = $vehicleAdDesc;
        return $this;
    }


    /**
     * Set idMateriel.
     *
     * @param string|null $idMateriel
     *
     * @return VehicleAd
     */
    public function setIdMateriel($idMateriel = null)
    {
        $this->idMateriel = $idMateriel;

        return $this;
    }

    /**
     * Get idMateriel.
     *
     * @return string|null
     */
    public function getIdMateriel()
    {
        return $this->idMateriel;
    }

    /**
     * Set specEnergie.
     *
     * @param string|null $specEnergie
     *
     * @return VehicleAd
     */
    public function setSpecEnergie($specEnergie = null)
    {
        $this->specEnergie = $specEnergie;

        return $this;
    }

    /**
     * Get specEnergie.
     *
     * @return string|null
     */
    public function getSpecEnergie()
    {
        return $this->specEnergie;
    }

    /**
     * Set specBoiteVitesse.
     *
     * @param string|null $specBoiteVitesse
     *
     * @return VehicleAd
     */
    public function setSpecBoiteVitesse($specBoiteVitesse = null)
    {
        $this->specBoiteVitesse = $specBoiteVitesse;

        return $this;
    }

    /**
     * Get specBoiteVitesse.
     *
     * @return string|null
     */
    public function getSpecBoiteVitesse()
    {
        return $this->specBoiteVitesse;
    }

    /**
     * Set paId.
     *
     * @param int|null $paId
     *
     * @return VehicleAd
     */
    public function setPaId($paId = null)
    {
        $this->paId = $paId;

        return $this;
    }

    /**
     * Get paId.
     *
     * @return int|null
     */
    public function getPaId()
    {
        return $this->paId;
    }

    /**
     * Set refPa.
     *
     * @param string|null $refPa
     *
     * @return VehicleAd
     */
    public function setRefPa($refPa = null)
    {
        $this->refPa = $refPa;

        return $this;
    }

    /**
     * Get refPa.
     *
     * @return string|null
     */
    public function getRefPa()
    {
        return $this->refPa;
    }

    /**
     * set reference
     *
     * @return self
     */
    public function setReference()
    {
        $reference = '';
        if ($distributor = $this->getDistributor()) {
            $infos = $distributor->getInfos();
            $name = '';
            if (property_exists($infos, 'name')) {
                $name = $infos->name;
            }
            $reference .= substr($name, 0, 4);
            $reference .= $distributor->getId();
        }
        $reference .= '-';
        if ($markPortail = $this->getMarkPortail()) {
            $reference .= substr($markPortail, 0, 4);
        }
        $reference .= '-';
        if ($id = $this->getId()) {
            $reference .= $id;
        }
        $reference .= '-';
        if ($modelPortail = $this->getModelPortail()) {
            $reference .= substr($modelPortail, 0, 4);
        }
        $reference = preg_replace('/\s+/', '_', $reference);

        return $this->setRefPa($reference);
    }

    /**
     * Get the value of externalSites
     *
     * @return  \Doctrine\Common\Collections\ArrayCollection
     */
    public function getExternalSites()
    {
        return $this->externalSites;
    }

    /**
     * Set the value of externalSites
     *
     * @param  \Doctrine\Common\Collections\ArrayCollection  $externalSites
     *
     * @return  self
     */
    public function setExternalSites(\Doctrine\Common\Collections\ArrayCollection $externalSites)
    {
        $this->externalSites = $externalSites;

        return $this;
    }
}
