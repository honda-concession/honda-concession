<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServicePage
 *
 * @ORM\Table(name="service_page")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\ServicePageRepository")
 */
class ServicePage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="intro", type="text", length=255, nullable=true)
     */
    private $intro;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="image", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $image;
    
    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Honda\MainBundle\Entity\Distributor", inversedBy="servicePage")
     */
    private $distributor;
    
    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        }
        
        return '';
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ServicePage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set intro
     *
     * @param string $intro
     *
     * @return ServicePage
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * Get intro
     *
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return ServicePage
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set distributor
     *
     * @param \Honda\MainBundle\Entity\Distributor $distributor
     *
     * @return ServicePage
     */
    public function setDistributor(\Honda\MainBundle\Entity\Distributor $distributor = null)
    {
        $this->distributor = $distributor;

        return $this;
    }

    /**
     * Get distributor
     *
     * @return \Honda\MainBundle\Entity\Distributor
     */
    public function getDistributor()
    {
        return $this->distributor;
    }
}
