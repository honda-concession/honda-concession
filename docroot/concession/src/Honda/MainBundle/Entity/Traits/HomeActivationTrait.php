<?php

namespace Honda\MainBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Trait ActivationTrait
 * @package Honda\Bundle\CommonBundle\Entity\Traits
 */
trait HomeActivationTrait
{
    /**
     * published on home
     * @var bool
     *
     * @ORM\Column(name="published_on_home", type="boolean", options={"default": 0})
     */
    protected $publishedOnHome = false;

    /**
     * Get published on home
     *
     * @return  bool
     */
    public function getPublishedOnHome()
    {
        return $this->publishedOnHome;
    }

    /**
     * Set published on home
     *
     * @param  bool  $publishedOnHome  published on home
     *
     * @return  self
     */
    public function setPublishedOnHome(bool $publishedOnHome)
    {
        $this->publishedOnHome = $publishedOnHome;

        return $this;
    }

    public function isPublishedOnHome()
    {
        return $this->getPublishedOnHome();
    }
}
