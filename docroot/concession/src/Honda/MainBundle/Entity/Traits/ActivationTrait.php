<?php

namespace Honda\MainBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Trait ActivationTrait
 * @package Honda\Bundle\CommonBundle\Entity\Traits
 */
trait ActivationTrait
{
    /**
     * published
     * @var bool
     *
     * @ORM\Column(name="published", type="boolean", options={"default": 0})
     */
    protected $published = false;

    /**
     * Get published
     *
     * @return  bool
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set published
     *
     * @param  bool  $published  published
     *
     * @return  self
     */
    public function setPublished(bool $published)
    {
        $this->published = $published;

        return $this;
    }

    public function isPublished()
    {
        return $this->getPublished();
    }
}
