<?php

namespace Honda\MainBundle\Entity\Traits;

use \JMS\Serializer\Annotation As JMS;

/**
 * Trait DistributorTrait
 * @package Honda\MainBundle\Entity\Traits
 */
trait DistributorTrait
{
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Honda\MainBundle\Entity\Distributor")
     * @JMS\Expose()
     * @JMS\Groups({"vo_list", "Default"})
     */
    private $distributor;
    
    
    /**
     * Set distributor
     *
     * @param \Honda\MainBundle\Entity\Distributor $distributor
     *
     * @return
     */
    public function setDistributor(\Honda\MainBundle\Entity\Distributor $distributor = null)
    {
        $this->distributor = $distributor;
        
        return $this;
    }
    
    /**
     * Get distributor
     *
     * @return \Honda\MainBundle\Entity\Distributor
     */
    public function getDistributor()
    {
        return $this->distributor;
    }
}
