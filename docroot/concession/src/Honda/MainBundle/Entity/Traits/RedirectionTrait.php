<?php

namespace Honda\MainBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Trait RedirectionTrait
 * @package Honda\Bundle\CommonBundle\Entity\Traits
 */
trait RedirectionTrait
{
    /**
     * redirect url
     * @var string
     *
     * @ORM\Column(name="redirect_url", type="string", length=255, nullable=true)
     */
    protected $redirectUrl;

    /**
     * redirect status
     *
     * @var boolean
     * @ORM\Column(name="redirect_status", type="boolean", options={"default": 0}, nullable=true)
     */
    protected $redirectStatus;

    /**
     * hide in navigation
     *
     * @var bool
     * @ORM\Column(name="hide_in_navigation", type="boolean", options={"default": 0}, nullable=true)
     */
    protected $hideInNavigation;

    /**
     * hide in sitemap
     *
     * @var bool
     * @ORM\Column(name="hide_in_sitemap", type="boolean", options={"default": 0}, nullable=true)
     */
    protected $hideInSitemap;

    /**
     * Get redirect url
     *
     * @return  string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * Set redirect url
     *
     * @param  string  $redirectUrl  redirect url
     *
     * @return  self
     */
    public function setRedirectUrl(?string $redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    /**
     * Get hide in navigation
     *
     * @return  bool
     */
    public function getHideInNavigation()
    {
        return $this->hideInNavigation;
    }

    /**
     * Set hide in navigation
     *
     * @param  bool  $hideInNavigation  hide in navigation
     *
     * @return  self
     */
    public function setHideInNavigation(?bool $hideInNavigation)
    {
        $this->hideInNavigation = $hideInNavigation;

        return $this;
    }

    /**
     * Get hide in sitemap
     *
     * @return  bool
     */
    public function getHideInSitemap()
    {
        return $this->hideInSitemap;
    }

    /**
     * Set hide in sitemap
     *
     * @param  bool  $hideInSitemap  hide in sitemap
     *
     * @return  self
     */
    public function setHideInSitemap(?bool $hideInSitemap)
    {
        $this->hideInSitemap = $hideInSitemap;

        return $this;
    }

    public function isHiddenInSitemap()
    {
        return $this->hideInSitemap;
    }

    /**
     * Get redirect status
     *
     * @return  boolean
     */
    public function getRedirectStatus()
    {
        return $this->redirectStatus;
    }

    /**
     * Set redirect status
     *
     * @param  boolean  $redirectStatus  redirect status
     *
     * @return  self
     */
    public function setRedirectStatus(?bool $redirectStatus)
    {
        $this->redirectStatus = $redirectStatus;

        return $this;
    }

    public function isRedirectionActive()
    {
        return $this->redirectStatus === true;
    }

    public function showInNavigation()
    {
        return !$this->getHideInNavigation();
    }

    public function hasRedirection()
    {
        return $this->isRedirectionActive();
    }

    public function getRedirectionUrl()
    {
        return $this->getRedirectUrl();
    }

    public function setRedirectionValuesToArray(array $array)
    {
        $array['hasRedirection'] = $this->isRedirectionActive();
        $array['showInNavigation'] = !$this->getHideInNavigation();
        $array['showInSitemap'] = !$this->getHideInSitemap();
        if ($this->isRedirectionActive()) {
            $array['redirectionUrl'] = $this->getRedirectUrl();
        }

        return $array;
    }
}
