<?php

namespace Honda\MainBundle\Entity\Traits;


/**
 * Trait ImageTrait
 * @package Honda\MainBundle\Entity\Traits
 */
trait ImageTrait
{
    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="image", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $image;
    
    
    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;
        
        return $this;
    }
    
    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }
}
