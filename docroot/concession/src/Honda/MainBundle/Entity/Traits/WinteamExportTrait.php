<?php

namespace Honda\MainBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Trait WinteamExportTrait
 * @package Honda\Bundle\CommonBundle\Entity\Traits
 */
trait WinteamExportTrait
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="published_on_leboncoin", type="boolean", options={"default": 0})
     */
    protected $publishedOnLeBonCoin = false;

    /**
     * Get the value of publishedOnLeBonCoin
     */
    public function getPublishedOnLeBonCoin()
    {
        return $this->publishedOnLeBonCoin;
    }

    /**
     * Set the value of publishedOnLeBonCoin
     *
     * @return  self
     */
    public function setPublishedOnLeBonCoin($publishedOnLeBonCoin)
    {
        $this->publishedOnLeBonCoin = $publishedOnLeBonCoin;

        return $this;
    }
}
