<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsSlide
 *
 * @ORM\Table(name="news_slide")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\NewsSlideRepository")
 */
class NewsSlide
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Honda\MainBundle\Entity\News", inversedBy="slides")
     */
    protected $news;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"}, fetch="LAZY")
     */
    protected $image;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    protected $position;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->position = 99999;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return NewsSlide
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set news
     *
     * @param \Honda\MainBundle\Entity\News $news
     *
     * @return NewsSlide
     */
    public function setNews(News $news = null)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news
     *
     * @return \Honda\MainBundle\Entity\News
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return NewsSlide
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set position
     *
     * @param int $position
     * @return NewsSlide
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    public function __toString()
    {
        return 'Slide';
    }
}
