<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VehicleAdDescription
 *
 * @ORM\Table(name="vehicle_ad_description")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\VehicleAdDescriptionRepository")
 */
class VehicleAdDescription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255, nullable=true)
     */
    private $content;
    
    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Honda\MainBundle\Entity\VehicleAd", inversedBy="vehicleAdDescriptions")
     */
    private $vehicleAd;
    

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return VehicleAdDescription
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set vehicleAd.
     *
     * @param \Honda\MainBundle\Entity\VehicleAd|null $vehicleAd
     *
     * @return VehicleAdDescription
     */
    public function setVehicleAd(\Honda\MainBundle\Entity\VehicleAd $vehicleAd = null)
    {
        $this->vehicleAd = $vehicleAd;

        return $this;
    }

    /**
     * Get vehicleAd.
     *
     * @return \Honda\MainBundle\Entity\VehicleAd|null
     */
    public function getVehicleAd()
    {
        return $this->vehicleAd;
    }
}
