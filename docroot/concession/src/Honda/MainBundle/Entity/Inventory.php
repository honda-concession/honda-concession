<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inventory
 *
 * @ORM\Table(name="inventory", indexes={@ORM\Index(name="newVehiclePortailId_idx", columns={"new_vehicle_id"})})
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\InventoryRepository")
 */
class Inventory
{
    
    use Traits\DistributorTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="in_stock", type="boolean", nullable=true, options={"default": 0})
     */
    private $inStock = false;
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="new_vehicle_id", type="integer")
     */
    private $newVehicleId;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_title", type="string", length=255, nullable=true)
     */
    private $newVehicleTitle;
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_reference", type="string", length=255, nullable=true)
     */
    private $newVehicleReference;
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_model", type="string", length=255, nullable=true)
     */
    private $newVehicleModel;
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_category", type="string", length=255, nullable=true)
     */
    private $newVehicleCategory;
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_mark", type="string", length=255, nullable=true)
     */
    private $newVehicleMark;
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_submodel", type="string", length=255, nullable=true)
     */
    private $newVehicleSubmodel;
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_submodel_slug", type="string", length=255, nullable=true)
     */
    private $newVehicleSubmodelSlug;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_submodel_id", type="string", length=255, nullable=true)
     */
    private $newVehicleSubmodelId;
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_color", type="string", length=255, nullable=true)
     */
    private $newVehicleColor;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_color_icon", type="string", length=255, nullable=true)
     */
    private $newVehicleColorIcon;
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_color_code", type="string", length=255, nullable=true)
     */
    private $newVehicleColorCode;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="new_vehicle_price", type="string", length=255, nullable=true)
     */
    private $newVehiclePrice;
    
    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @var string
     *
     * @ORM\Column(name="kora_id", type="string", length=255, nullable=true)
     */
    private $koraId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="color_kora_id", type="string", length=255, nullable=true)
     */
    private $colorKoraId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="color_kora_code", type="string", length=255, nullable=true)
     */
    private $colorKoraCode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="color_kora_name", type="string", length=255, nullable=true)
     */
    private $colorKoraName;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set inStock
     *
     * @param boolean $inStock
     *
     * @return Inventory
     */
    public function setInStock($inStock)
    {
        $this->inStock = $inStock;

        return $this;
    }

    /**
     * Get inStock
     *
     * @return boolean
     */
    public function getInStock()
    {
        return $this->inStock;
    }



    /**
     * Set newVehicleId
     *
     * @param integer $newVehicleId
     *
     * @return Inventory
     */
    public function setNewVehicleId($newVehicleId)
    {
        $this->newVehicleId = $newVehicleId;

        return $this;
    }

    /**
     * Get newVehicleId
     *
     * @return integer
     */
    public function getNewVehicleId()
    {
        return $this->newVehicleId;
    }

    /**
     * Set newVehicleTitle
     *
     * @param string $newVehicleTitle
     *
     * @return Inventory
     */
    public function setNewVehicleTitle($newVehicleTitle)
    {
        $this->newVehicleTitle = $newVehicleTitle;

        return $this;
    }

    /**
     * Get newVehicleTitle
     *
     * @return string
     */
    public function getNewVehicleTitle()
    {
        return $this->newVehicleTitle;
    }

    /**
     * Set newVehicleReference
     *
     * @param string $newVehicleReference
     *
     * @return Inventory
     */
    public function setNewVehicleReference($newVehicleReference)
    {
        $this->newVehicleReference = $newVehicleReference;

        return $this;
    }

    /**
     * Get newVehicleReference
     *
     * @return string
     */
    public function getNewVehicleReference()
    {
        return $this->newVehicleReference;
    }

    /**
     * Set newVehicleModel
     *
     * @param string $newVehicleModel
     *
     * @return Inventory
     */
    public function setNewVehicleModel($newVehicleModel)
    {
        $this->newVehicleModel = $newVehicleModel;

        return $this;
    }

    /**
     * Get newVehicleModel
     *
     * @return string
     */
    public function getNewVehicleModel()
    {
        return $this->newVehicleModel;
    }

    /**
     * Set newVehicleCategory
     *
     * @param string $newVehicleCategory
     *
     * @return Inventory
     */
    public function setNewVehicleCategory($newVehicleCategory)
    {
        $this->newVehicleCategory = $newVehicleCategory;

        return $this;
    }

    /**
     * Get newVehicleCategory
     *
     * @return string
     */
    public function getNewVehicleCategory()
    {
        return $this->newVehicleCategory;
    }

    /**
     * Set newVehicleMark
     *
     * @param string $newVehicleMark
     *
     * @return Inventory
     */
    public function setNewVehicleMark($newVehicleMark)
    {
        $this->newVehicleMark = $newVehicleMark;

        return $this;
    }

    /**
     * Get newVehicleMark
     *
     * @return string
     */
    public function getNewVehicleMark()
    {
        return $this->newVehicleMark;
    }

    /**
     * Set newVehicleSubmodel
     *
     * @param string $newVehicleSubmodel
     *
     * @return Inventory
     */
    public function setNewVehicleSubmodel($newVehicleSubmodel)
    {
        $this->newVehicleSubmodel = $newVehicleSubmodel;

        return $this;
    }

    /**
     * Get newVehicleSubmodel
     *
     * @return string
     */
    public function getNewVehicleSubmodel()
    {
        return $this->newVehicleSubmodel;
    }

    /**
     * Set newVehicleColor
     *
     * @param string $newVehicleColor
     *
     * @return Inventory
     */
    public function setNewVehicleColor($newVehicleColor)
    {
        $this->newVehicleColor = $newVehicleColor;

        return $this;
    }

    /**
     * Get newVehicleColor
     *
     * @return string
     */
    public function getNewVehicleColor()
    {
        return $this->newVehicleColor;
    }

    /**
     * Set newVehiclePrice
     *
     * @param string $newVehiclePrice
     *
     * @return Inventory
     */
    public function setNewVehiclePrice($newVehiclePrice)
    {
        $this->newVehiclePrice = $newVehiclePrice;

        return $this;
    }

    /**
     * Get newVehiclePrice
     *
     * @return string
     */
    public function getNewVehiclePrice()
    {
        return $this->newVehiclePrice;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Inventory
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set newVehicleSubmodelSlug.
     *
     * @param string|null $newVehicleSubmodelSlug
     *
     * @return Inventory
     */
    public function setNewVehicleSubmodelSlug($newVehicleSubmodelSlug = null)
    {
        $this->newVehicleSubmodelSlug = $newVehicleSubmodelSlug;

        return $this;
    }

    /**
     * Get newVehicleSubmodelSlug.
     *
     * @return string|null
     */
    public function getNewVehicleSubmodelSlug()
    {
        return $this->newVehicleSubmodelSlug;
    }

    /**
     * Set newVehicleSubmodelId.
     *
     * @param string|null $newVehicleSubmodelId
     *
     * @return Inventory
     */
    public function setNewVehicleSubmodelId($newVehicleSubmodelId = null)
    {
        $this->newVehicleSubmodelId = $newVehicleSubmodelId;

        return $this;
    }

    /**
     * Get newVehicleSubmodelId.
     *
     * @return string|null
     */
    public function getNewVehicleSubmodelId()
    {
        return $this->newVehicleSubmodelId;
    }

    /**
     * Set newVehicleColorIcon.
     *
     * @param string|null $newVehicleColorIcon
     *
     * @return Inventory
     */
    public function setNewVehicleColorIcon($newVehicleColorIcon = null)
    {
        $this->newVehicleColorIcon = $newVehicleColorIcon;

        return $this;
    }

    /**
     * Get newVehicleColorIcon.
     *
     * @return string|null
     */
    public function getNewVehicleColorIcon()
    {
        return $this->newVehicleColorIcon;
    }

    
    /**
     * @return string
     */
    public function getKoraId()
    {
        return $this->koraId;
    }
    
    /**
     * @param string $koraId
     * @return Inventory
     */
    public function setKoraId($koraId)
    {
        $this->koraId = $koraId;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getNewVehicleColorCode(): string
    {
        return $this->newVehicleColorCode;
    }
    
    /**
     * @param string $newVehicleColorCode
     * @return Inventory
     */
    public function setNewVehicleColorCode($newVehicleColorCode)
    {
        $this->newVehicleColorCode = $newVehicleColorCode;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getColorKoraId()
    {
        return $this->colorKoraId;
    }
    
    /**
     * @param string $colorKoraId
     * @return Inventory
     */
    public function setColorKoraId($colorKoraId)
    {
        $this->colorKoraId = $colorKoraId;
    
        return $this;
    }
    
    /**
     * @return string
     */
    public function getColorKoraCode()
    {
        return $this->colorKoraCode;
    }
    
    /**
     * @param string $colorKoraCode
     * @return Inventory
     */
    public function setColorKoraCode($colorKoraCode)
    {
        $this->colorKoraCode = $colorKoraCode;
    
        return $this;
    }
    
    /**
     * @return string
     */
    public function getColorKoraName()
    {
        return $this->colorKoraName;
    }
    
    /**
     * @param string $colorKoraName
     * @return Inventory
     */
    public function setColorKoraName($colorKoraName)
    {
        $this->colorKoraName = $colorKoraName;
        
        return $this;
    }
}
