<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * DistributorTime
 *
 * @ORM\Table(name="distributor_time")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\DistributorTimeRepository")
 */
class DistributorTime
{
    
    use Traits\DistributorTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="start_hour", type="datetime", nullable=true)
     */
    private $startHour;

    /**
     * @var string
     *
     * @ORM\Column(name="end_hour", type="datetime", nullable=true)
     */
    private $endHour;
    
    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

  
    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        }
        
        return '';
    }
    
    /**
     * @return mixed
     */
    public function getStartHourFormatted()
    {
        return $this->getStartHour()->format('H:i');
    }
    
    /**
     * @return mixed
     */
    public function getEndHourFormatted()
    {
        return $this->getEndHour()->format('H:i');
    }
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startHour
     *
     * @param string $startHour
     *
     * @return DistributorTime
     */
    public function setStartHour($startHour)
    {
        $this->startHour = $startHour;

        return $this;
    }

    /**
     * Get startHour
     *
     * @return string
     */
    public function getStartHour()
    {
        return $this->startHour;
    }

    /**
     * Set endHour
     *
     * @param string $endHour
     *
     * @return DistributorTime
     */
    public function setEndHour($endHour)
    {
        $this->endHour = $endHour;

        return $this;
    }

    /**
     * Get endHour
     *
     * @return string
     */
    public function getEndHour()
    {
        return $this->endHour;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return DistributorTime
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return DistributorTime
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

}
