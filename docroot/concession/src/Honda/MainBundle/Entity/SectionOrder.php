<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * SectionOrder
 *
 * @ORM\Table(name="section_order")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\SectionOrderRepository")
 */
class SectionOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @Gedmo\SortablePosition()
     * @ORM\Column(type="integer")
     */
    private $position;
    
    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Honda\MainBundle\Entity\Section")
     */
    private $section;
    
    /**
     * @var string
     * @Gedmo\SortableGroup()
     * @ORM\ManyToOne(targetEntity="Honda\MainBundle\Entity\Distributor")
     */
    private $distributor;
    
    /**
     * Set distributor
     *
     * @param \Honda\MainBundle\Entity\Distributor $distributor
     *
     * @return
     */
    public function setDistributor(\Honda\MainBundle\Entity\Distributor $distributor = null)
    {
        $this->distributor = $distributor;
        
        return $this;
    }
    
    /**
     * Get distributor
     *
     * @return \Honda\MainBundle\Entity\Distributor
     */
    public function getDistributor()
    {
        return $this->distributor;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return SectionOrder
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set section
     *
     * @param \Honda\MainBundle\Entity\Section $section
     *
     * @return SectionOrder
     */
    public function setSection(\Honda\MainBundle\Entity\Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \Honda\MainBundle\Entity\Section
     */
    public function getSection()
    {
        return $this->section;
    }
}
