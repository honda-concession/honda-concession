<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Section
 *
 * @ORM\Table(name="section")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\SectionRepository")
 */
class Section
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="sectionKey", type="string", length=255, unique=true)
     */
    private $sectionKey;
    
    /**
     * @var string
     *
     * @ORM\Column(name="templateName", type="string", length=255)
     */
    private $templateName;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Section
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set sectionKey
     *
     * @param string $sectionKey
     *
     * @return Section
     */
    public function setSectionKey($sectionKey)
    {
        $this->sectionKey = $sectionKey;

        return $this;
    }

    /**
     * Get sectionKey
     *
     * @return string
     */
    public function getSectionKey()
    {
        return $this->sectionKey;
    }

    /**
     * Set actionName
     *
     * @param string $actionName
     *
     * @return Section
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;

        return $this;
    }

    /**
     * Get actionName
     *
     * @return string
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * Set templateName
     *
     * @param string $templateName
     *
     * @return Section
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * Get templateName
     *
     * @return string
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }
}
