<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\MainBundle\Validator\Constraints\ContainsALaUne;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * ALaUne
 *
 * @ORM\Table(name="a_la_une")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\ALaUneRepository")
 * @ContainsALaUne()
 */
class ALaUne
{

    use Traits\DistributorTrait,
        Traits\TimestampableTrait,
        Traits\StartDateEndDateTrait,
        Traits\HomeActivationTrait,
        Traits\ActivationTrait,
        Traits\RedirectionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="text_mea", type="string", length=255, nullable=true)
     */
    private $textMea;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_price", type="string", length=255, nullable=true)
     */
    private $promoPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="link_label", type="string", length=255, nullable=true)
     */
    private $linkLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * open link in new tab
     * @var bool
     *
     * @ORM\Column(name="link_open_in_new_tab", type="boolean", options={"default": 0})
     */
    private $linkOpenInNewTab;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="media", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $media;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;


    public function __construct()
    {
        $this->linkLabel = 'Voir plus';
        $this->createdAt = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        }

        return '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ALaUne
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return ALaUne
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set textMea
     *
     * @param string $textMea
     *
     * @return ALaUne
     */
    public function setTextMea($textMea)
    {
        $this->textMea = $textMea;

        return $this;
    }

    /**
     * Get textMea
     *
     * @return string
     */
    public function getTextMea()
    {
        return $this->textMea;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return ALaUne
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set linkLabel
     *
     * @param string $linkLabel
     *
     * @return ALaUne
     */
    public function setLinkLabel($linkLabel)
    {
        $this->linkLabel = $linkLabel;

        return $this;
    }

    /**
     * Get linkLabel
     *
     * @return string
     */
    public function getLinkLabel()
    {
        return $this->linkLabel;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return ALaUne
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     *
     * @return ALaUne
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ALaUne
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ALaUne
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Get promoPrice
     *
     * @return integer
     */
    public function getPromoPrice()
    {
        return $this->promoPrice;
    }

    /**
     * Set promoPrice
     *
     * @param integer $promoPrice
     *
     * @return ALaUne
     */
    public function setPromoPrice($promoPrice)
    {
        $this->promoPrice = $promoPrice;
        return $this;
    }

    /**
     * Get open link in new tab
     *
     * @return  bool
     */
    public function getLinkOpenInNewTab()
    {
        return $this->linkOpenInNewTab;
    }

    /**
     * Set open link in new tab
     *
     * @param  bool  $linkOpenInNewTab  open link in new tab
     *
     * @return  self
     */
    public function setLinkOpenInNewTab(bool $linkOpenInNewTab)
    {
        $this->linkOpenInNewTab = $linkOpenInNewTab;

        return $this;
    }
}
