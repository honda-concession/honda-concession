<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * SocialNetworkLink
 *
 * @ORM\Table(name="social_network_link")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\SocialNetworkLinkRepository")
 */
class SocialNetworkLink
{
    
    use Traits\DistributorTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Url()
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @Assert\Url()
     * @ORM\Column(name="instagram", type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @var string
     *
     * @Assert\Url()
     * @ORM\Column(name="snapchat", type="string", length=255, nullable=true)
     */
    private $snapchat;

    /**
     * @var string
     *
     * @Assert\Url()
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @Assert\Url()
     * @ORM\Column(name="youtube", type="string", length=255, nullable=true)
     */
    private $youtube;
    
    /**
     * @return bool
     */
    public function hasAllLinks()
    {
        return $this->getFacebook() && $this->getTwitter() && $this->getYoutube() && $this->getInstagram();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     *
     * @return SocialNetworkLink
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set instagram
     *
     * @param string $instagram
     *
     * @return SocialNetworkLink
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * Set snapchat
     *
     * @param string $snapchat
     *
     * @return SocialNetworkLink
     */
    public function setSnapchat($snapchat)
    {
        $this->snapchat = $snapchat;

        return $this;
    }

    /**
     * Get snapchat
     *
     * @return string
     */
    public function getSnapchat()
    {
        return $this->snapchat;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return SocialNetworkLink
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set youtube
     *
     * @param string $youtube
     *
     * @return SocialNetworkLink
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Get youtube
     *
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }
}
