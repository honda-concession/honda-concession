<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \JMS\Serializer\Annotation As JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Accessory
 *
 * @ORM\Table(name="accessory")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\AccessoryRepository")
 * @JMS\ExclusionPolicy("all")
 */
class Accessory
{
    const MOTO_ACCESSORY_BLOCK = 'accessoires-motos';
    const MOTO_ACCESSORY_ENDPOINT = 'link1';
    const EQUIPMENT_ACCESSORY_BLOCK = 'equipement-du-motard';
    const EQUIPMENT_ACCESSORY_ENDPOINT = 'link2';
    const VETEMENT_ACCESSORY_BLOCK = 'accessoires-vetement';
    const VETEMENT_ACCESSORY_ENDPOINT = 'link3';
    const LOGO_ACCESSORY_BLOCK = 'logos';
    const LOGO_ACCESSORY_ENDPOINT = 'acccessoires-logo';

    use Traits\ImageTrait,
        Traits\DistributorTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="link_label", type="string", length=255, nullable=true)
     */
    private $linkLabel = 'Télécharger le catalogue';

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $pdf;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_label", type="string", length=255, nullable=true)
     */
    private $pdfLabel = 'Télécharger l\'autre catalogue';

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="description_accessory_clothing", type="text", nullable=true)
     */
    private $descriptionAccessoryClothing;

    /**
     * @var string
     *
     * @ORM\Column(name="description_accessory_helmet", type="text", nullable=true)
     */
    private $descriptionAccessoryHelmet;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Honda\MainBundle\Entity\AccessoryBlock")
     */
    private $accessoryBlock;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Honda\MainBundle\Entity\AccessoryItem", orphanRemoval=true, fetch="EXTRA_LAZY", mappedBy="accessory", cascade={"all"})
     */
    private $accessoryItems;

    /**
     * @var string
     *
     * @ORM\Column(name="clothing_logos", type="text", nullable=true)
     */
    private $clothingLogos;

    /**
     * @var string
     *
     * @ORM\Column(name="helmet_logos", type="text", nullable=true)
     */
    private $helmetLogos;

    public function __construct()
    {
        $this->accessoryItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param array $logos
     * @return $this
     */
    public function setClothingLogos(Array $logos = [])
    {
        $this->clothingLogos = serialize($logos);

        return $this;
    }

    /**
     * Get logos
     *
     * @return string
     */
    public function getClothingLogos()
    {
        return unserialize($this->clothingLogos);
    }

    /**
     * @return array
     */
    public function getClothingLogosByKeys()
    {
        $logos = $this->getClothingLogos();
        $tmpLogos = [];

        if ($logos) {
            foreach ($logos as $logo) {
                $tmpLogos[$logo['id']] = $logo;
            }
        }

        return $tmpLogos;
    }

    /**
     * @param array $logos
     * @return $this
     */
    public function setHelmetLogos(Array $logos = [])
    {
        $this->helmetLogos = serialize($logos);

        return $this;
    }

    /**
     * Get logos
     *
     * @return string
     */
    public function getHelmetLogos()
    {
        return unserialize($this->helmetLogos);
    }

    /**
     * @return array
     */
    public function getHelmetLogosByKeys()
    {
        $logos = $this->getHelmetLogos();
        $tmpLogos = [];

        if ($logos) {
            foreach ($logos as $logo) {
                $tmpLogos[$logo['id']] = $logo;
            }
        }

        return $tmpLogos;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Accessory
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Accessory
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set link.
     *
     * @param string|null $link1
     *
     * @return Accessory
     */
    public function setLink($link = null)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string|null
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set linkLabel.
     *
     * @param string|null $linkLabel
     *
     * @return Accessory
     */
    public function setLinkLabel($linkLabel = null)
    {
        $this->linkLabel = $linkLabel;

        return $this;
    }

    /**
     * Get linkLabel.
     *
     * @return string|null
     */
    public function getLinkLabel()
    {
        return $this->linkLabel;
    }

    /**
     * Set pdf.
     *
     * @param string|null $pdf
     *
     * @return Accessory
     */
    public function setPdf(\Application\Sonata\MediaBundle\Entity\Media $pdf = null)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf.
     *
     * @return string|null
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set pdfLabel.
     *
     * @param string|null $pdfLabel
     *
     * @return Accessory
     */
    public function setPdfLabel($pdfLabel = null)
    {
        $this->pdfLabel = $pdfLabel;

        return $this;
    }

    /**
     * Get pdfLabel.
     *
     * @return string|null
     */
    public function getPdfLabel()
    {
        return $this->pdfLabel;
    }

    /**
     * Set position.
     *
     * @param int|null $position
     *
     * @return Accessory
     */
    public function setPosition($position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int|null
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set descriptionAccessoryClothing.
     *
     * @param string|null $descriptionAccessoryClothing
     *
     * @return Accessory
     */
    public function setDescriptionAccessoryClothing($descriptionAccessoryClothing = null)
    {
        $this->descriptionAccessoryClothing = $descriptionAccessoryClothing;

        return $this;
    }

    /**
     * Get descriptionAccessoryClothing.
     *
     * @return string|null
     */
    public function getDescriptionAccessoryClothing()
    {
        return $this->descriptionAccessoryClothing;
    }

    /**
     * Set descriptionAccessoryHelmet.
     *
     * @param string|null $descriptionAccessoryHelmet
     *
     * @return Accessory
     */
    public function setDescriptionAccessoryHelmet($descriptionAccessoryHelmet = null)
    {
        $this->descriptionAccessoryHelmet = $descriptionAccessoryHelmet;

        return $this;
    }

    /**
     * Get descriptionAccessoryHelmet.
     *
     * @return string|null
     */
    public function getDescriptionAccessoryHelmet()
    {
        return $this->descriptionAccessoryHelmet;
    }

    /**
     * Set accessoryBlock.
     *
     * @param \Honda\MainBundle\Entity\AccessoryBlock|null $accessoryBlock
     *
     * @return Accessory
     */
    public function setAccessoryBlock(\Honda\MainBundle\Entity\AccessoryBlock $accessoryBlock = null)
    {
        $this->accessoryBlock = $accessoryBlock;

        return $this;
    }

    /**
     * Get accessoryBlock.
     *
     * @return \Honda\MainBundle\Entity\AccessoryBlock|null
     */
    public function getAccessoryBlock()
    {
        return $this->accessoryBlock;
    }

    /**
     * Get the value of accessoryItems
     *
     * @return  ArrayCollection
     */
    public function getAccessoryItems()
    {
        return $this->accessoryItems;
    }

    /**
     * Set the value of accessoryItems
     *
     * @param  ArrayCollection  $accessoryItems
     *
     * @return  self
     */
    public function setAccessoryItems(ArrayCollection $accessoryItems)
    {
        $this->accessoryItems = $accessoryItems;

        return $this;
    }
}
