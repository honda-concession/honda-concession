<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \JMS\Serializer\Annotation As JMS;


/**
 * Distributor
 *
 * @ORM\Table(name="distributor", indexes={@ORM\Index(name="domain_idx", columns={"domain"})})
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\DistributorRepository")
 * @ORM\EntityListeners({"Honda\MainBundle\Doctrine\DistributorListener"})
 * @JMS\ExclusionPolicy("all")
 */
class Distributor
{

    Const BGCOLORS = [
        '' => 'Noir',
        'theme-white' => 'Blanc',
        'theme-red' => 'Rouge',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @var save as json array
     *
     * @ORM\Column(name="infos", type="text", nullable=true)
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $infos;

    /**
     * @var int
     *
     * @ORM\Column(name="distributor_portail_id", type="integer")
     */
    private $distributorPortailId;

    /**
     * @var string
     *
     * @ORM\Column(name="bg_color", type="string", length=255, nullable=true)
     */
    private $bgColor = '#000000';

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255, nullable=true)
     * @JMS\Expose()
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="timetable", type="text", nullable=true)
     */
    private $timetable;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="text", nullable=true)
     */
     private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="text", nullable=true)
     */
     private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="town", type="text", nullable=true)
     */
    private $town;

    /**
     * @var string
     *
     * @ORM\Column(name="department", type="text", nullable=true)
     */
    private $department;

    /**
     * @var string
     *
     * @ORM\Column(name="google_analytic", type="text", nullable=true)
     */
    private $googleAnalytic;

    /**
     * @var string
     *
     * @ORM\Column(name="gtm_code", type="string", nullable=true)
     */
    private $gtmCode;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default": 0})
     * @JMS\Expose()
     */
    public $active = 0;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="insert_picture", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $insertPicture;

    /**
     * @var
     *
     * @ORM\OneToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", mappedBy="distributor", cascade={"all"})
     */
    private $user;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="logo", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $logo;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Honda\MainBundle\Entity\ServicePage", mappedBy="distributor")
     */
    private $servicePage;

    /**
     * @ORM\OneToOne(targetEntity="Honda\MainBundle\Entity\BillingAddress", mappedBy="distributor")
     */
    private $billingAddress;

    /**
     * @ORM\Embedded(class="Honda\MainBundle\Entity\SeoContent", columnPrefix="seo_content_")
     */
    private $seoContent;

    /**
     * Data Source Id
     *
     * @var string
     * @ORM\Column(name="data_studio_id", type="string", length=255, nullable=true)
     */
    private $dataStudioId;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * @return mixed
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }
    
    /**
     * @param mixed $billingAddress
     * @return Distributor
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
        
        return $this;
    }
    
    
    /**
     * Set infos
     *
     * @param array $infos
     *
     * @return Distributor
     */
    public function setInfos($infos)
    {
        if (is_array($infos)) {
    
            $this->infos = json_encode($infos) ;
            
            $errors = json_last_error();
            
            if ($errors) {
                throw  new \RuntimeException('infos is not a valid array');
            }
            
        }

        return $this;
    }

    /**
     * Get infos
     *
     * @return array
     */
    public function getInfos()
    {
        return  json_decode($this->infos);
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     *
     * @return Distributor
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
        $user->setDistributor($this);

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set distributorPortailId
     *
     * @param integer $distributorCentralId
     *
     * @return Distributor
     */
    public function setDistributorPortailId($distributorPortailId)
    {
        $this->distributorPortailId = $distributorPortailId;

        return $this;
    }

    /**
     * Get distributorPortailId
     *
     * @return integer
     */
    public function getDistributorPortailId()
    {
        return $this->distributorPortailId;
    }

    /**
     * Set bgColor
     *
     * @param string $bgColor
     *
     * @return Distributor
     */
    public function setBgColor($bgColor)
    {
        $this->bgColor = $bgColor;

        return $this;
    }

    /**
     * Get bgColor
     *
     * @return string
     */
    public function getBgColor()
    {
        return $this->bgColor;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Distributor
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set logo
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $logo
     *
     * @return Distributor
     */
    public function setLogo(\Application\Sonata\MediaBundle\Entity\Media $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getLogo()
    {
        return $this->logo;
    }


    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return Distributor
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set servicePage
     *
     * @param \Honda\MainBundle\Entity\ServicePage $servicePage
     *
     * @return Distributor
     */
    public function setServicePage(\Honda\MainBundle\Entity\ServicePage $servicePage = null)
    {
        $this->servicePage = $servicePage;

        return $this;
    }

    /**
     * Get servicePage
     *
     * @return \Honda\MainBundle\Entity\ServicePage
     */
    public function getServicePage()
    {
        return $this->servicePage;
    }

    /**
     * Set timetable.
     *
     * @param string $timetable
     *
     * @return Distributor
     */
    public function setTimetable($timetable)
    {
        $this->timetable = $timetable;

        return $this;
    }

    /**
     * Get timetable.
     *
     * @return string
     */
    public function getTimetable()
    {
        return $this->timetable;
    }

    /**
     * Set location.
     *
     * @param string|null $location
     *
     * @return Distributor
     */
    public function setLocation($location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location.
     *
     * @return string|null
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set region.
     *
     * @param string|null $region
     *
     * @return Distributor
     */
    public function setRegion($region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return string|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set department.
     *
     * @param string|null $department
     *
     * @return Distributor
     */
    public function setDepartment($department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department.
     *
     * @return string|null
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set googleAnalytic.
     *
     * @param string|null $googleAnalytic
     *
     * @return Distributor
     */
    public function setGoogleAnalytic($googleAnalytic = null)
    {
        $this->googleAnalytic = $googleAnalytic;

        return $this;
    }

    /**
     * Get googleAnalytic.
     *
     * @return string|null
     */
    public function getGoogleAnalytic()
    {
        return $this->googleAnalytic;
    }

    /**
     * Set active.
     *
     * @param bool|null $active
     *
     * @return Distributor
     */
    public function setActive($active = null)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool|null
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Set town.
     *
     * @param string|null $town
     *
     * @return Distributor
     */
    public function setTown($town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town.
     *
     * @return string|null
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @return null|\Application\Sonata\MediaBundle\Entity\Media
     */
    public function getInsertPicture()
    {
        return $this->insertPicture;
    }

    /**
     * @param mixed $insertPicture
     * @return Distributor
     */
    public function setInsertPicture(\Application\Sonata\MediaBundle\Entity\Media $insertPicture = null)
    {
        $this->insertPicture = $insertPicture;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeoContent()
    {
        return $this->seoContent;
    }

    /**
     * @param mixed $seoContent
     * @return Distributor
     */
    public function setSeoContent(\Honda\MainBundle\Entity\SeoContent $seoContent = null)
    {
        $this->seoContent = $seoContent;

        return $this;
    }

    /**
     * Get the value of gtmCode
     *
     * @return  string
     */
    public function getGtmCode()
    {
        return $this->gtmCode;
    }

    /**
     * Set the value of gtmCode
     *
     * @param  string  $gtmCode
     *
     * @return  self
     */
    public function setGtmCode(?string $gtmCode)
    {
        $this->gtmCode = $gtmCode;

        return $this;
    }

    public function getUrl()
    {
        return "https://{$this->domain}";
    }

    /**
     * Get data Source Id
     *
     * @return  string
     */
    public function getDataStudioId()
    {
        return $this->dataStudioId;
    }

    /**
     * Set data Source Id
     *
     * @param  string  $dataStudioId  Data Source Id
     *
     * @return  self
     */
    public function setDataStudioId(?string $dataStudioId)
    {
        $this->dataStudioId = $dataStudioId;

        return $this;
    }
}
