<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table
 * @ORM\Entity
 */
class Payment
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer $id
     */
    protected $id;
    
    /**
     * @ORM\Column(name="total", type="decimal", precision=10, scale=5, nullable=true)
     */
    protected $total;
    
    /**
     * @ORM\Column(name="currency", type="string", nullable=true)
     */
    protected $currency;
    
    /**
     * @ORM\Column(name="paypal_id", type="string", nullable=true)
     */
    protected $paypalId;
    
    /**
     * @ORM\Column(name="state", type="string", nullable=true)
     */
    protected $state;
    
    /**
     * @ORM\Column(name="create_time", type="datetime", nullable=true)
     */
    protected $createTime;
    
    /**
     * @ORM\OneToOne(targetEntity="Honda\MainBundle\Entity\Subscription", inversedBy="payment")
     */
    protected $subscription;
    
    /**
     * Payment constructor.
     */
    public function __construct()
    {
        $this->createTime = new \DateTime();
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }
    
    /**
     * @param mixed $total
     * @return Payment
     */
    public function setTotal($total)
    {
        $this->total = $total;
        
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    
    /**
     * @param mixed $currency
     * @return Payment
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getPaypalId()
    {
        return $this->paypalId;
    }
    
    /**
     * @param mixed $paypalId
     * @return Payment
     */
    public function setPaypalId($paypalId)
    {
        $this->paypalId = $paypalId;
        
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }
    
    /**
     * @param mixed $state
     * @return Payment
     */
    public function setState($state)
    {
        $this->state = $state;
        
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }
    
    /**
     * @param mixed $createTime
     * @return Payment
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;
        
        return $this;
    }
    

    /**
     * Set subscription.
     *
     * @param \Honda\MainBundle\Entity\Subscription|null $subscription
     *
     * @return Payment
     */
    public function setSubscription(\Honda\MainBundle\Entity\Subscription $subscription = null)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription.
     *
     * @return \Honda\MainBundle\Entity\Subscription|null
     */
    public function getSubscription()
    {
        return $this->subscription;
    }
}
