<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Honda\MainBundle\Entity\Payment;

/**
 * Subscription
 *
 * @ORM\Table(name="subscription")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\SubscriptionRepository")
 */
class Subscription
{

    const STATUS_PAYMENT_REFUSED = 'STATUS_PAYMENT_REFUSED';
    const STATUS_PAYMENT_ACCEPTED = 'STATUS_PAYMENT_ACCEPTED';
    const STATUS_PENDING = 'STATUS_PENDING';

    /**
     * Price of subscription
     */
    const PRICE = 150;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="billing_id", type="string", nullable=true)
     */
    private $billingId;

    /**
     * @ORM\Column(name="year", type="string", nullable=true)
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="period", type="string", nullable=true)
     */
    private $period;

    /**
     * @var float|null
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $price;

    /**
     * @var bool
     *
     * @ORM\Column(name="settlement", type="boolean", options={"default":"0"})
     */
    private $settlement;

    /**
     * @ORM\ManyToOne(targetEntity="Honda\MainBundle\Entity\Distributor")
     */
    private $distributor;

    /**
     * @ORM\Column(name="invoice_url", type="string", nullable=true)
     */
    private $invoice;

    /**
     * @ORM\OneToOne(targetEntity="Honda\MainBundle\Entity\Payment", mappedBy="subscription", cascade={"all"})
     */
    private $payment;

    /**
     * Subscription constructor.
     */
    public function __construct()
    {
        $this->price = self::PRICE;
        $this->status = self::STATUS_PENDING;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     * @return Subscription
     */
    public function setPayment(Payment $payment = null)
    {
        $this->payment = $payment;
        if ($payment) {
            $payment->setSubscription($this);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isStatusPending()
    {
        return $this->status == self::STATUS_PENDING;
    }

    /**
     * @return bool
     */
    public function isStatusAccepted()
    {
        return $this->status == self::STATUS_PAYMENT_ACCEPTED;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * @param mixed $distributor
     * @return Subscription
     */
    public function setDistributor($distributor)
    {
        $this->distributor = $distributor;

        return $this;
    }

    /**
     * Set price.
     *
     * @param float|null $price
     *
     * @return Subscription
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    public function getBillingId()
    {
        return $this->billingId;
    }

    public function setBillingId($billingId)
    {
        $this->billingId = $billingId;
        return $this;
    }

    /**
     * Get the value of year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @return  self
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set the value of invoice
     *
     * @return  self
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get the value of period
     *
     * @return  string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set the value of period
     *
     * @param  string  $period
     *
     * @return  self
     */
    public function setPeriod(string $period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get the value of settlement
     *
     * @return  bool
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * Set the value of settlement
     *
     * @param  bool  $settlement
     *
     * @return  self
     */
    public function setSettlement(bool $settlement)
    {
        $this->settlement = $settlement;

        return $this;
    }
}
