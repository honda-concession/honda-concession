<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class SeoContent
{

    /**
     * seo text for home
     *
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionHome;

    /**
     * @var text
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionPageVO;
    
    /**
     * @var text
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionPageServices;
    
    /**
     * @var text
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionPageAccessoires;
    
    /**
     * @var text
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionPageFicheVo;
    
    /**
     * @return text
     */
    public function getDescriptionPageVO()
    {
        return $this->descriptionPageVO;
    }
    
    /**
     * @param text $descriptionPageVO
     * @return SeoContent
     */
    public function setDescriptionPageVO($descriptionPageVO)
    {
        $this->descriptionPageVO = $descriptionPageVO;
    
        return $this;
    }
    
    /**
     * @return text
     */
    public function getDescriptionPageServices()
    {
        return $this->descriptionPageServices;
    }
    
    /**
     * @param text $descriptionPageServices
     * @return SeoContent
     */
    public function setDescriptionPageServices($descriptionPageServices)
    {
        $this->descriptionPageServices = $descriptionPageServices;
    
        return $this;
    }
    
    /**
     * @return text
     */
    public function getDescriptionPageFicheVo()
    {
        return $this->descriptionPageFicheVo;
    }
    
    /**
     * @param text $descriptionPageFicheVo
     * @return SeoContent
     */
    public function setDescriptionPageFicheVo($descriptionPageFicheVo)
    {
        $this->descriptionPageFicheVo = $descriptionPageFicheVo;
    
        return $this;
    }
    
    /**
     * @return text
     */
    public function getDescriptionPageAccessoires()
    {
        return $this->descriptionPageAccessoires;
    }
    
    /**
     * @param text $descriptionPageAccessoires
     * @return SeoContent
     */
    public function setDescriptionPageAccessoires($descriptionPageAccessoires)
    {
        $this->descriptionPageAccessoires = $descriptionPageAccessoires;
        
        return $this;
    }

    /**
     * Get seo text for home
     *
     * @return  string
     */
    public function getDescriptionHome()
    {
        return $this->descriptionHome;
    }

    /**
     * Set seo text for home
     *
     * @param  string  $descriptionHome  seo text for home
     *
     * @return  self
     */
    public function setDescriptionHome($descriptionHome)
    {
        $this->descriptionHome = $descriptionHome;

        return $this;
    }
}
