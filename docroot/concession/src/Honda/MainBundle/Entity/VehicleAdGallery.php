<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \JMS\Serializer\Annotation As JMS;

/**
 * VehicleAdGallery
 *
 * @ORM\Table(name="vehicle_ad_gallery")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\VehicleAdGalleryRepository")
 * @JMS\ExclusionPolicy("all")
 *
 */
class VehicleAdGallery
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="boolean", nullable=true, options={"default": 0})
     */
    private $isMainImage = false;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Honda\MainBundle\Entity\VehicleAd", inversedBy="vehicleAdGalleries")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $vehicleAd;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Jms\Accessor(getter="getImage")
     * @JMS\Expose()
     * @JMS\Groups({"vo_list"})
     * @Jms\MaxDepth(3)
     */
    protected $image;

    protected $mediaId;

    protected $mediaUrl;

    protected $mediaType;

    protected $reference;

    public function __construct()
    {
        $this->isMainImage = false;
        $this->mediaUrl = null;
        $this->mediaType = null;
    }

    public function __toString()
    {
        return 'image';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vehicleAd
     *
     * @param \Honda\MainBundle\Entity\VehicleAd $vehicleAd
     *
     * @return VehicleAdGallery
     */
    public function setVehicleAd(\Honda\MainBundle\Entity\VehicleAd $vehicleAd = null)
    {
        $this->vehicleAd = $vehicleAd;

        return $this;
    }

    /**
     * Get vehicleAd
     *
     * @return \Honda\MainBundle\Entity\VehicleAd
     */
    public function getVehicleAd()
    {
        return $this->vehicleAd;
    }

    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return VehicleAdGallery
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set isMainImage
     *
     * @param boolean $isMainImage
     *
     * @return VehicleAdGallery
     */
    public function setIsMainImage($isMainImage)
    {
        $this->isMainImage = $isMainImage;

        return $this;
    }

    /**
     * Get isMainImage
     *
     * @return boolean
     */
    public function getIsMainImage()
    {
        return $this->isMainImage;
    }

    /**
     * Get mediaId
     *
     * @return int
     */
    public function getMediaId()
    {
        return !empty($this->image) ? $this->image->getId() : null;
    }

    /**
     * Set mediaUrl
     *
     * @param string $mediaUrl
     *
     * @return VehicleAdGallery
     */
    public function setMediaUrl($mediaUrl)
    {
        $this->mediaUrl = $mediaUrl;

        return $this;
    }

    /**
     * Get mediaUrl
     *
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->mediaUrl;
    }

    /**
     * Set mediaType
     *
     * @param string $mediaType
     *
     * @return VehicleAdGallery
     */
    public function setMediaType($mediaType)
    {
        $this->mediaType = $mediaType;

        return $this;
    }

    /**
     * Get mediaType
     *
     * @return string
     */
    public function getMediaType()
    {
        return $this->mediaType;
    }

    /**
     * Get the value of reference
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set the value of reference
     *
     * @return  self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }
}
