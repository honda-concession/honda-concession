<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Setting
 *
 * @ORM\Table(name="setting")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\SettingRepository")
 */
class Setting
{
    
    CONST UNIQUE_ID = 1;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Mise en avant des sliders du concessionaire
     * @var int
     *
     * @ORM\Column(name="push_foward_slider", type="boolean", options={"default": 0})
     */
    private $pushFowardSlider = false;
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="push_foward_alaune", type="boolean", options={"default": 0})
     */
    private $pushFowardALaune = false;
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="push_foward_news", type="boolean", options={"default": 0})
     */
    private $pushFowardNews = false;
    
    /**
     * @var int
     *
     * @ORM\Column(name="push_foward_service", type="boolean", options={"default": 0})
     */
    private $pushFowardService = false;
    
    
    public function __toString()
    {
        return "Gestion paramètres";
    }
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pushFowardSlider.
     *
     * @param bool $pushFowardSlider
     *
     * @return Setting
     */
    public function setPushFowardSlider($pushFowardSlider)
    {
        $this->pushFowardSlider = $pushFowardSlider;

        return $this;
    }

    /**
     * Get pushFowardSlider.
     *
     * @return bool
     */
    public function getPushFowardSlider()
    {
        return $this->pushFowardSlider;
    }

    /**
     * Set pushFowardALaune.
     *
     * @param bool $pushFowardALaune
     *
     * @return Setting
     */
    public function setPushFowardALaune($pushFowardALaune)
    {
        $this->pushFowardALaune = $pushFowardALaune;

        return $this;
    }

    /**
     * Get pushFowardALaune.
     *
     * @return bool
     */
    public function getPushFowardALaune()
    {
        return $this->pushFowardALaune;
    }

    /**
     * Set pushFowardNews.
     *
     * @param bool $pushFowardNews
     *
     * @return Setting
     */
    public function setPushFowardNews($pushFowardNews)
    {
        $this->pushFowardNews = $pushFowardNews;

        return $this;
    }

    /**
     * Get pushFowardNews.
     *
     * @return bool
     */
    public function getPushFowardNews()
    {
        return $this->pushFowardNews;
    }

    /**
     * Set pushFowardService.
     *
     * @param bool $pushFowardService
     *
     * @return Setting
     */
    public function setPushFowardService($pushFowardService)
    {
        $this->pushFowardService = $pushFowardService;

        return $this;
    }

    /**
     * Get pushFowardService.
     *
     * @return bool
     */
    public function getPushFowardService()
    {
        return $this->pushFowardService;
    }
}
