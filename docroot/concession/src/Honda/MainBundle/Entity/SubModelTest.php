<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubModelTestv (sous modèle en essaie)
 *
 * @ORM\Table(name="sub_model_test")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\SubModelTestRepository")
 */
class SubModelTest
{
    
    use Traits\DistributorTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="sub_model_portail_title", type="string", length=255, nullable=true)
     */
    private $subModelPortailTitle;
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="sub_model_portail_id", type="integer", nullable=true)
     */
    private $subModelPortailId;

    /**
     * @var bool
     *
     * @ORM\Column(name="on_test", type="boolean", nullable=true, options={"default": 0})
     */
    private $onTest;
    
    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255, nullable=true)
     */
    private $model;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="kora_id", type="string", length=255, nullable=true)
     */
    private $koraId;
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

   
    /**
     * Set onTest
     *
     * @param boolean $onTest
     *
     * @return SubModelTest
     */
    public function setOnTest($onTest)
    {
        $this->onTest = $onTest;

        return $this;
    }

    /**
     * Get onTest
     *
     * @return bool
     */
    public function getOnTest()
    {
        return $this->onTest;
    }


    /**
     * Set subModelPortailTitle
     *
     * @param string $subModelPortailTitle
     *
     * @return SubModelTest
     */
    public function setSubModelPortailTitle($subModelPortailTitle)
    {
        $this->subModelPortailTitle = $subModelPortailTitle;

        return $this;
    }

    /**
     * Get subModelPortailTitle
     *
     * @return string
     */
    public function getSubModelPortailTitle()
    {
        return $this->subModelPortailTitle;
    }

    /**
     * Set subModelPortailId
     *
     * @param integer $subModelPortailId
     *
     * @return SubModelTest
     */
    public function setSubModelPortailId($subModelPortailId)
    {
        $this->subModelPortailId = $subModelPortailId;

        return $this;
    }

    /**
     * Get subModelPortailId
     *
     * @return integer
     */
    public function getSubModelPortailId()
    {
        return $this->subModelPortailId;
    }
    
    /**
     * @return int
     */
    public function getKoraId()
    {
        return $this->koraId;
    }
    
    /**
     * @param int $koraId
     * @return SubModelTest
     */
    public function setKoraId($koraId)
    {
        $this->koraId = $koraId;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }
    
    /**
     * @param string $model
     * @return SubModelTest
     */
    public function setModel($model)
    {
        $this->model = $model;
        
        return $this;
    }
}
