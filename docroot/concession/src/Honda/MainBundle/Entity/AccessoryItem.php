<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccessoryItem
 *
 * @ORM\Table(name="accessory_item")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\AccessoryItemRepository")
 */
class AccessoryItem
{

    use Traits\ActivationTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true, unique=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @var array
     *
     * @ORM\Column(name="logos", type="array", nullable=true)
     */
    private $logos;

    /**
     * @var array
     *
     * @ORM\Column(name="logo_links", type="array", nullable=true)
     */
    private $logoLinks;

    /**
     * @var Accessory
     *
     * @ORM\ManyToOne(targetEntity="Honda\MainBundle\Entity\Accessory", inversedBy="accessoryItems")
     */
    private $accessory;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return AccessoryBlock
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return AccessoryBlock
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get the value of description
     *
     * @return  string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param  string  $description
     *
     * @return  self
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of logos
     *
     * @return  string
     */
    public function getLogos()
    {
        return $this->logos;
    }

    /**
     * Set the value of logos
     *
     * @param  string  $logos
     *
     * @return  self
     */
    public function setLogos(array $logos)
    {
        $this->logos = $logos;

        return $this;
    }

    /**
     * Get the value of accessory
     *
     * @return  Accessory
     */
    public function getAccessory()
    {
        return $this->accessory;
    }

    /**
     * Set the value of accessory
     *
     * @param  Accessory  $accessory
     *
     * @return  self
     */
    public function setAccessory(Accessory $accessory)
    {
        $this->accessory = $accessory;

        return $this;
    }

    /**
     * Get the value of logoLinks
     *
     * @return  string
     */
    public function getLogoLinks()
    {
        return $this->logoLinks;
    }

    /**
     * Set the value of logoLinks
     *
     * @param  string  $logoLinks
     *
     * @return  self
     */
    public function setLogoLinks(array $logoLinks)
    {
        $this->logoLinks = $logoLinks;

        return $this;
    }

    /**
     * Get the value of icon
     *
     * @return  string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set the value of icon
     *
     * @param  string  $icon
     *
     * @return  self
     */
    public function setIcon(?string $icon)
    {
        $this->icon = $icon;

        return $this;
    }
}
