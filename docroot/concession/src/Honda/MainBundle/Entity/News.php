<?php

namespace Honda\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="Honda\MainBundle\Repository\NewsRepository")
 */
class News
{

    use Traits\DistributorTrait,
        Traits\StartDateEndDateTrait,
        Traits\HomeActivationTrait,
        Traits\ActivationTrait,
        Traits\RedirectionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="description1", type="text", nullable=true)
     */
    private $description1;

    /**
     * @var string
     *
     * @ORM\Column(name="description2", type="text", nullable=true)
     */
    private $description2;

    /**
     * @var string
     *
     * @ORM\Column(name="description3", type="text", nullable=true)
     */
    private $description3;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(unique=true)
     */
    private $slug;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media",  cascade={"all"}, fetch="LAZY")
     * @ORM\JoinColumn(name="image", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $image;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Honda\MainBundle\Entity\NewsSlide", orphanRemoval=true, fetch="EXTRA_LAZY", mappedBy="news", cascade={"all"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $slides;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $pdf;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var bool
     *
     * @ORM\Column(name="link_in_new_windows", type="boolean")
     */
    private $linkInNewWindow = false;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW', new \DateTimeZone('Europe/Paris'));
        $this->slides = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            return $this->getTitle();
        }

        return '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description1
     *
     * @param string $description1
     *
     * @return News
     */
    public function setDescription1($description1)
    {
        $this->description1 = $description1;

        return $this;
    }

    /**
     * Get description1
     *
     * @return string
     */
    public function getDescription1()
    {
        return $this->description1;
    }

    /**
     * Set description2
     *
     * @param string $description2
     *
     * @return News
     */
    public function setDescription2($description2)
    {
        $this->description2 = $description2;

        return $this;
    }

    /**
     * Get description2
     *
     * @return string
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * Set description3
     *
     * @param string $description3
     *
     * @return News
     */
    public function setDescription3($description3)
    {
        $this->description3 = $description3;

        return $this;
    }

    /**
     * Get description3
     *
     * @return string
     */
    public function getDescription3()
    {
        return $this->description3;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return News
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return News
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add slide
     *
     * @param Honda\MainBundle\Entity\NewsSlide $slide
     *
     * @return News
     */
    public function addSlide(NewsSlide $slide)
    {
        $this->slides[] = $slide;
        $slide->setNews($this);

        return $this;
    }

    /**
     * Remove slide
     *
     * @param Honda\MainBundle\Entity\NewsSlide $slide
     *
     * @return News
     */
    public function removeSlide(NewsSlide $slide)
    {
        $this->slides->removeElement($slide);
    }

    /**
     * Set slides
     *
     * @param ArrayCollection $slides
     *
     * @return News
     */
    public function setSlides($slides)
    {
        $this->slides = $slides;

        return $this;
    }

    /**
     * Get slides
     *
     * @return ArrayCollection
     */
    public function getSlides()
    {
        return $this->slides;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return News
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ALaUne
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Get the value of pdf
     *
     * @return  \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set the value of pdf
     *
     * @param  \Application\Sonata\MediaBundle\Entity\Media  $pdf
     *
     * @return  self
     */
    public function setPdf(\Application\Sonata\MediaBundle\Entity\Media $pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get the value of link
     *
     * @return  string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set the value of link
     *
     * @param  string  $link
     *
     * @return  self
     */
    public function setLink(?string $link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get the value of linkInNewWindow
     *
     * @return  boolean
     */
    public function getLinkInNewWindow()
    {
        return $this->linkInNewWindow;
    }

    /**
     * Set the value of linkInNewWindow
     *
     * @param  boolean  $linkInNewWindow
     *
     * @return  self
     */
    public function setLinkInNewWindow(bool $linkInNewWindow)
    {
        $this->linkInNewWindow = $linkInNewWindow;

        return $this;
    }
}
