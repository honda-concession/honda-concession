<?php
namespace Honda\MainBundle\EventSubscriber;

use Honda\MainBundle\Entity\VehicleAd;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Model\MediaManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VehicleAdSerializationSubscribers
 * @package Honda\MainBundle\EventSubscriber
 */
class VehicleAdSerializationSubscribers implements EventSubscriberInterface
{

    /**
     *
     * @var type ContainerInterface
     */
    protected $container;

    /**
     * @var MediaManagerInterface
     */
    protected $mediaManager;

    /**
     * VehicleAdSerializationSubscribers constructor.
     * @param ContainerInterface $container
     * @param MediaManagerInterface $mediaManager
     */
    public function __construct(ContainerInterface $container, MediaManagerInterface $mediaManager)
    {
        $this->container = $container;
        $this->mediaManager = $mediaManager;
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        $vehicleAd = $event->getObject();

        $domain = null;

        $vehicleAdGalleries = $vehicleAd->getVehicleAdGalleries();
        $vehicleAdGallery = $vehicleAdGalleries->first();

        $url = null;

        if ($vehicleAdGallery) {
            $media = $vehicleAdGallery->getImage();

            if ($media) {
                $provider = $this->container->get($media->getProviderName());

                if ($vehicleAd->getDistributor() && $vehicleAd->getDistributor()->getDomain()) {

                    $url = $this->container->get('request_stack')->getCurrentRequest()->getScheme() .'://'.  $vehicleAd->getDistributor()->getDomain() . $provider->generatePublicUrl($media, 'admin');
                }
            }

        }

        if ($distributor = $vehicleAd->getDistributor()) {

            $name = null;

            if ($distributor->getInfos() && $distributor->getInfos()->name) {
                $name = $distributor->getInfos()->name;
                $url = preg_replace('/_distributor/', "_{$this->getDirectoryName($distributor->getInfos()->url)}", $url);
            }

            $event->getVisitor()->addData('distributor_name', $name);
        }

        $event->getVisitor()->addData('image', $url);
    }

    private function getDirectoryName($url)
    {
        $sites = $this->getSites();

        return $sites[$url];
    }

    private function getSites()
    {
        $sm = $this->container->get('multisite.manager');
        $sites = $sm->list();
        $arrFliped = [];
        foreach ($sites as $folder => $infos) {
            foreach ($infos as $info) {
                $arrFliped[$info['host']] = $folder;
            }
        }

        return $arrFliped;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event' => 'serializer.post_serialize',
                'class' => VehicleAd::class,
                'method' => 'onPostSerialize'
            ],
        ];
    }
}
