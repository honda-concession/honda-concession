<?php

namespace Honda\MainBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Honda\MainBundle\Event\SubscriptionEvent;

/**
 * Class SubscriptionSubscribers
 * @package Honda\MainBundle\EventSubscriber
 */
class SubscriptionSubscribers implements EventSubscriberInterface
{

    /**
     * @param SubscriptionEvent $event
     */
    public function paymentSuccess(SubscriptionEvent $event)
    {
        //success
    }
    
    
    /**
     * @param SubscriptionEvent $event
     */
    public function paymentError(SubscriptionEvent $event)
    {
        // Error
    }
    
    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            SubscriptionEvent::PAYMENT_SUCCESS => 'paymentSuccess',
            SubscriptionEvent::PAYMENT_ERROR => 'paymentError',
        ];
    }
}
