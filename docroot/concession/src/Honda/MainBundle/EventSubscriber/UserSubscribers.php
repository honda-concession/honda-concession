<?php

namespace Honda\MainBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Honda\MainBundle\Event\UserEvent;
use Honda\MainBundle\Model\Admin\DistributorManager;


/**
 * Class UserSubscribers
 * @package Honda\MainBundle\EventSubscriber
 */
class UserSubscribers implements EventSubscriberInterface
{
    /**
     * @var DistributorManager
     */
    protected $distributorManager;
    
    /**
     * UserSubscribers constructor.
     * @param DistributorManager $distributorManager
     */
    public function __construct(DistributorManager $distributorManager)
    {
        $this->distributorManager = $distributorManager;
    }
    
    /**
     * @param UserEvent $event
     */
    public function updateDistributorInfos(UserEvent $event)
    {
        $this->distributorManager->saveDistributorInfosFromPortail($event->getUser());
    }
    
    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::ON_LOGIN_UPDATE_DISTRIBUTOR_INFOS => 'updateDistributorInfos',
        ];
    }
}
