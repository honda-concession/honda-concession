<?php

namespace Honda\MainBundle\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Honda\MainBundle\Tools\Encryption\EncryptorAesEcb;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;

class PortalSiteGateway
{

    /**
     * @var array
     */
    private $endpoints;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $env;

    /**
     * @var Client
     */
    private $client;

    public function __construct($endpoints, $key, $env)
    {
        $this->endpoints = $endpoints;
        $this->key = $key;
        $this->env = $env;
        $this->client = new Client(['timeout' => 30]);
    }

    /**
     * @param $data
     * @return array|mixed|\Psr\Http\Message\ResponseInterface
     */
    public function create($data)
    {

        $params = [
            'subject' => $data['subject'],
            'message' => $data['message'],
            'priority' => $data['priority'],
            'distributor' => $data['distributor_portail_id'],
            'filename' => $data['filename'],
            'size' => $data['size'],
            'mimetype' => $data['mimetype'],
        ];

        $token = EncryptorAesEcb::encode(json_encode($params), $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        $res = [];

        try {
            $res = $this->client->request('POST', $this->endpoints['create'], [
                //'headers' => ['X-AUTH-TOKEN' => $token],
                'verify' => $sslVerification,
                'body' => json_encode($params)
            ]);
            
            $res = $res->getBody(true)->getContents();
            
        } catch (ClientException $e) { 
        } catch (ServerException $e) {
            dump($e);
            exit;
        }

        return $res;
    }

    /**
     * @param $data
     * @return array|mixed|\Psr\Http\Message\ResponseInterface
     */
    public function reply($data)
    {

        $params = [
            'message' => $data['message'],
            'priority' => $data['priority'],
            'status' => $data['status'],
            'distributor' => $data['distributor_portail_id'],
            'ticketId' => $data['ticketId'],
            'filename' => $data['filename'],
            'size' => $data['size'],
            'mimetype' => $data['mimetype'],
        ];

        $token = EncryptorAesEcb::encode(json_encode($params), $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        $res = [];

        $endpoint = str_replace(':ticketId', $data['ticketId'], $this->endpoints['reply']);

        try {
            $res = $this->client->request('POST', $endpoint, [
                //'headers' => ['X-AUTH-TOKEN' => $token],
                'verify' => $sslVerification,
                'body' => json_encode($params)
            ]);

            $res = $res->getBody(true)->getContents();
        } catch (ClientException $e) {
        
        }

        return $res;
    }

    /**
     * get list tickets
     *
     * @return array
     */
    public function getList($distributorId)
    {
        $timestamp = strtotime("now");

        $token = EncryptorAesEcb::encode($timestamp, $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        $tickets = [];

        try {
            $res = $this->client->request('GET', $this->endpoints['list'] . '/' . $distributorId, [
                'headers' => ['Accept' => 'application/json'],
                'verify' => $sslVerification,
            ]);

            $tickets = json_decode($res->getBody()->getContents(), true);
            
        } catch (ClientException $e) {
            dump($e);
            exit;
        }


        return $tickets;
    }

    /**
     * get message
     *
     * @return array
     */
    public function getMessage($ticketId, $distributorId)
    {

        $timestamp = strtotime("now");

        $token = EncryptorAesEcb::encode($timestamp, $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        $tickets = [];

        $endpoint = str_replace(':ticketId', $ticketId, $this->endpoints['show']);

        try {
            $res = $this->client->request('GET', $endpoint . '/' . $distributorId, [
                'headers' => ['Accept' => 'application/json'],
                'verify' => $sslVerification,
            ]);


            $tickets = json_decode($res->getBody()->getContents(), true);
        } catch (ClientException $e) {
            dump($e);
            exit;
        }

        return $tickets;
    }

    /**
     * get list tickets
     *
     * @return array
     */
    public function download($messageId, $fileName)
    {
        $timestamp = strtotime("now");

        $token = EncryptorAesEcb::encode($timestamp, $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        try {
            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/attachment')) {
                mkdir($_SERVER['DOCUMENT_ROOT'] . '/uploads/attachment', 0777, true);
            }

            $endpoint = str_replace('/api', '', $this->endpoints['download']);

            $res = $this->client->request('GET', $endpoint . '/' . $fileName, [
                'verify' => $sslVerification,
                'sink' => $_SERVER['DOCUMENT_ROOT'] . '/uploads/attachment/' . $fileName
            ]);

            $response['fileName'] = $fileName;
            $response['mimetype'] = Psr7\mimetype_from_filename($fileName);
        } catch (ClientException $e) {
            dump($e);
            exit;
        }

        return $response;
    }

    /**
     * @param $file
     * @return array|mixed|\Psr\Http\Message\ResponseInterface
     */
    public function postFile($file)
    {
        $token = EncryptorAesEcb::encode(json_encode($file), $this->key);

        $sslVerification = false;

        if ($this->env != "dev") {
            $sslVerification = true;
        }

        $res = [];

        try {
            $res = $this->client->request('POST', $this->endpoints['save'], [
                //'headers' => ['X-AUTH-TOKEN' => $token],
                'verify' => $sslVerification,
                'multipart' => [
                    [
                        'name'     => 'fileContent',
                        'filename' => $file->getClientOriginalName(),
                        'Mime-Type'=> $file->getClientMimeType(),
                        'contents' => fopen($file->getPathname(), 'r' ),
                    ],
  
                ]
            ]);

            $res = $res->getBody()->getContents();

        } catch (ClientException $e) {
        
        }

        return $res;
    }

}
