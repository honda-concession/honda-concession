<?php

namespace Honda\MainBundle\Search;


use Symfony\Component\HttpFoundation\RequestStack;

/**
 * On parse la requête http, pour récupérer les critéres de recherche
 *
 * Class RequestCriteria
 * @package Honda\MainBundle\Search
 */
class RequestCriteria
{
    
    /**
     * @var RequestStack
     */
    protected $requestStack;
    
    /**
     * @var array
     */
    protected $filters;
    
    /**
     * RequestCriteria constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->filters = [];
    }
    
    /**
     *  On parse la requête http
     * @return array
     */
    public function getFilters()
    {
        $request = $this->requestStack->getCurrentRequest();
        
        return [];
    }
    
    
}


