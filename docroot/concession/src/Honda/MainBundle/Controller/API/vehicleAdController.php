<?php

namespace Honda\MainBundle\Controller\API;

use Honda\MainBundle\Entity\VehicleAd;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializationContext;


/**
 * @Route("/api", name="api_")
 */
class vehicleAdController extends Controller
{
    
    /**
     * @Route("/vehicleAd/list", name="vehiclead_list", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function listAction()
    {
        $vehicleAds = $this->getDoctrine()->getRepository(VehicleAd::class)->getAll();
        
        //dump($vehicleAds); exit;

        return $this->jsonResponse($vehicleAds, JsonResponse::HTTP_OK, ['vo_list']);
    }
    
    
    /**
     * @param mixed $data
     * @param int $code
     * @param array $groups
     * @return JsonResponse
     */
    public function jsonResponse($data, $code = 200, $groups = [])
    {
        $context = new SerializationContext();
        $defaultGroup[] = 'Default';
        $context->setGroups(array_merge($defaultGroup, $groups));
        $context->enableMaxDepthChecks();
        
        $json = $this->get('jms_serializer')->serialize($data, 'json', $context);
        
        return new JsonResponse($json, $code, [], true);
    }
    
}
