<?php

namespace Honda\MainBundle\Controller\API;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Honda\MainBundle\Tools\Encryption\EncryptorAesEcb;
use Honda\MainBundle\Validator\SiteValidator;
use Honda\MainBundle\Entity\Distributor;
use Application\Sonata\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class SiteController extends Controller
{
    /**
     * @Route("/api/site/create", name="api_site_create", methods={"POST"})
     *
     * @param array ['name', 'host', 'locale', 'status', 'cid', 'email', 'password']
     */
    public function createAction(Request $request)
    {
        $token = $request->headers->get('X-AUTH-TOKEN');

        $content = $request->getContent();

        $this->checkToken($token, $content);

        $data = json_decode($content, true);

        $sm = $this->container->get('multisite.manager');

        $errors = $this->validateSiteInfos($data, $sm);

        if(!empty($errors)) {
            return new JsonResponse(['errors' => $errors]);
        }

        $errors = $this->validateDistributorInfos($data);

        if(!empty($errors)) {
            return new JsonResponse(['errors' => $errors]);
        }

        try {
            $sm->create($data['name'], $data['host'], $data['locale']);
        } catch (\Exception $e) {
            return new JsonResponse(['errors' => [$e->getMessage() .'  -   '. $e->getTraceAsString()]]);
        }

        $sites = $sm->list();

        $status = isset($data['status']) ?? false;

        $sites[$data['name']][] = [
            'host' => $data['host'],
            'locale' => $data['locale'],
            'active' => $status
        ];
        $sm->updateSiteConfigs($sites);

        $distributor = $this->getDistributorFromData($data);
        $distributor
            ->setDomain($data['host'])
            ->setDistributorPortailId($data['cid'])
            ->setActive((boolean) $status);

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user
            ->setDistributor($distributor)
            ->setUsername($data['email'])
            ->setEmail($data['email'])
            ->setPlainPassword($data['password'])
            ->setEnabled(true)
            ->setRoles(array('ROLE_DISTRIBUTOR', 'SONATA'));

        $userManager->updateUser($user);

        return new JsonResponse(['message' => 'The site and user have been created'], 201);
    }

    /**
     * @Route("/api/site/list/{timestamp}", name="api_site_list", methods={"GET"}, defaults={"timestamp" : null})
     *
     */
    public function listAction(Request $request, $timestamp = null)
    {
        $token = $request->headers->get('X-AUTH-TOKEN');

        $this->checkToken($token, $timestamp);

        $repository = $this->getDoctrine()->getRepository(User::class);

        $query = $repository->createQueryBuilder('u')
            ->select(['u.username', 'u.email', 'u.lastLogin', 'd.distributorPortailId', 'd.domain', 'd.active'])
            ->innerJoin('u.distributor', 'd')
            ->where('u.roles NOT LIKE :roles')
            ->setParameter('roles', '%ROLE_ROOT%')
            ->getQuery();

        $users = $query->getResult();

        return new JsonResponse($users);
    }

    /**
     * @Route("/api/site/{distributorPortailId}/{timestamp}", name="api_site_get", methods={"GET"}, defaults={"timestamp" : null})
     *
     */
    public function getSiteAction(Request $request, $distributorPortailId, $timestamp = null)
    {
        $token = $request->headers->get('X-AUTH-TOKEN');
        $this->checkToken($token, $timestamp);

        $repository = $this->getDoctrine()->getRepository(Distributor::class);
        $distributor = $repository->findOneBy(['distributorPortailId' => $distributorPortailId]);

        $data = [];
        $sm = $this->container->get('multisite.manager');
        $sites = $sm->list();

        if ($distributor) {

            $site = '';
            $siteName = '';

            foreach ($sites as $name => $siteConfigs) {

                if (isset($siteConfigs[0]) && isset($siteConfigs[0]['host'])
                    && $distributor->getDomain() == $siteConfigs[0]['host']) {

                    $siteName = $name;
                    $site = $sm->find($name);
                    break;
                }
            }

            if (isset($site[0])) {
                $site[0]['active'] = $site[0]['active'] && $distributor->getActive();
                $data = array_merge(['siteName' => $siteName, 'distributorId' => $distributorPortailId], $site[0]);
            } else  {
                return $this->json(['errors' => 'Website not found in database ou config file'], Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->json($data);
    }

    /**
     * @Route("/api/site/update/{distributorPortailId}/{timestamp}", name="api_site_update", requirements={"distributorPortailId": "\d+"}, methods={"POST", "PUT"}, defaults={"timestamp" : null})
     */
    public function updateSiteAction(Request $request, $distributorPortailId, $timestamp = null)
    {
        $token = $request->headers->get('X-AUTH-TOKEN');
        $content = $request->getContent();
        $this->checkToken($token, $content);

        $errors = [];
        $data = json_decode($content, true);

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Distributor::class);
        $distributor = $repository->findOneBy(['distributorPortailId' => $distributorPortailId]);

        if (!isset($data['host']) || !SiteValidator::validateHost($data['host'])) {
            $errors['host'] = "The url is not valid, expecting something like site.demo.com.";
        }

        if (!$distributor && !$distributor instanceof Distributor) {
            $errors['distributor'] = "Distributor not found";
        }

        if (!empty($errors)) {
            return $this->json(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        }

        $sm = $this->container->get('multisite.manager');
        $userManager = $this->container->get('fos_user.user_manager');
        try {

            // Mise à jour du fichier sites.yml
            $sites = $sm->list();

            foreach($sites as $key => $site) {
                if (trim($site[0]['host']) == trim($data['host'])) {
                    $data['siteName'] = $key;
                }
            }

            //if (isset($sites[$data['siteName']]) && count($sites[$data['siteName']])) {

                // On initialise le tableau
                $sites[$data['siteName']] = [];

                $sites[$data['siteName']][] = [
                    'host' => $data['host'],
                    'locale' => $data['locale'],
                    'active' => (boolean) $data['status']
                ];
                $sm->updateSiteConfigs($sites);

                // Mise à jour de la base de données concessionaire
                $distributor
                    ->setDomain($data['host'])
                    ->setActive((boolean) $data['status'])
                    ->setGtmCode($data['gtmCode'])
                ;
                $user = $distributor->getUser();
                if (!$user) {
                    $user = $userManager->createUser();
                }
                $user
                    ->setDistributor($distributor)
                    ->setUsername($data['email'])
                    ->setEmail($data['email'])
                    ->setPlainPassword($data['password'])
                    ->setEnabled(true)
                    ->setRoles(array('ROLE_DISTRIBUTOR', 'SONATA'));

                $distributor->setUser($user);
                $userManager->updateUser($user, false);

                $em->persist($distributor);
                $em->flush();

            //}

        } catch (\Exception $e) {
        } catch (ORMException $e) {
        } catch (DBALException $e) {
            return $this->json($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        return $this->json("", Response::HTTP_NO_CONTENT);
    }

    /**
     * Validation site informations
     */
    protected function validateSiteInfos($data, $sm)
    {
        $errors = [];

        if(!isset($data['name']) || !SiteValidator::validateSiteName($data['name'])) {
            $errors['name'] = "Site name is not valid, the format should be (a-zA-Z0-9_).";
        }

        if(!isset($data['host']) || !SiteValidator::validateHost($data['host'])) {
            $errors['host'] = "The url is not valid, expecting something like site.demo.com.";
        }

        if(!isset($data['locale'])) {
            $errors['locale'] = "The locale is not valid.";
        }

        /*if(empty($errors)) {
            if(!$sm->checkHost($data['name'], $data['host'])) {
                $errors['host'] = 'Url '.$data['host'].' already exists.';
            }
        }*/

        if(empty($errors)) {
            if($sm->exists($data['name'])) {
                $errors['name'] = 'Site name '.$data['name'].' already exists.';
            }
        }

        return $errors;
    }

    /**
     * Validation distributor informations
     */
    protected function validateDistributorInfos($data)
    {
        $errors = [];

        $em = $this->container->get('doctrine.orm.entity_manager');
        $distributorRepository = $em->getRepository(Distributor::class);

        /*if(!isset($data['host']) || !SiteValidator::validateHost($data['host'])) {
            $errors['host'] = "Distributor url is not valid, expecting something like site.demo.com.";
        } else {
            $distributor = $distributorRepository->findOneBy(['domain' => $data['host']]);

            if($distributor) {
                $errors['host'] = "There is already an account with this 'host' for this distributor. " . $distributor->getDomain();
            }
        }

        if(!isset($data['cid'])) {
            $errors['cid'] = "Distributor not found.";
        } else {
            $distributor = $distributorRepository->findOneBy(['distributorPortailId' => $data['cid']]);

            if($distributor) {
                $errors['host'] = "There is already an account with this cid for this distributor. " . $distributor->getDomain();
            }
        }*/

        if(!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = "Distributor email is not valid.";
        } else {
            $userManager = $this->container->get('fos_user.user_manager');
            $user = $userManager->findUserByEmail($data['email']);
            if($user) {
                $errors['email'] = "Distributor email already exists.";
            }
        }

        if(!isset($data['password']) || strlen($data['password']) <= 3 ) {
            $errors['email'] = "Distributor email is not valid, the password required more than 3 characters.";
        }

        return $errors;

    }

    public function checkToken($token, $content)
    {
        $key = $this->container->getParameter('api_multisite_token');

        $encodeToken = EncryptorAesEcb::encode($content, $key);

        if($token !== $encodeToken) {
            throw new NotFoundHttpException("Page not found.");
        }
    }

    private function getDistributorFromData(array $data = [])
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $distributorRepository = $em->getRepository(Distributor::class);
        $distributor = $distributorRepository->findOneBy(['domain' => $data['host']]);
        if ($distributor) {
            return $distributor;
        }
        $distributor = $distributorRepository->findOneBy(['distributorPortailId' => $data['cid']]);
        if ($distributor) {
            return $distributor;
        }

        return new Distributor();
    }
}
