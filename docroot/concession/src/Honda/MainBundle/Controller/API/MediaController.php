<?php

namespace Honda\MainBundle\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MediaController extends Controller
{
    /**
     * @Route("/api/media/{mid}/{format}", name="api_media", requirements={"mid"="\d+"})
     */
    public function mediaAction($mid, $format = 'reference')
    {
        $cache = new FilesystemAdapter();

        $cacheFile = $cache->getItem('media_'.md5($mid.$format));

        if(!$cacheFile->isHit()) {

            $projectDir = $this->get('kernel')->getProjectDir();

            $mediaManager = $this->container->get('sonata.media.manager.media');

            $media = $mediaManager->findOneBy(array(
                'id' => $mid
            ));

            $provider = $this->container->get($media->getProviderName());

            $format = $provider->getFormatName($media, $format);

            $file = $projectDir.'/web/'.$provider->generatePublicUrl($media, $format);

            $response = new BinaryFileResponse($file);

            $cacheFile->set($response);
            $cache->save($cacheFile);
        }

        return $cacheFile->get();
    }

}
