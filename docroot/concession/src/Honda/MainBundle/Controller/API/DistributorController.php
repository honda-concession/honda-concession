<?php

namespace Honda\MainBundle\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Honda\MainBundle\Tools\Encryption\EncryptorAesEcb;
use Honda\MainBundle\Validator\SiteValidator;
use Honda\MainBundle\Entity\Distributor;

class DistributorController extends Controller
{
    /**
     * @Route("/api/distributor/create", name="api_distributor_create", methods={"POST"})
     *
     * @param array ['host', 'cid', 'email', 'password']
     */
    public function createAction(Request $request)
    {
        $token = $request->headers->get('X-AUTH-TOKEN');
        $key = $this->container->getParameter('api_multisite_token');
        $encodeToken = EncryptorAesEcb::encode($request->getContent(), $key);

        if($token !== $encodeToken) {
            throw new NotFoundHttpException("Page not found.");
        }

        $data = json_decode($request->getContent(), true);

        $errors = $this->validateDistributorInfos($data);

        if(!empty($errors)) {
            return new JsonResponse(['errors' => $errors], 400);
        }
        
        $userManager = $this->container->get('fos_user.user_manager');

        $distributor = new Distributor();
        $distributor
            ->setDomain($data['host'])
            ->setDistributorPortailId($data['cid']);

        $user = $userManager->createUser();
        $user
            ->setDistributor($distributor)
            ->setUsername($data['email'])
            ->setEmail($data['email'])
            ->setPlainPassword($data['password'])
            ->setEnabled(true)
            ->setRoles(array('ROLE_DISTRIBUTOR', 'SONATA'));

        $userManager->updateUser($user);

        return new JsonResponse(['message' => 'The account has been created'], 201);
    }

    /**
     * Validation distributor informations
     */
    protected function validateDistributorInfos($data)
    {
        $errors = [];

        $em = $this->container->get('doctrine.orm.entity_manager');
        $distributorRepository = $em->getRepository(Distributor::class);

        if(!isset($data['host']) || !SiteValidator::validateHost($data['host'])) {
            $errors['host'] = "Distributor url is not valid, expecting something like site.demo.com.";
        } else {
            $distributor = $distributorRepository->findOneBy(['domain' => $data['host']]);

            if($distributor) {
                $errors['host'] = "There is already an account for this distributor.";
            }
        }

        if(!isset($data['cid'])) {
            $errors['cid'] = "Distributor not found.";
        } else {
            $distributor = $distributorRepository->findOneBy(['distributorPortailId' => $data['cid']]);

            if($distributor) {
                $errors['host'] = "There is already an account for this distributor.";
            }
        }

        if(!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = "Distributor email is not valid.";
        }

        if(!isset($data['password']) || strlen($data['password']) <= 3 ) {
            $errors['email'] = "Distributor email is not valid, the password required more than 3 characters.";
        }

        return $errors;
    }
    
    /**
     * @Route("/api/distributor/update/{distributorPortailId}", name="api_distibutor_update", methods={"POST", "PUT"}, requirements={"distributorPortailId": "\d+"})
     */
    public function updateDistributorAction(Request $request, $distributorPortailId)
    {
        $token = $request->headers->get('X-AUTH-TOKEN');
        $key = $this->container->getParameter('api_multisite_token');
        $encodeToken = EncryptorAesEcb::encode($request->getContent(), $key);
        
        if ($token !== $encodeToken) {
            throw new NotFoundHttpException("Page not found.");
        }
        
        $em = $this->getDoctrine()->getManager();
        $distributor = $em->getRepository(Distributor::class)->findOneBy(['distributorPortailId' => $distributorPortailId]);
        
        if (!$distributor) {
            throw new NotFoundHttpException('Concessionaire introuvable');
        }
    
        $data = json_decode($request->getContent(), true);
        
        $distributor
            ->setInfos($data)
            ->setRegion($data['region'])
            ->setDepartment($data['department'])
            ->setLocation($data['location'])
            ->setPhone($data['phone'])
            ->setDomain($data['url'])
            ->setDataStudioId($data['dataStudioId'])
        ;
        $em->flush();
        
        return $this->json('', Response::HTTP_NO_CONTENT);
    }
    
}
