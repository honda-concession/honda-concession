<?php

namespace Honda\MainBundle\Controller\API;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use Honda\MainBundle\Mailer\Mailer;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Honda\MainBundle\Tools\Encryption\EncryptorAesEcb;
use Honda\MainBundle\Validator\SiteValidator;
use Honda\MainBundle\Entity\Distributor;
use Application\Sonata\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class TicketController extends Controller
{

    /**
     * @Route("/api/ticket/create", name="api_ticket_create", methods={"POST"})
     *
     * @param $request
     */
    public function createAction(Request $request)
    {
        $client = $this->container->get('honda_main.site_gateway');

        $data = $request->request->get('honda_mainbundle_admin_support');
        
        $files = $request->files->get('honda_mainbundle_admin_support');
        
        $data['filename'] = null;
        $data['size'] = null;
        $data['mimetype'] = null;

        if ($file = $files['file']) {
            $fileName = $file->getClientOriginalName();

            $post = $client->postFile($file);
            
            $data['filename'] = $fileName;
            $data['size'] = $file->getClientSize();
            $data['mimetype'] = $file->getClientMimeType();
        }
        
        $res = $client->create($data);

        $ticketObject = json_decode($res);

        $ticketId = $ticketObject->ticketId;

        $this->addFlash(
                'success', 'Votre message a bien eté envoyé.'
        );
        
        $this->get(Mailer::class)->messageNotificationCentral($ticketObject);

        return $this->redirectToRoute('support_create');
    }

    /**
     * @Route("/api/ticket/{ticketId}/reply", name="api_ticket_reply", methods={"POST"})
     *
     * @param $request
     */
    public function replyAction(Request $request)
    {
        $client = $this->container->get('honda_main.site_gateway');

        $data = $request->request->get('honda_mainbundle_admin_support_message');

        $files = $request->files->get('honda_mainbundle_admin_support_message');
        
        $data['filename'] = null;
        $data['size'] = null;
        $data['mimetype'] = null;

        if ($file = $files['file']) {
            $fileName = $file->getClientOriginalName();

            $post = $client->postFile($file);
            
            $data['filename'] = $fileName;
            $data['size'] = $file->getClientSize();
            $data['mimetype'] = $file->getClientMimeType();
        }

        $ticketId = (int) $request->attributes->get('ticketId');

        $data['ticketId'] = $ticketId;

        $res = $client->reply($data);
    
        $ticketObject = json_decode($res);

        $this->addFlash(
                'success', 'Votre message a bien eté envoyé.'
        );
    
        $this->get(Mailer::class)->messageNotificationCentral($ticketObject, [], true);
        
        return $this->redirectToRoute('support_edit', array('id' => $ticketId));
    }

    /**
     * @Route("/api/ticket/attachment/{messageId}/{fileName}", name="api_message_attachment", methods={"GET"})
     *
     * @param $request
     */
    public function downloadAction(Request $request, $messageId = null, $fileName = null)
    {
        $client = $this->container->get('honda_main.site_gateway');

        $messageId = $request->attributes->get('messageId');
        $fileName = $request->attributes->get('fileName');

        $res = $client->download($messageId, $fileName);

        $response = new BinaryFileResponse($this->getParameter('ticket_attachment') . $res['fileName']);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $res['fileName']);
        $response->headers->set('Content-type', $res['mimetype']);

        return $response;
    }

    public function checkToken($token, $content)
    {
        $key = $this->container->getParameter('api_multisite_token');

        $encodeToken = EncryptorAesEcb::encode($content, $key);

        if ($token !== $encodeToken) {
            throw new NotFoundHttpException("Page not found.");
        }
    }

}
