<?php

namespace Honda\MainBundle\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Honda\MainBundle\Tools\Encryption\EncryptorAesEcb;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;

/**
 * Class ElasticsearchController
 * @package Honda\MainBundle\Controller\API
 */
class ElasticsearchController extends Controller
{
    
    /**
     * @Route("/api/es/populate/{timestamp}", name="api_es_populate", defaults={"timestamp" : null}, methods={"POST"} )
     */
    public function populateAction(Request $request, $timestamp = null, KernelInterface $kernel)
    {
        $token = $request->headers->get('X-AUTH-TOKEN');
        $this->checkToken($token, $timestamp);
    
        $application = new Application($kernel);
        $application->setAutoExit(false);
    
        $env = $this->get('kernel')->getEnvironment();
        
        $input = new ArrayInput([
            'command' => 'fos:elastica:populate',
            '--env' => $env,
        ]);
    
        $output = new BufferedOutput(
            OutputInterface::VERBOSITY_NORMAL,
            true
        );
        $application->run($input, $output);
    
        $converter = new AnsiToHtmlConverter();
        $content = $output->fetch();
    
        return new Response($converter->convert($content));
    }
    
    /**
     * @param $token
     * @param $content
     */
    public function checkToken($token, $content)
    {
        $key = $this->container->getParameter('api_multisite_token');
        
        $encodeToken = EncryptorAesEcb::encode($content, $key);
        
        if ($token !== $encodeToken) {
            throw new NotFoundHttpException("Page not found.");
        }
    }
    
}
