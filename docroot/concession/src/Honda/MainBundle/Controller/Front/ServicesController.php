<?php

namespace Honda\MainBundle\Controller\Front;

use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;
use Honda\MainBundle\Model\Front\ServiceManager;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Symfony\Component\Routing\Annotation\Route;


class ServicesController extends AbstractFrontController
{
    /**
     * @Route("/services", name="services")
     */
    public function indexAction()
    {
        $endpoints = $this->getParameter('endpoints.others');
        $gateway = $this->get(GenericGateway::class);

        $pageInfos = $gateway->getData($endpoints['textGeneric'] . '/services');

        $title = $pageInfos->title ?? 'Nos Services';
        $description = $pageInfos->text ?? null;

        $breadcrumbs = [
            ['name' => $title, 'url' => $this->generateUrl('services'), 'last' => true],
        ];

        $services = $this->get(ServiceManager::class)->getServices($this->getDistributor());

        return $this->render('@HondaMain/Front/Services/index.html.twig', [
            'title' => $title,
            'description' => $description,
            'services' => $services,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }
}
