<?php

namespace Honda\MainBundle\Controller\Front;

use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;
use Honda\MainBundle\Entity\News;
use Honda\MainBundle\Model\Front\NewsManager;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends AbstractFrontController
{
    /**
     * @Route("/actualites", name="news")
     */
    public function listAction(Request $request)
    {
        $endpoints = $this->getParameter('endpoints.others');
        $gateway = $this->get(GenericGateway::class);

        $pageInfos = $gateway->getData($endpoints['textGeneric'] . '/actualites');

        $title = $pageInfos->title ?? 'Actualités';
        $description = $pageInfos->text ?? null;

        $breadcrumbs = [
            ['name' => $title, 'url' => $this->generateUrl('news'), 'last' => true],
        ];

        $newsManager = $this->get(NewsManager::class);

        $newsCategories = $newsManager->getNewsCategories();

        $res = $newsManager->getNewsForPage($this->getDistributor(), null, false);

        $newsCollection = $res['newsList'];
        $counter = $res['counter'];
        $article = null;

        if($counter > 0) {
            $article = array_shift($newsCollection);
        }

        $counter--;

        return $this->render('@HondaMain/Front/News/list.html.twig', [
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'mainClassBgColor' => 'bg-light',
            'newsCollection' => $newsCollection,
            'newsCategories' => $newsCategories,
            'dispoCats' => $res['dispoCats'],
            'counter' => $counter,
            'article' => $article,
        ]);
    }

    /**
     * @Route("/actualites/{slug}", name="news_show_article", requirements={"slug": "[a-zA-Z0-9\-]+"})
     */
    public function articleAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository(News::class)->getOneByDistributorAndSlug($this->getDistributor(), $slug);

        if (!$news) {
            throw $this->createNotFoundException();
        }

        $newsManager = $this->get(NewsManager::class);
        $distributor = $this->getDistributor();
        $newsCollection = $newsManager->getNewsHp($distributor);
        $newsCategories = $newsManager->getNewsCategories();

        $breadcrumbs = [
            ['name' => 'Actualités', 'url' => $this->generateUrl('news')],
            ['name' => $news->getTitle(), 'url' => $this->generateUrl('news', ['slug' => $news->getSlug()]), 'last' => true],
        ];

        return $this->render('@HondaMain/Front/News/article.html.twig', [
            'breadcrumbs' => $breadcrumbs,
            'article' => $news,
            'newsCollection' => $newsCollection,
            'newsCategories' => $newsCategories,
            'origin' => NewsManager::POST_ORIGIN_CONCESSION
        ]);
    }

    /**
     * @Route("/actualites/portail/{slug}", name="news_show_portail_article", requirements={"slug": "[a-zA-Z0-9\-]+"})
     */
    public function articlePortailAction(Request $request, $slug)
    {
        $newsManager = $this->get(NewsManager::class);
        $news = $newsManager->getRemoteNews($slug);

        if (empty($news)) {
            throw $this->createNotFoundException();
        }

        $breadcrumbs = [
            ['name' => 'Actualités', 'url' => $this->generateUrl('news')],
            ['name' => $news['title'], 'url' => $this->generateUrl('news', ['slug' => $news['slug']]), 'last' => true],
        ];

        $distributor = $this->getDistributor();
        $newsCollection = $newsManager->getNewsHp($distributor);
        $newsCategories = $newsManager->getNewsCategories();

        return $this->render('@HondaMain/Front/News/article.html.twig', [
            'breadcrumbs' => $breadcrumbs,
            'article' => $news,
            'slug' => $slug,
            'newsCollection' => $newsCollection,
            'newsCategories' => $newsCategories,
            'origin' => NewsManager::POST_ORIGIN_PORTAIL
        ]);
    }

    /**
     * @Route("/ajax/news-events.json", name="ajax_news_filters")
     */
    public function ajaxSwiperAction(Request $request)
    {
        $type = $request->query->get('select');
        $distributor = $this->getDistributor();
        $homepageNews = $this->get(NewsManager::class)->getNewsHp($distributor, $type);

        $content = $this->get('twig')->render('@HondaMain/Front/Block/_block_new_events_swiper.html.twig', [
                'homepageNews' => $homepageNews,
            ]
        );

        $content = '<div class="carousel-container">'.$content.'</div>';

        return $this->json([
            'html' => $content,
        ]);
    }

    /**
     * @Route("/ajax/article.json", name="ajax_news_list_filters")
     */
    public function ajaxListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $type = $request->query->get('select');
        $distributor = $this->getDistributor();

        $newsManager = $this->get(NewsManager::class);

        $newsCategories = $newsManager->getNewsCategories();

        $em = $this->getDoctrine()->getManager();

        $res = $newsManager->getNewsForPage($this->getDistributor(), $type, false);

        $newsCollection = $res['newsList'];
        $counter = $res['counter'];

        $article = null;

        if($counter > 0) {
            $article = array_pop($newsCollection);
        }

        $counter--;

        $content = $this->get('twig')->render('@HondaMain/Front/Block/block_list_article.html.twig', [
                'newsCollection' => $newsCollection,
                'counter' => $counter,
                'article' => $article,
            ]
        );

        return $this->json([
            'html' => $content,
        ]);
    }
}
