<?php

namespace Honda\MainBundle\Controller\Front\Base;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\SocialNetworkLink;
use Honda\MainBundle\Entity\SubModelTest;
use Honda\MainBundle\Model\Admin\HostManager;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class AbstractFrontController
 * @package Honda\MainBundle\Controller\Front\Base
 */
abstract class AbstractFrontController extends Controller
{
    /**
     *
     */
    CONST ACTIVE_DOMAIN_KEY = 'distributor';
    CONST PORTAIL_SOCIAL_LINKS = 'PORTAIL_SOCIAL_LINKS';
    CONST SOCIAL_LINKS = 'socialNetworkLinks';
    CONST APPLICATIONS_LINKS = 'APPLICATIONS_LINKS';
    CONST COOKIES_URL = 'COOKIES_URL';
    
    
    const PROVENANCE = [
        'DEFAULT' => 'DEFAULT',
        'VN' => 'VN',
        'VN_HORS_STOCK' => 'VN_HORS_STOCK',
        'VN_STICKY' => 'VN_STICKY',
        'VN_STOCK' => 'VN_STOCK',
        'VO' => 'VO',
        'VO_LIST' => 'VO_LIST',
    ];
    
    /**
     * initAction
     */
    public function initAction()
    {

        $domain = $this->get(HostManager::class)->getDomain();
        $request = $this->get('request_stack')->getCurrentRequest();
        
        $em = $this->getDoctrine()->getManager();
        $distributor = $em->getRepository(Distributor::class)->getDistributorByDomain($domain);
        
        $request->attributes->set(self::COOKIES_URL, $this->generateUrl('page_show', ['path' => 'vie-privee-et-cookies'], UrlGeneratorInterface::ABSOLUTE_URL));
        
        if ($distributor) {
    
            if (!$distributor->getActive()) {
    
                $isAuth = false;
                $securityContext = $this->container->get('security.authorization_checker');
                if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY') && $securityContext->isGranted('ROLE_DISTRIBUTOR')) {
                    $isAuth = true;
                }
                
                if (!$isAuth) {
                    throw new ServiceUnavailableHttpException();
                }

            }
            
            $socialLinks = $em->getRepository(SocialNetworkLink::class)->getAllByDistributor($distributor);
            $request->attributes->set('socialNetworkLinks', $socialLinks);
            
            if (!$socialLinks || ($socialLinks && !$socialLinks->hasAllLinks())) {
                $portailSocialNetworkLinks = $this->get(GenericGateway::class)->getData($this->getParameter('endpoints.others')['socialNetworkLinks'], true);
                $request->attributes->set(self::PORTAIL_SOCIAL_LINKS, $portailSocialNetworkLinks);
            }
            
            $applicationsLinks = $this->get(GenericGateway::class)->getData($this->getParameter('endpoints.others')['applicationsLinks'], true);
            
            $request->attributes->set(self::ACTIVE_DOMAIN_KEY, $distributor);
            $request->attributes->set(self::SOCIAL_LINKS, $socialLinks);
            $request->attributes->set(self::APPLICATIONS_LINKS, $applicationsLinks);
            
        } else {
            $content = $this->get('twig')->render('HondaPageBundle:Exception:error.html.twig');

            echo $content;
            exit();
            // throw $this->createNotFoundException('Concessionaire non trouvé.');
        }
    }
    
    /**
     * @return mixed
     */
    public function getDistributor()
    {
        
        $request = $this->get('request_stack')->getCurrentRequest();
        
        $distributor = $request->attributes->get(self::ACTIVE_DOMAIN_KEY);
    
        if ($distributor instanceof Distributor) {
            return $distributor;
        }
    
        throw $this->createNotFoundException('Concessionaire non trouvé.');
    }
    
}
