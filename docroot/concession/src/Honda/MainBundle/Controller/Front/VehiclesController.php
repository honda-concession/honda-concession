<?php

namespace Honda\MainBundle\Controller\Front;

use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;
use Honda\MainBundle\Entity\Inventory;
use Honda\MainBundle\Entity\SubModelTest;
use Honda\MainBundle\Model\Middleware\CategoryGateway;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * @Route("/gamme-et-tarifs")
 */
class VehiclesController extends AbstractFrontController
{
    /**
     * @Route("/{categorySlug}", name="new_vehicle_list", requirements={"categorySlug": "[a-zA-Z0-9\-_]+|null"}, defaults={"categorySlug": null})
     */
    public function listAction($categorySlug = null)
    {
        $endpoints = $this->getParameter('endpoints.others');
        $gateway = $this->get(GenericGateway::class);
        $categoriesChoices = $gateway->getData($endpoints['menus']);
        $gammeTarifs = $gateway->getData($endpoints['gammeTarifs'] . '/' . $categorySlug);
        $title = $gammeTarifs->page_title ?? 'Gamme et tarifs';
        
        $categoriesSlugToName = [];
        foreach ($categoriesChoices as $categoriesChoice) {
            $categoriesSlugToName[$categoriesChoice->category_slug] = $categoriesChoice->category;
        }
        
        $breadcrumbs = [
            ['name' => $title, 'url' => $this->generateUrl('new_vehicle_list'), 'last' => true],
        ];
        
        if ($categorySlug) {
            $breadcrumbs = [
                ['name' => $title, 'url' => $this->generateUrl('new_vehicle_list')],
            ];
            
            if (isset($categoriesSlugToName[$categorySlug]) && $categoriesSlugToName[$categorySlug]) {
                $last = ['name' =>  $categoriesSlugToName[$categorySlug], 'url' => $this->generateUrl('new_vehicle_list', ['category' => $categorySlug]), 'last' => true];
                array_push($breadcrumbs, $last);
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $subModelTestKeys = $em->getRepository(SubModelTest::class)->getSubModelTestAllKeys($this->getDistributor());
        $inventoryKeys = $em->getRepository(Inventory::class)->getInventoryAllKeys($this->getDistributor());
        
        return $this->render('@HondaMain/Front/Vehicles/list.html.twig', [
            'title' => $title,
            'breadcrumbs' => $breadcrumbs,
            'gammeTarifs' => $gammeTarifs,
            'categoriesChoices' => $categoriesChoices,
            'subModelTestKeys' => $subModelTestKeys,
            'inventoryKeys' => $inventoryKeys,
            'provenance' => parent::PROVENANCE['VN'],
            'categoriesSlugToName' => $categoriesSlugToName,
        ]);
    }
    
    /**
     * @Route("/{categorySlug}/{modelSlug}/{vehicleSlug}", name="new_vehicle_show", requirements={"categorySlug": "[a-zA-Z0-9\-_]+", "modelSlug": "[a-zA-Z0-9\-_]+", "vehicleSlug": "[a-zA-Z0-9\-]+|null"})
     * @param $categorySlug
     * @param $modelSlug
     * @param null $vehicleSlug
     * @return Response
     */
    public function showAction($categorySlug, $modelSlug, $vehicleSlug = null)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $modelEndpoint = $this->getParameter('endpoints.others')['ficheModel'] . $modelSlug;

        if ($vehicleSlug) {
            $modelEndpoint = $this->getParameter('endpoints.others')['ficheModel'] . $modelSlug .'/'. $vehicleSlug;
        }

        $model = (array) $this->get(GenericGateway::class)->getData($modelEndpoint);
        $modelPresentation = (array) $this->get(GenericGateway::class)->getData($this->getParameter('endpoints.others')['ficheModelPresentation'] . $modelSlug);
        $properties = [];

        if (isset($modelPresentation['presentation']) && $modelPresentation['presentation'] && $modelPresentation['presentation']->submodels) {
            $properties = current($modelPresentation['presentation']->submodels)->properties;
        }

        $em = $this->getDoctrine()->getManager();

        if (!$model) {
            throw $this->createNotFoundException();
        }

        $breadcrumbs = [
            ['name' => 'Gamme et tarifs', 'url' => $this->generateUrl('new_vehicle_list')],
            ['name' => $propertyAccessor->getValue($model, '[model_name]'), 'url' => '', 'last' => true],
        ];

        $inventoryKeys = $em->getRepository(Inventory::class)->getInventoryKeys($this->getDistributor());
        $subModelTestKeys = $em->getRepository(SubModelTest::class)->getSubModelTestAllKeys($this->getDistributor());

        return $this->render('@HondaMain/Front/Vehicles/show.html.twig', [
            'breadcrumbs' => $breadcrumbs,
            'model' => $model,
            'modelPresentation' => $modelPresentation,
            'submodels' => (array) $modelPresentation['presentation']->submodels,
            'inventoryKeys' => $inventoryKeys,
            'subModelTestKeys' => $subModelTestKeys,
            'provenance' => parent::PROVENANCE['VN'],
            'properties' => $properties,
            'provenance' => parent::PROVENANCE['VN'],
        ]);
    }

    /**
     * @Route("/ajax/vehicle-{vehicleId}.json", name="ajax_product", requirements={"vehicleId": "\d+"})
     */
    public function ajaxProductAction($vehicleId)
    {
        $em = $this->getDoctrine()->getManager();
        $vehicle = (array) $this->get(GenericGateway::class)->getData($this->getParameter('endpoints.others')['ficheVehicle'] . $vehicleId);
        $inventoryKeys = $em->getRepository(Inventory::class)->getInventoryKeys($this->getDistributor());
        
        if (count($vehicle) <= 0) {
            throw $this->createNotFoundException();
        }
        
        $provenance = isset($inventoryKeys[$vehicleId]) ? self::PROVENANCE['VN_STOCK'] : self::PROVENANCE['VN_HORS_STOCK'];
        
        $html = $this->renderView('@HondaMain/Front/Vehicles/_product.html.twig', [
                'vehicle' => $vehicle,
                'inventoryKeys' => $inventoryKeys,
                'provenance' => $provenance,
            ]
        );

        return $this->json(['html' => $html]);
    }

    /**
     * @Route("/ajax/modele-selectionne/{subModelId}.json", name="ajax_modele_selectionne", requirements={"subModelId": "\d+"})
     */
    public function ajaxModeleSelectionnerAction($subModelId)
    {
        $endpoint = str_replace(':subModelId', $subModelId, $this->getParameter('endpoints.others')['modeleSelectione']);
        $subModel =  $this->get(GenericGateway::class)->getData($endpoint, true);

        return $this->json($subModel);
    }

    /**
     * @Route("/ajax/moto-selectionnee/{vehicleId}.json", name="ajax_moto_selectionnee", requirements={"vehicleId": "\d+"})
     */
    public function ajaxMotoSelectionneeAction($vehicleId)
    {
        $endpoint = str_replace(':vehicleId', $vehicleId, $this->getParameter('endpoints.others')['motoSelectionee']);
        
        $subModel =  $this->get(GenericGateway::class)->getData($endpoint, true);
        
        
        return $this->json($subModel);
    }
}
