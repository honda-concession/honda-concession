<?php

namespace Honda\MainBundle\Controller\Front;

use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;
use Honda\MainBundle\Model\Front\AlaUneManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ALaUneController extends AbstractFrontController
{
    /**
     * @Route("/ajax/top-news.json", name="ajax_a_la_une_filters")
     */
    public function ajaxSwiperAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $type = $request->query->get('select');
        $distributor = $this->getDistributor();
        $alaUneManager = $this->get(AlaUneManager::class);
        $aLaUnes = $alaUneManager->getAlaUnes($distributor, $type);
        $content = $this->get('twig')->render('@HondaMain/Front/Block/_block_a_la_une_swiper.html.twig', [
            'aLaUnes' => $aLaUnes,
        ]);
        $content = '<div class="carousel-container">'.$content.'</div>';

        return $this->json([
            'html' => $content,
        ]);
    }
}
