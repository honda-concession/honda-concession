<?php

namespace Honda\MainBundle\Controller\Front;

use Symfony\Component\Routing\Annotation\Route;
use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;
use Honda\MainBundle\Model\Front\AccessoryManager;
use Honda\MainBundle\Entity\Accessory;
use Honda\MainBundle\Model\Middleware\PortailWrapper;
use Honda\MainBundle\Model\Middleware\GenericGateway;

class AccessoryController extends AbstractFrontController
{
    /**
     * @Route("/accessoires", name="accessoiries")
     */
    public function indexAction()
    {
        $endpoints = $this->getParameter('endpoints.others');
        $gateway = $this->get(GenericGateway::class);

        $pageInfos = $gateway->getData($endpoints['textGeneric'] . '/accessoires');

        $title = $pageInfos->title ?? 'Accessoires';
        $description = $pageInfos->text ?? null;

        $breadcrumbs = [
            ['name' => $title, 'url' => $this->generateUrl('accessoiries'), 'last' => true],
        ];

        $motoAccessory = $this->get(AccessoryManager::class)
            ->getAccessoryByDistributorAndAccessoryBlock($this->getDistributor(), Accessory::MOTO_ACCESSORY_BLOCK);

        $portailWrapper = $this->get(PortailWrapper::class);
        $catalogLink = $portailWrapper->getCatalogLinkGateway()->getCatalogLink(Accessory::MOTO_ACCESSORY_ENDPOINT);

        if($motoAccessory) {
            $motoAccessory->setLink($catalogLink->link ?? null);
            $motoAccessory->setLinkLabel($catalogLink->label ?? null);
        }

        $clothingAccessory = $this->get(AccessoryManager::class)
            ->getAccessoryByDistributorAndAccessoryBlock($this->getDistributor(), Accessory::VETEMENT_ACCESSORY_BLOCK);

        $catalogLink = $portailWrapper->getCatalogLinkGateway()->getCatalogLink(Accessory::VETEMENT_ACCESSORY_ENDPOINT);

        if($clothingAccessory) {
            $clothingAccessory->setLink($catalogLink->link ?? null);
            $clothingAccessory->setLinkLabel($catalogLink->label ?? null);
        }

        $equipmentAccessory = $this->get(AccessoryManager::class)
            ->getAccessoryByDistributorAndAccessoryBlock($this->getDistributor(), Accessory::EQUIPMENT_ACCESSORY_BLOCK);

        $catalogLink = $portailWrapper->getCatalogLinkGateway()->getCatalogLink(Accessory::EQUIPMENT_ACCESSORY_ENDPOINT);

        if($equipmentAccessory) {
            $equipmentAccessory->setLink($catalogLink->link ?? null);
            $equipmentAccessory->setLinkLabel($catalogLink->label ?? null);
        }

        $logoAccessory = $this->get(AccessoryManager::class)
            ->getAccessoryByDistributorAndAccessoryBlock($this->getDistributor(), Accessory::LOGO_ACCESSORY_BLOCK);

        return $this->render('@HondaMain/Front/Accessory/index.html.twig', [
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'motoAccessory' => $motoAccessory,
            'equipmentAccessory' => $equipmentAccessory,
            'clothingAccessory' => $clothingAccessory,
            'logoAccessory' => $logoAccessory,
        ]);
    }
}
