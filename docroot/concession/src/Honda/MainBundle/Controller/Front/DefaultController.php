<?php

namespace Honda\MainBundle\Controller\Front;

use Honda\MainBundle\Entity\VehicleAd;
use Honda\MainBundle\Model\Admin\SectionManager;
use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;
use Honda\MainBundle\Model\Front\AlaUneManager;
use Honda\MainBundle\Model\Front\NewsManager;
use Honda\MainBundle\Model\Front\ServiceManager;
use Honda\MainBundle\Model\Front\SiteMapBuilder;
use Honda\MainBundle\Model\Front\SliderManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractFrontController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $distributor = $this->getDistributor();

        $sectionOrders = $this->get(SectionManager::class)->getSectionOrderRepository()->getAllByDistributor($distributor);

        $vehiclesAds = $em->getRepository(VehicleAd::class)->getLastVehicleAdsByDistributor($distributor);

        $alaUneManager = $this->get(AlaUneManager::class);
        $aLaUnes = $alaUneManager->getAlaUnes($distributor, null, true);
        $aLaUnesCategories = $alaUneManager->getAlaUneCategories();

        $services = $this->get(ServiceManager::class)->getServices($distributor, true);

        $newsManager = $this->get(NewsManager::class);
        $homepageNews =  $newsManager->getNewsHp($distributor);
        $newsCategories = $newsManager->getNewsCategories();

        return $this->render('@HondaMain/Front/Default/homepage.html.twig', [
                'sectionOrders' => $sectionOrders,
                'vehiclesAds' => $vehiclesAds,
                'aLaUnes' => $aLaUnes,
                'aLaUnesCategories' => $aLaUnesCategories,
                'services' => $services,
                'homepageNews' => $homepageNews,
                'newsCategories' => $newsCategories
            ]
        );
    }

    /**
     * @Route("/hero-module", name="hero_module")
     */
    public function heroAction()
    {
        $distributor = $this->getDistributor();

        $sliders = $this->get(SliderManager::class)->getSliders($distributor);

        return $this->render('@HondaMain/Front/Default/hero_module.html.twig', ['sliders' => $sliders]);
    }

    /**
     * @Route("/ajax-vuesjs", name="ajax_vuesjs")
     */
    public function ajaxUpdateVuejsAction()
    {
        return $this->render('@HondaMain/Front/_vuejs_component.html.twig');
    }

    /**
     * @Route("/preview/logout", name="preview_logout")
     */
    public function logoutAction()
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }

    /**
     * @Route("/robots.txt", name="robots_txt")
     */
    public function robotsTxtAction()
    {
        $content = $this->get('twig')->render('@HondaMain/Front/Default/robots.txt.twig');

        $response = new Response($content , 200);
        $response->headers->set('Content-Type', 'text/plain');

        return $response;
    }

    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function siteMapAction()
    {
        $siteMapBuilder = $this->get(SiteMapBuilder::class);
        $content = $this->renderView('@HondaMain/Front/Default/sitemap.xml.twig', ['urls' => $siteMapBuilder->run($this->getDistributor())]);

        $response = new Response($content, 200);
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
