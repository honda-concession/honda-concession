<?php

namespace Honda\MainBundle\Controller\Front;

use Doctrine\Common\Collections\ArrayCollection;
use Honda\MainBundle\Controller\Front\Base\AbstractFrontController;
use Honda\MainBundle\Entity\VehicleAd;
use Honda\MainBundle\Model\Front\VehicleAdManager;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Honda\MainBundle\Model\Front\VoSearchRequest;

class VehicleAdController extends AbstractFrontController
{
    /**
     * @Route("/occasions", name="vehicule_occasion")
     * @Route("/ajax/retailers.json", name="vehicule_occasion_ajax")
     */
    public function listAction(Request $request)
    {
        $path = $request->getPathInfo();
    
        $voSearchRequest = new VoSearchRequest($request);
        $voSearchRequest->setOrders([
            'position' => 'DESC',
            'createdAt' => 'DESC'
        ]);

        $endpoints = $this->getParameter('endpoints.others');
        $gateway = $this->get(GenericGateway::class);

        $pageInfos = $gateway->getData($endpoints['textGeneric'] . '/vehicles-occasion');
        $banner = $gateway->getData($endpoints['bannerVo'], true);
        
        $distributor = $this->getDistributor();

        $title = 'NOS VÉHICULES D\'OCCASION';
        
        if ($distributor->getInfos()) {
            $infos = (array) $distributor->getInfos();
            if (isset($infos['name']) && $infos['name']) {
                $title = sprintf('Nos offres de motos d’occasion %s - %s', $infos['name'], $distributor->getTown());
            }
        }
        
        if ($pageInfos->title) {
            $title = $pageInfos->title;
        }
        
        $description = $pageInfos->text ?? null;
        
        $breadcrumbs = [
            ['name' => $title, 'url' => $this->generateUrl('vehicule_occasion'), 'last' => true],
        ];
    
        $bigBoxVehiclesAdCollection = new ArrayCollection();
        $vehiclesAdCollection = $this->get(VehicleAdManager::class)->getAllByDistributor($this->getDistributor(), $voSearchRequest);
        $collectionCounter = $vehiclesAdCollection->count();
        
        $vehiclesAd = $vehiclesAdCollection->first();
        $vehiclesAdCollection->removeElement($vehiclesAd);
        $counter = $vehiclesAdCollection->count();
        
        if ($counter) {
            $limit = $counter < 4 ? $counter : 4;
            $bigBoxVehiclesAdCollection = $vehiclesAdCollection->slice(0, $limit);
            
            foreach ($bigBoxVehiclesAdCollection as $bigBoxVehiclesAdItem) {
                if ($vehiclesAdCollection->contains($bigBoxVehiclesAdItem)) {
                    $vehiclesAdCollection->removeElement($bigBoxVehiclesAdItem);
                }
            }
        }
        
        $portailCategories = $this->get(GenericGateway::class)->getData($this->getParameter('endpoints.category')['list']);
        $portailCategories = $this->getDoctrine()->getRepository(VehicleAd::class)->getUsedVehicleAdCategories($this->getDistributor(), $portailCategories);
        
        if($path == "/ajax/retailers.json") {
            
            $content = $this->get('twig')->render('@HondaMain/Front/Block/block_list_vehicleAd.html.twig', [
                    'vehiclesAdCollection' => $vehiclesAdCollection,
                    'bigBoxVehiclesAdCollection' => $bigBoxVehiclesAdCollection,
                    'vehiclesAd' => $vehiclesAd,
                    'collectionCounter' => $collectionCounter,
                    'distributor' => $this->getDistributor(),
                ]
            );
            return new JsonResponse([
                'html' => $content,
            ]);
        }
        
        return $this->render('@HondaMain/Front/VehicleAd/list.html.twig', [
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'vehiclesAdCollection' => $vehiclesAdCollection,
            'bigBoxVehiclesAdCollection' => $bigBoxVehiclesAdCollection,
            'vehiclesAd' => $vehiclesAd,
            'collectionCounter' => $collectionCounter,
            'banner' => $banner,
            'provenance' => parent::PROVENANCE['VO_LIST'],
            'portailCategories' => $portailCategories,
        ]);
    }
    
    /**
     * @Route("/occasion/{slug}", name="vehicule_occasion_voir")
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $distributor = $this->getDistributor();
        
        $vehicleAd = $em->getRepository(VehicleAd::class)->getVehicleAdByDistributorAndSlug($distributor, $slug);

        if (!$vehicleAd) {
            return $this->createNotFoundException();
        }
        
        $breadcrumbs = [
            ['name' => 'Occasions', 'url' => $this->generateUrl('vehicule_occasion'),],
            ['name' => $vehicleAd->getTitle(), 'url' => $this->generateUrl('vehicule_occasion_voir', ['slug' => $slug]), 'last' => true],
        ];
    
        list($prev, $next) = $this->get(VehicleAdManager::class)->getNextAndPrevVehicleAds($distributor, $vehicleAd);
        
        return $this->render('@HondaMain/Front/VehicleAd/show.html.twig', [
                'vehicleAd' => $vehicleAd,
                'next' => $next,
                'prev' => $prev,
                'breadcrumbs' => $breadcrumbs,
                'provenance' => parent::PROVENANCE['VO'],
            ]
        );
    }
}
