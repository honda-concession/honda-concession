<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Model\Admin\SocialNetworkLinkManager;
use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Form\Admin\SocialNetworkLinkType;
use Honda\MainBundle\Entity\SocialNetworkLink;
use Honda\MainBundle\Model\Middleware\GenericGateway;


/**
 * Class SocialNetworkLinkCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class SocialNetworkLinkCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();
    
        $em = $this->getDoctrine()->getManager();
        
        $socialNetworkLink = $em->getRepository(SocialNetworkLink::class)->findOneBy(['distributor' => $this->getDistributor()]);
    
        if (!$socialNetworkLink) {
            $socialNetworkLink =  new SocialNetworkLink();
            $socialNetworkLink->setDistributor($this->getDistributor());
        }
        
        $form = $this->createForm(SocialNetworkLinkType::class, $socialNetworkLink);
        
        if ($this->get(SocialNetworkLinkManager::class)->handlerForm($request, $form)) {
            $this->addFlash('sonata_flash_success', 'Les infos des reseaux sociaux ont été correctement mis à jour.');
        }
    
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_social_network.html.twig', [
                'form' => $form->createView()
            ]
        );
    }
}
