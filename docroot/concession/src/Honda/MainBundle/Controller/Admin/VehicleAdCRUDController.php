<?php

namespace Honda\MainBundle\Controller\Admin;

use Pix\SortableBehaviorBundle\Controller\SortableAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormRenderer;

use Honda\MainBundle\Entity\VehicleAdGallery;

/**
 * Class VehicleAdCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class VehicleAdCRUDController extends SortableAdminController
{

    use Base\Traits\PublishableTrait;

    const IMAGES_LIMIT = 6;

    /**
     * @inheritdoc
     */
    protected function preCreate(Request $request, $object)
    {
        // Limit nombre d'image à 6 lors de la creation
        for ($i = 1; $i <= self::IMAGES_LIMIT; $i++) {
            $object->addVehicleAdGallery(new VehicleAdGallery());
        }
    }

    /**
     * @inheritdoc
     */
    protected function preEdit(Request $request, $object)
    {
        $diff = (int) (self::IMAGES_LIMIT - $object->getVehicleAdGalleries()->count());
        if ($diff) {
            for ($i = 1; $i <= $diff; $i++) {
                $object->addVehicleAdGallery(new VehicleAdGallery());
            }
        }
    }
}
