<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Model\Admin\PaypalManager;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Honda\MainBundle\Model\Admin\SubscriptionManager;
use PayPal\Api\Payment;

/**
 * Class PaymentCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class PaymentCRUDController extends CRUDController
{
    
    
    /**
     * @return RedirectResponse
     * @throws \Exception
     */
    public function paymentAction()
    {
        $paypalManager = $this->get(PaypalManager::class);
        $distributor = $this->getUser()->getDistributor();
    
        $subscriptionManager = $this->get(SubscriptionManager::class);
        $subscription = $subscriptionManager->createSubscription($distributor);
        
        $payment = $paypalManager->createPayment($distributor, $subscription);
        //dump($payment); exit;
        if ($payment instanceof Payment) {
            return new RedirectResponse($payment->getApprovalLink());
        } else {
            $this->addFlash('sonata_flash_error', 'Une erreur est survenue');
            return $this->redirectToRoute('subscription_list');
        }
    }
    
    /**
     * @return RedirectResponse
     */
    public function returnAction()
    {
        $paypalManager = $this->get(PaypalManager::class);
        
        if ($paypalManager->executePayment($this->getRequest())) {
            $this->addFlash('sonata_flash_success', 'Paiement reussie');
        } else {
            $this->addFlash('sonata_flash_error', 'Paiement echoué');
        }
        
        return $this->redirectToRoute('subscription_list');
    }
    
    
    /**
     * @return RedirectResponse
     */
    public function cancelAction()
    {
        $paypalManager = $this->get(PaypalManager::class);
        
        $paypalManager->executeCancel();
        $this->addFlash('sonata_flash_error', 'Paiement echoué');

        return $this->redirectToRoute('subscription_list');
    }
}
