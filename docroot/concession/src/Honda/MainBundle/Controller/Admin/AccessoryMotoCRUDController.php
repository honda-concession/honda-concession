<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Entity\Accessory;
use Honda\MainBundle\Entity\AccessoryBlock;
use Honda\MainBundle\Form\Admin\AccessoryType;
use Honda\MainBundle\Model\Admin\AccessoryManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Honda\MainBundle\Model\Middleware\PortailWrapper;


/**
 * Class AccessoryMotoCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class AccessoryMotoCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $accessoryBlockRepository = $em->getRepository(AccessoryBlock::class);
        $accessoryBlock = $accessoryBlockRepository->findOneBy(['slug' => 'accessoires-motos']);
        
        if (!$accessoryBlock) {
            throw new NotFoundHttpException(sprintf('%s block is required', 'accessoires motos'));
        }
    
        $portailWrapper = $this->get(PortailWrapper::class);
        $catalogLink = $portailWrapper->getCatalogLinkGateway()->getCatalogLink(Accessory::MOTO_ACCESSORY_ENDPOINT);
        
        $accessory = $em->getRepository(Accessory::class)->findOneBy([
                'distributor' => $this->getDistributor(),
                'accessoryBlock' => $accessoryBlock,
            ]
        );

        if (!$accessory) {

            $accessory = new Accessory();
            $accessory->setTitle($accessoryBlock->getTitle());
            $accessory->setDistributor($this->getDistributor());
            $accessory->setAccessoryBlock($accessoryBlock);
        }

        $accessory->setLink($catalogLink->link ?? null);
        $accessory->setLinkLabel($catalogLink->label ?? null);

        $form = $this->createForm(AccessoryType::class, $accessory);

        if ($this->get(AccessoryManager::class)->handlerForm($this->getRequest(), $form)) {

            $this->addFlash('sonata_flash_success', 'Accessoire correctement enregistré');
        }

        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_accessory.html.twig', [
                'form' => $form->createView(),
                'accessory' => $accessory
            ]
        );
    }

}
