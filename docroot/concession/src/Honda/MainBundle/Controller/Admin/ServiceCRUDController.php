<?php

namespace Honda\MainBundle\Controller\Admin;

use Pix\SortableBehaviorBundle\Controller\SortableAdminController;
use Application\Sonata\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ServiceCRUDController extends SortableAdminController
{

    use Base\Traits\PublishableTrait;

    /**
     * @param $id
     * @return bool
     */
    protected function redirectionToList($id)
    {
        $request = $this->getRequest();
        $user = $this->getUser();
        $toRedirect = false;
    
        if ($user->hasRole(User::ROLE_DISTRIBUTOR)) {
        
            $id = $request->get($this->admin->getIdParameter());
            $existingObject = $this->admin->getObject($id);
        
            if ($existingObject && $existingObject->getIsDefaultService()) {
                $toRedirect = true;
            }
        }
        
        return $toRedirect;
    }

    /**
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id = null)
    {
        if ($this->redirectionToList($id)) {
            
            $this->addFlash('sonata_flash_error', 'Vous n\'êtes pas autorisé à modifier, les services par default' );
    
            return new RedirectResponse($this->get('router')->generate('admin_honda_main_service_list'));
        }
      
       
        return parent::editAction($id);
    }
    
    /**
     * @param int|null|string $id
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id)
    {
        if ($this->redirectionToList($id)) {
        
            $this->addFlash('sonata_flash_error', 'Vous n\'êtes pas autorisé à modifier, les services par default' );
        
            return new RedirectResponse($this->get('router')->generate('admin_honda_main_service_list'));
        }
    
    
        return parent::deleteAction($id);
    }
}
