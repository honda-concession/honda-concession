<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Form\Admin\GoogleAnalyticType;
use Honda\MainBundle\Model\Admin\GenericFormHandler;


/**
 * Class GoogleAnalyticController
 * @package Honda\MainBundle\Controller\Admin
 */
class GoogleAnalyticController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();
        
        $distributor = $this->getDistributor();
        
        $form = $this->createForm(GoogleAnalyticType::class, $distributor);
        
        if ($this->get(GenericFormHandler::class)->handlerForm($request, $form)) {
            $this->addFlash('sonata_flash_success', 'Le tag analytic a été correctement mis à jour.');
            
            return $this->redirectToRoute('google-analytic_list');
        }
        
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_google_analytic.html.twig', [
                'form' => $form->createView()
            ]
        );
    }
}
