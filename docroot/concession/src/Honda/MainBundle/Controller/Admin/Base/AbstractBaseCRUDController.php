<?php

namespace Honda\MainBundle\Controller\Admin\Base;

use Sonata\AdminBundle\Controller\CRUDController;


/**
 * Class AbstractBaseCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
abstract class AbstractBaseCRUDController extends CRUDController
{
    use Traits\DistributorTrait;
}
