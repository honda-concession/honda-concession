<?php

namespace Honda\MainBundle\Controller\Admin\Base;

use Pix\SortableBehaviorBundle\Controller\SortableAdminController;


/**
 * Class AbstractBaseSortableCRUDController
 * @package Honda\MainBundle\Controller\Admin\Base
 */
abstract class AbstractBaseSortableCRUDController extends SortableAdminController
{
    use Traits\DistributorTrait;
}
