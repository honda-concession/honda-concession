<?php

namespace Honda\MainBundle\Controller\Admin\Base\Traits;

use Pix\SortableBehaviorBundle\Controller\SortableAdminController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Trait PublishableTrait
 * @package Honda\MainBundle\Controller\Admin\Base\Traits
 */
trait PublishableTrait
{
    public function batchActionPublishOnHome(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $selectedModels = $selectedModelQuery->execute();
        $modelManager = $this->admin->getModelManager();
        foreach ($selectedModels as $selectedModel) {
            $selectedModel->setPublishedOnHome(true);
            $modelManager->update($selectedModel);
        }
        $this->addFlash('sonata_flash_success', 'Contenu publier sur la page d\'accueil');

        return new RedirectResponse(
            $this->admin->generateUrl('list', [
                'filter' => $this->admin->getFilterParameters()
            ])
        );
    }

    public function batchActionPublished(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $selectedModels = $selectedModelQuery->execute();
        $modelManager = $this->admin->getModelManager();
        foreach ($selectedModels as $selectedModel) {
            $selectedModel->setPublished(true);
            $modelManager->update($selectedModel);
        }
        $this->addFlash('sonata_flash_success', 'Contenu publier');

        return new RedirectResponse(
            $this->admin->generateUrl('list', [
                'filter' => $this->admin->getFilterParameters()
            ])
        );
    }

    public function batchActionUnpublished(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $selectedModels = $selectedModelQuery->execute();
        $modelManager = $this->admin->getModelManager();
        foreach ($selectedModels as $selectedModel) {
            $selectedModel->setPublished(false);
            $modelManager->update($selectedModel);
        }
        $this->addFlash('sonata_flash_success', 'Contenu dépublier');

        return new RedirectResponse(
            $this->admin->generateUrl('list', [
                'filter' => $this->admin->getFilterParameters()
            ])
        );
    }
}
