<?php

namespace Honda\MainBundle\Controller\Admin\Base\Traits;


/**
 * Trait DistributorTrait
 * @package Honda\MainBundle\Controller\Admin\Base\Traits
 */
trait DistributorTrait
{
    
    /**
     * @return mixed
     */
    public function getDistributor()
    {
        
        if ($distributor = $this->getUser()->getDistributor()) {
            return $distributor;
        }
        
        throw new \RuntimeException(sprintf('Distributor is required'));
    }
    
    /**
     * @return null|string
     */
    public function getDistributorDomain()
    {
        $request = $this->getRequest();
        
        $distributor = $this->getDistributor();
        
        if ($distributor->getDomain()) {
        
            return $request->getScheme() . '://' . $distributor->getDomain();
        }
        
        return null;
    }
    
    
}
