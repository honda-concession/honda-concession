<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;


/**
 * Class StatsCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class StatsCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_page_stats.html.twig', [
            'dataStudioId' => $this->getDistributor()->getDataStudioId()
        ]);
    }
}
