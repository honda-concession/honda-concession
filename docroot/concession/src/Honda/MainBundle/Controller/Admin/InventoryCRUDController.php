<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Entity\Inventory;
use Honda\MainBundle\Model\Admin\InventoryManager;
use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Model\Middleware\InventoryGateway;
use Symfony\Component\HttpFoundation\JsonResponse;
use Honda\MainBundle\Security\PermissionVoter;


/**
 * Class InventoryCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class InventoryCRUDController extends AbstractBaseCRUDController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
     public function listAction()
     {
        $request = $this->getRequest();
        $this->admin->checkAccess('list');
        if (!$request->query->has('filter')) {
            $vehiclesJson = $this->get(InventoryGateway::class)->getVehicles();
            $this->get(InventoryManager::class)->importNewVehicles($this->getDistributor(), $vehiclesJson);
        }
        $response = parent::listAction();

        return $response;
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function ajaxToggleInStockAction($id)
    {
        $request = $this->getRequest();
        
        $inventory = $this->getDoctrine()->getManager()->getRepository(Inventory::class)->findOr404($id);
        
        $this->denyAccessUnlessGranted(PermissionVoter::OWNER, $inventory);
        
        $this->get(InventoryManager::class)->saveInStock($inventory, $request);
    
        return new JsonResponse($inventory);
    }
    
}
