<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Form\Admin\BgColorType;
use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Model\Admin\GenericFormHandler;


/**
 * Class BgColorCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class BgColorCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $this->getUser();
        $request = $this->getRequest();
        
        $distributor = $this->getDistributor();
        
        $form = $this->createForm(BgColorType::class, $distributor);
        
        if ($this->get(GenericFormHandler::class)->handlerForm($request, $form)) {
            $this->addFlash('sonata_flash_success', 'La couleur a été correctement mis à jour.');
            
            return $this->redirectToRoute('bg-color_list');
        }
        
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_bg_color.html.twig', [
                'form' => $form->createView()
            ]
        );
    }
}
