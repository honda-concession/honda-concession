<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Entity\Accessory;
use Honda\MainBundle\Entity\AccessoryBlock;
use Honda\MainBundle\Form\Admin\AccessoryClotingType;
use Honda\MainBundle\Model\Admin\AccessoryManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Honda\MainBundle\Model\Middleware\PortailWrapper;


/**
 * Class AccessoryEquipementMotardCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class AccessoryEquipementMotardCRUDController extends AbstractBaseCRUDController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $accessoryBlockRepository = $em->getRepository(AccessoryBlock::class);
        $accessoryBlock = $accessoryBlockRepository->findOneBy(['slug' => 'equipement-du-motard']);

        if (!$accessoryBlock) {
            throw new NotFoundHttpException(sprintf('%s block is required', 'equipement-du-motard'));
        }

        $portailWrapper = $this->get(PortailWrapper::class);

        $clothingLogos = $portailWrapper->getAccessoriesLogosGateway()->getClothingLogos();
        $helmetLogos = $portailWrapper->getAccessoriesLogosGateway()->getHelmetLogos();
        $catalogLink = $portailWrapper->getCatalogLinkGateway()->getCatalogLink(Accessory::EQUIPMENT_ACCESSORY_ENDPOINT);

        $request->attributes->set('requestClothingLogos', $clothingLogos);
        $request->attributes->set('requestHelmetLogos', $helmetLogos);

        $accessory = $em->getRepository(Accessory::class)->findOneBy([
                'distributor' => $this->getDistributor(),
                'accessoryBlock' => $accessoryBlock,
            ]
        );

        if (!$accessory) {
            $accessory = new Accessory();
            $accessory->setTitle($accessoryBlock->getTitle());
            $accessory->setDistributor($this->getDistributor());
            $accessory->setAccessoryBlock($accessoryBlock);
        }

        $accessory->setLink($catalogLink->link ?? null);
        $accessory->setLinkLabel($catalogLink->label ?? null);

        $form = $this->createForm(AccessoryClotingType::class, $accessory);

        if ($this->get(AccessoryManager::class)->handlerForm($this->getRequest(), $form)) {

            $this->addFlash('sonata_flash_success', 'Accessoire correctement enregistré');
        }

        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_accessory_cloting.html.twig', [
                'form' => $form->createView(),
                'accessory' => $accessory,
                'clothingLogos' => $clothingLogos,
                'helmetLogos' => $helmetLogos,
            ]
        );
    }

}
