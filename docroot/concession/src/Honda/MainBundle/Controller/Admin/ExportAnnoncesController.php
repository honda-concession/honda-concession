<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Honda\MainBundle\Entity\VehicleAd;

/**
 * Class GoogleAnalyticController
 * @package Honda\MainBundle\Controller\Admin
 */
class ExportAnnoncesController extends AbstractBaseCRUDController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->renderWithExtraParams('@HondaMain/Admin/export_annonces.html.twig', []);
    }

    public function exportAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $annonces = $em->getRepository(VehicleAd::class)
                ->getVehicleAds();
        
        $dataHead = array(
            'Id',
            'Nom',
            'Marque',
            'Modele',
            utf8_decode('Année modèle'),
            'Prix',
            'Km',
            //'Description',
            'Garantie',
            //'Concessionnaire',
            'Concess Id',
            'Mise en circulation',
            'Cylindree',
            //'Collaborateur',
            utf8_decode('Catégorie'),
            //utf8_decode('Reférence'),
            //'Image',
            //'Lien Youtube',
            utf8_decode('Date création'),
            utf8_decode('Date de mise à jour')
        );


        foreach ($annonces as $annonce) {

            $data = array(
                $annonce->getId(),
                $annonce->getTitle(),
                $annonce->getMarkPortail(),
                $annonce->getModelPortail(),
                $annonce->getModelYear(),
                $annonce->getPrice(),
                $annonce->getKm(),
                //$annonce->getDescription(),
                $annonce->getWarrant(),
                $annonce->getDistributor()->getId(),
                $annonce->getReleaseDate()->format('d/m/Y'),
                $annonce->getCylinder(),
                utf8_decode($annonce->getCategoryPortail()),
                $annonce->getCreatedAt()->format('d/m/Y'),
                $annonce->getUpdatedAt()->format('d/m/Y'),
            );
            
            $rows[] = implode(',', $data);
        }

        $content = implode("\n", $rows);
        $response = new Response($content);
        $response->headers->set('Content-Type', 'text/csv');

        return $response;
    }

}
