<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Entity\SubModelTest;
use Honda\MainBundle\Model\Admin\SubModelTestManager;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Symfony\Component\HttpFoundation\JsonResponse;
use Honda\MainBundle\Security\PermissionVoter;
use Honda\MainBundle\Model\Middleware\VehicleModelGateway;

/**
 * Class SubModelTestCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class SubModelTestCRUDController extends AbstractBaseCRUDController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();
        $this->admin->checkAccess('list');
        if (!$request->query->has('filter')) {
            $json = $this->get(GenericGateway::class)->getData($this->getParameter('endpoints.vehicle')['submodels']);
            $this->get(SubModelTestManager::class)->importSubModels($this->getDistributor(), $json);
        }
        $response = parent::listAction();

        return $response;
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function ajaxToggleOnTestAction($id)
    {
        $request = $this->getRequest();
    
        $subModelTest = $this->getDoctrine()->getManager()->getRepository(SubModelTest::class)->findOr404($id);
        
        $this->denyAccessUnlessGranted(PermissionVoter::OWNER, $subModelTest);
        
        $this->get(SubModelTestManager::class)->saveOnTest($subModelTest, $request);
    
        return new JsonResponse($subModelTest);
    }
    
}
