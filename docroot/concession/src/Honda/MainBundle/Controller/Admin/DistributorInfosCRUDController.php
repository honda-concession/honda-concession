<?php

namespace Honda\MainBundle\Controller\Admin;

use Application\Sonata\MediaBundle\Entity\Media;
use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Event\UserEvent;
use Honda\MainBundle\Form\Admin\DistributorType;

/**
 * Class InventoryCRUDController
 * @package Honda\MainBundle\Controller\Adminpost
 */
class DistributorInfosCRUDController extends AbstractBaseCRUDController
{
    
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();
        $distributor = $this->getDistributor();

        if ($request->isMethod('GET')) {
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(UserEvent::ON_LOGIN_UPDATE_DISTRIBUTOR_INFOS, new UserEvent($this->getUser()));
        }
    
        $distributorInfos =  $this->getDistributor()->getInfos();

        $form = $this->createForm(DistributorType::class);
    
        $form->get('insertPicture')->setData($distributor->getInsertPicture());
        
        $did = $distributor ? $distributor->getDistributorPortailId() : null;

        $form->get('id')->setData($did);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $infos = $form->getData();

            $diff = array_diff_assoc($infos, (array)$distributorInfos);

            if (!empty($diff)) {
    
                $em = $this->getDoctrine()->getManager();
                
                $distributor->setTimeTable($infos['timetable']);
                $distributor->getSeoContent()->setDescriptionHome($infos['descriptionHome']);
                $distributor->setInsertPicture($infos['insertPicture']);
    
                $em->flush();
                
                $insertPictureUrl = null;
                if ($distributor->getInsertPicture() && $distributor->getInsertPicture() instanceof Media) {
                    $media = $distributor->getInsertPicture();
                    $provider = $this->get($distributor->getInsertPicture()->getProviderName());
                    $format = $provider->getFormatName($media, 'big');
                    $insertPictureUrl =  $this->getDistributorDomain() . $provider->generatePublicUrl($distributor->getInsertPicture(), $format);
                }
 
                $diff['insertPicture'] = $insertPictureUrl;

                $distributorGateway = $this->container->get('Honda\MainBundle\Model\Middleware\DistributorGateway');
                $distributorGateway->updateDistributorInfos($distributor, $diff);
    
                $this->addFlash('sonata_flash_success', 'Les informations ont été correctement mis à jour.');
                
                return $this->redirectToRoute('distributor-infos_list');
            }
        }
    
        // Setter pour le formulaire
        if ($distributorInfos) {
            $distributorInfos->timetable = $distributor->getTimeTable();
            $distributorInfos->descriptionHome = $distributor->getSeoContent()->getDescriptionHome();
        }
        
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_distributor_infos.html.twig', [
            'distributorInfos' => $distributorInfos,
            'distributor' => $distributor,
            'form' => $form->createView()
        ]);
    }
    
}
