<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;

use Honda\MainBundle\Entity\BillingAddress;
use Honda\MainBundle\Form\Admin\BillingAddressType;
use Honda\MainBundle\Model\Admin\GenericFormHandler;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class BillingAddressCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class BillingAddressCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $distributor = $this->getDistributor();
    
        $billingAddress = $em->getRepository(BillingAddress::class)->findOneBy(['distributor' => $distributor]);
    
        if (!$billingAddress) {
            $billingAddress = new BillingAddress();
            $billingAddress->setDistributor($distributor);
        }
    
        $form = $this->createForm(BillingAddressType::class, $billingAddress);
        
        if ($this->get(GenericFormHandler::class)->handlerForm($request, $form)) {
            $this->addFlash('sonata_flash_success', 'Le adresse de facturation a été correctement mis à jour.');
        
            return $this->redirectToRoute('billing-address_list');
        }
        
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_billing_address.html.twig', [
            'billingAddress' => $billingAddress,
            'form' =>  $form->createView(),
        ]);
    }
}
