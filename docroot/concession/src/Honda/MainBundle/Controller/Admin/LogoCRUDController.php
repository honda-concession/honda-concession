<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Form\Admin\LogoType;
use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Model\Admin\GenericFormHandler;


/**
 * Class LogoCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class LogoCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();
        
        $distributor = $this->getDistributor();
        
        $form = $this->createForm(LogoType::class, $distributor);
    
        if ($this->get(GenericFormHandler::class)->handlerForm($request, $form)) {
            $this->addFlash('sonata_flash_success', 'Le logo a été correctement mis à jour.');
            
            return $this->redirectTo('logo_list');
        }
        
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_logo.html.twig', [
                'form' => $form->createView()
            ]
        );
    }
}
