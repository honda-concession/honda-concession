<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;

use Honda\MainBundle\Entity\ServicePage;
use Honda\MainBundle\Form\Admin\ServicePageType;
use Honda\MainBundle\Model\Admin\GenericFormHandler;


class ServicePageCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();
        
        $em = $this->getDoctrine()->getManager();
        
        $servicePage = $em->getRepository(ServicePage::class)->findOneBy(['distributor' => $this->getDistributor()]);
        
        if (!$servicePage) {
            $servicePage =  new ServicePage();
            $servicePage->setDistributor($this->getDistributor());
        }
        
        $form = $this->createForm(ServicePageType::class, $servicePage);
    
        if ($this->get(GenericFormHandler::class)->handlerForm($request, $form)) {
            $this->addFlash('sonata_flash_success', 'Les infos de la page service ont été correctement mis à jour.');
        }
        
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_service_page.html.twig', [
                'form' => $form->createView()
            ]
        );
    }
    
}
