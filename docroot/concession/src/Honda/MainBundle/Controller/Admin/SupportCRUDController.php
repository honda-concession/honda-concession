<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Form\Admin\SupportType;
use Honda\MainBundle\Form\Admin\SupportMessageType;
use Honda\MainBundle\Http\PortalSiteGateway;


/**
 * Class SupportCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class SupportCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        $distributor = $this->getDistributor();
    
        $form = $this->createForm(SupportType::class);
    
        $form->get('distributor_portail_id')->setData($distributor->getDistributorPortailId());
    
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_page_support.html.twig', [
                'form' => $form->createView()
            ]
        );
    }
    
    public function listAction()
    {
        $client = $this->container->get(PortalSiteGateway::class);
        
        $distributor = $this->getDistributor();
        
        $tickets = $client->getList($distributor->getDistributorPortailId());
    
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_page_support_list.html.twig', [
                'tickets' => $tickets['ticket'],
                'ticketState' => $tickets['ticketState'],
                'ticketPriority' => $tickets['ticketPriority']
            ]
        );
    }
    
    public function editAction($ticketId = NULL)
    {
        $client = $this->container->get(PortalSiteGateway::class);
        
        $request = $this->getRequest();

        $ticketId = (int) $request->attributes->get('id');
        
        $distributor = $this->getDistributor();
        
        $form = $this->createForm(SupportMessageType::class);
    
        $form->get('distributor_portail_id')->setData($distributor->getDistributorPortailId());

        $ticket = $client->getMessage($ticketId, $distributor->getDistributorPortailId());

        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_page_support_show.html.twig', [
                'ticket' => $ticket['ticket'],
                'messages' => $ticket['messages'],
                'form' => $form->createView()
            ]
        );
    }
}
