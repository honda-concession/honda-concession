<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Form\Admin\DistributorSeoContentType;
use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Model\Admin\GenericFormHandler;


/**
 * Class SeoContentCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class SeoContentCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();
        
        $distributor = $this->getDistributor();
        
        $form = $this->createForm(DistributorSeoContentType::class, $distributor);
    
        if ($this->get(GenericFormHandler::class)->handlerForm($request, $form)) {
            $this->addFlash('sonata_flash_success', 'Le contenu a été correctement mis à jour.');
            
            return $this->redirectTo('seo_content_list');
        }
        
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_seo_content.html.twig', [
                'form' => $form->createView()
            ]
        );
    }
}
