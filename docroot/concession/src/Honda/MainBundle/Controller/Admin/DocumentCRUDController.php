<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;


/**
 * Class DocumentCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class DocumentCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_page.html.twig', []);
    }
}
