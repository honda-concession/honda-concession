<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Entity\BillingAddress;
use Honda\MainBundle\Model\Admin\SubscriptionManager;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\HttpFoundation\Response;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

/**
 * Class SubscriptionCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class SubscriptionCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function listAction()
    {
        $this->admin->checkAccess('list');

        $em = $this->getDoctrine()->getManager();
        $distributor = $this->getDistributor();

        //$billingAddress = $em->getRepository(BillingAddress::class)->findOneBy(['distributor' => $distributor]);

        $subscriptionManager = $this->get(SubscriptionManager::class);
        $subscriptionManager->update($this->getDistributor());
        //$currentSubscription = $subscriptionManager->getRepository()->getAllSubscriptionByDistributor($this->getDistributor());

        //$this->admin->billingAddress = $billingAddress;
        //$this->admin->currentSubscription = $currentSubscription;

        $response = parent::listAction();

        return $response;
    }

    /**
     * @param $id
     * @return Response
     */
    public function billAction($id)
    {
        $subscriptionManager = $this->get(SubscriptionManager::class);
        $subscription = $subscriptionManager->find($this->getDistributor(), $id);

        if (!$subscription) {
            throw new NotFoundHttpException();
        }

        $html = $this->renderView('@HondaMain/Admin/sonata_bill_pdf.html.twig', array('subscription' => $subscription));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            'facture.pdf'
        );
    }
}
