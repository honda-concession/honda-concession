<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Model\Admin\SectionManager;


class SectionCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $this->admin->checkAccess('list');
    
        $sectionOrders =  $this->get(SectionManager::class)->getSectionOrderRepository()->getAllByDistributor($this->getDistributor());
        
        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_section_page.html.twig', ['sectionOrders' => $sectionOrders]);
    }
    
    
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxOrderAction()
    {
        $request = $this->getRequest();
        
        $this->get(SectionManager::class)->handlerSortable($this->getDistributor(), $request);
        
        return $this->json(['status' => 200, 'content' => 'done']);
    }
    
}
