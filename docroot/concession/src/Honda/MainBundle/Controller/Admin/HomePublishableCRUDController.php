<?php

namespace Honda\MainBundle\Controller\Admin;

use Pix\SortableBehaviorBundle\Controller\SortableAdminController;

/**
 * Class HomePublishableCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class HomePublishableCRUDController extends SortableAdminController
{
    use Base\Traits\PublishableTrait;
}
