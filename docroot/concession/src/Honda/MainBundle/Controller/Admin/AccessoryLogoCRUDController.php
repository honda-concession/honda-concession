<?php

namespace Honda\MainBundle\Controller\Admin;

use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Honda\MainBundle\Entity\Accessory;
use Honda\MainBundle\Entity\AccessoryBlock;
use Honda\MainBundle\Form\Admin\AccessoryLogoType;
use Honda\MainBundle\Model\Admin\AccessoryManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Honda\MainBundle\Model\Middleware\PortailWrapper;


/**
 * Class AccessoryEquipementMotardCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class AccessoryLogoCRUDController extends AbstractBaseCRUDController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $accessoryManager = $this->get(AccessoryManager::class);
        $accessoryBlockRepository = $em->getRepository(AccessoryBlock::class);
        $accessoryBlock = $accessoryBlockRepository->findOneBy(['slug' => 'logos']);

        if (!$accessoryBlock) {
            throw new NotFoundHttpException(sprintf('%s block is required', 'logos'));
        }

        $portailWrapper = $this->get(PortailWrapper::class);
        $catalogLink = $portailWrapper->getCatalogLinkGateway()->getCatalogLink(Accessory::EQUIPMENT_ACCESSORY_ENDPOINT);

        $accessory = $em->getRepository(Accessory::class)->findOneBy([
                'distributor' => $this->getDistributor(),
                'accessoryBlock' => $accessoryBlock,
            ]
        );

        if (!$accessory) {
            $accessory = new Accessory();
            $accessory->setTitle($accessoryBlock->getTitle());
            $accessory->setDistributor($this->getDistributor());
            $accessory->setDescription('logo accessory');
            $accessory->setAccessoryBlock($accessoryBlock);
        }

        $accessory->setLink($catalogLink->link ?? null);
        $accessory->setLinkLabel($catalogLink->label ?? null);
        $accessoryManager->updateItems($accessory);
        $form = $this->createForm(AccessoryLogoType::class, $accessory);

        if ($accessoryManager->handlerForm($this->getRequest(), $form)) {
            $this->addFlash('sonata_flash_success', 'Accessoire correctement enregistré');
        }

        return $this->renderWithExtraParams('@HondaMain/Admin/sonata_accessory_logo.html.twig', [
                'form' => $form->createView(),
                'accessory' => $accessory,
                'categories' => $accessoryManager->getCategories(),
            ]
        );
    }

}
