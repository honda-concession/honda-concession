<?php

namespace Honda\MainBundle\Controller\Admin;

use Application\Sonata\UserBundle\Entity\User;
use Honda\MainBundle\Controller\Admin\Base\AbstractBaseCRUDController;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PreviewCRUDController
 * @package Honda\MainBundle\Controller\Admin
 */
class PreviewCRUDController extends AbstractBaseCRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function maintenanceAction(Request $request)
    {
        $user = $this->getUser();
        $distributor = $this->getDistributor();
        
        if (!$distributor->getActive()) {
            
            $firewall = 'preview';
            $token = new UsernamePasswordToken($user, null, $firewall, $user->getRoles());
            $this->get("security.token_storage")->setToken($token);
    
            $this->get('session')->set('_security_' . $firewall, serialize($token));
            $this->get('session')->save();
        }
        
        
        return $this->redirectToRoute('homepage');
    }
}
