<?php

namespace Honda\MainBundle\Event;

use Honda\MainBundle\Entity\Subscription;
use Symfony\Component\EventDispatcher\Event;
use PayPal\Api\Payment;

/**
 * Class SubscriptionEvent
 * @package Honda\MainBundle\Event
 */
class SubscriptionEvent extends Event
{
    
    const PAYMENT_SUCCESS = 'on.payment.success';
    const PAYMENT_ERROR = 'on.payment.error';
    
    
    protected $payment;
    protected $subscrip0tion;
    
    
    /**
     * SubscriptionEvent constructor.
     * @param Subscription $subscription
     * @param Payment|null $payment
     */
    public function __construct(Subscription $subscription, Payment $payment = null)
    {
        $this->payment = $payment;
        $this->subscription = $subscription;
    }
    
    
    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }
    
    /**
     * @param Subscription $subscription
     * @return SubscriptionEvent
     */
    public function setSubscription(Subscription $subscription)
    {
        $this->subscription = $subscription;
        return $this;
    }
    
    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }
    
    /**
     * @param Payment $payment
     * @return SubscriptionEvent
     */
    public function setPayment(Payment $payment = null)
    {
        $this->payment = $payment;
        
        return $this;
    }
    
    

}
