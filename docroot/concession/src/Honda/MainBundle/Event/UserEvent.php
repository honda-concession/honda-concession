<?php

namespace Honda\MainBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Application\Sonata\UserBundle\Entity\User;


/**
 * Class UserEvent
 * @package Honda\MainBundle\Event
 */
class UserEvent extends Event
{
    
    const ON_LOGIN_UPDATE_DISTRIBUTOR_INFOS = 'on.login.update.distributor_infos';
    
    /**
     * @var User
     */
    protected $user;
    
    /**
     * UserEvent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    
    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }
    
}
