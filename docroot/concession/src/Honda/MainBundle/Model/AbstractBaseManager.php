<?php

namespace Honda\MainBundle\Model;


use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AbstractBaseManager
 * @package AppBundle\Model\Manager
 */
abstract class AbstractBaseManager
{
    
    const POST_ORIGIN_CONCESSION = 'concession';
    const POST_ORIGIN_PORTAIL = 'portail';
    
    /**
     * @var ObjectManager
     */
    protected $em;
    
    /**
     * AbstractManager constructor.
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * @return ObjectManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }
    
    /**
     * @param $subject
     * @return AbstractBaseManager
     */
    public function save($subject)
    {
        return $this
            ->persist($subject)
            ->flush()
            ;
    }
    
    /**
     * @param $subject
     * @return $this
     */
    public function persist($subject)
    {
        $this->em->persist($subject);
        
        return $this;
    }
    
    /**
     * @return $this
     */
    public function flush()
    {
        $this->em->flush();
        
        return $this;
    }
    
}
