<?php

namespace Honda\MainBundle\Model\Front;

use Honda\MainBundle\Entity\Accessory;
use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Model\AbstractBaseManager;

/**
 * Class AccessoryManager
 * @package Honda\MainBundle\Model\Front
 */
class AccessoryManager extends AbstractBaseManager
{

    /**
     * @param Distributor $distributor
     * @param $accessoryBlockSlug
     * @return string
     */
    public function getAccessoryByDistributorAndAccessoryBlock(Distributor $distributor, $accessoryBlockSlug)
    {
        $accessory = $this->getEntityManager()
            ->getRepository(Accessory::class)->getAccessoryByDistributorAndAccessoryBlock($distributor, $accessoryBlockSlug);

        return $accessory;
    }
}
