<?php

namespace Honda\MainBundle\Model\Front;

use Honda\MainBundle\Entity\Setting;
use Honda\MainBundle\Model\AbstractBaseManager;

/**
 * Class SettingManager
 * @package Honda\MainBundle\Model\Front
 */
class SettingManager extends AbstractBaseManager
{
    
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\SettingRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(Setting::class);
    }
    
    /**
     * @return bool|null|object
     */
    public function getSetting()
    {
        $setting = $this->getRepository()->find(Setting::UNIQUE_ID);
        
        if ($setting) {
            return $setting;
        }
        
        return false;
    }
    
}
