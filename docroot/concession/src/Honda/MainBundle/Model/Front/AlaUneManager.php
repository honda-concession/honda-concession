<?php

namespace Honda\MainBundle\Model\Front;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\ALaUne;
use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Model\AbstractBaseManager;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Honda\MainBundle\Service\MediaUrl;
use Honda\MainBundle\Model\Front\SettingManager;

/**
 * Class AlaUneManager
 * @package Honda\MainBundle\Model\Front
 */
class AlaUneManager extends AbstractBaseManager
{

    const POST_ORIGIN_CONCESSION = 'concession';
    const POST_ORIGIN_PORTAIL = 'portail';

    /**
     * @var GenericGateway
     */
    protected $genericGateway;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var MediaUrl
     */
    protected $mediaUrl;

    /**
     * @var array
     */
    protected $endpoints;

    /**
     * @var \Honda\MainBundle\Model\Front\SettingManager
     */
    protected $settingManager;

    /**
     * SliderManager constructor.
     * @param ObjectManager $em
     * @param GenericGateway $genericGateway
     * @param MediaUrl $mediaUrl
     * @param array $endpoints
     * @param SettingManager $settingManager
     */
    public function __construct(
        ObjectManager $em,
        ContainerInterface $container,
        GenericGateway $genericGateway,
        MediaUrl $mediaUrl,
        $endpoints = [],
        SettingManager $settingManager
    ) {
        parent::__construct($em);

        $this->genericGateway = $genericGateway;
        $this->container = $container;
        $this->mediaUrl = $mediaUrl;
        $this->endpoints = $endpoints;
        $this->settingManager = $settingManager;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\ALaUneRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(ALaUne::class);
    }

    /**
     * @param Distributor $distributor
     * @return array
     */
    public function getAlaUnes(Distributor $distributor, $filter = null, $publishedOnHome = null)
    {
        $aLaUnes = $this->getRepository()->getALaUnesByDistributor($distributor, $filter, $publishedOnHome);

        $endpoint = $this->endpoints['alaune'].'/'.$distributor->getDistributorPortailId();

        if(!is_null($filter)) {
            $endpoint .= '/'.$filter;
        }

        $portailALaUnes =  $this->genericGateway->getData($endpoint, true);

        list($alauneList, $dispoCats) = $this->merge($aLaUnes, $portailALaUnes);

        $counter = count($alauneList);

        return ['alauneList' => $alauneList, 'dispoCats' => $dispoCats, 'counter' => $counter];
    }

    /**
     * @return array
     */
    public function getAlaUneCategories()
    {
        $client = $this->container->get('fos_elastica.client.default');

        $index = $client->getIndex('honda_portail_cat1');

        $type = $index->getType('catALaUne');

        $query = [
            'from' => 0,
            'size' => 100,
            'sort' => [
                'position' => ['order' => 'asc']
            ]
        ];

        $res = $type->search($query)->getResults();

        $categories = [];

        if(!empty($res)) {
            foreach ($res as $cat) {
                $source = $cat->getSource();

                $categories[] = [
                    'id' => $source['id'],
                    'name' => $source['category']
                ];
            }
        } else {
            $categories =  $this->genericGateway->getData($this->endpoints['alauneCategories'], true);
        }

        return $categories;
    }

    /**
     * @param array $aLaUneConcessionaires
     * @param array $aLaUnePortail
     *
     * @return array
     */
    public function merge($aLaUneConcessionaires, $aLaUnePortail)
    {
        $setting = $this->settingManager->getSetting();
        $pushFowardALaune = false;

        if ($setting) {
            $pushFowardALaune = $setting->getPushFowardALaune();
        }

        $alauneListPortail = [];
        $alauneListConcession = [];
        $alauneList = [];
        $dispoCats = [];

        if (count($aLaUnePortail) > 0) {
            foreach ($aLaUnePortail as $alaune) {
                $alauneListPortail[] = [
                    'origin' => self::POST_ORIGIN_PORTAIL,
                    'alaune' => $alaune,
                    'created' => $alaune['created']
                ];

                $type = $alaune['type'];
                if(!is_null($type)) {
                    $dispoCats['type-'.$type] = $type;
                }
            }
        }

        if (count($aLaUneConcessionaires) > 0) {

            foreach ($aLaUneConcessionaires as $alaune) {
                $created = $alaune->getCreatedAt()->getTimestamp();
                $alauneListConcession[] = [
                    'origin' => self::POST_ORIGIN_CONCESSION,
                    'alaune' => $alaune,
                    'created' => $created,
                ];

                $type = $alaune->getType();
                if (!is_null($type)) {
                    $dispoCats['type-'.$type] = $type;
                }
            }
        }

        if ($pushFowardALaune) {
            $alauneList = array_merge($alauneListConcession, $alauneListPortail);
        } else {
            $alauneList = array_merge($alauneListPortail, $alauneListConcession);
        }

//        usort($alauneList, function($a, $b) {
//            return $b['created'] <=> $a['created'];
//        });

        return [$alauneList, $dispoCats];
    }
}
