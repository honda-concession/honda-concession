<?php

namespace Honda\MainBundle\Model\Front;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\Slider;
use Honda\MainBundle\Model\AbstractBaseManager;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Honda\MainBundle\Service\MediaUrl;

/**
 * Class SliderManager
 */
class SliderManager extends AbstractBaseManager
{

    /**
     * @var GenericGateway
     */
    protected $genericGateway;

    /**
     * @var MediaUrl
     */
    protected $mediaUrl;

    /**
     * @var array
     */
    protected $endpoints;

    /**
     * @var SettingManager
     */
    protected $settingManager;

    /**
     * SliderManager constructor.
     * @param ObjectManager $em
     * @param GenericGateway $genericGateway
     * @param MediaUrl $mediaUrl
     * @param array $endpoints
     * @param SettingManager $settingManager
     *
     */
    public function __construct(
        ObjectManager $em,
        GenericGateway $genericGateway,
        MediaUrl $mediaUrl,
        $endpoints = [],
        SettingManager $settingManager
    ) {
        parent::__construct($em);

        $this->genericGateway = $genericGateway;
        $this->mediaUrl = $mediaUrl;
        $this->endpoints = $endpoints;
        $this->settingManager = $settingManager;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\SliderRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(Slider::class);
    }


    /**
     * @param Distributor $distributor
     * @return array
     */
    public function getSliders(Distributor $distributor)
    {
        $setting = $this->settingManager->getSetting();
        $pushFowardSlider = false;

        if ($setting) {
            $pushFowardSlider = $setting->getPushFowardSlider();
        }

        $distributorSliders = $this->getRepository()->getActivesByDistributor($distributor);
        $portailSliders = $this->genericGateway->getData($this->endpoints['slider'].'/'.$distributor->getDistributorPortailId());

        $collection = [];

        foreach ($distributorSliders as $distributorSlider) {

            $item = [];

            if ($distributorSlider->getMedia() && $distributorSlider->getMedia()->getProviderName() == 'sonata.media.provider.image') {

                $item['type'] = 'image';
                $item['origin'] = self::POST_ORIGIN_CONCESSION;
                $item['url'] = $distributorSlider->getUrl();
                $item['external_link'] = (bool) $distributorSlider->getExternalLink();
                $item['title'] = $distributorSlider->getMedia()->getName();
                $item['images'] =  [
                    'ref' => $this->mediaUrl->getUrl($distributorSlider->getMedia(), 'reference'),
                    'srcsets' => [
                        $this->mediaUrl->getUrl($distributorSlider->getMedia(), 'big'),
                        $this->mediaUrl->getUrl($distributorSlider->getMedia(), 'medium'),
                        $this->mediaUrl->getUrl($distributorSlider->getMedia(), 'small'),
                    ]
                ];

            } elseif ($distributorSlider->getMedia() && $distributorSlider->getMedia()->getProviderName() == 'sonata.media.provider.youtube') {

                $item['type'] = 'video';
                $item['origin'] = self::POST_ORIGIN_CONCESSION;
                $item['title'] = $distributorSlider->getMedia()->getName();
                $item['video'] = [
                    'image' => $this->mediaUrl->getUrl($distributorSlider->getMedia(), 'reference'),
                    'providerReference' => $distributorSlider->getMedia()->getProviderReference(),
                    'url' => $distributorSlider->getMedia()->getUrl()
                ];
                if ($imageYoutube = $distributorSlider->getImageYoutube()) {
                    $item['imageYoutube'] = $this->mediaUrl->getUrl($imageYoutube, 'reference');
                }
            }

            $collection[] = $item;
        }

        if ($pushFowardSlider) {
            $collection = array_merge($collection, $portailSliders);
        } else {
            $collection = array_merge($portailSliders, $collection);
        }

        return $collection;
    }

}
