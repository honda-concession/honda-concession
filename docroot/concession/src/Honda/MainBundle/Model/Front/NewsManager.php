<?php

namespace Honda\MainBundle\Model\Front;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\News;
use Honda\MainBundle\Model\AbstractBaseManager;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Honda\MainBundle\Model\Front\SettingManager;


/**
 * Class NewsManager
 * @package Honda\MainBundle\Model\Front
 */
class NewsManager extends AbstractBaseManager
{

    /**
     * @var GenericGateway
     */
    protected $genericGateway;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $endpoints;

    /**
     * SliderManager constructor.
     * @param ObjectManager $em
     * @param GenericGateway $genericGateway
     * @param array $endpoints
     * @param SettingManager $settingManager
     */
    public function __construct(
        ObjectManager $em,
        ContainerInterface $container,
        GenericGateway $genericGateway,
        $endpoints = [],
        SettingManager $settingManager
    ) {
        parent::__construct($em);
        $this->container = $container;
        $this->genericGateway = $genericGateway;
        $this->endpoints = $endpoints;
        $this->settingManager = $settingManager;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\NewsRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(News::class);
    }

    /**
     * @param Distributor $distributor
     * @param null $filter
     * @param bool $all
     * @return array
     */
    public function getNewsHp(Distributor $distributor, $filter = null, $all = false)
    {
        $distributorNews = $this->getRepository()->getHPByDistributor($distributor, $filter, $all);

        $endpoint = $this->endpoints['newsHp'].'/'.$distributor->getDistributorPortailId();

        if(!is_null($filter)) {
            $endpoint .= '/'.$filter;
        }

        $portailNews = $this->genericGateway->getData($endpoint, true);

        list($newsList, $dispoCats) = $this->merge($distributorNews, $portailNews);

        $counter = count($newsList);

        return  ['newsList' => $newsList, 'dispoCats' => $dispoCats, 'counter' => $counter] ;
    }

    /**
     * @param Distributor $distributor
     * @return array
     */
    public function getNewsForPage(Distributor $distributor, $filter = null, $all = false)
    {
        $distributorNews = $this->getRepository()->getNewsForPageNewsByDistributor($distributor, $filter, $all);

        $endpoint = $this->endpoints['newsHp'].'/'.$distributor->getDistributorPortailId();

        if(!is_null($filter)) {
            $endpoint .= '/'.$filter;
        }

        $portailNews = $this->genericGateway->getData($endpoint, true);

        list($newsList, $dispoCats) = $this->merge($distributorNews, $portailNews);

        $counter = count($newsList);

        return  ['newsList' => $newsList, 'dispoCats' => $dispoCats, 'counter' => $counter] ;
    }

    /**
     * @return array
     */
    public function getNewsCategories()
    {
        $client = $this->container->get('fos_elastica.client.default');

        $index = $client->getIndex('honda_portail_cat');

        $type = $index->getType('catNews');

        $query = [
            'from' => 0,
            'size' => 100,
            'sort' => [
                'position' => ['order' => 'asc']
            ]
        ];

        $res = $type->search($query)->getResults();

        $categories = [];

        if(!empty($res)) {
            foreach ($res as $cat) {
                $source = $cat->getSource();

                $categories[] = [
                    'id' => $source['id'],
                    'name' => $source['category']
                ];
            }
        } else {
            $categories = $this->genericGateway->getData($this->endpoints['newsCategories'], true);
        }

        return $categories;
    }

    /**
     * @param string $slug
     * @return array
     */
    public function getRemoteNews($slug)
    {
        $news = $this->genericGateway->getData($this->endpoints['news'].'/'.$slug, true);

        return $news;
    }

    /**
     * @param array $distributorNews
     * @param array $portailNews
     *
     * @return array
     */
    public function merge($distributorNews, $portailNews)
    {
        $setting = $this->settingManager->getSetting();
        $pushFoward = false;
        if ($setting) {
            $pushFoward = $setting->getPushFowardNews();
        }

        $listPortail = [];
        $listConcession = [];
        $newsList = [];
        $dispoCats = [];

        if (count($distributorNews) > 0) {
            foreach ($distributorNews as $news) {
                $created = $news->getCreatedAt()->getTimestamp();
                $listConcession[] = [
                    'origin' => self::POST_ORIGIN_CONCESSION,
                    'news' => $news,
                    'created' => $created
                ];
                $type = $news->getType();
                if (!is_null($type)) {
                    $dispoCats['type-'.$type] = $type;
                }
            }
        }

        if (count($portailNews) > 0) {
            foreach ($portailNews as $news) {
                $listPortail[] = [
                    'origin' => self::POST_ORIGIN_PORTAIL,
                    'news' => $news,
                    'created' => $news['created']
                ];
                $type = $news['type'];
                if (!is_null($type)) {
                    $dispoCats['type-'.$type] = $type;
                }
            }
        }

        if ($pushFoward) {
            $newsList = array_merge($listConcession, $listPortail);
        } else {
            $newsList = array_merge($listPortail, $listConcession);
        }

        /*usort($newsList, function($a, $b) {
            return $b['created'] <=> $a['created'];
        });*/

        return [$newsList, $dispoCats];
    }
}
