<?php


namespace Honda\MainBundle\Model\Front;

use Symfony\Component\HttpFoundation\Request;


class VoSearchRequest
{
    
    /**
     * @var Array
     */
    protected $orders = [];
    
    /**
     * @var Array
     */
    protected $categories = [];
    
    /**
     * VoSearchRequest constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        if ($request->query->has('header-select')) {
            $this->setOrders(['price' => $request->query->get('header-select', [])]);
        }
    
        if ($request->query->has('header-select-2')) {
            $this->setCategories(['categoryPortail' => $request->query->get('header-select-2', [])]);
        }
    }
    
    /**
     * @return mixed
     */
    public function getOrders()
    {
        return $this->orders;
    }
    
    /**
     * @param mixed $orders
     * @return VoSearchRequest
     */
    public function setOrders($orders = [])
    {
        $this->orders = $orders;
        
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }
    
    /**
     * @param mixed $categories
     * @return VoSearchRequest
     */
    public function setCategories($categories = [])
    {
        $this->categories = $categories;
    
        return $this;
    }
}
