<?php

namespace Honda\MainBundle\Model\Front;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\News;
use Honda\MainBundle\Entity\VehicleAd;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Honda\MainBundle\Model\Middleware\PortailWrapper;
use Symfony\Component\Routing\RouterInterface;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Honda\MainBundle\Model\AbstractBaseManager;

/**
 * Class SiteMapBuilder
 * @package Honda\MainBundle\Model\Front
 */
class SiteMapBuilder
{
    
    /**
     * @var ContainerInterface
     */
    protected $container;
    
    /**
     * @var RouterInterface
     */
    protected $router;
    
    /**
     * @var PortailWrapper
     */
    protected $portailWrapper;
    
    /**
     * @var Array
     */
    protected $endpoints;
    
    /**
     * @var ObjectManager
     */
    protected $em;
    
    /**
     * Builder constructor.
     * @param RouterInterface $router
     * @param PortailWrapper $portailWrapper
     * @param $endpoints
     * @param ObjectManager $em
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = $this->container->get('router');
        $this->portailWrapper = $this->container->get(PortailWrapper::class);
        $this->endpoints = $this->container->getParameter('endpoints.others');
        $this->em = $this->container->get('doctrine.orm.entity_manager');
    }
    
    /**
     * @param Distributor|null $distributor
     * @return array
     */
    public function run(Distributor $distributor)
    {
        $urls = [];
        
        $gateway = $this->container->get(GenericGateway::class);
        $gammeTarifs = $gateway->getData($this->endpoints['gammeTarifs'] . '/', true);
        
        $urls[] = [
            'loc' => $this->router->generate('homepage', [], RouterInterface::ABSOLUTE_URL),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => 0.9
        ];
        $urls[] = [
            'loc' => $this->router->generate('services', [], RouterInterface::ABSOLUTE_URL),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => 0.8
        ];
        $urls[] = [
            'loc' => $this->router->generate('accessoiries', [], RouterInterface::ABSOLUTE_URL),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => 0.8
        ];
        $urls[] = [
            'loc' => $this->router->generate('vehicule_occasion', [], RouterInterface::ABSOLUTE_URL),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => 0.8
        ];
        
        foreach ($gammeTarifs['categories'] as $category) {

            $urls[] = [
                'loc' => $this->router->generate('new_vehicle_list', ['categorySlug' => $category['category_slug']], RouterInterface::ABSOLUTE_URL),
                'lastmod' => date('Y-m-d'),
                'changefreq' => 'daily',
                'priority' => 0.7
            ];
    
            if (isset($category['models'])) {
                
                foreach ($category['models'] as $model) {
        
                    if (isset($model['submodels'])) {
                        
                        foreach ($model['submodels'] as $submodel) {
        
                            $urls[] = [
                                'loc' => $this->router->generate('new_vehicle_show', [
                                    'categorySlug' => $category['category_slug'],
                                    'modelSlug' => $model['slug'],
                                    'vehicleSlug' => $submodel['vehicle_slug'],
                                ], RouterInterface::ABSOLUTE_URL),
                                'lastmod' => date('Y-m-d'),
                                'changefreq' => 'daily',
                                'priority' => 0.6
                            ];
        
                        }
                    }
                }
            }
        }
    
        $newsManager = $this->container->get(NewsManager::class);
        $res = $newsManager->getNewsForPage($distributor, null, false);
        $news = $res['newsList'] ?? [];

        foreach ($news as $newsItem) {

            $url = null;

            if (AbstractBaseManager::POST_ORIGIN_PORTAIL == $newsItem['origin']) {
                if (isset($newsItem['news']) && !$newsItem['news']['showInSitemap']) {
                    continue;
                }
                if (isset($newsItem['news']) && is_array($newsItem['news']) && isset($newsItem['news']['slug'])) {
                    $url = $this->router->generate('news_show_portail_article', ['slug' => $newsItem['news']['slug']], RouterInterface::ABSOLUTE_URL);
                }
            } elseif (AbstractBaseManager::POST_ORIGIN_CONCESSION == $newsItem['origin']) {
                if (isset($newsItem['news']) && $newsItem['news'] instanceof News) {
                    $news = $newsItem['news'];
                    if ($news->getHideInSitemap()) {
                        continue;
                    }
                    $url = $this->router->generate('news_show_article', ['slug' => $news->getSlug()], RouterInterface::ABSOLUTE_URL);
                }
            }

            if ($url) {

                $urls[] = [
                    'loc' => $url,
                    'lastmod' => date('Y-m-d'),
                    'changefreq' => 'daily',
                    'priority' => 0.5
                ];
            }
          
        }
        
        $vehicleAdRepository = $this->container->get('doctrine.orm.entity_manager')->getRepository(VehicleAd::class);
        $vehicleAds = $vehicleAdRepository->getVehicleAdByDistributor($distributor);
        
        foreach ($vehicleAds as $vehicleAd) {
    
            $urls[] = [
                'loc' => $this->router->generate('vehicule_occasion_voir', ['slug' => $vehicleAd->getSlug()], RouterInterface::ABSOLUTE_URL),
                'lastmod' => date('Y-m-d'),
                'changefreq' => 'daily',
                'priority' => 0.8
            ];
        }

        return $urls;
    }
}
