<?php

namespace Honda\MainBundle\Model\Front;

use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Model\AbstractBaseManager;
use Honda\MainBundle\Entity\Service;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Honda\MainBundle\Model\Front\SettingManager;


/**
 * Class ServiceManager
 * @package Honda\MainBundle\Model\Front
 */
class ServiceManager extends AbstractBaseManager
{
    /**
     * @var GenericGateway
     */
    protected $genericGateway;

    /**
     * @var array
     */
    protected $endpoints;

    /**
     * @var \Honda\MainBundle\Model\Front\SettingManager
     */
    protected $settingManager;

    /**
     * SliderManager constructor.
     * @param ObjectManager $em
     * @param GenericGateway $genericGateway
     * @param array $endpoints
     * @param SettingManager $settingManager
     */
    public function __construct(ObjectManager $em, GenericGateway $genericGateway, $endpoints = [], SettingManager $settingManager)
    {
        parent::__construct($em);

        $this->genericGateway = $genericGateway;
        $this->endpoints = $endpoints;
        $this->settingManager = $settingManager;
    }

    /**
     * @param Distributor $distributor
     * @return array
     */
    public function getServices(Distributor $distributor, $publishedOnHome = null)
    {
        $setting = $this->settingManager->getSetting();
        $pushFoward = false;
        if ($setting) {
            $pushFoward = $setting->getPushFowardService();
        }

        $endpoint = $this->endpoints['services'].'/'.$distributor->getDistributorPortailId();

        $defaultServices = $this->genericGateway->getData($endpoint, true);
        $distributorServices = $this->em->getRepository(Service::class)->getAllByDistributor($distributor, $publishedOnHome);

        $collection['services'] = [];

        foreach ($distributorServices as $distributorService) {
            $item = [];
            $item['id'] = $distributorService->getId();
            $item['block_uniqid'] = $distributorService->getSlug();
            $item['title'] = $distributorService->getTitle();
            $item['icon'] = [
                'type' => 'jpg',
                'value' => $distributorService->getPicto()
            ];
            $item['pdf'] = $distributorService->getPdf();
            $item['label'] = "";
            $item['content'] = $distributorService->getContent();
            $item['priceListTable'] = $distributorService->getPriceListTable();
            $item['contentWithoutPriceListTable'] = $distributorService->getContentWithoutPriceListTable();
            $item['link'] = $distributorService->getLink();
            $item['image'] = $distributorService->getImage();
            $item['is_default_service'] = false;
            $item['from_portail'] = false;
            $item = $distributorService->setRedirectionValuesToArray($item);
            $collection['services'][] = $item;
        }
        if ($pushFoward) {
            $services = array_merge_recursive($collection, $defaultServices);
        } else {
            $services = array_merge_recursive($defaultServices, $collection);
        }

        return $services;
    }
}
