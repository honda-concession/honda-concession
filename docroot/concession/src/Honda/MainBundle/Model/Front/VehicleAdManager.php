<?php

namespace Honda\MainBundle\Model\Front;

use Doctrine\Common\Collections\ArrayCollection;
use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\VehicleAd;
use Honda\MainBundle\Entity\VehicleAdGallery;
use Honda\MainBundle\Model\AbstractBaseManager;
use Honda\MainBundle\Model\Front\VoSearchRequest;

/**
 * Class VehicleAdManager
 * @package Honda\MainBundle\Model\Front
 */
class VehicleAdManager extends AbstractBaseManager
{
   
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\SliderRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(VehicleAd::class);
    }
    
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\SliderRepository
     */
    public function getVehicleAdRepository()
    {
        return $this->getEntityManager()->getRepository(VehicleAdGallery::class);
    }
    
    /**
     * @param Distributor $distributor
     * @param \Honda\MainBundle\Model\Front\VoSearchRequest $voSearchRequest
     * @return mixed
     */
    public function getAllByDistributor(Distributor $distributor, VoSearchRequest $voSearchRequest = null)
    {
        return $this->getRepository()->getVehicleAdByDistributor($distributor, $voSearchRequest);
    }
    
    /**
     * @param Distributor $distributor
     * @param VehicleAd $vehicleAd
     * @return array<VehicleAd>
     */
    public function getNextAndPrevVehicleAds(Distributor $distributor, VehicleAd $vehicleAd)
    {
        $vehiclesAds = $this->getAllByDistributor($distributor);
        
        $next = null;
        $prev = null;
        
        $counter = count($vehiclesAds);
        
        foreach ($vehiclesAds as $i => $vehiclesAdItem) {
            
            if ($vehiclesAdItem === $vehicleAd) {
                
                $indexPrev  = $i - 1 < 0 ? 0 : $i - 1;
                $prev       = $vehiclesAds[$indexPrev];
                
                if ($indexPrev == $i) {
                    $prev = null;
                }
    
                $indexNext  = $i + 1 > $counter ? $counter : $i + 1;
                $next       = $vehiclesAds[$indexNext];
                
                break;
            }
        }
        
        return [$prev, $next];
    }
    
}
