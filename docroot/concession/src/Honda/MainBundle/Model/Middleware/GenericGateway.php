<?php

namespace Honda\MainBundle\Model\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;


/**
 * Class GenericGateway
 * @package Honda\MainBundle\Model\Middleware
 */
class GenericGateway
{
    /**
     * @var
     */
    protected $serverEnv;
    
    /**
     * AbstractGateway constructor.
     * @param $serverEnv
     */
    public function __construct($serverEnv)
    {
        $this->serverEnv = $serverEnv;
    }
    
    /**
     * @param $endpoint
     * @param bool $displayArray
     * @return array|mixed
     */
    public function getData($endpoint, $displayArray = false)
    {
        $configs = ['timeout'  => 10];
        
        $client = new Client($configs);
        
        $sslVerification = false;
        
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
        
        try {
            $res = $client->request('GET', $endpoint, ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
        
        
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
          
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, $displayArray);
               
                return $responseData;
                
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){
            }
        }
        
        return [];
    }
}
