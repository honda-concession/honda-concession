<?php

namespace Honda\MainBundle\Model\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Psr7\Response;


/**
 * Class ColorsVoGateway
 * @package Honda\MainBundle\Model\Middleware
 */
class ColorsVoGateway extends AbstractGateway
{
    
    /**
     * @return array|null
     */
    public function getColors()
    {
        $configs = ['timeout'  => 10];
        
        $client = new Client($configs);
        $data = [];
        
        $sslVerification = false;
        
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
        
        try {
            $res = $client->request('GET', $this->endpoints['colorVo'], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
        
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
            
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);
    
                $collection = [];
    
                foreach ($responseData as $color) {
                    if (isset($color['name'])) {
                        $collection[$color['name']] = $color['name'];
                    }
                }
                
                return $collection;
                
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
            
        }
        
        return [];
    }
}
