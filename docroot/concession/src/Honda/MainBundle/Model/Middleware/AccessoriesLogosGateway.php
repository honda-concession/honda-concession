<?php

namespace Honda\MainBundle\Model\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;


/**
 * Class AccessoriesLogosGateway
 * @package Honda\MainBundle\Model\Middleware
 */
class AccessoriesLogosGateway extends AbstractGateway
{
    
    /**
     * @return array|mixed
     */
    public function getHelmetLogos()
    {
        $configs = ['timeout'  => 10];
    
        $client = new Client($configs);
    
        $sslVerification = false;
    
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
    
        try {
            $res = $client->request('GET', $this->endpoints['helmetLogos'], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
    
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
            
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);
                
                return $responseData;
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
            
        }
    
        return [];
    }

    /**
     * @return array|mixed
     */
    public function getClothingLogos()
    {
        $configs = ['timeout'  => 10];
        
        $client = new Client($configs);
        
        $sslVerification = false;
        
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
        
        try {
            $res = $client->request('GET', $this->endpoints['clothingLogos'], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
        
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);
        
                return $responseData;
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
        }
        
        return [];
    }
    
}
