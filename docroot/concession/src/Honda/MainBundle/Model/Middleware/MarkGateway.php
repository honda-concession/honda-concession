<?php

namespace Honda\MainBundle\Model\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Psr7\Response;


/**
 * Class MarkGateway
 * @package Honda\MainBundle\Model\Middleware
 */
class MarkGateway extends AbstractGateway
{
    
    /**
     * @return array
     */
    public function getMarks()
    {
        $configs = ['timeout'  => 10];
        
        $client = new Client($configs);
        $collection = [];
    
        $sslVerification = false;
    
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
        
        try {
            $res = $client->request('GET', $this->endpoints['list'], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
        
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
    
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);
    
                return $responseData;
                
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
            
        }
        
        return $collection;
    }
}
