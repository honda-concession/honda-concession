<?php

namespace Honda\MainBundle\Model\Middleware;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Honda\MainBundle\Tools\Encryption\EncryptorAesEcb;
use Honda\MainBundle\Entity\Distributor;

/**
 * Class DistributorGateway
 * @package Honda\MainBundle\Model\Middleware
 */
class DistributorGateway extends AbstractGateway
{
    
    /**
     * @var SessionInterface
     */
    protected $securityTokenStorage;
    
    /**
     * DistributorGateway constructor.
     * @param array $endpoints
     * @param $serverEnv
     * @param TokenStorageInterface $securityTokenStorage
     */
    public function __construct(array $endpoints, $serverEnv, TokenStorageInterface $securityTokenStorage, string $key = null)
    {
        parent::__construct($endpoints, $serverEnv);
        
        $this->securityTokenStorage = $securityTokenStorage;
        $this->key = $key;
    }
    
    /**
     * @param Distributor|null $distributor
     * @return array|mixed
     */
    public function getDistributorInfos(Distributor $distributor = null)
    {
        
        if (is_null($distributor)) {
            if ($this->securityTokenStorage->getToken()
                && $this->securityTokenStorage->getToken()->getUser()
                && $this->securityTokenStorage->getToken()->getUser()->getDistributor()) {
                $distributor = $this->securityTokenStorage->getToken()->getUser()->getDistributor();
            }
        }
        
        if (!$distributor || ! ($distributor instanceof Distributor)) {
            throw new \RuntimeException('Un distributeur valide est obligatoire');
        }

        $token = EncryptorAesEcb::encode($distributor->getDistributorPortailId(), $this->key);


    
        $endpoint = str_replace(':id', $distributor->getDistributorPortailId(), $this->endpoints['get']);
        
        $configs = ['timeout'  => 20];
        
        $client = new Client($configs);
    
        $sslVerification = false;
    
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
    
        try {
            $res = $client->request('GET', $endpoint, ['verify' => $sslVerification, 'headers' => ['X-AUTH-TOKEN' => $token]]);
        } catch (RequestException $e) {}
        
        if (isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
  
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);

                return $responseData;
        
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
            
        }
        
        return [];
    }

    /**
     * @param array $data
     * @return null
     */
    public function updateDistributorInfos(Distributor $distributor = null, array $data = [])
    {
        if(!empty($data) && !empty($distributor)) {
 
            $token = EncryptorAesEcb::encode(implode('-', $data), $this->key);
            $endpoint = str_replace(':id', $distributor->getDistributorPortailId(), $this->endpoints['update']);
            $configs = ['timeout'  => 20];

            $client = new Client($configs);
        
            $sslVerification = false;
        
            if ($this->serverEnv == 'prod') {
                $sslVerification = true;
            }
        
            try {
                $res = $client->post($endpoint, [
                        'verify' => $sslVerification,
                        'headers' => ['X-AUTH-TOKEN' => $token],
                        'json' => $data
                    ]
                );
            } catch (RequestException $e) {
            }
        }
    }
    
}
