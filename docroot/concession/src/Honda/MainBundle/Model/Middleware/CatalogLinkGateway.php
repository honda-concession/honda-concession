<?php

namespace Honda\MainBundle\Model\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Psr7\Response;


/**
 * Class CatalogLinkGateway
 * @package Honda\MainBundle\Model\Middleware
 */
class CatalogLinkGateway extends AbstractGateway
{
    
    
    /**
     * @return array|mixed
     */
    public function getCatalogLink($endpoint)
    {
        $configs = ['timeout'  => 10];
    
        $client = new Client($configs);
    
        $sslVerification = false;
    
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
    
        try {
            $res = $client->request('GET', $this->endpoints[$endpoint], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
    
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
        
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json);
                return $responseData;
    
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
        }
    
        return [];
    }
}
