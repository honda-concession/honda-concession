<?php

namespace Honda\MainBundle\Model\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\GuzzleException;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class InventoryGateway
 * @package Honda\MainBundle\Model\Middleware
 */
class InventoryGateway extends AbstractGateway
{
    
    /**
     * Récupèrer les vehicles pour la gestion de stock
     */
    public function getVehicles()
    {
        $configs = ['timeout'  => 10];
    
        $client = new Client($configs);
    
        $sslVerification = false;
    
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
        
        try {
            $res = $client->request('GET', $this->endpoints['inventory'], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
    
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
            
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json);
    
                return $responseData;
    
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
        }
        
        return [];
    }
    
}
