<?php

namespace Honda\MainBundle\Model\Middleware;


/**
 * Class PortailWrapper
 * @package Honda\MainBundle\Model\Middleware
 */
class PortailWrapper
{
    /**
     * @var MarkGateway
     */
    protected $markGateway;
    
    /**
     * @var CategoryGateway
     */
    protected $categoryGateway;
    
    /**
     * @var DistributorGateway
     */
    protected $distributorGateway;
    
    /**
     * @var AccessoriesLogosGateway
     */
    protected $accessoriesLogosGateway;
    
    /**
     * @var InventoryGateway
     */
    protected $inventoryGateway;
    
    /**
     * @var CatalogLinkGateway
     */
    protected $catalogLinkGateway;
    
    /**
     * @var ModelGateway
     */
    protected $modelGateway;
    
    /**
     * @var GenericGateway
     */
    protected $genericGateway;
    
    /**
     * @var VoGateway
     */
    protected $voGateway;
    
    
    /**
     * PortailWrapper constructor.
     * @param MarkGateway $markGateway
     * @param CategoryGateway $categoryGateway
     * @param DistributorGateway $distributorGateway
     * @param AccessoriesLogosGateway $accessoriesLogosGateway
     * @param InventoryGateway $inventoryGateway
     * @param CatalogLinkGateway $catalogLinkGateway
     * @param ModelGateway $modelGateway
     * @param GenericGateway $genericGateway
     * @param VoGateway $colorsVoGateway
     */
    public function __construct(
        MarkGateway $markGateway,
        CategoryGateway $categoryGateway,
        DistributorGateway $distributorGateway,
        AccessoriesLogosGateway $accessoriesLogosGateway,
        InventoryGateway $inventoryGateway,
        CatalogLinkGateway $catalogLinkGateway,
        ModelGateway $modelGateway,
        GenericGateway $genericGateway,
        VoGateway $voGateway
    ) {
        $this->markGateway = $markGateway;
        $this->categoryGateway = $categoryGateway;
        $this->distributorGateway = $distributorGateway;
        $this->accessoriesLogosGateway = $accessoriesLogosGateway;
        $this->inventoryGateway = $inventoryGateway;
        $this->catalogLinkGateway = $catalogLinkGateway;
        $this->modelGateway = $modelGateway;
        $this->genericGateway = $genericGateway;
        $this->voGateway = $voGateway;
    }
    
    /**
     * @return VoGateway
     */
    public function getVoGateway()
    {
        return $this->voGateway;
    }
    
    /**--
     * @return MarkGateway
     */
    public function getMarkGateway()
    {
        return $this->markGateway;
    }
    
    /**
     * @return CategoryGateway
     */
    public function getCategoryGateway()
    {
        return $this->categoryGateway;
    }
    
    /**
     * @return DistributorGateway
     */
    public function getDistributorGateway()
    {
        return $this->distributorGateway;
    }
    
    /**
     * @return AccessoriesLogosGateway
     */
    public function getAccessoriesLogosGateway()
    {
        return $this->accessoriesLogosGateway;
    }
    
    /**
     * @return InventoryGateway
     */
    public function getInventoryGateway()
    {
        return $this->inventoryGateway;
    }
    
    /**
     * @return CatalogLinkGateway
     */
    public function getCatalogLinkGateway()
    {
        return $this->catalogLinkGateway;
    }
    
    /**
     * @return GenericGateway
     */
    public function getGenericGateway()
    {
        return $this->genericGateway;
    }
    
    /**
     * @return ModelGateway
     */
    public function getModelGateway()
    {
        return $this->modelGateway;
    }
}
