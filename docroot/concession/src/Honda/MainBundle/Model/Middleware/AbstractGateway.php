<?php

namespace Honda\MainBundle\Model\Middleware;


/**
 * Class AbstractGateway
 * @package Honda\MainBundle\Model\Middleware
 */
abstract class AbstractGateway
{
    /**
     * @var
     */
    protected $endpoints;
    
    /**
     * @var
     */
    protected $serverEnv;
    
    /**
     * AbstractGateway constructor.
     * @param array $endpoints
     * @param $serverEnv
     */
    public function __construct(array $endpoints, $serverEnv)
    {
        $this->endpoints = $endpoints;
        $this->serverEnv = $serverEnv;
    }
    
    /**
     * @return mixed
     */
    public function getEndpoints()
    {
        return $this->endpoints;
    }
    
    
}
