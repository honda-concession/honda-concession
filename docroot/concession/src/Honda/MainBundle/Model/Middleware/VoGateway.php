<?php

namespace Honda\MainBundle\Model\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;


/**
 * Class AbstractGateway
 * @package Honda\MainBundle\Model\Middleware
 */
class VoGateway extends AbstractGateway
{
    
    /**
     * @var Client
     */
    protected $client;
    
    /**
     * VoGateway constructor.
     * @param array $endpoints
     * @param $serverEnv
     */
    public function __construct(array $endpoints, $serverEnv)
    {
        parent::__construct($endpoints, $serverEnv);
        $this->client = new Client(['timeout' => 10]);
        
    }
    
    /**
     * @return array|null
     */
    public function getColors()
    {
        $sslVerification = false;
        
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
        
        try {
            $res = $this->client->request('GET', $this->endpoints['colorVo'], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
        
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
            
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);
                
                $collection = [];
                
                foreach ($responseData as $color) {
                    if (isset($color['name'])) {
                        $collection[$color['name']] = $color['name'];
                    }
                }
                
                return $collection;
                
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
            
        }
        
        return [];
    }
    
    /**
     * @return array|mixed
     */
    public function getData($endpointKey)
    {
        $sslVerification = false;
    
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
        
        if (!isset($this->endpoints[$endpointKey])) {
            return;
        }
    
        try {
            $res = $this->client->request('GET', $this->endpoints[$endpointKey], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }
    
        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
        
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);
                
                return $responseData;
            
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
        
        }
    
        return [];
    }
    
}
