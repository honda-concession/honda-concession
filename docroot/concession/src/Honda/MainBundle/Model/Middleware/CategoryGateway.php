<?php

namespace Honda\MainBundle\Model\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;


/**
 * Class CategoryGateway
 * @package Honda\MainBundle\Model\Middleware
 */
class CategoryGateway extends AbstractGateway
{
    
    /**
     * @return array|null
     */
    public function getCategories()
    {
        $configs = ['timeout'  => 10];
        
        $client = new Client($configs);
        $data = [];
    
        $sslVerification = false;
        
        if ($this->serverEnv == 'prod') {
            $sslVerification = true;
        }
        
        try {
            $res = $client->request('GET', $this->endpoints['list'], ['verify' => $sslVerification]);
        } catch (RequestException $e) {
        }

        if ( isset($res) && $res instanceof Response && 200 == $res->getStatusCode()) {
    
            try {
                $json = (string) $res->getBody();
                $responseData = \GuzzleHttp\json_decode($json, true);
    
                foreach ($responseData as $category) {
                    if(isset($category['category'])){
                        $data[$category['category']] = $category['category'];
                    }
        
                }
            } catch(GuzzleException $e){
            } catch(\InvalidArgumentException $e){}
            
        }

        return $data;
    }
}
