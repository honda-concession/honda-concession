<?php

namespace Honda\MainBundle\Model\Admin;


use Honda\MainBundle\Model\AbstractBaseManager;
use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\Inventory;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class InventoryManager
 * @package Honda\MainBundle\Model\Admin
 */
class InventoryManager extends AbstractBaseManager
{
    
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\InventoryRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(Inventory::class);
    }
    
    /**
     * @param Distributor $distributor
     * @param $jsonData
     * @return ArrayCollection
     */
    public function importNewVehicles(Distributor $distributor, $jsonData)
    {
        $collection = new ArrayCollection();
        
        $inventories = $this->getRepository()->findAll();
        $tab = [];
        $fromApitab = [];
        
        foreach ($inventories as $inventory) {
            if (!isset($tab[$inventory->getNewVehicleId()])) {
                $tab[$inventory->getNewVehicleId()] = $inventory;
            }
        }
        
        foreach ($jsonData as $newVehiclePortail) {
            
            $inventory = $this->getRepository()->loadByDistributorAndNewVehiclePortailId($distributor, $newVehiclePortail->id);
            
            if (!$inventory) {
                $inventory = new Inventory();
                $inventory
                    ->setNewVehicleId($newVehiclePortail->id)
                    ->setDistributor($distributor)
                ;
            }
            
            $inventory
                ->setNewVehicleTitle($newVehiclePortail->title)
                ->setNewVehicleColor($newVehiclePortail->color)
                ->setNewVehicleColorIcon($newVehiclePortail->color_icon)
                ->setNewVehicleColorCode($newVehiclePortail->color_code)
                ->setNewVehicleModel($newVehiclePortail->model)
                ->setNewVehicleCategory($newVehiclePortail->category)
                ->setNewVehiclePrice($newVehiclePortail->price)
                ->setNewVehicleSubmodel($newVehiclePortail->submodel)
                ->setNewVehicleReference($newVehiclePortail->reference)
                ->setNewVehicleSubmodelId($newVehiclePortail->submodel_id)
                
                ->setColorKoraId($newVehiclePortail->color_kora_id)
                ->setColorKoraCode($newVehiclePortail->color_kora_code)
                ->setColorKoraName($newVehiclePortail->color_kora_name)
                
                ->setImage($newVehiclePortail->image)
                ->setKoraId($newVehiclePortail->kora_id)
            ;
    
            $this->persist($inventory);
            
            $item = ['newVehicle' => $newVehiclePortail, 'inventory' =>  $inventory, 'distributor' => $distributor ];
            $collection->add($item);
            $fromApitab[$newVehiclePortail->id] = $inventory->getNewVehicleId();
        }
        
        foreach (array_diff(array_keys($tab), $fromApitab) as $key => $inventory) {

            if (isset($tab[$inventory])) {
                $this->em->remove($tab[$inventory]);
            }
        }
        
        $this->flush();
        
        return $collection;
    }
    
    /**
     * @param Inventory $inventory
     * @param Request $request
     * @return Inventory
     */
    public function saveInStock(Inventory $inventory, Request $request)
    {
        $inStock = (bool) $request->request->get('inStock');
    
        $inventory->setInStock($inStock);
        $this->getEntityManager()->flush();
        
        return $inventory;
    }
    
}
