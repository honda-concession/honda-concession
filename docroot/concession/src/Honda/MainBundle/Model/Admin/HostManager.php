<?php

namespace Honda\MainBundle\Model\Admin;

use Symfony\Component\HttpFoundation\RequestStack;
use Prodigious\MultisiteBundle\Manager\SiteManager;
use Honda\MainBundle\Entity\Distributor;

/**
 * Class HostManager
 * @package Honda\MainBundle\Model\Admin
 */
class HostManager
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var SiteManager
     */
    protected $sm;
    
    
    /**
     * DomainManager constructor.
     * @param RequestStack $requestStack
     * @param null $baseHost
     */
    public function __construct(RequestStack $requestStack, SiteManager $sm)
    {
        $this->requestStack = $requestStack;
        $this->sm = $sm;
    }
    
    /**
     * @return mixed
     */
    public function getDomain()
    {
        $request = $this->requestStack->getCurrentRequest();
        $currentHost = $request->getHttpHost();

        return $currentHost;
    }
    
    /**
     * @param Distributor $distributor
     * @return bool
     */
    public function isDomainValid(Distributor $distributor)
    {
        $currentSite = $this->requestStack->getCurrentRequest()->get('site');

        $distributorDomain = $distributor->getDomain();
 
        $currentHost = $this->getDomain();

        $notExist = $this->sm->checkHost($currentSite, $distributorDomain);

        return (!$notExist && $currentHost == $distributorDomain);

    }
    
}
