<?php
/**
 * Created by PhpStorm.
 * User: monkees
 * Date: 25/10/18
 * Time: 16:28
 */

namespace Honda\MainBundle\Model\Admin;


use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\Subscription;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;

use PayPal\Api\Payer;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;

use Honda\MainBundle\Event\SubscriptionEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class PaypalManager
{
    protected $router;
    protected $session;
    protected $paypalClientID;
    protected $paypalSecret;
    protected $apiContext;
    protected $em;
    protected $dispatcher;
    
    /**
     * PaypalManager constructor.
     * @param SessionInterface $session
     * @param RouterInterface $router
     * @param ObjectManager $em
     * @param EventDispatcherInterface $dispatcher
     * @param $paypalClientID
     * @param $paypalSecret
     */
    public function __construct(
        SessionInterface $session,
        RouterInterface $router,
        ObjectManager $em,
        EventDispatcherInterface $dispatcher,
        $paypalClientID,
        $paypalSecret
    ) {
        $this->session = $session;
        $this->router = $router;
        $this->paypalClientID = $paypalClientID;
        $this->paypalSecret = $paypalSecret;
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        
        $this->apiContext =  new ApiContext(
            new OAuthTokenCredential(
                'AYSq3RDGsmBLJE-otTkBtM-jBRd1TCQwFf9RGfwddNXWz0uFU9ztymylOhRS',
                'EGnHDxD_qRPdaLdZz8iCr8N7_MzF-YHPTkjs6NKYQvQSBngp4PTTVWkPZRbL'
            )
        );
    }
    
    /**
     * @param Distributor $distributor
     * @param Subscription $subscription
     * @return bool|Payment
     */
    public function createPayment(Distributor $distributor, Subscription $subscription)
    {
        $payer = new Payer();
        $payer
            ->setPaymentMethod('paypal')
        ;
    
        $amount = new Amount();
        $amount->setTotal($subscription->getPrice());
        $amount->setCurrency('EUR');
    
        $transaction = new Transaction();
        $transaction
            ->setAmount($amount)
            ->setCustom($subscription->getId())
            ->setDescription('Abonnement annuel.')
            ->setNoteToPayee('Contacter nous pour plus d\'informations')
        ;
    
        $redirectUrls = new RedirectUrls();
        $redirectUrls
            ->setReturnUrl($this->router->generate('payment_return', [], UrlGeneratorInterface::ABSOLUTE_URL))
            ->setCancelUrl($this->router->generate('payment_cancel', [], UrlGeneratorInterface::ABSOLUTE_URL));
    
    
        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls)
            ;
    
        try {
            $payment->create($this->apiContext);
            
            $this->session->set('cart', $subscription->getId());
            //dump($payment); exit;
            return $payment;
        }
        catch (PayPalConnectionException $ex) {
            echo $ex->getData();
            
            return false;
        }
    }
    
    /**
     * @param Request $request
     * @return bool
     */
    public function executePayment(Request $request)
    {
        $paymentId = $request->query->get('paymentId', null);
        $token = $request->query->get('token', null);
        $payerID = $request->query->get('PayerID', null);
        $cart = $this->session->get('cart');
        
        $this->session->remove('cart');
        
        if (!$paymentId || !$token || !$payerID) {
            return false;
        }
        
        $payment = Payment::get($paymentId, $this->apiContext);
    
        $execution = new PaymentExecution();
        $execution->setPayerId($payerID);
    
        $subscription = $this->em->getRepository(Subscription::class)->find($cart);
    
        if (!$subscriptionPayment = $subscription->getPayment()) {
            $subscriptionPayment = new \Honda\MainBundle\Entity\Payment();
        }
    
        $subscriptionPayment
            ->setState($payment->getState())
            ->setPaypalId($payment->getId())
            ->setCreateTime(new \DateTime())
        ;
    
        $subscription->setPayment($subscriptionPayment);
    
        try {
            $payment->execute($execution, $this->apiContext);
            
            if ('approved' == $payment->getState()) {
                
                if ($subscription) {
    
                    $subscriptionPayment->setState($payment->getState());
                    $subscription->setStatus(Subscription::STATUS_PAYMENT_ACCEPTED);
                    
                    $this->em->flush();
    
                    $this->dispatcher->dispatch(SubscriptionEvent::PAYMENT_SUCCESS, new SubscriptionEvent($subscription, $payment));
                }
                
            }
            
            return $payment;
        } catch (\Exception $e) {
            $data = \GuzzleHttp\json_decode($e->getData());
            echo $data;
        }
        
        $subscription->setStatus(Subscription::STATUS_PAYMENT_REFUSED);
        $this->em->flush();
    
        $this->dispatcher->dispatch(SubscriptionEvent::PAYMENT_ERROR, new SubscriptionEvent($subscription, $payment));
        
        return false;
    }
    
    /**
     * @return bool
     */
    public function executeCancel()
    {
        $cart = $this->session->get('cart');
    
        $this->session->remove('cart');
    
        $subscription = $this->em->getRepository(Subscription::class)->find($cart);
    
        if ($subscription) {
        
            $subscription->setStatus(Subscription::STATUS_PAYMENT_REFUSED);
            $this->em->flush();
        }
    
        $this->dispatcher->dispatch(SubscriptionEvent::PAYMENT_ERROR, new SubscriptionEvent($subscription));
    
    
        return true;
    }
    
}
