<?php

namespace Honda\MainBundle\Model\Admin;


use Honda\MainBundle\Model\AbstractBaseManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;


/**
 * Class SocialNetworkLinkManager
 * @package Honda\MainBundle\Model\Admin
 */
class SocialNetworkLinkManager extends AbstractBaseManager
{

    public function handlerForm(Request $request, FormInterface $form)
    {
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
    
            $object = $form->getData();
    
            $this->save($object);
            
            return true;
        }
    }

}
