<?php

namespace Honda\MainBundle\Model\Admin;

use Doctrine\Common\Persistence\ObjectManager;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\Subscription;
use Honda\MainBundle\Model\AbstractBaseManager;
use Honda\MainBundle\Model\Middleware\SubscriptionGateway;

class SubscriptionManager extends AbstractBaseManager
{

    /**
     * Subscription Gateway instance
     *
     * @var SubscriptionGateway
     */
    private $subscriptionGateway;

    public function __construct(ObjectManager $em, SubscriptionGateway $subscriptionGateway) {
        parent::__construct($em);
        $this->subscriptionGateway = $subscriptionGateway;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\InventoryRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(Subscription::class);
    }

    /**
     * @param Distributor $distributor
     * @param $SubscriptionId
     * @return mixed
     */
    public function find(Distributor $distributor, $SubscriptionId)
    {
        return $this->getRepository()->getSubscriptionByDistributorAndId($distributor, $SubscriptionId);
    }

    /**
     * @param Distributor $distributor
     * @return Subscription
     * @throws \Exception
     */
    public function createSubscription(Distributor $distributor)
    {
        $subscription = $this->getRepository()->getCurrentSubscriptionByDistributor($distributor);

        $now = new \Datetime();
        $endDate = clone $now;
        $endDate = $endDate->add(new \DateInterval('P1Y'));

        if (!$subscription) {

            $subscription = new Subscription();
            $subscription
                ->setDistributor($distributor)
                ->setStartDate($now)
                ->setEndDate($endDate)
                ;

            $this->save($subscription);

        } else {
            // On met à jour l'année de soubscription en fonction de la date de paiement
            $subscription
                ->setStartDate($now)
                ->setEndDate($endDate)
            ;
            $this->flush();
        }

       return $subscription;
    }

    public function update(Distributor $distributor)
    {
        $portailSubscriptions = $this->subscriptionGateway->getSubscriptions($distributor);
        $validReferences = [];
        foreach ($portailSubscriptions as $portailSubscription) {
            $validReferences[] = $portailSubscription['reference'];
            $subscription = $this->getRepository()->findOneBy([
                'distributor' => $distributor,
                'billingId' => $portailSubscription['reference']
            ]);
            if (!$subscription) {
                $subscription = new Subscription();
            }
            $subscription
                ->setBillingId($portailSubscription['reference'])
                ->setPrice($portailSubscription['price'])
                ->setYear($portailSubscription['year'])
                ->setPeriod($portailSubscription['period'])
                ->setDistributor($distributor)
                ->setInvoice($portailSubscription['invoice'])
                ->setSettlement($portailSubscription['settlement'])
            ;
            $this->persist($subscription);
        }
        $this->flush();
        $this->removeInvalidSubscription($distributor, $validReferences);

        return $this;
    }

    private function removeInvalidSubscription(Distributor $distributor, $validReferences)
    {
        $subscriptions = $this->getRepository()->findInvalidSubscription($distributor, $validReferences);
        foreach ($subscriptions as $subscription) {
            $this->em->remove($subscription);
        }

        $this->flush();
    }
}
