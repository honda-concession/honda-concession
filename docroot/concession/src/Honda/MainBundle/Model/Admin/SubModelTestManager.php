<?php

namespace Honda\MainBundle\Model\Admin;

use Honda\MainBundle\Entity\SubModelTest;
use Honda\MainBundle\Model\AbstractBaseManager;
use Honda\MainBundle\Entity\Distributor;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class SubModelTestManager
 * @package Honda\MainBundle\Model\Admin
 */
class SubModelTestManager extends AbstractBaseManager
{
    
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\SubModelTestRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(SubModelTest::class);
    }
    
    /**
     * @param Distributor $distributor
     * @param $jsonData
     * @return ArrayCollection
     */
    public function importSubModels(Distributor $distributor, $jsonData)
    {
        $collection = new ArrayCollection();
    
        $subModelTests = $this->getRepository()->findAll();
        $tab = [];
        $fromApitab = [];
    
        foreach ($subModelTests as $subModelTest) {
            if (!isset($tab[$subModelTest->getSubModelPortailId()])) {
                $tab[$subModelTest->getSubModelPortailId()] = $subModelTest;
            }
        }
        
        foreach ($jsonData as $submodelPortail) {
    
            $subModelTest = $this->getRepository()->loadByDistributorAndSubModelPortailId($distributor, $submodelPortail->id);
            
            if (!$subModelTest) {
                $subModelTest = new SubModelTest();
                $subModelTest
                    ->setSubModelPortailId($submodelPortail->id)
                    ->setDistributor($distributor)
                ;
            }
    
            $subModelTest->setSubModelPortailTitle($submodelPortail->title);
            $subModelTest->setKoraId($submodelPortail->kora_id);
            $subModelTest->setModel($submodelPortail->model);
            
            $this->persist($subModelTest);
            
            $item = ['subModelTest' => $submodelPortail, 'subModelTest' =>  $subModelTest, 'distributor' => $distributor ];
            $collection->add($item);
            $fromApitab[$submodelPortail->id] = $subModelTest->getSubModelPortailId();
        }
        
        foreach (array_diff(array_keys($tab), $fromApitab) as $key => $subModelTest) {
        
            if (isset($tab[$subModelTest])) {
                $this->em->remove($tab[$subModelTest]);
            }
        }
        
    
        $this->flush();
        
        return $collection;
    }
    
    /**
     * @param SubModelTest $subModelTest
     * @param Request $request
     * @return SubModelTest
     */
    public function saveOnTest(SubModelTest $subModelTest, Request $request)
    {
        $onTest = (bool) $request->request->get('onTest');
    
        $subModelTest->setOnTest($onTest);
        $this->save($subModelTest);
        
        return $subModelTest;
    }
    
}
