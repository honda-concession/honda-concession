<?php

namespace Honda\MainBundle\Model\Admin;


use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Entity\Section;
use Honda\MainBundle\Entity\SectionOrder;
use Honda\MainBundle\Model\AbstractBaseManager;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SectionManager
 * @package Honda\MainBundle\Model\Admin
 */
class SectionManager extends AbstractBaseManager
{
    
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\SectionRepository
     */
    public function getSectionRepository()
    {
        return $this->getEntityManager()->getRepository(Section::class);
    }
    
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Honda\MainBundle\Repository\SectionOrderRepository
     */
    public function getSectionOrderRepository()
    {
        return $this->getEntityManager()->getRepository(SectionOrder::class);
    }
    
    /**
     * @param Distributor $distributor
     * @return array|bool
     */
    public function createSectionOrder(Distributor $distributor)
    {
        $sectionOrders = $this->getSectionOrderRepository()->getAllByDistributor($distributor);
        $sections = $this->getSectionRepository()->findAll();
        $add = false;
        
        if (!count($sectionOrders)) {
            
            foreach ($sections as $section) {
                $sectionOrder = new SectionOrder();
                $sectionOrder
                    ->setDistributor($distributor)
                    ->setSection($section)
                ;
                $this->persist($sectionOrder);
            }
            $this->flush();
    
            $add = true;
            
        } elseif (count($sectionOrders) && count($sectionOrders) < count($sections)) {
            
            $sectionOrderkeys = $this->getSectionOrderRepository()->getAllByDistributorWithSectionKeys($distributor);
            $sectionKeys = $this->getSectionRepository()->getAllWithSectionKeys();
            
            $diffKeys = array_diff_key($sectionKeys, $sectionOrderkeys);
            
            foreach ($diffKeys as $sectionKey => $diffKey) {
                
                $sectionOrder = new SectionOrder();
                $sectionOrder
                    ->setDistributor($distributor)
                    ->setSection($sectionKeys[$sectionKey])
                ;
                $this->persist($sectionOrder);
            }
            $this->flush();
    
            $add = true;
        }
        
        if ($add) {
            return $sectionOrders;
        }
        
        return false;
    }
    
    /**
     * @param Distributor $distributor
     * @param Request $request
     * @return bool
     */
    public function handlerSortable(Distributor $distributor, Request $request)
    {
        $sectionOrderQueries = $request->request->get('section-item');
        
        foreach ($sectionOrderQueries as $key => $sectionOrderQuery) {
            
            $sectionOrder = $this->getSectionOrderRepository()->find($sectionOrderQuery);
            $sectionOrder->setPosition($key);
            $this->persist($sectionOrder);
        }
        
        $this->flush();
        
        return true;
    }
    
}
