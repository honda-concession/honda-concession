<?php

namespace Honda\MainBundle\Model\Admin;


use Honda\MainBundle\Model\AbstractBaseManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Model\Middleware\GenericGateway;
use Honda\MainBundle\Service\MediaUrl;
use Honda\MainBundle\Entity\Accessory;
use Honda\MainBundle\Entity\AccessoryItem;

/**
 * Class AccessoryManager
 * @package Honda\MainBundle\Model\Admin
 */
class AccessoryManager extends AbstractBaseManager
{

    /**
     * @var GenericGateway
     */
    protected $genericGateway;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var MediaUrl
     */
    protected $mediaUrl;

    /**
     * @var array
     */
    protected $endpoints;

    /**
     * SliderManager constructor.
     * @param ObjectManager $em
     * @param GenericGateway $genericGateway
     * @param MediaUrl $mediaUrl
     * @param array $endpoints
     * @param SettingManager $settingManager
     */
    public function __construct(
        ObjectManager $em,
        ContainerInterface $container,
        GenericGateway $genericGateway,
        $endpoints = []
    ) {
        parent::__construct($em);

        $this->genericGateway = $genericGateway;
        $this->container = $container;
        $this->endpoints = $endpoints;
    }


    public function updateItems(Accessory $accessory)
    {
        $this->em->persist($accessory);
        $this->em->flush();
        foreach ($this->getCategories() as $category) {
            $exist = $accessory->getAccessoryItems()->exists(function($key, $item) use ($category) {
                return $item->getTitle() == $category['title'];
            });
            if (!$exist) {
                $item = new AccessoryItem();
            } else {
                $item = $accessory->getAccessoryItems()->filter(function($item) use ($category) {
                    return $item->getTitle() == $category['title'];
                })->first();
            }
            $item
                ->setTitle($category['title'])
                ->setAccessory($accessory)
                ->setLogoLinks($category['logos'])
                ->setIcon($category['icon'])
            ;

            $this->em->persist($item);
        }
        foreach ($accessory->getAccessoryItems() as $item) {
            $found = false;
            foreach ($this->getCategories() as $category) {
                if ($item->getTitle() == $category['title']) {
                    $found = true;
                }
            }
            if (!$found) {
                $this->em->remove($item);
            }
        }
        $this->em->flush();

        return $this;
    }

    public function getCategories()
    {
        $endpoint = $this->endpoints['category'];

        return  $this->genericGateway->getData($endpoint, true);
    }

    public function handlerForm(Request $request, FormInterface $form)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $this->isRequestValid($request)) {
            $accessory = $form->getData();
            $this->setLogoValues($request, $accessory);
            $this->save($accessory);

            return true;
        }

        return false;
    }

    private function setLogoValues(Request $request, $accessory)
    {
        foreach($request->request->all() as $key => $parameter) {
            if (!preg_match('/Logo$/', $key)) {
                continue;
            }
            $formLogos = $request->request->get($key);
            $title = preg_replace('/Logo$/', null, $key);
            foreach($accessory->getAccessoryItems() as $item) {
                if ($item->getTitle() == $title) {
                    $item->setLogos($formLogos);
                }
            }
        }
    }

    private function isRequestValid(Request $request)
    {
        foreach($request->request->all() as $key => $parameter) {
            if (preg_match('/Logo$/', $key)) {
                return true;
            }
        }

        return false;
    }

}
