<?php

namespace Honda\MainBundle\Model\Admin;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Model\Middleware\DistributorGateway;
use Honda\MainBundle\Model\AbstractBaseManager;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class DistributorManager
 * @package Honda\MainBundle\Model\Admin
 */
class DistributorManager extends AbstractBaseManager
{
    
    /**
     * @var DistributorGateway
     */
    protected $distributorGateway;
    
    /**
     * DistributorManager constructor.
     * @param ObjectManager $em
     * @param DistributorGateway $distributorGateway
     */
    public function __construct(
        ObjectManager $em,
        DistributorGateway $distributorGateway
    ) {
        
        parent::__construct($em);
        $this->distributorGateway = $distributorGateway;
    }
    
    /**
     * @return mixed
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository(Distributor::class);
    }
    
    /**
     * @param $object
     * @return bool|void
     */
    public function saveDistributorInfosFromPortail($object)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $distributor = null;
        
            if ($object instanceof User) {
                $distributor = $object->getDistributor();
            } elseif ($object instanceof Distributor) {
                $distributor = $object;
            } else {
                @trigger_error('Object instance of User|Distributor is required');
            }
            
            $infos = $this->distributorGateway->getDistributorInfos($distributor);

            if ($distributor instanceof Distributor && is_array($infos)) {
                
                $distributor
                    ->setInfos($infos)
                    ->setDepartment($propertyAccessor->getValue($infos, '[department]'))
                    ->setLocation($propertyAccessor->getValue($infos, '[location]'))
                    ->setRegion($propertyAccessor->getValue($infos, '[region]'))
                    ->setTown($propertyAccessor->getValue($infos, '[town]'))
                ;
                
                $this->save($distributor);
    
                return true;
            }
            
        return;
    }
    
    
}
