<?php

namespace Honda\MainBundle\Service;


use Sonata\MediaBundle\Model\MediaInterface;
use Symfony\Component\DependencyInjection\Container;

class MediaUrl
{
    /**
     * @var Container
     */
    protected $container;
    
    /**
     * MediaUrl constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
    
    /**
     * @param MediaInterface $media
     * @param string $format
     * @return mixed
     */
    public function getUrl(MediaInterface $media, $format = 'reference')
    {
        $container = $this->container;
    
        $provider = $container->get($media->getProviderName());
        
        $format = $provider->getFormatName($media, $format);
        $url = $provider->generatePublicUrl($media, $format);
    
        return $url;
    }
}
