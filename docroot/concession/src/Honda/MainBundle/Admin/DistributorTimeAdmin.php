<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type As Type;
use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class DistributorTimeAdmin
 * @package Honda\MainBundle\Admin
 */
class DistributorTimeAdmin extends AbstractDistributor
{
    
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];
    
    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        if (isset($actions['delete'])) {
            unset($actions['delete']);
        }
        
        return $actions;
    }
    
    /**
     * {@inheritdoc}
     *
     * NEXT_MAJOR: return null to indicate no override
     */
    public function getExportFormats()
    {
        return [];
    }
    
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
    
            ->with('config.content',  ['class' => 'col-md-6'])
                ->add('title', Type\TextType::class)
            ->end()
            
            ->with('config.meta_data',  ['class' => 'col-md-6  hour-minute-choice'])
                ->add('startHour', Type\TimeType::class, [])
                ->add('endHour', Type\TimeType::class, [])
            ->end()
        ;
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        ;
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', '' , ['label' => 'Titre'])
            ->add('startHourFormatted', '' , ['label' => 'Heure début'])
            ->add('endHourFormatted', '' , ['label' => 'Heure fin'])
            ->add('_action', null, [
                'actions' => [
                    'move' => [
                        'template' => '@PixSortableBehavior/Default/_sort.html.twig'
                    ],
                ]
            ])
            /*
            ->add('_action', null, array(
                'actions' => array(
                    'move' => array(
                        'template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig',
                    ),
                ),
            ))
            */
        ;
    }
    
}
