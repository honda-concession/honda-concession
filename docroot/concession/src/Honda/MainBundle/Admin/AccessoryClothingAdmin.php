<?php

namespace Honda\MainBundle\Admin;


use Sonata\AdminBundle\Route\RouteCollection;
use Honda\MainBundle\Admin\Base\AbstractDistributor;


/**
 * Class AccessoryClothingAdmin
 * @package Honda\MainBundle\Admin
 */
class AccessoryClothingAdmin extends AbstractDistributor
{
    
    protected $baseRoutePattern = 'accessory-clothing';
    protected $baseRouteName = 'accessory-clothing';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
