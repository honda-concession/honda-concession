<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Honda\MainBundle\Admin\Base\AbstractBaseAdmin;


class BillingDocumentsAdmin extends AbstractBaseAdmin
{

    protected $baseRoutePattern = 'subscription-docs';
    protected $baseRouteName = 'subscription-docs';
    
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        
        $listMapper
            ->addIdentifier('name', null, ['label' => "Vos documents",'template' => '@HondaMain/Admin/CRUD/Subscription/document_action.html.twig'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

                ->add('name', null, ['label' => 'Nom du document'])

                ->add('pdf', ModelListType::class, [
                        'label' => 'Document',
                    ], [
                        'link_parameters' => ['context' => 'service_pdf']
                    ]
                )
            ->end()
        ;
    }
    
}
