<?php

namespace Honda\MainBundle\Admin;

use Honda\MainBundle\Form\Type\PictoServicesChoiceType;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Honda\MainBundle\Admin\Base\AbstractBaseAdmin;
use Honda\MainBundle\Admin\Base\Traits\SetDistributorTrait;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;

/**
 * Class ServiceAdmin
 * @package Honda\MainBundle\Admin
 */
class ServiceAdmin extends AbstractBaseAdmin
{
    use SetDistributorTrait,
        Traits\ActivationAdminTrait,
        Traits\RedirectionAdminTrait;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    /**
     * @inheritdoc
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
        parent::configureRoutes($collection);
    }

    /**
     * {@inheritdoc}
     *
     * NEXT_MAJOR: return null to indicate no override
     */
    public function getExportFormats()
    {
        return [];
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add('position', 'actions', array(
                'actions' => array(
                    'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig')
                )
            ))
            ->add('picto', 'string', array('template' => '@templates/Admin/MediaAdmin/list_image_src.html.twig'))
            ->addIdentifier('title', null, ['label' => "Nom du service"])
            ->add('published', null, ['editable' => true])
            ->add('publishedOnHome', null, ['editable' => true])
            ->add('startDate', 'datetime', ['format'=>'d/m/Y'])
            ->add('endDate', 'datetime', ['format'=>'d/m/Y'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->with('Ajout d\'un nouveau service',  ['class' => 'col-md-8'])
                ->add('title')
                ->add('content', CKEditorType::class, [
                        'required' => false,
                        'config_name' => 'full'
                    ]
                )
                ->add('image', ModelListType::class, [
                    'label' => 'Image',
                    'required' => true
                        ], [
                    'link_parameters' => [
                        'context' => 'service_page'
                    ]
                ])
                ->add('picto', PictoServicesChoiceType::class, [
                        'required' => true,
                        'label' => 'Choisir un picto',
                        'expanded' => true,
                        'attr' => ["class" => 'services-picto']
                    ]
                )
                ->add('pdf', ModelListType::class, [
                        'label' => 'config.label_pdf',
                        'required' => false,
                        'btn_delete' => false,
                        'btn_edit' => false
                    ], [
                        'link_parameters' => ['context' => 'service_pdf']
                    ]
                )
                ->add('link', null, array(
                    'label' => 'Lien contact'
                ))

            ->end()
            ->with('Activation', ['class' => 'col-md-4'])
                ->add('startDate', DateTimePickerType::class, ['format'=>'dd/MM/yyyy H:mm:ss'])
                ->add('endDate', DateTimePickerType::class, [ 'format'=>'dd/MM/yyyy H:mm:ss'])
                ->add('published')
                ->add('publishedOnHome', null, [
                    'label' => 'Publier sur la page d\'accueil',
                    'required' => false,
                ])
            ->end()
        ;
        $this->addRedirectionBlock($formMapper, 'col-md-4');
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('content')
            ->add('published')
            ->add('position')
        ;
    }

    /**
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $queryBuilder = $query->getQueryBuilder();

        $rootAlias = current($queryBuilder->getRootAliases());

        $queryBuilder->andWhere($queryBuilder->expr()->orX(
            $queryBuilder->expr()->eq($rootAlias . '.distributor', ':distributor')
        ));

        $queryBuilder->setParameter('distributor', $this->getDistributor());

        return $query;
    }
}
