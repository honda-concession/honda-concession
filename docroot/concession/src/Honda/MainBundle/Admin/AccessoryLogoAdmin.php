<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Honda\MainBundle\Admin\Base\AbstractDistributor;

/**
 * Class AccessoryLogoAdmin
 * @package Honda\MainBundle\Admin
 */
class AccessoryLogoAdmin extends AbstractDistributor
{

    protected $baseRoutePattern = 'accessory-logo';
    protected $baseRouteName = 'accessory-logo';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
}
