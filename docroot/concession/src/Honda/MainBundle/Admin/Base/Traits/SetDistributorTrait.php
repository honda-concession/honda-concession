<?php

namespace Honda\MainBundle\Admin\Base\Traits;


/**
 * Trait SetDistributorTrait
 * @package Honda\MainBundle\Admin\Base\Traits
 */
trait SetDistributorTrait
{

    /**
     * @inheritdoc
     */
    public function prePersist($object)
    {
        if (method_exists($object, 'setDistributor')) {
            $object->setDistributor($this->getDistributor());
        }

        @trigger_error('Verifier que la methode "setDistributor" existe', E_USER_WARNING);
    }

    /**
     * @inheritdoc
     */
    public function preUpdate($object)
    {
        if (method_exists($object, 'setDistributor')) {
            $object->setDistributor($this->getDistributor());
        }

        @trigger_error('Verifier que la methode "setDistributor" existe', E_USER_WARNING);
    }
}
