<?php

namespace Honda\MainBundle\Admin\Base\Traits;

use Sonata\MediaBundle\Model\MediaInterface;

/**
 * Trait MediaTrait
 * @package Honda\MainBundle\Entity\Traits
 */
trait MediaTrait
{
    
    /**
     * @param $contextSize
     * @param MediaInterface|null $media
     * @return bool|string
     */
    public function getImageUrl($contextSize,  MediaInterface $media = null)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $host = $container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost();
        
        if ($media) {
            $provider = $container->get($media->getProviderName());
            $format = $provider->getFormatName($media, $contextSize);
            
            return $host .  $provider->generatePublicUrl($media, $format);
        }
        
        return false;
    }
}
