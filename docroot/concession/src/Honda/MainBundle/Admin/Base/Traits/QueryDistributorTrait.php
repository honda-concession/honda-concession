<?php

namespace Honda\MainBundle\Admin\Base\Traits;


/**
 * Trait DistributorPersistTrait
 * @package Honda\MainBundle\Admin\Base\Traits
 */
trait QueryDistributorTrait
{
    /**
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        
        $queryBuilder = $query->getQueryBuilder();
        
        $rootAlias = current($queryBuilder->getRootAliases());
        
        $queryBuilder->andWhere($rootAlias . '.distributor = :distributor');
        $queryBuilder->setParameter('distributor', $this->getDistributor());
        
        return $query;
    }
}
