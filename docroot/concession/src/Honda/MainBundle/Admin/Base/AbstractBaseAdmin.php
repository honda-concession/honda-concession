<?php

namespace Honda\MainBundle\Admin\Base;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class BaseAdmin
 * @package Honda\MainBundle\Admin
 */
abstract class AbstractBaseAdmin extends AbstractAdmin
{
    
    use Traits\MediaTrait;
    
    /**
     * @return mixed
     */
    public function getUser()
    {
        $token = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken();
        
        if ($token) {
            return $token->getUser();
        }
    
        if (php_sapi_name() <> "cli") {
            @trigger_error('logged user is required', E_USER_ERROR);
        }
    }
    
    /**
     * @return mixed
     */
    public function getDistributor()
    {
        
        if ($this->getUser() instanceof UserInterface) {
            
            if ($distributor = $this->getUser()->getDistributor()) {
                return $distributor;
            }
        }
        
        if (php_sapi_name() <> "cli") {
            @trigger_error('Distributor is required', E_USER_ERROR);
        }
        
    }
    
}
