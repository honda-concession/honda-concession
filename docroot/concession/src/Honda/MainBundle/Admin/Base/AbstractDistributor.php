<?php

namespace Honda\MainBundle\Admin\Base;



/**
 * Class AbstractDistributor
 * @package Honda\MainBundle\Admin
 */
abstract class AbstractDistributor extends AbstractBaseAdmin
{
   use Traits\SetDistributorTrait,
       Traits\QueryDistributorTrait;

}
