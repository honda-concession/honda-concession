<?php

namespace Honda\MainBundle\Admin;


use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class StatsAdmin
 * @package Honda\MainBundle\Admin
 */
class StatsAdmin extends AbstractDistributor
{
    protected $baseRoutePattern = 'stats';
    protected $baseRouteName = 'stats';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
