<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;


/**
 * Class BgColorAdmin
 * @package Honda\MainBundle\Admin
 */
class BgColorAdmin extends AbstractAdmin
{
    
    protected $baseRoutePattern = 'bg-color';
    protected $baseRouteName = 'bg-color';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
