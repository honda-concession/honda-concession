<?php

namespace Honda\MainBundle\Admin\Traits;

use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Trait ActivationAdminTrait
 */
trait ActivationAdminTrait
{
    protected function configureBatchActions($actions)
    {
        $actions['published'] = [
            'ask_confirmation' => false,
            'label' => 'Publier',
        ];
        $actions['unpublished'] = [
            'ask_confirmation' => false,
            'label' => 'Dépublier',
        ];

        return $actions;
    }
}
