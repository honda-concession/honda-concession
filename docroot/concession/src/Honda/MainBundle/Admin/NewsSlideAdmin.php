<?php

namespace Honda\MainBundle\Admin;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;

class NewsSlideAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'combination-video';

    /**
     * @inheritdoc
     */
    public function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('position', HiddenType::class)
            ->add('title', TextType::class, [
                    'required' => false
                ]
            )
            ->add('image', ModelListType::class, [
                    'label' => 'Image',
                    'by_reference' => false,
                    'btn_delete' => false,
                    'btn_edit' => false
                ],
                [
                    'link_parameters' => [
                        'context' => 'news', 
                        'filter' => [
                            'context' => [
                                'value' => 'news'
                            ]
                        ]
                    ]
                ]
            )
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureListFields(ListMapper $listMapper)
    {
    }

    /**
     * @inheritdoc
     */
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    /**
     * @inheritdoc
     */
    public function toString($object)
    {
        return '';
    }
}