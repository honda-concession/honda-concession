<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;


/**
 * Class DistributorInfosAdmin
 * @package Honda\MainBundle\Admin
 */
class DistributorInfosAdmin extends AbstractAdmin
{
    
    protected $baseRoutePattern = 'distributor-infos';
    protected $baseRouteName = 'distributor-infos';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
