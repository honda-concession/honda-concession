<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;


/**
 * Class GoogleAnalyticAdmin
 * @package Honda\MainBundle\Admin
 */
class GoogleAnalyticAdmin extends AbstractAdmin
{
    
    protected $baseRoutePattern = 'google-analytic';
    protected $baseRouteName = 'google-analytic';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
