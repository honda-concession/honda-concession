<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;

use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class LogoAdmin
 * @package Honda\MainBundle\Admin
 */
class LogoAdmin extends AbstractAdmin
{
    
    protected $baseRoutePattern = 'logo';
    protected $baseRouteName = 'logo';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
