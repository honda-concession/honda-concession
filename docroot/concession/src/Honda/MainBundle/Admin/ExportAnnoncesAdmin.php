<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;


/**
 * Class ExportAnnoncesAdmin
 * @package Honda\MainBundle\Admin
 */
class ExportAnnoncesAdmin extends AbstractAdmin
{
    
    protected $baseRoutePattern = 'annonces';
    protected $baseRouteName = 'annonces';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        $collection->add('export');
    }
    
}
