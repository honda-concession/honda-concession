<?php

namespace Honda\MainBundle\Admin;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type As Type;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Honda\MainBundle\Entity\News;
use Honda\MainBundle\Model\Front\NewsManager;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class NewsAdmin
 * @package Honda\MainBundle\Admin
 */
class NewsAdmin extends AbstractDistributor
{

    use Traits\ActivationAdminTrait,
        Traits\RedirectionAdminTrait;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    /**
     * @inheritdoc
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    public $choices = [];

    /**
     * @var array
     */
    public $choicesFlipped = [];

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        $types = $this->container->get(NewsManager::class)->getNewsCategories();
        foreach ($types as $type) {
            $this->choices[$type['name']] = $type['id'];
        }
        $this->choicesFlipped = array_flip($this->choices);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Actualité',  ['class' => 'col-md-8'])
                ->add('title', Type\TextType::class)
                ->add('type', Type\ChoiceType::class, [
                        'choices' => $this->choices,
                    ]
                )
                ->add('description1', CKEditorType::class, [
                        'config_name' => 'simple',
                    ]
                )
                ->add('description2', CKEditorType::class, [
                        'required' => false,
                        'config_name' => 'default',
                        'config' => [
                            'stylesSet' => 'eidtor_styles'
                        ]
                    ]
                )
                ->add('slides', CollectionType::class,
                    [
                        'label' => 'Slideshow',
                        'required' => false,
                        'by_reference' => false,
                    ],
                    [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                        'limit' => 6
                    ]
                )
                ->add('description3', CKEditorType::class, [
                        'required' => false,
                        'config_name' => 'default',
                        'config' => [
                            'stylesSet' => 'eidtor_styles'
                        ]
                    ]
                )
            ->end()
            ->with('Dates auxquelles cette actualité sera visible sur votre site',  ['class' => 'col-md-4'])
                ->add('startDate', DateTimePickerType::class, ['format'=>'dd/MM/yyyy H:mm:ss'])
                ->add('endDate', DateTimePickerType::class, [ 'format'=>'dd/MM/yyyy H:mm:ss'])
                ->add('published', null, [
                        'label' => 'Publié',
                        'required' => false,
                    ]
                )
                ->add('publishedOnHome', null, [
                    'label' => 'Publier sur la page d\'accueil',
                    'required' => false,
                ])
            ->end()
            ->with('Image principale', ['class' => 'col-md-4'])
                ->add('image', ModelListType::class, [
                        'label' => 'config.label_image',
                        'btn_delete' => false,
                        'btn_edit' => false
                    ], [
                        'link_parameters' => ['context' => 'news']
                    ]
                )
            ->end()
        ;
        $this->addRedirectionBlock($formMapper, 'col-md-4');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('published')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('position', 'actions', array(
                'actions' => array(
                    'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig')
                )
            ))
            ->addIdentifier('title')
            ->add('published', null, ['editable' => true])
            ->add('publishedOnHome', null, ['editable' => true])
            ->add('startDate', 'datetime', ['format'=>'d/m/Y'])
            ->add('endDate', 'datetime', ['format'=>'d/m/Y'])
            ->add('type', 'string', ['template' => '@HondaMain/Admin/CRUD/News/list_translate.html.twig', 'label' => 'Catégorie de l\'actualité'])
        ;
    }
}
