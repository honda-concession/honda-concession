<?php

namespace Honda\MainBundle\Admin;

use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Honda\MainBundle\Form\Type\VoCheckboxType;
use Honda\MainBundle\Model\Middleware\PortailWrapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type As Type;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

use Honda\MainBundle\Entity\VehicleAd;

class VehicleAdAdmin extends AbstractDistributor
{
    use Traits\ActivationAdminTrait,
        Traits\RedirectionAdminTrait;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    public $supportsPreviewMode = true;

    /**
     * @var PortailWrapper
     */
    protected $portailWrapper;

    /**
     * VehicleAdAdmin constructor.
     * @param $code
     * @param $class
     * @param $baseControllerName
     * @param PortailWrapper $portailWrapper
     */
    public function __construct(
        $code,
        $class,
        $baseControllerName,
        PortailWrapper $portailWrapper
    ) {
        parent::__construct($code, $class, $baseControllerName);

        $this->portailWrapper = $portailWrapper;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('price')
            ->add('modelYear')
            ->add('releaseDate')
            ->add('warrant')
            ->add('modelPortail')
            ->add('categoryPortail')
            ->add('markPortail')
            ->add('published')
        ;
    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('position', 'actions', array(
                    'actions' => array(
                        'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig', 'enable_top_bottom_buttons' => true)
                    )
                ))
                ->addIdentifier('image', 'string', ['template' => '@HondaMain/Admin/CRUD/VehicleAd/list_image.html.twig', 'label' => 'Image principale'])
                ->addIdentifier('title')
                ->add('published', null, ['editable' => true])
                ->add('publishedOnHome', null, ['editable' => true])
                ->add('price')
                ->add('km')
                ->add('modelYear')
                ->add('releaseDate', 'datetime', ['format' => 'd/m/Y'])
                ->add('warrant')
                ->add('modelPortail')
                ->add('categoryPortail')
                ->add('markPortail')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $years = [];

        foreach (array_reverse(range(1900, date('Y'))) as $year) {
            $years[$year] = $year;
        }
        $builder = $formMapper->getFormBuilder();
        $categories = $this->portailWrapper->getCategoryGateway()->getCategories();
        $marks = $this->portailWrapper->getMarkGateway()->getMarks();
        $topMarksData = [];
        $otherMarksData = [];
        $topMarks['Top marques'] = [];
        $otherMarks['Autres marques'] = [];

        foreach ($marks as $mark) {
            if ($mark['topmark']) {
                $topMarksData[$mark['title']] = $mark['title'];
            } else {
                $otherMarksData[$mark['title']] = $mark['title'];
            }
        }

        $topMarks['Top marques'] = $topMarksData;
        $otherMarks['Autres marques'] = $otherMarksData;
        $marks = array_merge($topMarks, $otherMarks);
        $models = $this->portailWrapper->getModelGateway()->getModels();
        $colors = $this->portailWrapper->getVoGateway()->getColors();
        $infoComplementaires = $this->portailWrapper->getVoGateway()->getData('infoComplementairesVo');
        $plusProduits = $this->portailWrapper->getVoGateway()->getData('plusProduitsVo');
        $modelApiEndpoint = $this->portailWrapper->getModelGateway()->getEndpoints()['list'] ?? null;
        $modelPortail = $this->getSubject() ? $this->getSubject()->getModelPortail() : null;
        $distributorPortail = $this->portailWrapper->getDistributorGateway()->getDistributorInfos($this->getDistributor());
        $diffusion = $distributorPortail['diffusion'] ?? 0;
        $quota = $diffusion['Le Bon Coin'] ?? 0;
        $publishedOnLeBonCoinHelp = 'A noter que si vous avez atteint votre quota d’annonces simultanées sur LeBonCoin et que vous cochez la case pour publication, cette annonce prendra la place de l’annonce la plus ancienne qui disparaitra du site LeBonCoin.';

        $formMapper
                ->tab('Information')
                    ->with('Ajout d\'un vehicle d\'occasion', ['class' => 'col-md-8'])
                        ->add('title', null, [
                            'label' => 'Nom du véhicule'
                                ]
                        )
                        ->add('demoVehicle')
                        ->add('drivingLicenseA2')
                        ->add('price', null, [
                            'label' => 'Prix (en €)'
                        ])
                        ->add('km', null, [
                            'label' => 'kilométrage (en km)'
                                ]
                        )
                        ->add('cylinder', null, [
                            'label' => 'Cylindrée (en cm3)'
                                ]
                        )
                        ->add('specEnergie', null, [
                            'label' => 'Energie',
                            'data' => 'Essence'
                                ]
                        )
                        ->add('specBoiteVitesse', null, [
                            'label' => 'Boîte de vitesse ',
                            'attr' => ['disabled' => true]
                                ]
                        )
                        ->add('color', Type\ChoiceType::class, [
                                'label' => 'Couleur',
                                'choices' => $colors,
                            ]
                        )
                        ->add('distributorDescription', Type\TextareaType::class, [
                            'required' => false,
                            'attr' => ['rows' => 5]
                                ]
                        )
                        ->add('warrant', Type\ChoiceType::class, [
                                'choices' => range(0, 100)
                            ]
                        )
                        ->add('vehicleAdDesc', CKEditorType::class, [
                                'config_name' => 'simple',
                                'label' => 'Descriptif'
                            ]
                        )
                    ->end()
                    ->with('Équipements - Saisissez avec des virgules entre chaque équipement.', ['class' => 'col-md-4'])
                        ->add('options')
                        ->add('modelYear', Type\ChoiceType::class, [
                                'choices' => $years,
                            ]
                        )
                        ->add('releaseDate', DatePickerType::class, [
                                'format' => 'dd/MM/yyyy'
                            ]
                        )
                        ->add('categoryPortail', Type\ChoiceType::class, [
                                'placeholder' => 'Choississez un type',
                                'label' => 'Type *',
                                'required' => false,
                                'choices' => count($categories) ? $categories : [],
                                'attr' => ['class' => 'categoriesVo']
                            ]
                        )
                        ->add('markPortail', Type\ChoiceType::class, [
                                'placeholder' => 'Choississez une marque',
                                'label' => 'Marque *',
                                'choices' => $marks,
                                'required' => false,
                                'attr' => ['class' => 'marksVo', 'data-url' => $modelApiEndpoint,]
                            ]
                        )
                        ->add('modelPortail', Type\ChoiceType::class, [
                                'placeholder' => 'Choississez un modèle',
                                'label' => 'Modèle *',
                                'choices' => $models,
                                'required' => false,
                                'attr' => [
                                    'class' => 'modelsVo',
                                    'data-uniqid' => $this->getUniqid() . '_modelPortail',
                                    'data-modelPortail' => $modelPortail,
                                ]
                            ]
                        )
                        ->add('published', null, ['attr' => ['required' => false]])
                        ->add('publishedOnHome', null, [
                            'label' => 'Publier sur la page d\'accueil',
                            'required' => false,
                        ])
                    ->end();
                    $this->addRedirectionBlock($formMapper, 'col-md-4');
                    $formMapper->with('Information complémentaire – Un seul choix possible - Facultatif', ['class' => 'col-md-4'])
                        ->add('infosComplementaires', Type\ChoiceType::class, [
                                 'required' => false,
                                 'label' => '',
                                 'expanded' => true,
                                 'multiple' => false,
                                 'choices' => $infoComplementaires,
                                 'attr' => ['class' => 'infoComplementaires']
                             ]
                         )
                    ->end()
                    ->with('Les + du véhicule d’occasion – Plusieurs choix possibles - Facultatif', ['class' => 'col-md-8'])
                        ->add('plusProduits', VoCheckboxType::class, [
                                'required' => false,
                                'label' => '',
                                'object' => $this->getSubject(),
                                'checkboxes' => $plusProduits,
                                'checkboxes_type' => 'plusProduits',
                            ]
                        )
                    ->end()
                    ->with("Vous avez un quota d’annonces simultanées de {$quota} sur LeBonCoin.", ['class' => 'col-md-8'])
                        ->add('publishedOnLeBonCoin', null, [
                            'label' => 'Publier sur « LeBoncoin » ?',
                            'required' => false,
                            'help' => $publishedOnLeBonCoinHelp
                        ])
                    ->end()
                ->end()
                ->tab('Galerie')
                    ->with(false, ['class' => 'col-md-12 galleriesVo'])
                        ->add('vehicleAdGalleries', CollectionType::class, [
                                'label' => ' ',
                                'attr' => [ 'id' => 'galleries'],
                                'required' => false,
                                'by_reference' => false,
                                'btn_add' => false,
                                'allow_extra_fields' => true
                            ], [
                                'edit' => 'inline',
                                'inline' => 'table',
                                'sortable' => 'position',
                                'allow_delete' => true,
                                'delete_empty' => true,
                            ])
                    ->end()
                ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('id')
                ->add('title')
                ->add('description')
                ->add('price')
                ->add('km')
                ->add('distributorDescription')
                ->add('modelYear')
                ->add('releaseDate')
                ->add('warrant')
                ->add('modelPortail')
                ->add('categoryPortail')
                ->add('markPortail')
        ;
    }

    /**
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $queryBuilder = $query->getQueryBuilder();

        $rootAlias = current($queryBuilder->getRootAliases());

        $queryBuilder->addSelect('images');
        $queryBuilder->leftJoin($rootAlias . '.vehicleAdGalleries', 'images', 'WITH', 'images.isMainImage = true');
        $queryBuilder->addOrderBy($rootAlias . '.position', 'ASC');
        $queryBuilder->addOrderBy($rootAlias . '.createdAt', 'DESC');

        return $query;
    }

    public function getFormTheme()
    {
        return array_merge(
                parent::getFormTheme(), array('@HondaMain/Admin/CRUD/VehicleAd/form_admin_fields.html.twig')
        );
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'preview':
                return '@HondaMain/Admin/CRUD/VehicleAd/preview.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($object)
    {
        parent::prePersist($object);

        $object->setPlusProduits($this->getRequest()->get('plusProduits', []));
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);

        $object->setPlusProduits($this->getRequest()->get('plusProduits', []));
    }

    /**
     * use PostPersist method to set VehicleAd reference
     *
     * @param mixed $object
     * @return void
     */
    public function postPersist($object)
    {
        if ($object instanceof VehicleAd) {
            $object->setReference();
            $this->getModelManager()->update($object);
        }
    }
}
