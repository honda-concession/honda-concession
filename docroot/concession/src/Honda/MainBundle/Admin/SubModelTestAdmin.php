<?php

namespace Honda\MainBundle\Admin;


use Sonata\AdminBundle\Route\RouteCollection;
use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;


/**
 * Class SubModelTestAdmin
 * @package Honda\MainBundle\Admin
 */
class SubModelTestAdmin extends AbstractDistributor
{
    
    protected $baseRoutePattern = 'submodel-test';
    protected $baseRouteName = 'submodel-test';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        $collection->add('ajaxToggleOnTest', $this->getRouterIdParameter().'/ajax-ajaxToggle-onTest', [], [], [], '', [], ['POST']);
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        if (isset($actions['delete'])) {
            unset($actions['delete']);
        }
        
        return $actions;
    }

    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('subModelPortailId', null, ['label' => 'Id'])
            ->add('subModelPortailTitle', null, ['label' => 'Sous modèle'])
            ->add('onTest_handler', 'string', ['label' => 'Gestion essai',  'template' => '@templates/Admin/CRUD/onTest_checkbox.html.twig'])
        ;
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('subModelPortailId', null, ['label' => 'Id'])
            ->add('subModelPortailTitle', null, ['label' => 'Sous modèle'])
            ->add('onTest', null, ['label' => 'actif'])
        ;
    }
    
    /**
     * {@inheritdoc}
     *
     * NEXT_MAJOR: return null to indicate no override
     */
    public function getExportFormats()
    {
        return [];
    }
    
}
