<?php

namespace Honda\MainBundle\Admin;


use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class AccesAdmin
 * @package Honda\MainBundle\Admin
 */
class AccesAdmin extends AbstractDistributor
{
    protected $baseRoutePattern = 'acces';
    protected $baseRouteName = 'acces';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
