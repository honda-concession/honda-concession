<?php

namespace Honda\MainBundle\Admin;

use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;


class ServicePageAdmin extends AbstractDistributor
{
    
    protected $baseRoutePattern = 'servicepage';
    protected $baseRouteName = 'servicepage';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
