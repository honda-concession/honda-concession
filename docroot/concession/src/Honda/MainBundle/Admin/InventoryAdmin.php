<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

/**
 * Class InventoryAdmin
 * @package Honda\MainBundle\Admin
 */
class InventoryAdmin extends AbstractDistributor
{
    
    protected $baseRoutePattern = 'inventory';
    protected $baseRouteName = 'inventory';
    
    
    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        if (isset($actions['delete'])) {
            unset($actions['delete']);
        }
        
        return $actions;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        
        $collection->add('ajaxToggleInStock', $this->getRouterIdParameter().'/ajax-toggleInStock', [], [], [], '', [], ['POST']);
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('image', 'string', ['template' => '@templates/Admin/CRUD/url_image.html.twig'])
            ->add('newVehicleTitle', null, ['label' => 'Véhicule'])
            ->add('newVehicleCategory', null, ['label' => 'Categorie'])
            ->add('newVehicleModel', null, ['label' => 'Modèle'])
            ->add('newVehicleSubmodel', null, ['label' => 'Sous-modèle'])
            ->add('newVehicleColor', null, ['label' => 'Couleur'])
            ->add('newVehiclePrice', null, ['label' => 'Prix'])
            ->add('colorKoraId', null, ['label' => 'kora Color Id'])
            ->add('colorKoraCode', null, ['label' => 'kora Color Code'])
            ->add('colorKoraName', null, ['label' => 'kora Color Name'])
            ->add('inventory_handler', 'string', ['label' => 'Gestion stock (coché / décoché)',  'template' => '@templates/Admin/CRUD/inventory_checkbox.html.twig'])
        ;
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('newVehicleTitle', null, ['label' => 'Véhicule'])
            ->add('newVehicleCategory', null, ['label' => 'Categorie'])
            ->add('newVehicleModel', null, ['label' => 'Modèle'])
            ->add('newVehicleSubmodel', null, ['label' => 'Sous-modèle'])
            ->add('newVehicleColor', null, ['label' => 'Couleur'])
            ->add('newVehiclePrice', null, ['label' => 'Prix'])
            ->add('inStock', null, ['label' => 'En stock'])
        ;
    }
    
    /**
     * {@inheritdoc}
     *
     * NEXT_MAJOR: return null to indicate no override
     */
    public function getExportFormats()
    {
        return [];
    }
    
}
