<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;

use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class SeoContentAdmin
 * @package Honda\MainBundle\Admin
 */
class SeoContentAdmin extends AbstractAdmin
{
    
    protected $baseRoutePattern = 'seo-content';
    protected $baseRouteName = 'seo_content';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
