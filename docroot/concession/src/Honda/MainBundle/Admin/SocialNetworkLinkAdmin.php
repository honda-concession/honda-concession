<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;


/**
 * Class SocialNetworkLinkAdmin
 * @package Honda\MainBundle\Admin
 */
class SocialNetworkLinkAdmin extends AbstractAdmin
{
    
    protected $baseRoutePattern = 'social-network-link';
    protected $baseRouteName = 'social-network-link';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
