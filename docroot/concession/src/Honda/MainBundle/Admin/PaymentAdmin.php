<?php

namespace Honda\MainBundle\Admin;


use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class PaymentAdmin
 * @package Honda\MainBundle\Admin
 */
class PaymentAdmin extends AbstractDistributor
{
    protected $baseRoutePattern = 'payment';
    protected $baseRouteName = 'payment';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clear();
        $collection->add('payment', 'payment/checkout');
        $collection->add('return', 'payment/return');
        $collection->add('cancel', 'payment/cancel');
    }
    
}
