<?php

namespace Honda\MainBundle\Admin;


use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class DocumentAdmin
 * @package Honda\MainBundle\Admin
 */
class DocumentAdmin extends AbstractDistributor
{
    protected $baseRoutePattern = 'documents';
    protected $baseRouteName = 'documents';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
