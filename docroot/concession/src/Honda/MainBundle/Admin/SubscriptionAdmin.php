<?php

namespace Honda\MainBundle\Admin;

use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

/**
 * Class SubscriptionAdmin
 * @package Honda\MainBundle\Admin
 */
class SubscriptionAdmin extends AbstractDistributor
{
    protected $baseRoutePattern = 'subscription';
    protected $baseRouteName = 'subscription';

    public $billingAddress;
    public $currentSubscription;
    public $subscriptions;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        //$collection->add('bill', 'subscription/'. $this->getRouterIdParameter() .'/bill');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('billingId', null, ['label'=> 'Facture', 'template' => 'SonataMediaBundle:MediaAdmin:invoice.html.twig'])
            ->add('year', null, ['label' => 'Année'])
            ->add('period', null, ['label' => 'Période'])
            ->add('price', null, ['precision' => 2, 'label' => 'Montant de l\'abonnement TTC (en €)'])
            ->add('settlement', null, ['label' => 'Réglé'])
            /*->add('_action', 'actions', [
                'actions' => [
                    'bill' => [
                        'template' => '@HondaMain/Admin/CRUD/Subscription/list__action_bill.html.twig'
                    ]
                ]
            ])*/
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('year')
        ;
    }
}
