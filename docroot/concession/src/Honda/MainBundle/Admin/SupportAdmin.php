<?php

namespace Honda\MainBundle\Admin;


use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class SupportAdmin
 * @package Honda\MainBundle\Admin
 */
class SupportAdmin extends AbstractDistributor
{
    protected $baseRoutePattern = 'support';
    protected $baseRouteName = 'support';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['create','list','edit']);
    }
    
}
