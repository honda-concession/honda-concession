<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SettingAdmin extends AbstractAdmin
{
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('list')
            ->remove('delete')
            ->remove('create')
            ->remove('show')
        ;
    }
    
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('pushFowardSlider')
            ->add('pushFowardALaune')
            ->add('pushFowardNews')
            ->add('pushFowardService')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('pushFowardSlider')
            ->add('pushFowardALaune')
            ->add('pushFowardNews')
            ->add('pushFowardService')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('pushFowardSlider', null, ['label' => 'Mettre en avant mon "Carousel"'])
            ->add('pushFowardALaune', null, ['label' => 'Mettre en avant mon "A la une"'])
            ->add('pushFowardNews', null, ['label' => 'Mettre en avant mon "Actualité"'])
            ->add('pushFowardService', null, ['label' => 'Mettre en avant mon "Service"'])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('pushFowardSlider')
            ->add('pushFowardALaune')
            ->add('pushFowardNews')
            ->add('pushFowardService')
        ;
    }
}
