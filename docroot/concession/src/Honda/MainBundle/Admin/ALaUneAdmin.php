<?php

namespace Honda\MainBundle\Admin;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type As Type;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Honda\MainBundle\Model\Front\AlaUneManager;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;

/**
 * Class ALaUneAdmin
 * @package Honda\MainBundle\Admin
 */
class ALaUneAdmin extends AbstractDistributor
{

    use Traits\ActivationAdminTrait,
        Traits\RedirectionAdminTrait;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $choices = [];

    /**
     * @var array
     */
    public $choicesFlipped = [];

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        $types = $this->container->get(AlaUneManager::class)->getAlaUneCategories();
        foreach ($types as $type) {
            $this->choices[$type['name']] = $type['id'];
        }
        $this->choicesFlipped = array_flip($this->choices);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('subtitle')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('position_', 'actions', array(
                'actions' => array(
                    'move' => array('template' => '@PixSortableBehavior/Default/_sort_drag_drop.html.twig')
                )
            ))
            ->addIdentifier('title')
            ->add('subtitle')
            ->add('media', 'string', array('template' => '@SonataMedia/MediaAdmin/list_image.html.twig'))
            ->add('published', null, ['editable' => true])
            ->add('publishedOnHome', null, ['editable' => true])
            ->add('type', 'string', ['template' => '@HondaMain/Admin/CRUD/News/list_translate.html.twig', 'label' => 'Catégorie de l\'actualité'])
            ->add('startDate', 'datetime', ['format' => 'd/m/Y'])
            ->add('endDate', 'datetime', ['format' => 'd/m/Y'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('config.content',  ['class' => 'col-md-9'])
                ->add('type', Type\ChoiceType::class, [
                        'choices' => $this->choices
                    ]
                )
                ->add('title', Type\TextareaType::class, [
                        'attr' => [
                            'rows' => 2
                        ]
                    ]
                )
                ->add('subtitle')
                ->add('textMea')
                ->add('promoPrice')
                ->add('linkLabel', null, [
                        'label' => 'Label bouton',
                        'required' => false
                    ]
                )
                ->add('link', null, [
                        'label' => 'Lien bouton',
                        'required' => false
                    ]
                )
                ->add('linkOpenInNewTab', null, [
                    'label' => 'Ouvrir dans une nouvelle fenêtre',
                    'required' => false,
                ])
                ->add('media', ModelListType::class, [
                        'label' => 'config.label_media',
                        'required' => false,
                    ], [
                            'link_parameters' => ['context' => 'alaune']
                        ]
                    )
            ->end()

            ->with('Activation', array('class' => 'col-md-3'))
                ->add('startDate', DateTimePickerType::class, ['format'=>'dd/MM/yyyy H:mm:ss'])
                ->add('endDate', DateTimePickerType::class, [ 'format'=>'dd/MM/yyyy H:mm:ss'])
                ->add('published', null, [
                    'label' => 'Publier',
                    'required' => false,
                ])
                ->add('publishedOnHome', null, [
                    'label' => 'Publier sur la page d\'accueil',
                    'required' => false,
                ])
            ->end();
        $this->addRedirectionBlock($formMapper);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('media')
            ->add('subtitle')
            ->add('textMea')
            ->add('link')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
        parent::configureRoutes($collection);
    }

}
