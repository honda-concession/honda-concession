<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;


class VehicleAdDescriptionAdmin extends AbstractAdmin
{
    
    protected $baseRoutePattern = 'combination-description';
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('content');
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    protected function configureListFields(ListMapper $listMapper)
    {
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
    }
    
    /**
     * @inheritdoc
     */
    public function toString($object)
    {
        return '';
    }
}
