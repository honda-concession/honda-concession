<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;

class VehicleAdGalleryAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('image')
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('image')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('isMainImage', null, [
                    'attr' => ['class' => 'galleryVo'],
                    'label_attr' => array('class' => 'labelGalleryVo'),
                ]
            )
            ->add('image', ModelListType::class, [
                    'label' => 'Média',
                    'btn_delete'    => false,
                ],
                [
                    'link_parameters' => [
                        'context' => 'vehicleAd'
                    ]
                ]
            )
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('image')
        ;
    }
}
