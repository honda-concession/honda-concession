<?php

namespace Honda\MainBundle\Admin;


use Sonata\AdminBundle\Route\RouteCollection;
use Honda\MainBundle\Admin\Base\AbstractDistributor;


/**
 * Class AccessoryEquipementMotardAdmin
 * @package Honda\MainBundle\Admin
 */
class AccessoryEquipementMotardAdmin extends AbstractDistributor
{
    
    protected $baseRoutePattern = 'accessory-equipement-motard';
    protected $baseRouteName = 'accessory-equipement-motard';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
