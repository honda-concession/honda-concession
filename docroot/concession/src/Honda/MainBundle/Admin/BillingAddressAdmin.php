<?php

namespace Honda\MainBundle\Admin;


use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class BillingAddressAdmin
 * @package Honda\MainBundle\Admin
 */
class BillingAddressAdmin extends AbstractDistributor
{
    protected $baseRoutePattern = 'billing-address';
    protected $baseRouteName = 'billing-address';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
    
}
