<?php

namespace Honda\MainBundle\Admin;


use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class SectionAdmin
 * @package Honda\MainBundle\Admin
 */
class SectionAdmin extends AbstractDistributor
{
    protected $baseRoutePattern = 'section';
    protected $baseRouteName = 'section';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        $collection->add('ajaxOrder', 'ajax-order', [], [], [], '', [], ['POST', 'GET']);
    }
    
}
