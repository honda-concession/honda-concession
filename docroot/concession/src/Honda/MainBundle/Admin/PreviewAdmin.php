<?php

namespace Honda\MainBundle\Admin;


use Honda\MainBundle\Admin\Base\AbstractDistributor;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class PreviewAdmin
 * @package Honda\MainBundle\Admin
 */
class PreviewAdmin extends AbstractDistributor
{
    protected $baseRoutePattern = 'preview';
    protected $baseRouteName = 'preview';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clear();
        $collection->add('maintenance', '', [], [], [], '', [], ['GET']);
    }
    
}
