<?php

namespace Honda\MainBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Honda\MainBundle\Admin\Base\AbstractDistributor;

/**
 * Class AccessoryMotoAdmin
 * @package Honda\MainBundle\Admin
 */
class AccessoryMotoAdmin extends AbstractDistributor
{

    protected $baseRoutePattern = 'accessory-moto';
    protected $baseRouteName = 'accessory-moto';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
}
