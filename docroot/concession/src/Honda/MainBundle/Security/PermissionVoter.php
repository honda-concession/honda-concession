<?php

namespace Honda\MainBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * Class PermissionVoter
 * @package AppBundle\Security
 */
class PermissionVoter extends Voter
{
    
    const OWNER = 'owner';
    
    protected function supports($attribute, $subject)
    {
        
        if (!in_array($attribute, array(self::OWNER))) {
            return false;
        }
        
        return true;
    }
    
    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
    
        if (!$user instanceof UserInterface) {
            return false;
        }
        
        return $this->hasAccess($subject, $user);
        
    }
    
    /**
     * @param $object
     * @param User $user
     * @return bool
     */
    private function hasAccess($object, UserInterface $user)
    {
        
        if (method_exists($object, 'getDistributor')) {
            
            return $user->getDistributor() === $object->getDistributor();
        }
        
        return false;
    }
    
}
