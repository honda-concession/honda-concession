<?php

namespace Honda\MainBundle\Security;

use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use FOS\UserBundle\Model\UserInterface;

use Honda\MainBundle\Entity\Distributor;
use Honda\MainBundle\Event\UserEvent;
use Honda\MainBundle\Model\Admin\HostManager;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;
/**
 * Class DomainAuthenticator
 * @package Honda\MainBundle\Security
 */
class DomainAuthenticator implements SimpleFormAuthenticatorInterface
{
    private $encoder;
    private $encoderFactory;
    private $requestStack;
    private $hostManager;
    private $dispatcher;
    private $em;
    private $tokenStorage;
    private $storage;

    /**
     * DomainAuthenticator constructor.
     * @param UserPasswordEncoderInterface $encoder
     * @param EncoderFactoryInterface $encoderFactory
     * @param RequestStack $requestStack
     * @param HostManager $hostManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        UserPasswordEncoderInterface $encoder,
        EncoderFactoryInterface $encoderFactory,
        RequestStack $requestStack,
        HostManager $hostManager,
        EventDispatcherInterface $dispatcher,
        EntityManagerInterface $em
    ) {
        $this->encoder = $encoder;
        $this->encoderFactory  = $encoderFactory;
        $this->requestStack = $requestStack;
        $this->hostManager = $hostManager;
        $this->dispatcher = $dispatcher;
        $this->em = $em;
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {

        $isPasswordValid = false;
        $isHostValid = false;

        try {
            $user = $userProvider->loadUserByUsername($token->getUsername());
        } catch (UsernameNotFoundException $exception) {
            throw new CustomUserMessageAuthenticationException('Invalid username or password');
        }

        $currentUser = $token->getUser();

        if ($currentUser instanceof UserInterface) {
            if ($currentUser->getPassword() !== $user->getPassword()) {
                throw new BadCredentialsException('The credentials were changed from another session.');
            }
        } else {

            if ('' === ($givenPassword = $token->getCredentials())) {
                throw new BadCredentialsException('The given password cannot be empty.');
            }

            if (!$isPasswordValid = $this->encoderFactory->getEncoder($user)->isPasswordValid($user->getPassword(), $givenPassword, $user->getSalt())) {
                throw new BadCredentialsException('The given password is invalid.');
            }
        }

        $isAuthenticationValid = false;

        if ($user->hasRole('ROLE_ROOT')) {
            $isAuthenticationValid = true;
            $distributor = $this->em->getRepository(Distributor::class)->findOneByDomain($this->getCurrentDomain());
            $user->setDistributor($distributor);
            $this->em->persist($user);
            $this->em->flush($user);
        } else {
            // Ici on vérifie ici le sous domaine du concessionaire
            /**
             * @var Distributor
             */
            $distributor = $user->getDistributor();

            if (!$isHostValid = $this->hostManager->isDomainValid($distributor)) {
                throw new BadCredentialsException('Your host is not valid.');
            }

            if ($isPasswordValid && $isHostValid) {
                $isAuthenticationValid = true;
            }
        }

       if ($isAuthenticationValid) {

           return new UsernamePasswordToken(
               $user,
               $user->getPassword(),
               $token->getProviderKey(),
               $user->getRoles()
           );
       }

        throw new CustomUserMessageAuthenticationException('Invalid username or password');
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof UsernamePasswordToken
            && $token->getProviderKey() === $providerKey;
    }

    public function createToken(Request $request, $username, $password, $providerKey)
    {
        return new UsernamePasswordToken($username, $password, $providerKey);
    }

    protected function getCurrentDomain()
    {
        $request = $this->requestStack->getCurrentRequest();

        return $request->getHttpHost();
    }
}
