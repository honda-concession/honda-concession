<?php

namespace Honda\MainBundle\Repository;

use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;


/**
 * DistributorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DistributorRepository extends \Doctrine\ORM\EntityRepository
{
    
    /**
     * @param $domain
     * @return bool|null|object
     */
    public function getDistributorByDomain($domain)
    {
        $qb = $this->createQueryBuilder('this');
    
        $qb->addSelect('logo');
        $qb->leftJoin('this.logo', 'logo');
        $qb->where('this.domain LIKE :domain');
        $qb->setParameter('domain', $domain);
    
        try {
            return $qb->getQuery()->getSingleResult();
        } catch(NoResultException $e) {
        } catch(NonUniqueResultException $e) {
        }
    
        return false;
    }
}
