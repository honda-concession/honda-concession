<?php

namespace Honda\MainBundle\Tools\Encryption;

use Honda\MainBundle\Tools\Encryption\Lib\EncryptorInterface;

/**
 * Class EncryptorOpenssl
 * 
 */
class EncryptorAesEcb implements EncryptorInterface
{   
    /**
     * @param $value 
     * @param $key
     * @param $method
     *
     * @return string
     */
    public static function encode($value, $key, $method="AES-128-ECB")
    { 
        if(!$value || !$key ){
            return false;
        }
        
        return openssl_encrypt($value, $method, $key);
    }

    /**
     * @param $value 
     * @param $key
     * @param $method
     *
     * @return string
     */
    public static function decode($value, $key, $method="AES-128-ECB")
    {
        if(!$value || !$key){
            return false;
        }
        
        return openssl_decrypt($value, $method, $key);
    }

    /**
     * get namespace function
     *
     * @return string
     */
    public static function getName()
    {
        return __CLASS__;
    }

}