<?php

namespace Honda\MainBundle\Block\Service;

use Honda\MainBundle\Entity\Subscription;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityRepository;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\Service\AbstractBlockService as BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Honda\MainBundle\Entity\Distributor;

/**
 * Class SubscriptionAlertService
 * @package Honda\MainBundle\Block\Service
 */
abstract class AbstractBlockService extends BaseBlockService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    public function __construct($name, EngineInterface $templating, ContainerInterface $container, TokenStorageInterface $tokenStorage)
    {
        parent::__construct($name, $templating);
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
    }
}