<?php

namespace Honda\MainBundle\Block\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityRepository;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BlockContextInterface;

/**
 * Description of NewsBlockService
 *
 * @author Administrateur
 */
class ServiceBlockService extends AbstractBlockService
{

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'sonata.block.service.service';
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultSettings()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block){}

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block){}

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $data = null;

        $settings = array_merge($this->getDefaultSettings(), $blockContext->getSettings());

        if($this->tokenStorage->getToken()){
            $user = $this->tokenStorage->getToken()->getUser();
            $distributor = $user->getDistributor();
            $doctrine = $this->container->get('doctrine');
            $em = $doctrine->getManager();

            $query = $em->createQuery('SELECT count(n) as num FROM HondaMainBundle:Service n WHERE n.distributor = :distributor')
                        ->setParameter('distributor', $distributor->getId());

            $data = $query->getSingleResult();
        }

        return $this->renderResponse('@HondaMain/Admin/Block/block_service.html.twig', array(
            'block'     => $blockContext->getBlock(),
            'settings'  => $settings,
            'data'      => $data
        ), $response);
    }
}