<?php

namespace Honda\MainBundle\Block\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Honda\MainBundle\Http\PortalSiteGateway;

/**
 * Description of NewsBlockService
 *
 * @author Administrateur
 */
class TicketBlockService extends AbstractBlockService
{

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'sonata.block.service.ticket';
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultSettings()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block){}

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block){}

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $tickets = [
            'ticket' => 0
        ];

        if($this->tokenStorage->getToken()){
            $user = $this->tokenStorage->getToken()->getUser();
            $distributor = $user->getDistributor();
            $client = $this->container->get(PortalSiteGateway::class);
            $tickets = $client->getList($distributor->getDistributorPortailId());
        }

        return $this->renderResponse('@HondaMain/Admin/Block/block_ticket.html.twig', array(
            'count' => count($tickets['ticket']),
        ), $response);
    }
}
