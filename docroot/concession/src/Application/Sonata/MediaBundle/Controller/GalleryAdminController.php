<?php

namespace Application\Sonata\MediaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\MediaBundle\Controller\GalleryAdminController as BaseController;

class GalleryAdminController extends BaseController
{
    /**
     * This method can be overloaded in your custom CRUD controller.
     * It's called from editAction.
     *
     * @param mixed $object
     *
     * @return Response|null
     */
    protected function preEdit(Request $request, $object)
    {
        if(!$this->checkOwner($object))
            throw $this->createNotFoundException('The page does not exist');
    }

    /**
     * This method can be overloaded in your custom CRUD controller.
     * It's called from deleteAction.
     *
     * @param mixed $object
     *
     * @return Response|null
     */
    protected function preDelete(Request $request, $object)
    {
        if(!$this->checkOwner($object))
            throw $this->createNotFoundException('The page does not exist');
    }

    /**
     * This method can be overloaded in your custom CRUD controller.
     * It's called from showAction.
     *
     * @param mixed $object
     *
     * @return Response|null
     */
    protected function preShow(Request $request, $object)
    {
        if(!$this->checkOwner($object))
            throw $this->createNotFoundException('The page does not exist');
    }

    protected function checkOwner($object)
    {
        $user = $this->getUser();
        $distributor = $user->getDistributor();
        $didUser = $distributor->getId();
        $didMedia = $object->getDistributor();

        return $didUser == $didMedia;
    }
}
