<?php

namespace Application\Sonata\MediaBundle\Admin\Extension;

use Sonata\AdminBundle\Admin\AbstractAdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class MediaExtension extends AbstractAdminExtension
{
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('distributor', null, [
                'show_filter' => false
            ]);
    }
}