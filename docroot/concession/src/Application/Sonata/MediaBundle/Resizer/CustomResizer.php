<?php
namespace Application\Sonata\MediaBundle\Resizer;

use Gaufrette\File;
use Imagine\Exception\InvalidArgumentException;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Sonata\MediaBundle\Metadata\MetadataBuilderInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Resizer\ResizerInterface;
use Sonata\MediaBundle\Resizer\SimpleResizer;
use Sonata\MediaBundle\Resizer\SquareResizer;

/**
 * Class CustomResizer
 *
 * @package Application\Sonata\MediaBundle\Resizer
 */
class CustomResizer implements ResizerInterface
{
    /**
     * @var ImagineInterface
     */
    protected $adapter;
    
    /**
     * @var MetadataBuilderInterface
     */
    protected $metadata;
    
    /**
     * @var string
     */
    protected $mode;
    
    /**
     * @param ImagineInterface         $adapter
     * @param string                   $mode
     * @param MetadataBuilderInterface $metadata
     */
    public function __construct(ImagineInterface $adapter, $mode, MetadataBuilderInterface $metadata)
    {
        $this->adapter  = $adapter;
        $this->mode     = $mode;
        $this->metadata = $metadata;
    }
    
    /**
     * {@inheritdoc}
     */
    public function resize(MediaInterface $media, File $in, File $out, $format, array $settings)
    {
        if (isset($settings['constraint']) && $settings['constraint'] == 'square') {
            $s = new SquareResizer($this->adapter, $this->mode, $this->metadata);
            $s->resize($media, $in, $out, $format, $settings);
            
            return;
        }
        
        if (isset($settings['constraint']) && $settings['constraint'] == 'simple') {
            $s = new SimpleResizer($this->adapter, $this->mode, $this->metadata);
            $s->resize($media, $in, $out, $format, $settings);
            
            return;
        }
        
        if (!(isset($settings['width']) && $settings['width'])) {
            throw new \RuntimeException(
                sprintf(
                    'Width parameter is missing in context "%s" for provider "%s"',
                    $media->getContext(),
                    $media->getProviderName()
                )
            );
        }
    
        $image = $this->adapter->load($in->getContent());
        
        $mode = ImageInterface::THUMBNAIL_INSET;
        
        $content = $image
            ->thumbnail($this->getBox($media, $settings), $mode)
            ->get($format, array('quality' => $settings['quality']));
        
        
        $out->setContent($content, $this->metadata->get($media, $out->getName()));
        
        //$imagePad = $this->pad($this->adapter->load($out->getContent()), $this->getBox($media, $settings));
    
        //$out->setContent($this->adapter->load($imagePad), $this->metadata->get($media, $out->getName()));
        
    }
    
    
    function pad(\Imagine\Gd\Image $img, \Imagine\Image\Box $size, $fcolor='#000', $ftransparency = 100)
    {
        $tsize = $img->getSize();
        $x = $y = 0;
        
        if ($size->getWidth() > $tsize->getWidth()) {
            $x =  round(($size->getWidth() - $tsize->getWidth()) / 2);
        } elseif ($size->getHeight() > $tsize->getHeight()) {
            $y = round(($size->getHeight() - $tsize->getHeight()) / 2);
        }
        
        $pasteto = new \Imagine\Image\Point($x, $y);
        $imagine = new \Imagine\Gd\Imagine();
    
        $palette = new \Imagine\Image\Palette\RGB();
        $color = $palette->color($fcolor, $ftransparency);
        $image = $imagine->create($size, $color);
        
       // dump($image, 'test'); exit;
        
        $image->paste($img, $pasteto);
        
        return $image;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBox(MediaInterface $media, array $settings)
    {
        $size      = $media->getBox();
        $hasWidth  = isset($settings['width']) && $settings['width'];
        $hasHeight = isset($settings['height']) && $settings['height'];
        
        if (!$hasWidth && !$hasHeight) {
            throw new \RuntimeException(
                sprintf(
                    'Width/Height parameter is missing in context "%s" for provider "%s". Please add at least one parameter.',
                    $media->getContext(),
                    $media->getProviderName()
                )
            );
        }
        
        if ($hasWidth && $hasHeight) {
            return new Box($settings['width'], $settings['height']);
        }
        
        if (!$hasHeight) {
            $settings['height'] = intval($settings['width'] * $size->getHeight() / $size->getWidth());
        }
        
        if (!$hasWidth) {
            $settings['width'] = intval($settings['height'] * $size->getWidth() / $size->getHeight());
        }
        
        return $this->computeBox($media, $settings);
    }
    
    /**
     * @throws InvalidArgumentException
     *
     * @param MediaInterface $media
     * @param array          $settings
     *
     * @return Box
     */
    private function computeBox(MediaInterface $media, array $settings)
    {
        if ($this->mode !== ImageInterface::THUMBNAIL_INSET && $this->mode !== ImageInterface::THUMBNAIL_OUTBOUND) {
            throw new InvalidArgumentException('Invalid mode specified');
        }
        
        $size = $media->getBox();
        
        $ratios = [
            $settings['width'] / $size->getWidth(),
            $settings['height'] / $size->getHeight(),
        ];
        
        if ($this->mode === ImageInterface::THUMBNAIL_INSET) {
            $ratio = min($ratios);
        } else {
            $ratio = max($ratios);
        }
        
        return $size->scale($ratio);
    }
}
