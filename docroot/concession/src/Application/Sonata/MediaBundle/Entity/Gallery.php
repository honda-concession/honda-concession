<?php

namespace Application\Sonata\MediaBundle\Entity;

use Sonata\MediaBundle\Entity\BaseGallery as BaseGallery;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation As JMS;

/**
 * This file has been generated by the SonataEasyExtendsBundle.
 *
 * @link https://sonata-project.org/easy-extends
 *
 * References:
 * @link http://www.doctrine-project.org/projects/orm/2.0/docs/reference/working-with-objects/en
 *
 * @ORM\Table(name="media__gallery")
 * @ORM\Entity
 * @JMS\ExclusionPolicy("all")
 */
class Gallery extends BaseGallery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @JMS\Expose()
     * @JMS\Groups({"sonata_api_read", "sonata_api_write", "sonata_search"})
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="distributor", type="integer")
     */
    protected $distributor;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get distributor.
     *
     * @return int $distributor
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * Set distributor.
     *
     * @param int $distributor
     *
     * @return Media
     */
    public function setDistributor($distributor)
    {
        $this->distributor = $distributor;

        return $this;
    }
}
