<?php

namespace Application\Sonata\MediaBundle\Provider;

use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\YouTubeProvider as BaseVideoProvider;
use Sonata\MediaBundle\Provider\MediaProviderInterface;

class YouTubeProvider extends BaseVideoProvider
{
    /**
     * {@inheritdoc}
     */
    public function getReferenceFile(MediaInterface $media)
    {
        $key = $this->generatePrivateUrl($media, MediaProviderInterface::FORMAT_REFERENCE);
        $referenceFile = $this->getFilesystem()->get($key, true);
        $metadata = $this->metadata ? $this->metadata->get($media, $referenceFile->getName()) : [];
        $referenceFile->setContent($this->browser->get($this->getReferenceImage($media))->getContent(), $metadata);


        return $referenceFile;
    }
}
