<?php

namespace Application\Sonata\MediaBundle\EventSubscriber;

use Doctrine\ORM\Events;
use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MediaSubscriber implements EventSubscriber
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    protected $container;

    protected $orientation;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * set service container
     *
     * @param ContainerInterface $container
     * @return self
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::postPersist
        ];
    }

    /**
     *
     * @param EventArgs $args
     *
     * @return void
     */
    public function prePersist(EventArgs $eventArgs)
    {
        $object = $eventArgs->getEntity();

        if ($object instanceof MediaInterface) {
            $object = $this->setDistributor($object);
        }
    }

    /**
     *
     * @param EventArgs $args
     *
     * @return void
     */
    public function postPersist(EventArgs $eventArgs)
    {
        $object = $eventArgs->getEntity();
        if (!($object instanceof MediaInterface)) {
            return;
        }

        $provider = $this->container->get($object->getProviderName());
        $projectDir = $this->container->get('kernel')->getProjectDir();
        $file = $projectDir.'/web'.$provider->generatePublicUrl($object, MediaProviderInterface::FORMAT_REFERENCE);
        if ($this->fixImageOrientation($file)) {
            $contextFormats = $this->getContextFormats($object->getContext(), $provider->getFormats());
            foreach ($contextFormats as $contextFormat) {
                $thumbnailFile = $projectDir.'/web'.$provider->generatePublicUrl($object, $contextFormat);
                $this->fixImageOrientation($thumbnailFile);
            }
            $this->orientation = null;
        }
    }

    /**
     *
     * @param EventArgs $args
     *
     * @return void
     */
    public function preUpdate(EventArgs $eventArgs)
    {
        $object = $eventArgs->getEntity();

        if ($object instanceof MediaInterface) {
            $object = $this->setDistributor($object);
        }
    }

    /**
     * Set distributor id
     *
     * @param MediaInterface $object
     */
    public function setDistributor(&$object)
    {
        if ($this->tokenStorage->getToken()){
            $user = $this->tokenStorage->getToken()->getUser();
            $distributor = $user->getDistributor();
            $object->setDistributor($distributor->getId());
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function getNamespace()
    {
        return __NAMESPACE__;
    }

    /**
     * fix image orientation
     *
     * @param string $filename
     * @return bool
     */
    private function fixImageOrientation($filename) {
        if (function_exists('exif_read_data')) {
            $exif = exif_read_data($filename);
            if (($exif && isset($exif['Orientation'])) || $this->orientation) {
                $orientation = isset($exif['Orientation']) ? $exif['Orientation'] : $this->orientation;
                if ($orientation != 1){
                    $img = imagecreatefromjpeg($filename);
                    $deg = 0;
                    switch ($orientation) {
                        case 3:
                            $deg = 180;
                            break;
                        case 6:
                            $deg = 270;
                            break;
                        case 8:
                            $deg = 90;
                        break;
                    }
                    if ($deg) {
                        if (!preg_match('/thumb/', $filename)) {
                            $this->orientation = $orientation;
                        }
                        $img = imagerotate($img, $deg, 0);
                    }
                    imagejpeg($img, $filename, 95);

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * get thumbnail context formats
     *
     * @param string $context
     * @param array $formats
     * @return array
     */
    private function getContextFormats($context, $formats)
    {
        $contextFormats = ['admin'];
        foreach (array_keys($formats) as $format) {
            $expression = "/^{$context}_/";
            if (preg_match($expression, $format)) {
                $contextFormats[] = $format;
            }
        }

        return $contextFormats;
    }
}
