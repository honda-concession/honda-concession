<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190205220840 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE distributor ADD insert_picture INT DEFAULT NULL');
        $this->addSql('ALTER TABLE distributor ADD CONSTRAINT FK_A3C557712D3000DF FOREIGN KEY (insert_picture) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_A3C557712D3000DF ON distributor (insert_picture)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE distributor DROP FOREIGN KEY FK_A3C557712D3000DF');
        $this->addSql('DROP INDEX IDX_A3C557712D3000DF ON distributor');
        $this->addSql('ALTER TABLE distributor DROP insert_picture');
    }
}
