<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181210101250 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fos_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_4B019DDB5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id INT AUTO_INCREMENT NOT NULL, distributor_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, date_of_birth DATETIME DEFAULT NULL, firstname VARCHAR(64) DEFAULT NULL, lastname VARCHAR(64) DEFAULT NULL, website VARCHAR(64) DEFAULT NULL, biography VARCHAR(1000) DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, locale VARCHAR(8) DEFAULT NULL, timezone VARCHAR(64) DEFAULT NULL, phone VARCHAR(64) DEFAULT NULL, facebook_uid VARCHAR(255) DEFAULT NULL, facebook_name VARCHAR(255) DEFAULT NULL, facebook_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', twitter_uid VARCHAR(255) DEFAULT NULL, twitter_name VARCHAR(255) DEFAULT NULL, twitter_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', gplus_uid VARCHAR(255) DEFAULT NULL, gplus_name VARCHAR(255) DEFAULT NULL, gplus_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', token VARCHAR(255) DEFAULT NULL, two_step_code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_957A6479C05FB297 (confirmation_token), UNIQUE INDEX UNIQ_957A64792D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user_group (user_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_583D1F3EA76ED395 (user_id), INDEX IDX_583D1F3EFE54D947 (group_id), PRIMARY KEY(user_id, group_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media__gallery_media (id INT AUTO_INCREMENT NOT NULL, gallery_id INT DEFAULT NULL, media_id INT DEFAULT NULL, position INT NOT NULL, enabled TINYINT(1) NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_80D4C5414E7AF8F (gallery_id), INDEX IDX_80D4C541EA9FDD75 (media_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media__gallery (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, context VARCHAR(64) NOT NULL, default_format VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, distributor INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media__media (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, enabled TINYINT(1) NOT NULL, provider_name VARCHAR(255) NOT NULL, provider_status INT NOT NULL, provider_reference VARCHAR(255) NOT NULL, provider_metadata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', width INT DEFAULT NULL, height INT DEFAULT NULL, length NUMERIC(10, 0) DEFAULT NULL, content_type VARCHAR(255) DEFAULT NULL, content_size INT DEFAULT NULL, copyright VARCHAR(255) DEFAULT NULL, author_name VARCHAR(255) DEFAULT NULL, context VARCHAR(64) DEFAULT NULL, cdn_is_flushable TINYINT(1) DEFAULT NULL, cdn_flush_identifier VARCHAR(64) DEFAULT NULL, cdn_flush_at DATETIME DEFAULT NULL, cdn_status INT DEFAULT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, distributor INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_documents (id INT AUTO_INCREMENT NOT NULL, billing_documents INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_16A310D616A310D6 (billing_documents), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscription (id INT AUTO_INCREMENT NOT NULL, distributor_id INT DEFAULT NULL, billingId VARCHAR(255) DEFAULT NULL, start_date DATETIME DEFAULT NULL, end_date DATETIME DEFAULT NULL, price NUMERIC(10, 5) DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, INDEX IDX_A3C664D32D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicle_ad_description (id INT AUTO_INCREMENT NOT NULL, vehicle_ad_id INT DEFAULT NULL, content VARCHAR(255) DEFAULT NULL, INDEX IDX_3D4969142D9B557A (vehicle_ad_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE service_page (id INT AUTO_INCREMENT NOT NULL, image INT DEFAULT NULL, distributor_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, intro TINYTEXT DEFAULT NULL, INDEX IDX_DA4902CCC53D045F (image), UNIQUE INDEX UNIQ_DA4902CC2D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE service (id INT AUTO_INCREMENT NOT NULL, icon INT DEFAULT NULL, pdf INT DEFAULT NULL, image_id INT DEFAULT NULL, distributor_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, is_default_service TINYINT(1) DEFAULT \'0\' NOT NULL, active TINYINT(1) DEFAULT \'0\' NOT NULL, position INT DEFAULT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_E19D9AD2989D9B62 (slug), INDEX IDX_E19D9AD2659429DB (icon), INDEX IDX_E19D9AD2EF0DB8C (pdf), INDEX IDX_E19D9AD23DA5256D (image_id), INDEX IDX_E19D9AD22D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventory (id INT AUTO_INCREMENT NOT NULL, distributor_id INT DEFAULT NULL, in_stock TINYINT(1) DEFAULT \'0\', new_vehicle_id INT NOT NULL, new_vehicle_title VARCHAR(255) DEFAULT NULL, new_vehicle_reference VARCHAR(255) DEFAULT NULL, new_vehicle_model VARCHAR(255) DEFAULT NULL, new_vehicle_category VARCHAR(255) DEFAULT NULL, new_vehicle_mark VARCHAR(255) DEFAULT NULL, new_vehicle_submodel VARCHAR(255) DEFAULT NULL, new_vehicle_submodel_slug VARCHAR(255) DEFAULT NULL, new_vehicle_submodel_id VARCHAR(255) DEFAULT NULL, new_vehicle_color VARCHAR(255) DEFAULT NULL, new_vehicle_color_icon VARCHAR(255) DEFAULT NULL, new_vehicle_price VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, INDEX IDX_B12D4A362D863A58 (distributor_id), INDEX newVehiclePortailId_idx (new_vehicle_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE distributor (id INT AUTO_INCREMENT NOT NULL, logo INT DEFAULT NULL, infos LONGTEXT DEFAULT NULL, distributor_portail_id INT NOT NULL, bg_color VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, domain VARCHAR(255) NOT NULL, timetable LONGTEXT DEFAULT NULL, location LONGTEXT DEFAULT NULL, region LONGTEXT DEFAULT NULL, town LONGTEXT DEFAULT NULL, department LONGTEXT DEFAULT NULL, google_analytic LONGTEXT DEFAULT NULL, active TINYINT(1) DEFAULT \'0\', UNIQUE INDEX UNIQ_A3C55771A7A91E0B (domain), INDEX IDX_A3C55771E48E9A13 (logo), INDEX domain_idx (domain), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news (id INT AUTO_INCREMENT NOT NULL, image INT DEFAULT NULL, distributor_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, description1 LONGTEXT DEFAULT NULL, description2 LONGTEXT DEFAULT NULL, description3 LONGTEXT DEFAULT NULL, start_date DATETIME DEFAULT NULL, end_date DATETIME DEFAULT NULL, active TINYINT(1) DEFAULT \'0\', is_on_homepage TINYINT(1) DEFAULT \'0\', type INT DEFAULT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_1DD39950989D9B62 (slug), INDEX IDX_1DD39950C53D045F (image), INDEX IDX_1DD399502D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sub_model_test (id INT AUTO_INCREMENT NOT NULL, distributor_id INT DEFAULT NULL, sub_model_portail_title VARCHAR(255) DEFAULT NULL, sub_model_portail_id INT DEFAULT NULL, on_test TINYINT(1) DEFAULT \'0\', INDEX IDX_D9E3C1262D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicle_ad_gallery (id INT AUTO_INCREMENT NOT NULL, vehicle_ad_id INT DEFAULT NULL, image_id INT DEFAULT NULL, is_main_image TINYINT(1) DEFAULT \'0\', INDEX IDX_FF31E232D9B557A (vehicle_ad_id), INDEX IDX_FF31E233DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE section (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, sectionKey VARCHAR(255) NOT NULL, templateName VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_2D737AEFCBFCB03 (sectionKey), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_slide (id INT AUTO_INCREMENT NOT NULL, news_id INT DEFAULT NULL, image_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, position INT DEFAULT NULL, INDEX IDX_850693CB5A459A0 (news_id), INDEX IDX_850693C3DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicle_ad (id INT AUTO_INCREMENT NOT NULL, distributor_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, price INT DEFAULT NULL, km VARCHAR(255) DEFAULT NULL, cylinder INT DEFAULT NULL, first_hand TINYINT(1) DEFAULT \'0\' NOT NULL, driving_license_a2 TINYINT(1) DEFAULT \'0\' NOT NULL, demo_vehicle TINYINT(1) DEFAULT \'0\' NOT NULL, garantie TINYINT(1) DEFAULT \'0\' NOT NULL, distributor_description LONGTEXT DEFAULT NULL, model_year VARCHAR(255) DEFAULT NULL, release_date DATETIME DEFAULT NULL, warrant VARCHAR(255) DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, options VARCHAR(255) DEFAULT NULL, model_portail VARCHAR(255) DEFAULT NULL, category_portail VARCHAR(255) DEFAULT NULL, mark_portail VARCHAR(255) DEFAULT NULL, is_published TINYINT(1) DEFAULT \'0\' NOT NULL, vehicleAdDesc LONGTEXT DEFAULT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_9C8A9DD9989D9B62 (slug), INDEX IDX_9C8A9DD92D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE social_network_link (id INT AUTO_INCREMENT NOT NULL, distributor_id INT DEFAULT NULL, facebook VARCHAR(255) DEFAULT NULL, instagram VARCHAR(255) DEFAULT NULL, snapchat VARCHAR(255) DEFAULT NULL, twitter VARCHAR(255) DEFAULT NULL, youtube VARCHAR(255) DEFAULT NULL, INDEX IDX_9152F3B92D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE a_la_une (id INT AUTO_INCREMENT NOT NULL, media INT DEFAULT NULL, distributor_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, subtitle VARCHAR(255) DEFAULT NULL, text_mea VARCHAR(255) DEFAULT NULL, promo_price VARCHAR(255) DEFAULT NULL, type INT DEFAULT NULL, link_label VARCHAR(255) DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, is_published TINYINT(1) DEFAULT \'0\' NOT NULL, position INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_7068117F6A2CA10C (media), INDEX IDX_7068117F2D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_address (id INT AUTO_INCREMENT NOT NULL, distributor_id INT DEFAULT NULL, firstName VARCHAR(255) DEFAULT NULL, company VARCHAR(255) DEFAULT NULL, lastName VARCHAR(255) DEFAULT NULL, address LONGTEXT DEFAULT NULL, address2 LONGTEXT DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, zipcode VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_6660E4562D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accessory_block (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_9620782989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE distributor_time (id INT AUTO_INCREMENT NOT NULL, distributor_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, start_hour DATETIME DEFAULT NULL, end_hour DATETIME DEFAULT NULL, position INT DEFAULT NULL, INDEX IDX_B297ADBB2D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment (id INT AUTO_INCREMENT NOT NULL, subscription_id INT DEFAULT NULL, total NUMERIC(10, 5) DEFAULT NULL, currency VARCHAR(255) DEFAULT NULL, paypal_id VARCHAR(255) DEFAULT NULL, state VARCHAR(255) DEFAULT NULL, create_time DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_6D28840D9A1887DC (subscription_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slider (id INT AUTO_INCREMENT NOT NULL, media INT DEFAULT NULL, distributor_id INT DEFAULT NULL, start_date DATETIME DEFAULT NULL, end_date DATETIME DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_CFC710076A2CA10C (media), INDEX IDX_CFC710072D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE section_order (id INT AUTO_INCREMENT NOT NULL, section_id INT DEFAULT NULL, distributor_id INT DEFAULT NULL, position INT NOT NULL, INDEX IDX_627EF4FCD823E37A (section_id), INDEX IDX_627EF4FC2D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accessory (id INT AUTO_INCREMENT NOT NULL, accessory_block_id INT DEFAULT NULL, image INT DEFAULT NULL, distributor_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, link1 VARCHAR(255) DEFAULT NULL, link1_label VARCHAR(255) DEFAULT NULL, link2 VARCHAR(255) DEFAULT NULL, link2_label VARCHAR(255) DEFAULT NULL, position INT DEFAULT NULL, description_accessory_clothing LONGTEXT DEFAULT NULL, description_accessory_helmet LONGTEXT DEFAULT NULL, clothing_logos LONGTEXT DEFAULT NULL, helmet_logos LONGTEXT DEFAULT NULL, INDEX IDX_A1B1251C680D9C64 (accessory_block_id), INDEX IDX_A1B1251CC53D045F (image), INDEX IDX_A1B1251C2D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page_block (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, template VARCHAR(255) DEFAULT NULL, path VARCHAR(255) DEFAULT NULL, help_text VARCHAR(255) DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, INDEX path_idx (path), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, page_block_id INT DEFAULT NULL, image INT DEFAULT NULL, distributor_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, metaDescription LONGTEXT DEFAULT NULL, metaKeywords LONGTEXT DEFAULT NULL, description1 LONGTEXT DEFAULT NULL, description2 LONGTEXT DEFAULT NULL, btn_link VARCHAR(255) DEFAULT NULL, btn_label VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_140AB6206972852C (page_block_id), INDEX IDX_140AB620C53D045F (image), INDEX IDX_140AB6202D863A58 (distributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE honda_label_slide (id INT AUTO_INCREMENT NOT NULL, page_id INT DEFAULT NULL, image_id INT DEFAULT NULL, INDEX IDX_C7717112C4663E4 (page_id), INDEX IDX_C77171123DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page_cookies_block (id INT AUTO_INCREMENT NOT NULL, page_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, block_type VARCHAR(255) DEFAULT NULL, INDEX IDX_90D067A2C4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_classes (id INT UNSIGNED AUTO_INCREMENT NOT NULL, class_type VARCHAR(200) NOT NULL, UNIQUE INDEX UNIQ_69DD750638A36066 (class_type), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_security_identities (id INT UNSIGNED AUTO_INCREMENT NOT NULL, identifier VARCHAR(200) NOT NULL, username TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8835EE78772E836AF85E0677 (identifier, username), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_object_identities (id INT UNSIGNED AUTO_INCREMENT NOT NULL, parent_object_identity_id INT UNSIGNED DEFAULT NULL, class_id INT UNSIGNED NOT NULL, object_identifier VARCHAR(100) NOT NULL, entries_inheriting TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_9407E5494B12AD6EA000B10 (object_identifier, class_id), INDEX IDX_9407E54977FA751A (parent_object_identity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_object_identity_ancestors (object_identity_id INT UNSIGNED NOT NULL, ancestor_id INT UNSIGNED NOT NULL, INDEX IDX_825DE2993D9AB4A6 (object_identity_id), INDEX IDX_825DE299C671CEA1 (ancestor_id), PRIMARY KEY(object_identity_id, ancestor_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_entries (id INT UNSIGNED AUTO_INCREMENT NOT NULL, class_id INT UNSIGNED NOT NULL, object_identity_id INT UNSIGNED DEFAULT NULL, security_identity_id INT UNSIGNED NOT NULL, field_name VARCHAR(50) DEFAULT NULL, ace_order SMALLINT UNSIGNED NOT NULL, mask INT NOT NULL, granting TINYINT(1) NOT NULL, granting_strategy VARCHAR(30) NOT NULL, audit_success TINYINT(1) NOT NULL, audit_failure TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4 (class_id, object_identity_id, field_name, ace_order), INDEX IDX_46C8B806EA000B103D9AB4A6DF9183C9 (class_id, object_identity_id, security_identity_id), INDEX IDX_46C8B806EA000B10 (class_id), INDEX IDX_46C8B8063D9AB4A6 (object_identity_id), INDEX IDX_46C8B806DF9183C9 (security_identity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A64792D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE fos_user_group ADD CONSTRAINT FK_583D1F3EA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE fos_user_group ADD CONSTRAINT FK_583D1F3EFE54D947 FOREIGN KEY (group_id) REFERENCES fos_group (id)');
        $this->addSql('ALTER TABLE media__gallery_media ADD CONSTRAINT FK_80D4C5414E7AF8F FOREIGN KEY (gallery_id) REFERENCES media__gallery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media__gallery_media ADD CONSTRAINT FK_80D4C541EA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE billing_documents ADD CONSTRAINT FK_16A310D616A310D6 FOREIGN KEY (billing_documents) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE subscription ADD CONSTRAINT FK_A3C664D32D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE vehicle_ad_description ADD CONSTRAINT FK_3D4969142D9B557A FOREIGN KEY (vehicle_ad_id) REFERENCES vehicle_ad (id)');
        $this->addSql('ALTER TABLE service_page ADD CONSTRAINT FK_DA4902CCC53D045F FOREIGN KEY (image) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE service_page ADD CONSTRAINT FK_DA4902CC2D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2659429DB FOREIGN KEY (icon) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2EF0DB8C FOREIGN KEY (pdf) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD23DA5256D FOREIGN KEY (image_id) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD22D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE inventory ADD CONSTRAINT FK_B12D4A362D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE distributor ADD CONSTRAINT FK_A3C55771E48E9A13 FOREIGN KEY (logo) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD39950C53D045F FOREIGN KEY (image) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD399502D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE sub_model_test ADD CONSTRAINT FK_D9E3C1262D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE vehicle_ad_gallery ADD CONSTRAINT FK_FF31E232D9B557A FOREIGN KEY (vehicle_ad_id) REFERENCES vehicle_ad (id)');
        $this->addSql('ALTER TABLE vehicle_ad_gallery ADD CONSTRAINT FK_FF31E233DA5256D FOREIGN KEY (image_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE news_slide ADD CONSTRAINT FK_850693CB5A459A0 FOREIGN KEY (news_id) REFERENCES news (id)');
        $this->addSql('ALTER TABLE news_slide ADD CONSTRAINT FK_850693C3DA5256D FOREIGN KEY (image_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE vehicle_ad ADD CONSTRAINT FK_9C8A9DD92D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE social_network_link ADD CONSTRAINT FK_9152F3B92D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE a_la_une ADD CONSTRAINT FK_7068117F6A2CA10C FOREIGN KEY (media) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE a_la_une ADD CONSTRAINT FK_7068117F2D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE billing_address ADD CONSTRAINT FK_6660E4562D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE distributor_time ADD CONSTRAINT FK_B297ADBB2D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D9A1887DC FOREIGN KEY (subscription_id) REFERENCES subscription (id)');
        $this->addSql('ALTER TABLE slider ADD CONSTRAINT FK_CFC710076A2CA10C FOREIGN KEY (media) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE slider ADD CONSTRAINT FK_CFC710072D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE section_order ADD CONSTRAINT FK_627EF4FCD823E37A FOREIGN KEY (section_id) REFERENCES section (id)');
        $this->addSql('ALTER TABLE section_order ADD CONSTRAINT FK_627EF4FC2D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE accessory ADD CONSTRAINT FK_A1B1251C680D9C64 FOREIGN KEY (accessory_block_id) REFERENCES accessory_block (id)');
        $this->addSql('ALTER TABLE accessory ADD CONSTRAINT FK_A1B1251CC53D045F FOREIGN KEY (image) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE accessory ADD CONSTRAINT FK_A1B1251C2D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB6206972852C FOREIGN KEY (page_block_id) REFERENCES page_block (id)');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB620C53D045F FOREIGN KEY (image) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB6202D863A58 FOREIGN KEY (distributor_id) REFERENCES distributor (id)');
        $this->addSql('ALTER TABLE honda_label_slide ADD CONSTRAINT FK_C7717112C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE honda_label_slide ADD CONSTRAINT FK_C77171123DA5256D FOREIGN KEY (image_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE page_cookies_block ADD CONSTRAINT FK_90D067A2C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE acl_object_identities ADD CONSTRAINT FK_9407E54977FA751A FOREIGN KEY (parent_object_identity_id) REFERENCES acl_object_identities (id)');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors ADD CONSTRAINT FK_825DE2993D9AB4A6 FOREIGN KEY (object_identity_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors ADD CONSTRAINT FK_825DE299C671CEA1 FOREIGN KEY (ancestor_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B806EA000B10 FOREIGN KEY (class_id) REFERENCES acl_classes (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B8063D9AB4A6 FOREIGN KEY (object_identity_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B806DF9183C9 FOREIGN KEY (security_identity_id) REFERENCES acl_security_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user_group DROP FOREIGN KEY FK_583D1F3EFE54D947');
        $this->addSql('ALTER TABLE fos_user_group DROP FOREIGN KEY FK_583D1F3EA76ED395');
        $this->addSql('ALTER TABLE media__gallery_media DROP FOREIGN KEY FK_80D4C5414E7AF8F');
        $this->addSql('ALTER TABLE media__gallery_media DROP FOREIGN KEY FK_80D4C541EA9FDD75');
        $this->addSql('ALTER TABLE billing_documents DROP FOREIGN KEY FK_16A310D616A310D6');
        $this->addSql('ALTER TABLE service_page DROP FOREIGN KEY FK_DA4902CCC53D045F');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2659429DB');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2EF0DB8C');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD23DA5256D');
        $this->addSql('ALTER TABLE distributor DROP FOREIGN KEY FK_A3C55771E48E9A13');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD39950C53D045F');
        $this->addSql('ALTER TABLE vehicle_ad_gallery DROP FOREIGN KEY FK_FF31E233DA5256D');
        $this->addSql('ALTER TABLE news_slide DROP FOREIGN KEY FK_850693C3DA5256D');
        $this->addSql('ALTER TABLE a_la_une DROP FOREIGN KEY FK_7068117F6A2CA10C');
        $this->addSql('ALTER TABLE slider DROP FOREIGN KEY FK_CFC710076A2CA10C');
        $this->addSql('ALTER TABLE accessory DROP FOREIGN KEY FK_A1B1251CC53D045F');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB620C53D045F');
        $this->addSql('ALTER TABLE honda_label_slide DROP FOREIGN KEY FK_C77171123DA5256D');
        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840D9A1887DC');
        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A64792D863A58');
        $this->addSql('ALTER TABLE subscription DROP FOREIGN KEY FK_A3C664D32D863A58');
        $this->addSql('ALTER TABLE service_page DROP FOREIGN KEY FK_DA4902CC2D863A58');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD22D863A58');
        $this->addSql('ALTER TABLE inventory DROP FOREIGN KEY FK_B12D4A362D863A58');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD399502D863A58');
        $this->addSql('ALTER TABLE sub_model_test DROP FOREIGN KEY FK_D9E3C1262D863A58');
        $this->addSql('ALTER TABLE vehicle_ad DROP FOREIGN KEY FK_9C8A9DD92D863A58');
        $this->addSql('ALTER TABLE social_network_link DROP FOREIGN KEY FK_9152F3B92D863A58');
        $this->addSql('ALTER TABLE a_la_une DROP FOREIGN KEY FK_7068117F2D863A58');
        $this->addSql('ALTER TABLE billing_address DROP FOREIGN KEY FK_6660E4562D863A58');
        $this->addSql('ALTER TABLE distributor_time DROP FOREIGN KEY FK_B297ADBB2D863A58');
        $this->addSql('ALTER TABLE slider DROP FOREIGN KEY FK_CFC710072D863A58');
        $this->addSql('ALTER TABLE section_order DROP FOREIGN KEY FK_627EF4FC2D863A58');
        $this->addSql('ALTER TABLE accessory DROP FOREIGN KEY FK_A1B1251C2D863A58');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB6202D863A58');
        $this->addSql('ALTER TABLE news_slide DROP FOREIGN KEY FK_850693CB5A459A0');
        $this->addSql('ALTER TABLE section_order DROP FOREIGN KEY FK_627EF4FCD823E37A');
        $this->addSql('ALTER TABLE vehicle_ad_description DROP FOREIGN KEY FK_3D4969142D9B557A');
        $this->addSql('ALTER TABLE vehicle_ad_gallery DROP FOREIGN KEY FK_FF31E232D9B557A');
        $this->addSql('ALTER TABLE accessory DROP FOREIGN KEY FK_A1B1251C680D9C64');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB6206972852C');
        $this->addSql('ALTER TABLE honda_label_slide DROP FOREIGN KEY FK_C7717112C4663E4');
        $this->addSql('ALTER TABLE page_cookies_block DROP FOREIGN KEY FK_90D067A2C4663E4');
        $this->addSql('ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B806EA000B10');
        $this->addSql('ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B806DF9183C9');
        $this->addSql('ALTER TABLE acl_object_identities DROP FOREIGN KEY FK_9407E54977FA751A');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors DROP FOREIGN KEY FK_825DE2993D9AB4A6');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors DROP FOREIGN KEY FK_825DE299C671CEA1');
        $this->addSql('ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B8063D9AB4A6');
        $this->addSql('DROP TABLE fos_group');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('DROP TABLE fos_user_group');
        $this->addSql('DROP TABLE media__gallery_media');
        $this->addSql('DROP TABLE media__gallery');
        $this->addSql('DROP TABLE media__media');
        $this->addSql('DROP TABLE billing_documents');
        $this->addSql('DROP TABLE subscription');
        $this->addSql('DROP TABLE vehicle_ad_description');
        $this->addSql('DROP TABLE service_page');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE inventory');
        $this->addSql('DROP TABLE distributor');
        $this->addSql('DROP TABLE news');
        $this->addSql('DROP TABLE sub_model_test');
        $this->addSql('DROP TABLE vehicle_ad_gallery');
        $this->addSql('DROP TABLE section');
        $this->addSql('DROP TABLE news_slide');
        $this->addSql('DROP TABLE vehicle_ad');
        $this->addSql('DROP TABLE social_network_link');
        $this->addSql('DROP TABLE a_la_une');
        $this->addSql('DROP TABLE billing_address');
        $this->addSql('DROP TABLE accessory_block');
        $this->addSql('DROP TABLE distributor_time');
        $this->addSql('DROP TABLE payment');
        $this->addSql('DROP TABLE slider');
        $this->addSql('DROP TABLE section_order');
        $this->addSql('DROP TABLE accessory');
        $this->addSql('DROP TABLE page_block');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE honda_label_slide');
        $this->addSql('DROP TABLE page_cookies_block');
        $this->addSql('DROP TABLE acl_classes');
        $this->addSql('DROP TABLE acl_security_identities');
        $this->addSql('DROP TABLE acl_object_identities');
        $this->addSql('DROP TABLE acl_object_identity_ancestors');
        $this->addSql('DROP TABLE acl_entries');
    }
}
