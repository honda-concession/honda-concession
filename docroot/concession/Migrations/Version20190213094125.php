<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190213094125 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE distributor ADD seo_content_description_page_vo LONGTEXT DEFAULT NULL, ADD seo_content_description_page_services LONGTEXT DEFAULT NULL, ADD seo_content_description_page_accessoires LONGTEXT DEFAULT NULL, ADD seo_content_description_page_fiche_vo LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE distributor DROP seo_content_description_page_vo, DROP seo_content_description_page_services, DROP seo_content_description_page_accessoires, DROP seo_content_description_page_fiche_vo');
    }
}
