<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190212140418 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE slider DROP FOREIGN KEY FK_CFC710077BE9645A');
        $this->addSql('DROP INDEX IDX_CFC710077BE9645A ON slider');
        $this->addSql('ALTER TABLE slider CHANGE imagethumb image_youtube INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slider ADD CONSTRAINT FK_CFC710076568CEE8 FOREIGN KEY (image_youtube) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_CFC710076568CEE8 ON slider (image_youtube)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE slider DROP FOREIGN KEY FK_CFC710076568CEE8');
        $this->addSql('DROP INDEX IDX_CFC710076568CEE8 ON slider');
        $this->addSql('ALTER TABLE slider CHANGE image_youtube imageThumb INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slider ADD CONSTRAINT FK_CFC710077BE9645A FOREIGN KEY (imageThumb) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_CFC710077BE9645A ON slider (imageThumb)');
    }
}
