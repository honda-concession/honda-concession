concession
==========

### pour deployer en recette

Uniquement pour la recette/dev il y'a un fichier makefile dans le projet créet des alias; Ainsi pour deployer le projet en recette
il faut allez dans le dossier ansible du projet, copier le fichier **[vars.yml.dist](../ansible/vars.yml.dist)** -> **vars.yml** et changer la valeur du parametre **local_projet_path**
ensuite lancer la commande "make deploy-recette"

Note: il faut installer [ansible](https://docs.ansible.com/) au prèalable



### Installation rapide du projet

Un fichier makefile permet de lancer rapidement des commandes ou shortcuts, vous pouvez jeter un coup d'oeil    . Ces commandes sont prevu
pour les phases de dev et biensûr doivent supprimer en production

Note : Pour installer un projet lancer la commande **make all-install** 
- si votre base n'existe pas encore vous pouvez lancer la commande **make create-db all-install**


### Creation d'un  site concessionaire

On crée un site avec la commande : 
php bin/console  site:create

Ensuite on crée un user et le distributeur qui vas avec la commande:
php bin/console  honda:create-user-distributor
