import sys
import os
from fabric.api import local, run, cd, env
from fabric.contrib.project import rsync_project
from fabric.context_managers import shell_env
import time
import shutil
from os import mkdir
from os.path import join, exists


RSYNC_EXCLUDE = (
    'fabfile.py',
    'makefile',
    'fabfile.pyc',
    'var/logs/*',
    'var/logs/',
    'var/cache/*',
    'var/cache/',
    'var/sessions/*',
    'var/sessions/',
    'web/uploads/*',
    'web/public/*',
    'web/uploads/',
    'web/public/',
    '.git',
    '.gitignore',
    'makefile',
    '.idea/',
    'nbproject/',
    'README.md',
    'vendor/',
    'sites/'
    'sites/*',
    'sites/sites.yml'
)

env.use_ssh_config = True
env.home = "/home/prodigious/concession.honda-occasions.kora.pro/concession"
env.exclude = RSYNC_EXCLUDE
env.php = 'php7.2'
env.backup_dir = '/home/prodigious/backup'


def kora():
    env.host_string = 'hondacorapreprod'
    env.environment = 'kora'

def prod_setting():
    env.host_string = 'hondacoraprod'
    env.environment = 'prod'

def hello():
    run("echo 'run'")

'''
def backup_bdd():
    with cd(env.home):
        timestamp = str(time.time())
        sqlFilename = 'concession_' + timestamp + '.sql'
        run("mysqldump -uprodigious-sql -po8MfERH9Lgxr moto_concession_honda_occasions > " + sqlFilename)

def backup_prod_bdd():
    prod_setting()
    backup_bdd()

def backup():

    run("mkdir -m 777 " + env.backup_dir)
    print env.backup_dir + "created"

    timestamp = str(time.time())
    destDir = join(env.backup_dir, timestamp)

'''


def rsync():
    rsync_project(
        env.home, os.getcwd() + "/", exclude=env.exclude, delete=False)

def ls():
  with cd(env.home):
        run("ls")

def cc():
  with cd(env.home):
        run(env.php + " bin/console cache:clear --env=prod ")

def composer():
  with cd(env.home):
        run(env.php + " ../../composer.phar install")

def parametersYml():
    with cd(env.home):
        if env.environment == 'prod':
            run("cd app/config && cp parameters_prod.yml parameters.yml")
            run("cd app_distributor/config && cp parameters_prod.yml parameters.yml")
            run(env.php + " bin/console site:config:sync")
        else:
            run("cd app/config && cp parameters_kora_preprod.yml parameters.yml")
            run("cd app_distributor/config && cp parameters_kora_preprod.yml parameters.yml")
            run(env.php + " bin/console site:config:sync")

def assets():
    with cd(env.home):
        run(env.php + " bin/console assets:install web --symlink --env=prod")
        run(env.php + " bin/console assetic:dump --env=prod --no-debug")

def optimizeAutoload():
    with cd(env.home):
        run("composer.phar dumpautoload -o")


def buildJs():
    local("yarn encore production")

def symLink():
    with cd(env.home):
        run("ln -s "+ env.home +"/web " + env.home)

def removeCache():
    with cd(env.home + "/var"):
        run("rm -rf cache/")

def permission():
    with cd(env.home + "/var"):
        run("chmod -R 777 cache/")
        run("chmod -R 777 logs/")
        run("chmod -R 777 sessions/")

def prod():
    prod_setting()
    buildJs()
    assets()
    rsync()
    composer()
    parametersYml()
    removeCache()
    cc()
    permission()
    assets()


def deployKora():
    kora()
    buildJs()
    assets()
    rsync()
    composer()
    parametersYml()
    assets()
    cc()
