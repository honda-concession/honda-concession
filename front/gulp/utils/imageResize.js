const gutil = require( 'gulp-util' );
const through = require( 'through2' );
const sharp = require( 'sharp' );

const PLUGIN_NAME = 'image-resizer';

module.exports = function( options ){
  return through.obj( function( file, encoding, callback ) {
    if( file.isNull()){
      this.push( file );

      return callback();
    }

    if( file.isStream()){
      this.emit( 'error', new gutil.PluginError( PLUGIN_NAME, 'Received a stream... Streams are not supported. Sorry.' ));

      return callback();
    }

    if( file.isBuffer()){
      const image = sharp( file.contents );

      image
        .metadata()
        .then( function( metadata ){
          return image
            .resize( Math.round( metadata.width * options.scale || 0.5 ), Math.round( metadata.height * options.scale || 0.5 ))
            .toBuffer();
        })
        .then(() => {
          var newFile = new gutil.File({
            cwd: file.cwd,
            base: file.base,
            path: file.path,
            contents: image
          });

          callback( null, newFile );
        });
    }
  });
};
