const notify = require( 'gulp-notify' );

module.exports = ( ...args ) => {

  // Send error to notification center with gulp-notify
  Reflect.apply( notify.onError({
    title: 'Compile Error',
    message: '<%= error %>'
  }), this, args );

  // Keep gulp from hanging on this task
  if( 'emit' in this ){
    this.emit( 'end' );
  }
};
