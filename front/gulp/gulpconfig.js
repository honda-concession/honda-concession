// Config by S W I T C H
const handleErrors = require( './utils/handleErrors' );
const plumber = require( 'gulp-plumber' );
const through = require( 'through2' );

const assets = './assets';
const dest = './dist';
const src = './src';
const views = `${src}/views`;

module.exports = {
  browsers: 'last 2 versions, not ie 10',
  build: [ 'copy', 'svg', 'images', 'resize', 'js', 'vendors', 'css', 'html' ],
  dev: process.env.NODE_ENV !== 'production',
  catchError: () => {
    return plumber({
      errorHandler: handleErrors
    });
  },
  locals: {},
  noop: through.obj,
  path: {
    assets,
    dest,
    src,
    views
  },
  tasks: {},
  taskConfig: {}
};
