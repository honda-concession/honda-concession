// Task by SWITCH
const task = 'config';
const globalConfig = require( '../gulpconfig' );
const gulp = require( 'gulp' );
const browserSync = require( 'browser-sync' );
const filendir = require( 'filendir' );
const tap = require( 'gulp-tap' );
const yaml = require( 'gulp-yaml' );

const commonConf = `${globalConfig.path.src}/config/*.yaml`;

const config = globalConfig.taskConfig[ task ] || {
  scripts: {
    src: [commonConf, `${globalConfig.path.src}/config/scripts/**/*.yaml`],
    var: 'config',
    dest: `${globalConfig.path.dest}/config/config.js`
  },
  page: {
    src: `${globalConfig.path.src}/config/scripts/page/**/*.yaml`,
    var: 'page',
    dest: `${globalConfig.path.dest}/config/page.js`
  },
  views: {
    src: [commonConf, `${globalConfig.path.src}/config/views/**/*.yaml`]
  },
  styles: {
    src: [commonConf, `${globalConfig.path.src}/config/styles/**/*.yaml`],
    dest: `${globalConfig.path.src}/styles/config.json`,
    prefix: '$'
  }
};

const watch = {};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  watch[ taskKey ] = {
    src: params.src
  };

  gulp.task( taskName, () => {
    var obj = {};

    return gulp.src( params.src, {
      base: params.base || null
    })
      .pipe( globalConfig.catchError())
      .pipe( yaml())
      .pipe( tap( file => {
        let json = {};

        try {
          json = JSON.parse( file.contents );
        }
        catch( e ) {}

        Object.assign( obj, json );
      }))
      .on( 'end', () => {

        if( Object.keys(obj).length == 0 ){
          return;
        }

        if( !params.dest ){
          Object.assign( globalConfig.locals, obj );
        }
        else{
          let txt;

          if( params.prefix ){
            for( let key in obj ){
              obj[params.prefix+key] = obj[key];
              delete obj[key];
            }
          }

          if( params.var ){
            txt = [ 'window.', params.var, ' = ', JSON.stringify( obj, null, 2 ), ';' ].join( '' );
          }
          else{
            txt = JSON.stringify( obj, null, 2 );
          }

          filendir.writeFileSync( params.dest, txt );
        }

        browserSync.reload();
      });
  });

  return taskName;
});

module.exports = { tasks, watch };
