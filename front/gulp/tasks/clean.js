// Task by SWITCH
const task = 'clean';

const gulp = require( 'gulp' );
const clean = require( 'gulp-clean' );
const globalConfig = require( '../gulpconfig' );
const path = require( 'path' );

const config = {
  main: {
    src: globalConfig.path.dest
  }
};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  gulp.task( taskName, () => {
    return gulp.src( params.src, {
      read: false
    })
      .pipe( clean({
        force: true
      }));
  });

  return taskName;
});

module.exports = { config, tasks };
