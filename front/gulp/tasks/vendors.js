// Task by SWITCH
const task = 'vendors';
const globalConfig = require( '../gulpconfig' );
const gulp = require( 'gulp' );
const browserSync = require( 'browser-sync' );
const uglifyjs = require( 'uglify-js' );
const tap = require( 'gulp-tap' );

const config = globalConfig.taskConfig[ task ] || {
  main: {
    src: `${globalConfig.path.src}/scripts/vendors/*.js`,
    dest: `${globalConfig.path.dest}/js/`
  }
};

const watch = {};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  watch[ taskKey ] = {
    src: params.src
  };

  gulp.task( taskName, () => {
    return gulp.src( params.src, {
      base: params.base || null
    })
      .pipe( globalConfig.catchError())
      .pipe( tap( file => {
        const uglified = uglifyjs.minify( file.path );

        file.contents = Buffer.from( uglified.code );
      }))
      .pipe( gulp.dest( params.dest ))
      .on( 'end', () => {
        browserSync.reload();
      });
  });

  return taskName;
});

module.exports = { tasks, watch };
