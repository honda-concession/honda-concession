// Task by SWITCH
const task = 'html';
const globalConfig = require( '../gulpconfig' );
const gulp = require( 'gulp' );
const pug = require( 'gulp-pug' );
const path = require( 'path' );
const tap = require( 'gulp-tap' );
const rename = require( 'gulp-rename' );
const htmlBeautify = require( 'js-beautify' ).html;

const config = globalConfig.taskConfig[ task ] || {
  pages: {
    src: `${globalConfig.path.src}/views/pages/**/*.pug`,
    dest: globalConfig.path.dest,
    pages: true
  },
  modules: {
    src: `${globalConfig.path.src}/views/{atoms,organisms,molecules,modules}/**/*.pug`,
    dest: globalConfig.path.dest
  },
  sub: {
    base: `${globalConfig.path.src}/views/`,
    src: `${globalConfig.path.src}/views/ajax/**/*.pug`,
    dest: globalConfig.path.dest
  }
};

const beautifyOptions = {
  'indent_size': 2,
  'indent_level': 0,
  'space_in_paren': true,
  'unformatted': 'svg'
};

const deps = [ 'config:views', 'tree', 'json' ];

const watch = {
  change: {
    events: [ 'change' ],
    reload: true,
    src: [ `${globalConfig.path.src}/views/**/*.pug`, `${globalConfig.path.src}/locales/**/*.yaml` ]
  }
};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  gulp.task( taskName, () => {
    let indexExists = false;

    return gulp.src( params.src, {
      base: params.base || null
    })
      .pipe( globalConfig.catchError())
      .pipe( pug({
        doctype: 'html',
        pretty: true,
        locals: params.pages ? Object.assign({filePath: '/pages/'}, globalConfig.locals) : globalConfig.locals,
        basedir: globalConfig.path.views
      }))
      .pipe( tap(( currentFile, then ) => {
        const filename = path.basename( currentFile.path, '.html' );

        if( !indexExists && filename === 'index' ){
          indexExists = true;
        }

        if( !indexExists && filename === 'treeview' ){
          return then.through( rename, [{
            basename: 'index'
          }]);
        }
      }))
      .pipe( tap( file => {
        file.contents = new Buffer( htmlBeautify( file.contents.toString( 'utf-8' ), beautifyOptions ));
      }))
      .pipe( gulp.dest( params.dest ));
  });

  return taskName;
});

module.exports = { deps, tasks, watch };
