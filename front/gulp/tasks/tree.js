// Task by SWITCH
const task = 'tree';
const globalConfig = require( '../gulpconfig' );
const browserSync = require( 'browser-sync' );
const directoryMap = require( 'gulp-directory-map' );
const gulp = require( 'gulp' );
const tap = require( 'gulp-tap' );

const config = globalConfig.taskConfig[ task ] || {
  src: [
    `${globalConfig.path.src}/views/**/*.pug`,
    `!${globalConfig.path.src}/views/{ajax,includes,layout,tools}/**/*.pug`
  ],
  dest: 'tree.json'
};

const watch = {
  main: {
    events: [ 'add', 'unlink' ],
    src: config.src
  }
};

const taskName = `${task}:main`;

gulp.task( taskName, () => {
  const tree = {};

  return gulp.src( config.src )
    .pipe( globalConfig.catchError())
    .pipe( directoryMap({
      filename: config.dest
    }))
    .pipe( tap( file => {
      Object.assign( tree, JSON.parse( file.contents ));
    }))
    .on( 'end', () => {
      globalConfig.locals.tree = tree;
      browserSync.reload();
    });
});

module.exports = { tasks: [ taskName ], watch };
