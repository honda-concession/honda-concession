// Task by SWITCH
const task = 'css';
const globalConfig = require( '../gulpconfig' );
const autoprefixer = require( 'gulp-autoprefixer' );
const browserSync = require( 'browser-sync' );
const chouchenn = require( 'chouchenn' );
const cleanCSS = require( 'gulp-clean-css' );
const gulp = require( 'gulp' );
const sourcemaps = require( 'gulp-sourcemaps' );
const stylus = require( 'gulp-stylus' );

const config = globalConfig.taskConfig[ task ] || {
  main: {
    dest: `${globalConfig.path.dest}/css`,
    src: `${globalConfig.path.src}/styles/*.styl`,
    watch: {
      src: `${globalConfig.path.src}/styles/**/*.styl`
    }
  }
};

const deps = [ 'config:styles' ];

const watch = {};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  if( params.watch ){
    watch[ taskKey ] = params.watch;
  }

  gulp.task( taskName, () => {
    return gulp.src( params.src )
      .pipe( globalConfig.catchError())
      .pipe( globalConfig.dev ? sourcemaps.init() : globalConfig.noop())
      .pipe( stylus({
        use: [ chouchenn() ],
        import: [ 'chouchenn' ],
        compress: false,
        rawDefine: params.data || {}
      }))
      .pipe( autoprefixer({
        browsers: params.browsers
      }))
      .pipe( globalConfig.dev ? sourcemaps.write() : cleanCSS({
        keepBreaks: true,
        restructuring: false
      }))
      .pipe( gulp.dest( params.dest ))
      .pipe( globalConfig.dev ? browserSync.reload({
        stream: true
      }) : globalConfig.noop());
  });

  return taskName;
});

module.exports = { deps, tasks, watch };
