// Task by SWITCH
const task = 'server';
const browserSync = require( 'browser-sync' );
const globalConfig = require( '../gulpconfig' );
const gulp = require( 'gulp' );
const pug = require( 'pug' );
const urlParser = require( 'url' );
const bodyParser = require( 'body-parser' );
const path = require( 'path' );
const util = require( 'util' );
const fs = require( 'fs' );
const fsAccess = util.promisify( fs.access );
const handleError = require( '../utils/handleErrors' );

let filePath;

const config = globalConfig.taskConfig[ task ] || {
  server: {
    // Serve up our build folder
    baseDir: globalConfig.path.dest
  },
  ghostMode: false,
  logSnippet: false,
  open: false,
  notify: false,
  ui: false
};

const compileDom = ( dir, filename ) => {
  const file = `${filename}.pug`;
  // compile the pug file
  const directory = path.join( 'src/views', dir, file );

  filePath = directory;

  return new Promise( resolve => {

    return fsAccess( directory )
      .catch(() => true )
      .then( status => resolve( !status ));
  })
    .then( fileExists => {
      if( fileExists ){
        return pug.compileFile( directory, {
          basedir: globalConfig.path.views,
          pretty: true
        });
      }

      return compileDom( '/pages', filename === 'index' ? 'treeview' : 'index' );
    });
};

const middlewares = [
  // compile pug file
  bodyParser.json(),
  bodyParser.urlencoded({ extended: false }),
  ( req, res, next ) => {
    if( req.method === 'POST' ){
      req.method = 'GET';
    }

    next();
  },
  // compile pug file on the fly
  ( req, res, next ) => {

    const urlParts = urlParser.parse( req.originalUrl, true );
    const originalUrl = urlParts.pathname;
    const url = path.parse( originalUrl );
    const { ext } = url;

    // filter non html pages
    if( '.html' !== ext && '' !== ext ){
      return next();
    }

    const locals = {};

    if( urlParts.query ){
      locals.get = urlParts.query;
    }

    if( req.body ){
      locals.post = req.body;
    }

    let dir = url.dir === '/' ? `/pages/${url.dir}` : url.dir;
    const filename = url.name === '' || originalUrl.slice( -1 ) === '/' ? 'index' : url.name;

    if( originalUrl.slice( -1 ) === '/' && url.base === url.name ){
      dir = path.join( dir, url.base );
    }

    res.setHeader( 'Content-Type', 'text/html; charset=utf-8' );

    return compileDom( dir, filename )
      .then( dom => {
        locals.fileFullPath = path.join(__dirname, filePath);
        locals.filePath = filePath;
        // render and send pug file
        res.end( dom( Object.assign( locals, globalConfig.locals )));
      })
      .catch( err => {

        locals.err = err;

        handleError( err );

        // display error message
        const dom = pug.compileFile( 'src/views/tools/compile-error.pug', {
          basedir: globalConfig.path.views,
          pretty: true
        });

        res.end( dom( Object.assign( locals, globalConfig.locals )));
      })
      .catch( err => {
        handleError( err );
        res.end( err );
      });

  }
];

config.middleware = middlewares;

gulp.task( 'server', [ 'config:views', 'tree' ], () => {
  browserSync( config );
});
