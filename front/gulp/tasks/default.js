// Task by SWITCH
const task = 'default';
const globalConfig = require( '../gulpconfig' );
const gulp = require( 'gulp' );
const runSequence = require( 'run-sequence' );

const config = globalConfig.taskConfig[ task ] || [
  'clean',
  globalConfig.build
];

gulp.task( task, () => {
  runSequence.apply( runSequence, config );
});
