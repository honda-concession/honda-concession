# Intro
Ce projet est basé sur une architecture de tâches Gulp créée par [Switch](https://switch.paris).

# Task structure

## Export

* `deps`: array of tasks to run before running the current sub tasks
* `tasks`: sub tasks array
* `watch`: configuration object of each sub task

### `watch` object

```json
{
  "main": {
    "events": ["events to watch for"],
    "deps": ["array of tasks to run before running the current tasks"],
    "reload": boolean, //just do a browser reload
    "src": ["path to files to watch"],
    "task": ["tasks to run"]
  }
}
```
