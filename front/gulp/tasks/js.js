// Task by SWITCH
const task = 'js';
const globalConfig = require( '../gulpconfig' );
const gulp = require( 'gulp' );
const browserSync = require( 'browser-sync' );
const rollup = require( 'rollup' ).rollup;
const commonjs = require( 'rollup-plugin-commonjs' );
const nodeResolve = require( 'rollup-plugin-node-resolve' );
const uglify = require( 'rollup-plugin-uglify' );
const babel = require( 'rollup-plugin-babel' );

const config = globalConfig.taskConfig[ task ] || {
  main: {
    src: `${globalConfig.path.src}/scripts/app.js`,
    name: 'app',
    watch: {
      src: [ `${globalConfig.path.src}/scripts/**/*.js`, `!${globalConfig.path.src}/scripts/vendors/*.js` ]
    },
    dest: `${globalConfig.path.dest}/js/app.js`
  }
};

const deps = [ 'config:scripts', 'config:page' ];

const watch = {};

const plugins = [
  nodeResolve(),
  commonjs(),
  babel({
    exclude: 'node_modules/!(@switch-company)/**',
    presets: [
      [ 'env', {
        modules: false,
        targets: {
          browsers: globalConfig.browsers
        }
      }]
    ]
  })
];

if( !globalConfig.dev ){
  plugins.push( uglify());
}

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  if( params.watch ){
    watch[ taskKey ] = params.watch;
  }

  const rollupIn = {
    input: params.src,
    plugins
  };

  const rollupOut = {
    file: params.dest,
    format: 'iife',
    name: params.name,
    sourcemap: globalConfig.dev
  };

  gulp.task( taskName, () => {
    return rollup( rollupIn )
      .then( bundle => bundle.write( rollupOut ))
      .then(() => {
        if( globalConfig.dev ){
          browserSync.reload();
        }
      });
  });

  return taskName;
});

module.exports = { deps, tasks, watch };
