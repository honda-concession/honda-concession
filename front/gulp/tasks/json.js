// Task by SWITCH
const task = 'json';
const globalConfig = require( '../gulpconfig' );
const browserSync = require( 'browser-sync' );
const gulp = require( 'gulp' );
const yaml = require( 'gulp-yaml' );

const config = globalConfig.taskConfig[ task ] || {
  main: {
    src: `${globalConfig.path.src}/ajax/**/*.yaml`,
    dest: `${globalConfig.path.dest}/ajax`
  }
};

const watch = {};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  watch[ taskKey ] = {
    src: params.src
  };

  gulp.task( taskName, () => {
    return gulp.src( params.src )
      .pipe( globalConfig.catchError())
      .pipe( yaml({
        space: 2
      }))
      .pipe( gulp.dest( params.dest ))
      .on( 'end', () => {
        browserSync.reload();
      });
  });

  return taskName;
});

module.exports = { tasks, watch };
