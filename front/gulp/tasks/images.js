// Task by SWITCH
const task = 'images';
const globalConfig = require( '../gulpconfig' );
const browserSync = require( 'browser-sync' );
const changed = require( 'gulp-changed' );
const gulp = require( 'gulp' );
const imagemin = require( 'gulp-imagemin' );
const jpegrecompress = require( 'imagemin-jpeg-recompress' );
const pngquant = require( 'imagemin-pngquant' );

const config = globalConfig.taskConfig[ task ] || {
  main: {
    base: `${globalConfig.path.src}/media/`,
    src: `${globalConfig.path.src}/media/{img,dyn}/**/*.{gif,jpg,jpeg,png,svg}`,
    dest: `${globalConfig.path.dest}/media`
  }
};

const watch = {};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  watch[ taskKey ] = {
    src: params.src
  };

  gulp.task( taskName, () => {
    return gulp.src( params.src, {
      base: params.base
    })
      .pipe( globalConfig.catchError())
      // Ignore unchanged files
      .pipe( changed( params.dest ))
      .pipe( imagemin([
        imagemin.gifsicle(),
        imagemin.svgo({
          plugins: [
            { removeDoctype: false },
            { cleanupIDs: false }
          ]
        })
      ],
      jpegrecompress({
        max: 85
      }),
      pngquant({
        quality: '65-80'
      })
      ))
      // Optimize
      .pipe( gulp.dest( params.dest ))
      .pipe( browserSync.reload({
        stream: true
      }));
  });

  return taskName;
});

module.exports = { tasks, watch };
