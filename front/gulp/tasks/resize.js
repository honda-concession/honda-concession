// Task by SWITCH
const task = 'resize';
const globalConfig = require( '../gulpconfig' );
const changed = require( 'gulp-changed' );
const gulp = require( 'gulp' );
const buffer = require( 'vinyl-buffer' );
const imageResize = require( '../utils/imageResize' );
const imagemin = require( 'gulp-imagemin' );
const jpegrecompress = require( 'imagemin-jpeg-recompress' );
const pngquant = require( 'imagemin-pngquant' );
const rename = require( 'gulp-rename' );

const config = globalConfig.taskConfig[ task ] || {
  main: {
    src: `${globalConfig.path.src}/media/img/2x/**/*.{jpg,jpeg,png}`,
    dest: `${globalConfig.path.dest}/media/img/1x`
  }
};

const plugin = [ imagemin.gifsicle(),
  jpegrecompress({
    max: 85
  }),
  pngquant({
    quality: '80-90'
  })
];

const watch = {};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  watch[ taskKey ] = {
    src: params.src
  };

  gulp.task( taskName, () => {
    return gulp.src( params.src )
      .pipe( globalConfig.catchError())
      // Ignore unchanged files
      .pipe( changed( params.dest ))
      .pipe( imageResize({
        scale: 0.5
      }))
      .pipe( buffer())
      // Optimize
      .pipe( imagemin( params.plugin || plugin ))
      .pipe( rename( function( file ){
        if( params.ext ){
          file.extname = params.ext;
        }

        return file;
      }))
      .pipe( gulp.dest( params.dest ));
  });

  return taskName;
});

module.exports = { tasks, watch };
