// Task by SWITCH
const task = 'svg';
const globalConfig = require( '../gulpconfig' );
const browserSync = require( 'browser-sync' );
const gulp = require( 'gulp' );
const svgmin = require( 'gulp-svgmin' );
const concat = require( 'gulp-concat' );
const cheerio = require( 'cheerio' );
const tap = require( 'gulp-tap' );
const path = require( 'path' );

const config = globalConfig.taskConfig[ task ] || {
  main: {
    src: './assets/ui/**/*.svg',
    watch: {
      src: './assets/ui/**/*.svg'
    },
    dest: `${globalConfig.path.src}/views/tools/`
  }
};

const watch = {};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  if( params.watch ){
    watch[ taskKey ] = params.watch;
  }

  gulp.task( taskName, () => {
    return gulp.src( params.src )
      .pipe( globalConfig.catchError())
      .pipe( svgmin({
        plugins: [{
          removeAttrs: {
            attrs: [ 'stroke', 'fill-opacity', 'stroke-opacity', 'xmlns', 'id' ]
          }
        },
        {
          mergePaths: true
        },
        {
          convertStyleToAttrs: true
        },
        {
          removeStyleElement: true
        },
        {
          collapseGroups: true
        }],
      }))
      .pipe( tap( file => {
        const filename = path.basename( file.path, '.svg' );
        const classname = ( `ui ui--${filename}` ).replace( '_', '-' );
        const $ = cheerio.load( file.contents.toString(), { xmlMode: true });

        $( 'svg' ).attr( 'aria-hidden', 'true' );
        $( 'svg' ).addClass( classname );

        file.contents = new global.Buffer( `    when "${filename}"\n      ${$.html()}` );
      }))
      .pipe( concat( 'ui.pug' ))
      .pipe( tap( file => {
        file.contents = global.Buffer.concat([
          new global.Buffer( 'mixin ui( type )\n  case type\n' ),
          file.contents
        ]);
      }))
      .pipe( gulp.dest( params.dest ))
      .pipe( browserSync.reload({
        stream: true
      }));
  });

  return taskName;
});

module.exports = { tasks, watch };
