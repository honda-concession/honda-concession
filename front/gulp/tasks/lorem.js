// Task by SWITCH
const globalConfig = require( '../gulpconfig' );
const lorem = require( 'lorem-ipsum' );

globalConfig.locals.lorem = ( length, unit ) => {
  return lorem({
    count: length,
    units: unit || 'paragraphs'
  }).split( '\n\n' );
};
