const task = 'sprites';
const globalConfig = require( '../gulpconfig' );
const buffer = require( 'vinyl-buffer' );
const gulp = require( 'gulp' );
const merge = require( 'merge-stream' );
const spritesmith = require( 'gulp.spritesmith' );
const fs = require( 'fs' );

const config = {
  cssTemplate: `${globalConfig.path.assets}/templates/sprites/stylus.template.handlebars`,
  imgPath: '../media/img/1x/',
  imgPath2x: '../media/img/2x/',
  destSprite2x: `${globalConfig.path.src}/media/img/2x/`,
  destStyle: `${globalConfig.path.src}/styles/sprites/`,
  base: `${globalConfig.path.assets}/sprites/`
};

config.folders = fs.existsSync( config.base ) ? fs.readdirSync( config.base ).map( name => {
  if( fs.lstatSync( `${config.base}${name}` ).isDirectory()){
    return name;
  }
}).filter( Boolean ) : [];

// Sprite generator
const tasks = config.folders.map( taskKey => {
  const params = Object.assign({
    cssName: `${taskKey}.styl`,
    cssPrefix: `sprite-${taskKey}`,
    imgName: `${taskKey}.png`,
    src: `${config.base}${taskKey}/**/*.{png,jpg}`
  }, config );
  const taskName = `${task}:${taskKey}`;

  gulp.task( taskName, () => {

    // Generate our spritesheet
    const spriteData = gulp.src( params.src )
      .pipe( spritesmith({
        imgPath: params.imgPath,
        imgName: params.imgName,
        cssName: params.cssName,
        cssTemplate: params.cssTemplate,
        cssOpts: {
          functions: true,
          cssPrefix: params.cssPrefix,
          spriteImg: params.imgName,
          spritePath: params.imgPath,
          spritePath2x: params.imgPath2x,
        },
        cssSpritesheetName: taskKey,
        cssHandlebarsHelpers: {
          half: function( value ){
            return value / 2;
          },
          json: function( obj ){
            return JSON.stringify( obj, null, 2 );
          },
          uppercase: function( str ){
            return str.toUpperCase();
          }
        }
      }));

    // Pipe image stream through image optimizer and onto disk
    const retinaStream = spriteData.img
      .pipe( buffer())
      .pipe( gulp.dest( params.destSprite2x ));

    // Pipe CSS stream through CSS optimizer and onto disk
    const cssStream = spriteData.css
      .pipe( gulp.dest( params.destStyle ));

    // Return a merged stream to handle both `end` events
    return merge( retinaStream, cssStream );
  });

  return taskName;
});

module.exports = { tasks };
