// Task by SWITCH
const task = 'copy';
const globalConfig = require( '../gulpconfig' );
const gulp = require( 'gulp' );
const browserSync = require( 'browser-sync' );

const config = globalConfig.taskConfig[ task ] || {
  fonts: {
    src: `${globalConfig.path.src}/media/fonts/**`,
    dest: `${globalConfig.path.dest}/media/fonts`
  },
  icons: {
    src: `${globalConfig.path.src}/media/icons/**`,
    dest: `${globalConfig.path.dest}/media/icons`
  }
};

const watch = {};

const tasks = Object.keys( config ).map( taskKey => {
  const params = config[ taskKey ];
  const taskName = `${task}:${taskKey}`;

  watch[ taskKey ] = {
    src: params.src
  };

  gulp.task( taskName, () => {
    return gulp.src( params.src, {
      base: params.base || null
    })
      .pipe( globalConfig.catchError())
      .pipe( gulp.dest( params.dest ))
      .pipe( browserSync.reload({
        stream: true
      }));
  });

  return taskName;
});

module.exports = { tasks, watch };
