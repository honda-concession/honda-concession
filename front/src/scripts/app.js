// polyfills
import "core-js/es6/symbol";
import smoothscroll from 'smoothscroll-polyfill';
import './polyfills/closest.js';
import './polyfills/object-assign.js';
import 'promise-polyfill/src/polyfill';
import 'whatwg-fetch';
import 'formdata-polyfill';


// Helpers
import './helpers/listener';
import './helpers/eventDelegation';
import './helpers/qsa-array';
import './helpers/index';
import './polyfills/object-fit.js';

// Tools
import ps from 'pubsub-js';


// Modules
import './modules/sliders';
import './modules/select';
import './modules/toggle';
import './modules/gallery';
import './modules/range';
import './modules/misc';
import './modules/form';


smoothscroll.polyfill();

window.locale = navigator.languages ? navigator.languages[0] : ( navigator.language ? navigator.language : document.documentElement.dataset.locale );

// Trigger app start custom event
ps.publish( 'app.start' );

new LazyLoad();


$('.carousel-cinquo').each(function(){

	console.log($(this).find('.swiper-slide').length)

	if($(this).find('.swiper-slide').length == 1)
	{
		$(this).parent().find('.custom-button').addClass('hide-arrow')
		$(this).parent().find('.swiper-pagination').hide()
	}
	// console.log($(this).length)
})

