// Cookies manager
var Cookie = {

    create: function (name, value, days, secure) {

        var expires = "";
        var days = days || 1;
        var secure;

        if (secure) {
            secure = 'secure;';
        }

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }

        document.cookie = name + "=" + value + expires + "; "+ secure +" path=/;samesite=strict";
    },

    read: function (name) {

        var nameEQ = name + "=";
        var ca = document.cookie.split(";");

        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " ") c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }

        return null;
    },

    erase: function (name) {
        Cookie.create(name, "", -1);
    }

};

var gaCode
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

function callAnalytics(){
    
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()

    { (i[r].q=i[r].q||[]).push(arguments)}
        ,i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', gaCode, 'auto');
    ga('send', 'pageview');
}  

// function removeAnalytics(){
    
//     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()

//     { (i[r].q=i[r].q||[]).push(arguments)}
//         ,i[r].l=1*new Date();a=s.createElement(o),
//         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//     })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

//     ga('create', gaCode, 'auto');
//     ga('send', 'pageview');
// }  



$('#refused').change(function(e) {
    e.preventDefault;
    Cookie.erase('cookies_policy');
    $('#cookies').attr('aria-hidden', 'false')
    $("#agreementbando").prop("checked", false);
    // removeAnalytics()
});

$('#agreed').change(function(e) {
    e.preventDefault;
    hideCookie()
    
});

$('#agreementbando').click(function(e) {
    e.preventDefault;
    hideCookie()
    $("#agreed").prop("checked", true);
});


function hideCookie()
{
    console.log("accept cookie")
    Cookie.create('cookies_policy', true, 21, true);
    $('#cookies').attr('aria-hidden', 'true')
    callAnalytics()
}

(function($) {

    $(function() {      
        var userCookie = getCookie("cookies_policy");
        console.log(userCookie)
        gaCode = $('#google-analytics').attr('data-code')
        if(userCookie == 'true')
        {
            console.log('has cookie')

            $("#agreed").prop("checked", true);
            // $("#agreed-bandeau").prop("checked", true);

            // $('.agreement').each(function(){

            //     $(this).parent().find('#agreed').prop("checked", true);
             
            // })
            
           
            callAnalytics()
        }

        else
        {
            $("#refused").prop("checked", true);
        }


        $('#bandeau-btn-open-cookie').click(function(){
         
            window.location.href = "/vie-privee-et-cookies#cookies-page"
        })

    });

})(jQuery);


