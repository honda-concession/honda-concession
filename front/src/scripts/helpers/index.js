window.Element.prototype.nodeIndex = function index(){
  return Array.from(this.parentNode.children).indexOf(this);
};
