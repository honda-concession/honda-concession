class Template {

  run(strings, ...interpolatedValues) {
    return strings.reduce((total, current, index) => {
      total += current;
      if (interpolatedValues.hasOwnProperty(index)) {
        total += String(interpolatedValues[index]);
        //.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
      }
      return total;
    }, '');
  }

  registerHelper (name, fn) {
    this.helpers[name] = fn;
  }

  constructor() {

    this.helpers = {
      if: (condition, thenTemplate, elseTemplate = '') => {
        return condition ? thenTemplate : elseTemplate;
      },
      unless: (condition, thenTemplate, elseTemplate) => {
        return !condition ? thenTemplate : elseTemplate;
      }
    }
  }
}

export default new Template();

// var page = ({ people, isAdmin }) => Template.run`
//   <h2>People</h2>
//   <ul>
//   ${people.map(person => `
//       <li>
//         <span>${person}</span>
//         ${Template.helpers.if(isAdmin,
//           `<button>Delete ${person}</button>`
//         )}
//       </li>
//   `)}
//   </ul>
// `;

// Template.registerHelper('capitalize', (string) => {
//   return string[0].toUpperCase() + string.slice(1).toUpperCase();
// });
// console.log(Template.helpers)
// console.log(page({ isAdmin: true, people: ['Keith', 'Dave', 'Amy' ] }));
