/**
 * Slider
 * @constructor
 * @requires pubsub-js
 */
class Slider {

  constructor(){

    this.init = this.init.bind(this);

    this.init();

  }

  init( dom ){

    dom = dom || document;

    this.sliders = dom.qsa('[data-sliderConfig]');

    var mySwiper = new Swiper('.product-category-wrap', {
      speed: 400,
      spaceBetween: 100,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
          return '<span class="' + className + '">' + (index + 1) + '</span>';
        }
      },
  });
  

    // var productCarousels = this.sliders
    var productCarousel;
    var sliderArr = [] 
    var slide_id;


    // $('a[data-product_slide]').each(function(){

    //   $(this).click(function(e) {
    //     e.preventDefault();
  
    //     console.log("product mobs sliders")
    //     let slideno = $(this).data('product_slide');
    //     let slidenoArr = slideno.split(" ")
    //     $('a[data-product_slide]').parent().removeClass('active')
    //     $(this).parent().addClass('active')
    //     productCarousel.slideTo($('.block-product.'+slidenoArr[0]).index());
    //   });
    // })
    


    // $('a[data-product_slide]').click(function(e) {
    //   e.preventDefault();

    //   console.log("product mob sliders")
    //   let slideno = $(this).data('product_slide');
    //   let slidenoArr = slideno.split(" ")
    //   $('a[data-product_slide]').parent().removeClass('active')
    //   $(this).parent().addClass('active')
    //   productCarousel.slideTo($('.block-product.'+slidenoArr[0]).index());
    // });


    this.sliders.forEach(function(slider, sliderIndex){



      // console.log(sliderIndex) 

      var config = JSON.parse(slider.dataset.sliderconfig);
      



      if( config['pagination'] ){

        //if(!config['pagination'])
        config.pagination = {
          el: slider.querySelector('.swiper-pagination'),
          clickable: true,
          renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (index + 1) + '</span>';
          }
        }



      }
      if( config['breakpoints'] ){
        config.breakpoints = {
          320: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          600: {
            slidesPerView: 1,
            spaceBetween: 10
          },
          1024: {
            slidesPerView: 2,
            spaceBetween: 10
          }
        }
      }
      if( config['navigation'] ){
        config.navigation = {
          nextEl: '.bt-slider.next',
          prevEl: '.bt-slider.prev',
        }
      }
      if( config['navigation-full-height'] ){
        config.navigation = {
          nextEl: '.custom-button.next',
          prevEl: '.custom-button.prev',
        }
      }

      

      // if( config['navigation-mob'] ){
      //   config.navigation = {
      //     nextEl: '.custom-button_'+config['id']+'.next',
      //     prevEl: '.custom-button_'+config['id']+'.prev',
      //   }
      // }

      if( config["external-nav"] ){
        config.on = {
          slideChange: function(){


        

          // $('.retailer-wrap .swiper-wrapper .swiper-slide').removeClass('active')
            
            if( slider.sliderDom ){
              var slides = slider.closest('[data-sliderContainer]').qsa('[data-slideTo]');
              slides.forEach( (slide) => {
                slide.classList.remove('active');
              });
              var currentSlide = slider.closest('[data-sliderContainer]').querySelector('[data-slideTo="' + parseInt(slider.sliderDom.realIndex+1,10) + '"]');
              if( currentSlide ){
                currentSlide.classList.add('active');
              }

            }
          }
        }
      }
      if(!config['external-nav']){
        config.on = {};
        config.on.init = function(){
          if( this.isBeginning && this.isEnd ){
            slider.classList.add('all-visible');
          }
        };

        config.on.resize = function(swiper){
          if( this.isBeginning && this.isEnd ){
            slider.classList.add('all-visible');
          }
          else{
            slider.classList.remove('all-visible');
          }
        }
      }


      config.on = {
        slideChange: function(){


          if(config['pagination-mob'])
          {
            
            slide_id = config.id
            let currProductSlide = this.realIndex
            $('.color-icons ul[data-slide_id='+slide_id+'] li').each(function(index){
                // console.log(index)
                if(index == currProductSlide)
                {
                  $('.color-icons ul[data-slide_id='+slide_id+'] li').removeClass('active')
                

                    $(this).addClass("active")
               
          
                }
              })

              
          }

          if(config['external-nav'])
          {
            if( slider.sliderDom ){
              var slides = slider.closest('[data-sliderContainer]').qsa('[data-slideTo]');
              slides.forEach( (slide) => {
                slide.classList.remove('active');
              });
              var currentSlide = slider.closest('[data-sliderContainer]').querySelector('[data-slideTo="' + parseInt(slider.sliderDom.realIndex+1,10) + '"]');
              if( currentSlide ){
                currentSlide.classList.add('active');
              }

            }
          }

        }
      }

      window.requestAnimationFrame(function(){
        slider.sliderDom = new Swiper(slider, config);

      

        // productCarousel = new Swiper('.mobile-product-carousel .swiper-container',{ 
        //   speed: 400,
    
        //   pagination:{
        //     el: $('.swiper-pagination'),
        //     clickable: true,
        //     renderBullet: function (index, className) {
        //       return '<span class="' + className + '">' + (index + 1) + '</span>';
        //     }
        //   },
    
        //   on:{
        //     slideChange:function(){
              
        //       let currProductSlide = this.realIndex
    
        //       $('.color-icons li').each(function(index){
        //         // console.log(index)
        //         if(index == currProductSlide)
        //         {
        //           $('.color-icons li').removeClass('active')
        //           $(this).addClass("active")
          
        //         }
        //       })
        //     }
        //   }
        
        // });

        // console.log(slider.sliderDom)

        
        if( config['pagination-mob'] )
        {
          sliderArr.push(slider.sliderDom)
          // slide_id = 1
        }

        


        $('a[data-product_slide]').click(function(e) {
          e.preventDefault();

          

          // slide_id = config.id

         

          var currProductSlide = $(this).parent().parent().data('slide_id')
    
          let slideno = $(this).data('product_slide');
          // let slidenoArr = slideno.split(" ")
          // $('.color-icons ul[data-slide_id='+slide_id+'] li').removeClass('active')
          $('.color-icons ul[data-slide_id='+slide_id+'] li').removeClass('active')
          $(this).parent().addClass('active')
  
          sliderArr[currProductSlide - 1].slideTo($('.swiper-slide-active .block-product.'+slideno).index());
       
        });


      });

      if( config.autoplay ){
        document.addEventListener('visibilitychange', function(){
          if( !slider.sliderDom ){
            return;
          }
          if( document.hidden ){
            slider.sliderDom.autoplay.stop();
          }
          else{
            slider.sliderDom.autoplay.start();
          }
        });
      }

    });

    var controls = document.querySelector('[data-slider-controls]');

    if( !document.querySelector('[data-slideTo]') ){
      return;
    }
    controls.qsa('[data-slideTo]').forEach( (control) => {

      control.addEventListener('click', function(e){
       
        var trigger = e.currentTarget,
            slide = +trigger.dataset.slideto,
            slider = trigger.closest('[data-sliderContainer]').querySelector('[data-sliderConfig]');


        // console.log(trigger)

        // $('.retailer-wrap .swiper-wrapper .swiper-slide').removeClass('active')

        // trigger.classList.add("active");


        slider.sliderDom.slideTo(slide);

      });

    });
  }

}

export default new Slider();
