/*
  gulpfile.js by [Switch](https://switch.paris)
  ============================================
  Rather than manage one giant configuration file responsible
  for creating multiple tasks, each task has been broken out into
  its own file in gulp/tasks. Any files in that directory get
  automatically required below.

  To add a new task, simply add a new task file that directory.
  gulp/tasks/default.js specifies the default set of tasks to run
  when you run `gulp`.
*/

const gulp = require( 'gulp' );
const requireDir = require( 'require-dir' );
const config = require('./gulp/gulpconfig');

// load subtasks
config.tasks = requireDir( './gulp/tasks' );

const tasks = Object.keys( config.tasks );

// create global tasks
tasks.forEach( taskName => {
  if( config.tasks[taskName].tasks ){
    if( config.tasks[taskName].deps ){
      gulp.task( taskName, config.tasks[taskName].deps, () => {
        gulp.start( config.tasks[taskName].tasks );
      });
    }
    else {
      gulp.task( taskName, config.tasks[taskName].tasks );
    }
  }
});

// watch
const watch = require( 'gulp-watch' );
const browserSync = require( 'browser-sync' );
const runSequence = require( 'run-sequence' );

gulp.task( 'watch', [ 'server' ], () => {
  tasks.forEach( taskName => {
    if( !config.tasks[taskName].watch ){
      return;
    }

    Object.keys( config.tasks[taskName].watch ).forEach( taskKey => {
      const data = config.tasks[taskName].watch[taskKey];
      const param = {};

      param.events = data.events || [ 'add', 'change', 'unlink' ];

      watch( data.src, param, () => {
        if( data.reload ){
          browserSync.reload();
        }
        else {
          const task = data.task || `${taskName}:${taskKey}`

          if( data.deps ){
            runSequence.apply( runSequence, data.deps.concat(task) );
          }
          else {
            gulp.start( task );
          }
        }
      });
    } );
  });
});
