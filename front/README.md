## Accès
honda:dikokoy

## Mode d'emploi
La structure de projet employée par [Switch](https://switch.paris) utilise [npm](https://www.npmjs.com/) et [gulp](http://gulpjs.com/) pour la compilation des fichiers de développement en livrables de production (.html, .js, .css, ...).


### Technique

#### Autoprefixer scope

`last 2 versions, not ie 10`

### Arborescence
La compilation exploite chaque dossier du projet qui contient les éléments suivants :

- **_sources/** :
  - *assets/icons/* : les pictogrammes générés en font lors du build
  - *assets/templates/* : les templates utilisés pour la génération de fichiers de styles pour les polices
  - des ressources propres au projet non compilées
- **dist/** : contient tous les fichiers *compilés* du projet
- **gulp/** : contient toutes les tâches nécessaires à la compilation

- **src/** : contient tous les fichiers *éditables* du projet
    - **config/** : fichiers de configuration potentiellement administrables en vue d'une exploitation en Javascript (ex : clés de traduction) compilés en .js dans le dossier */dist/config*
    - **locales/** : fichiers de configuration potentiellement administrables (textes, coordonnées GPS, ...) utilisés par la compilation des fichiers .pug en .html
    - **media/** :
      - *img/* : images de structure
      - *dyn/* : images administrables
      - *videos/* : vidéos
      - *fonts/* : typographies et font d'icône générée
      - *icons/* : fichiers d'icône (favicon, icônes pour les périphériques tactiles, ...)
    - **views/** : fichiers de développement compilables en HTML
      - *ajax/* : inclusions de retours ajax
      - *includes/* : éléments structurants du layout (header, footer, ...)
      - *layout/* : gabarits types extensibles avec des modules
      - *modules/* : contenu des pages injectés dans un gabarit type
      - *pages/* : gabarits de pages compilés
      - *tools/* : outils utilisés pour la compilation (mixins, ...)
    - **scripts/** : fichiers de développement compilables en JS
      - *app.js* : fichier de configuration dynamisable copié tel quel lors de la dynamisation. Il contient notamment tous les
      - *helpers/* : bibliothèque de méthodes propres au projet
      - *modules/* : fonctionnalités du projet
      - *tools/* : scripts externes concaténables et minifiables (extensions)
      - *vendors/* : fichiers copiés tels quels lors de la compilation (principalement des librairies)
    - **styles/** : fichiers de développement compilables en CSS
      - *tools/* : outils utilisés pour la compilation (mixins, variables, ...)

### Installation
Se référer à la documentation pour procéder à l'installation de npm et gulp :

- npm : [https://docs.npmjs.com/getting-started/installing-node](https://docs.npmjs.com/getting-started/installing-node)
- gulp : [https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)

### Développement
#### #installer le projet
Une fois npm et gulp installés, la commande `npm install` installe les ressources nécessaires au projet

#### #lancer le watcher
Lancer un watcher se fait en exécutant la commande `npm start` dans le terminal. Si `npm install` n'a pas été exécuté, l'installation se fait automatiquement à ce moment. Une fois le watcher lancé, le terminal indique que le watcher est en cours ainsi que le port où consulter le projet.

Le site sera consultable à l'adresse *http://localhost:3000*

#### #développer
Une fois le watcher lancé, la compilation des fichiers se fait automatiquement si les fichiers sont pris en compte dans la tâche gulp du watcher (c'est notamment le cas pour la plupart des fichiers compilables en HTML, CSS ou JS). Le watcher compile les fichiers à la volée en utilisant les tâches de compilation du projet (cf. partie ci-dessous).

### Compiler
Le *build* du projet peut se faire indépendamment du watcher. Celui-ci est exécutable via la commande `npm run build` (fichiers non minifiés, avertissements non bloquants) ou `npm run release` (avertissements bloquants). Les fichiers de développement sont alors générés dans le dossier *dist* et le site final est consultable en rafraîchissant la page si le watcher est lancé.

### Tâches npm
- **build** : lance la commande `gulp` qui construit le projet
- **fixup** : applique la convention de code javascript configuré dans _eslint_
- **release** : lance la commande `gulp` en environnement de production (minification)
- **start** : installe les dépendances, construit le projet puis lance le _watcher_
- **test** : valide que le javascript est conforme à la convention d'écriture défini dans _eslint_

### Tâches Gulp
- **clean** : supprime le dossier de build
- **config** : génère les fichiers de configuration présents dans le dossier *src/config*, le .json de dépendance pour la tâche stylus et les locales, en se basant sur les fichiers de configuration du dossier `/config`
- **copy** : copie les ressources concernées (medias, scripts non concaténables, ...)
- **css** : auto-préfixe et compile les fichiers `.styl` en `.css`.
- **html** : compile les fichiers .pug en .html
- **images** : optimise des images
- **js** : analyse les appels *require()* et les compile en un fichier *scripts.js*
- **json** : transforme les fichiers .yaml du dossier *src/ajax/* en .json dans le dossier *dist/ajax/*
- **resize** : crée des images 1x (*media/img/1x/*) à partir de fichiers 2x (*media/img/2x/*)
- **server** : lance le serveur local de consultation (par défaut, *http://localhost:3000*)
- **svg** : concatène les icônes depuis */assets/icons* en un seul fichier dans *dist/media/img/*
- **tree** : génère un fichier *index.html* listant les pages du projet
- **vendors** : copie les fichiers .js du dossier *scripts/vendors* à la racine du dossier js
- **watch** : lance le watcher de fichiers
