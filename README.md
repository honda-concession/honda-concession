# Installation

## Create virtual host
#### Add domain name to your hosts file :

    127.0.0.1   portail.honda
    127.0.0.1   concession.honda
    127.0.0.1   elastic.honda
    127.0.0.1   kibana.honda

## Run docker

### Start docker
#### Execute the following commands 

    cd project_dir/docker
    bash start.sh

### Stop docker
#### Execute the following commands 

    bash stop.sh

# Access
## Portail
Site url
https://portail.honda

Mysql:
    DB_NAME: portailbdd
    DB_USER: admin
    DB_PASS: admin

Phpmyadmin
http://localhost:8081/

## Concession
Site url
https://concession.honda

Mysql:
    DB_NAME: concessionbdd
    DB_USER: admin
    DB_PASS: admin


Phpmyadmin
http://localhost:8082/

## Elasticsearch
Url
http://elastic.honda:9200/

## Kibana
Url
http://kibana.honda:5601/
